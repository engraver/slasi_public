#include "../../alltypes.h"
#include "slsbwriter.h"

void writeSLSB(FILE * fid, double t) {

	/* find number of scalars and fields */
	int numberFields = 6;
	if (userVars.writingBasis == WRITINGBASIS_ON)
		numberFields += 9;
	if (userVars.writingEnergy == WRITINGENERGY_ON) {
		if (userVars.flexOnOff == FLEX_ON)
			numberFields += 12;
		else
			numberFields += 10;
	}
	if (userVars.writingExtField == WRITINGEXTFIELD_ON) {
		numberFields += 3;
	}

	/* the following number should be in agreement with `magninit.c:slsb()` and `convSLS` scrpt */

	/* basic number of scalar variables */
	int numberScalars = 27;
	if (userVars.launch == LAUNCH_MINIMIZATION) {
		numberScalars++; /* one additional scalar variable */
	}
	if (userVars.launch == LAUNCH_SIMULATION) {
		numberScalars += 6; /* six additional scalar variables */
	}

	/* number of scalars */
	fwrite(&numberScalars, sizeof(int), 1, fid); /* scalar field 1 */
	/* number of arrays */
	fwrite(&numberFields, sizeof(int), 1, fid); /* scalar field 2 */
	/* number of magnetic moments for each axis */
	int nn = userVars.N1 - 2;
	fwrite(&nn, sizeof(int), 1, fid); /* scalar field 3 */
	nn = userVars.N2 - 2;
	fwrite(&nn, sizeof(int), 1, fid); /* scalar field 4 */
	nn = userVars.N3 - 2;
	fwrite(&nn, sizeof(int), 1, fid); /* scalar field 5 */
	/* write type of lattice */
	int latticeType;
	if (userVars.latticeType == LATTICE_CUBIC) {
		latticeType = 0; // cubic
	} else {
		latticeType = 1; // triangular
	}
	fwrite(&latticeType, sizeof(int), 1, fid); /* scalar field 6 */
	/* write type of launch */
	int launchType;
	if (userVars.launch == LAUNCH_SIMULATION) {
		launchType = 0; // simulation
	} else if (userVars.launch == LAUNCH_MINIMIZATION) {
		launchType = 1; // minimization
	} else if (userVars.launch == LAUNCH_HYSTERESIS) {
		launchType = 2; // hysteresis
	}
	fwrite(&launchType, sizeof(int), 1, fid); /* scalar field 7 */
	/* write different energies */
	fwrite(&EnergyOfSystem, sizeof(double), 1, fid); /* scalar field 8 */
	fwrite(&EnergyExchange, sizeof(double), 1, fid); /* scalar field 9 */
	fwrite(&EnergyAnisotropyAxisK1, sizeof(double), 1, fid); /* scalar field 10 */
	fwrite(&EnergyAnisotropyAxisK2, sizeof(double), 1, fid); /* scalar field 11 */
	fwrite(&EnergyAnisotropyAxisK3, sizeof(double), 1, fid); /* scalar field 12 */
	fwrite(&EnergyDipole, sizeof(double), 1, fid); /* scalar field 13 */
	fwrite(&EnergyExternal, sizeof(double), 1, fid); /* scalar field 14 */
	fwrite(&EnergyDMIAxis1, sizeof(double), 1, fid); /* scalar field 15 */
	fwrite(&EnergyDMIAxis2, sizeof(double), 1, fid); /* scalar field 16 */
	fwrite(&EnergyDMIAxis3, sizeof(double), 1, fid); /* scalar field 17 */
	fwrite(&EnergyDMITriangleBulk, sizeof(double), 1, fid); /* scalar field 18 */
	fwrite(&EnergyDMITriangleInterface, sizeof(double), 1, fid); /* scalar field 19 */
	fwrite(&EnergyStretching, sizeof(double), 1, fid); /* scalar field 20 */
	fwrite(&EnergyBending, sizeof(double), 1, fid); /* scalar field 21 */
	/* write total magnetic moments for each axis */
	fwrite(&mxTot, sizeof(double), 1, fid); /* scalar field 22 */
	fwrite(&myTot, sizeof(double), 1, fid); /* scalar field 23 */
	fwrite(&mzTot, sizeof(double), 1, fid); /* scalar field 24 */
	/* write uniform external field */
	fwrite(&userVars.Bx, sizeof(double), 1, fid); /* scalar field 25 */
	fwrite(&userVars.By, sizeof(double), 1, fid); /* scalar field 26 */
	fwrite(&userVars.Bz, sizeof(double), 1, fid); /* scalar field 27 */
	if (userVars.launch == LAUNCH_SIMULATION) {
		/* write variables which are related to time (for simulation) */
		fwrite(&MaxDmDt, sizeof(double), 1, fid); /* scalar field 28 */
		fwrite(&t, sizeof(double), 1, fid); /* scalar field 29 */
		fwrite(&integrationStepMin, sizeof(double), 1, fid); /* scalar field 30 */
		fwrite(&integrationStepMax, sizeof(double), 1, fid); /* scalar field 31 */
		fwrite(&frameNumber, sizeof(unsigned long long), 1, fid); /* scalar field 32 */
		fwrite(&numberOfFile, sizeof(unsigned long long), 1, fid); /* scalar field 33 */
	} else if (userVars.launch == LAUNCH_MINIMIZATION) {
		/* write number of frames */
		fwrite(&numberFrames, sizeof(int), 1, fid); /* scalar field 28 */
	}

	/* IN ALL OTHER PLACES IT IS ASSUMED THAT MAGNETIZATION IS ALWAYS FIRST VECTOR FIELD! */
	/* auxiliary variables */
	double arrVector[3];
	size_t curIndex, indexLattice;
	/* write id for magnetic moments */
	nn = (int) BIN_MAGN;
	fwrite(&nn, sizeof(int), 1, fid);
	nn = (userVars.N1 - 2) * (userVars.N2 - 2) * (userVars.N3 - 2) * 3;
	fwrite(&nn, sizeof(int), 1, fid);
	/* write magnetic moments of each node */
	curIndex = 0;
	indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
				}
				else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				/* magnetic moments */
				if (latticeParam[indexLattice].magnetic) {
					arrVector[0] = lattice[curIndex];
					arrVector[1] = lattice[curIndex + 1];
					arrVector[2] = lattice[curIndex + 2];
				} else {
					arrVector[0] = SLS_NAN;
					arrVector[1] = SLS_NAN;
					arrVector[2] = SLS_NAN;
				}
				fwrite(&arrVector, sizeof(double), 3, fid);
			}
		}
	}

	/* write id for coordinates */
	nn = (int) BIN_COORDS;
	fwrite(&nn, sizeof(int), 1, fid);
	nn = (userVars.N1 - 2) * (userVars.N2 - 2) * (userVars.N3 - 2) * 3;
	fwrite(&nn, sizeof(int), 1, fid);
	/* write coordinates of each node */
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3) + spinsNum;
					indexLattice = (size_t)IDX(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2) + spinsNum;
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				/* coordinates */
				if (latticeParam[indexLattice].magnetic) {
					arrVector[0] = userVars.a * lattice[curIndex];
					arrVector[1] = userVars.a * lattice[curIndex + 1];
					arrVector[2] = userVars.a * lattice[curIndex + 2];
				} else  {
					arrVector[0] = SLS_NAN;
					arrVector[1] = SLS_NAN;
					arrVector[2] = SLS_NAN;
				}
				fwrite(&arrVector, sizeof(double), 3, fid);
			}
		}
	}

	/* write orthonormal basis of magnetic nodes */
	if (userVars.writingBasis == WRITINGBASIS_ON) {

		/* write id for TNB-basis */
		nn = (int) BIN_TNB;
		fwrite(&nn, sizeof(int), 1, fid);
		nn = (userVars.N1 - 2) * (userVars.N2 - 2) * (userVars.N3 - 2) * 9;
		fwrite(&nn, sizeof(int), 1, fid);

		/* write all vectors for TNB-basis of each node */
		indexLattice = 0;
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
					}
					else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
					}
					//indexLattice = IDX(i1, i2, i3);
					/* tangential vector */
					arrVector[0] = latticeParam[indexLattice].tx;
					arrVector[1] = latticeParam[indexLattice].ty;
					arrVector[2] = latticeParam[indexLattice].tz;
					fwrite(&arrVector, sizeof(double), 3, fid);
					/* normal vector */
					arrVector[0] = latticeParam[indexLattice].nx;
					arrVector[1] = latticeParam[indexLattice].ny;
					arrVector[2] = latticeParam[indexLattice].nz;
					fwrite(&arrVector, sizeof(double), 3, fid);
					/* binormal vector */
					arrVector[0] = latticeParam[indexLattice].bx;
					arrVector[1] = latticeParam[indexLattice].by;
					arrVector[2] = latticeParam[indexLattice].bz;
					fwrite(&arrVector, sizeof(double), 3, fid);
				}
			}
		}

	}

	/* auxiliary value to save energy */
	double energyValue;

	/* write different kinds of energy for magnetic nodes */
	if (userVars.writingEnergy == WRITINGENERGY_ON) {

		indexLattice = 0;
		/* find and normalize all kinds of energy for each node */
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
					}
					else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
					}
					//indexLattice = IDX(i1, i2, i3);
					/* normalize energy */
					latticeParam[indexLattice].EnergyExchange =
							latticeParam[indexLattice].EnergyExchangeNorm * userVars.NormCoefficientEnergy;
					latticeParam[indexLattice].EnergyAnisotropyAxisK1 =
							latticeParam[indexLattice].EnergyAnisotropyNormAxisK1 * userVars.NormCoefficientEnergy;
					latticeParam[indexLattice].EnergyAnisotropyAxisK2 =
							latticeParam[indexLattice].EnergyAnisotropyNormAxisK2 * userVars.NormCoefficientEnergy;
					latticeParam[indexLattice].EnergyAnisotropyAxisK3 =
							latticeParam[indexLattice].EnergyAnisotropyNormAxisK3 * userVars.NormCoefficientEnergy;
					latticeParam[indexLattice].EnergyDipole =
							latticeParam[indexLattice].EnergyDipoleNorm * userVars.NormCoefficientEnergy;
					latticeParam[indexLattice].EnergyExternal =
							latticeParam[indexLattice].EnergyExternalNorm * userVars.NormCoefficientEnergy;
					latticeParam[indexLattice].EnergyStretching =
							latticeParam[indexLattice].EnergyStretchingNorm * userVars.NormCoefficientEnergy;
					latticeParam[indexLattice].EnergyBending =
							latticeParam[indexLattice].EnergyBendingNorm * userVars.NormCoefficientEnergy;
					latticeParam[indexLattice].EnergyDMIAxis1 =
							latticeParam[indexLattice].EnergyDMIAxis1Norm * userVars.NormCoefficientEnergy;
					latticeParam[indexLattice].EnergyDMIAxis2 =
							latticeParam[indexLattice].EnergyDMIAxis2Norm * userVars.NormCoefficientEnergy;
					latticeParam[indexLattice].EnergyDMIAxis3 =
							latticeParam[indexLattice].EnergyDMIAxis3Norm * userVars.NormCoefficientEnergy;

					/* calculate total energy of spin */
					latticeParam[indexLattice].EnergyOfSpin = latticeParam[indexLattice].EnergyExchange +
					                                          latticeParam[indexLattice].EnergyAnisotropyAxisK1 +
					                                          latticeParam[indexLattice].EnergyAnisotropyAxisK2 +
					                                          latticeParam[indexLattice].EnergyAnisotropyAxisK3 +
					                                          latticeParam[indexLattice].EnergyDipole +
					                                          latticeParam[indexLattice].EnergyExternal +
					                                          latticeParam[indexLattice].EnergyStretching +
					                                          latticeParam[indexLattice].EnergyBending +
					                                          latticeParam[indexLattice].EnergyDMIAxis1 +
					                                          latticeParam[indexLattice].EnergyDMIAxis2 +
					                                          latticeParam[indexLattice].EnergyDMIAxis3;

				}
			}
		}

		/* write id for total energy of spin */
		nn = (int) BIN_SPIN;
		fwrite(&nn, sizeof(int), 1, fid);
		nn = (userVars.N1 - 2) * (userVars.N2 - 2) * (userVars.N3 - 2);
		fwrite(&nn, sizeof(int), 1, fid);

		indexLattice = 0;
		/* write total energy for each node */
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
					}
					else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
					}
					//indexLattice = IDX(i1, i2, i3);
					/* total energy of spin */
					energyValue = latticeParam[indexLattice].EnergyOfSpin;
					fwrite(&energyValue, sizeof(double), 1, fid);
				}
			}
		}

		/* write id for exchange energy of spin */
		nn = (int) BIN_EXCHANGE;
		fwrite(&nn, sizeof(int), 1, fid);
		nn = (userVars.N1 - 2) * (userVars.N2 - 2) * (userVars.N3 - 2);
		fwrite(&nn, sizeof(int), 1, fid);

		/* write exchange energy for each node */
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
					}
					else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
					}
					//indexLattice = IDX(i1, i2, i3);
					/* exchange energy of spin */
					energyValue = latticeParam[indexLattice].EnergyExchange;
					fwrite(&energyValue, sizeof(double), 1, fid);
				}
			}
		}

		/* write id for energy of anisotropy of spin */
		nn = (int) BIN_ANISOTROPY;
		fwrite(&nn, sizeof(int), 1, fid);
		nn = 3 * (userVars.N1 - 2) * (userVars.N2 - 2) * (userVars.N3 - 2);
		fwrite(&nn, sizeof(int), 1, fid);

		/* write energy of anisotropy for each node */
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
					}
					else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
					}
					//indexLattice = IDX(i1, i2, i3);
					/* energy of anisotropy of spin for 3 axes */
					arrVector[0] = latticeParam[indexLattice].EnergyAnisotropyAxisK1;
					arrVector[1] = latticeParam[indexLattice].EnergyAnisotropyAxisK2;
					arrVector[2] = latticeParam[indexLattice].EnergyAnisotropyAxisK3;
					fwrite(&arrVector, sizeof(double), 3, fid);
				}
			}
		}

		/* write id for dipole energy of spin */
		nn = (int) BIN_DIP;
		fwrite(&nn, sizeof(int), 1, fid);
		nn = (userVars.N1 - 2) * (userVars.N2 - 2) * (userVars.N3 - 2);
		fwrite(&nn, sizeof(int), 1, fid);

		/* write dipole energy for each node */
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
					}
					else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
					}
					//indexLattice = IDX(i1, i2, i3);
					/* dipole energy of spin */
					energyValue = latticeParam[indexLattice].EnergyDipole;
					fwrite(&energyValue, sizeof(double), 1, fid);
				}
			}
		}

		/* write id for energy of external field of spin */
		nn = (int) BIN_EXT;
		fwrite(&nn, sizeof(int), 1, fid);
		nn = (userVars.N1 - 2) * (userVars.N2 - 2) * (userVars.N3 - 2);
		fwrite(&nn, sizeof(int), 1, fid);

		/* write energy of external field for each node */
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
					}
					else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
					}
					//indexLattice = IDX(i1, i2, i3);
					/* energy of external field of spin */
					energyValue = latticeParam[indexLattice].EnergyExternal;
					fwrite(&energyValue, sizeof(double), 1, fid);
				}
			}
		}

		/* write id for energy of DMI interaction for spin */
		nn = (int) BIN_DMI;
		fwrite(&nn, sizeof(int), 1, fid);
		nn = 3 * (userVars.N1 - 2) * (userVars.N2 - 2) * (userVars.N3 - 2);
		fwrite(&nn, sizeof(int), 1, fid);

		/* write energy of DMI for each node */
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
					}
					else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
					}
					//indexLattice = IDX(i1, i2, i3);
					/* energy of DMI of spin for 3 axes */
					arrVector[0] = latticeParam[indexLattice].EnergyDMIAxis1;
					arrVector[1] = latticeParam[indexLattice].EnergyDMIAxis2;
					arrVector[2] = latticeParam[indexLattice].EnergyDMIAxis3;
					fwrite(&arrVector, sizeof(double), 3, fid);
				}
			}
		}

		/* interactions for flexible system */
		if (userVars.flexOnOff == FLEX_ON) {

			/* write id for energy of stretching interaction of spin */
			nn = (int) BIN_STR;
			fwrite(&nn, sizeof(int), 1, fid);
			nn = (userVars.N1 - 2) * (userVars.N2 - 2) * (userVars.N3 - 2);
			fwrite(&nn, sizeof(int), 1, fid);

			/* write energy of stretching interaction for each node */
			for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
				for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
					for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
						if (userVars.latticeType == LATTICE_CUBIC) {
							indexLattice = (size_t)IDX(i1, i2, i3);
						}
						else if (userVars.latticeType == LATTICE_TRIANGULAR) {
							indexLattice = (size_t)IDXtr(i1, i2);
						}
						//indexLattice = IDX(i1, i2, i3);
						/* energy of stretching interaction of spin */
						energyValue = latticeParam[indexLattice].EnergyStretching;
						fwrite(&energyValue, sizeof(double), 1, fid);
					}
				}
			}

			/* write id for energy of bending interaction of spin */
			nn = (int) BIN_BEND;
			fwrite(&nn, sizeof(int), 1, fid);
			nn = (userVars.N1 - 2) * (userVars.N2 - 2) * (userVars.N3 - 2);
			fwrite(&nn, sizeof(int), 1, fid);

			/* write energy of bending interaction for each node */
			for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
				for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
					for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
						if (userVars.latticeType == LATTICE_CUBIC) {
							indexLattice = (size_t)IDX(i1, i2, i3);
						}
						else if (userVars.latticeType == LATTICE_TRIANGULAR) {
							indexLattice = (size_t)IDXtr(i1, i2);
						}
						//indexLattice = IDX(i1, i2, i3);
						/* energy of bending interaction of spin */
						energyValue = latticeParam[indexLattice].EnergyBending;
						fwrite(&energyValue, sizeof(double), 1, fid);
					}
				}
			}

		}

	}

	/* write amplitude of external magnetic field */
	if (userVars.writingExtField == WRITINGEXTFIELD_ON) {

		/* write id for external magnetic field */
		nn = (int) BIN_EXTFIELD;
		fwrite(&nn, sizeof(int), 1, fid);
		/* write number of components */
		nn = (userVars.N1 - 2) * (userVars.N2 - 2) * (userVars.N3 - 2) * 3;
		fwrite(&nn, sizeof(int), 1, fid);
		/* write amplitudes for each node */
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					if (userVars.latticeType == LATTICE_CUBIC) {
						curIndex = (size_t)IDXmg(i1, i2, i3) + spinsNum;
						indexLattice = (size_t)IDX(i1, i2, i3);
					} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						curIndex = (size_t)IDXtrmg(i1, i2) + spinsNum;
						indexLattice = (size_t)IDXtr(i1, i2);
					}
					/* amplitudes of external magnetic field */
					if (latticeParam[indexLattice].magnetic) {
						arrVector[0] = Bamp[curIndex];
						arrVector[1] = Bamp[curIndex + 1];
						arrVector[2] = Bamp[curIndex + 2];
					} else  {
						arrVector[0] = SLS_NAN;
						arrVector[1] = SLS_NAN;
						arrVector[2] = SLS_NAN;
					}
					fwrite(&arrVector, sizeof(double), 3, fid);
				}
			}
		}

	}

}

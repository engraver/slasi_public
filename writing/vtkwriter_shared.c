#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "../../alltypes.h"
#include "vtkwriter_binary.h"
#include "../lattice/index_nums.h"

#define SIZE_ROW 200

/* function to write row */
void writeRow(FILE * fid, char * row) {

	/* copy row to buffer */
	char headRow[SIZE_ROW];
	memset(headRow, 0, SIZE_ROW * sizeof(char));
	strcpy(headRow, row);
	/* find length of row */
	int size = strlen(headRow);
	/* write row into file */
	fwrite(headRow, sizeof(char) * size, 1, fid);

}
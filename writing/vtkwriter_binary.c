#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "../../alltypes.h"
#include "vtkwriter_binary.h"
#include "vtkwriter_shared.h"
#include "../lattice/index_nums.h"

#define SIZE_ROW 200

/*
 * AVS UCD description
 * https://dav.lbl.gov/archive/NERSC/Software/express/help6.1/help/reference/dvmac/UCD_Form.htm
 *
 * VTK description
 * http://www.cs.utah.edu/~ssingla/Research/file-formats.pdf
 */

/* function to change byte order for double */
double swapDouble(double x) {

	double y;
	char * px = (char*) &x;
	char * py = (char*) &y;

	/* change byte order */
	for (unsigned int i = 0; i < sizeof(double); i++)
		py[i] = px[sizeof(double) - 1 - i];

	/* return value */
	return y;

}

/* function to change byte order for int */
int swapInt(int x) {

	int y;
	char * px = (char*) &x;
	char * py = (char*) &y;

	/* change byte order */
	for (unsigned int i = 0; i < sizeof(int); i++)
		py[i] = px[sizeof(int) - 1 - i];

	/* return value */
	return y;

}

/* function to write header rows */
void writeHeadersBinary(FILE *fid) {

	/* make temporary buffer */
	char tempBuffer[SIZE_ROW];
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));

	/* write head rows */
	writeRow(fid, "# vtk DataFile Version 4.2\n");
	writeRow(fid, "written by SLaSi\n");
	writeRow(fid, "BINARY\n");
	writeRow(fid, "DATASET UNSTRUCTURED_GRID\n");
	sprintf(tempBuffer, "%s %d %s\n", "POINTS", magnSitesNum, "double");
	writeRow(fid, tempBuffer);

}

/* function to write coordinates */
void writeCoordinatesBinary(FILE *fid) {

	/* make additional vector */
	double addVector[3];

	/* write coordinates */
	size_t curIndex = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t) IDXmg(i1, i2, i3) + spinsNum;
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t) IDXtrmg(i1, i2) + spinsNum;
				}
				/* find coordinates */
				addVector[0] = userVars.a * lattice[curIndex];
				addVector[1] = userVars.a * lattice[curIndex + 1];
				addVector[2] = userVars.a * lattice[curIndex + 2];
				/* swap byte order */
				addVector[0] = swapDouble(addVector[0]);
				addVector[1] = swapDouble(addVector[1]);
				addVector[2] = swapDouble(addVector[2]);
				/* write real numbers */
				fwrite(&addVector[0], sizeof(double), 1, fid);
				fwrite(&addVector[1], sizeof(double), 1, fid);
				fwrite(&addVector[2], sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

}

/* function to write data about cells */
void writeDataCellsBinary(FILE *fid) {

	/* make temporary buffer */
	char tempBuffer[SIZE_ROW];
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));

	/* cells */
	if (userVars.latticeType == LATTICE_TRIANGULAR) {
		sprintf(tempBuffer, "%s %d %d\n", "CELLS", connElements, 4 * connElements);
	} else if ((userVars.latticeType == LATTICE_CUBIC) && (userVars.N2 == 3) && (userVars.N3 == 3)) {
		sprintf(tempBuffer, "%s %d %d\n", "CELLS", connElements, 3 * connElements);
	} else if ((userVars.latticeType == LATTICE_CUBIC) && (userVars.N3 == 3)) {
		sprintf(tempBuffer, "%s %d %d\n", "CELLS", connElements, 5 * connElements);
	} else {
		sprintf(tempBuffer, "%s %d %d\n", "CELLS", connElements, 9 * connElements);
	}
	writeRow(fid, tempBuffer);

	/* write data about cells */
	if (userVars.latticeType == LATTICE_TRIANGULAR) {
		/* triangle */
		for (int i = 0; i < connElements; i++) {
			int tempValue1 = swapInt(3);
			int tempValue2 = swapInt(connectivity[i][CONN_FIRST_NGBR]);
			int tempValue3 = swapInt(connectivity[i][CONN_SECOND_NGBR]);
			int tempValue4 = swapInt(connectivity[i][CONN_THIRD_NGBR]);
			fwrite(&tempValue1, sizeof(int), 1, fid);
			fwrite(&tempValue2, sizeof(int), 1, fid);
			fwrite(&tempValue3, sizeof(int), 1, fid);
			fwrite(&tempValue4, sizeof(int), 1, fid);
		}
	} else if ((userVars.latticeType == LATTICE_CUBIC) && (userVars.N2 == 3) && (userVars.N3 == 3)) {
		/* line */
		for (int i = 0; i < connElements; i++) {
			int tempValue1 = swapInt(2);
			int tempValue2 = swapInt(connectivity[i][CONN_FIRST_NGBR]);
			int tempValue3 = swapInt(connectivity[i][CONN_SECOND_NGBR]);
			fwrite(&tempValue1, sizeof(int), 1, fid);
			fwrite(&tempValue2, sizeof(int), 1, fid);
			fwrite(&tempValue3, sizeof(int), 1, fid);
		}
	} else if ((userVars.latticeType == LATTICE_CUBIC) && (userVars.N3 == 3)) {
		/* square */
		for (int i = 0; i < connElements; i++) {
			int tempValue1 = swapInt(4);
			int tempValue2 = swapInt(connectivity[i][CONN_FIRST_NGBR]);
			int tempValue3 = swapInt(connectivity[i][CONN_SECOND_NGBR]);
			int tempValue4 = swapInt(connectivity[i][CONN_THIRD_NGBR]);
			int tempValue5 = swapInt(connectivity[i][CONN_FOURTH_NGBR]);
			fwrite(&tempValue1, sizeof(int), 1, fid);
			fwrite(&tempValue2, sizeof(int), 1, fid);
			fwrite(&tempValue3, sizeof(int), 1, fid);
			fwrite(&tempValue4, sizeof(int), 1, fid);
			fwrite(&tempValue5, sizeof(int), 1, fid);
		}
	} else {
		/* cubic */
		for (int i = 0; i < connElements; i++) {
			int tempValue1 = swapInt(8);
			int tempValue2 = swapInt(connectivity[i][CONN_FIRST_NGBR]);
			int tempValue3 = swapInt(connectivity[i][CONN_SECOND_NGBR]);
			int tempValue4 = swapInt(connectivity[i][CONN_THIRD_NGBR]);
			int tempValue5 = swapInt(connectivity[i][CONN_FOURTH_NGBR]);
			int tempValue6 = swapInt(connectivity[i][CONN_FIFTH_NGBR]);
			int tempValue7 = swapInt(connectivity[i][CONN_SIXTH_NGBR]);
			int tempValue8 = swapInt(connectivity[i][CONN_SEVENTH_NGBR]);
			int tempValue9 = swapInt(connectivity[i][CONN_EIGHTH_NGBR]);
			fwrite(&tempValue1, sizeof(int), 1, fid);
			fwrite(&tempValue2, sizeof(int), 1, fid);
			fwrite(&tempValue3, sizeof(int), 1, fid);
			fwrite(&tempValue4, sizeof(int), 1, fid);
			fwrite(&tempValue5, sizeof(int), 1, fid);
			fwrite(&tempValue6, sizeof(int), 1, fid);
			fwrite(&tempValue7, sizeof(int), 1, fid);
			fwrite(&tempValue8, sizeof(int), 1, fid);
			fwrite(&tempValue9, sizeof(int), 1, fid);
		}
	}
	writeRow(fid, "\n");

}

/* function to write data about types of cell */
void writeTypesCellBinary(FILE *fid) {

	/* make temporary buffer */
	char tempBuffer[SIZE_ROW];
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));

	/* types of cells */
	sprintf(tempBuffer, "%s %d\n", "CELL_TYPES", connElements);
	writeRow(fid, tempBuffer);

	/* write data about types of cells */
	int typeCells;
	if (userVars.latticeType == LATTICE_TRIANGULAR) {
		typeCells = 5;
	} else if ((userVars.latticeType == LATTICE_CUBIC) && (userVars.N2 == 3) && (userVars.N3 == 3)) {
		typeCells = 3;
	} else if ((userVars.latticeType == LATTICE_CUBIC) && (userVars.N3 == 3)) {
		typeCells = 9;
	} else {
		typeCells = 12;
	}
	int tempValueCells = swapInt(typeCells);
	for (int i = 0; i < connElements; i++) {
		fwrite(&tempValueCells, sizeof(int), 1, fid);
	}
	writeRow(fid, "\n");

}

/* function to write field fata */
void writeFieldDataBinary(FILE * fid) {

	/* make temporary buffer */
	char tempBuffer[SIZE_ROW];
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	/* make additional vector */
	double addVector[3];

	/* field data */
	sprintf(tempBuffer, "%s %d\n", "POINT_DATA", magnSitesNum);
	writeRow(fid, tempBuffer);
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	if (userVars.exchType == EXCH_INHOMOGENEOUS) {
		sprintf(tempBuffer, "%s %d\n", "FIELD FieldData", 19);
	} else {
		sprintf(tempBuffer, "%s %d\n", "FIELD FieldData", 13);
	}
	writeRow(fid, tempBuffer);

	/* write magnetic moments */
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	sprintf(tempBuffer, "%s %d %d double\n", "m", 3, magnSitesNum);
	writeRow(fid, tempBuffer);
	size_t indexLattice = 0;
	size_t curIndex = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
				}
				else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				if (latticeParam[indexLattice].magnetic) {
					addVector[0] = lattice[curIndex];
					addVector[1] = lattice[curIndex + 1];
					addVector[2] = lattice[curIndex + 2];
				} else {
					addVector[0] = SLS_NAN;
					addVector[1] = SLS_NAN;
					addVector[2] = SLS_NAN;
				}
				/* swap byte order */
				addVector[0] = swapDouble(addVector[0]);
				addVector[1] = swapDouble(addVector[1]);
				addVector[2] = swapDouble(addVector[2]);
				/* write real numbers */
				fwrite(&addVector[0], sizeof(double), 1, fid);
				fwrite(&addVector[1], sizeof(double), 1, fid);
				fwrite(&addVector[2], sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

	/* write the first vector of anisotropy */
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	sprintf(tempBuffer, "%s %d %d double\n", "ea1", 3, magnSitesNum);
	writeRow(fid, tempBuffer);
	curIndex = 0;
	indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
				}
				else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				addVector[0] = latticeParam[indexLattice].e11;
				addVector[1] = latticeParam[indexLattice].e12;
				addVector[2] = latticeParam[indexLattice].e13;
				/* swap byte order */
				addVector[0] = swapDouble(addVector[0]);
				addVector[1] = swapDouble(addVector[1]);
				addVector[2] = swapDouble(addVector[2]);
				/* write real numbers */
				fwrite(&addVector[0], sizeof(double), 1, fid);
				fwrite(&addVector[1], sizeof(double), 1, fid);
				fwrite(&addVector[2], sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

	/* write the second vector of anisotropy */
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	sprintf(tempBuffer, "%s %d %d double\n", "ea2", 3, magnSitesNum);
	writeRow(fid, tempBuffer);
	curIndex = 0;
	indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
				}
				else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				addVector[0] = latticeParam[indexLattice].e21;
				addVector[1] = latticeParam[indexLattice].e22;
				addVector[2] = latticeParam[indexLattice].e23;
				/* swap byte order */
				addVector[0] = swapDouble(addVector[0]);
				addVector[1] = swapDouble(addVector[1]);
				addVector[2] = swapDouble(addVector[2]);
				/* write real numbers */
				fwrite(&addVector[0], sizeof(double), 1, fid);
				fwrite(&addVector[1], sizeof(double), 1, fid);
				fwrite(&addVector[2], sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

	/* write the third vector of anisotropy */
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	sprintf(tempBuffer, "%s %d %d double\n", "ea3", 3, magnSitesNum);
	writeRow(fid, tempBuffer);
	curIndex = 0;
	indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
				}
				else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				addVector[0] = latticeParam[indexLattice].e31;
				addVector[1] = latticeParam[indexLattice].e32;
				addVector[2] = latticeParam[indexLattice].e33;
				/* swap byte order */
				addVector[0] = swapDouble(addVector[0]);
				addVector[1] = swapDouble(addVector[1]);
				addVector[2] = swapDouble(addVector[2]);
				/* write real numbers */
				fwrite(&addVector[0], sizeof(double), 1, fid);
				fwrite(&addVector[1], sizeof(double), 1, fid);
				fwrite(&addVector[2], sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

	/* write the first coefficient of anisotropy */
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	sprintf(tempBuffer, "%s %d %d double\n", "aniK1", 1, magnSitesNum);
	writeRow(fid, tempBuffer);
	curIndex = 0;
	indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
				}
				else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				double tempValue1 = latticeParam[indexLattice].aniK1;
				tempValue1 = swapDouble(tempValue1);
				fwrite(&tempValue1, sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

	/* write the second coefficient of anisotropy */
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	sprintf(tempBuffer, "%s %d %d double\n", "aniK1", 1, magnSitesNum);
	writeRow(fid, tempBuffer);
	curIndex = 0;
	indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
				}
				else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				double tempValue1 = latticeParam[indexLattice].aniK2;
				tempValue1 = swapDouble(tempValue1);
				fwrite(&tempValue1, sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

	/* write the third coefficient of anisotropy */
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	sprintf(tempBuffer, "%s %d %d double\n", "aniK1", 1, magnSitesNum);
	writeRow(fid, tempBuffer);
	curIndex = 0;
	indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
				}
				else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				double tempValue1 = latticeParam[indexLattice].aniK3;
				tempValue1 = swapDouble(tempValue1);
				fwrite(&tempValue1, sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

	/* write the first vector of TNB-basis */
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	sprintf(tempBuffer, "%s %d %d double\n", "e1", 3, magnSitesNum);
	writeRow(fid, tempBuffer);
	curIndex = 0;
	indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
				}
				else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				addVector[0] = latticeParam[indexLattice].tx;
				addVector[1] = latticeParam[indexLattice].ty;
				addVector[2] = latticeParam[indexLattice].tz;
				/* swap byte order */
				addVector[0] = swapDouble(addVector[0]);
				addVector[1] = swapDouble(addVector[1]);
				addVector[2] = swapDouble(addVector[2]);
				/* write real numbers */
				fwrite(&addVector[0], sizeof(double), 1, fid);
				fwrite(&addVector[1], sizeof(double), 1, fid);
				fwrite(&addVector[2], sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

	/* write the second vector of anisotropy */
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	sprintf(tempBuffer, "%s %d %d double\n", "e2", 3, magnSitesNum);
	writeRow(fid, tempBuffer);
	curIndex = 0;
	indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
				}
				else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				addVector[0] = latticeParam[indexLattice].nx;
				addVector[1] = latticeParam[indexLattice].ny;
				addVector[2] = latticeParam[indexLattice].nz;
				/* swap byte order */
				addVector[0] = swapDouble(addVector[0]);
				addVector[1] = swapDouble(addVector[1]);
				addVector[2] = swapDouble(addVector[2]);
				/* write real numbers */
				fwrite(&addVector[0], sizeof(double), 1, fid);
				fwrite(&addVector[1], sizeof(double), 1, fid);
				fwrite(&addVector[2], sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

	/* write the third vector of anisotropy */
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	sprintf(tempBuffer, "%s %d %d double\n", "e3", 3, magnSitesNum);
	writeRow(fid, tempBuffer);
	curIndex = 0;
	indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
				}
				else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				addVector[0] = latticeParam[indexLattice].bx;
				addVector[1] = latticeParam[indexLattice].by;
				addVector[2] = latticeParam[indexLattice].bz;
				/* swap byte order */
				addVector[0] = swapDouble(addVector[0]);
				addVector[1] = swapDouble(addVector[1]);
				addVector[2] = swapDouble(addVector[2]);
				/* write real numbers */
				fwrite(&addVector[0], sizeof(double), 1, fid);
				fwrite(&addVector[1], sizeof(double), 1, fid);
				fwrite(&addVector[2], sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

	/* write vector of external field */
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	sprintf(tempBuffer, "%s %d %d double\n", "B", 3, magnSitesNum);
	writeRow(fid, tempBuffer);
	curIndex = 0;
	indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
				}
				else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				addVector[0] = Bamp[curIndex];
				addVector[1] = Bamp[curIndex + 1];
				addVector[2] = Bamp[curIndex + 2];
				/* swap byte order */
				addVector[0] = swapDouble(addVector[0]);
				addVector[1] = swapDouble(addVector[1]);
				addVector[2] = swapDouble(addVector[2]);
				/* write real numbers */
				fwrite(&addVector[0], sizeof(double), 1, fid);
				fwrite(&addVector[1], sizeof(double), 1, fid);
				fwrite(&addVector[2], sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

	/* write sublattice */
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	sprintf(tempBuffer, "%s %d %d double\n", "sublattice", 1, magnSitesNum);
	writeRow(fid, tempBuffer);
	int subl = 0;
	double double_subl = 0.0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					subl = ((i1 & 0x1) ^ (i2 & 0x1) ) ^ (i3 & 0x1);
				}
				double_subl = (double) subl;
				double_subl = swapDouble(double_subl);
				fwrite(&double_subl, sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

	/* write sublattice_ext */
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
	sprintf(tempBuffer, "%s %d %d double\n", "sublattice_ext", 1, magnSitesNum);
	writeRow(fid, tempBuffer);
	int sublext = 0;
	double double_sublext = 0.0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					sublext = (i1 & 0x1) + 2*(i2 & 0x1) + 4*(i3 & 0x1);
				}
				double_sublext = (double) sublext;
				double_sublext = swapDouble(double_sublext);
				fwrite(&double_sublext, sizeof(double), 1, fid);
			}
		}
	}
	writeRow(fid, "\n");

	if (userVars.exchType == EXCH_INHOMOGENEOUS) {

		/* write J1 or Jm1 */
		memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
		if (userVars.latticeType == LATTICE_TRIANGULAR) {
			sprintf(tempBuffer, "%s %d %d double\n", "J1", 1, magnSitesNum);
		} else if (userVars.latticeType == LATTICE_CUBIC) {
			sprintf(tempBuffer, "%s %d %d double\n", "Jm1", 1, magnSitesNum);
		}
		writeRow(fid, tempBuffer);
		indexLattice = 0;
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					double tempValue = 0.0;
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
						tempValue = swapDouble(latticeParam[indexLattice].Jm1);
					} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
						tempValue = swapDouble(latticeParam[indexLattice].J1);
					}
					fwrite(&tempValue, sizeof(double), 1, fid);
				}
			}
		}
		writeRow(fid, "\n");

		/* write J2 or Jp1 */
		memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
		if (userVars.latticeType == LATTICE_TRIANGULAR) {
			sprintf(tempBuffer, "%s %d %d double\n", "J2", 1, magnSitesNum);
		} else if (userVars.latticeType == LATTICE_CUBIC) {
			sprintf(tempBuffer, "%s %d %d double\n", "Jp1", 1, magnSitesNum);
		}
		writeRow(fid, tempBuffer);
		indexLattice = 0;
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					double tempValue = 0.0;
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
						tempValue = swapDouble(latticeParam[indexLattice].Jp1);
					} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
						tempValue = swapDouble(latticeParam[indexLattice].J2);
					}
					fwrite(&tempValue, sizeof(double), 1, fid);
				}
			}
		}
		writeRow(fid, "\n");

		/* write J3 or Jm2 */
		memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
		if (userVars.latticeType == LATTICE_TRIANGULAR) {
			sprintf(tempBuffer, "%s %d %d double\n", "J3", 1, magnSitesNum);
		} else if (userVars.latticeType == LATTICE_CUBIC) {
			sprintf(tempBuffer, "%s %d %d double\n", "Jm2", 1, magnSitesNum);
		}
		writeRow(fid, tempBuffer);
		indexLattice = 0;
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					double tempValue = 0.0;
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
						tempValue = swapDouble(latticeParam[indexLattice].Jm2);
					} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
						tempValue = swapDouble(latticeParam[indexLattice].J3);
					}
					fwrite(&tempValue, sizeof(double), 1, fid);
				}
			}
		}
		writeRow(fid, "\n");

		/* write J4 or Jp2 */
		memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
		if (userVars.latticeType == LATTICE_TRIANGULAR) {
			sprintf(tempBuffer, "%s %d %d double\n", "J4", 1, magnSitesNum);
		} else if (userVars.latticeType == LATTICE_CUBIC) {
			sprintf(tempBuffer, "%s %d %d double\n", "Jp2", 1, magnSitesNum);
		}
		writeRow(fid, tempBuffer);
		indexLattice = 0;
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					double tempValue = 0.0;
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
						tempValue = swapDouble(latticeParam[indexLattice].Jp2);
					} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
						tempValue = swapDouble(latticeParam[indexLattice].J4);
					}
					fwrite(&tempValue, sizeof(double), 1, fid);
				}
			}
		}
		writeRow(fid, "\n");

		/* write J5 or Jm3 */
		memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
		if (userVars.latticeType == LATTICE_TRIANGULAR) {
			sprintf(tempBuffer, "%s %d %d double\n", "J5", 1, magnSitesNum);
		} else if (userVars.latticeType == LATTICE_CUBIC) {
			sprintf(tempBuffer, "%s %d %d double\n", "Jm3", 1, magnSitesNum);
		}
		writeRow(fid, tempBuffer);
		indexLattice = 0;
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					double tempValue = 0.0;
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
						tempValue = swapDouble(latticeParam[indexLattice].Jm3);
					} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
						tempValue = swapDouble(latticeParam[indexLattice].J5);
					}
					fwrite(&tempValue, sizeof(double), 1, fid);
				}
			}
		}
		writeRow(fid, "\n");

		/* write J6 or Jp3 */
		memset(tempBuffer, 0, SIZE_ROW * sizeof(char));
		if (userVars.latticeType == LATTICE_TRIANGULAR) {
			sprintf(tempBuffer, "%s %d %d double\n", "J6", 1, magnSitesNum);
		} else if (userVars.latticeType == LATTICE_CUBIC) {
			sprintf(tempBuffer, "%s %d %d double\n", "Jp3", 1, magnSitesNum);
		}
		writeRow(fid, tempBuffer);
		indexLattice = 0;
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					double tempValue = 0.0;
					if (userVars.latticeType == LATTICE_CUBIC) {
						indexLattice = (size_t)IDX(i1, i2, i3);
						tempValue = swapDouble(latticeParam[indexLattice].Jp3);
					} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						indexLattice = (size_t)IDXtr(i1, i2);
						tempValue = swapDouble(latticeParam[indexLattice].J6);
					}
					fwrite(&tempValue, sizeof(double), 1, fid);
				}
			}
		}
		writeRow(fid, "\n");

	}

}

void writeVTK_binary(FILE * fid, double t, double MaxDmDt, double MaxDrDt) {

	/* write headers */
	writeHeadersBinary(fid);
	/* write coordinates */
	writeCoordinatesBinary(fid);
	/* write data about cells */
	writeDataCellsBinary(fid);
	/* write data about types of cell */
	writeTypesCellBinary(fid);
	/* write field data */
	writeFieldDataBinary(fid);

}


#ifndef _UCD_WRITER_H_
#define _UCD_WRITER_H_

/**
 * @brief writeAVS_ASCII, saves content of AVS UCD format (ASCII)
 * @param fid `FILE` identifier. Should be opened and closed by the calling method.
 * @param t Time in ps
 * @param MaxDmDt :math:`\mathrm{max}\, \left| \frac{d\vec{m}}{dt} \right|`
 * @param MaxDrDt :math:`\mathrm{max}\, \left| \frac{d\vec{r}}{dt} \right|`
 */
void writeAVS_ASCII(FILE * fid, double t, double MaxDmDt, double MaxDrDt);

#endif

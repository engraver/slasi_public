#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "../../alltypes.h"
#include "vtkwriter_xmlbase64.h"
#include "vtkwriter_shared.h"
#include "../lattice/index_nums.h"
#include <unistd.h>

#define SIZE_ROW 200

/* basic row for base64 transformation */
char rowBase64[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/* function to write base64 array */
void writeBase64Array(FILE *fid, unsigned char * symbols, unsigned int sizeByteArray) {

	/* intermediate char array for working */
	unsigned char base64Code[4];

	/* go through all symbols */
	unsigned int i;
	for (i = 0; i <= sizeByteArray - 3; i += 3) {
		/* convert main symbols to numbers */
		//printf("%i\n", (symbols[i] & ((unsigned char) 252)) / ((unsigned char) 4));
		//usleep(1000000);
		base64Code[0] = rowBase64[(symbols[i] & ((char) 252)) >> 2];
		base64Code[1] = rowBase64[((symbols[i] & ((char) 3)) << 4) + ((symbols[i + 1] & ((char) 240)) >> 4)];
		base64Code[2] = rowBase64[((symbols[i + 1] & ((char) 15)) << 2) + ((symbols[i + 2] & ((char) 192)) >> 6)];
		base64Code[3] = rowBase64[symbols[i + 2] & ((char) 63)];
		/* write new portion of data */
		fwrite(&base64Code, 4 * sizeof(char), 1, fid);
	}

	/* convert last symbols */
	if (sizeByteArray % 3 == 2) {
		base64Code[0] = rowBase64[(symbols[i] & ((char) 252)) >> 2];
		base64Code[1] = rowBase64[((symbols[i] & ((char) 3)) << 4) + ((symbols[i + 1] & ((char) 240)) >> 4)];
		base64Code[2] = rowBase64[(symbols[i + 1] & ((char) 15)) << 2];
		base64Code[3] = '=';
		/* write new portion of data */
		fwrite(&base64Code, 4 * sizeof(char), 1, fid);
	} else if (sizeByteArray % 3 == 1) {
		/* if number of symbols executes such condition (n % 3 == 1) */
		base64Code[0] = rowBase64[(symbols[i] & ((char) 252)) >> 2];
		base64Code[1] = rowBase64[(symbols[i] & ((char) 3)) << 4];
		base64Code[2] = '=';
		base64Code[3] = '=';
		/* write new portion of data */
		fwrite(&base64Code, 4 * sizeof(char), 1, fid);
	}

}

/* function to write data about exchange interaction */
void writeExchangeXMLBase64(FILE *fid) {

	/* intermediate variables */
	unsigned int sizeBase64Array = magnSitesNum * sizeof(double);
	unsigned int * unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	unsigned int sizeByteArray = 4;

	/* save the first coefficient of exchange interaction */
	double * doubleArray = (double *) (unsignedIntArray + 1);
	size_t indexLattice = 0;
	unsigned int index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t) IDX(i1, i2, i3);
					doubleArray[index] = latticeParam[indexLattice].Jm1;
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t) IDXtr(i1, i2);
					doubleArray[index] = latticeParam[indexLattice].J1;
				}
				index += 1;
				sizeByteArray += 8;
			}
		}
	}

	/* write array of the first coefficient of exchange interaction */
	if (userVars.latticeType == LATTICE_CUBIC) {
		writeRow(fid, "<DataArray type=\"Float64\" Name=\"Jm1\" NumberOfComponents=\"1\" format=\"binary\">\n");
	} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
		writeRow(fid, "<DataArray type=\"Float64\" Name=\"J1\" NumberOfComponents=\"1\" format=\"binary\">\n");
	}
	unsigned char * byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables */
	sizeBase64Array = magnSitesNum * sizeof(double);
	unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save the second coefficient of exchange interaction */
	doubleArray = (double *) (unsignedIntArray + 1);
	indexLattice = 0;
	index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t) IDX(i1, i2, i3);
					doubleArray[index] = latticeParam[indexLattice].Jp1;
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t) IDXtr(i1, i2);
					doubleArray[index] = latticeParam[indexLattice].J2;
				}
				index += 1;
				sizeByteArray += 8;
			}
		}
	}

	/* write array of the second coefficient of exchange interaction */
	if (userVars.latticeType == LATTICE_CUBIC) {
		writeRow(fid, "<DataArray type=\"Float64\" Name=\"Jp1\" NumberOfComponents=\"1\" format=\"binary\">\n");
	} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
		writeRow(fid, "<DataArray type=\"Float64\" Name=\"J2\" NumberOfComponents=\"1\" format=\"binary\">\n");
	}
	byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables */
	sizeBase64Array = magnSitesNum * sizeof(double);
	unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save the third coefficient of exchange interaction */
	doubleArray = (double *) (unsignedIntArray + 1);
	indexLattice = 0;
	index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t) IDX(i1, i2, i3);
					doubleArray[index] = latticeParam[indexLattice].Jm2;
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t) IDXtr(i1, i2);
					doubleArray[index] = latticeParam[indexLattice].J3;
				}
				index += 1;
				sizeByteArray += 8;
			}
		}
	}

	/* write array of the third coefficient of exchange interaction */
	if (userVars.latticeType == LATTICE_CUBIC) {
		writeRow(fid, "<DataArray type=\"Float64\" Name=\"Jm2\" NumberOfComponents=\"1\" format=\"binary\">\n");
	} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
		writeRow(fid, "<DataArray type=\"Float64\" Name=\"J3\" NumberOfComponents=\"1\" format=\"binary\">\n");
	}
	byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables */
	sizeBase64Array = magnSitesNum * sizeof(double);
	unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save the fourth coefficient of exchange interaction */
	doubleArray = (double *) (unsignedIntArray + 1);
	indexLattice = 0;
	index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t) IDX(i1, i2, i3);
					doubleArray[index] = latticeParam[indexLattice].Jp2;
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t) IDXtr(i1, i2);
					doubleArray[index] = latticeParam[indexLattice].J4;
				}
				index += 1;
				sizeByteArray += 8;
			}
		}
	}

	/* write array of the fourth coefficient of exchange interaction */
	if (userVars.latticeType == LATTICE_CUBIC) {
		writeRow(fid, "<DataArray type=\"Float64\" Name=\"Jp2\" NumberOfComponents=\"1\" format=\"binary\">\n");
	} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
		writeRow(fid, "<DataArray type=\"Float64\" Name=\"J4\" NumberOfComponents=\"1\" format=\"binary\">\n");
	}
	byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables */
	sizeBase64Array = magnSitesNum * sizeof(double);
	unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save the fifth coefficient of exchange interaction */
	doubleArray = (double *) (unsignedIntArray + 1);
	indexLattice = 0;
	index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t) IDX(i1, i2, i3);
					doubleArray[index] = latticeParam[indexLattice].Jm3;
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t) IDXtr(i1, i2);
					doubleArray[index] = latticeParam[indexLattice].J5;
				}
				index += 1;
				sizeByteArray += 8;
			}
		}
	}

	/* write array of the fifth coefficient of exchange interaction */
	if (userVars.latticeType == LATTICE_CUBIC) {
		writeRow(fid, "<DataArray type=\"Float64\" Name=\"Jm3\" NumberOfComponents=\"1\" format=\"binary\">\n");
	} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
		writeRow(fid, "<DataArray type=\"Float64\" Name=\"J5\" NumberOfComponents=\"1\" format=\"binary\">\n");
	}
	byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables */
	sizeBase64Array = magnSitesNum * sizeof(double);
	unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save the sixth coefficient of exchange interaction */
	doubleArray = (double *) (unsignedIntArray + 1);
	indexLattice = 0;
	index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t) IDX(i1, i2, i3);
					doubleArray[index] = latticeParam[indexLattice].Jp3;
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t) IDXtr(i1, i2);
					doubleArray[index] = latticeParam[indexLattice].J6;
				}
				index += 1;
				sizeByteArray += 8;
			}
		}
	}

	/* write array of the sixth coefficient of exchange interaction */
	if (userVars.latticeType == LATTICE_CUBIC) {
		writeRow(fid, "<DataArray type=\"Float64\" Name=\"Jp3\" NumberOfComponents=\"1\" format=\"binary\">\n");
	} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
		writeRow(fid, "<DataArray type=\"Float64\" Name=\"J6\" NumberOfComponents=\"1\" format=\"binary\">\n");
	}
	byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

}

/* function to write header rows */
void writeHeadersXMLBase64(FILE *fid) {

	/* make temporary buffer */
	char tempBuffer[SIZE_ROW];
	memset(tempBuffer, 0, SIZE_ROW * sizeof(char));

	/* write head rows */
	writeRow(fid, "<?xml version=\"1.0\"?>\n");
	writeRow(fid, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	writeRow(fid, "<UnstructuredGrid>\n");
	sprintf(tempBuffer, "<Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n", magnSitesNum, connElements);
	writeRow(fid, tempBuffer);

}

/* function to write coordinates */
void writeCoordinatesBinaryXMLBase64(FILE *fid) {

	/* write headers */
	writeRow(fid, "<Points>\n");
	writeRow(fid, "<DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"binary\">\n");

	/* intermediate variables */
	unsigned int sizeBase64Array = 3 * magnSitesNum * sizeof(double);
	unsigned int * unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	unsigned int sizeByteArray = 4;

	/* save coordinates */
	double * doubleArray = (double *) (unsignedIntArray + 1);
	size_t curIndex = 0;
	unsigned int index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t) IDXmg(i1, i2, i3) + spinsNum;
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t) IDXtrmg(i1, i2) + spinsNum;
				}
				doubleArray[index] = userVars.a * lattice[curIndex];
				doubleArray[index + 1] = userVars.a * lattice[curIndex + 1];
				doubleArray[index + 2] = userVars.a * lattice[curIndex + 2];
				index += 3;
				sizeByteArray += 24;
			}
		}
	}

	/* write array of coordinates after type transformation */
	unsigned char * byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");

	/* write final rows */
	writeRow(fid, "</DataArray>\n");
	writeRow(fid, "</Points>\n");

}

/* function to write data about cells */
void writeDataCellsXMLBase64(FILE *fid) {

	/* write header */
	writeRow(fid, "<Cells>\n");

	/* intermediate variables ("connection" array) */
	unsigned int sizeBase64ArrayConn;
	if (userVars.latticeType == LATTICE_TRIANGULAR) {
		sizeBase64ArrayConn = 3 * connElements * sizeof(int);
	} else if ((userVars.latticeType == LATTICE_CUBIC) && (userVars.N2 == 3) && (userVars.N3 == 3)) {
		sizeBase64ArrayConn = 2 * connElements * sizeof(int);
	} else if ((userVars.latticeType == LATTICE_CUBIC) && (userVars.N3 == 3)) {
		sizeBase64ArrayConn = 4 * connElements * sizeof(int);
	} else {
		sizeBase64ArrayConn = 8 * connElements * sizeof(int);
	}
	unsigned int * unsignedIntArrayConn = (unsigned int *) (addIntVectorConnXML);
	unsignedIntArrayConn[0] = sizeBase64ArrayConn;
	unsigned int sizeByteArrayConn = 4;

	/* intermediate variables (integer array) */
	unsigned int sizeBase64Array = connElements * sizeof(int);
	unsigned int * unsignedIntArray = (unsigned int *) (addIntVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	unsigned int sizeByteArray = 4;

	/* go through all connection elements */
	int * connectionArray = (int *) (unsignedIntArrayConn + 1);
	int * intArray = (int *) (unsignedIntArray + 1);
	int offset = 0;
	int sizeArrayConn = 0;
	for (int i = 0; i < connElements; i++) {
		if (userVars.latticeType == LATTICE_TRIANGULAR) {
			connectionArray[sizeArrayConn] = connectivity[i][CONN_FIRST_NGBR];
			connectionArray[sizeArrayConn + 1] = connectivity[i][CONN_SECOND_NGBR];
			connectionArray[sizeArrayConn + 2] = connectivity[i][CONN_THIRD_NGBR];
			intArray[i] = offset + 3;
			offset = offset + 3;
			sizeByteArrayConn += 12;
			sizeArrayConn += 3;
			sizeByteArray += 4;
		} else if ((userVars.latticeType == LATTICE_CUBIC) && (userVars.N2 == 3) && (userVars.N3 == 3)) {
			connectionArray[sizeArrayConn] = connectivity[i][CONN_FIRST_NGBR];
			connectionArray[sizeArrayConn + 1] = connectivity[i][CONN_SECOND_NGBR];
			intArray[i] = offset + 2;
			offset = offset + 2;
			sizeByteArrayConn += 8;
			sizeArrayConn += 2;
			sizeByteArray += 4;
		} else if ((userVars.latticeType == LATTICE_CUBIC) && (userVars.N3 == 3)) {
			connectionArray[sizeArrayConn] = connectivity[i][CONN_FIRST_NGBR];
			connectionArray[sizeArrayConn + 1] = connectivity[i][CONN_SECOND_NGBR];
			connectionArray[sizeArrayConn + 2] = connectivity[i][CONN_THIRD_NGBR];
			connectionArray[sizeArrayConn + 3] = connectivity[i][CONN_FOURTH_NGBR];
			intArray[i] = offset + 4;
			offset = offset + 4;
			sizeByteArrayConn += 16;
			sizeArrayConn += 4;
			sizeByteArray += 4;
		} else {
			connectionArray[sizeArrayConn] = connectivity[i][CONN_FIRST_NGBR];
			connectionArray[sizeArrayConn + 1] = connectivity[i][CONN_SECOND_NGBR];
			connectionArray[sizeArrayConn + 2] = connectivity[i][CONN_THIRD_NGBR];
			connectionArray[sizeArrayConn + 3] = connectivity[i][CONN_FOURTH_NGBR];
			connectionArray[sizeArrayConn + 4] = connectivity[i][CONN_FIFTH_NGBR];
			connectionArray[sizeArrayConn + 5] = connectivity[i][CONN_SIXTH_NGBR];
			connectionArray[sizeArrayConn + 6] = connectivity[i][CONN_SEVENTH_NGBR];
			connectionArray[sizeArrayConn + 7] = connectivity[i][CONN_EIGHTH_NGBR];
			intArray[i] = offset + 8;
			offset = offset + 8;
			sizeByteArrayConn += 32;
			sizeArrayConn += 8;
			sizeByteArray += 4;
		}
	}

	/* write data about connectivity */
	writeRow(fid, "<DataArray type=\"Int32\" Name=\"connectivity\" format=\"binary\">\n");
	unsigned char * byteArrayConn = (unsigned char *) addIntVectorConnXML;
	writeBase64Array(fid, byteArrayConn, sizeByteArrayConn);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* write data about offsets */
	writeRow(fid, "<DataArray type=\"Int32\" Name=\"offsets\" format=\"binary\">\n");
	unsigned char * byteArray = (unsigned char *) addIntVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables (integer array) */
	sizeBase64Array = connElements * sizeof(int);
	unsignedIntArray = (unsigned int *) (addIntVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* go through all connection elements */
	intArray = (int *) (unsignedIntArray + 1);
	for (int i = 0; i < connElements; i++) {
		if (userVars.latticeType == LATTICE_TRIANGULAR) {
			intArray[i] = 5;
			sizeByteArray += 4;
		} else if ((userVars.latticeType == LATTICE_CUBIC) && (userVars.N2 == 3) && (userVars.N3 == 3)) {
			intArray[i] = 3;
			sizeByteArray += 4;
		} else if ((userVars.latticeType == LATTICE_CUBIC) && (userVars.N3 == 3)) {
			intArray[i] = 9;
			sizeByteArray += 4;
		} else {
			intArray[i] = 12;
			sizeByteArray += 4;
		}
	}

	/* write data about types of cells */
	writeRow(fid, "<DataArray type=\"Int32\" Name=\"types\" format=\"binary\">\n");
    byteArray = (unsigned char *) addIntVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* write final row */
	writeRow(fid, "</Cells>\n");

}

/* function to write field fata */
void writeFieldDataXMLBase64(FILE * fid) {

	/* write header */
	writeRow(fid, "<PointData>\n");

	/* intermediate variables */
	unsigned int sizeBase64Array = 3 * magnSitesNum * sizeof(double);
	unsigned int * unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	unsigned int sizeByteArray = 4;

	/* save magnetic moments */
	double * doubleArray = (double *) (unsignedIntArray + 1);
	size_t curIndex = 0;
	unsigned int index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t) IDXmg(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t) IDXtrmg(i1, i2);
				}
				doubleArray[index] = lattice[curIndex];
				doubleArray[index + 1] = lattice[curIndex + 1];
				doubleArray[index + 2] = lattice[curIndex + 2];
				index += 3;
				sizeByteArray += 24;
			}
		}
	}

	/* write array of magnetic moments */
	writeRow(fid, "<DataArray type=\"Float64\" Name=\"m\" NumberOfComponents=\"3\" format=\"binary\">\n");
	unsigned char * byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables */
	sizeBase64Array = 3 * magnSitesNum * sizeof(double);
	unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save the first vector of anisotropy */
	doubleArray = (double *) (unsignedIntArray + 1);
	size_t indexLattice = 0;
	index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t) IDX(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t) IDXtr(i1, i2);
				}
				doubleArray[index] = latticeParam[indexLattice].e11;
				doubleArray[index + 1] = latticeParam[indexLattice].e12;
				doubleArray[index + 2] = latticeParam[indexLattice].e13;
				index += 3;
				sizeByteArray += 24;
			}
		}
	}

	/* write array of the first vector of anisotropy */
	writeRow(fid, "<DataArray type=\"Float64\" Name=\"ea1\" NumberOfComponents=\"3\" format=\"binary\">\n");
	byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables */
	sizeBase64Array = 3 * magnSitesNum * sizeof(double);
	unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save the second vector of anisotropy */
	doubleArray = (double *) (unsignedIntArray + 1);
	indexLattice = 0;
	index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t) IDX(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t) IDXtr(i1, i2);
				}
				doubleArray[index] = latticeParam[indexLattice].e21;
				doubleArray[index + 1] = latticeParam[indexLattice].e22;
				doubleArray[index + 2] = latticeParam[indexLattice].e23;
				index += 3;
				sizeByteArray += 24;
			}
		}
	}

	/* write array of the second vector of anisotropy */
	writeRow(fid, "<DataArray type=\"Float64\" Name=\"ea2\" NumberOfComponents=\"3\" format=\"binary\">\n");
	byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables */
	sizeBase64Array = 3 * magnSitesNum * sizeof(double);
	unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save the third vector of anisotropy */
	doubleArray = (double *) (unsignedIntArray + 1);
	indexLattice = 0;
	index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t) IDX(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t) IDXtr(i1, i2);
				}
				doubleArray[index] = latticeParam[indexLattice].e31;
				doubleArray[index + 1] = latticeParam[indexLattice].e32;
				doubleArray[index + 2] = latticeParam[indexLattice].e33;
				index += 3;
				sizeByteArray += 24;
			}
		}
	}

	/* write array of the third vector of anisotropy */
	writeRow(fid, "<DataArray type=\"Float64\" Name=\"ea3\" NumberOfComponents=\"3\" format=\"binary\">\n");
	byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables */
	sizeBase64Array = 3 * magnSitesNum * sizeof(double);
	unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save the first vector of TNB-basis */
	doubleArray = (double *) (unsignedIntArray + 1);
	indexLattice = 0;
	index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t) IDX(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t) IDXtr(i1, i2);
				}
				doubleArray[index] = latticeParam[indexLattice].tx;
				doubleArray[index + 1] = latticeParam[indexLattice].ty;
				doubleArray[index + 2] = latticeParam[indexLattice].tz;
				index += 3;
				sizeByteArray += 24;
			}
		}
	}

	/* write array of the first vector of TNB-basis */
	writeRow(fid, "<DataArray type=\"Float64\" Name=\"e1\" NumberOfComponents=\"3\" format=\"binary\">\n");
	byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables */
	sizeBase64Array = 3 * magnSitesNum * sizeof(double);
	unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save the second vector of TNB-basis */
	doubleArray = (double *) (unsignedIntArray + 1);
	indexLattice = 0;
	index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t) IDX(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t) IDXtr(i1, i2);
				}
				doubleArray[index] = latticeParam[indexLattice].nx;
				doubleArray[index + 1] = latticeParam[indexLattice].ny;
				doubleArray[index + 2] = latticeParam[indexLattice].nz;
				index += 3;
				sizeByteArray += 24;
			}
		}
	}

	/* write array of the second vector of anisotropy */
	writeRow(fid, "<DataArray type=\"Float64\" Name=\"e2\" NumberOfComponents=\"3\" format=\"binary\">\n");
	byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables */
	sizeBase64Array = 3 * magnSitesNum * sizeof(double);
	unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save the third vector of TNB-basis */
	doubleArray = (double *) (unsignedIntArray + 1);
	indexLattice = 0;
	index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t) IDX(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t) IDXtr(i1, i2);
				}
				doubleArray[index] = latticeParam[indexLattice].bx;
				doubleArray[index + 1] = latticeParam[indexLattice].by;
				doubleArray[index + 2] = latticeParam[indexLattice].bz;
				index += 3;
				sizeByteArray += 24;
			}
		}
	}

	/* write array of the third vector of TNB-basis */
	writeRow(fid, "<DataArray type=\"Float64\" Name=\"e3\" NumberOfComponents=\"3\" format=\"binary\">\n");
	byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables */
	sizeBase64Array = 3 * magnSitesNum * sizeof(double);
	unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save the vector of external field */
	doubleArray = (double *) (unsignedIntArray + 1);
	curIndex = 0;
	index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t) IDXmg(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t) IDXtrmg(i1, i2);
				}
				doubleArray[index] = Bamp[curIndex];
				doubleArray[index + 1] = Bamp[curIndex + 1];
				doubleArray[index + 2] = Bamp[curIndex + 2];
				index += 3;
				sizeByteArray += 24;
			}
		}
	}

	/* write array of the external field */
	writeRow(fid, "<DataArray type=\"Float64\" Name=\"B\" NumberOfComponents=\"3\" format=\"binary\">\n");
	byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables */
	sizeBase64Array = 3 * magnSitesNum * sizeof(double);
	unsignedIntArray = (unsigned int *) (addDoubleVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save array of the anisotropy coefficients  */
	doubleArray = (double *) (unsignedIntArray + 1);
	indexLattice = 0;
	index = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t) IDX(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t) IDXtr(i1, i2);
				}
				doubleArray[index] = latticeParam[indexLattice].aniK1;
				doubleArray[index + 1] = latticeParam[indexLattice].aniK2;
				doubleArray[index + 2] = latticeParam[indexLattice].aniK3;
				index += 3;
				sizeByteArray += 24;
			}
		}
	}

	/* write array of the anisotropy coefficients */
	writeRow(fid, "<DataArray type=\"Float64\" Name=\"K_vect\" NumberOfComponents=\"3\" format=\"binary\">\n");
	byteArray = (unsigned char *) addDoubleVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables (integer array) */
	sizeBase64Array = magnSitesNum * sizeof(int);
	unsignedIntArray = (unsigned int *) (addIntVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save sublattice identifier */
	int * intArray = (int *) (unsignedIntArray + 1);
	indexLattice = 0;
	index = 0;
	int subl = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					subl = ((i1 & 0x1) ^ (i2 & 0x1) ) ^ (i3 & 0x1);
				}
				intArray[index] = subl;
				index += 1;
				sizeByteArray += 4;
			}
		}
	}

	/* write data about sublattice identifier */
	writeRow(fid, "<DataArray type=\"Int32\" Name=\"sublattice\" format=\"binary\">\n");
	byteArray = (unsigned char *) addIntVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* intermediate variables (integer array) */
	sizeBase64Array = magnSitesNum * sizeof(int);
	unsignedIntArray = (unsigned int *) (addIntVectorXML);
	unsignedIntArray[0] = sizeBase64Array;
	sizeByteArray = 4;

	/* save sublattice_ext identifier */
    intArray = (int *) (unsignedIntArray + 1);
	indexLattice = 0;
	index = 0;
	int sublext = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					sublext = (i1 & 0x1) + 2*(i2 & 0x1) + 4*(i3 & 0x1);
				}
				intArray[index] = sublext;
				index += 1;
				sizeByteArray += 4;
			}
		}
	}

	/* write data about sublattice_ext identifier */
	writeRow(fid, "<DataArray type=\"Int32\" Name=\"sublattice_ext\" format=\"binary\">\n");
	byteArray = (unsigned char *) addIntVectorXML;
	writeBase64Array(fid, byteArray, sizeByteArray);
	writeRow(fid, "\n");
	writeRow(fid, "</DataArray>\n");

	/* write data about exchange interaction */
	if (userVars.exchType == EXCH_INHOMOGENEOUS) {
		writeExchangeXMLBase64(fid);
	}

	/* write final row */
	writeRow(fid, "</PointData>\n");

}

/* function write end rows */
void writeEndRowsXMLBase64(FILE *fid) {

	/* write end rows */
	writeRow(fid, "</Piece>\n");
	writeRow(fid, "</UnstructuredGrid>\n");
	writeRow(fid, "</VTKFile>\n");

}

void writeVTK_xmlbase64(FILE * fid, double t, double MaxDmDt, double MaxDrDt) {

	/* write headers */
	writeHeadersXMLBase64(fid);
	/* write coordinates */
	writeCoordinatesBinaryXMLBase64(fid);
	/* write information about cells */
	writeDataCellsXMLBase64(fid);
	/* write field data */
	writeFieldDataXMLBase64(fid);
	/* write end rows */
	writeEndRowsXMLBase64(fid);

}

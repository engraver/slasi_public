########################################################################
#
# Part of SLaSi project
#
########################################################################

target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/writing.c)
target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/slsbwriter.c)
target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/ucdwriter.c)
target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/vtkwriter_binary.c)
target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/vtkwriter_xmlbase64.c)
target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/vtkwriter_shared.c)

target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/writing.c)
target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/slsbwriter.c)
target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/ucdwriter.c)
target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/vtkwriter_binary.c)
target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/vtkwriter_xmlbase64.c)
target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/vtkwriter_shared.c)
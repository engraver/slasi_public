#ifndef _WRITING_H_
#define _WRITING_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif
#include "../alltypes.h"
#include "../integration/integration.h"

/**
 * @brief This function writes results of simulation or optimization for one frame
 * @param t variable, time of frame to write results
 * @param flog_ON variable, which means whether to record results into log file (it is necessary for tests)
 * @param ind is indicator to write in both files or not
 * @return 0 in the case of absence of errors
 */
int WriteFrame(double t, bool flog_ON, int ind);

#endif

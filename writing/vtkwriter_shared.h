#ifndef _VTK_WRITER_SHARED_H_
#define _VTK_WRITER_SHARED_H_

/**
 * @brief writeRow write one row string row into vtk file
 * @param fid `FILE` identifier.
 * @param row is string to write into file
 */
void writeRow(FILE * fid, char * row);

#endif

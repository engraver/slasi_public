#ifndef _SLSB_WRITER_H_
#define _SLSB_WRITER_H_

/**
 * @brief writeSLSB, saves content of SLSB file (binary)
 * @param fid `FILE` identifier. Should be opened and closed by the calling method.
 * @param t Time in ps
 */
void writeSLSB(FILE * fid, double t);

#endif

#ifndef _VTK_WRITER_BINARY_H_
#define _VTK_WRITER_BINARY_H_

/**
 * @brief writeVTK_binary, saves content of VTK format (binary)
 * @param fid `FILE` identifier. Should be opened and closed by the calling method.
 * @param t Time in ps
 * @param MaxDmDt :math:`\mathrm{max}\, \left| \frac{d\vec{m}}{dt} \right|`
 * @param MaxDrDt :math:`\mathrm{max}\, \left| \frac{d\vec{r}}{dt} \right|`
 */
void writeVTK_binary(FILE * fid, double t, double MaxDmDt, double MaxDrDt);

#endif

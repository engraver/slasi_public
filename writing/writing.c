#include <stdio.h>
#include <string.h>
#include <nlopt.h>
#include <float.h>

#include "writing.h"
#include "slsbwriter.h"
#include "ucdwriter.h"
#include "vtkwriter_binary.h"
#include "vtkwriter_xmlbase64.h"

void WriteEnergy(double t, bool flog_ON) {

	/* first start indicator to write header for logfile of energy */
	static bool firstLaunchEnergy = true;

	if ((flogEnergy) && (firstLaunchEnergy) && (flog_ON)/* && (userVars.restartOnOff == RESTART_OFF)*/) {
		/* write column indexes in logfile of energy */
		fprintf(flogEnergy,
		        "#             1%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s\n",
		        "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16");
		/* write headers of physical parameters in logfile of energy */
		fprintf(flogEnergy, "#          Step%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s\n",
		        "Time", "E(tot)", "E(exch)", "E(aniK1)", "E(aniK2)", "E(aniK3)", "E(dip)", "E(ext)", "E(dmi1)", "E(dmi2)",
		        "E(dmi3)", "EdmiTrBloch", "EdmiTrNeel", "Estr", "Ebend");
		fprintf(flogEnergy, "#           <1>%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s%26s\n",
		        userVars.u_time, userVars.u_energy, userVars.u_energy, userVars.u_energy, userVars.u_energy,
		        userVars.u_energy, userVars.u_energy, userVars.u_energy, userVars.u_energy, userVars.u_energy,
		        userVars.u_energy, userVars.u_energy, userVars.u_energy, userVars.u_energy, userVars.u_energy
		);
		fflush(flogEnergy);
		firstLaunchEnergy = false;
	}

	/* normalization of energy (with and without exchange interaction) */
	if (userVars.exchOnOff == EXCH_ON) {
		EnergyOfSystem = EnergyOfSystemNorm * userVars.NormCoefficientEnergy;
		EnergyExchange = EnergyExchangeNorm * userVars.NormCoefficientEnergy;
		EnergyAnisotropyAxisK1 = EnergyAnisotropyNormAxisK1 * userVars.NormCoefficientEnergy;
		EnergyAnisotropyAxisK2 = EnergyAnisotropyNormAxisK2 * userVars.NormCoefficientEnergy;
		EnergyAnisotropyAxisK3 = EnergyAnisotropyNormAxisK3 * userVars.NormCoefficientEnergy;
		EnergyDipole = EnergyDipoleNorm * userVars.NormCoefficientEnergy;
		EnergyExternal = EnergyExternalNorm * userVars.NormCoefficientEnergy;
		EnergyDMIAxis1 = EnergyDMIAxis1Norm * userVars.NormCoefficientEnergy;
		EnergyDMIAxis2 = EnergyDMIAxis2Norm * userVars.NormCoefficientEnergy;
		EnergyDMIAxis3 = EnergyDMIAxis3Norm * userVars.NormCoefficientEnergy;
		EnergyDMITriangleBulk = EnergyDMITriangleBulkNorm * userVars.NormCoefficientEnergy;
		EnergyDMITriangleInterface = EnergyDMITriangleInterfaceNorm * userVars.NormCoefficientEnergy;
		EnergyStretching = EnergyStretchingNorm * userVars.NormCoefficientEnergy;
		EnergyBending = EnergyBendingNorm * userVars.NormCoefficientEnergy;
	} else {
		EnergyOfSystem = EnergyOfSystemNorm * userVars.NormCoefficientEnergy * 1e15;
		EnergyExchange = EnergyExchangeNorm * userVars.NormCoefficientEnergy * 1e15;
		EnergyAnisotropyAxisK1 = EnergyAnisotropyNormAxisK1 * userVars.NormCoefficientEnergy * 1e15;
		EnergyAnisotropyAxisK2 = EnergyAnisotropyNormAxisK2 * userVars.NormCoefficientEnergy * 1e15;
		EnergyAnisotropyAxisK3 = EnergyAnisotropyNormAxisK3 * userVars.NormCoefficientEnergy * 1e15;
		EnergyDipole = EnergyDipoleNorm * userVars.NormCoefficientEnergy * 1e15;
		EnergyExternal = EnergyExternalNorm * userVars.NormCoefficientEnergy * 1e15;
		EnergyDMIAxis1 = EnergyDMIAxis1Norm * userVars.NormCoefficientEnergy * 1e15;
		EnergyDMIAxis2 = EnergyDMIAxis2Norm * userVars.NormCoefficientEnergy * 1e15;
		EnergyDMIAxis3 = EnergyDMIAxis3Norm * userVars.NormCoefficientEnergy * 1e15;
		EnergyDMITriangleBulk = EnergyDMITriangleBulkNorm * userVars.NormCoefficientEnergy * 1e15;
		EnergyDMITriangleInterface = EnergyDMITriangleInterfaceNorm * userVars.NormCoefficientEnergy * 1e15;
		EnergyStretching = EnergyStretchingNorm * userVars.NormCoefficientEnergy * 1e15;
		EnergyBending = EnergyBendingNorm * userVars.NormCoefficientEnergy * 1e15;
	}

	/* write energetic parameters of system in logfile */
	if (flog_ON) {
		fprintf(flogEnergy, "%15llu ", frameNumber);
		fprintf(flogEnergy, "%25.17e ", t);
		fprintf(flogEnergy, "%25.17e ", EnergyOfSystem);
		fprintf(flogEnergy, "%25.17e ", EnergyExchange);
		fprintf(flogEnergy, "%25.17e ", EnergyAnisotropyAxisK1);
		fprintf(flogEnergy, "%25.17e ", EnergyAnisotropyAxisK2);
		fprintf(flogEnergy, "%25.17e ", EnergyAnisotropyAxisK3);
		fprintf(flogEnergy, "%25.17e ", EnergyDipole);
		fprintf(flogEnergy, "%25.17e ", EnergyExternal);
		fprintf(flogEnergy, "%25.17e ", EnergyDMIAxis1);
		fprintf(flogEnergy, "%25.17e ", EnergyDMIAxis2);
		fprintf(flogEnergy, "%25.17e ", EnergyDMIAxis3);
		fprintf(flogEnergy, "%25.17e ", EnergyDMITriangleBulk);
		fprintf(flogEnergy, "%25.17e ", EnergyDMITriangleInterface);
		fprintf(flogEnergy, "%25.17e ", EnergyStretching);
		fprintf(flogEnergy, "%25.17e ", EnergyBending);
		fprintf(flogEnergy, "\n");
		fflush(flogEnergy);
	}

}

void WriteMag(double t, bool flog_ON) {

	/* first start indicator to write header for logfile of magnetic moments */
	static bool firstLaunchMag = true;

	if ((flogMag) && (firstLaunchMag) && (flog_ON)/* && (userVars.restartOnOff == RESTART_OFF)*/) {
		/* write column indexes in logfile of magnetic moments */
		fprintf(flogMag,
		        "#             1               2%34s%34s%34s\n",
		        "3", "4", "5");
		/* write headers of physical parameters in logfile of magnetic moments */
		fprintf(flogMag,
		        "#          Step            Time%34s%34s%34s\n",
		        "mx(tot)", "my(tot)", "mz(tot)");
		/* write column indexes in logfile of magnetic moments */
		fflush(flogMag);
		firstLaunchMag = false;
	}


	/* write magnetic parameters of system in logfile */
	if (flog_ON) {
		fprintf(flogMag, "%15llu ", frameNumber);
		fprintf(flogMag, "%15le ", t);
		fprintf(flogMag, "%33.25e ", mxTot);
		fprintf(flogMag, "%33.25e ", myTot);
		fprintf(flogMag, "%33.25e ", mzTot);
		fprintf(flogMag, "\n");
		fflush(flogMag);
	}

}

void WriteOther(double t, bool flog_ON) {

	/* first start indicator to write header for logfile of magnetic moments */
	static bool firstLaunchOther = true;

	if ((flogOther) && (firstLaunchOther) && (flog_ON)/* && (userVars.restartOnOff == RESTART_OFF)*/) {
		/* write column indexes in logfile of other fields */
		fprintf(flogOther,
		        "#             1               2%26s%26s%26s%26s%26s%26s%26s%26s\n",
		        "3", "4", "5", "6", "7", "8", "9", "10");
		/* write headers of physical parameters in logfile of other fields */
		fprintf(flogOther,
		        "#          Step            Time%26s%26s%26s%26s%26s%26s%26s%26s\n",
		        "MaxDmDt", "MaxDrDt", "Bx", "By", "Bz", "error", "step", "timeStep");
		/* write column indexes in logfile of other fields */
		fflush(flogOther);
		firstLaunchOther = false;
	}

	/* write other parameters of system in logfile */
	if (flog_ON) {
		fprintf(flogOther, "%15llu ", frameNumber);
		fprintf(flogOther, "%15le ", t);
		fprintf(flogOther, "%25.17e ", MaxDmDt);
		fprintf(flogOther, "%25.17e ", MaxDrDt);
		fprintf(flogOther, "%25.17e ", userVars.Bx);
		fprintf(flogOther, "%25.17e ", userVars.By);
		fprintf(flogOther, "%25.17e ", userVars.Bz);
		fprintf(flogOther, "%25.17e ", errorStep);
		fprintf(flogOther, "%25.17e ", intStep);
		fprintf(flogOther, "%25.17e ", timeSnap);
		fprintf(flogOther, "\n");
		fflush(flogOther);
	}

}

int createFrameSLSB(char * fullPath, char * filenameNumber, double t) {

	/* object to read file */
	FILE *fid;
	/* code of error */
	int tester = 0;

	/*  write snapshot using slsb format */
	sprintf(fullPath, "%s%s.%s.slsb", userVars.pathToWrite, userVars.projName, filenameNumber);
	fid = fopen(fullPath, "wb");
	if (fid) {
		writeSLSB(fid, t);
		fclose(fid);
	} else {
		/* write error */
		tester = 1;
		fprintf(stderr, "(file %s | line %d) Wrong name of file for writing results!\n", __FILE__, __LINE__);
	}

	/* return code of error */
	return tester;

}

int createFrameAVS_ASCII(char * fullPath, char * filenameNumber, double t) {

	/* object to read file */
	FILE *fid;
	/* code of error */
	int tester = 0;

	/*  write snapshot using ucd format */
	sprintf(fullPath, "%s%s.%s.inp", userVars.pathToWrite, userVars.projName, filenameNumber);
	fid = fopen(fullPath, "w");
	if (fid) {
		writeAVS_ASCII(fid, t, MaxDmDt, MaxDrDt);
		fclose(fid);
	} else {
		/* write error */
		tester = 1;
		fprintf(stderr, "(file %s | line %d) Wrong name of file for writing results!\n", __FILE__, __LINE__);
	}

	/* return code of error */
	return tester;

}

int createFrameVTK_binary(char * fullPath, char * filenameNumber, double t) {

	/* object to read file */
	FILE *fid;
	/* code of error */
	int tester = 0;

	/*  write snapshot using vtk binary format */
	sprintf(fullPath, "%s%s.%s.vtk", userVars.pathToWrite, userVars.projName, filenameNumber);
	fid = fopen(fullPath, "w");
	if (fid) {
		writeVTK_binary(fid, t, MaxDmDt, MaxDrDt);
		fclose(fid);
	} else {
		/* write error */
		tester = 1;
		fprintf(stderr, "(file %s | line %d) Wrong name of file for writing results!\n", __FILE__, __LINE__);
	}

	/* return code of error */
	return tester;

}

int createFrameVTK_xmlbase64(char * fullPath, char * filenameNumber, double t) {

	/* object to read file */
	FILE *fid;
	/* code of error */
	int tester = 0;

	/*  write snapshot using vtk xml base64 format */
	sprintf(fullPath, "%s%s.%s.vtu", userVars.pathToWrite, userVars.projName, filenameNumber);
	fid = fopen(fullPath, "w");
	if (fid) {
		writeVTK_xmlbase64(fid, t, MaxDmDt, MaxDrDt);
		fclose(fid);
	} else {
		/* write error */
		tester = 1;
		fprintf(stderr, "(file %s | line %d) Wrong name of file for writing results!\n", __FILE__, __LINE__);
	}

	/* return code of error */
	return tester;

}

int WriteFrame(double t, bool flog_ON, int ind) {

	/* object to read file */
	FILE *fid;
	/* code of error */
	int tester = 0;

	/* get path to write and name of project using alltypes.h for test file */
	char fullPath[strlen(userVars.pathToWrite) + strlen(userVars.projName) + 100];
	memset(fullPath, 0, sizeof(fullPath));

	/* find path to output file, knowing writing path and name of project for test file */
	char filenameNumber[100];
	if (frameNumber < 10) {
		sprintf(filenameNumber, "0000%llu", frameNumber);
	} else if (frameNumber < 100) {
		sprintf(filenameNumber, "000%llu", frameNumber);
	} else if (frameNumber < 1000) {
		sprintf(filenameNumber, "00%llu", frameNumber);
	} else if (frameNumber < 10000) {
		sprintf(filenameNumber, "0%llu", frameNumber);
	} else {
		sprintf(filenameNumber, "%llu", frameNumber);
	}

	/* make correction for exchange interaction and general energy for increasing readability */
	EnergyOfSystemNorm += EnergyCorrectionNorm;
	EnergyExchangeNorm += EnergyCorrectionNorm;

	/* translate physical quantities from normalized into real */
	/* time */
	t /= userVars.NormCoefficientTime;
	/* integration step */
	intStep /= userVars.NormCoefficientTime;
	integrationStepMax /= userVars.NormCoefficientTime;
	integrationStepMin /= userVars.NormCoefficientTime;
	/* changes of mangetic moments */
	MaxDmDt *= userVars.NormCoefficientTime;
	/* velocity */
	MaxDrDt *= userVars.NormCoefficientTime * userVars.a;

	// to debug code
	//fprintf(stderr, "%f\n", NormCoefficientTime);
	//fprintf(stderr, "%f\n", userVars.a);

	/* write results into different files for logging */
	WriteEnergy(t, flog_ON);
	WriteMag(t, flog_ON);
	WriteOther(t, flog_ON);

	/* add one to number of frames */
	frameNumber++;
	/* if writing in .sls files is not necessary */
	if (userVars.saveSLS == SLSSAVE_OFF)
		return 0;

	/* calculate number of components for magnetic moment */
	spinsNum = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* check whether it is essential to write */
	if (ind == 1) {
		/* create SLSB frame */
		tester = createFrameSLSB(fullPath, filenameNumber, t);
	}
	if ((userVars.snapshotPeriodMultiplier > 0) && ((frameNumber - 1) % userVars.snapshotPeriodMultiplier == 0)) {
		/* create SLSB frame */
		tester = createFrameSLSB(fullPath, filenameNumber, t);
	}

	/* check whether it is essential to write */
	if (ind == 1) {
		if (userVars.outputFileType == UCD_ASCII) {
			/* create AVS_ASCII frame */
			tester = createFrameAVS_ASCII(fullPath, filenameNumber, t);
		} else if (userVars.outputFileType == VTK_BINARY) {
			/* create VTK_BINARY frame */
			tester = createFrameVTK_binary(fullPath, filenameNumber, t);
		} else if (userVars.outputFileType == VTK_XMLBASE64) {
			/* create VTK_XMLBASE64 frame */
			tester = createFrameVTK_xmlbase64(fullPath, filenameNumber, t);
		}
	}
	if ((userVars.backupPeriod > 0) && ((frameNumber - 1) % userVars.backupPeriod == 0)) {
		if (userVars.outputFileType == UCD_ASCII) {
			/* create AVS_ASCII frame */
			tester = createFrameAVS_ASCII(fullPath, filenameNumber, t);
		} else if (userVars.outputFileType == VTK_BINARY) {
			/* create VTK_BINARY frame */
			tester = createFrameVTK_binary(fullPath, filenameNumber, t);
		} else if (userVars.outputFileType == VTK_XMLBASE64) {
			/* create VTK_XMLBASE64 frame */
			tester = createFrameVTK_xmlbase64(fullPath, filenameNumber, t);
		}
	}

	/* write restart file */
	sprintf(fullPath, "%s%s.restart.slsb", userVars.pathToWrite, userVars.projName);
	fid = fopen(fullPath, "wb");
	if (fid) {
		writeSLSB(fid, t);
		fclose(fid);
	} else {
		/* write error */
		tester = 1;
		fprintf(stderr, "(file %s | line %d) Wrong name of file for writing results!\n", __FILE__, __LINE__);
	}

	/* return value of error */
	return tester;

}

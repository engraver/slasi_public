#include <stdlib.h>
#include <memory.h>
#include "../../alltypes.h"
#include "ucdwriter.h"
#include "../lattice/index_nums.h"

/*
 * AVS UCD description
 * https://dav.lbl.gov/archive/NERSC/Software/express/help6.1/help/reference/dvmac/UCD_Form.htm
 *
 * VTK description
 * http://www.cs.utah.edu/~ssingla/Research/file-formats.pdf
 */

void writeAVS_ASCII(FILE * fid, double t, double MaxDmDt, double MaxDrDt) {

	/* write general parameters */
	int fields_num = 3 + 9 + 3 + 9 + 1 + 1; /* magnetization + 3 axes of anisotropy + 3 coefficients per site + 3 axes of local basis + 2 scalar field for parity of lattice site */
	if (userVars.latticeType == LATTICE_CUBIC && userVars.exchType == EXCH_INHOMOGENEOUS) {
		fields_num += 6; /*  + 6 exchange coefficients per site (for cubic lattice and inhomogeneous exchange only) */
	}
	fprintf(fid, "%d %d %d 0 0\n", magnSitesNum, connElements, fields_num); 

	/* coordinates */
	size_t indexLattice = 0;
	size_t curIndex = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t) IDXmg(i1, i2, i3) + spinsNum;
					indexLattice = (size_t) IDX(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2) + spinsNum;
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				if (latticeParam[indexLattice].magnetic) {
					fprintf(fid, "%d %lg %lg %lg\n", nodeIndices[indexLattice], userVars.a * lattice[curIndex], userVars.a * lattice[curIndex + 1], userVars.a * lattice[curIndex + 2]);
				}
			}
		}
	}

	/* cells */
	if (userVars.latticeType == LATTICE_CUBIC) {
		if ((userVars.N2 == 3) && (userVars.N3 == 3)) {
			/* spin chain */
			for (int i = 0; i < connElements; i ++) {
				fprintf(fid, "%d 0 line %d %d\n", i + 1, connectivity[i][CONN_FIRST_NGBR], connectivity[i][CONN_SECOND_NGBR]);
			}
		} else if (userVars.N3 == 3) {
			/* square lattice */
			for (int i = 0; i < connElements; i ++) {
				fprintf(fid, "%d 0 quad %d %d %d %d\n", i + 1, connectivity[i][CONN_FIRST_NGBR], connectivity[i][CONN_SECOND_NGBR], connectivity[i][CONN_THIRD_NGBR], connectivity[i][CONN_FOURTH_NGBR]);
			}
		} else {
			/* bulk */
			for (int i = 0; i < connElements; i ++) {
				fprintf(fid, "%d 0 hex %d %d %d %d %d %d %d %d\n", i + 1, connectivity[i][CONN_FIRST_NGBR], connectivity[i][CONN_SECOND_NGBR], connectivity[i][CONN_THIRD_NGBR], connectivity[i][CONN_FOURTH_NGBR], connectivity[i][CONN_FIFTH_NGBR], connectivity[i][CONN_SIXTH_NGBR], connectivity[i][CONN_SEVENTH_NGBR], connectivity[i][CONN_EIGHTH_NGBR]);}
		}
	} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
		for (int i = 0; i < connElements; i ++) {
			fprintf(fid, "%d 0 tri %d %d %d\n", i + 1, connectivity[i][CONN_FIRST_NGBR], connectivity[i][CONN_SECOND_NGBR], connectivity[i][CONN_THIRD_NGBR]);
		}
	}

    if (userVars.exchType == EXCH_INHOMOGENEOUS) {
		fprintf(fid, "19 3 3 3 3 1 1 1 1 1 1 1 1 1 3 3 3 3 1 1\n");
	}
	else {
		fprintf(fid, "13 3 3 3 3 1 1 1 3 3 3 3 1 1\n");
	}
	fprintf(fid, "m, none\n");
	fprintf(fid, "ea1, none\n");
	fprintf(fid, "ea2, none\n");
	fprintf(fid, "ea3, none\n");
    fprintf(fid, "aniK1, pJ\n");
    fprintf(fid, "aniK2, pJ\n");
    fprintf(fid, "aniK3, pJ\n");
    if (userVars.latticeType == LATTICE_CUBIC && userVars.exchType == EXCH_INHOMOGENEOUS) {
		fprintf(fid, "exchJp1, pJ\n");
		fprintf(fid, "exchJm1, pJ\n");
		fprintf(fid, "exchJp2, pJ\n");
		fprintf(fid, "exchJm2, pJ\n");
		fprintf(fid, "exchJp3, pJ\n");
		fprintf(fid, "exchJm3, pJ\n");
	} else if (userVars.latticeType == LATTICE_TRIANGULAR && userVars.exchType == EXCH_INHOMOGENEOUS) {
	    fprintf(fid, "exchJ1, pJ\n");
	    fprintf(fid, "exchJ2, pJ\n");
	    fprintf(fid, "exchJ3, pJ\n");
	    fprintf(fid, "exchJ4, pJ\n");
	    fprintf(fid, "exchJ5, pJ\n");
	    fprintf(fid, "exchJ6, pJ\n");
    }
	fprintf(fid, "e1, none\n");
	fprintf(fid, "e2, none\n");
	fprintf(fid, "e3, none\n");
	fprintf(fid, "B, T\n");
	fprintf(fid, "sublattice, none\n");
	fprintf(fid, "sublattice_ext, none\n");
	int subl = 0, sublext = 0; /* sublattice number */
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
					subl = ((i1 & 0x1) ^ (i2 & 0x1) ) ^ (i3 & 0x1);
					sublext = (i1 & 0x1) + 2*(i2 & 0x1) + 4*(i3 & 0x1);
				}
				else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				if (latticeParam[indexLattice].magnetic) {
					if (userVars.latticeType == LATTICE_CUBIC && userVars.exchType == EXCH_INHOMOGENEOUS) {
						fprintf(fid, "%d %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %d %d\n"
								, nodeIndices[indexLattice]
								, lattice[curIndex]
								, lattice[curIndex + 1]
								, lattice[curIndex + 2]
								, latticeParam[indexLattice].e11
								, latticeParam[indexLattice].e12
								, latticeParam[indexLattice].e13
								, latticeParam[indexLattice].e21
								, latticeParam[indexLattice].e22
								, latticeParam[indexLattice].e23
								, latticeParam[indexLattice].e31
								, latticeParam[indexLattice].e32
								, latticeParam[indexLattice].e33
								, latticeParam[indexLattice].aniK1 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].aniK2 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].aniK3 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].Jp1 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].Jm1 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].Jp2 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].Jm2 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].Jp3 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].Jm3 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].tx
								, latticeParam[indexLattice].ty
								, latticeParam[indexLattice].tz
								, latticeParam[indexLattice].nx
								, latticeParam[indexLattice].ny
								, latticeParam[indexLattice].nz
								, latticeParam[indexLattice].bx
								, latticeParam[indexLattice].by
								, latticeParam[indexLattice].bz
								, Bamp[curIndex]
								, Bamp[curIndex + 1]
								, Bamp[curIndex + 2]
								, subl
								, sublext
						);
					} else if (userVars.latticeType == LATTICE_TRIANGULAR && userVars.exchType == EXCH_INHOMOGENEOUS) {
						fprintf(fid, "%d %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %d %d\n"
								, nodeIndices[indexLattice]
								, lattice[curIndex]
								, lattice[curIndex + 1]
								, lattice[curIndex + 2]
								, latticeParam[indexLattice].e11
								, latticeParam[indexLattice].e12
								, latticeParam[indexLattice].e13
								, latticeParam[indexLattice].e21
								, latticeParam[indexLattice].e22
								, latticeParam[indexLattice].e23
								, latticeParam[indexLattice].e31
								, latticeParam[indexLattice].e32
								, latticeParam[indexLattice].e33
								, latticeParam[indexLattice].aniK1 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].aniK2 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].aniK3 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].J1 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].J2 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].J3 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].J4 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].J5 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].J6 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].tx
								, latticeParam[indexLattice].ty
								, latticeParam[indexLattice].tz
								, latticeParam[indexLattice].nx
								, latticeParam[indexLattice].ny
								, latticeParam[indexLattice].nz
								, latticeParam[indexLattice].bx
								, latticeParam[indexLattice].by
								, latticeParam[indexLattice].bz
								, Bamp[curIndex]
								, Bamp[curIndex + 1]
								, Bamp[curIndex + 2]
								, subl
								, sublext
						);
					} else {
						fprintf(fid, "%d %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %d %d\n"
								, nodeIndices[indexLattice]
								, lattice[curIndex]
								, lattice[curIndex + 1]
								, lattice[curIndex + 2]
								, latticeParam[indexLattice].e11
								, latticeParam[indexLattice].e12
								, latticeParam[indexLattice].e13
								, latticeParam[indexLattice].e21
								, latticeParam[indexLattice].e22
								, latticeParam[indexLattice].e23
								, latticeParam[indexLattice].e31
								, latticeParam[indexLattice].e32
								, latticeParam[indexLattice].e33
								, latticeParam[indexLattice].aniK1 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].aniK2 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].aniK3 * userVars.NormCoefficientEnergy / (userVars.S * userVars.S)
								, latticeParam[indexLattice].tx
								, latticeParam[indexLattice].ty
								, latticeParam[indexLattice].tz
								, latticeParam[indexLattice].nx
								, latticeParam[indexLattice].ny
								, latticeParam[indexLattice].nz
								, latticeParam[indexLattice].bx
								, latticeParam[indexLattice].by
								, latticeParam[indexLattice].bz
								, Bamp[curIndex]
								, Bamp[curIndex + 1]
								, Bamp[curIndex + 2]
								, subl
								, sublext
						);
					}
				}
			}
		}
	}

}
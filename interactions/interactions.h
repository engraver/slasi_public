#ifndef _INTERACTIONS_H_
#define _INTERACTIONS_H_

#include "exchange/uniformexchange.h"
#include "exchange/uniformexchange_triangle.h"
#include "anisotropy/uniaxialAnisotropy.h"
#include "dipole/dipole.h"
#include "externalField/externalField.h"
#include "externalForce/forceSetup.h"
#include "dmi/dmi.h"
#include "stretching/stretching.h"
#include "bending/bending.h"
#include "exchange/inhomexchange.h"

#endif

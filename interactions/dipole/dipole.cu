#include <stdio.h>
#include "../../alltypes.h"
extern "C" {
	#include "dipole.h"
}

/* dipole interaction for one site (GPU) */
__global__
void spinDipoleField(double *d_H, double *d_lattice, double *d_EnergyTotal, LatticeSiteDescr * d_latticeParam, long N1Ext, long N2Ext, long N3Ext, double kDip, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* check boundary condition */
	if ((indexLattice < numberSites) &&
	    (d_lattice[curIndex] * d_lattice[curIndex] + d_lattice[curIndex + 1] * d_lattice[curIndex + 1] + d_lattice[curIndex + 2] * d_lattice[curIndex + 2] > LENG_ZERO)) {

		/* radius vector between two spins and
		common element for calculating various components of dipole interaction */
		double Rx, Ry, Rz, R2, commElement;
		/* offset for coordinates in array "lattice" */
		size_t offset = NUMBER_COMP_MAGN * N1Ext * N2Ext * N3Ext;
		/* intermediate variables for calculation 1/R^3 and 1/R^5 */
		double oneoverR3, oneoverR3m3;
		/* variables to go through all nodes */
		size_t indexLatticeDipole, curIndexDipole;
		/* local effective field */
		double HxLocal = 0.0;
		double HyLocal = 0.0;
		double HzLocal = 0.0;

		/* pass through each magnetic moment */
		for (int i1 = 1; i1 < N1Ext - 1; i1 ++) {
			for (int i2 = 1; i2 < N2Ext - 1; i2 ++) {
				for (int i3 = 1; i3 < N3Ext - 1; i3 ++) {

					/* number of spin in lattice */
					indexLatticeDipole = IDXGPU(i1, i2, i3);
					if ( (d_latticeParam[indexLattice].magnetic) &&
						 (d_latticeParam[indexLatticeDipole].magnetic) &&
					 	 (indexLatticeDipole != indexLattice) ) {

						/* number of spin in array "lattice" */
						curIndexDipole = IDXmgGPU(i1, i2, i3);

						/* get distances */
						Rx = d_lattice[curIndexDipole + offset] - d_lattice[curIndex + offset];
						Ry = d_lattice[curIndexDipole + offset + 1] - d_lattice[curIndex + offset + 1];
						Rz = d_lattice[curIndexDipole + offset + 2] - d_lattice[curIndex + offset + 2];
						R2 = Rx * Rx + Ry * Ry + Rz * Rz;

						/* intermediate values of calculations */
						oneoverR3 = 1.0 / R2 * sqrt(R2);
						oneoverR3m3 = (3.0 * oneoverR3) / R2;

						/* common element for calculations */
						commElement = d_lattice[curIndexDipole]     * Rx +
						              d_lattice[curIndexDipole + 1] * Ry +
						              d_lattice[curIndexDipole + 2] * Rz;

						/* calculation of components of effective field */
						HxLocal += - d_lattice[curIndexDipole] * oneoverR3 +
										(Rx * commElement) * oneoverR3m3;
						HyLocal += - d_lattice[curIndexDipole + 1] * oneoverR3 +
										(Ry * commElement) * oneoverR3m3;
						HzLocal += - d_lattice[curIndexDipole + 2] * oneoverR3 +
										(Rz * commElement) * oneoverR3m3;

					}

				}
			}
		}

		/* multiply by coefficient to get effective field */
		HxLocal *= kDip;
		HyLocal *= kDip;
		HzLocal *= kDip;

		/* calculate energy of dipole interaction for one spin and add to general energy */
		d_EnergyTotal[indexLattice] = -(HxLocal * d_lattice[curIndex]  +
									   HyLocal * d_lattice[curIndex + 1] +
									   HzLocal * d_lattice[curIndex + 2]);

		/* add generated effective field to the total */
		d_H[curIndex] += HxLocal;
		d_H[curIndex + 1] += HyLocal;
		d_H[curIndex + 2] += HzLocal;

	}

}

extern "C"
void dipoleFieldCUDA(double *d_H, double *d_EnergyTotal, long numberSites) {

	/* setup the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* wrapper of CUDA function */
	spinDipoleField<<<cores,threads>>>(d_H, d_lattice, d_EnergyTotal, d_latticeParam, userVars.N1, userVars.N2, userVars.N3, userVars.NormDipole, numberSites);

}

/* cached dipole variables for one site (GPU) */
__global__
void spinCacheDipoleVariables(double ** d_dipole_R3coef, double ** d_dipole_RxR5coef, double ** d_dipole_RyR5coef, double ** d_dipole_RzR5coef, double * d_lattice, int N1Ext, int N2Ext, int N3Ext, long offset, long numberSites) {

	/* find indices of site */
	long indexLattice1 = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice1;

	/* declaration of temporary variables */
	double rx, ry, rz, r2;

	/* check boundary condition */
	if ((indexLattice1 < numberSites) &&
	    (d_lattice[curIndex] * d_lattice[curIndex] + d_lattice[curIndex + 1] * d_lattice[curIndex + 1] + d_lattice[curIndex + 2] * d_lattice[curIndex + 2] > LENG_ZERO)) {

		for (int j1 = 1; j1 < N1Ext - 1; j1++) {
			for (int j2 = 1; j2 < N2Ext - 1; j2++) {
				for (int j3 = 1; j3 < N3Ext - 1; j3++) {

					size_t indexLattice2 = IDXGPU(j1, j2, j3);
					if (indexLattice1 == indexLattice2) {
						d_dipole_R3coef[indexLattice1][indexLattice2] = 0.0;
						d_dipole_RxR5coef[indexLattice1][indexLattice2] = 0.0;
						d_dipole_RyR5coef[indexLattice1][indexLattice2] = 0.0;
						d_dipole_RzR5coef[indexLattice1][indexLattice2] = 0.0;
					} else {
						rx = d_lattice[indexLattice2 + offset] - d_lattice[indexLattice1 + offset];
						ry = d_lattice[indexLattice2 + offset + 1] - d_lattice[indexLattice1 + offset + 1];
						rz = d_lattice[indexLattice2 + offset + 2] - d_lattice[indexLattice1 + offset + 2];
						r2 = rx * rx + ry * ry + rz * rz;
						double tmp = 1.0 / (r2 * sqrt(r2));
						d_dipole_R3coef[indexLattice1][indexLattice2] = tmp;
						d_dipole_RxR5coef[indexLattice1][indexLattice2] = rx * 3.0 * tmp / r2;
						d_dipole_RyR5coef[indexLattice1][indexLattice2] = ry * 3.0 * tmp / r2;
						d_dipole_RzR5coef[indexLattice1][indexLattice2] = rz * 3.0 * tmp / r2;
					}

				}
			}
		}

	}

}

extern "C"
void cacheDipoleVariables(void) {

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;
	long offset = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

    /* setup the execution configuration */
    dim3 cores(numberSites / THREADS_CORE + 1, 1);
    dim3 threads(THREADS_CORE, 1);

	/* cache dipole variables using graphic card */
    spinCacheDipoleVariables<<<cores, threads>>>(d_dipole_R3coef, d_dipole_RxR5coef, d_dipole_RyR5coef, d_dipole_RzR5coefHost, d_lattice, userVars.N1, userVars.N2, userVars.N3, offset, numberSites);

}

/* dipole interaction for one site (GPU) using cached arrays */
__global__
void spinDipoleFieldCached(double *d_H, double *d_lattice, double *d_EnergyTotal, LatticeSiteDescr * d_latticeParam, double ** d_dipole_R3coef, double ** d_dipole_RxR5coef, double ** d_dipole_RyR5coef, double ** d_dipole_RzR5coef, long N1Ext, long N2Ext, long N3Ext, double kDip, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* check boundary condition */
	if ((indexLattice < numberSites) &&
	    (d_lattice[curIndex] * d_lattice[curIndex] + d_lattice[curIndex + 1] * d_lattice[curIndex + 1] + d_lattice[curIndex + 2] * d_lattice[curIndex + 2] > LENG_ZERO)) {

		/* calculate offset for array "lattice" */
		size_t offset = NUMBER_COMP_MAGN * N1Ext * N2Ext * N3Ext;
		/* effective field of dipole interaction */
		double HxLocal = 0.0;
		double HyLocal = 0.0;
		double HzLocal = 0.0;
		/* radius vector between two spins and
		common element for calculating various components of dipole interaction */
		double Rx, Ry, Rz, commElement;
		/* intermediate variables for calculation 1/R^3 and 1/R^5 */
		double oneoverR3, oneoverR3m3x, oneoverR3m3y, oneoverR3m3z;
		/* variables to go through all nodes */
		size_t indexLatticeDipole, curIndexDipole;

		/* pass through each magnetic moment */
		for (int i1 = 1; i1 < N1Ext - 1; i1 ++) {
			for (int i2 = 1; i2 < N2Ext - 1; i2 ++) {
				for (int i3 = 1; i3 < N3Ext - 1; i3 ++) {

					/* number of spin in lattice */
					indexLatticeDipole = IDXGPU(i1, i2, i3);
					if ( (d_latticeParam[indexLattice].magnetic) &&
					     (indexLatticeDipole != indexLattice) ) {

						/* number of spin in array "lattice" */
						curIndexDipole = IDXmgGPU(i1, i2, i3);

						/* get distances */
						Rx = d_lattice[curIndexDipole + offset] - d_lattice[curIndex + offset];
						Ry = d_lattice[curIndexDipole + offset + 1] - d_lattice[curIndex + offset + 1];
						Rz = d_lattice[curIndexDipole + offset + 2] - d_lattice[curIndex + offset + 2];

						/* common element for calculations */
						commElement = d_lattice[curIndexDipole]     * Rx +
						              d_lattice[curIndexDipole + 1] * Ry +
						              d_lattice[curIndexDipole + 2] * Rz;

						//printf("---- %ld %ld\n", curIndex, curIndexDipole);
						oneoverR3    = d_dipole_R3coef[curIndex][curIndexDipole];
						oneoverR3m3x = d_dipole_RxR5coef[curIndex][curIndexDipole];
						oneoverR3m3y = d_dipole_RyR5coef[curIndex][curIndexDipole];
						oneoverR3m3z = d_dipole_RzR5coef[curIndex][curIndexDipole];

						/* calculation of components of effective field */
						HxLocal += - d_lattice[curIndexDipole] * oneoverR3     + commElement * oneoverR3m3x;
						HyLocal += - d_lattice[curIndexDipole + 1] * oneoverR3 + commElement * oneoverR3m3y;
						HzLocal += - d_lattice[curIndexDipole + 2] * oneoverR3 + commElement * oneoverR3m3z;

					}

				}
			}
		}

		/* multiply by coefficient to get effective field */
		HxLocal *= kDip;
		HyLocal *= kDip;
		HzLocal *= kDip;

		/* calculate energy of dipole interaction for one spin and add to general energy */
		d_EnergyTotal[indexLattice] = -(HxLocal * d_lattice[curIndex]  +
		                               HyLocal * d_lattice[curIndex + 1] +
		                               HzLocal * d_lattice[curIndex + 2]);

		/* add generated effective field to the total */
		d_H[curIndex] += HxLocal;
		d_H[curIndex + 1] += HyLocal;
		d_H[curIndex + 2] += HzLocal;

	}

}

extern "C"
void dipoleFieldCachedCUDA(double *d_H, double *d_EnergyTotal, long numberSites) {

	/* setup the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* wrapper of CUDA function */
	spinDipoleFieldCached<<<cores,threads>>>(d_H, d_lattice, d_EnergyTotal, d_latticeParam, dipole_R3coef, d_dipole_RxR5coef, d_dipole_RyR5coef, d_dipole_RzR5coef, userVars.N1, userVars.N2, userVars.N3, userVars.NormDipole, numberSites);

}

#ifndef _DIPOLE_H_
#define _DIPOLE_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../../alltypes.h"
#include "../interactions.h"
#include "../../utils/indices.h"

/**
 * @brief Caches constant values for dipolar interaction
 */
void cacheDipoleVariables(void);

/**
 * @brief This function returns effective field and energy
 * which created by dipole interaction of surrounding spins
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Hx effective field on axis x
 * @param Hy effective field on axis y
 * @param Hz effective field on axis z
 * @param EnergyDipole normalized dipole energy
 */
void dipoleField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyDipole);

/**
 * @brief This function returns effective field and energy
 * which created by dipole interaction of surrounding spins (uses cache for variables)
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Hx effective field on axis x
 * @param Hy effective field on axis y
 * @param Hz effective field on axis z
 * @param EnergyDipole normalized dipole energy
 */
void dipoleFieldCached(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyDipole);


/**
 * @brief This function of wrapper returns effective field and energy
 * which created by dipole interaction of surrounding spins (GPU)
 * @param d_H is array of effective field (GPU)
 * @param d_EnergyTotal is array of dipole energy for sites (GPU)
 * @param numberSites is number of sites
 */
void dipoleFieldCUDA(double *d_H, double *d_EnergyTotal, long numberSites);

/**
 * @brief This function of wrapper returns effective field and energy
 * which created by dipole interaction of surrounding spins using cached arrays (GPU)
 * @param d_H is array of effective field (GPU)
 * @param d_EnergyTotal is array of dipole energy for sites (GPU)
 * @param numberSites is number of sites
 */
void dipoleFieldCachedCUDA(double *d_H, double *d_EnergyTotal, long numberSites);

#endif

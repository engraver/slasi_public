#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "dipole.h"
#include "../../alltypes.h"

void cacheDipoleVariables(void) {

	/* calculate and save distances for array of dipole interaction */
	size_t offset0 = NUMBER_COMP_MAGN * spinsNum;
	double rx, ry, rz, r2;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				size_t ind1 = IDXmg(i1, i2, i3);
				for (int j1 = 1; j1 < userVars.N1 - 1; j1++) {
					for (int j2 = 1; j2 < userVars.N2 - 1; j2++) {
						for (int j3 = 1; j3 < userVars.N3 - 1; j3++) {

							size_t ind2 = IDX(j1, j2, j3);
							if (ind1 == ind2) {
								dipole_R3coef[ind1][ind2]   = 0.0;
								dipole_RxR5coef[ind1][ind2] = 0.0;
								dipole_RyR5coef[ind1][ind2] = 0.0;
								dipole_RzR5coef[ind1][ind2] = 0.0;
							} else {
								rx = lattice[ind2 + offset0] - lattice[ind1 + offset0];
								ry = lattice[ind2 + offset0 + 1] - lattice[ind1 + offset0 + 1];
								rz = lattice[ind2 + offset0 + 2] - lattice[ind1 + offset0 + 2];

								r2 = rx * rx + ry * ry + rz * rz;

								double tmp = 1.0 / (r2 * sqrt(r2));
								dipole_R3coef[ind1][ind2]   = tmp;
								dipole_RxR5coef[ind1][ind2] = rx * 3.0 * tmp / r2;
								dipole_RyR5coef[ind1][ind2] = ry * 3.0 * tmp / r2;
								dipole_RzR5coef[ind1][ind2] = rz * 3.0 * tmp / r2;
							}

						}
					}
				}
			}
		}
	}

}

void dipoleField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyDipole) {
	
	/* number of components of magnetic moments */
	// size_t spinsNum = 3*userVars.N1*userVars.N2*userVars.N3;
	/* effective field of dipole interaction */
	double effectFieldx = 0.0; 
	double effectFieldy = 0.0; 
	double effectFieldz = 0.0;
	/* radius vector between two spins and 
	common element for calculating various components of dipole interaction */
	double Rx, Ry, Rz, R2, commElement;
	/* offset for coordinates in array "lattice" */
	size_t offset = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	/* intermediate variables for calculation 1/R^3 and 1/R^5 */
	double oneoverR3, oneoverR3m3;
	/* variables to go through all nodes */
	size_t indexLatticeDipole, curIndexDipole;
	
	/* pass through each magnetic moment */
	for (int i1 = 1; i1 < userVars.N1-1; i1 ++) {
		for (int i2 = 1; i2 < userVars.N2-1; i2 ++) {
			for (int i3 = 1; i3 < userVars.N3-1; i3 ++) {
				
				/* number of spin in lattice */
				indexLatticeDipole = IDX(i1, i2, i3);
				if ( (latticeParam[indexLattice].magnetic) && 
				     (indexLatticeDipole != indexLattice) ) {
					
					/* number of spin in array "lattice" */   
					curIndexDipole = IDXmg(i1, i2, i3);

					/* get distances */
					Rx = lattice[curIndexDipole + offset] - lattice[curIndex + offset];
					Ry = lattice[curIndexDipole + offset + 1] - lattice[curIndex + offset + 1];
					Rz = lattice[curIndexDipole + offset + 2] - lattice[curIndex + offset + 2];
					R2 = Rx * Rx + Ry * Ry + Rz * Rz;

					/* intermediate values of calculations */
					oneoverR3 = 1.0 / R2 * sqrt(R2);
					oneoverR3m3 = (3.0 * oneoverR3) / R2;

					/* common element for calculations */
					commElement = lattice[curIndexDipole]     * Rx +
					              lattice[curIndexDipole + 1] * Ry +
					              lattice[curIndexDipole + 2] * Rz;
					
					/* calculation of components of effective field */
					effectFieldx += - lattice[curIndexDipole] * oneoverR3 +
					                (Rx * commElement)*oneoverR3m3;
					effectFieldy += - lattice[curIndexDipole + 1] * oneoverR3 +
					                (Ry * commElement)*oneoverR3m3;
					effectFieldz += - lattice[curIndexDipole + 2] * oneoverR3 +
					                (Rz * commElement)*oneoverR3m3;
					
				}
					
			}
		}
	}

	/* multiply by coefficient to get effective field */
	effectFieldx *= userVars.NormDipole;
	effectFieldy *= userVars.NormDipole;
	effectFieldz *= userVars.NormDipole;
	
	/* calculate energy of dipole interaction for one spin and add to general energy */
	latticeParam[indexLattice].EnergyDipoleNorm = -(effectFieldx * lattice[curIndex]     + 
	                                               effectFieldy * lattice[curIndex + 1] +
	                                               effectFieldz * lattice[curIndex + 2]);
	*EnergyDipole += latticeParam[indexLattice].EnergyDipoleNorm;
	
	/* add generated effective field to the total */	
	*Hx += effectFieldx;
	*Hy += effectFieldy;
	*Hz += effectFieldz;

}

void dipoleFieldCached(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyDipole) {

	/* calculate offset for array "lattice" */
	size_t offset0 = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	/* effective field of dipole interaction */
	double effectFieldx = 0.0;
	double effectFieldy = 0.0;
	double effectFieldz = 0.0;
	/* radius vector between two spins and
    common element for calculating various components of dipole interaction */
	double Rx, Ry, Rz, commElement;
	/* intermediate variables for calculation 1/R^3 and 1/R^5 */
	double oneoverR3, oneoverR3m3x, oneoverR3m3y, oneoverR3m3z;
	/* variables to go through all nodes */
	size_t indexLatticeDipole, curIndexDipole;

	/* pass through each magnetic moment */
	for (int i1 = 1; i1 < userVars.N1-1; i1 ++) {
		for (int i2 = 1; i2 < userVars.N2-1; i2 ++) {
			for (int i3 = 1; i3 < userVars.N3-1; i3 ++) {

				/* number of spin in lattice */
				indexLatticeDipole = IDX(i1, i2, i3);
				if ( (latticeParam[indexLattice].magnetic) &&
				     (indexLatticeDipole != indexLattice) ) {

					/* number of spin in array "lattice" */
					curIndexDipole = IDXmg(i1, i2, i3);

					/* get distances */
					Rx = lattice[curIndexDipole + offset0] - lattice[curIndex + offset0];
					Ry = lattice[curIndexDipole + offset0 + 1] - lattice[curIndex + offset0 + 1];
					Rz = lattice[curIndexDipole + offset0 + 2] - lattice[curIndex + offset0 + 2];

					/* common element for calculations */
					commElement = lattice[curIndexDipole]     * Rx +
					              lattice[curIndexDipole + 1] * Ry +
					              lattice[curIndexDipole + 2] * Rz;

					//printf("---- %ld %ld\n", curIndex, curIndexDipole);
					oneoverR3    = dipole_R3coef[curIndex][curIndexDipole]; //1.0/(R2*sqrt(R2));
					oneoverR3m3x = dipole_RxR5coef[curIndex][curIndexDipole]; //(3.0 * oneoverR3)/R2;
					oneoverR3m3y = dipole_RyR5coef[curIndex][curIndexDipole];
					oneoverR3m3z = dipole_RzR5coef[curIndex][curIndexDipole];

					/* calculation of components of effective field */
					effectFieldx += - lattice[curIndexDipole] * oneoverR3     + commElement * oneoverR3m3x;
					effectFieldy += - lattice[curIndexDipole + 1] * oneoverR3 + commElement * oneoverR3m3y;
					effectFieldz += - lattice[curIndexDipole + 2] * oneoverR3 + commElement * oneoverR3m3z;

				}

			}
		}
	}

	/* multiply by coefficient to get effective field */
	effectFieldx *= userVars.NormDipole;
	effectFieldy *= userVars.NormDipole;
	effectFieldz *= userVars.NormDipole;

	/* calculate energy of dipole interaction for one spin and add to general energy */
	latticeParam[indexLattice].EnergyDipoleNorm = -(effectFieldx * lattice[curIndex]     +
	                                               effectFieldy * lattice[curIndex + 1] +
	                                               effectFieldz * lattice[curIndex + 2]);
	*EnergyDipole += latticeParam[indexLattice].EnergyDipoleNorm;

	/* add generated effective field to the total */
	*Hx += effectFieldx;
	*Hy += effectFieldy;
	*Hz += effectFieldz;

}

#ifndef _STRETCHING_H
#define _STRETCHING_H

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../../alltypes.h"
#include "../interactions.h"
#include "../../utils/indices.h"

/**
 * @brief setupStretchingTriangle
 * @param numbers array with values from paramFile
 * @param indlp index for latticeParam array
 * @param indn index for numbers array
 * @return 0 if ok, otherwise nonzero value
 */
int setupStretchingTriangle(double **numbers, int indlp, int indn);

/**
 * @brief This function returns force and energy
 * which created by stretching interaction of one node
 * @param i1 the first index of node
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Fx force of stretching interaction on axis x
 * @param Fy force of stretching interaction on axis y
 * @param Fz force of stretching interaction on axis z
 * @param Estr energy of stretching
 */
void stretchingForceAndEnergy(int i1, int curIndex, int indexLattice, double *Fx, double *Fy, double *Fz, double *Estr);

/**
 * @brief This function returns force and energy
 * which created by stretching interaction (GPU)
 * @param d_F forces of stretching (array)
 * @param d_EnergyTotal energy of stretching (array)
 * @param numberSites number of sites in system
 */
void stretchingForceAndEnergyTriangleCUDA(double *d_F, double *d_EnergyTotal, long numberSites);

/**
 * @brief This function returns force and energy
 * which created by stretching interaction of one node for triangle lattice
 * @param i1 the first index of node in the lattice
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Fx force of stretching interaction on axis x
 * @param Fy force of stretching interaction on axis y
 * @param Fz force of stretching interaction on axis z
 * @param Estr energy of stretching
 */
void stretchingForceAndEnergyTriangle(int i1, int i2, int curIndex, int indexLattice, double *Fx, double *Fy, double *Fz, double *Estr);

#endif

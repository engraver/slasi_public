#include "../../alltypes.h"
extern "C"{
	#include "stretching.h"
}

/* interaction (stretching) of one spin (GPU) */
__global__
void spinStretchingForceAndEnergyTriangleCUDA(double *d_F, double *d_lattice, double *d_EnergyTotal, LatticeSiteDescr * d_latticeParam, PeriodicBC periodicBC1, long N1Ext, long N2Ext, long N3Ext, long numberSites) {

	/* find indices of site */
	long indexLatticeSite = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndexSite = 3 * indexLatticeSite;
	long spinsNumber = 3 * numberSites;

	/* check boundary condition for threads */
	if ((indexLatticeSite < numberSites) &&
	    (d_lattice[curIndexSite] * d_lattice[curIndexSite] + d_lattice[curIndexSite + 1] * d_lattice[curIndexSite + 1] + d_lattice[curIndexSite + 2] * d_lattice[curIndexSite + 2] > LENG_ZERO)) {

            /* save indices using intermediate values */
            long indexLattice = indexLatticeSite;
            long curIndex = curIndexSite;

        	/***************************
            * i1-th site
            ***************************/
        	/* coordinates */
        	double xi = d_lattice[curIndex + spinsNumber];
        	double yi = d_lattice[curIndex + spinsNumber + 1];
        	double zi = d_lattice[curIndex + spinsNumber + 2];
        	/* coefficient of stretching */
        	double lambda_i = d_latticeParam[indexLattice].lambda;
        	if ((indexLattice == N1Ext - 2) && (periodicBC1 == PBC_OFF)) {
        		lambda_i = 0.0;
        	}

        	/***************************
            * (i1-1)-th site
            ***************************/
        	if (indexLatticeSite > 1) {
        		curIndex = IDXmgGPU(indexLatticeSite - 1, 1, 1);
        		indexLattice = IDXGPU(indexLatticeSite - 1, 1, 1);
        	} else {
        		curIndex = IDXmgGPU(N1Ext - 2, 1, 1);
        		indexLattice = IDXGPU(N1Ext - 2, 1, 1);
        	}
        	/* coordinates */
        	double xp = d_lattice[curIndex + spinsNumber];
        	double yp = d_lattice[curIndex + spinsNumber + 1];
        	double zp = d_lattice[curIndex + spinsNumber + 2];
        	/* coefficient of stretching */
        	double lambda_im1 = d_latticeParam[indexLattice].lambda;
        	if ((indexLattice == 1) && (periodicBC1 == PBC_OFF)) {
        		lambda_im1 = 0.0;
        	}

        	/***************************
            * (i1+1)-th site
            ****************************/
        	if (indexLattice < N1Ext - 2) {
        		curIndex = IDXmgGPU(indexLatticeSite + 1, 1, 1);
        		indexLattice = IDXGPU(indexLatticeSite + 1, 1, 1);
        	} else {
        		curIndex = IDXmgGPU(1, 1, 1);
        		indexLattice = IDXGPU(1, 1, 1);
        	}
        	/* coordinates */
        	double xn = d_lattice[curIndex + spinsNumber];
        	double yn = d_lattice[curIndex + spinsNumber + 1];
        	double zn = d_lattice[curIndex + spinsNumber + 2];

        	/* distance */
        	double txi = xn - xi;
        	double tyi = yn - yi;
        	double tzi = zn - zi;
        	double txim1 = xi - xp;
        	double tyim1 = yi - yp;
        	double tzim1 = zi - zp;

        	/* distance |r_{i+1} - r_{i}| */
        	double ri = sqrt(txi * txi + tyi * tyi + tzi * tzi);
        	/* distance |r_{i} - r_{i-1}| */
        	double rim1 = sqrt(txim1 * txim1 + tyim1 * tyim1 + tzim1 * tzim1);

        	/* calculate energy of stretching interaction */
        	d_EnergyTotal[indexLatticeSite] = lambda_i * (ri - 1.0) * (ri - 1.0);

        	/* calculate force of stretching interaction */
        	d_F[curIndexSite] -= ((lambda_im1 * (rim1 - 1.0) * txim1 / rim1 - lambda_i * (ri - 1.0) * txi / ri ));
        	d_F[curIndexSite + 1] -= ((lambda_im1 * (rim1 - 1.0) * tyim1 / rim1 - lambda_i * (ri - 1.0) * tyi / ri ));
        	d_F[curIndexSite + 2] -= ((lambda_im1 * (rim1 - 1.0) * tzim1 / rim1 - lambda_i * (ri - 1.0) * tzi / ri ));

	}

}

extern "C"
void stretchingForceAndEnergyTriangleCUDA(double *d_F, double *d_EnergyTotal, long numberSites) {

	/* setup the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* wrapper of CUDA function */
	spinStretchingForceAndEnergyTriangleCUDA<<<cores, threads>>>(d_F, d_lattice, d_EnergyTotal, d_latticeParam, userVars.periodicBC1, userVars.N1, userVars.N2, userVars.N3, numberSites);

}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "stretching.h"

int setupStretchingTriangle(double **numbers, int indlp, int indn) {

	if ((userVars.ilambda1 != -1) && (userVars.ilambda2 != -1)  && (userVars.ilambda3 != -1) &&
	    (userVars.ilambda4 != -1) && (userVars.ilambda5 != -1)  && (userVars.ilambda6 != -1)) {
		/* from parameter file */
		latticeParam[indlp].lambda1 = numbers[indn][userVars.ilambda1];
		latticeParam[indlp].lambda2 = numbers[indn][userVars.ilambda2];
		latticeParam[indlp].lambda3 = numbers[indn][userVars.ilambda3];
		latticeParam[indlp].lambda4 = numbers[indn][userVars.ilambda4];
		latticeParam[indlp].lambda5 = numbers[indn][userVars.ilambda5];
		latticeParam[indlp].lambda6 = numbers[indn][userVars.ilambda6];
	} else if (userVars.blambdaTri) {
		/* constant value */
		latticeParam[indlp].lambda1 = userVars.lambdaTri;
		latticeParam[indlp].lambda2 = userVars.lambdaTri;
		latticeParam[indlp].lambda3 = userVars.lambdaTri;
		latticeParam[indlp].lambda4 = userVars.lambdaTri;
		latticeParam[indlp].lambda5 = userVars.lambdaTri;
		latticeParam[indlp].lambda6 = userVars.lambdaTri;
	} else {
		/* message about error */
		char msg[] = "STRETCHING.C: Something wrong with flexible parameters (Are all of them given in paramFile?)!\n";
		fprintf(flog, "%s", msg);
		fprintf(stderr, "%s", msg);
		return 1;
	}

	return 0;

}

void stretchingForceAndEnergy(int i1, int curIndex1, int indexLattice1, double *Fx, double *Fy, double *Fz, double *Estr) {

	/* number of projections of all spins */
	size_t spinsNum = NUMBER_COMP_MAGN*userVars.N1*userVars.N2*userVars.N3;
	size_t curIndex, indexLattice;

	/***************************
    * i1-th site
    ***************************/
	/* coordinates */
	double xi = lattice[curIndex1 + spinsNum];
	double yi = lattice[curIndex1 + spinsNum + 1];
	double zi = lattice[curIndex1 + spinsNum + 2];
	/* coefficient of stretching */
	double lambda_i = latticeParam[indexLattice1].lambda;
	if ((i1 == userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF)) {
		lambda_i = 0.0;
	}

	/***************************
    * (i1-1)-th site
    ***************************/
	if (i1 > 1) {
		curIndex = IDXmg(i1 - 1, 1, 1);
		indexLattice = IDX(i1 - 1, 1, 1);
	} else {
		curIndex = IDXmg(userVars.N1 - 2, 1, 1);
		indexLattice = IDX(userVars.N1 - 2, 1, 1);
	}
	/* coordinates */
	double xp = lattice[curIndex + spinsNum];
	double yp = lattice[curIndex + spinsNum + 1];
	double zp = lattice[curIndex + spinsNum + 2];
	/* coefficient of stretching */
	double lambda_im1 = latticeParam[indexLattice].lambda;
	if ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		lambda_im1 = 0.0;
	}

	/***************************
    * (i1+1)-th site
    ****************************/
	if (i1 < userVars.N1 - 2) {
		curIndex = IDXmg(i1 + 1, 1, 1);
		indexLattice = IDX(i1 + 1, 1, 1);
	} else {
		curIndex = IDXmg(1, 1, 1);
		indexLattice = IDX(1, 1, 1);
	}
	/* coordinates */
	double xn = lattice[curIndex + spinsNum];
	double yn = lattice[curIndex + spinsNum + 1];
	double zn = lattice[curIndex + spinsNum + 2];

	/* distance */
	double txi = xn - xi;
	double tyi = yn - yi;
	double tzi = zn - zi;
	double txim1 = xi - xp;
	double tyim1 = yi - yp;
	double tzim1 = zi - zp;
	
	/* distance |r_{i+1} - r_{i}| */
	double ri = sqrt( txi*txi + tyi*tyi + tzi*tzi );
	/* distance |r_{i} - r_{i-1}| */
	double rim1 = sqrt( txim1*txim1 + tyim1*tyim1 + tzim1*tzim1 );

	/* calculate energy of stretching interaction */
	latticeParam[indexLattice1].EnergyStretchingNorm = lambda_i * (ri - 1.0) * (ri - 1.0);
	*Estr += latticeParam[indexLattice1].EnergyStretchingNorm;

	/* calculate force of stretching interaction */
	*Fx -= ((lambda_im1 * (rim1 - 1.0) * txim1 / rim1 - lambda_i * (ri - 1.0) * txi / ri ));
	*Fy -= ((lambda_im1 * (rim1 - 1.0) * tyim1 / rim1 - lambda_i * (ri - 1.0) * tyi / ri ));
	*Fz -= ((lambda_im1 * (rim1 - 1.0) * tzim1 / rim1 - lambda_i * (ri - 1.0) * tzi / ri ));

}

void stretchingForceAndEnergyTriangle(int i1, int i2, int curIndex, int indexLattice, double *Fx, double *Fy, double *Fz, double *Estr) {

	/* number of projections of all spins */
	size_t spinsNum = NUMBER_COMP_MAGN*userVars.N1*userVars.N2*userVars.N3;

	/***************************
    * i-th site
    ***************************/
	/* coordinates */
	double xi = lattice[curIndex + spinsNum];
	double yi = lattice[curIndex + spinsNum + 1];
	double zi = lattice[curIndex + spinsNum + 2];
    
	/***************************
    * 1-st neighbouring site
    ***************************/
	if (i1 < userVars.N1 - 2 ) {
		curIndex = IDXtrmg1(i1, i2);
	} else {
		curIndex = IDXtrmg(1, i2);
	}
	/* coordinates */
	double x1 = lattice[curIndex + spinsNum];
	double y1 = lattice[curIndex + spinsNum + 1];
	double z1 = lattice[curIndex + spinsNum + 2];
	/* coefficient of stretching */
	double lambda_1 = latticeParam[indexLattice].lambda1;
	if ((i1 == userVars.N1-2) && (userVars.periodicBC1 == PBC_OFF)) {
		lambda_1 = 0.0;
	}
    
	/***************************
    * 2-nd neighbouring site
    ***************************/
	if (i1 < userVars.N1 - 2) {
		curIndex = IDXtrmg2(i1, i2);
	} else if ((i1 == userVars.N1 - 2) && IsODD(i2) ) {
		/* odd row */
		curIndex = IDXtrmg2(i1, i2);
	} else {
		/* even row */
		curIndex = IDXtrmg(1, i2 + 1);
	}
	/* coordinates */
	double x2 = lattice[curIndex + spinsNum];
	double y2 = lattice[curIndex + spinsNum + 1];
	double z2 = lattice[curIndex + spinsNum + 2];
	/* coefficient of stretching */
	double lambda_2 = latticeParam[indexLattice].lambda2;
	if ((i1 == userVars.N1 - 2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		lambda_2 = 0.0;
	}
	if (i2 == userVars.N2 - 2) {
		lambda_2 = 0.0;
	}
    
	/***************************
    * 3-rd neighbouring site
    ***************************/
	if (i1 > 1){
		curIndex = IDXtrmg3(i1, i2);
	} else if ((i1 == 1) && IsODD(i2) ) {
		/* odd row */
		curIndex = IDXtrmg(userVars.N1 - 2, i2 + 1);
	} else {
		/* even row */
		curIndex = IDXtrmg3(i1, i2);
	}
	/* coordinates */
	double x3 = lattice[curIndex + spinsNum];
	double y3 = lattice[curIndex + spinsNum + 1];
	double z3 = lattice[curIndex + spinsNum + 2];
	/* coefficient of stretching */
	double lambda_3 = latticeParam[indexLattice].lambda3;
	if ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		lambda_3 = 0.0;
	}
	if (i2 == userVars.N2 - 2) {
		lambda_3 = 0.0;
	}	
	
	/***************************
    * 4-th neighbouring site
    ***************************/
	if (i1 > 1) {
		curIndex = IDXtrmg4(i1, i2);
	} else {
		curIndex = IDXtrmg(userVars.N1 - 2, i2);
	}
	/* coordinates */
	double x4 = lattice[curIndex + spinsNum];
	double y4 = lattice[curIndex + spinsNum + 1];
	double z4 = lattice[curIndex + spinsNum + 2];
	/* coefficient of stretching */
	double lambda_4 = latticeParam[indexLattice].lambda4;
	if ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		lambda_4 = 0.0;
	}
    
	/***************************
    * 5-th neighbouring site
    ***************************/
	if (i1 > 1) {
		curIndex = IDXtrmg5(i1, i2);
	} else if ((i1 == 1) && IsODD(i2) ) {
		/* odd row */
		curIndex = IDXtrmg(userVars.N1 - 2, i2 - 1);
	} else {
		/* even row */
		curIndex = IDXtrmg5(i1, i2);
	}
	/* coordinates */
	double x5 = lattice[curIndex + spinsNum];
	double y5 = lattice[curIndex + spinsNum + 1];
	double z5 = lattice[curIndex + spinsNum + 2];
	/* coefficient of stretching */
	double lambda_5 = latticeParam[indexLattice].lambda5;
	if ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF)) {
		lambda_5 = 0.0;
	}
	if (i2 == 1) {
		lambda_5 = 0.0;
	}
    
	/***************************
    * 6-th neighbouring site
    ***************************/
	if (i1 < userVars.N1 - 2) {
		curIndex = IDXtrmg6(i1, i2);
	} else if ((i1 == userVars.N1 - 2) && IsODD(i2) ) {
		/* odd row */
		curIndex = IDXtrmg6(i1, i2);
	} else {
		/*even row */
		curIndex = IDXtrmg(1, i2 - 1);
	}
	/* coordinates */
	double x6 = lattice[curIndex + spinsNum];
	double y6 = lattice[curIndex + spinsNum + 1];
	double z6 = lattice[curIndex + spinsNum + 2];
	/* coefficient of stretching */
	double lambda_6 = latticeParam[indexLattice].lambda6;
	if ((i1 == userVars.N1 - 2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		lambda_6 = 0.0;
	}
	if (i2 == 1) {
		lambda_6 = 0.0;
	}

	/* distance r1 - ri*/
	double xi1 = x1 - xi;
	double yi1 = y1 - yi;
	double zi1 = z1 - zi;
	/* distance r2 - ri*/
	double xi2 = x2 - xi;
	double yi2 = y2 - yi;
	double zi2 = z2 - zi;
	/* distance r3 - ri*/
	double xi3 = x3 - xi;
	double yi3 = y3 - yi;
	double zi3 = z3 - zi;
	/* distance ri - r4*/
	double xi4 = xi - x4;
	double yi4 = yi - y4;
	double zi4 = zi - z4;
	/* distance ri - r5*/
	double xi5 = xi - x5;
	double yi5 = yi - y5;
	double zi5 = zi - z5;
	/* distance ri - r6*/
	double xi6 = xi - x6;
	double yi6 = yi - y6;
	double zi6 = zi - z6;
	
	/* distance |r1 - ri| */
	double ri1 = sqrt( xi1*xi1 + yi1*yi1 + zi1*zi1 );
	/* distance |r2 - ri| */
	double ri2 = sqrt( xi2*xi2 + yi2*yi2 + zi2*zi2 );
	/* distance |r3 - ri| */
	double ri3 = sqrt( xi3*xi3 + yi3*yi3 + zi3*zi3 );
	/* distance |ri - r4| */
	double ri4 = sqrt( xi4*xi4 + yi4*yi4 + zi4*zi4 );
	/* distance |ri - r5| */
	double ri5 = sqrt( xi5*xi5 + yi5*yi5 + zi5*zi5 );
	/* distance |ri - r6| */
	double ri6 = sqrt( xi6*xi6 + yi6*yi6 + zi6*zi6 );
	
	/* rescaled lambda_i */
	lambda_1 *= (ri1 - 1.0);
	lambda_2 *= (ri2 - 1.0);
	lambda_3 *= (ri3 - 1.0);
	lambda_4 *= (ri4 - 1.0);
	lambda_5 *= (ri5 - 1.0);
	lambda_6 *= (ri6 - 1.0);
	
	/* calculate energy of stretching interaction */
	*Estr += 0.5 * ( lambda_1 * (ri1 - 1.0) +
	                 lambda_2 * (ri2 - 1.0) +
	                 lambda_3 * (ri3 - 1.0) +
	                 lambda_4 * (ri4 - 1.0) +
	                 lambda_5 * (ri5 - 1.0) +
	                 lambda_6 * (ri6 - 1.0)
	               );

	/* calculate force of stretching interaction */
     
	*Fx += ( lambda_1 * xi1 / ri1 +
	         lambda_2 * xi2 / ri2 +
	         lambda_3 * xi3 / ri3 -
	         lambda_4 * xi4 / ri4 -
	         lambda_5 * xi5 / ri5 -
	         lambda_6 * xi6 / ri6
	);
	*Fy += ( lambda_1 * yi1 / ri1 +
	         lambda_2 * yi2 / ri2 +
	         lambda_3 * yi3 / ri3 -
	         lambda_4 * yi4 / ri4 -
	         lambda_5 * yi5 / ri5 -
	         lambda_6 * yi6 / ri6
	);
	*Fz += ( lambda_1 * zi1 / ri1 +
	         lambda_2 * zi2 / ri2 +
	         lambda_3 * zi3 / ri3 -
	         lambda_4 * zi4 / ri4 -
	         lambda_5 * zi5 / ri5 -
	         lambda_6 * zi6 / ri6
	);

}

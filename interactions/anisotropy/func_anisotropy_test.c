#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "CUnit/Basic.h"

#include "../../alltypes.h"
#include "../../simulation.h"

/* names of configuration files for testing */
#define FILE_ANIS_CFG1 "testanis1.cfg"
#define FILE_ANIS_DAT1 "testanis1.dat"
#define FILE_ANIS_BAS1 "testanis1"

#define FILE_ANIS_CFG2 "testanis2.cfg"
#define FILE_ANIS_DAT2 "testanis2.dat"
#define FILE_ANIS_BAS2 "testanis2"

#define FILE_ANIS_CFG3 "testanis3.cfg"
#define FILE_ANIS_DAT3 "testanis3.dat"
#define FILE_ANIS_BAS3 "testanis3"

#define FILE_ANIS_CFG4 "testanis4.cfg"
#define FILE_ANIS_DAT4 "testanis4.dat"
#define FILE_ANIS_BAS4 "testanis4"

#define PREC_FUNC_TEST_ANIS 0.000001

/* initialization function for tests */
int init_func_anisotropy_test(void) {
	return 0;
}

/* function of cleaning for tests */
int clean_func_anisotropy_test(void) {
	return 0;
}

/* function to create files to test magnetic simulator (for anisotropy) */
void createFuncAnisFiles(char * nameConfFile, char * nameParamFile, char * rowConfFile, char * rowParamFile) {

	/* create configuration file */
	FILE * fid1 = fopen(nameConfFile, "w");
	fprintf(fid1, "%s", rowConfFile);
	fclose(fid1);

	/* create parameter file */
	FILE * fid2 = fopen(nameParamFile, "w");
	fprintf(fid2, "%s", rowParamFile);
	fclose(fid2);

}

/* function to launch simulation (for anisotropy) */
void launchFuncAnisSimulation(char * nameConfFile, char * nameParamFile) {

	/* make parameters for launching */
	int argc = 4;
	char **argv;
	argv = (char**) malloc(sizeof(char**) * 4);
	argv[0] = (char*) malloc(sizeof("slasi"));
	argv[0] = "slasi";
	argv[1] = (char*) malloc(sizeof(nameConfFile));
	argv[1] = nameConfFile;
	argv[2] = (char*) malloc(sizeof("-p"));
	argv[2] = "-p";
	argv[3] = (char*) malloc(sizeof(nameParamFile));
	argv[3] = nameParamFile;

	/* launch simulation */
	slasi(argc, argv, false);

}

/* function to check magnetic moments of system */
void checkAnisMagnetism(int N1, int N2, int N3, double mx, double my, double mz) {

	/* go through all nodes */
	for (int i1 = 1; i1 <= N1; i1++) {
		for (int i2 = 1; i2 <= N2; i2++) {
			for (int i3 = 1; i3 <= N3; i3++) {

				/* check node */
				int indexMagn = IDXmg(i1, i2, i3);
				CU_ASSERT((fabs(lattice[indexMagn] - mx) < PREC_FUNC_TEST_ANIS) || (fabs(lattice[indexMagn] + mx) < PREC_FUNC_TEST_ANIS));
				CU_ASSERT((fabs(lattice[indexMagn + 1] - my) < PREC_FUNC_TEST_ANIS) || (fabs(lattice[indexMagn + 1] + my) < PREC_FUNC_TEST_ANIS));
				CU_ASSERT((fabs(lattice[indexMagn + 2] - mz) < PREC_FUNC_TEST_ANIS) || (fabs(lattice[indexMagn + 2] + mz) < PREC_FUNC_TEST_ANIS));

			}
		}
	}

}

/* function to delete two files (for anisotropy) */
void deleteFuncAnisFiles(char * nameConfFile, char * nameParamFile, char * baseName) {

	/* delete parameter and configuration files */
	remove(nameConfFile);
	remove(nameParamFile);

	/* delete energy logfile which are created during simulation */
	char energy_log [100];
	strcpy(energy_log, baseName);
	strcat(energy_log, "_energy.log");
	remove(energy_log);

	/* delete logfile of initialization which are created during simulation */
	char init_log [100];
	strcpy(init_log, baseName);
	strcat(init_log, "_init.log");
	remove(init_log);

	/* delete logfile of magnetism which are created during simulation */
	char mag_log [100];
	strcpy(mag_log, baseName);
	strcat(mag_log, "_mag.log");
	remove(mag_log);

	/* delete logfile for other parameters which are created during simulation */
	char other_log [100];
	strcpy(other_log, baseName);
	strcat(other_log, "_other.log");
	remove(other_log);

	/* delete file for table of parameters */
	char table_file [100];
	strcpy(table_file, baseName);
	strcat(table_file, ".tbl");
	remove(table_file);

	/* delete the first file of intermediate results */
	char file_inter1 [100];
	strcpy(file_inter1, baseName);
	strcat(file_inter1, ".00000.slsb");
	remove(file_inter1);

	/* delete the second file of intermediate results */
	char file_inter2 [100];
	strcpy(file_inter2, baseName);
	strcat(file_inter2, ".00001.slsb");
	remove(file_inter2);

	/* delete the restart file of intermediate results */
	char file_restart [100];
	strcpy(file_restart, baseName);
	strcat(file_restart, ".restart.slsb");
	remove(file_restart);

}

/*
 * @brief yet another test of exchange interaction and anisotropy
 *
 * chain of two magnetic moments with exchange interaction and anisotropy
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3
 * 000 000 000 000
 * 000 0+0 0+0 000
 * 000 000 000 000
 * @endverbatim
 * Here, + magnetic sites with initial values (0.707,0.707,0) and (0.707,0.5,0.5).
 * The magnetic system has exchange interaction and easy axis anisotropy (1,0,0).
 * So, all magnetic moments have to be parallel and directed along axis of anisotropy.
 */
void test_anisotropy_simulation_1(void) {

	/* number of spins along axis */
	int N1 = 2;
	int N2 = 1;
	int N3 = 1;

	/* correct values of components of magnetic moments */
	double mx = 1.0;
	double my = 0.0;
	double mz = 0.0;

	/* create files to test magnetic simulator */
	createFuncAnisFiles(FILE_ANIS_CFG1, FILE_ANIS_DAT1, "N1 = 2\nN2 = 1\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 100\nframe = 100\naniK1 = 4e-10\noutput = SLSB\n", "mx my mz x y z\n0.707 0.707 0 0 0 0\n0.707 0.5 0.5 1 0 0\n");

	/* launch simulation */
	launchFuncAnisSimulation(FILE_ANIS_CFG1, FILE_ANIS_DAT1);

	/* check nodes */
	checkAnisMagnetism(N1, N2, N3, mx, my, mz);

	/* delete created files */
	deleteFuncAnisFiles(FILE_ANIS_CFG1, FILE_ANIS_DAT1, FILE_ANIS_BAS1);

}

/*
 * @brief yet another test of exchange interaction and anisotropy
 *
 * plain of four magnetic moments with exchange interaction and anisotropy
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3
 * 000 000 000 000
 * 000 0+0 0+0 000
 * 000 0+0 0+0 000
 * 000 000 000 000
 * @endverbatim
 * Here, + magnetic sites with initial values (1.0, 0.0, 0.0), (0.0, 1.0, 0.0),
 * (0.707, 0.5, 0.5) and (0.5, 0.707, 0.5).
 * The magnetic system has exchange interaction and easy axis anisotropy (0.707, 0.707, 0).
 * So, all magnetic moments have to be parallel and directed along axis of anisotropy.
 */
void test_anisotropy_simulation_2(void) {

	/* number of spins along axis */
	int N1 = 2;
	int N2 = 2;
	int N3 = 1;

	/* correct values of components of magnetic moments */
	double mx = 0.707107;
	double my = 0.707107;
	double mz = 0.0;

	/* create files to test magnetic simulator */
	createFuncAnisFiles(FILE_ANIS_CFG2, FILE_ANIS_DAT2, "N1 = 2\nN2 = 2\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 200\nframe = 200\naniK1 = 4e-10\nanisotropyType = file\noutput = SLSB\n", "mx my mz x y z e11 e12 e13 e21 e22 e23 e31 e32 e33\n1.0 0.0 0.0 1 1 1 0.707 0.707 0.0 1.0 0.0 0.0 1.0 0.0 0.0\n0.0 1.0 0.0 1 2 1 0.707 0.707 0.0 1.0 0.0 0.0 1.0 0.0 0.0\n0.707 0.5 0.5 2 1 1 0.707 0.707 0.0 1.0 0.0 0.0 1.0 0.0 0.0\n0.5 0.707 0.5 2 2 1 0.707 0.707 0.0 1.0 0.0 0.0 1.0 0.0 0.0\n");

	/* launch simulation */
	launchFuncAnisSimulation(FILE_ANIS_CFG2, FILE_ANIS_DAT2);

	/* check nodes */
	checkAnisMagnetism(N1, N2, N3, mx, my, mz);

	/* delete created files */
	deleteFuncAnisFiles(FILE_ANIS_CFG2, FILE_ANIS_DAT2, FILE_ANIS_BAS2);

}

/*
 * @brief yet another test of exchange interaction and anisotropy
 *
 * cube of eight magnetic moments with exchange interaction and anisotropy
 *
 * @verbatim
 * Location of magnetic moment components in an array
 * 0    1    2    3
 * 0000 0000 0000 0000
 * 0000 0++0 0++0 0000
 * 0000 0++0 0++0 0000
 * 0000 0000 0000 0000
 * @endverbatim
 * Here, + magnetic sites with initial values (1.0,0.0,0.0),
 * (0.0,1.0,0.0), (0.0,0.0,1.0), (0.707,0.707,0.0), (0.0,0.707,0.707),
 * (0.707,0.0,0.707), (0.5,0.5,0.707), (0.5,0.707,0.5).
 * The magnetic system has exchange interaction and easy axis anisotropy (0.707,0.5,0.5).
 * So, all magnetic moments have to be parallel and directed along axis of anisotropy.
 */
void test_anisotropy_simulation_3(void) {

	/* number of spins along axis */
	int N1 = 2;
	int N2 = 2;
	int N3 = 2;

	/* correct values of components of magnetic moments */
	double mx = 0.707053;
	double my = 0.500038;
	double mz = 0.500038;

	/* create files to test magnetic simulator */
	createFuncAnisFiles(FILE_ANIS_CFG3, FILE_ANIS_DAT3, "N1 = 2\nN2 = 2\nN3 = 2\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 400\nframe = 400\naniK2 = 4e-10\nanisotropyType = file\noutput = SLSB\n", "mx my mz x y z e11 e12 e13 e21 e22 e23 e31 e32 e33\n1.0 0.0 0.0 1 1 1 1.0 0.0 0.0 0.707 0.5 0.5 1.0 0.0 0.0\n0.0 1.0 0.0 1 1 2 1.0 0.0 0.0 0.707 0.5 0.5 1.0 0.0 0.0\n0.0 0.0 1.0 1 2 1 1.0 0.0 0.0 0.707 0.5 0.5 1.0 0.0 0.0\n0.707 0.707 0.0 1 2 2 1.0 0.0 0.0 0.707 0.5 0.5 1.0 0.0 0.0\n0.0 0.707 0.707 2 1 1 1.0 0.0 0.0 0.707 0.5 0.5 1.0 0.0 0.0\n0.707 0.0 0.707 2 1 2 1.0 0.0 0.0 0.707 0.5 0.5 1.0 0.0 0.0\n0.5 0.5 0.707 2 2 1 1.0 0.0 0.0 0.707 0.5 0.5 1.0 0.0 0.0\n0.5 0.707 0.5 2 2 2 1.0 0.0 0.0 0.707 0.5 0.5 1.0 0.0 0.0\n");

	/* launch simulation */
	launchFuncAnisSimulation(FILE_ANIS_CFG3, FILE_ANIS_DAT3);

	/* check nodes */
	checkAnisMagnetism(N1, N2, N3, mx, my, mz);

	/* delete created files */
	deleteFuncAnisFiles(FILE_ANIS_CFG3, FILE_ANIS_DAT3, FILE_ANIS_BAS3);

}

/*
 * @brief yet another test of exchange interaction and anisotropy
 *
 * cube of eight magnetic moments with exchange interaction and anisotropy
 *
 * @verbatim
 * Location of magnetic moment components in an array
 * 0    1    2    3
 * 0000 0000 0000 0000
 * 0000 0++0 0++0 0000
 * 0000 0++0 0++0 0000
 * 0000 0000 0000 0000
 * @endverbatim
 * Here, + magnetic site with random initial value.
 * The magnetic system has exchange interaction and easy axis anisotropy (0.707,0.0,0.707).
 * So, all magnetic moments have to be parallel and directed along axis of anisotropy.
 */
void test_anisotropy_simulation_4(void) {

	/* number of spins along axis */
	int N1 = 2;
	int N2 = 2;
	int N3 = 2;

	/* correct values of components of magnetic moments */
	double mx = 0.707107;
	double my = 0.0;
	double mz = 0.707107;

	/* create files to test magnetic simulator */
	createFuncAnisFiles(FILE_ANIS_CFG4, FILE_ANIS_DAT4, "N1 = 2\nN2 = 2\nN3 = 2\nJ = 1e-10\nmagnInit = random\ngilbertDamping=0.5\nt0 = 0\ntfin = 400\nframe = 400\naniK3 = 4e-10\nanisotropyType = file\noutput = SLSB\n", "x y z e11 e12 e13 e21 e22 e23 e31 e32 e33\n1 1 1 1.0 0.0 0.0 1.0 0.0 0.0 0.707 0.0 0.707\n1 1 2 1.0 0.0 0.0 1.0 0.0 0.0 0.707 0.0 0.707\n1 2 1 1.0 0.0 0.0 1.0 0.0 0.0 0.707 0.0 0.707\n1 2 2 1.0 0.0 0.0 1.0 0.0 0.0 0.707 0.0 0.707\n2 1 1 1.0 0.0 0.0 1.0 0.0 0.0 0.707 0.0 0.707\n2 1 2 1.0 0.0 0.0 1.0 0.0 0.0 0.707 0.0 0.707\n2 2 1 1.0 0.0 0.0 1.0 0.0 0.0 0.707 0.0 0.707\n2 2 2 1.0 0.0 0.0 1.0 0.0 0.0 0.707 0.0 0.707\n");

	/* launch simulation */
	launchFuncAnisSimulation(FILE_ANIS_CFG4, FILE_ANIS_DAT4);

	/* check nodes */
	checkAnisMagnetism(N1, N2, N3, mx, my, mz);

	/* delete created files */
	deleteFuncAnisFiles(FILE_ANIS_CFG4, FILE_ANIS_DAT4, FILE_ANIS_BAS4);

}
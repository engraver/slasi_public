#include <stdio.h>
#include <stdlib.h>
#include "uniaxialAnisotropy.h"
#include "../../utils/indices.h"

/* identification of coordinates for gradient */
#define id0 0
#define id1 1
#define id2 2
#define id3 3
#define id4 4
#define id5 5
#define id6 6

/* forbidden index */
#define forbIndex -1

int setupAniK(double **numbers, int indlp, int indn) {

	/* aniK1 */
	if (userVars.baniK1) {
		latticeParam[indlp].aniK1 = userVars.aniK1;
	} else if (userVars.ianiK1 != -1) {
		if (numbers == NULL) {
			char msg[] = "UNIAXIALANISOTROPY.C: paramFile is not processed, while expected for aniK1!\n";
			fprintf(flog, "%s", msg);
			fprintf(stderr, "%s", msg);
			return 1;
		}
		latticeParam[indlp].aniK1 = numbers[indn][userVars.ianiK1];
	}

	/* aniK2 */
	if (userVars.baniK2) {
		latticeParam[indlp].aniK2 = userVars.aniK2;
	} else if (userVars.ianiK2 != -1) {
		if (numbers == NULL) {
			char msg[] = "UNIAXIALANISOTROPY.C: paramFile is not processed, while expected for aniK2!\n";
			fprintf(flog, "%s", msg);
			fprintf(stderr, "%s", msg);
			return 1;
		}
		latticeParam[indlp].aniK2 = numbers[indn][userVars.ianiK2];
	}

	/* aniK3 */
	if (userVars.baniK3) {
		latticeParam[indlp].aniK3 = userVars.aniK3;
	} else if (userVars.ianiK3 != -1) {
		if (numbers == NULL) {
			char msg[] = "UNIAXIALANISOTROPY.C: paramFile is not processed, while expected for aniK3!\n";
			fprintf(flog, "%s", msg);
			fprintf(stderr, "%s", msg);
			return 1;
		}
		latticeParam[indlp].aniK3 = numbers[indn][userVars.ianiK3];
	}

	return 0;

}


/* the following code was not tested! */
int setupInterAniK(double **numbers, int indlp, int indn) {

	/* aniInterK1 */
	if (userVars.baniInterK1) {
		latticeParam[indlp].aniInterK1m1 = userVars.aniInterK1;
		latticeParam[indlp].aniInterK1p1 = userVars.aniInterK1;
		latticeParam[indlp].aniInterK1m2 = userVars.aniInterK1;
		latticeParam[indlp].aniInterK1p2 = userVars.aniInterK1;
		latticeParam[indlp].aniInterK1m3 = userVars.aniInterK1;
		latticeParam[indlp].aniInterK1p3 = userVars.aniInterK1;
	} else if (userVars.ianiInterK1m1 != -1 || userVars.ianiInterK1p1 != -1 || userVars.ianiInterK1m2 != -1 ||
	           userVars.ianiInterK1p2 != -1 || userVars.ianiInterK1m3 != -1 || userVars.ianiInterK1p3 != -1) {
		if (!(userVars.ianiInterK1m1 != -1 && userVars.ianiInterK1p1 != -1 && userVars.ianiInterK1m2 != -1 &&
		      userVars.ianiInterK1p2 != -1 && userVars.ianiInterK1m3 != -1 && userVars.ianiInterK1p3 != -1) &&
		    numbers == NULL) {
			char msg[] = "UNIAXIALANISOTROPY.C: paramFile is not processed, while expected for aniInterK1!\n";
			fprintf(flog, "%s", msg);
			fprintf(stderr, "%s", msg);
			return 1;
		}
		latticeParam[indlp].aniInterK1m1 = numbers[indn][userVars.ianiInterK1m1];
		latticeParam[indlp].aniInterK1p1 = numbers[indn][userVars.ianiInterK1p1];
		latticeParam[indlp].aniInterK1m2 = numbers[indn][userVars.ianiInterK1m2];
		latticeParam[indlp].aniInterK1p2 = numbers[indn][userVars.ianiInterK1p2];
		latticeParam[indlp].aniInterK1m3 = numbers[indn][userVars.ianiInterK1m3];
		latticeParam[indlp].aniInterK1p3 = numbers[indn][userVars.ianiInterK1p3];
	} else if (userVars.ianiInterK1m1 != -1 || userVars.ianiInterK1p1 != -1 || userVars.ianiInterK1m2 != -1 ||
	           userVars.ianiInterK1p2 != -1 || userVars.ianiInterK1m3 != -1 || userVars.ianiInterK1p3 != -1) {
		char msg[] = "UNIAXIALANISOTROPY.C: paramFile is not processed, while expected for aniInterK1!\n";
		fprintf(flog, "%s", msg);
		fprintf(stderr, "%s", msg);
		return 1;
	}

	/* aniInterK2 */
	if (userVars.baniInterK2) {
		latticeParam[indlp].aniInterK2m1 = userVars.aniInterK2;
		latticeParam[indlp].aniInterK2p1 = userVars.aniInterK2;
		latticeParam[indlp].aniInterK2m2 = userVars.aniInterK2;
		latticeParam[indlp].aniInterK2p2 = userVars.aniInterK2;
		latticeParam[indlp].aniInterK2m3 = userVars.aniInterK2;
		latticeParam[indlp].aniInterK2p3 = userVars.aniInterK2;
	} else if (userVars.ianiInterK2m1 != -1 || userVars.ianiInterK2p1 != -1 || userVars.ianiInterK2m2 != -1 ||
	           userVars.ianiInterK2p2 != -1 || userVars.ianiInterK2m3 != -1 || userVars.ianiInterK2p3 != -1) {
		if (!(userVars.ianiInterK2m1 != -1 && userVars.ianiInterK2p1 != -1 && userVars.ianiInterK2m2 != -1 &&
		      userVars.ianiInterK2p2 != -1 && userVars.ianiInterK2m3 != -1 && userVars.ianiInterK2p3 != -1) &&
		    numbers == NULL) {
			char msg[] = "UNIAXIALANISOTROPY.C: paramFile is not processed, while expected for aniInterK2!\n";
			fprintf(flog, "%s", msg);
			fprintf(stderr, "%s", msg);
			return 1;
		}
		latticeParam[indlp].aniInterK2m1 = numbers[indn][userVars.ianiInterK2m1];
		latticeParam[indlp].aniInterK2p1 = numbers[indn][userVars.ianiInterK2p1];
		latticeParam[indlp].aniInterK2m2 = numbers[indn][userVars.ianiInterK2m2];
		latticeParam[indlp].aniInterK2p2 = numbers[indn][userVars.ianiInterK2p2];
		latticeParam[indlp].aniInterK2m3 = numbers[indn][userVars.ianiInterK2m3];
		latticeParam[indlp].aniInterK2p3 = numbers[indn][userVars.ianiInterK2p3];
	} else if (userVars.ianiInterK2m1 != -1 || userVars.ianiInterK2p1 != -1 || userVars.ianiInterK2m2 != -1 ||
	           userVars.ianiInterK2p2 != -1 || userVars.ianiInterK2m3 != -1 || userVars.ianiInterK2p3 != -1) {
		char msg[] = "UNIAXIALANISOTROPY.C: paramFile is not processed, while expected for aniInterK2!\n";
		fprintf(flog, "%s", msg);
		fprintf(stderr, "%s", msg);
		return 1;
	}

	/* aniInterK3 */
	if (userVars.baniInterK3) {
		latticeParam[indlp].aniInterK3m1 = userVars.aniInterK3;
		latticeParam[indlp].aniInterK3p1 = userVars.aniInterK3;
		latticeParam[indlp].aniInterK3m2 = userVars.aniInterK3;
		latticeParam[indlp].aniInterK3p2 = userVars.aniInterK3;
		latticeParam[indlp].aniInterK3m3 = userVars.aniInterK3;
		latticeParam[indlp].aniInterK3p3 = userVars.aniInterK3;
	} else if (userVars.ianiInterK3m1 != -1 || userVars.ianiInterK3p1 != -1 || userVars.ianiInterK3m2 != -1 ||
	           userVars.ianiInterK3p2 != -1 || userVars.ianiInterK3m3 != -1 || userVars.ianiInterK3p3 != -1) {
		if (!(userVars.ianiInterK3m1 != -1 && userVars.ianiInterK3p1 != -1 && userVars.ianiInterK3m2 != -1 &&
		      userVars.ianiInterK3p2 != -1 && userVars.ianiInterK3m3 != -1 && userVars.ianiInterK3p3 != -1) &&
		    numbers == NULL) {
			char msg[] = "UNIAXIALANISOTROPY.C: paramFile is not processed, while expected for aniInterK3!\n";
			fprintf(flog, "%s", msg);
			fprintf(stderr, "%s", msg);
			return 1;
		}
		latticeParam[indlp].aniInterK3m1 = numbers[indn][userVars.ianiInterK3m1];
		latticeParam[indlp].aniInterK3p1 = numbers[indn][userVars.ianiInterK3p1];
		latticeParam[indlp].aniInterK3m2 = numbers[indn][userVars.ianiInterK3m2];
		latticeParam[indlp].aniInterK3p2 = numbers[indn][userVars.ianiInterK3p2];
		latticeParam[indlp].aniInterK3m3 = numbers[indn][userVars.ianiInterK3m3];
		latticeParam[indlp].aniInterK3p3 = numbers[indn][userVars.ianiInterK3p3];
	} else if (userVars.ianiInterK3m1 != -1 || userVars.ianiInterK3p1 != -1 || userVars.ianiInterK3m2 != -1 ||
	           userVars.ianiInterK3p2 != -1 || userVars.ianiInterK3m3 != -1 || userVars.ianiInterK3p3 != -1) {
		char msg[] = "UNIAXIALANISOTROPY.C: paramFile is not processed, while expected for aniInterK3!\n";
		fprintf(flog, "%s", msg);
		fprintf(stderr, "%s", msg);
		return 1;
	}

	return 0;

}

int setupAnisotropyAxes(double **numbers, int indlp, int indn) {

	/* additional variables */
	double length;
	double e11, e12, e13, e21, e22, e23, e31, e32, e33;

	if ((userVars.ie11 != -1) && (userVars.ie12 != -1) && (userVars.ie13 != -1) &&
	    (userVars.ie21 != -1) && (userVars.ie22 != -1) && (userVars.ie23 != -1) &&
	    (userVars.ie31 != -1) && (userVars.ie32 != -1) && (userVars.ie33 != -1) &&
	    (userVars.anisotropyType == ANIS_FILE)) {
		/* from file */
		if (numbers == NULL) {
			char msg[] = "UNIAXIALANISOTROPY.C: paramFile is not processed, while expected for setupAnisotropyAxes()!\n";
			fprintf(flog, "%s", msg);
			fprintf(stderr, "%s", msg);
			return 1;
		}

		/* the first axis */
		e11 = numbers[indn][userVars.ie11];
		e12 = numbers[indn][userVars.ie12];
		e13 = numbers[indn][userVars.ie13];

		/* calculate for normalization */
		length = sqrt(e11 * e11 + e12 * e12 + e13 * e13);

		latticeParam[indlp].e11 = e11 / length;
		latticeParam[indlp].e12 = e12 / length;
		latticeParam[indlp].e13 = e13 / length;

		/* the second axis */
		e21 = numbers[indn][userVars.ie21];
		e22 = numbers[indn][userVars.ie22];
		e23 = numbers[indn][userVars.ie23];

		/* calculate for normalization */
		length = sqrt(e21 * e21 + e22 * e22 + e23 * e23);

		latticeParam[indlp].e21 = e21 / length;
		latticeParam[indlp].e22 = e22 / length;
		latticeParam[indlp].e23 = e23 / length;

		/* the third axis */
		e31 = numbers[indn][userVars.ie31];
		e32 = numbers[indn][userVars.ie32];
		e33 = numbers[indn][userVars.ie33];

		/* calculate for normalization */
		length = sqrt(e31 * e31 + e32 * e32 + e33 * e33);

		latticeParam[indlp].e31 = e31 / length;
		latticeParam[indlp].e32 = e32 / length;
		latticeParam[indlp].e33 = e33 / length;

		/* variable to test becomes "true" */
		testAnisotropy = true;

	} else if ((userVars.be11) && (userVars.be12) && (userVars.be13) &&
	           (userVars.be21) && (userVars.be22) && (userVars.be23) &&
	           (userVars.be31) && (userVars.be32) && (userVars.be33) &&
	           (userVars.anisotropyType == ANIS_CONST)) {
		/* constant value */

		/* the first axis */
		e11 = userVars.e11;
		e12 = userVars.e12;
		e13 = userVars.e13;

		/* calculate for normalization */
		length = sqrt(e11 * e11 + e12 * e12 + e13 * e13);

		latticeParam[indlp].e11 = e11 / length;
		latticeParam[indlp].e12 = e12 / length;
		latticeParam[indlp].e13 = e13 / length;

		/* the second axis */
		e21 = userVars.e21;
		e22 = userVars.e22;
		e23 = userVars.e23;

		/* calculate for normalization */
		length = sqrt(e21 * e21 + e22 * e22 + e23 * e23);

		latticeParam[indlp].e21 = e21 / length;
		latticeParam[indlp].e22 = e22 / length;
		latticeParam[indlp].e23 = e23 / length;

		/* the third axis */
		e31 = userVars.e31;
		e32 = userVars.e32;
		e33 = userVars.e33;

		/* calculate for normalization */
		length = sqrt(e31 * e11 + e32 * e32 + e33 * e33);

		latticeParam[indlp].e31 = e31 / length;
		latticeParam[indlp].e32 = e32 / length;
		latticeParam[indlp].e33 = e33 / length;

		/* variable to test becomes "true" */
		testAnisotropy = true;

	} else {
		/* default vector for anisotropy */

		/* the first axis */
		latticeParam[indlp].e11 = 1.0;
		latticeParam[indlp].e12 = 0.0;
		latticeParam[indlp].e13 = 0.0;

		/* the second axis */
		latticeParam[indlp].e21 = 0.0;
		latticeParam[indlp].e22 = 1.0;
		latticeParam[indlp].e23 = 0.0;

		/* the third axis */
		latticeParam[indlp].e31 = 0.0;
		latticeParam[indlp].e32 = 0.0;
		latticeParam[indlp].e33 = 1.0;
	}

	return 0;

}


void uniaxialAnisotropyField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EaniK1, double *EaniK2, double *EaniK3) {

	/* project vectors of anisotropy on orthonormal basis */
	/* the first vector */
	double e11, e12, e13;
	/* the second vector */
	double e21, e22, e23;
	/* the third vector */
	double e31, e32, e33;
	/* if it is curve, then we will use TNB-orthonormal basis  */
	if ((userVars.dimensionOfSystem == 1) || ((userVars.dimensionOfSystem == 2) && (userVars.latticeType == LATTICE_CUBIC))) {

		e11 = latticeParam[indexLattice].tx * latticeParam[indexLattice].e11
		      + latticeParam[indexLattice].nx * latticeParam[indexLattice].e12
		      + latticeParam[indexLattice].bx * latticeParam[indexLattice].e13;
		e12 = latticeParam[indexLattice].ty * latticeParam[indexLattice].e11
		      + latticeParam[indexLattice].ny * latticeParam[indexLattice].e12
		      + latticeParam[indexLattice].by * latticeParam[indexLattice].e13;
		e13 = latticeParam[indexLattice].tz * latticeParam[indexLattice].e11
		      + latticeParam[indexLattice].nz * latticeParam[indexLattice].e12
		      + latticeParam[indexLattice].bz * latticeParam[indexLattice].e13;

		e21 = latticeParam[indexLattice].tx * latticeParam[indexLattice].e21
		      + latticeParam[indexLattice].nx * latticeParam[indexLattice].e22
		      + latticeParam[indexLattice].bx * latticeParam[indexLattice].e23;
		e22 = latticeParam[indexLattice].ty * latticeParam[indexLattice].e21
		      + latticeParam[indexLattice].ny * latticeParam[indexLattice].e22
		      + latticeParam[indexLattice].by * latticeParam[indexLattice].e23;
		e23 = latticeParam[indexLattice].tz * latticeParam[indexLattice].e21
		      + latticeParam[indexLattice].nz * latticeParam[indexLattice].e22
		      + latticeParam[indexLattice].bz * latticeParam[indexLattice].e23;

		e31 = latticeParam[indexLattice].tx * latticeParam[indexLattice].e31
		      + latticeParam[indexLattice].nx * latticeParam[indexLattice].e32
		      + latticeParam[indexLattice].bx * latticeParam[indexLattice].e33;
		e32 = latticeParam[indexLattice].ty * latticeParam[indexLattice].e31
		      + latticeParam[indexLattice].ny * latticeParam[indexLattice].e32
		      + latticeParam[indexLattice].by * latticeParam[indexLattice].e33;
		e33 = latticeParam[indexLattice].tz * latticeParam[indexLattice].e31
		      + latticeParam[indexLattice].nz * latticeParam[indexLattice].e32
		      + latticeParam[indexLattice].bz * latticeParam[indexLattice].e33;

	} else {

		/* if it is not curve, then we put default orthonormal basis */
		e11 = latticeParam[indexLattice].e11;
		e12 = latticeParam[indexLattice].e12;
		e13 = latticeParam[indexLattice].e13;

		e21 = latticeParam[indexLattice].e21;
		e22 = latticeParam[indexLattice].e22;
		e23 = latticeParam[indexLattice].e23;

		e31 = latticeParam[indexLattice].e31;
		e32 = latticeParam[indexLattice].e32;
		e33 = latticeParam[indexLattice].e33;

	}

	//to debug code
	//fprintf(stderr, "%f %f %f\n", latticeParam[indexLattice].tx, latticeParam[indexLattice].ty, latticeParam[indexLattice].tz);
	//fprintf(stderr, "%f %f %f\n", e11, e12, e13);
	//fprintf(stderr, "%f\n", latticeParam[indexLattice].aniK1);

	/* calculate effective field and energy of the first axis */
	double inner1 = lattice[curIndex]   * e11
	                + lattice[curIndex+1] * e12
	                + lattice[curIndex+2] * e13;

	double effectFieldX1 = latticeParam[indexLattice].aniK1 * e11 * inner1;
	double effectFieldY1 = latticeParam[indexLattice].aniK1 * e12 * inner1;
	double effectFieldZ1 = latticeParam[indexLattice].aniK1 * e13 * inner1;

	latticeParam[indexLattice].EnergyAnisotropyNormAxisK1 =
			-(effectFieldX1*lattice[curIndex] + effectFieldY1*lattice[curIndex+1] + effectFieldZ1*lattice[curIndex+2]);
	*EaniK1 += latticeParam[indexLattice].EnergyAnisotropyNormAxisK1;

	/* if system is flexible or has triangle lattice, then we will not calculate other two axes */
	if ((userVars.flexOnOff == FLEX_OFF) && (userVars.latticeType == LATTICE_CUBIC)) {

		/* calculate effective field and energy of the second axis */
		double inner2 = lattice[curIndex] * e21
		                + lattice[curIndex + 1] * e22
		                + lattice[curIndex + 2] * e23;

		double effectFieldX2 = latticeParam[indexLattice].aniK2 * e21 * inner2;
		double effectFieldY2 = latticeParam[indexLattice].aniK2 * e22 * inner2;
		double effectFieldZ2 = latticeParam[indexLattice].aniK2 * e23 * inner2;

		latticeParam[indexLattice].EnergyAnisotropyNormAxisK2 =
				-(effectFieldX2 * lattice[curIndex] + effectFieldY2 * lattice[curIndex + 1] +
				  effectFieldZ2 * lattice[curIndex + 2]);
		*EaniK2 += latticeParam[indexLattice].EnergyAnisotropyNormAxisK2;

		/* calculate effective field and energy of the third axis */
		double inner3 = lattice[curIndex] * e31
		                + lattice[curIndex + 1] * e32
		                + lattice[curIndex + 2] * e33;

		double effectFieldX3 = latticeParam[indexLattice].aniK3 * e31 * inner3;
		double effectFieldY3 = latticeParam[indexLattice].aniK3 * e32 * inner3;
		double effectFieldZ3 = latticeParam[indexLattice].aniK3 * e33 * inner3;

		latticeParam[indexLattice].EnergyAnisotropyNormAxisK3 =
				-(effectFieldX3 * lattice[curIndex] + effectFieldY3 * lattice[curIndex + 1] +
				  effectFieldZ3 * lattice[curIndex + 2]);
		*EaniK3 += latticeParam[indexLattice].EnergyAnisotropyNormAxisK3;

		/* add generated effective field to the total */
		*Hx += (effectFieldX1 + effectFieldX2 + effectFieldX3);
		*Hy += (effectFieldY1 + effectFieldY2 + effectFieldY3);
		*Hz += (effectFieldZ1 + effectFieldZ2 + effectFieldZ3);

	} else {

		/* add generated effective field to the total (reduced for flexible system) */
		*Hx += effectFieldX1;
		*Hy += effectFieldY1;
		*Hz += effectFieldZ1;

	}

}

void uniaxialAnisotropyForce(int i1, size_t curIndex, size_t indexLattice, double *Fx, double *Fy, double *Fz) {

	/* number of projections of all spins */
	size_t spinsNum = NUMBER_COMP_MAGN*userVars.N1*userVars.N2*userVars.N3;

	/***************************
	 * i1-th site
	 ***************************/
	double mxi = lattice[curIndex];
	double myi = lattice[curIndex + 1];
	double mzi = lattice[curIndex + 2];
	double xi  = lattice[curIndex + spinsNum];
	double yi  = lattice[curIndex + spinsNum + 1];
	double zi  = lattice[curIndex + spinsNum + 2];
	double txi = latticeParam[indexLattice].tx;
	double tyi = latticeParam[indexLattice].ty;
	double tzi = latticeParam[indexLattice].tz;
	double aniK1i = latticeParam[indexLattice].aniK1;
	if ((i1 == userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF)) {
		aniK1i = 0.0;
	}

	/***************************
     * (i1-1)-th site
     ***************************/
	if (i1 > 1) {
		curIndex = IDXmg(i1 - 1, 1, 1);
		indexLattice = IDX(i1 - 1, 1, 1);
	} else {
		curIndex = IDXmg(userVars.N1 - 2, 1, 1);
		indexLattice = IDX(userVars.N1 - 2, 1, 1);
	}
	double mxim1 = lattice[curIndex];
	double myim1 = lattice[curIndex + 1];
	double mzim1 = lattice[curIndex + 2];
	double xim1  = lattice[curIndex + spinsNum];
	double yim1  = lattice[curIndex + spinsNum + 1];
	double zim1  = lattice[curIndex + spinsNum + 2];
	double txim1 = latticeParam[indexLattice].tx;
	double tyim1 = latticeParam[indexLattice].ty;
	double tzim1 = latticeParam[indexLattice].tz;
	double aniK1im1 = latticeParam[indexLattice].aniK1;
	if (i1 == 1 && userVars.periodicBC1 == PBC_OFF) {
		aniK1im1 = 0.0;
	}

	/***************************
    * (i1+1)-th site
    ****************************/
	if (i1 < userVars.N1 - 2) {
		curIndex = IDXmg(i1 + 1, 1, 1);
		indexLattice = IDX(i1 + 1, 1, 1);
	} else {
		curIndex = IDXmg(1, 1, 1);
		indexLattice = IDX(1, 1, 1);
	}
	double xip1  = lattice[curIndex + spinsNum];
	double yip1  = lattice[curIndex + spinsNum + 1];
	double zip1  = lattice[curIndex + spinsNum + 2];

	/* derivatives */
	double dx = xi-xim1;
	double dy = yi-yim1;
	double dz = zi-zim1;
	double dr = sqrt( dx*dx + dy*dy + dz*dz );
	double dr3 = dr*dr*dr;
	double dtxim1_dx = 1.0/dr - dx*dx/dr3;
	double dtxim1_dy = -dx*dy/dr3;
	double dtxim1_dz = -dx*dz/dr3;
	double dtyim1_dx = dtxim1_dy;
	double dtyim1_dy = 1.0/dr - dy*dy/dr3;
	double dtyim1_dz = -dy*dz/dr3;
	double dtzim1_dx = dtxim1_dz;
	double dtzim1_dy = dtyim1_dz;
	double dtzim1_dz = 1.0/dr - dz*dz/dr3;
	dx = xip1-xi;
	dy = yip1-yi;
	dz = zip1-zi;
	dr = sqrt( dx*dx + dy*dy + dz*dz );
	dr3 = dr*dr*dr;
	double dtxi_dx = -1.0/dr + dx*dx/dr3;
	double dtxi_dy = dx*dy/dr3;
	double dtxi_dz = dx*dz/dr3;
	double dtyi_dx = dtxi_dy;
	double dtyi_dy = -1.0/dr + dy*dy/dr3;
	double dtyi_dz = dy*dz/dr3;
	double dtzi_dx = dtxi_dz;
	double dtzi_dy = dtyi_dz;
	double dtzi_dz = -1.0/dr + dz*dz/dr3;

	/* dot products */
	double aniKim1_mim1_dot_tim1 = -aniK1im1*(mxim1*txim1 + myim1*tyim1 + mzim1*tzim1);
	double aniKi_mi_dot_ti       = -aniK1i*  (mxi  *txi   + myi  *tyi   + mzi  *tzi);

	/* forces */
	*Fx -= (aniKim1_mim1_dot_tim1 * ( mxim1*dtxim1_dx + myim1*dtyim1_dx + mzim1*dtzim1_dx) +
			aniKi_mi_dot_ti       * ( mxi  *dtxi_dx   + myi  *dtyi_dx   + mzi  *dtzi_dx));
	*Fy -= (aniKim1_mim1_dot_tim1 * ( mxim1*dtxim1_dy + myim1*dtyim1_dy + mzim1*dtzim1_dy) +
			aniKi_mi_dot_ti       * ( mxi  *dtxi_dy   + myi  *dtyi_dy   + mzi  *dtzi_dy));
	*Fz -= (aniKim1_mim1_dot_tim1 * ( mxim1*dtxim1_dz + myim1*dtyim1_dz + mzim1*dtzim1_dz) +
			aniKi_mi_dot_ti       * ( mxi  *dtxi_dz   + myi  *dtyi_dz   + mzi  *dtzi_dz));

}

void uniaxialAnisotropyFieldTriangle(int i1, size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EaniK1, double *EaniK3) {

	/***************************
	* i-th site
	***************************/
	/* magnetization */
	double mxi = lattice[curIndex];
	double myi = lattice[curIndex + 1];
	double mzi = lattice[curIndex + 2];
	/* tangential vector of site */
	double nxi = latticeParam[indexLattice].trinx;
	double nyi = latticeParam[indexLattice].triny;
	double nzi = latticeParam[indexLattice].trinz;
	/* normal vector of site */
	double txi = latticeParam[indexLattice].tx;
	double tyi = latticeParam[indexLattice].ty;
	double tzi = latticeParam[indexLattice].tz;
	double aniK1_i = latticeParam[indexLattice].aniK1;
	double aniK3_i = latticeParam[indexLattice].aniK3;
	if ((i1 == userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF)) {
		aniK1_i = 0.0;
	}

	/* calculate effective field and energy of the first axis (main direction) */
	double inner1 = mxi * txi
					+ myi * tyi
					+ mzi * tzi;

	double effectFieldX1 = aniK1_i * txi * inner1;
	double effectFieldY1 = aniK1_i * tyi * inner1;
	double effectFieldZ1 = aniK1_i * tzi * inner1;

	latticeParam[indexLattice].EnergyAnisotropyNormAxisK1 =
			-(effectFieldX1*lattice[curIndex] + effectFieldY1*lattice[curIndex+1] + effectFieldZ1*lattice[curIndex+2]);
	*EaniK1 += latticeParam[indexLattice].EnergyAnisotropyNormAxisK1;

	/* calculate effective field and energy of the third axis (normal vector) */
	double inner3 = mxi * nxi
					+ myi * nyi
					+ mzi * nzi;

	double effectFieldX3 = aniK3_i * nxi * inner3;
	double effectFieldY3 = aniK3_i * nyi * inner3;
	double effectFieldZ3 = aniK3_i * nzi * inner3;

	latticeParam[indexLattice].EnergyAnisotropyNormAxisK3 =
			-(effectFieldX3*lattice[curIndex] + effectFieldY3*lattice[curIndex+1] + effectFieldZ3*lattice[curIndex+2]);
	*EaniK3 += latticeParam[indexLattice].EnergyAnisotropyNormAxisK3;

	/* add generated effective field to the total */
	*Hx += (effectFieldX1 + effectFieldX3);
	*Hy += (effectFieldY1 + effectFieldY3);
	*Hz += (effectFieldZ1 + effectFieldZ3);

}

void uniaxialAnisotropyTangForceTriangle(int i1, int i2, size_t curIndex, size_t indexLattice, double *Fx, double *Fy, double *Fz) {
	
	/* number of projections of all spins */
	size_t spinsNum = NUMBER_COMP_MAGN*userVars.N1*userVars.N2*userVars.N3;
	
	/***************************
    * i-th site
    ***************************/
    /* magnetization */
	double mxi = lattice[curIndex];
	double myi = lattice[curIndex + 1];
	double mzi = lattice[curIndex + 2];
	/* coordinates */
	double xi = lattice[curIndex + spinsNum];
	double yi = lattice[curIndex + spinsNum + 1];
	double zi = lattice[curIndex + spinsNum + 2];
	/* tangential vector components */
	double txi = latticeParam[indexLattice].tx;
	double tyi = latticeParam[indexLattice].ty;
	double tzi = latticeParam[indexLattice].tz;
	/* anisotropy constant */
	double aniK1_i = latticeParam[indexLattice].aniK1;
	if ((i1 == userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF)) {
		aniK1_i = 0.0;
	}
	
    /***************************
    * 1-st neighbouring site
    ***************************/
    if (i1 < userVars.N1 - 2)  {
        indexLattice = IDXtr1(i1, i2);
        curIndex = IDXtrmg1(i1, i2);
    } else {
        indexLattice = IDXtr(1, i2);
        curIndex = IDXtrmg(1, i2);
    }
    /* coordinates */
	double x1 = lattice[curIndex + spinsNum];
	double y1 = lattice[curIndex + spinsNum + 1];
	double z1 = lattice[curIndex + spinsNum + 2];
    
    /***************************
    * 4-th neighbouring site
    ***************************/
    if (i1 > 1 ) {
        curIndex = IDXtrmg4(i1, i2);
        indexLattice = IDXtr4(i1, i2);
    } else {
        curIndex = IDXtrmg(userVars.N1 - 2, i2);
        indexLattice = IDXtr(userVars.N1 - 2, i2);
    }
    /* magnetization */
	double mx4 = lattice[curIndex];
	double my4 = lattice[curIndex + 1];
	double mz4 = lattice[curIndex + 2];
    /* coordinates */
	double x4 = lattice[curIndex + spinsNum];
	double y4 = lattice[curIndex + spinsNum + 1];
	double z4 = lattice[curIndex + spinsNum + 2];
	/* tangential vector components */
	double tx4 = latticeParam[indexLattice].tx;
	double ty4 = latticeParam[indexLattice].ty;
	double tz4 = latticeParam[indexLattice].tz;
    /* coefficient of bulk DMI */
    double aniK1_4 = latticeParam[indexLattice].aniK1;
	if ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		aniK1_4 = 0.0;
	}
	
	/* dot products */
	double ki_mi_dot_ti = - aniK1_i * (mxi * txi + myi * tyi + mzi * tzi);
	double k4_m4_dot_t4 = - aniK1_4 * (mx4 * tx4 + my4 * ty4 + mz4 * tz4);
	
	/* distance  r1 - ri */
	double xi1 = x1 - xi;
	double yi1 = y1 - yi;
	double zi1 = z1 - zi;
	/* distance  ri - r4 */
	double x4i = xi - x4;
	double y4i = yi - y4;
	double z4i = zi - z4;
	/* normalization */
	double ri1 = sqrt(xi1 * xi1 + yi1 *yi1 + zi1 *zi1);
	double r4i = sqrt(x4i * x4i + y4i *y4i + z4i *z4i);
	double ri1_3 = ri1 * ri1 * ri1;
	double r4i_3 = r4i * r4i * r4i;
	
	/* derivative of ti vector with respect to xi */
	double Dtxi_Dx = -(yi1 * yi1 + zi1 * zi1) / ri1_3;
	double Dtyi_Dx = xi1 * yi1 / ri1_3;
	double Dtzi_Dx = xi1 * zi1 / ri1_3;
	/* derivative of ti vector with respect to yi */
	double Dtxi_Dy = yi1 * xi1 / ri1_3;
	double Dtyi_Dy = -(xi1 * xi1 + zi1 * zi1) / ri1_3;
	double Dtzi_Dy = yi1 * zi1 / ri1_3;
	/* derivative of ti vector with respect to zi */
	double Dtxi_Dz = zi1 * xi1 / ri1_3;
	double Dtyi_Dz = zi1 * yi1 / ri1_3;
	double Dtzi_Dz = -(xi1 * xi1 + yi1 * yi1) / ri1_3;
	
	/* derivative of t4 vector with respect to xi */
	double Dtx4_Dx = (y4i * y4i + z4i * z4i) / r4i_3;
	double Dty4_Dx = - x4i * y4i / r4i_3;
	double Dtz4_Dx = - x4i * z4i / r4i_3;
	/* derivative of t4 vector with respect to yi */
	double Dtx4_Dy = - y4i * x4i / r4i_3;
	double Dty4_Dy = (x4i * x4i + z4i * z4i) / r4i_3;
	double Dtz4_Dy = - y4i * z4i / r4i_3;
	/* derivative of t4 vector with respect to zi */
	double Dtx4_Dz = - z4i * x4i / r4i_3;
	double Dty4_Dz = - z4i * y4i / r4i_3;
	double Dtz4_Dz = (x4i * x4i + y4i * y4i) / r4i_3;
	
	/* forces */
	*Fx -= (ki_mi_dot_ti * (mxi * Dtxi_Dx + myi * Dtyi_Dx + mzi * Dtzi_Dx) +
			k4_m4_dot_t4 * (mx4 * Dtx4_Dx + my4 * Dty4_Dx + mz4 * Dtz4_Dx)
		   );
	*Fy -= (ki_mi_dot_ti * (mxi * Dtxi_Dy + myi * Dtyi_Dy + mzi * Dtzi_Dy) +
			k4_m4_dot_t4 * (mx4 * Dtx4_Dy + my4 * Dty4_Dy + mz4 * Dtz4_Dy)
		   );
	*Fz -= (ki_mi_dot_ti * (mxi * Dtxi_Dz + myi * Dtyi_Dz + mzi * Dtzi_Dz) +
			k4_m4_dot_t4 * (mx4 * Dtx4_Dz + my4 * Dty4_Dz + mz4 * Dtz4_Dz)
		   );

}

/* function to calculate normal vectors and partial derivatives of this normal vectors
 * using id of coordinates for gradient */
void calculateNormalVectors(int i1, int i2, int id, double *nxNorm, double *nyNorm, double *nzNorm,
							double *nxNormDx, double *nxNormDy, double *nxNormDz,
							double *nyNormDx, double *nyNormDy, double *nyNormDz,
							double *nzNormDx, double *nzNormDy, double *nzNormDz) {

	/* check indexes */
	if ((i1 == forbIndex) && (i2 == forbIndex)) {
		return;
	}

	/* number of maximum indexes horizontally and vertically */
	int N1 = userVars.N1 - 2;
	int N2 = userVars.N2 - 2;

	/* switches of triangles */
	double k1 = 1.0, k2 = 1.0, k3 = 1.0, k4 = 1.0, k5 = 1.0, k6 = 1.0;

	/* periodic conditions for switches */

	/* the first switch */
	if ((i1 == N1) && (userVars.periodicBC1 == PBC_OFF)) {
		k1 = 0.0;
	}
	if (i2 == N2) {
		k1 = 0.0;
	}

	/* the second switch */
	if (i2 == N2) {
		k2 = 0.0;
	}
	if ((i1 == N1) && (i2 % 2 == 0) && (userVars.periodicBC1 == PBC_OFF)) {
		k2 = 0.0;
	}
	if ((i1 == 1) && (i2 % 2 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		k2 = 0.0;
	}

	/* the third switch */
	if (i2 == N2) {
		k3 = 0.0;
	}
	if ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		k3 = 0.0;
	}

	/* the fourth switch */
	if ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		k4 = 0.0;
	}
	if (i2 == 1) {
		k4 = 0.0;
	}

	/* the fifth switch */
	if (i2 == 1) {
		k5 = 0.0;
	}
	if ((i1 == 1) && (i2 % 2 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		k5 = 0.0;
	}
	if ((i1 == N1) && (i2 % 2 == 0) && (userVars.periodicBC1 == PBC_OFF)) {
		k5 = 0.0;
	}

	/* the sixth switch */
	if (i2 == 1) {
		k6 = 0.0;
	}
	if ((i1 == N1) && (userVars.periodicBC1 == PBC_OFF)) {
		k6 = 0.0;
	}

	// to debug code
	//fprintf(stderr,"%d %d %1.1f %1.1f %1.1f %1.1f %1.1f %1.1f\n", i1, i2, k1, k2, k3, k4, k5, k6);

	/* find indexes of neighbour sites (coordinates) */
	int indexLattice0 = IDXtrmg(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLattice1 = IDXtrmg1(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLattice2 = IDXtrmg2(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLattice3 = IDXtrmg3(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLattice4 = IDXtrmg4(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLattice5 = IDXtrmg5(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLattice6 = IDXtrmg6(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* find coordinates of neighbour sites */

	/* the current neighbour */
	double x0 = lattice[indexLattice0];
	double y0 = lattice[indexLattice0 + 1];
	double z0 = lattice[indexLattice0 + 2];

	/* the first neighbour */
	double x1 = lattice[indexLattice1];
	double y1 = lattice[indexLattice1 + 1];
	double z1 = lattice[indexLattice1 + 2];

	/* the second neighbour */
	double x2 = lattice[indexLattice2];
	double y2 = lattice[indexLattice2 + 1];
	double z2 = lattice[indexLattice2 + 2];

	/* the third neighbour */
	double x3 = lattice[indexLattice3];
	double y3 = lattice[indexLattice3 + 1];
	double z3 = lattice[indexLattice3 + 2];

	/* the fourth neighbour */
	double x4 = lattice[indexLattice4];
	double y4 = lattice[indexLattice4 + 1];
	double z4 = lattice[indexLattice4 + 2];

	/* the fifth neighbour */
	double x5 = lattice[indexLattice5];
	double y5 = lattice[indexLattice5 + 1];
	double z5 = lattice[indexLattice5 + 2];

	/* the sixth neighbour */
	double x6 = lattice[indexLattice6];
	double y6 = lattice[indexLattice6 + 1];
	double z6 = lattice[indexLattice6 + 2];

	/* calculate components of normal vectors without normalization */

	/* component along x-axis */
	double nx = k1 * (y1 * z2 - z1 * y2 - y1 * z0 + z1 * y0 - y0 * z2 + z0 * y2) +
				k2 * (y2 * z3 - z2 * y3 - y2 * z0 + z2 * y0 - y0 * z3 + z0 * y3) +
				k3 * (y3 * z4 - z3 * y4 - y3 * z0 + z3 * y0 - y0 * z4 + z0 * y4) +
				k4 * (y4 * z5 - z4 * y5 - y4 * z0 + z4 * y0 - y0 * z5 + z0 * y5) +
				k5 * (y5 * z6 - z5 * y6 - y5 * z0 + z5 * y0 - y0 * z6 + z0 * y6) +
				k6 * (y6 * z1 - z6 * y1 - y6 * z0 + z6 * y0 - y0 * z1 + z0 * y1);

	/* component along y-axis */
	double ny = k1 * (z1 * x2 - x1 * z2 - z1 * x0 + x1 * z0 - z0 * x2 + x0 * z2) +
				k2 * (z2 * x3 - x2 * z3 - z2 * x0 + x2 * z0 - z0 * x3 + x0 * z3) +
				k3 * (z3 * x4 - x3 * z4 - z3 * x0 + x3 * z0 - z0 * x4 + x0 * z4) +
				k4 * (z4 * x5 - x4 * z5 - z4 * x0 + x4 * z0 - z0 * x5 + x0 * z5) +
				k5 * (z5 * x6 - x5 * z6 - z5 * x0 + x5 * z0 - z0 * x6 + x0 * z6) +
				k6 * (z6 * x1 - x6 * z1 - z6 * x0 + x6 * z0 - z0 * x1 + x0 * z1);

	/* component along z-axis */
	double nz = k1 * (x1 * y2 - y1 * x2 - x1 * y0 + y1 * x0 - x0 * y2 + y0 * x2) +
				k2 * (x2 * y3 - y2 * x3 - x2 * y0 + y2 * x0 - x0 * y3 + y0 * x3) +
				k3 * (x3 * y4 - y3 * x4 - x3 * y0 + y3 * x0 - x0 * y4 + y0 * x4) +
				k4 * (x4 * y5 - y4 * x5 - x4 * y0 + y4 * x0 - x0 * y5 + y0 * x5) +
				k5 * (x5 * y6 - y5 * x6 - x5 * y0 + y5 * x0 - x0 * y6 + y0 * x6) +
				k6 * (x6 * y1 - y6 * x1 - x6 * y0 + y6 * x0 - x0 * y1 + y0 * x1);

	/* calculate length of normal vector */
	double length = sqrt(nx * nx + ny * ny + nz * nz);
	double length3 = length * length * length;

	/* normalize normal vector and save result */
	*nxNorm = nx / length;
	*nyNorm = ny / length;
	*nzNorm = nz / length;

	/* declaration of partial derivatives of normal vector without normalization */
	double nxDx, nxDy = 0.0, nxDz = 0.0;
	double nyDx = 0.0, nyDy, nyDz = 0.0;
	double nzDx = 0.0, nzDy = 0.0, nzDz;

	/* calculate partial derivatives of normal vector without normalization
	 * using id of coordinates for gradient */

	/* component of normal vector along x-axis with different coordinates */

	/* partial derivative of x-coordinate */
	nxDx = 0.0;
	/* partial derivative of y-coordinate */
	if (id == id1) {
		nxDy = k1 * (z2 - z0) + k6 * (-z6 + z0);
	} else if (id == id2) {
		nxDy = k1 * (-z1 + z0) + k2 * (z3 - z0);
	} else if (id == id3) {
		nxDy = k2 * (-z2 + z0) + k3 * (z4 - z0);
	} else if (id == id4) {
		nxDy = k3 * (-z3 + z0) + k4 * (z5 - z0);
	} else if (id == id5) {
		nxDy = k4 * (-z4 + z0) + k5 * (z6 - z0);
	} else if (id == id6) {
		nxDy = k5 * (-z5 + z0) + k6 * (z1 - z0);
	} else if (id == id0) {
		nxDy = k1 * (z1 - z2) + k2 * (z2 - z3) + k3 * (z3 - z4) + k4 * (z4 -z5) + k5 * (z5 - z6) + k6 * (z6 - z1);
	}
	/* partial derivative of z-coordinate */
	if (id == id1) {
		nxDz = k1 * (-y2 + y0) + k6 * (y6 - y0);
	} else if (id == id2) {
		nxDz = k1 * (y1 - y0) + k2 * (-y3 + y0);
	} else if (id == id3) {
		nxDz = k2 * (y2 - y0) + k3 * (-y4 + y0);
	} else if (id == id4) {
		nxDz = k3 * (y3 - y0) + k4 * (-y5 + y0);
	} else if (id == id5) {
		nxDz = k4 * (y4 - y0) + k5 * (-y6 + y0);
	} else if (id == id6) {
		nxDz = k5 * (y5 - y0) + k6 * (-y1 + y0);
	} else if (id == id0) {
		nxDz = k1 * (-y1 + y2) + k2 * (-y2 + y3) + k3 * (-y3 + y4) + k4 * (-y4 + y5)  + k5 * (-y5 + y6) + k6 * (-y6 + y1);
	}

	/* component of normal vector along y-axis with different coordinates */

	/* partial derivative of x-coordinate */
	if (id == id1) {
		nyDx = k1 * (-z2 + z0) + k6 * (z6 - z0);
	} else if (id == id2) {
		nyDx = k1 * (z1 - z0) + k2 * (-z3 + z0);
	} else if (id == id3) {
		nyDx = k2 * (z2 - z0) + k3 * (-z4 + z0);
	} else if (id == id4) {
		nyDx = k3 * (z3 - z0) + k4 * (-z5 + z0);
	} else if (id == id5) {
		nyDx = k4 * (z4 - z0) + k5 * (-z6 + z0);
	} else if (id == id6) {
		nyDx = k5 * (z5 - z0) + k6 * (-z1 + z0);
	} else if (id == id0) {
		nyDx = k1 * (-z1 + z2) + k2 * (-z2 + z3) + k3 * (-z3 + z4) + k4 * (-z4 + z5) + k5 * (-z5 + z6) + k6 * (-z6 + z1);
	}
	/* partial derivative of y-coordinate */
	nyDy = 0.0;
	/* partial derivative of z-coordinate */
	if (id == id1) {
		nyDz = k1 * (x2 - x0) + k6 * (-x6 + x0);
	} else if (id == id2) {
		nyDz = k1 * (-x1 + x0) + k2 * (x3 - x0);
	} else if (id == id3) {
		nyDz = k2 * (-x2 + x0) + k3 * (x4 - x0);
	} else if (id == id4) {
		nyDz = k3 * (-x3 + x0) + k4 * (x5 - x0);
	} else if (id == id5) {
		nyDz = k4 * (-x4 + x0) + k5 * (x6 - x0);
	} else if (id == id6) {
		nyDz = k5 * (-x5 + x0) + k6 * (x1 - x0);
	} else if (id == id0) {
		nyDz = k1 * (x1 - x2) + k2 * (x2 - x3) + k3 * (x3 - x4) + k4 * (x4 - x5) + k5 * (x5 - x6) + k6 * (x6 - x1);
	}

	/* component of normal vector along z-axis with different coordinates */

	/* partial derivative of x-coordinate */
	if (id == id1) {
		nzDx = k1 * (y2 - y0) + k6 * (-y6 + y0);
	} else if (id == id2) {
		nzDx = k1 * (-y1 + y0) + k2 * (y3 - y0);
	} else if (id == id3) {
		nzDx = k2 * (-y2 + y0) + k3 * (y4 - y0);
	} else if (id == id4) {
		nzDx = k3 * (-y3 + y0) + k4 * (y5 - y0);
	} else if (id == id5) {
		nzDx = k4 * (-y4 + y0) + k5 * (y6 - y0);
	} else if (id == id6) {
		nzDx = k5 * (-y5 + y0) + k6 * (y1 - y0);
	} else if (id == id0) {
		nzDx = k1 * (y1 - y2) + k2 * (y2 - y3) + k3 * (y3 - y4) + k4 * (y4 - y5) + k5 * (y5 - y6) + k6 * (y6 - y1);
	}
	/* partial derivative of y-coordinate */
	if (id == id1) {
		nzDy = k1 * (-x2 + x0) + k6 * (x6 - x0);
	} else if (id == id2) {
		nzDy = k1 * (x1 - x0) + k2 * (-x3 + x0);
	} else if (id == id3) {
		nzDy = k2 * (x2 - x0) + k3 * (-x4 + x0);
	} else if (id == id4) {
		nzDy = k3 * (x3 - x0) + k4 * (-x5 + x0);
	} else if (id == id5) {
		nzDy = k4 * (x4 - x0) + k5 * (-x6 + x0);
	} else if (id == id6) {
		nzDy = k5 * (x5 - x0) + k6 * (-x1 + x0);
	} else if (id == id0) {
		nzDy = k1 * (-x1 + x2) + k2 * (-x2 + x3) + k3 * (-x3 + x4) + k4 * (-x4 + x5) + k5 * (-x5 + x6) + k6 * (-x6 + x1);
	}
	/* partial derivative of z-coordinate */
	nzDz = 0.0;

	/* calculate partial derivatives of normal vector with normalization and save this result */

	/* component of normal vector along x-axis with different coordinates */
	*nxNormDx = (nxDx * (ny * ny + nz * nz) - nx * (ny * nyDx + nz * nzDx)) / length3;
	*nxNormDy = (nxDy * (ny * ny + nz * nz) - nx * (ny * nyDy + nz * nzDy)) / length3;
	*nxNormDz = (nxDz * (ny * ny + nz * nz) - nx * (ny * nyDz + nz * nzDz)) / length3;

	/* component of normal vector along y-axis with different coordinates */
	*nyNormDx = (nyDx * (nx * nx + nz * nz) - ny * (nx * nxDx + nz * nzDx)) / length3;
	*nyNormDy = (nyDy * (nx * nx + nz * nz) - ny * (nx * nxDy + nz * nzDy)) / length3;
	*nyNormDz = (nyDz * (nx * nx + nz * nz) - ny * (nx * nxDz + nz * nzDz)) / length3;

	/* component of normal vector along y-axis with different coordinates */
	*nzNormDx = (nzDx * (nx * nx + ny * ny) - nz * (nx * nxDx + ny * nyDx)) / length3;
	*nzNormDy = (nzDy * (nx * nx + ny * ny) - nz * (nx * nxDy + ny * nyDy)) / length3;
	*nzNormDz = (nzDz * (nx * nx + ny * ny) - nz * (nx * nxDz + ny * nyDz)) / length3;

	//to debug code
	//fprintf(stderr, "%d %d %1.1f %1.1f %1.1f %1.1f %1.1f %1.1f %f %f %f \n", i1, i2, k1, k2, k3, k4, k5, k6, x1, y1, z1);

}

void uniaxialAnisotropyNormForceTriangle(int i1, int i2, double *Fx, double *Fy, double *Fz) {

	/* indexes of neighbours */
	int neighI1, neighJ1;
	int neighI2, neighJ2;
	int neighI3, neighJ3;
	int neighI4, neighJ4;
	int neighI5, neighJ5;
	int neighI6, neighJ6;

	/* number of maximum indexes horizontally and vertically */
	int N1 = userVars.N1 - 2;
	int N2 = userVars.N2 - 2;

	/* calculate indexes of neighbours */

	/* the first neighbour */
	if (i1 != N1) {
		neighI1 = i1 + 1; neighJ1 = i2;
	} else if (userVars.periodicBC1 == PBC_ON) {
		neighI1 = 1; neighJ1 = i2;
	} else {
		neighI1 = forbIndex; neighJ1 = forbIndex;
	}

	/* the second neighbour */
	if (i1 != N1) {
		if (i2 % 2 == 1) {
			neighI2 = i1; neighJ2 = i2 + 1;
		} else {
			neighI2 = i1 + 1; neighJ2 = i2 + 1;
		}
	} else {
		if (i2 % 2 == 1) {
			neighI2 = i1; neighJ2 = i2 + 1;
		} else if (userVars.periodicBC1 == PBC_ON) {
			neighI2 = 1; neighJ2 = i2 + 1;
		} else {
			neighI2 = forbIndex; neighJ2 = forbIndex;
		}
	}

	/* the third neighbour */
	if (i1 != 1) {
		if (i2 % 2 == 1) {
			neighI3 = i1 - 1; neighJ3 = i2 + 1;
		} else {
			neighI3 = i1; neighJ3 = i2 + 1;
		}
	} else {
		if (i2 % 2 == 0) {
			neighI3 = i1; neighJ3 = i2 + 1;
		} else if (userVars.periodicBC1 == PBC_ON) {
			neighI3 = N1; neighJ3 = i2 + 1;
		} else {
			neighI3 = forbIndex; neighJ3 = forbIndex;
		}
	}

	/* the fourth neighbour */
	if (i1 != 1) {
		neighI4 = i1 - 1; neighJ4 = i2;
	} else if (userVars.periodicBC1 == PBC_ON) {
		neighI4 = N1; neighJ4 = i2;
	} else {
		neighI4 = forbIndex; neighJ4 = forbIndex;
	}

	/* the fifth neighbour */
	if (i1 != 1) {
		if (i2 % 2 == 1) {
			neighI5 = i1 - 1; neighJ5 = i2 - 1;
		} else {
			neighI5 = i1; neighJ5 = i2 - 1;
		}
	} else {
		if (i2 % 2 == 0) {
			neighI5 = i1; neighJ5 = i2 - 1;
		} else if (userVars.periodicBC1 == PBC_ON) {
			neighI5 = N1; neighJ5 = i2 - 1;
		} else {
			neighI5 = forbIndex; neighJ5 = forbIndex;
		}
	}

	/* the sixth neighbour */
	if (i1 != N1) {
		if (i2 % 2 == 1) {
			neighI6 = i1; neighJ6 = i2 - 1;
		} else {
			neighI6 = i1 + 1; neighJ6 = i2 - 1;
		}
	} else {
		if (i2 % 2 == 1) {
			neighI6 = i1; neighJ6 = i2 - 1;
		} else if (userVars.periodicBC1 == PBC_ON) {
			neighI6 = 1; neighJ6 = i2 - 1;
		} else {
			neighI6 = forbIndex; neighJ6 = forbIndex;
		}
	}

	/* declaration of normal vectors */
	double nx0 = 0.0, ny0 = 0.0, nz0 = 0.0;
	double nx1 = 0.0, ny1 = 0.0, nz1 = 0.0;
	double nx2 = 0.0, ny2 = 0.0, nz2 = 0.0;
	double nx3 = 0.0, ny3 = 0.0, nz3 = 0.0;
	double nx4 = 0.0, ny4 = 0.0, nz4 = 0.0;
	double nx5 = 0.0, ny5 = 0.0, nz5 = 0.0;
	double nx6 = 0.0, ny6 = 0.0, nz6 = 0.0;

	/* declaration of partial derivative for normal vectors */
	double nx0Dx = 0.0, nx0Dy = 0.0, nx0Dz = 0.0, ny0Dx = 0.0, ny0Dy = 0.0, ny0Dz = 0.0, nz0Dx = 0.0, nz0Dy = 0.0, nz0Dz = 0.0;
	double nx1Dx = 0.0, nx1Dy = 0.0, nx1Dz = 0.0, ny1Dx = 0.0, ny1Dy = 0.0, ny1Dz = 0.0, nz1Dx = 0.0, nz1Dy = 0.0, nz1Dz = 0.0;
	double nx2Dx = 0.0, nx2Dy = 0.0, nx2Dz = 0.0, ny2Dx = 0.0, ny2Dy = 0.0, ny2Dz = 0.0, nz2Dx = 0.0, nz2Dy = 0.0, nz2Dz = 0.0;
	double nx3Dx = 0.0, nx3Dy = 0.0, nx3Dz = 0.0, ny3Dx = 0.0, ny3Dy = 0.0, ny3Dz = 0.0, nz3Dx = 0.0, nz3Dy = 0.0, nz3Dz = 0.0;
	double nx4Dx = 0.0, nx4Dy = 0.0, nx4Dz = 0.0, ny4Dx = 0.0, ny4Dy = 0.0, ny4Dz = 0.0, nz4Dx = 0.0, nz4Dy = 0.0, nz4Dz = 0.0;
	double nx5Dx = 0.0, nx5Dy = 0.0, nx5Dz = 0.0, ny5Dx = 0.0, ny5Dy = 0.0, ny5Dz = 0.0, nz5Dx = 0.0, nz5Dy = 0.0, nz5Dz = 0.0;
	double nx6Dx = 0.0, nx6Dy = 0.0, nx6Dz = 0.0, ny6Dx = 0.0, ny6Dy = 0.0, ny6Dz = 0.0, nz6Dx = 0.0, nz6Dy = 0.0, nz6Dz = 0.0;

	//fprintf(stderr, "%d %d\n", i1, i2);

	/* calculate normal vectors and partial derivatives according to boundary condition */
	if (i2 == 1) {
		calculateNormalVectors(i1, i2, id0, &nx0, &ny0, &nz0, &nx0Dx, &nx0Dy, &nx0Dz,
				               &ny0Dx, &ny0Dy, &ny0Dz, &nz0Dx, &nz0Dy, &nz0Dz);
		calculateNormalVectors(neighI1, neighJ1, id4, &nx1, &ny1, &nz1, &nx1Dx, &nx1Dy, &nx1Dz,
							   &ny1Dx, &ny1Dy, &ny1Dz, &nz1Dx, &nz1Dy, &nz1Dz);
		calculateNormalVectors(neighI2, neighJ2, id5, &nx2, &ny2, &nz2, &nx2Dx, &nx2Dy, &nx2Dz,
							   &ny2Dx, &ny2Dy, &ny2Dz, &nz2Dx, &nz2Dy, &nz2Dz);
		calculateNormalVectors(neighI3, neighJ3, id6, &nx3, &ny3, &nz3, &nx3Dx, &nx3Dy, &nx3Dz,
							   &ny3Dx, &ny3Dy, &ny3Dz, &nz3Dx, &nz3Dy, &nz3Dz);
		calculateNormalVectors(neighI4, neighJ4, id1, &nx4, &ny4, &nz4, &nx4Dx, &nx4Dy, &nx4Dz,
							   &ny4Dx, &ny4Dy, &ny4Dz, &nz4Dx, &nz4Dy, &nz4Dz);
	} else if (i2 == N2) {
		calculateNormalVectors(i1, i2, id0, &nx0, &ny0, &nz0, &nx0Dx, &nx0Dy, &nx0Dz,
							   &ny0Dx, &ny0Dy, &ny0Dz, &nz0Dx, &nz0Dy, &nz0Dz);
		calculateNormalVectors(neighI1, neighJ1, id4, &nx1, &ny1, &nz1, &nx1Dx, &nx1Dy, &nx1Dz,
							   &ny1Dx, &ny1Dy, &ny1Dz, &nz1Dx, &nz1Dy, &nz1Dz);
		calculateNormalVectors(neighI4, neighJ4, id1, &nx4, &ny4, &nz4, &nx4Dx, &nx4Dy, &nx4Dz,
							   &ny4Dx, &ny4Dy, &ny4Dz, &nz4Dx, &nz4Dy, &nz4Dz);
		calculateNormalVectors(neighI5, neighJ5, id2, &nx5, &ny5, &nz5, &nx5Dx, &nx5Dy, &nx5Dz,
							   &ny5Dx, &ny5Dy, &ny5Dz, &nz5Dx, &nz5Dy, &nz5Dz);
		calculateNormalVectors(neighI6, neighJ6, id3, &nx6, &ny6, &nz6, &nx6Dx, &nx6Dy, &nx6Dz,
							   &ny6Dx, &ny6Dy, &ny6Dz, &nz6Dx, &nz6Dy, &nz6Dz);
	} else {
		calculateNormalVectors(i1, i2, id0, &nx0, &ny0, &nz0, &nx0Dx, &nx0Dy, &nx0Dz,
							   &ny0Dx, &ny0Dy, &ny0Dz, &nz0Dx, &nz0Dy, &nz0Dz);
		calculateNormalVectors(neighI1, neighJ1, id4,  &nx1, &ny1, &nz1, &nx1Dx, &nx1Dy, &nx1Dz,
							   &ny1Dx, &ny1Dy, &ny1Dz, &nz1Dx, &nz1Dy, &nz1Dz);
		calculateNormalVectors(neighI2, neighJ2, id5, &nx2, &ny2, &nz2, &nx2Dx, &nx2Dy, &nx2Dz,
							   &ny2Dx, &ny2Dy, &ny2Dz, &nz2Dx, &nz2Dy, &nz2Dz);
		calculateNormalVectors(neighI3, neighJ3, id6, &nx3, &ny3, &nz3, &nx3Dx, &nx3Dy, &nx3Dz,
							   &ny3Dx, &ny3Dy, &ny3Dz, &nz3Dx, &nz3Dy, &nz3Dz);
		calculateNormalVectors(neighI4, neighJ4, id1, &nx4, &ny4, &nz4, &nx4Dx, &nx4Dy, &nx4Dz,
							   &ny4Dx, &ny4Dy, &ny4Dz, &nz4Dx, &nz4Dy, &nz4Dz);
		calculateNormalVectors(neighI5, neighJ5, id2, &nx5, &ny5, &nz5, &nx5Dx, &nx5Dy, &nx5Dz,
							   &ny5Dx, &ny5Dy, &ny5Dz, &nz5Dx, &nz5Dy, &nz5Dz);
		calculateNormalVectors(neighI6, neighJ6, id3, &nx6, &ny6, &nz6, &nx6Dx, &nx6Dy, &nx6Dz,
							   &ny6Dx, &ny6Dy, &ny6Dz, &nz6Dx, &nz6Dy, &nz6Dz);
	}

	/* find indexes of neighbour sites in array "latticeParam" and "lattice" */

	/* "latticeParam" */
	int indexLattice0 = IDXtr(i1, i2);
	int indexLattice1 = IDXtr1(i1, i2);
	int indexLattice2 = IDXtr2(i1, i2);
	int indexLattice3 = IDXtr3(i1, i2);
	int indexLattice4 = IDXtr4(i1, i2);
	int indexLattice5 = IDXtr5(i1, i2);
	int indexLattice6 = IDXtr6(i1, i2);

	/* "lattice" */
	int curIndex0 = IDXtrmg(i1, i2);
	int curIndex1 = IDXtrmg1(i1, i2);
	int curIndex2 = IDXtrmg2(i1, i2);
	int curIndex3 = IDXtrmg3(i1, i2);
	int curIndex4 = IDXtrmg4(i1, i2);
	int curIndex5 = IDXtrmg5(i1, i2);
	int curIndex6 = IDXtrmg6(i1, i2);

	/* find coefficients of anisotropy and components of magnetic moments for sites */

	/* zero site */
	double aniK0 = latticeParam[indexLattice0].aniK3;
	double mx0 = lattice[curIndex0];
	double my0 = lattice[curIndex0 + 1];
	double mz0 = lattice[curIndex0 + 2];

	/* the first site */
	double aniK1 = latticeParam[indexLattice1].aniK3;
	double mx1 = lattice[curIndex1];
	double my1 = lattice[curIndex1 + 1];
	double mz1 = lattice[curIndex1 + 2];
	if ((neighI1 == forbIndex) && (neighJ1 == forbIndex)) {
		aniK1 = 0.0;
		mx1 = 0.0;
		my1 = 0.0;
		mz1 = 0.0;
	}

	/* the second site */
	double aniK2 = latticeParam[indexLattice2].aniK3;
	double mx2 = lattice[curIndex2];
	double my2 = lattice[curIndex2 + 1];
	double mz2 = lattice[curIndex2 + 2];
	if ((neighI2 == forbIndex) && (neighJ2 == forbIndex)) {
		aniK2 = 0.0;
		mx2 = 0.0;
		my2 = 0.0;
		mz2 = 0.0;
	}

	/* the third site */
	double aniK3 = latticeParam[indexLattice3].aniK3;
	double mx3 = lattice[curIndex3];
	double my3 = lattice[curIndex3 + 1];
	double mz3 = lattice[curIndex3 + 2];
	if ((neighI3 == forbIndex) && (neighJ3 == forbIndex)) {
		aniK3 = 0.0;
		mx3 = 0.0;
		my3 = 0.0;
		mz3 = 0.0;
	}

	/* the fourth site */
	double aniK4 = latticeParam[indexLattice4].aniK3;
	double mx4 = lattice[curIndex4];
	double my4 = lattice[curIndex4 + 1];
	double mz4 = lattice[curIndex4 + 2];
	if ((neighI4 == forbIndex) && (neighJ4 == forbIndex)) {
		aniK4 = 0.0;
		mx4 = 0.0;
		my4 = 0.0;
		mz4 = 0.0;
	}

	/* the fifth site */
	double aniK5 = latticeParam[indexLattice5].aniK3;
	double mx5 = lattice[curIndex5];
	double my5 = lattice[curIndex5 + 1];
	double mz5 = lattice[curIndex5 + 2];
	if ((neighI5 == forbIndex) && (neighJ5 == forbIndex)) {
		aniK5 = 0.0;
		mx5 = 0.0;
		my5 = 0.0;
		mz5 = 0.0;
	}

	/* the sixth site */
	double aniK6 = latticeParam[indexLattice6].aniK3;
	double mx6 = lattice[curIndex6];
	double my6 = lattice[curIndex6 + 1];
	double mz6 = lattice[curIndex6 + 2];
	if ((neighI6 == forbIndex) && (neighJ6 == forbIndex)) {
		aniK6 = 0.0;
		mx6 = 0.0;
		my6 = 0.0;
		mz6 = 0.0;
	}

	/* calculate intermediate values for components of forces */
	double interValue0 = mx0 * nx0 + my0 * ny0 + mz0 * nz0;
	double interValue1 = mx1 * nx1 + my1 * ny1 + mz1 * nz1;
	double interValue2 = mx2 * nx2 + my2 * ny2 + mz2 * nz2;
	double interValue3 = mx3 * nx3 + my3 * ny3 + mz3 * nz3;
	double interValue4 = mx4 * nx4 + my4 * ny4 + mz4 * nz4;
	double interValue5 = mx5 * nx5 + my5 * ny5 + mz5 * nz5;
	double interValue6 = mx6 * nx6 + my6 * ny6 + mz6 * nz6;

	/* calculate component of force */

	/* component along x-axis */
	*Fx += aniK0 * interValue0 * (mx0 * nx0Dx + my0 * ny0Dx + mz0 * nz0Dx) +
		   aniK1 * interValue1 * (mx1 * nx1Dx + my1 * ny1Dx + mz1 * nz1Dx) +
		   aniK2 * interValue2 * (mx2 * nx2Dx + my2 * ny2Dx + mz2 * nz2Dx) +
		   aniK3 * interValue3 * (mx3 * nx3Dx + my3 * ny3Dx + mz3 * nz3Dx) +
		   aniK4 * interValue4 * (mx4 * nx4Dx + my4 * ny4Dx + mz4 * nz4Dx) +
		   aniK5 * interValue5 * (mx5 * nx5Dx + my5 * ny5Dx + mz5 * nz5Dx) +
		   aniK6 * interValue6 * (mx6 * nx6Dx + my6 * ny6Dx + mz6 * nz6Dx);

	/* component along y-axis */
	*Fy += aniK0 * interValue0 * (mx0 * nx0Dy + my0 * ny0Dy + mz0 * nz0Dy) +
		   aniK1 * interValue1 * (mx1 * nx1Dy + my1 * ny1Dy + mz1 * nz1Dy) +
		   aniK2 * interValue2 * (mx2 * nx2Dy + my2 * ny2Dy + mz2 * nz2Dy) +
		   aniK3 * interValue3 * (mx3 * nx3Dy + my3 * ny3Dy + mz3 * nz3Dy) +
		   aniK4 * interValue4 * (mx4 * nx4Dy + my4 * ny4Dy + mz4 * nz4Dy) +
		   aniK5 * interValue5 * (mx5 * nx5Dy + my5 * ny5Dy + mz5 * nz5Dy) +
		   aniK6 * interValue6 * (mx6 * nx6Dy + my6 * ny6Dy + mz6 * nz6Dy);

	/* component along z-axis */
	*Fz += aniK0 * interValue0 * (mx0 * nx0Dz + my0 * ny0Dz + mz0 * nz0Dz) +
		   aniK1 * interValue1 * (mx1 * nx1Dz + my1 * ny1Dz + mz1 * nz1Dz) +
		   aniK2 * interValue2 * (mx2 * nx2Dz + my2 * ny2Dz + mz2 * nz2Dz) +
		   aniK3 * interValue3 * (mx3 * nx3Dz + my3 * ny3Dz + mz3 * nz3Dz) +
		   aniK4 * interValue4 * (mx4 * nx4Dz + my4 * ny4Dz + mz4 * nz4Dz) +
		   aniK5 * interValue5 * (mx5 * nx5Dz + my5 * ny5Dz + mz5 * nz5Dz) +
		   aniK6 * interValue6 * (mx6 * nx6Dz + my6 * ny6Dz + mz6 * nz6Dz);

}

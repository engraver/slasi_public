/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

/**
 * @file magninit.h
 * @author Oleksandr Pylypovskyi, Artem Tomilo
 * @date 12 Jan 2018
 * @brief Initialization of vectors for anisotropy
 */

#ifndef _SCRIPT_ANISOTROPY_H_
#define _SCRIPT_ANISOTROPY_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../../alltypes.h"
#include "../../utils/indices.h"

/**
 * @brief Initialization of vectors
for anisotropy using Python script
 * Function works with `userVars` and 'latticeParam', so does not take any arguments.
 */
int scriptAnisotropy(void);

#endif

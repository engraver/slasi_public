#ifndef _UNIAXIALANISOTROPY_H_
#define _UNIAXIALANISOTROPY_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../../alltypes.h"
#include "../interactions.h"
#include "../../utils/indices.h"

/**
 * @brief Setup single-ion anisotropy
 *
 * @param numbers array with values from paramFile
 * @param indlp index for latticeParam array
 * @param indn index for numbers array
 *
 * @return 0 if ok, otherwise nonzero value
 */
int setupAniK(double ** numbers, int indlp, int indn);

/**
 * @brief Setup inter-ion anisotropy
 *
 * @param numbers array with values from paramFile
 * @param indlp index for latticeParam array
 * @param indn index for numbers array
 *
 * @return 0 if ok, otherwise nonzero value
 */
int setupInterAniK(double ** numbers, int indlp, int indn);

/**
 * @brief Setup anisotropy axes
 *
 * @param numbers array with values from paramFile
 * @param indlp index for latticeParam array
 * @param indn index for numbers array
 *
 * @return 0 if ok, otherwise nonzero value
 */
int setupAnisotropyAxes(double ** numbers, int indlp, int indn);

/**
 * @brief This function returns effective field and energy
 * which created by magnetic anisotropy of three axises
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Hx effective field on axis x
 * @param Hy effective field on axis y
 * @param Hz effective field on axis z
 * @param EaniK1 normalized energy of anisotropy for the first axis (x)
 * @param EaniK2 normalized energy of anisotropy for the second axis (y)
 * @param EaniK3 normalized energy of anisotropy for the third axis (z)
 */
void uniaxialAnisotropyField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EaniK1, double *EaniK2, double *EaniK3);


/**
 * @brief This function returns force
 * which created by magnetic anisotropy of three axises
 * @param i1 the first index of node
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Fx effective field on axis x
 * @param Fy effective field on axis y
 * @param Fz effective field on axis z
 */
void uniaxialAnisotropyForce(int i1, size_t curIndex, size_t indexLattice, double *Fx, double *Fy, double *Fz);

/**
 * @brief This function returns effective field and energy
 * which created by magnetic anisotropy in triangular lattice
 * @param i1 the first index of node
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Hx effective field on axis x
 * @param Hy effective field on axis y
 * @param Hz effective field on axis z
 * @param EaniK1 normalized energy of anisotropy for main direction
 * @param EaniK3 normalized energy of anisotropy for normal vector
 */
void uniaxialAnisotropyFieldTriangle(int i1, size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EaniK1, double *EaniK3);

/**
 * @brief This function returns force
 * which created by magnetic anisotropy along main direction of three axises for triangle lattice
 * @param i1 the first index of node
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Fx effective field on axis x
 * @param Fy effective field on axis y
 * @param Fz effective field on axis z
 */
void uniaxialAnisotropyTangForceTriangle(int i1, int i2, size_t curIndex, size_t indexLattice, double *Fx, double *Fy, double *Fz);

/**
 * @brief This function returns force
 * which created by magnetic anisotropy along normal vector of three axises for triangle lattice
 * @param i1 the first index of node
 * @param Fx effective field on axis x
 * @param Fy effective field on axis y
 * @param Fz effective field on axis z
 */
void uniaxialAnisotropyNormForceTriangle(int i1, int i2, double *Fx, double *Fy, double *Fz);


/**
 * @brief This function returns effective field and energy (CUDA)
 * which created by uniaxial anisotropy (GPU)
 * @param d_H is array of effective field (GPU)
 * @param d_EnergyTotal1 is array of energy for sites along the first axis (GPU)
 * @param d_EnergyTotal2 is array of energy for sites along the second axis (GPU)
 * @param d_EnergyTotal3 is array of energy for sites along the third axis (GPU)
 * @param d_latticeParam is array for parameters of sites (GPU)
 * @param numberSites is number of sites
 */
void uniaxialAnisotropyFieldCUDA(double *d_H, double *d_EnergyTotal1, double *d_EnergyTotal2, double *d_EnergyTotal3, LatticeSiteDescr * d_latticeParam, long numberSites);

/**
 * @brief This function returns effective field and energy (CUDA)
 * which created by uniaxial anisotropy (GPU)
 * @param d_H is array of effective field (GPU)
 * @param d_EnergyTotal1 is array of energy for sites along the main direction (GPU)
 * @param d_EnergyTotal3 is array of energy for sites along the normal vector (GPU)
 * @param d_latticeParam is array for parameters of sites (GPU)
 * @param numberSites is number of sites
 */
void uniaxialAnisotropyFieldTriangleCUDA(double *d_H, double *d_EnergyTotal1, double *d_EnergyTotal3, LatticeSiteDescr * d_latticeParam, long numberSites);

#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "CUnit/Basic.h"

#include "../../alltypes.h"
#include "../../simulation.h"

/* names of configuration files for testing */
#define FILE_ANISENER_CFG1 "testanisener1.cfg"
#define FILE_ANISENER_DAT1 "testanisener1.dat"
#define FILE_ANISENER_BAS1 "testanisener1"

#define FILE_ANISENER_CFG2 "testanisener2.cfg"
#define FILE_ANISENER_DAT2 "testanisener2.dat"
#define FILE_ANISENER_BAS2 "testanisener2"

#define FILE_ANISENER_CFG3 "testanisener3.cfg"
#define FILE_ANISENER_DAT3 "testanisener3.dat"
#define FILE_ANISENER_BAS3 "testanisener3"

#define FILE_ANISENER_CFG4 "testanisener4.cfg"
#define FILE_ANISENER_DAT4 "testanisener4.dat"
#define FILE_ANISENER_BAS4 "testanisener4"

#define FILE_ANISENER_CFG5 "testanisener5.cfg"
#define FILE_ANISENER_DAT5 "testanisener5.dat"
#define FILE_ANISENER_BAS5 "testanisener5"

#define PREC_TEST_ANIS 0.000001

/* initialization function for tests */
int init_anisotropy_test(void) {
	return 0;
}

/* function of cleaning for tests */
int clean_anisotropy_test(void) {
	return 0;
}

/* function to create files to test magnetic simulator (for anisotropy) */
void createAnisFiles(char * nameConfFile, char * nameParamFile, char * rowConfFile, char * rowParamFile) {

	/* create configuration file */
	FILE * fid1 = fopen(nameConfFile, "w");
	fprintf(fid1, "%s", rowConfFile);
	fclose(fid1);

	/* create parameter file */
	FILE * fid2 = fopen(nameParamFile, "w");
	fprintf(fid2, "%s", rowParamFile);
	fclose(fid2);

}

/* function to launch simulation (for anisotropy) */
void launchAnisSimulation(char * nameConfFile, char * nameParamFile) {

	/* make parameters for launching */
	int argc = 4;
	char **argv;
	argv = (char**) malloc(sizeof(char**) * 4);
	argv[0] = (char*) malloc(sizeof("slasi"));
	argv[0] = "slasi";
	argv[1] = (char*) malloc(sizeof(nameConfFile));
	argv[1] = nameConfFile;
	argv[2] = (char*) malloc(sizeof("-p"));
	argv[2] = "-p";
	argv[3] = (char*) malloc(sizeof(nameParamFile));
	argv[3] = nameParamFile;

	/* launch simulation */
	slasi(argc, argv, false);

}

/* function to delete two files (for anisotropy) */
void deleteAnisFiles(char * nameConfFile, char * nameParamFile, char * baseName) {

	/* delete parameter and configuration files */
	remove(nameConfFile);
	remove(nameParamFile);

	/* delete energy logfile which are created during simulation */
	char energy_log [100];
	strcpy(energy_log, baseName);
	strcat(energy_log, "_energy.log");
	remove(energy_log);

	/* delete logfile of initialization which are created during simulation */
	char init_log [100];
	strcpy(init_log, baseName);
	strcat(init_log, "_init.log");
	remove(init_log);

	/* delete logfile of magnetism which are created during simulation */
	char mag_log [100];
	strcpy(mag_log, baseName);
	strcat(mag_log, "_mag.log");
	remove(mag_log);

	/* delete logfile for other parameters which are created during simulation */
	char other_log [100];
	strcpy(other_log, baseName);
	strcat(other_log, "_other.log");
	remove(other_log);

	/* delete file for table of parameters */
	char table_file [100];
	strcpy(table_file, baseName);
	strcat(table_file, ".tbl");
	remove(table_file);

	/* delete the first file of intermediate results */
	char file_inter1 [100];
	strcpy(file_inter1, baseName);
	strcat(file_inter1, ".00000.slsb");
	remove(file_inter1);

	/* delete the second file of intermediate results */
	char file_inter2 [100];
	strcpy(file_inter2, baseName);
	strcat(file_inter2, ".00000.inp");
	remove(file_inter2);

	/* delete the third file of intermediate results */
	char file_inter3 [100];
	strcpy(file_inter3, baseName);
	strcat(file_inter3, ".00001.slsb");
	remove(file_inter3);

	/* delete the fourth file of intermediate results */
	char file_inter4 [100];
	strcpy(file_inter4, baseName);
	strcat(file_inter4, ".00001.inp");
	remove(file_inter4);

	/* delete the restart file of intermediate results */
	char file_restart [100];
	strcpy(file_restart, baseName);
	strcat(file_restart, ".restart.slsb");
	remove(file_restart);

}

/*
 * @brief yet another test of exchange interaction and anisotropy
 *
 * chain of two magnetic moments with exchange interaction and anisotropy
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3
 * 000 000 000 000
 * 000 0+0 0+0 000
 * 000 000 000 000
 * @endverbatim
 * Here, + magnetic site with initial value (1,0,0).
 * The magnetic system has exchange interaction and easy axis anisotropy (1,0,0).
 * So, all magnetic moments have to be parallel and have total anisotropy energy -4.0
 * (4 times more than the exchange interaction, and
 * the normalized interaction energy of pair of parallel magnetic moments is -1.0)
 */
void test_anisotropy_energy_1(void) {

	/* correct energy */
	double EnergyCorr = -4.0;

	/* create files to test magnetic simulator */
	createAnisFiles(FILE_ANISENER_CFG1, FILE_ANISENER_DAT1, "N1 = 2\nN2 = 1\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\naniK1 = 4e-10\n", "mx my mz x y z\n1.0 0 0 0 0 0\n1.0 0 0 1 0 0\n");

	/* launch simulation */
	launchAnisSimulation(FILE_ANISENER_CFG1, FILE_ANISENER_DAT1);

	/* check energy */
	CU_ASSERT(fabs(EnergyAnisotropyNormAxisK1 - EnergyCorr) < PREC_TEST_ANIS);

	/* delete created files */
	deleteAnisFiles(FILE_ANISENER_CFG1, FILE_ANISENER_DAT1, FILE_ANISENER_BAS1);

}

/*
 * @brief yet another test of exchange interaction and anisotropy
 *
 * plain of four magnetic moments with exchange interaction and anisotropy
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3
 * 000 000 000 000
 * 000 0+0 0+0 000
 * 000 0+0 0+0 000
 * 000 000 000 000
 * @endverbatim
 * Here, + magnetic site with initial value (0,1,0).
 * The magnetic system has exchange interaction and easy axis anisotropy (0,1,0).
 * So, all magnetic moments have to be parallel and have total anisotropy energy -10.0
 * (5 times more than the exchange interaction, and
 * the normalized interaction energy of pair of parallel magnetic moments is -1.0)
 */
void test_anisotropy_energy_2(void) {

	/* correct energy */
	double EnergyCorr = -10.0;

	/* create files to test magnetic simulator */
	createAnisFiles(FILE_ANISENER_CFG2, FILE_ANISENER_DAT2, "N1 = 2\nN2 = 2\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\naniK2 = 5e-10\n", "mx my mz x y z\n0 1.0 0 0 0 0\n0 1.0 0 1 0 0\n0 1.0 0 0 1 0\n0 1.0 0 1 1 0\n");

	/* launch simulation */
	launchAnisSimulation(FILE_ANISENER_CFG2, FILE_ANISENER_DAT2);

	/* check energy */
	CU_ASSERT(fabs(EnergyAnisotropyNormAxisK2 - EnergyCorr) < PREC_TEST_ANIS);

	/* delete created files */
	deleteAnisFiles(FILE_ANISENER_CFG2, FILE_ANISENER_DAT2, FILE_ANISENER_BAS2);

}

/*
 * @brief yet another test of exchange interaction and anisotropy
 *
 * cube of eight magnetic moments with exchange interaction and anisotropy
 *
 * @verbatim
 * Location of magnetic moment components in an array
 * 0    1    2    3
 * 0000 0000 0000 0000
 * 0000 0++0 0++0 0000
 * 0000 0++0 0++0 0000
 * 0000 0000 0000 0000
 * @endverbatim
 * Here, + magnetic site with initial value (0,0,1).
 * The magnetic system has exchange interaction and easy axis anisotropy (0,0,1).
 * So, all magnetic moments have to be parallel and have total anisotropy energy -24.0
 * (6 times more than the exchange interaction, and
 * the normalized interaction energy of pair of parallel magnetic moments is -1.0)
 */
void test_anisotropy_energy_3(void) {

	/* correct energy */
	double EnergyCorr = -24.0;

	/* create files to test magnetic simulator */
	createAnisFiles(FILE_ANISENER_CFG3, FILE_ANISENER_DAT3, "N1 = 2\nN2 = 2\nN3 = 2\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\naniK3 = 6e-10\n", "mx my mz x y z\n0 0 1.0 0 0 0\n0 0 1.0 1 0 0\n0 0 1.0 0 1 0\n0 0 1.0 1 1 0\n0 0 1.0 0 0 1\n0 0 1.0 1 0 1\n0 0 1.0 0 1 1\n0 0 1.0 1 1 1\n");

	/* launch simulation */
	launchAnisSimulation(FILE_ANISENER_CFG3, FILE_ANISENER_DAT3);

	/* check energy */
	CU_ASSERT(fabs(EnergyAnisotropyNormAxisK3 - EnergyCorr) < PREC_TEST_ANIS);

	/* delete created files */
	deleteAnisFiles(FILE_ANISENER_CFG3, FILE_ANISENER_DAT3, FILE_ANISENER_BAS3);

}

/*
 * @brief yet another test of exchange interaction and anisotropy
 *
 * plain of eight magnetic moments with hole of nonmagnetic spin
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3   4
 * 000 000 000 000 000
 * 000 0+0 0+0 0+0 000
 * 000 0+0 0-0 0+0 000
 * 000 0+0 0+0 0+0 000
 * 000 000 000 000 000
 * @endverbatim
 * Here, + magnetic site with initial value (1,0,0) and - non-magnetic site with initial value (0,0,0).
 * The magnetic system has exchange interaction and easy axis anisotropy (1,0,0).
 * So, all magnetic moments have to be parallel and have total anisotropy energy -20.0
 * (5 times more than the exchange interaction, and
 * the normalized interaction energy of pair of parallel magnetic moments is -1.0)
 */
void test_anisotropy_energy_4(void) {

	/* correct energy */
	double EnergyCorr = -20.0;

	/* create files to test magnetic simulator */
	createAnisFiles(FILE_ANISENER_CFG4, FILE_ANISENER_DAT4, "N1 = 3\nN2 = 3\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\naniK1 = 5e-10\ntypeMagn = file\n", "mx my mz x y z magn\n1.0 0.0 0.0 1 1 1 1\n1.0 0.0 0.0 1 2 1 1\n1.0 0.0 0.0 1 3 1 1\n1.0 0.0 0.0 2 1 1 1\n0.0 0.0 0.0 2 2 1 0\n1.0 0.0 0.0 2 3 1 1\n1.0 0.0 0.0 3 1 1 1\n1.0 0.0 0.0 3 2 1 1\n1.0 0.0 0.0 3 3 1 1\n");

	/* launch simulation */
	launchAnisSimulation(FILE_ANISENER_CFG4, FILE_ANISENER_DAT4);

	/* check energy */
	CU_ASSERT(fabs(EnergyAnisotropyNormAxisK1 - EnergyCorr) < PREC_TEST_ANIS);

	/* delete created files */
	deleteAnisFiles(FILE_ANISENER_CFG4, FILE_ANISENER_DAT4, FILE_ANISENER_BAS4);

}

/*
 * @brief yet another test of exchange interaction and anisotropy
 *
 * cube of sixteen magnetic moments with exchange interaction and anisotropy
 *
 * @verbatim
 * Location of magnetic moment components in an array
 * 0    1    2    3    4
 * 0000 0000 0000 0000 0000
 * 0000 0++0 0++0 0++0 0000
 * 0000 0++0 0--0 0++0 0000
 * 0000 0++0 0++0 0++0 0000
 * 0000 0000 0000 0000 0000
 * @endverbatim
 * Here, + magnetic site with initial value (0,0,1).
 * The magnetic system has exchange interaction and easy axis anisotropy (0,0,1).
 * So, all magnetic moments have to be parallel and have total anisotropy energy -6.0
 * (4 times more than the exchange interaction, and
 * the normalized interaction energy of pair of parallel magnetic moments is -1.0)
 */
void test_anisotropy_energy_5(void) {

	/* correct energy */
	double EnergyCorr = -32.0;

	/* create files to test magnetic simulator */
	createAnisFiles(FILE_ANISENER_CFG5, FILE_ANISENER_DAT5, "N1 = 3\nN2 = 3\nN3 = 2\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\naniK3 = 4e-10\ntypeMagn = file\n", "mx my mz x y z magn\n0.0 0.0 1.0 1 1 1 1\n0.0 0.0 1.0 1 1 2 1\n0.0 0.0 1.0 1 2 1 1\n0.0 0.0 1.0 1 2 2 1\n0.0 0.0 1.0 1 3 1 1\n0.0 0.0 1.0 1 3 2 1\n0.0 0.0 1.0 2 1 1 1\n0.0 0.0 1.0 2 1 2 1\n0.0 0.0 0.0 2 2 1 0\n0.0 0.0 0.0 2 2 2 0\n0.0 0.0 1.0 2 3 1 1\n0.0 0.0 1.0 2 3 2 1\n0.0 0.0 1.0 3 1 1 1\n0.0 0.0 1.0 3 1 2 1\n0.0 0.0 1.0 3 2 1 1\n0.0 0.0 1.0 3 2 2 1\n0.0 0.0 1.0 3 3 1 1\n0.0 0.0 1.0 3 3 2 1\n");

	/* launch simulation */
	launchAnisSimulation(FILE_ANISENER_CFG5, FILE_ANISENER_DAT5);

	/* check energy */
	CU_ASSERT(fabs(EnergyAnisotropyNormAxisK3 - EnergyCorr) < PREC_TEST_ANIS);

	/* delete created files */
	deleteAnisFiles(FILE_ANISENER_CFG5, FILE_ANISENER_DAT5, FILE_ANISENER_BAS5);

}
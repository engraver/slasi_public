#include <stdio.h>
#include "../../alltypes.h"
extern "C" {
	#include "uniaxialAnisotropy.h"
}

/* uniaxial anisotropy for one site in usual lattice (GPU) */
__global__
void spinUniaxialAnisotropyField(double *d_H, double *d_lattice, double *d_EnergyTotal1, double *d_EnergyTotal2, double *d_EnergyTotal3, LatticeSiteDescr *d_latticeParam, int dimension, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* project vectors of anisotropy on orthonormal basis */
	/* the first vector */
	double e11, e12, e13;
	/* the second vector */
	double e21, e22, e23;
	/* the third vector */
	double e31, e32, e33;

	if (indexLattice < numberSites) {

		/* if it is curve, then we will use TNB-orthonormal basis  */
		if (dimension == 1) {

			e11 = d_latticeParam[indexLattice].tx * d_latticeParam[indexLattice].e11 + d_latticeParam[indexLattice].nx * d_latticeParam[indexLattice].e12 + d_latticeParam[indexLattice].bx * d_latticeParam[indexLattice].e13;
			e12 = d_latticeParam[indexLattice].ty * d_latticeParam[indexLattice].e11 + d_latticeParam[indexLattice].ny * d_latticeParam[indexLattice].e12 + d_latticeParam[indexLattice].by * d_latticeParam[indexLattice].e13;
			e13 = d_latticeParam[indexLattice].tz * d_latticeParam[indexLattice].e11 + d_latticeParam[indexLattice].nz * d_latticeParam[indexLattice].e12 + d_latticeParam[indexLattice].bz * d_latticeParam[indexLattice].e13;

			e21 = d_latticeParam[indexLattice].tx * d_latticeParam[indexLattice].e21 + d_latticeParam[indexLattice].nx * d_latticeParam[indexLattice].e22 + d_latticeParam[indexLattice].bx * d_latticeParam[indexLattice].e23;
			e22 = d_latticeParam[indexLattice].ty * d_latticeParam[indexLattice].e21 + d_latticeParam[indexLattice].ny * d_latticeParam[indexLattice].e22 + d_latticeParam[indexLattice].by * d_latticeParam[indexLattice].e23;
			e23 = d_latticeParam[indexLattice].tz * d_latticeParam[indexLattice].e21 + d_latticeParam[indexLattice].nz * d_latticeParam[indexLattice].e22 + d_latticeParam[indexLattice].bz * d_latticeParam[indexLattice].e23;

			e31 = d_latticeParam[indexLattice].tx * d_latticeParam[indexLattice].e31 + d_latticeParam[indexLattice].nx * d_latticeParam[indexLattice].e32 + d_latticeParam[indexLattice].bx * d_latticeParam[indexLattice].e33;
			e32 = d_latticeParam[indexLattice].ty * d_latticeParam[indexLattice].e31 + d_latticeParam[indexLattice].ny * d_latticeParam[indexLattice].e32 + d_latticeParam[indexLattice].by * d_latticeParam[indexLattice].e33;
			e33 = d_latticeParam[indexLattice].tz * d_latticeParam[indexLattice].e31 + d_latticeParam[indexLattice].nz * d_latticeParam[indexLattice].e32 + d_latticeParam[indexLattice].bz * d_latticeParam[indexLattice].e33;

		} else {

			/* if it is not curve, then we put default orthonormal basis */
			e11 = d_latticeParam[indexLattice].e11;
			e12 = d_latticeParam[indexLattice].e12;
			e13 = d_latticeParam[indexLattice].e13;

			e21 = d_latticeParam[indexLattice].e21;
			e22 = d_latticeParam[indexLattice].e22;
			e23 = d_latticeParam[indexLattice].e23;

			e31 = d_latticeParam[indexLattice].e31;
			e32 = d_latticeParam[indexLattice].e32;
			e33 = d_latticeParam[indexLattice].e33;

		}

		/* calculate effective field and energy of the first axis */
		double inner1 = d_lattice[curIndex] * e11 + d_lattice[curIndex + 1] * e12 + d_lattice[curIndex + 2] * e13;

		double HxLocal1 = d_latticeParam[indexLattice].aniK1 * e11 * inner1;
		double HyLocal1 = d_latticeParam[indexLattice].aniK1 * e12 * inner1;
		double HzLocal1 = d_latticeParam[indexLattice].aniK1 * e13 * inner1;

		d_EnergyTotal1[indexLattice] = -(HxLocal1 * d_lattice[curIndex] + HyLocal1 * d_lattice[curIndex + 1] +
		                                 HzLocal1 * d_lattice[curIndex + 2]);

		/* calculate effective field and energy of the second axis */
		double inner2 = d_lattice[curIndex] * e21 + d_lattice[curIndex + 1] * e22 + d_lattice[curIndex + 2] * e23;

		double HxLocal2 = d_latticeParam[indexLattice].aniK2 * e21 * inner2;
		double HyLocal2 = d_latticeParam[indexLattice].aniK2 * e22 * inner2;
		double HzLocal2 = d_latticeParam[indexLattice].aniK2 * e23 * inner2;

		d_EnergyTotal2[indexLattice] = -(HxLocal2 * d_lattice[curIndex] + HyLocal2 * d_lattice[curIndex + 1] +
		                                 HzLocal2 * d_lattice[curIndex + 2]);

		/* calculate effective field and energy of the third axis */
		double inner3 = d_lattice[curIndex] * e31 + d_lattice[curIndex + 1] * e32 + d_lattice[curIndex + 2] * e33;

		double HxLocal3 = d_latticeParam[indexLattice].aniK3 * e31 * inner3;
		double HyLocal3 = d_latticeParam[indexLattice].aniK3 * e32 * inner3;
		double HzLocal3 = d_latticeParam[indexLattice].aniK3 * e33 * inner3;

		d_EnergyTotal3[indexLattice] = -(HxLocal3 * d_lattice[curIndex] + HyLocal3 * d_lattice[curIndex + 1] +
		                                 HzLocal3 * d_lattice[curIndex + 2]);

		/* add generated effective field to the total */
		d_H[curIndex] += HxLocal1 + HxLocal2 + HxLocal3;
		d_H[curIndex + 1] += HyLocal1 + HyLocal2 + HyLocal3;
		d_H[curIndex + 2] += HzLocal1 + HzLocal2 + HzLocal3;

	}

}

extern "C"
void uniaxialAnisotropyFieldCUDA(double *d_H, double *d_EnergyTotal1, double *d_EnergyTotal2, double *d_EnergyTotal3, LatticeSiteDescr *d_latticeParam, long numberSites) {

	/* setup the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* wrapper of CUDA function */
	spinUniaxialAnisotropyField<<<cores, threads>>>(d_H, d_lattice, d_EnergyTotal1, d_EnergyTotal2, d_EnergyTotal3, d_latticeParam, userVars.dimensionOfSystem, numberSites);

}

/* uniaxial anisotropy for one site in triangular lattice (GPU) */
__global__
void spinUniaxialAnisotropyFieldTriangle(double *d_H, double *d_lattice, double *d_EnergyTotal1, double *d_EnergyTotal3, LatticeSiteDescr *d_latticeParam, long N1Ext, long N2Ext, long N3Ext, PeriodicBC periodicBC1, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	if ((indexLattice < numberSites) &&
	    (d_lattice[curIndex] * d_lattice[curIndex] + d_lattice[curIndex + 1] * d_lattice[curIndex + 1] + d_lattice[curIndex + 2] * d_lattice[curIndex + 2] > LENG_ZERO)) {

		/* index in triangular lattice */
		long i1 = indexLattice / (N2Ext * N3Ext);

		/* magnetization */
		double mxi = d_lattice[curIndex];
		double myi = d_lattice[curIndex + 1];
		double mzi = d_lattice[curIndex + 2];
		/* tangential vector of site */
		double nxi = d_latticeParam[indexLattice].trinx;
		double nyi = d_latticeParam[indexLattice].triny;
		double nzi = d_latticeParam[indexLattice].trinz;
		/* normal vector of site */
		double txi = d_latticeParam[indexLattice].tx;
		double tyi = d_latticeParam[indexLattice].ty;
		double tzi = d_latticeParam[indexLattice].tz;
		double aniK1_i = d_latticeParam[indexLattice].aniK1;
		double aniK3_i = d_latticeParam[indexLattice].aniK3;
		if ((i1 == N1Ext - 2) && (periodicBC1 == PBC_OFF)) {
			aniK1_i = 0.0;
		}

		/* calculate effective field and energy of the first axis (main direction) */
		double inner1 = mxi * txi + myi * tyi + mzi * tzi;

		double HxLocal1 = aniK1_i * txi * inner1;
		double HyLocal1 = aniK1_i * tyi * inner1;
		double HzLocal1 = aniK1_i * tzi * inner1;

		d_EnergyTotal1[indexLattice] = -(HxLocal1 * d_lattice[curIndex] + HyLocal1 * d_lattice[curIndex + 1] + HzLocal1 * d_lattice[curIndex + 2]);

		/* calculate effective field and energy of the third axis (normal vector) */
		double inner3 = mxi * nxi + myi * nyi + mzi * nzi;

		double HxLocal3 = aniK3_i * nxi * inner3;
		double HyLocal3 = aniK3_i * nyi * inner3;
		double HzLocal3 = aniK3_i * nzi * inner3;

		d_EnergyTotal3[indexLattice] = -(HxLocal3 * d_lattice[curIndex] + HyLocal3 * d_lattice[curIndex + 1] + HzLocal3 * d_lattice[curIndex + 2]);

		/* add generated effective field to the total */
		d_H[curIndex] += (HxLocal1 + HxLocal3);
		d_H[curIndex + 1] += (HyLocal1 + HyLocal3);
		d_H[curIndex + 2] += (HzLocal1 + HzLocal3);

	}

}

extern "C"
void uniaxialAnisotropyFieldTriangleCUDA(double *d_H, double *d_EnergyTotal1, double *d_EnergyTotal3, LatticeSiteDescr *d_latticeParam, long numberSites) {

	/* setup the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* wrapper of CUDA function */
	spinUniaxialAnisotropyFieldTriangle<<<cores, threads>>>(d_H, d_lattice, d_EnergyTotal1, d_EnergyTotal3, d_latticeParam, userVars.N1, userVars.N2, userVars.N3, userVars.periodicBC1, numberSites);

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include <Python.h>
#include <math.h>
#include "scriptAnisotropy.h"

/* initialize all vectors for anisotropy */
int initAllVectors(void) {

	/* declarations of objects to work with Python.h */
	PyObject *pArgs, *pFunc;
	PyObject *pValue;

	/* declarations of variable */
	double e11, e12, e13, e21, e22, e23, e31, e32, e33, K1, K2, K3;
	double length;
	int res;

	/* check if module was found */
	if (pModulePython != NULL) {

		/* search of function wint name "getAnisotropy" in module */
		pFunc = PyObject_GetAttrString(pModulePython, "getAnisotropy");
		/* check if function was found */
		if (pFunc && PyCallable_Check(pFunc)) {

			/* pass through each magnetic moment */
			for (int i1 = 0; i1 < userVars.N1; i1++) {
				for (int i2 = 0; i2 < userVars.N2; i2++) {
					for (int i3 = 0; i3 < userVars.N3; i3++) {
						if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
						    i3 < userVars.N3 - 1) {

							/* number of spin in lattice */
							int indexLattice = IDX(i1, i2, i3);
							/* check if node is magnetic */
							if (latticeParam[indexLattice].magnetic) {

								/* creating parameters to call function (coordinates and indices) */
								pArgs = PyTuple_New(6);

								/* the first coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].x) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									fprintf(stderr, "Cannot convert arguments for function getAnisotropy!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 0, pValue);

								/* the second coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].y) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									fprintf(stderr, "Cannot convert arguments for function getAnisotropy!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 1, pValue);

								/* the third coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].z) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									fprintf(stderr, "Cannot convert arguments for function getAnisotropy!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 2, pValue);

								/* the first index */
								pValue = PyLong_FromLong(i1);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									fprintf(stderr, "Cannot convert arguments for function getAnisotropy!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 3, pValue);

								/* the second index */
								pValue = PyLong_FromLong(i2);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									fprintf(stderr, "Cannot convert arguments for function getAnisotropy!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 4, pValue);

								/* the third index */
								pValue = PyLong_FromLong(i3);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									fprintf(stderr, "Cannot convert arguments for function getAnisotropy!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 5, pValue);

								/* call function */
								pValue = PyObject_CallObject(pFunc, pArgs);
								/* check results of calling */
								if (pValue != NULL) {

									/* get results of calling */
									res = PyArg_ParseTuple(pValue, "dddddddddddd", &K1, &e11, &e12, &e13, &K2,
									                       &e21, &e22, &e23, &K3, &e31, &e32, &e33);
									/* check if it was able to get */
									if (res) {
										/* save results */

										/* calculate for normalization */
										length = sqrt(e11 * e11 + e12 * e12 + e13 * e13);
										if (fabs(length) < 1e-5) {
											/* output error */
											PyErr_Print();
											fprintf(stderr, "(file %s | line %d) Script %s returns zero anisotropy axis for site (%d, %d, %d) with coords (%lg, %lg, %lg)!\n", __FILE__, __LINE__, userVars.scriptFile, i1, i2, i3, *(latticeParam[indexLattice].x), *(latticeParam[indexLattice].y), *(latticeParam[indexLattice].z));
											return 1;
										}

										/* the first vector of anisotropy */
										latticeParam[indexLattice].e11 = e11 / length;
										latticeParam[indexLattice].e12 = e12 / length;
										latticeParam[indexLattice].e13 = e13 / length;
										latticeParam[indexLattice].aniK1 = K1;

										/* calculate for normalization */
										length = sqrt(e21 * e21 + e22 * e22 + e23 * e23);
										if (fabs(length) < 1e-5) {
											/* output error */
											PyErr_Print();
											fprintf(stderr, "(file %s | line %d) Script %s returns zero anisotropy axis for site (%d, %d, %d) with coords (%lg, %lg, %lg)!\n", __FILE__, __LINE__, userVars.scriptFile, i1, i2, i3, *(latticeParam[indexLattice].x), *(latticeParam[indexLattice].y), *(latticeParam[indexLattice].z));
											return 1;
										}

										/* the second vector of anisotropy */
										latticeParam[indexLattice].e21 = e21 / length;
										latticeParam[indexLattice].e22 = e22 / length;
										latticeParam[indexLattice].e23 = e23 / length;
										latticeParam[indexLattice].aniK2 = K2;

										/* calculate for normalization */
										length = sqrt(e31 * e31 + e32 * e32 + e33 * e33);
										if (fabs(length) < 1e-5) {
											/* output error */
											PyErr_Print();
											fprintf(stderr, "(file %s | line %d) Script %s returns zero anisotropy axis for site (%d, %d, %d) with coords (%lg, %lg, %lg)!\n", __FILE__, __LINE__, userVars.scriptFile, i1, i2, i3, *(latticeParam[indexLattice].x), *(latticeParam[indexLattice].y), *(latticeParam[indexLattice].z));
											return 1;
										}

										/* the third vector of anisotropy */
										latticeParam[indexLattice].e31 = e31 / length;
										latticeParam[indexLattice].e32 = e32 / length;
										latticeParam[indexLattice].e33 = e33 / length;
										latticeParam[indexLattice].aniK3 = K3;

									} else {
										/* output error */
										fprintf(stderr, "Call of python function getAnisotropy failed!\n");
										return 1;
									}
									Py_DECREF(pValue);

								} else {
									/* output error */
									Py_DECREF(pFunc);
									PyErr_Print();
									fprintf(stderr, "Call of python function getAnisotropy failed\n");
									return 1;
								}

							}

						}
					}
				}
			}

		} else {
			/* output error */
			if (PyErr_Occurred())
				PyErr_Print();
			fprintf(stderr, "Cannot find function getAnisotropy in python file!\n");
			return 1;
		}
		Py_XDECREF(pFunc);

	} else {
		/* output error */
		PyErr_Print();
		fprintf(stderr, "Failed to load python file %s!\n", userVars.scriptFile);
		return 1;
	}

	return 0;

}

int scriptAnisotropy() {

	/* initialize all vectors */
	if (initAllVectors()) {
		return 1;
	}
	return 0;

}

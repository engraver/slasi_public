#ifndef _ANISOTROPY_TEST_H
#define _ANISOTROPY_TEST_H

int init_anisotropy_test(void);
int clean_anisotropy_test(void);

/* uniform exchange energy and easy-axis anisotropy for chain (energy) */
char * test_anisotropy_energy_1_desc = "UniX test: 2-spin chain energy (exchange interaction and easy-axis anisotropy)\n";
void test_anisotropy_energy_1(void);

/* uniform exchange energy and easy-axis anisotropy for plain (energy) */
char * test_anisotropy_energy_2_desc = "UniX test: 4-spin plain energy (exchange interaction and easy-axis anisotropy)\n";
void test_anisotropy_energy_2(void);

/* uniform exchange energy and easy-axis anisotropy for cube (energy) */
char * test_anisotropy_energy_3_desc = "UniX test: 8-spin cube energy (exchange interaction and easy-axis anisotropy)\n";
void test_anisotropy_energy_3(void);

/* uniform exchange energy and easy-axis anisotropy for plain with hole (energy) */
char * test_anisotropy_energy_4_desc = "UniX test: 8-spin plain energy with hole (exchange interaction and easy-axis anisotropy)\n";
void test_anisotropy_energy_4(void);

/* uniform exchange energy and easy-axis anisotropy for parallelepiped with hole (energy) */
char * test_anisotropy_energy_5_desc = "UniX test: 16-spin parallelepiped energy with hole (exchange interaction and easy-axis anisotropy)\n";
void test_anisotropy_energy_5(void);

#endif
#ifndef _FUNC_ANISOTROPY_TEST_H
#define _FUNC_ANISOTROPY_TEST_H

int init_func_anisotropy_test(void);
int clean_func_anisotropy_test(void);

/* uniform exchange energy and easy-axis anisotropy for chain (simulation) */
char * test_anisotropy_simulation_1_desc = "UniX test: 2-spin chain simulation (exchange interaction and easy-axis anisotropy)\n";
void test_anisotropy_simulation_1(void);

/* uniform exchange energy and easy-axis anisotropy for plain (simulation) */
char * test_anisotropy_simulation_2_desc = "UniX test: 4-spin plain simulation (exchange interaction and easy-axis anisotropy)\n";
void test_anisotropy_simulation_2(void);

/* uniform exchange energy and easy-axis anisotropy for cube (simulation) */
char * test_anisotropy_simulation_3_desc = "UniX test: 8-spin cube simulation(exchange interaction and easy-axis anisotropy)\n";
void test_anisotropy_simulation_3(void);

/* uniform exchange energy and easy-axis anisotropy for cube with random magnetic moments (simulation) */
char * test_anisotropy_simulation_4_desc = "UniX test: 8-spin cube simulation with random magnetic moments (exchange interaction and easy-axis anisotropy)\n";
void test_anisotropy_simulation_4(void);

#endif
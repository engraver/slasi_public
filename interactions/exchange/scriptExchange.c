/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include <Python.h>
#include "scriptExchange.h"
#include "../../alltypes.h"

/* initialize exchange integral coefficients */
int getExchange(void) {

	/* declarations of objects to work with Python.h */
	PyObject *pArgs, *pFunc;
	PyObject *pValue;

	/* declarations of variable */
	double J1, J2, J3, J4, J5, J6;
	int tester;

	/* check if module was found */
	if (pModulePython != NULL) {

		/* search of function wint name "getDMI" in module */
		pFunc = PyObject_GetAttrString(pModulePython, "getExchange");
		/* check if function was found */
		if (pFunc && PyCallable_Check(pFunc)) {

			/* pass through each magnetic moment */
			for (int i1 = 0; i1 < userVars.N1; i1++) {
				for (int i2 = 0; i2 < userVars.N2; i2++) {
					for (int i3 = 0; i3 < userVars.N3; i3++) {
						if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
						    i3 < userVars.N3 - 1) {

							/* number of spin in lattice */
							int indexLattice = IDX(i1, i2, i3);
							/* check if node is magnetic */
							if (latticeParam[indexLattice].magnetic) {

								/* creating parameters to call function (coordinates and indices) */
								pArgs = PyTuple_New(6);

								/* the first coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].x) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "(file %s | line %d) Cannot convert arguments for function getExchange!\n", __FILE__, __LINE__);
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 0, pValue);

								/* the second coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].y) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "(file %s | line %d) Cannot convert arguments for function getExchange!\n", __FILE__, __LINE__);
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 1, pValue);

								/* the third coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].z) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "(file %s | line %d) Cannot convert arguments for function getExchange!\n", __FILE__, __LINE__);
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 2, pValue);

								/* the first index */
								pValue = PyLong_FromLong(i1);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "(file %s | line %d) Cannot convert arguments for function getExchange!\n", __FILE__, __LINE__);
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 3, pValue);

								/* the second index */
								pValue = PyLong_FromLong(i2);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "(file %s | line %d) Cannot convert arguments for function getExchange!\n", __FILE__, __LINE__);
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 4, pValue);

								/* the third index */
								pValue = PyLong_FromLong(i3);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "(file %s | line %d) Cannot convert arguments for function getExchange!\n", __FILE__, __LINE__);
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 5, pValue);

								/* call function */
								pValue = PyObject_CallObject(pFunc, pArgs);
								/* check results of calling */
								if (pValue != NULL) {

									/* get results of calling */
									tester = PyArg_ParseTuple(pValue, "dddddd", &J1, &J2, &J3, &J4, &J5, &J6);
									/* check if it was able to get */
									if (tester) {
										/* save results for cubic or triangular lattice */
										if (userVars.latticeType == LATTICE_CUBIC) {
											latticeParam[indexLattice].Jm1 = J1;
											latticeParam[indexLattice].Jp1 = J2;
											latticeParam[indexLattice].Jm2 = J3;
											latticeParam[indexLattice].Jp2 = J4;
											latticeParam[indexLattice].Jm3 = J5;
											latticeParam[indexLattice].Jp3 = J6;
										} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
											latticeParam[indexLattice].J1 = J1;
											latticeParam[indexLattice].J2 = J2;
											latticeParam[indexLattice].J3 = J3;
											latticeParam[indexLattice].J4 = J4;
											latticeParam[indexLattice].J5 = J5;
											latticeParam[indexLattice].J6 = J6;
										}
									} else {
										/* output error */
										PyErr_Print();
										fprintf(stderr, "(file %s | line %d) Call of python function getExchange failed!\n", __FILE__, __LINE__);
										return 1;
									}
									Py_DECREF(pValue);

								} else {
									/* output error */
									Py_DECREF(pFunc);
									Py_DECREF(pModulePython);
									PyErr_Print();
									fprintf(stderr, "(file %s | line %d) Call of python function getExchange failed!\n", __FILE__, __LINE__);
									return 1;
								}

							}

						}
					}
				}
			}

		} else {
			/* output error */
			if (PyErr_Occurred())
				PyErr_Print();
			fprintf(stderr, "(file %s | line %d) Cannot find function getExchange in python file!\n", __FILE__, __LINE__);
			return 1;
		}
		Py_XDECREF(pFunc);

	} else {
		/* output error */
		PyErr_Print();
		fprintf(stderr, "(file %s | line %d) Failed to load python file %s!\n", __FILE__, __LINE__, userVars.scriptFile);
		return 1;
	}

	return 0;

}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "uniformexchange_triangle.h"
#include "../../utils/parallel_utils.h"

void uniformExchangeFieldTriangle(int i1, int i2, size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyExchange) {

	/* neighbor's indexes */
	size_t neighIndex1, neighIndex2, neighIndex3, neighIndex4, neighIndex5, neighIndex6;

	/* sign of exchange */
	int signJ = userVars.J > 0 ? 1 : -1;

	/* calculate neighbor's indices */
	neighIndex1 = IDXtrmg1(i1, i2);
	neighIndex2 = IDXtrmg2(i1, i2);
	neighIndex3 = IDXtrmg3(i1, i2);
	neighIndex4 = IDXtrmg4(i1, i2);
	neighIndex5 = IDXtrmg5(i1, i2);
	neighIndex6 = IDXtrmg6(i1, i2);

	/* calculate effective field */
	double effectFieldx = signJ * (lattice[neighIndex1] + lattice[neighIndex2] + lattice[neighIndex3] +
	                               lattice[neighIndex4] + lattice[neighIndex5] + lattice[neighIndex6]);
	double effectFieldy = signJ * (lattice[neighIndex1 + 1] + lattice[neighIndex2 + 1] + lattice[neighIndex3 + 1] +
	                               lattice[neighIndex4 + 1] + lattice[neighIndex5 + 1] + lattice[neighIndex6 + 1]);
	double effectFieldz = signJ * (lattice[neighIndex1 + 2] + lattice[neighIndex2 + 2] + lattice[neighIndex3 + 2] +
	                               lattice[neighIndex4 + 2] + lattice[neighIndex5 + 2] + lattice[neighIndex6 + 2]);

	/* calculate energy of exchange */
	latticeParam[indexLattice].EnergyExchangeNorm =
			(-0.5) * (effectFieldx * lattice[curIndex] + effectFieldy * lattice[curIndex + 1] +
			          effectFieldz * lattice[curIndex + 2]);
	*EnergyExchange += latticeParam[indexLattice].EnergyExchangeNorm;

	/* add generated effective field to the total */
	*Hx += effectFieldx;
	*Hy += effectFieldy;
	*Hz += effectFieldz;

}

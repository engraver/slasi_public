#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "CUnit/Basic.h"

#include "../../alltypes.h"
#include "../../simulation.h"
#include "uniformexchange.h"

/* names of configuration files for testing */
#define FILE_EXCHENER_CFG1 "testexchener1.cfg"
#define FILE_EXCHENER_DAT1 "testexchener1.dat"
#define FILE_EXCHENER_BAS1 "testexchener1"

#define FILE_EXCHENER_CFG2 "testexchener2.cfg"
#define FILE_EXCHENER_DAT2 "testexchener2.dat"
#define FILE_EXCHENER_BAS2 "testexchener2"

#define FILE_EXCHENER_CFG3 "testexchener3.cfg"
#define FILE_EXCHENER_DAT3 "testexchener3.dat"
#define FILE_EXCHENER_BAS3 "testexchener3"

#define FILE_EXCHENER_CFG4 "testexchener4.cfg"
#define FILE_EXCHENER_DAT4 "testexchener4.dat"
#define FILE_EXCHENER_BAS4 "testexchener4"

#define FILE_EXCHENER_CFG5 "testexchener5.cfg"
#define FILE_EXCHENER_DAT5 "testexchener5.dat"
#define FILE_EXCHENER_BAS5 "testexchener5"

#define FILE_EXCHENER_CFG6 "testexchener6.cfg"
#define FILE_EXCHENER_DAT6 "testexchener6.dat"
#define FILE_EXCHENER_BAS6 "testexchener6"

#define FILE_EXCHENER_CFG7 "testexchener7.cfg"
#define FILE_EXCHENER_DAT7 "testexchener7.dat"
#define FILE_EXCHENER_BAS7 "testexchener7"

#define FILE_EXCHENER_CFG8 "testexchener8.cfg"
#define FILE_EXCHENER_DAT8 "testexchener8.dat"
#define FILE_EXCHENER_BAS8 "testexchener8"

#define PREC_TEST_EXCH 0.000001

/* initialization function for tests */
int init_uniformexchange_test(void) {
	return 0;
}

/* function of cleaning for tests */
int clean_uniformexchange_test(void) {
	return 0;
}

/* function to create files to test magnetic simulator (for exchange interaction) */
void createExchFiles(char * nameConfFile, char * nameParamFile, char * rowConfFile, char * rowParamFile) {

	/* create configuration file */
	FILE * fid1 = fopen(nameConfFile, "w");
	fprintf(fid1, "%s", rowConfFile);
	fclose(fid1);

	/* create parameter file */
	FILE * fid2 = fopen(nameParamFile, "w");
	fprintf(fid2, "%s", rowParamFile);
	fclose(fid2);

}

/* function to launch simulation (for exchange interaction) */
void launchExchSimulation(char * nameConfFile, char * nameParamFile) {

	/* make parameters for launching */
	int argc = 4;
	char **argv;
	argv = (char**) malloc(sizeof(char**) * 4);
	argv[0] = (char*) malloc(sizeof("slasi"));
	argv[0] = "slasi";
	argv[1] = (char*) malloc(sizeof(nameConfFile));
	argv[1] = nameConfFile;
	argv[2] = (char*) malloc(sizeof("-p"));
	argv[2] = "-p";
	argv[3] = (char*) malloc(sizeof(nameParamFile));
	argv[3] = nameParamFile;

	/* launch simulation */
	slasi(argc, argv, false);

}

/* function to delete which are created during checking */
void deleteExchFiles(char * nameConfFile, char * nameParamFile, char * baseName) {

	/* delete parameter and configuration files */
	remove(nameConfFile);
	remove(nameParamFile);

	/* delete energy logfile which are created during simulation */
	char energy_log [100];
	strcpy(energy_log, baseName);
	strcat(energy_log, "_energy.log");
	remove(energy_log);

	/* delete logfile of initialization which are created during simulation */
	char init_log [100];
	strcpy(init_log, baseName);
	strcat(init_log, "_init.log");
	remove(init_log);

	/* delete logfile of magnetism which are created during simulation */
	char mag_log [100];
	strcpy(mag_log, baseName);
	strcat(mag_log, "_mag.log");
	remove(mag_log);

	/* delete logfile for other parameters which are created during simulation */
	char other_log [100];
	strcpy(other_log, baseName);
	strcat(other_log, "_other.log");
	remove(other_log);

	/* delete file for table of parameters */
	char table_file [100];
	strcpy(table_file, baseName);
	strcat(table_file, ".tbl");
	remove(table_file);

	/* delete the first file of intermediate results */
	char file_inter1 [100];
	strcpy(file_inter1, baseName);
	strcat(file_inter1, ".00000.slsb");
	remove(file_inter1);

	/* delete the second file of intermediate results */
	char file_inter2 [100];
	strcpy(file_inter2, baseName);
	strcat(file_inter2, ".00000.inp");
	remove(file_inter2);

	/* delete the third file of intermediate results */
	char file_inter3 [100];
	strcpy(file_inter3, baseName);
	strcat(file_inter3, ".00001.slsb");
	remove(file_inter3);

	/* delete the fourth file of intermediate results */
	char file_inter4 [100];
	strcpy(file_inter4, baseName);
	strcat(file_inter4, ".00001.inp");
	remove(file_inter4);

	/* delete the restart file of intermediate results */
	char file_restart [100];
	strcpy(file_restart, baseName);
	strcat(file_restart, ".restart.slsb");
	remove(file_restart);

}

/*
 * @brief yet another test of exchange interaction
 *
 * chain of two magnetic moments with only exchange interaction
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3
 * 000 000 000 000
 * 000 0+0 0+0 000
 * 000 000 000 000
 * @endverbatim
 * Here, + magnetic site with initial value (1,0,0).
 * The magnetic system only has an exchange interaction,
 * so the magnetic moments should be parallel and
 * have normalizing total exchange energy 1.0 (ExchangeEnergy + NumberOfSpins)
 */
void test_uniformexchange_energy_1(void) {

	/* correct energy */
	double EnergyCorr = 1.0;

	/* create files to test magnetic simulator */
	createExchFiles(FILE_EXCHENER_CFG1, FILE_EXCHENER_DAT1, "N1 = 2\nN2 = 1\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\n", "mx my mz x y z\n1 0 0 0 0 0\n1 0 0 1 0 0\n");

	/* launch simulation */
	launchExchSimulation(FILE_EXCHENER_CFG1, FILE_EXCHENER_DAT1);

	/* check energy */
	printf("%f", EnergyExchange);
	CU_ASSERT(fabs(EnergyExchangeNorm - EnergyCorr) < PREC_TEST_EXCH);

	/* delete created files */
	deleteExchFiles(FILE_EXCHENER_CFG1, FILE_EXCHENER_DAT1, FILE_EXCHENER_BAS1);

}

/*
 * @brief yet another test of exchange interaction
 *
 * plane of four magnetic moments with only exchange interaction
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3
 * 000 000 000 000
 * 000 0+0 0+0 000
 * 000 0+0 0+0 000
 * 000 000 000 000
 * @endverbatim
 * Here, + magnetic site with initial value (1,0,0).
 * The magnetic system only has an exchange interaction,
 * so the magnetic moments should be parallel and
 * have normalizing total exchange energy 0.0 (ExchangeEnergy + NumberOfSpins)
 */
void test_uniformexchange_energy_2(void) {

	/* correct energy */
	double EnergyCorr = 0.0;

	/* create files to test magnetic simulator */
	createExchFiles(FILE_EXCHENER_CFG2, FILE_EXCHENER_DAT2, "N1 = 2\nN2 = 2\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\n", "mx my mz x y z\n1 0 0 0 0 0\n1 0 0 1 0 0\n1 0 0 0 1 0\n1 0 0 1 1 0\n");

	/* launch simulation */
	launchExchSimulation(FILE_EXCHENER_CFG2, FILE_EXCHENER_DAT2);

	/* check energy */
	CU_ASSERT(fabs(EnergyExchangeNorm - EnergyCorr) < PREC_TEST_EXCH);

	/* delete created files */
	deleteExchFiles(FILE_EXCHENER_CFG2, FILE_EXCHENER_DAT2, FILE_EXCHENER_BAS2);

}

/*
 * @brief yet another test of exchange interaction
 *
 * cube of eight magnetic moments with only exchange interaction
 *
 * @verbatim
 * Location of magnetic moment components in an array
 * 0    1    2    3
 * 0000 0000 0000 0000
 * 0000 0++0 0++0 0000
 * 0000 0++0 0++0 0000
 * 0000 0000 0000 0000
 * @endverbatim
 * Here, + magnetic site with initial value (1,0,0).
 * The magnetic system only has an exchange interaction,
 * so the magnetic moments should be parallel and
 * have normalizing total exchange energy -4.0 (ExchangeEnergy + NumberOfSpins)
 */
void test_uniformexchange_energy_3(void) {

	/* correct energy */
	double EnergyCorr = -4.0;

	/* create files to test magnetic simulator */
	createExchFiles(FILE_EXCHENER_CFG3, FILE_EXCHENER_DAT3, "N1 = 2\nN2 = 2\nN3 = 2\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\n", "mx my mz x y z\n0 1 0 0 0 0\n0 1 0 1 0 0\n0 1 0 0 1 0\n0 1 0 1 1 0\n0 1 0 0 0 1\n0 1 0 1 0 1\n0 1 0 0 1 1\n0 1 0 1 1 1\n");

	/* launch simulation */
	launchExchSimulation(FILE_EXCHENER_CFG3, FILE_EXCHENER_DAT3);

	/* check energy */
	CU_ASSERT(fabs(EnergyExchangeNorm - EnergyCorr) < PREC_TEST_EXCH);

	/* delete created files */
	deleteExchFiles(FILE_EXCHENER_CFG3, FILE_EXCHENER_DAT3, FILE_EXCHENER_BAS3);

}

/*
 * @brief yet another test of exchange interaction
 *
 * chain of two magnetic moments with only exchange interaction
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3   4
 * 000 000 000 000 000
 * 000 0+0 0-0 0+0 000
 * 000 000 000 000 000
 * @endverbatim
 * Here, + magnetic site with initial value (1,0,0) and - non-magnetic site (0,0,0).
 * The magnetic system only has an exchange interaction,
 * so the magnetic moments should stay their directions and
 * have normalizing total exchange energy 2.0 (ExchangeEnergy + NumberOfSpins)
 */
void test_uniformexchange_energy_4(void) {

	/* correct energy */
	double EnergyCorr = 2.0;

	/* create files to test magnetic simulator */
	createExchFiles(FILE_EXCHENER_CFG4, FILE_EXCHENER_DAT4, "N1 = 3\nN2 = 1\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\ntypeMagn = file\n", "mx my mz x y z magn\n1 0 0 0 0 0 1\n0 0 0 1 0 0 0\n1 0 0 2 0 0 1\n");

	/* launch simulation */
	launchExchSimulation(FILE_EXCHENER_CFG4, FILE_EXCHENER_DAT4);

	/* check energy */
	CU_ASSERT(fabs(EnergyExchangeNorm - EnergyCorr) < PREC_TEST_EXCH);

	/* delete created files */
	deleteExchFiles(FILE_EXCHENER_CFG4, FILE_EXCHENER_DAT4, FILE_EXCHENER_BAS4);

}

/*
 * @brief yet another test of exchange interaction
 *
 * plane of four magnetic moments with only exchange interaction
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3
 * 000 000 000 000
 * 000 0+0 0+0 000
 * 000 0+0 0-0 000
 * 000 000 000 000
 * @endverbatim
 * Here, + magnetic site with initial value (1,0,0) and - non-magnetic site (0,0,0).
 * The magnetic system only has an exchange interaction,
 * so the magnetic moments should stay their directions and
 * have normalizing total exchange energy 2.0 (ExchangeEnergy + NumberOfSpins)
 */
void test_uniformexchange_energy_5(void) {

	/* correct energy */
	double EnergyCorr = 1.0;

	/* create files to test magnetic simulator */
	createExchFiles(FILE_EXCHENER_CFG5, FILE_EXCHENER_DAT5, "N1 = 2\nN2 = 2\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\ntypeMagn = file\n", "mx my mz x y z magn\n1 0 0 0 0 0 1\n1 0 0 1 0 0 1\n1 0 0 0 1 0 1\n0 0 0 1 1 0 0\n");

	/* launch simulation */
	launchExchSimulation(FILE_EXCHENER_CFG5, FILE_EXCHENER_DAT5);

	/* check energy */
	CU_ASSERT(fabs(EnergyExchangeNorm - EnergyCorr) < PREC_TEST_EXCH);

	/* delete created files */
	deleteExchFiles(FILE_EXCHENER_CFG5, FILE_EXCHENER_DAT5, FILE_EXCHENER_BAS5);

}

/*
 * @brief yet another test of exchange interaction
 *
 * cube of eight magnetic moments with only exchange interaction
 *
 * @verbatim
 * Location of magnetic moment components in an array
 * 0    1    2    3
 * 0000 0000 0000 0000
 * 0000 0++0 0++0 0000
 * 0000 0++0 0+-0 0000
 * 0000 0000 0000 0000
 * @endverbatim
 * Here, + magnetic site with initial value (1,0,0) and - non-magnetic site (0,0,0).
 * The magnetic system only has an exchange interaction,
 * so the magnetic moments should stay their directions and
 * have normalizing total exchange energy -1.0 (ExchangeEnergy + NumberOfSpins)
 */
void test_uniformexchange_energy_6(void) {

	/* correct energy */
	double EnergyCorr = -2.0;

	/* create files to test magnetic simulator */
	createExchFiles(FILE_EXCHENER_CFG6, FILE_EXCHENER_DAT6, "N1 = 2\nN2 = 2\nN3 = 2\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\ntypeMagn = file\n", "mx my mz x y z magn\n0 1 0 0 0 0 1\n0 1 0 1 0 0 1\n0 1 0 0 1 0 1\n0 1 0 1 1 0 1\n0 1 0 0 0 1 1\n0 1 0 1 0 1 1\n0 1 0 0 1 1 1\n0 0 0 1 1 1 0\n");

	/* launch simulation */
	launchExchSimulation(FILE_EXCHENER_CFG6, FILE_EXCHENER_DAT6);

	/* check energy */
	CU_ASSERT(fabs(EnergyExchangeNorm - EnergyCorr) < PREC_TEST_EXCH);

	/* delete created files */
	deleteExchFiles(FILE_EXCHENER_CFG6, FILE_EXCHENER_DAT6, FILE_EXCHENER_BAS6);

}

/*
 * @brief yet another test of exchange interaction
 *
 * plane of eight magnetic moments with only exchange interaction
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3   4
 * 000 000 000 000 000
 * 000 0+0 0+0 0+0 000
 * 000 0+0 0-0 0+0 000
 * 000 0+0 0+0 0+0 000
 * 000 000 000 000 000
 * @endverbatim
 * Here, + magnetic site with initial value (1,0,0) and - non-magnetic site (0,0,0).
 * The magnetic system only has an exchange interaction,
 * so the magnetic moments should stay their directions and
 * have normalizing total exchange energy 0.0 (ExchangeEnergy + NumberOfSpins)
 */
void test_uniformexchange_energy_7(void) {

	/* correct energy */
	double EnergyCorr = 0.0;

	/* create files to test magnetic simulator */
	createExchFiles(FILE_EXCHENER_CFG7, FILE_EXCHENER_DAT7, "N1 = 3\nN2 = 3\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\ntypeMagn = file\n", "mx my mz x y z magn\n1.0 0.0 0.0 1 1 1 1\n1.0 0.0 0.0 1 2 1 1\n1.0 0.0 0.0 1 3 1 1\n1.0 0.0 0.0 2 1 1 1\n0.0 0.0 0.0 2 2 1 0\n1.0 0.0 0.0 2 3 1 1\n1.0 0.0 0.0 3 1 1 1\n1.0 0.0 0.0 3 2 1 1\n1.0 0.0 0.0 3 3 1 1\n");

	/* launch simulation */
	launchExchSimulation(FILE_EXCHENER_CFG7, FILE_EXCHENER_DAT7);

	/* check energy */
	CU_ASSERT(fabs(EnergyExchangeNorm - EnergyCorr) < PREC_TEST_EXCH);

	/* delete created files */
	deleteExchFiles(FILE_EXCHENER_CFG7, FILE_EXCHENER_DAT7, FILE_EXCHENER_BAS7);

}

/*
 * @brief yet another test of exchange interaction
 *
 * parallelepiped of sixteen magnetic moments with only exchange interaction
 *
 * @verbatim
 * Location of magnetic moment components in an array
 * 0    1    2    3    4
 * 0000 0000 0000 0000 0000
 * 0000 0++0 0++0 0++0 0000
 * 0000 0++0 0--0 0++0 0000
 * 0000 0++0 0++0 0++0 0000
 * 0000 0000 0000 0000 0000
 * @endverbatim
 * Here, + magnetic site with initial value (1,0,0) and - non-magnetic site (0,0,0).
 * The magnetic system only has an exchange interaction,
 * so the magnetic moments should stay their directions and
 * have normalizing total exchange energy -8.0 (ExchangeEnergy + NumberOfSpins)
 */
void test_uniformexchange_energy_8(void) {

	/* correct energy */
	double EnergyCorr = -8.0;

	/* create files to test magnetic simulator */
	createExchFiles(FILE_EXCHENER_CFG8, FILE_EXCHENER_DAT8, "N1 = 3\nN2 = 3\nN3 = 2\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\ntypeMagn = file\n", "mx my mz x y z magn\n1.0 0.0 0.0 1 1 1 1\n1.0 0.0 0.0 1 1 2 1\n1.0 0.0 0.0 1 2 1 1\n1.0 0.0 0.0 1 2 2 1\n1.0 0.0 0.0 1 3 1 1\n1.0 0.0 0.0 1 3 2 1\n1.0 0.0 0.0 2 1 1 1\n1.0 0.0 0.0 2 1 2 1\n0.0 0.0 0.0 2 2 1 0\n0.0 0.0 0.0 2 2 2 0\n1.0 0.0 0.0 2 3 1 1\n1.0 0.0 0.0 2 3 2 1\n1.0 0.0 0.0 3 1 1 1\n1.0 0.0 0.0 3 1 2 1\n1.0 0.0 0.0 3 2 1 1\n1.0 0.0 0.0 3 2 2 1\n1.0 0.0 0.0 3 3 1 1\n1.0 0.0 0.0 3 3 2 1\n");

	/* launch simulation */
	launchExchSimulation(FILE_EXCHENER_CFG8, FILE_EXCHENER_DAT8);

	/* check energy */
	CU_ASSERT(fabs(EnergyExchangeNorm - EnergyCorr) < PREC_TEST_EXCH);

	/* delete created files */
	deleteExchFiles(FILE_EXCHENER_CFG8, FILE_EXCHENER_DAT8, FILE_EXCHENER_BAS8);

}


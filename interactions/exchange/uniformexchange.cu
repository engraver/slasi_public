#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../../alltypes.h"
extern "C"{
	#include "uniformexchange.h"
}

/* exchange interaction for one site (GPU) */
__global__
void spinExchangeUniformField(double *d_H, double *d_lattice, double *d_EnergyTotal, double J, long N2Ext, long N3Ext, long numberSites) {

	/* neighbor's indexes */
	long p1, m1, p2, m2, p3, m3;
	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;
	/* local effective field */
	double HxLocal, HyLocal, HzLocal;
	/* sign of exchange */
	int signJ = J > 0 ? 1 : -1;

	/* check boundary condition for threads */
	if ((indexLattice < numberSites) &&
	    (d_lattice[curIndex] * d_lattice[curIndex] + d_lattice[curIndex + 1] * d_lattice[curIndex + 1] + d_lattice[curIndex + 2] * d_lattice[curIndex + 2] > LENG_ZERO)) {

		/* calculate neighbor's indices */
		p1 = curIndex + 3;
		p2 = curIndex + 3 * N3Ext;
		p3 = curIndex + 3 * N3Ext * N2Ext;
		m1 = curIndex - 3;
		m2 = curIndex - 3 * N3Ext;
		m3 = curIndex - 3 * N3Ext * N2Ext;

		/* calculate effective field */
		HxLocal = signJ * (d_lattice[p1] + d_lattice[p2] + d_lattice[p3] + d_lattice[m1] + d_lattice[m2] + d_lattice[m3]);
		HyLocal = signJ * (d_lattice[p1 + 1] + d_lattice[p2 + 1] + d_lattice[p3 + 1] + d_lattice[m1 + 1] + d_lattice[m2 + 1] + d_lattice[m3 + 1]);
		HzLocal = signJ * (d_lattice[p1 + 2] + d_lattice[p2 + 2] + d_lattice[p3 + 2] + d_lattice[m1 + 2] + d_lattice[m2 + 2] + d_lattice[m3 + 2]);

		/* calculate energy of this spin and add to the total energy  */
		d_EnergyTotal[indexLattice] = (-1.0) *
				(HxLocal * d_lattice[curIndex] + HyLocal * d_lattice[curIndex + 1] + HzLocal * d_lattice[curIndex + 2]);

		/* add partial effective field to total field */
		d_H[curIndex] += HxLocal;
		d_H[curIndex + 1] += HyLocal;
		d_H[curIndex + 2] += HzLocal;

	}

}

extern "C"
void uniformExchangeFieldCUDA(double *d_H, double *d_EnergyTotal, double J, long N2Ext, long N3Ext, long numberSites) {

	/* setup the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* wrapper of CUDA function */
	spinExchangeUniformField<<<cores, threads>>>(d_H, d_lattice, d_EnergyTotal, J, N2Ext, N3Ext, numberSites);

}
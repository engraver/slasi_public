#ifndef _FUNC_UNIFORMEXCHANGE_TEST_H
#define _FUNC_UNIFORMEXCHANGE_TEST_H

int init_func_uniformexchange_test(void);
int clean_func_uniformexchange_test(void);

/* uniform exchange simulation for chain */
char * test_uniformexchange_simulation_1_desc = "UniX test: simulation of 2-spin chain (exchange interaction)\n";
void test_uniformexchange_simulation_1(void);

/* uniform exchange simulation for plain */
char * test_uniformexchange_simulation_2_desc = "UniX test: simulation of 4-spin plane(exchange interaction)\n";
void test_uniformexchange_simulation_2(void);

/* uniform exchange simulation for cube */
char * test_uniformexchange_simulation_3_desc = "UniX test: simulation of 8-spin cube (exchange interaction)\n";
void test_uniformexchange_simulation_3(void);

/* uniform exchange simulation for cube with random magnetic moments */
char * test_uniformexchange_simulation_4_desc = "UniX test: simulation of 8-spin cube with random initial magnetic moments (exchange interaction)\n";
void test_uniformexchange_simulation_4(void);

#endif
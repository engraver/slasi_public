#ifndef INFOMEXCHANGE_H
#define INFOMEXCHANGE_H

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../../alltypes.h"
#include "../interactions.h"
#include "../../utils/indices.h"
#include "../../utils/parallel_utils.h"

/**
 * @brief Setup inhomogeneous exchange
 *
 * @param numbers array with values from paramFile
 * @param indlp index for latticeParam array
 * @param indn index for numbers array
 *
 * @return 0 if ok, otherwise nonzero value
 */
int setupInhomogeneousExchange(double **numbers, int indlp, int indn);

/**
 * @brief This function returns effective field and energy
 * which created by inhomogeneous exchange interaction of surrounding spins with the same exchange constants
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Hx effective field on axis x
 * @param Hy effective field on axis y
 * @param Hz effective field on axis z
 * @param EnergyExchange normalized exchange energy
 */
void inhomogeneousExchangeField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyExchange);

/**
 * @brief This function returns effective field and energy
 * which created by inhomogeneous exchange interaction of surrounding spins with the same exchange constants for triangle lattice
 * @param i1 the first index of node in the lattice
 * @param i2 the second index of node in the lattice
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Hx effective field on axis x
 * @param Hy effective field on axis y
 * @param Hz effective field on axis z
 * @param EnergyExchange normalized exchange energy
 */
void inhomogeneousExchangeFieldTriangle(int i1, int i2, size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyExchange);

/**
 * @brief This function returns effective field and energy (CUDA)
 * which created by inhomogeneous exchange interaction of surrounding spins with the same exchange constants (GPU)
 * @param d_H is array of effective field (GPU)
 * @param d_latticeParam is array of lattice site parameters
 * @param d_EnergyTotal is array of energy for sites (GPU)
 * @param N2Ext is number of sites along the second axis
 * @param N3Ext is number of sites along the third axis
 * @param numberSites is number of sites
 */
void inhomogeneousExchangeFieldCUDA(double *d_H,  LatticeSiteDescr *d_latticeParam, double *d_EnergyTotal, long N2Ext, long N3Ext, long numberSites);

/**
 * @brief This function returns effective field and energy (CUDA)
 * which created by inhomogeneous exchange interaction of surrounding spins with the same exchange constants (GPU)
 * @param d_H is array of effective field (GPU)
 * @param d_latticeParam is array of lattice site parameters
 * @param d_EnergyTotal is array of energy for sites (GPU)
 * @param N2Ext is number of sites along the second axis
 * @param N3Ext is number of sites along the third axis
 * @param numberSites is number of sites
 */
void inhomogeneousExchangeFieldTriangleCUDA(double *d_H, LatticeSiteDescr *d_latticeParam, double *d_EnergyTotal, long N2Ext, long N3Ext, long numberSites);

#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "CUnit/Basic.h"

#include "../../alltypes.h"
#include "../../simulation.h"
#include "uniformexchange.h"

/* names of configuration files for testing */
#define FILE_EXCH_CFG1 "testexch1.cfg"
#define FILE_EXCH_DAT1 "testexch1.dat"
#define FILE_EXCH_BAS1 "testexch1"

#define FILE_EXCH_CFG2 "testexch2.cfg"
#define FILE_EXCH_DAT2 "testexch2.dat"
#define FILE_EXCH_BAS2 "testexch2"

#define FILE_EXCH_CFG3 "testexch3.cfg"
#define FILE_EXCH_DAT3 "testexch3.dat"
#define FILE_EXCH_BAS3 "testexch3"

#define FILE_EXCH_CFG4 "testexch4.cfg"
#define FILE_EXCH_DAT4 "testexch4.dat"
#define FILE_EXCH_BAS4 "testexch4"

#define PREC_FUNC_TEST_EXCH 0.000001

/* initialization function for tests */
int init_func_uniformexchange_test(void) {
	return 0;
}

/* function of cleaning for tests */
int clean_func_uniformexchange_test(void) {
	return 0;
}

/* function to create files to test magnetic simulator (for exchange interaction) */
void createFuncExchFiles(char * nameConfFile, char * nameParamFile, char * rowConfFile, char * rowParamFile) {

	/* create configuration file */
	FILE * fid1 = fopen(nameConfFile, "w");
	fprintf(fid1, "%s", rowConfFile);
	fclose(fid1);

	/* create parameter file */
	FILE * fid2 = fopen(nameParamFile, "w");
	fprintf(fid2, "%s", rowParamFile);
	fclose(fid2);

}

/* function to launch simulation (for exchange interaction) */
void launchFuncExchSimulation(char * nameConfFile, char * nameParamFile) {

	/* make parameters for launching */
	int argc = 4;
	char **argv;
	argv = (char**) malloc(sizeof(char**) * 4);
	argv[0] = (char*) malloc(sizeof("slasi"));
	argv[0] = "slasi";
	argv[1] = (char*) malloc(sizeof(nameConfFile));
	argv[1] = nameConfFile;
	argv[2] = (char*) malloc(sizeof("-p"));
	argv[2] = "-p";
	argv[3] = (char*) malloc(sizeof(nameParamFile));
	argv[3] = nameParamFile;

	/* launch simulation */
	slasi(argc, argv, false);

}

/* function to check magnetic moments of system */
void checkExchMagnetism(int N1, int N2, int N3, double mx, double my, double mz) {

	/* go through all nodes */
	for (int i1 = 1; i1 <= N1; i1++) {
		for (int i2 = 1; i2 <= N2; i2++) {
			for (int i3 = 1; i3 <= N3; i3++) {

				/* check node */
				int indexMagn = IDXmg(i1, i2, i3);
				CU_ASSERT(fabs(lattice[indexMagn] - mx) < PREC_FUNC_TEST_EXCH);
				CU_ASSERT(fabs(lattice[indexMagn + 1] - my) < PREC_FUNC_TEST_EXCH);
				CU_ASSERT(fabs(lattice[indexMagn + 2] - mz) < PREC_FUNC_TEST_EXCH);

			}
		}
	}

}

/* function to delete which are created during checking */
void deleteFuncExchFiles(char * nameConfFile, char * nameParamFile, char * baseName) {

	/* delete parameter and configuration files */
	remove(nameConfFile);
	remove(nameParamFile);

	/* delete energy logfile which are created during simulation */
	char energy_log [100];
	strcpy(energy_log, baseName);
	strcat(energy_log, "_energy.log");
	remove(energy_log);

	/* delete logfile of initialization which are created during simulation */
	char init_log [100];
	strcpy(init_log, baseName);
	strcat(init_log, "_init.log");
	remove(init_log);

	/* delete logfile of magnetism which are created during simulation */
	char mag_log [100];
	strcpy(mag_log, baseName);
	strcat(mag_log, "_mag.log");
	remove(mag_log);

	/* delete logfile for other parameters which are created during simulation */
	char other_log [100];
	strcpy(other_log, baseName);
	strcat(other_log, "_other.log");
	remove(other_log);

	/* delete file for table of parameters */
	char table_file [100];
	strcpy(table_file, baseName);
	strcat(table_file, ".tbl");
	remove(table_file);

	/* delete the first file of intermediate results */
	char file_inter1 [100];
	strcpy(file_inter1, baseName);
	strcat(file_inter1, ".00000.slsb");
	remove(file_inter1);

	/* delete the second file of intermediate results */
	char file_inter2 [100];
	strcpy(file_inter2, baseName);
	strcat(file_inter2, ".00001.slsb");
	remove(file_inter2);

	/* delete the restart file of intermediate results */
	char file_restart [100];
	strcpy(file_restart, baseName);
	strcat(file_restart, ".restart.slsb");
	remove(file_restart);

}

/*
 * @brief yet another test of exchange interaction
 *
 * chain of two magnetic moments with only exchange interaction
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3
 * 000 000 000 000
 * 000 0+0 0+0 000
 * 000 000 000 000
 * @endverbatim
 * Here, + magnetic sites with initial value (0.707,0.707,0) and (1.0,0.0,0.0).
 * The magnetic system only has an exchange interaction,
 * so the magnetic moments should have value (0.923880,0.382683,0.0) because
 * exchange interaction wants to direct parallel magnetic moments
 */
void test_uniformexchange_simulation_1(void) {

	/* number of spins along axis */
	int N1 = 2;
	int N2 = 1;
	int N3 = 1;

	/* correct values of components of magnetic moments */
	double mx = 0.923880;
	double my = 0.382683;
	double mz = 0.0;

	/* create files to test magnetic simulator */
	createFuncExchFiles(FILE_EXCH_CFG1, FILE_EXCH_DAT1, "N1 = 2\nN2 = 1\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 100\nframe = 100\noutput = SLSB\n", "mx my mz x y z\n0.707 0.707 0 0 0 0\n1 0 0 1 0 0\n");

	/* launch simulation */
	launchFuncExchSimulation(FILE_EXCH_CFG1, FILE_EXCH_DAT1);

	/* check magnetic moments of sites */
	checkExchMagnetism(N1, N2, N3, mx, my, mz);

	/* delete created files */
	deleteFuncExchFiles(FILE_EXCH_CFG1, FILE_EXCH_DAT1, FILE_EXCH_BAS1);

}

/*
 * @brief yet another test of exchange interaction
 *
 * plane of four magnetic moments with only exchange interaction
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3
 * 000 000 000 000
 * 000 0+0 0+0 000
 * 000 0+0 0+0 000
 * 000 000 000 000
 * @endverbatim
 * Here, + magnetic sites with initial value (1.0,0.0,0.0),(0.707,0.707,0),(0.5,0.5,0.707) and (0.0,1.0,0.0).
 * The magnetic system only has an exchange interaction,
 * so the magnetic moments should have value (0.683709,0.691197,0.234070) because
 * exchange interaction wants to direct parallel magnetic moments
 */
void test_uniformexchange_simulation_2(void) {

	/* number of spins along axis */
	int N1 = 2;
	int N2 = 2;
	int N3 = 1;

	/* correct values of components of magnetic moments */
	double mx = 0.683709;
	double my = 0.691197;
	double mz = 0.234070;

	/* create files to test magnetic simulator */
	createFuncExchFiles(FILE_EXCH_CFG2, FILE_EXCH_DAT2, "N1 = 2\nN2 = 2\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 200\nframe = 200\noutput = SLSB\n", "mx my mz x y z\n1 0 0 0 0 0\n0.707 0.707 0 1 0 0\n0.5 0.5 0.707 0 1 0\n0 1 0 1 1 0\n");

	/* launch simulation */
	launchFuncExchSimulation(FILE_EXCH_CFG2, FILE_EXCH_DAT2);

	/* check magnetic moments of sites */
	checkExchMagnetism(N1, N2, N3, mx, my, mz);

	/* delete created files */
	deleteFuncExchFiles(FILE_EXCH_CFG2, FILE_EXCH_DAT2, FILE_EXCH_BAS2);

}


/*
 * @brief yet another test of exchange interaction
 *
 * cube of eight magnetic moments with only exchange interaction
 *
 * @verbatim
 * Location of magnetic moment components in an array
 * 0    1    2    3
 * 0000 0000 0000 0000
 * 0000 0++0 0++0 0000
 * 0000 0++0 0++0 0000
 * 0000 0000 0000 0000
 * @endverbatim
 * Here, + magnetic sites with initial value (1.0,0.0,0.0), (0.0,1.0,0.0), (0.0,0.0,1.0), (0.5,0.5,0.707),
 * (0.5,0.707,0.5), (0.707,0.5,0.5), (0.707,0.707,0.0), (0.707,0.0,0.707).
 * The magnetic system only has an exchange interaction,
 * so the magnetic moments should have value (0.653660,0.532080,0.538163) because
 * exchange interaction wants to direct parallel magnetic moments
 */
void test_uniformexchange_simulation_3(void) {

	/* number of spins along axis */
	int N1 = 2;
	int N2 = 2;
	int N3 = 2;

	/* correct values of components of magnetic moments */
	double mx = 0.653660;
	double my = 0.532080;
	double mz = 0.538163;

	/* create files to test magnetic simulator */
	createFuncExchFiles(FILE_EXCH_CFG3, FILE_EXCH_DAT3, "N1 = 2\nN2 = 2\nN3 = 2\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 400\nframe = 400\noutput = SLSB\n", "mx my mz x y z\n1 0 0 0 0 0\n0 1 0 1 0 0\n0 0 1 0 1 0\n0.5 0.5 0.707 1 1 0\n0.5 0.707 0.5 0 0 1\n0.707 0.5 0.5 1 0 1\n0.707 0.707 0.0 0 1 1\n0.707 0.0 0.707 1 1 1\n");

	/* launch simulation */
	launchFuncExchSimulation(FILE_EXCH_CFG3, FILE_EXCH_DAT3);

	/* check magnetic moments of sites */
	checkExchMagnetism(N1, N2, N3, mx, my, mz);

	/* delete created files */
	deleteFuncExchFiles(FILE_EXCH_CFG3, FILE_EXCH_DAT3, FILE_EXCH_BAS3);

}

/*
 * @brief yet another test of exchange interaction
 *
 * cube of eight magnetic moments with only exchange interaction
 *
 * @verbatim
 * Location of magnetic moment components in an array
 * 0    1    2    3    4    5
 * 0000 0000 0000 0000 0000 0000
 * 0000 0++0 0++0 0++0 0++0 0000
 * 0000 0++0 0++0 0++0 0++0 0000
 * 0000 0000 0000 0000 0000 0000
 * @endverbatim
 * Here, + magnetic site with random initial value.
 * The magnetic system only has an exchange interaction,
 * so the magnetic moments should have value (0.221554,0.292816,0.930147) because
 * exchange interaction wants to direct parallel magnetic moments
 */
void test_uniformexchange_simulation_4(void) {

	/* number of spins along axis */
	int N1 = 4;
	int N2 = 2;
	int N3 = 2;

	/* correct values of components of magnetic moments */
	double mx = 0.221554;
	double my = 0.292816;
	double mz = 0.930147;

	/* create files to test magnetic simulator */
	createFuncExchFiles(FILE_EXCH_CFG4, FILE_EXCH_DAT4, "N1 = 4\nN2 = 2\nN3 = 2\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 800\nframe = 800\noutput = SLSB\n", "mx my mz x y z\n0.881079 -0.345588 -0.322908 1 1 1\n-0.803590 0.331591 -0.494258 1 1 2\n-0.375824 -0.896516 0.234553 1 2 1\n0.113463 0.170433 0.978815 1 2 2\n-0.094322 0.284732 0.953955 2 1 1\n0.388846 -0.920063 -0.047774 2 1 2\n-0.456224 0.424891 0.781874 2 2 1\n-0.825027 0.163355 0.540968 2 2 2\n-0.207743 0.748251 0.630051 3 1 1\n0.166047 0.829407 -0.533397 3 1 2\n-0.382321 0.457028 0.803092 3 2 1\n0.633466 -0.741774 -0.220207 3 2 2\n-0.232093 0.761531 0.605147 4 1 1\n0.188968 0.675432 -0.712799 4 1 2\n-0.134890 -0.387763 -0.911836 4 2 1\n0.981475 0.176403 -0.074764 4 2 2\n");

	/* launch simulation */
	launchFuncExchSimulation(FILE_EXCH_CFG4, FILE_EXCH_DAT4);

	/* check magnetic moments of sites */
	checkExchMagnetism(N1, N2, N3, mx, my, mz);

	/* delete created files */
	deleteFuncExchFiles(FILE_EXCH_CFG4, FILE_EXCH_DAT4, FILE_EXCH_BAS4);

}


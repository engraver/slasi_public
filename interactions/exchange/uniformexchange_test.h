#ifndef _UNIFORMEXCHANGE_TEST_H
#define _UNIFORMEXCHANGE_TEST_H

int init_uniformexchange_test(void);
int clean_uniformexchange_test(void);

/* uniform exchange energy for chain */
char * test_uniformexchange_energy_1_desc = "UniX test: energy of 2-spin chain (exchange interaction)\n";
void test_uniformexchange_energy_1(void);

/* uniform exchange energy for plain */
char * test_uniformexchange_energy_2_desc = "UniX test: energy of 4-spin plain (exchange interaction)\n";
void test_uniformexchange_energy_2(void);

/* uniform exchange energy for cube */
char * test_uniformexchange_energy_3_desc = "UniX test: energy of 8-spin cube (exchange interaction)\n";
void test_uniformexchange_energy_3(void);

/* uniform exchange energy for chain with a break */
char * test_uniformexchange_energy_4_desc = "UniX test: energy of 2-spin chain with a break (exchange interaction)\n";
void test_uniformexchange_energy_4(void);

/* uniform exchange energy for plain with one nonmagnetic spin */
char * test_uniformexchange_energy_5_desc = "UniX test: energy of 4-spin plain with one nonmagnetic spin (exchange interaction)\n";
void test_uniformexchange_energy_5(void);

/* uniform exchange energy for cube with one nonmagnetic spin */
char * test_uniformexchange_energy_6_desc = "UniX test: energy of 8-spin cube with one nonmagnetic spin (exchange interaction)\n";
void test_uniformexchange_energy_6(void);

/* uniform exchange energy for rectangle with hole of nonmagnetic spin */
char * test_uniformexchange_energy_7_desc = "UniX test: energy of 8-spin rectangle with hole of nonmagnetic spin (exchange interaction)\n";
void test_uniformexchange_energy_7(void);

/* uniform exchange energy for parallelepiped with hole of nonmagnetic spin */
char * test_uniformexchange_energy_8_desc = "UniX test: energy of 16-spin parallelepiped with hole of nonmagnetic spin (exchange interaction)\n";
void test_uniformexchange_energy_8(void);

#endif

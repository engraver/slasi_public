#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
extern "C" {
	#include "inhomexchange.h"
}

/**
 * functions to find different indexes in array "lattice" and "latticeParam" (GPU version)
 * */

/** implementation of inline functions to find neighbours of current magnetic node (array "lattice") */

/** the first neighbour */
__device__ int IDXtrmg1GPUing(int i1, int i2, int N2, int N3) {
	int curIndex = 1 + N3 * (i2 + i1 * N2);
	return 3 * (curIndex + N2 * N3);
}

/** the second neighbour */
__device__ int IDXtrmg2GPUing(int i1, int i2, int N2, int N3) {
	int curIndex = 1 + N3 * (i2 + i1 * N2);
	return 3 * (curIndex + IsEVEN(i2) * N2 * N3 + N3);
}

/** the third neighbour */
__device__ int IDXtrmg3GPUing(int i1, int i2, int N2, int N3) {
	int curIndex = 1 + N3 * (i2 + i1 * N2);
	return 3 * (curIndex - IsODD(i2) * N2 * N3 + N3);
}

/** the fourth neighbour */
__device__ int IDXtrmg4GPUing(int i1, int i2, int N2, int N3) {
	int curIndex = 1 + N3 * (i2 + i1 * N2);
	return 3 * (curIndex - N2 * N3);
}

/** the fifth neighbour */
__device__ int IDXtrmg5GPUing(int i1, int i2, int N2, int N3) {
	int curIndex = 1 + N3 * (i2 + i1 * N2);
	return 3 * (curIndex - IsODD(i2) * N2 * N3 - N3);
}

/** the sixth neighbour */
__device__ int IDXtrmg6GPUing(int i1, int i2, int N2, int N3) {
	int curIndex = 1 + N3 * (i2 + i1 * N2);
	return 3 * (curIndex + IsEVEN(i2) * N2 * N3 - N3);
}

/* exchange inhomogeneous interaction for one site in triangular lattice (GPU) */
__global__
void spinExchangeInhomogeneousFieldTriangle(double *d_H, LatticeSiteDescr *d_latticeParam, double *d_lattice, double *d_EnergyTotal, long N2Ext, long N3Ext, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* check boundary condition for threads */
	if ((indexLattice < numberSites) &&
	    (d_lattice[curIndex] * d_lattice[curIndex] + d_lattice[curIndex + 1] * d_lattice[curIndex + 1] + d_lattice[curIndex + 2] * d_lattice[curIndex + 2] > LENG_ZERO)) {

		/* neighbor's indexes */
		size_t neighIndex1, neighIndex2, neighIndex3, neighIndex4, neighIndex5, neighIndex6;
		/* indexes in triangular lattice */
		long i1 = indexLattice / (N2Ext * N3Ext);
		long i2 = (indexLattice % (N2Ext * N3Ext)) / N3Ext;

		/* to debug code */
		//printf("%li %li %li\n", indexLattice, i1, i2);

		/* calculate neighbor's indices */
		neighIndex1 = IDXtrmg1GPUing(i1, i2, N2Ext, N3Ext);
		neighIndex2 = IDXtrmg2GPUing(i1, i2, N2Ext, N3Ext);
		neighIndex3 = IDXtrmg3GPUing(i1, i2, N2Ext, N3Ext);
		neighIndex4 = IDXtrmg4GPUing(i1, i2, N2Ext, N3Ext);
		neighIndex5 = IDXtrmg5GPUing(i1, i2, N2Ext, N3Ext);
		neighIndex6 = IDXtrmg6GPUing(i1, i2, N2Ext, N3Ext);

		/* calculate effective field */
		double HxLocal =          (d_latticeParam[indexLattice].J1 * d_lattice[neighIndex1] +
				                   d_latticeParam[indexLattice].J2 * d_lattice[neighIndex2] +
				                   d_latticeParam[indexLattice].J3 * d_lattice[neighIndex3] +
				                   d_latticeParam[indexLattice].J4 * d_lattice[neighIndex4] +
				                   d_latticeParam[indexLattice].J5 * d_lattice[neighIndex5] +
				                   d_latticeParam[indexLattice].J6 * d_lattice[neighIndex6]);
		double HyLocal =          (d_latticeParam[indexLattice].J1 * d_lattice[neighIndex1 + 1] +
				                   d_latticeParam[indexLattice].J2 * d_lattice[neighIndex2 + 1] +
				                   d_latticeParam[indexLattice].J3 * d_lattice[neighIndex3 + 1] +
				                   d_latticeParam[indexLattice].J4 * d_lattice[neighIndex4 + 1] +
				                   d_latticeParam[indexLattice].J5 * d_lattice[neighIndex5 + 1] +
				                   d_latticeParam[indexLattice].J6 * d_lattice[neighIndex6 + 1]);
		double HzLocal =          (d_latticeParam[indexLattice].J1 * d_lattice[neighIndex1 + 2] +
				                   d_latticeParam[indexLattice].J2 * d_lattice[neighIndex2 + 2] +
				                   d_latticeParam[indexLattice].J3 * d_lattice[neighIndex3 + 2] +
				                   d_latticeParam[indexLattice].J4 * d_lattice[neighIndex4 + 2] +
				                   d_latticeParam[indexLattice].J5 * d_lattice[neighIndex5 + 2] +
				                   d_latticeParam[indexLattice].J6 * d_lattice[neighIndex6 + 2]);

		/* calculate energy of this spin and add to the total energy  */
		d_EnergyTotal[indexLattice] += (-1.0) *
		                               (HxLocal * d_lattice[curIndex] + HyLocal * d_lattice[curIndex + 1] + HzLocal * d_lattice[curIndex + 2]);

		/* add generated effective field to the total */
		d_H[curIndex] += HxLocal;
		d_H[curIndex + 1] += HyLocal;
		d_H[curIndex + 2] += HzLocal;

	}

}

extern "C"
void inhomogeneousExchangeFieldTriangleCUDA(double *d_H, LatticeSiteDescr *d_latticeParam, double *d_EnergyTotal, long N2Ext, long N3Ext, long numberSites) {

	/* setup the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* wrapper of CUDA function */
	spinExchangeInhomogeneousFieldTriangle<<<cores, threads>>>(d_H, d_latticeParam, d_lattice, d_EnergyTotal, N2Ext, N3Ext, numberSites);

}

/* exchange interaction for one site (GPU) */
__global__
void spinExchangeInhomogeneousField(double *d_H, double *d_lattice, LatticeSiteDescr *d_latticeParam, double *d_EnergyTotal, long N2Ext, long N3Ext, long numberSites) {

	/* neighbor's indexes */
	long p1, m1, p2, m2, p3, m3;
	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* check boundary condition for threads */
	if ((indexLattice < numberSites) &&
	    (d_lattice[curIndex] * d_lattice[curIndex] + d_lattice[curIndex + 1] * d_lattice[curIndex + 1] + d_lattice[curIndex + 2] * d_lattice[curIndex + 2] > LENG_ZERO)) {

		/* calculate neighbor's indices */
		p1 = curIndex + 3;
		p2 = curIndex + 3 * N3Ext;
		p3 = curIndex + 3 * N3Ext * N2Ext;
		m1 = curIndex - 3;
		m2 = curIndex - 3 * N3Ext;
		m3 = curIndex - 3 * N3Ext * N2Ext;

		/* calculate effective field */
		double HxLocal =          (d_latticeParam[indexLattice].Jp1 * d_lattice[p1] +
				                   d_latticeParam[indexLattice].Jm1 * d_lattice[m1] +
				                   d_latticeParam[indexLattice].Jp2 * d_lattice[p2] +
				                   d_latticeParam[indexLattice].Jm2 * d_lattice[m2] +
				                   d_latticeParam[indexLattice].Jp3 * d_lattice[p3] +
				                   d_latticeParam[indexLattice].Jm3 * d_lattice[m3]);
		double HyLocal =          (d_latticeParam[indexLattice].Jp1 * d_lattice[p1 + 1] +
				                   d_latticeParam[indexLattice].Jm1 * d_lattice[m1 + 1] +
				                   d_latticeParam[indexLattice].Jp2 * d_lattice[p2 + 1] +
				                   d_latticeParam[indexLattice].Jm2 * d_lattice[m2 + 1] +
				                   d_latticeParam[indexLattice].Jp3 * d_lattice[p3 + 1] +
				                   d_latticeParam[indexLattice].Jm3 * d_lattice[m3 + 1]);
		double HzLocal =          (d_latticeParam[indexLattice].Jp1 * d_lattice[p1 + 2] +
								   d_latticeParam[indexLattice].Jm1 * d_lattice[m1 + 2] +
								   d_latticeParam[indexLattice].Jp2 * d_lattice[p2 + 2] +
								   d_latticeParam[indexLattice].Jm2 * d_lattice[m2 + 2] +
								   d_latticeParam[indexLattice].Jp3 * d_lattice[p3 + 2] +
								   d_latticeParam[indexLattice].Jm3 * d_lattice[m3 + 2]);

		/* calculate energy of this spin and add to the total energy  */
		d_EnergyTotal[indexLattice] = -(HxLocal * d_lattice[curIndex] + HyLocal * d_lattice[curIndex + 1] + HzLocal * d_lattice[curIndex + 2]);

		/* add partial effective field to total field */
		d_H[curIndex] += HxLocal;
		d_H[curIndex + 1] += HyLocal;
		d_H[curIndex + 2] += HzLocal;

	}

}

extern "C"
void inhomogeneousExchangeFieldCUDA(double *d_H, LatticeSiteDescr *d_latticeParam, double *d_EnergyTotal, long N2Ext, long N3Ext, long numberSites) {

	/* setup the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* wrapper of CUDA function */
	spinExchangeInhomogeneousField<<<cores, threads>>>(d_H, d_lattice, d_latticeParam, d_EnergyTotal, N2Ext, N3Ext, numberSites);

}

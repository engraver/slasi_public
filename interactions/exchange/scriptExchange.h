/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

/**
 * @file scriptExchange.h
 * @author Oleksandr Pylypovskyi, Artem Tomilo
 * @date 12 Jan 2018
 * @brief Initialization of vectors for exchange interaction
 */

#ifndef SCRIPTEXCHNAGE_H
#define SCRIPTEXCHNAGE_H

#include "../../alltypes.h"
#include "../../utils/indices.h"

/**
 * @brief Initialization of exchange integral coefficients of sites
 * Function works with `userVars` and 'latticeParam', so does not take any arguments.
 */
int getExchange(void);

#endif //SLASI_SCRIPTEXCHNAGE_H

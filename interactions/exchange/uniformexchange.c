#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "uniformexchange.h"
#include "../../utils/parallel_utils.h"

void uniformExchangeField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyExchange) {

	/* neighbor's indexes */
	size_t p1, m1, p2, m2, p3, m3;

	/* sign of exchange */
	int signJ = userVars.J > 0 ? 1 : -1;

	/* calculate neighbor's indices */
	p1 = curIndex + 3;
	p2 = curIndex + 3 * userVars.N3;
	p3 = curIndex + 3 * userVars.N3 * userVars.N2;
	m1 = curIndex - 3;
	m2 = curIndex - 3 * userVars.N3;
	m3 = curIndex - 3 * userVars.N3 * userVars.N2;

	/* calculate effective field */
	double effectFieldx = signJ * (lattice[p1] + lattice[m1] + lattice[p2] + lattice[m2]
	                               + lattice[p3] + lattice[m3]);
	double effectFieldy = signJ * (lattice[p1 + 1] + lattice[m1 + 1] + lattice[p2 + 1] + lattice[m2 + 1]
	                               + lattice[p3 + 1] + lattice[m3 + 1]);
	double effectFieldz = signJ * (lattice[p1 + 2] + lattice[m1 + 2] + lattice[p2 + 2] + lattice[m2 + 2]
	                               + lattice[p3 + 2] + lattice[m3 + 2]);

	/* calculate energy of exchange */
	latticeParam[indexLattice].EnergyExchangeNorm =
			-(effectFieldx * lattice[curIndex] + effectFieldy * lattice[curIndex + 1] +
			  effectFieldz * lattice[curIndex + 2]);
	*EnergyExchange += latticeParam[indexLattice].EnergyExchangeNorm;

	/* add generated effective field to the total */
	*Hx += effectFieldx;
	*Hy += effectFieldy;
	*Hz += effectFieldz;

}


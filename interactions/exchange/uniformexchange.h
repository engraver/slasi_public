#ifndef _UNIFORMEXCHANGE_H_
#define _UNIFORMEXCHANGE_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../../alltypes.h"
#include "../interactions.h"
#include "../../utils/indices.h"

/**
 * @brief Setup exchange
 *
 * @param numbers array with values from paramFile
 * @param indlp index for latticeParam array
 * @param indn index for numbers array
 *
 * @return 0 if ok, otherwise nonzero value
 */
int setupExchange(double **numbers, int indlp, int indn);

/**
 * @brief This function returns effective field and energy
 * which created by exchange interaction of surrounding spins with the same exchange constants
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Hx effective field on axis x
 * @param Hy effective field on axis y
 * @param Hz effective field on axis z
 * @param EnergyExchange normalized exchange energy
 */
void uniformExchangeField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyExchange);

/**
 * @brief This function returns effective field and energy (CUDA)
 * which created by exchange interaction of surrounding spins with the same exchange constants (GPU)
 * @param d_H is array of effective field (GPU)
 * @param d_EnergyTotal is array of energy for sites (GPU)
 * @param J is exchange integral
 * @param N2Ext is number of sites along the second axis
 * @param N3Ext is number of sites along the third axis
 * @param numberSites is number of sites
 */
void uniformExchangeFieldCUDA(double *d_H, double *d_EnergyTotal, double J, long N2Ext, long N3Ext, long numberSites);


#endif

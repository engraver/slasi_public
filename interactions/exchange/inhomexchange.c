#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "inhomexchange.h"
#include "../../utils/parallel_utils.h"

int setupExchange(double **numbers, int indlp, int indn) {

	if (userVars.exchInit == EI_UNIFORM) {
		/* isotropic exchange constant is given */
		latticeParam[indlp].Jm1 = userVars.J;
		latticeParam[indlp].Jp1 = userVars.J;
		latticeParam[indlp].Jm2 = userVars.J;
		latticeParam[indlp].Jp2 = userVars.J;
		latticeParam[indlp].Jm3 = userVars.J;
		latticeParam[indlp].Jp3 = userVars.J;
	} else if (userVars.exchInit == EI_FILE) {
		if (userVars.latticeType == LATTICE_CUBIC) {
			/* anisotropic exchange is given in `paramFile` */
			if (numbers == NULL) {
				char msg[] = "UNIFORMEXCHANGE.C: paramFile is not processed, while expected for J!\n";
				fprintf(flog, "%s", msg);
				fprintf(stderr, "%s", msg);
				return 1;
			}
			if (userVars.iJm1 == -1 || userVars.iJp1 == -1 || userVars.iJm2 == -1 || userVars.iJp2 == -1 ||
			    userVars.iJm3 == -1 || userVars.iJp3 == -1) {
				char msg[] = "UNIFORMEXCHANGE.C: Exchange intergral values  'Jm1', 'Jp1', 'Jm2', 'Jp2', 'Jm3', 'Jp3' are missing!\n";
				fprintf(flog, "%s", msg);
				fprintf(stderr, "%s", msg);
				return 1;
			}

			/* possible different values of exchange constant for all 6
             * neighbours */
			latticeParam[indlp].Jm1 = numbers[indn][userVars.iJm1];
			latticeParam[indlp].Jp1 = numbers[indn][userVars.iJp1];
			latticeParam[indlp].Jm2 = numbers[indn][userVars.iJm2];
			latticeParam[indlp].Jp2 = numbers[indn][userVars.iJp2];
			latticeParam[indlp].Jm3 = numbers[indn][userVars.iJm3];
			latticeParam[indlp].Jp3 = numbers[indn][userVars.iJp3];
		}
	}

	return 0;

}

int setupExchangeTriangle(double **numbers, int indlp, int indn) {

	if (userVars.exchInit == EI_UNIFORM) {
		/* isotropic exchange constant is given */
		latticeParam[indlp].J1 = userVars.J;
		latticeParam[indlp].J2 = userVars.J;
		latticeParam[indlp].J3 = userVars.J;
		latticeParam[indlp].J4 = userVars.J;
		latticeParam[indlp].J5 = userVars.J;
		latticeParam[indlp].J6 = userVars.J;
	} else if (userVars.exchInit == EI_FILE) {
		if (userVars.latticeType == LATTICE_TRIANGULAR) {
			/* anisotropic exchange is given in `paramFile` */
			if (numbers == NULL) {
				char msg[] = "UNIFORMEXCHANGE.C: paramFile is not processed, while expected for J!\n";
				fprintf(flog, "%s", msg);
				fprintf(stderr, "%s", msg);
				return 1;
			}
			if (userVars.iJ1 == -1 || userVars.iJ2 == -1 || userVars.iJ3 == -1 || userVars.iJ4 == -1 ||
			    userVars.iJ5 == -1 || userVars.iJ6 == -1) {
				char msg[] = "UNIFORMEXCHANGE.C: Exchange intergral values  'J1', 'J2', 'J3', 'J4', 'J5', 'J6' are missing!\n";
				fprintf(flog, "%s", msg);
				fprintf(stderr, "%s", msg);
				return 1;
			}

			/* possible different values of exchange constant for all 6
             * neighbours */
			latticeParam[indlp].J1 = numbers[indn][userVars.iJ1];
			latticeParam[indlp].J2 = numbers[indn][userVars.iJ2];
			latticeParam[indlp].J3 = numbers[indn][userVars.iJ3];
			latticeParam[indlp].J4 = numbers[indn][userVars.iJ4];
			latticeParam[indlp].J5 = numbers[indn][userVars.iJ5];
			latticeParam[indlp].J6 = numbers[indn][userVars.iJ6];
		}
	}

	return 0;

}

void inhomogeneousExchangeField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyExchange) {

	/* neighbor's indexes */
	size_t p1, m1, p2, m2, p3, m3;

	/* calculate neighbor's indices */
	p1 = curIndex + 3;
	p2 = curIndex + 3 * userVars.N3;
	p3 = curIndex + 3 * userVars.N3 * userVars.N2;
	m1 = curIndex - 3;
	m2 = curIndex - 3 * userVars.N3;
	m3 = curIndex - 3 * userVars.N3 * userVars.N2;

	/* calculate effective field */
	double effectFieldx = latticeParam[indexLattice].Jp1 * lattice[p1] +
			              latticeParam[indexLattice].Jm1 * lattice[m1] +
			              latticeParam[indexLattice].Jp2 * lattice[p2] +
			              latticeParam[indexLattice].Jm2 * lattice[m2] +
			              latticeParam[indexLattice].Jp3 * lattice[p3] +
			              latticeParam[indexLattice].Jm3 * lattice[m3];
	double effectFieldy = latticeParam[indexLattice].Jp1 * lattice[p1 + 1] +
	                      latticeParam[indexLattice].Jm1 * lattice[m1 + 1] +
	                      latticeParam[indexLattice].Jp2 * lattice[p2 + 1] +
	                      latticeParam[indexLattice].Jm2 * lattice[m2 + 1] +
	                      latticeParam[indexLattice].Jp3 * lattice[p3 + 1] +
	                      latticeParam[indexLattice].Jm3 * lattice[m3 + 1];
	double effectFieldz = latticeParam[indexLattice].Jp1 * lattice[p1 + 2] +
			              latticeParam[indexLattice].Jm1 * lattice[m1 + 2] +
			              latticeParam[indexLattice].Jp2 * lattice[p2 + 2] +
	                      latticeParam[indexLattice].Jm2 * lattice[m2 + 2] +
	                      latticeParam[indexLattice].Jp3 * lattice[p3 + 2] +
	                      latticeParam[indexLattice].Jm3 * lattice[m3 + 2];

	/* calculate energy of exchange */
	latticeParam[indexLattice].EnergyExchangeNorm =
			-(effectFieldx * lattice[curIndex] + effectFieldy * lattice[curIndex + 1] +
			  effectFieldz * lattice[curIndex + 2]);
	*EnergyExchange += latticeParam[indexLattice].EnergyExchangeNorm;

	/* add generated effective field to the total */
	*Hx += effectFieldx;
	*Hy += effectFieldy;
	*Hz += effectFieldz;

}

void inhomogeneousExchangeFieldTriangle(int i1, int i2, size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyExchange) {

	/* neighbor's indexes */
	size_t neighIndex1, neighIndex2, neighIndex3, neighIndex4, neighIndex5, neighIndex6;

	/* calculate neighbor's indices */
	neighIndex1 = IDXtrmg1(i1, i2);
	neighIndex2 = IDXtrmg2(i1, i2);
	neighIndex3 = IDXtrmg3(i1, i2);
	neighIndex4 = IDXtrmg4(i1, i2);
	neighIndex5 = IDXtrmg5(i1, i2);
	neighIndex6 = IDXtrmg6(i1, i2);

	/* calculate effective field */
	double effectFieldx = (-1.0) * (latticeParam[indexLattice].J1 * lattice[neighIndex1] +
			                        latticeParam[indexLattice].J2 * lattice[neighIndex2] +
			                        latticeParam[indexLattice].J3 * lattice[neighIndex3] +
						            latticeParam[indexLattice].J4 * lattice[neighIndex4] +
					                latticeParam[indexLattice].J5 * lattice[neighIndex5] +
						            latticeParam[indexLattice].J6 * lattice[neighIndex6]);
	double effectFieldy = (-1.0) * (latticeParam[indexLattice].J1 * lattice[neighIndex1 + 1] +
	                                latticeParam[indexLattice].J2 * lattice[neighIndex2 + 1] +
	                                latticeParam[indexLattice].J3 * lattice[neighIndex3 + 1] +
	                                latticeParam[indexLattice].J4 * lattice[neighIndex4 + 1] +
	                                latticeParam[indexLattice].J5 * lattice[neighIndex5 + 1] +
	                                latticeParam[indexLattice].J6 * lattice[neighIndex6 + 1]);
	double effectFieldz = (-1.0) * (latticeParam[indexLattice].J1 * lattice[neighIndex1 + 2] +
	                                latticeParam[indexLattice].J2 * lattice[neighIndex2 + 2] +
	                                latticeParam[indexLattice].J3 * lattice[neighIndex3 + 2] +
	                                latticeParam[indexLattice].J4 * lattice[neighIndex4 + 2] +
	                                latticeParam[indexLattice].J5 * lattice[neighIndex5 + 2] +
	                                latticeParam[indexLattice].J6 * lattice[neighIndex6 + 2]);

	/* calculate energy of exchange */
	latticeParam[indexLattice].EnergyExchangeNorm =
			(-0.5) * (effectFieldx * lattice[curIndex] + effectFieldy * lattice[curIndex + 1] +
			          effectFieldz * lattice[curIndex + 2]);
	*EnergyExchange += latticeParam[indexLattice].EnergyExchangeNorm;

	/* add generated effective field to the total */
	*Hx += effectFieldx;
	*Hy += effectFieldy;
	*Hz += effectFieldz;

}
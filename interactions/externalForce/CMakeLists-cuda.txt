########################################################################
#
# Part of SLaSi project
#
########################################################################

target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/forceSetup.c)
target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/forceSetupInt.c)

target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/forceSetup.c)
target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/forceSetupInt.c)
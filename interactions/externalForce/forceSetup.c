/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include <Python.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "forceSetup.h"

int setupExtForce(double **numbers, int indlp, int indn) {

	if ((numbers) && (userVars.iFx != -1) && (userVars.iFy != -1) && (userVars.iFz != -1)) {
		/* from file */
		latticeParam[indlp].Fx = numbers[indn][userVars.iFx];
		latticeParam[indlp].Fy = numbers[indn][userVars.iFy];
		latticeParam[indlp].Fz = numbers[indn][userVars.iFz];
	} else if ((userVars.bFx) && (userVars.bFy) && (userVars.bFx)) {
		/* constant value */
		latticeParam[indlp].Fx = userVars.Fx;
		latticeParam[indlp].Fy = userVars.Fy;
		latticeParam[indlp].Fz = userVars.Fz;
	} else {
		/* message about error */
		char msg[] = "FORCESETUP.C: Something wrong with components of external force (Are all of them given in paramFile?)!\n";
		fprintf(flog, "%s", msg);
		fprintf(stderr, "%s", msg);
		return 1;
	}

	return 0;

}

int setupPhaseAndFreqExtForce(double **numbers, int indlp, int indn) {

	if ((numbers) && (userVars.iFphase != -1) && (userVars.iFfreq != -1)) {
		/* from .dat file */
		latticeParam[indlp].Fphase = numbers[indn][userVars.iFphase];
		latticeParam[indlp].Ffreq = numbers[indn][userVars.iFfreq];
	} else if ((userVars.bFphase) && (userVars.bFfreq)) {
		/* from .cfg file */
		latticeParam[indlp].Fphase = userVars.Fphase;
		latticeParam[indlp].Ffreq = userVars.Ffreq;
	} else {
		/* return and write error */
		char msg[] = "Something wrong with phase or frequency of external force 'Ffreq' or 'Fphase' (Are all of them given in paramFile or configuration file?)!\n";
		fprintf(flog, "%s", msg);
		fprintf(stderr, "%s", msg);
		return 1;
	}

	return 0;

}


int harmForce(double t) {

	/* check variables for harmonic external force (if all parameters are in .dat file ) */
	if ((userVars.iFx != -1) && (userVars.iFy != -1) && (userVars.iFx != -1) &&
	    (userVars.iFphase != -1) && (userVars.iFfreq != -1)) {

		/* pass through each magnetic moment */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						/* check if site is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							/* to save amplitude of external force (with normalization) */
							latticeParam[indexLattice].savedFx = latticeParam[indexLattice].Fx * userVars.a /
							                                     (fabs(userVars.J) * userVars.S * userVars.S);
							latticeParam[indexLattice].savedFy = latticeParam[indexLattice].Fy * userVars.a /
							                                     (fabs(userVars.J) * userVars.S * userVars.S);
							latticeParam[indexLattice].savedFz = latticeParam[indexLattice].Fz * userVars.a /
							                                     (fabs(userVars.J) * userVars.S * userVars.S);;
						}
					}
				}
			}
		}

		/* pass through each magnetic moment to setup harmonic external force */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						/* check if site is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							/* change external force using harmonic law */
							latticeParam[indexLattice].Fx = latticeParam[indexLattice].savedFx *
							                                sin(latticeParam[indexLattice].Ffreq * t +
							                                    latticeParam[indexLattice].Fphase);
							latticeParam[indexLattice].Fy = latticeParam[indexLattice].savedFy *
							                                sin(latticeParam[indexLattice].Ffreq * t +
							                                    latticeParam[indexLattice].Fphase);
							latticeParam[indexLattice].Fz = latticeParam[indexLattice].savedFz *
							                                sin(latticeParam[indexLattice].Ffreq * t +
							                                    latticeParam[indexLattice].Fphase);
						}
					}
				}
			}
		}

		/* check variables for harmonic field (if all parameters are in .cfg file ) */
	} else if ((userVars.bFx) && (userVars.bFy) && (userVars.bFz) &&
	           (userVars.bFphase) && (userVars.bFfreq)) {

		/* pass through each magnetic moment */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						/* check if site is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							/* change external force using harmonic law */
							latticeParam[indexLattice].Fx = userVars.Fx * sin(userVars.Ffreq * t + userVars.Fphase);
							latticeParam[indexLattice].Fy = userVars.Fy * sin(userVars.Ffreq * t + userVars.Fphase);
							latticeParam[indexLattice].Fz = userVars.Fz * sin(userVars.Ffreq * t + userVars.Fphase);
						}
					}
				}
			}
		}

	} else {
		/* return error */
		fprintf(stderr, "Lack of parameters for external harmonic force!\n");
		return 1;
	}

	return 0;

}

int scriptForce(double t) {

	/* declarations of objects to work with Python.h */
	PyObject *pArgs, *pFunc;
	PyObject *pValue;

	/* declarations of variable */
	double Fx, Fy, Fz;
	int tester;

	/* check if module was found */
	if (pModulePython != NULL) {

		/* search of function with name "getExtForce" in module */
		pFunc = PyObject_GetAttrString(pModulePython, "getExtForce");
		/* check if function was found */
		if (pFunc && PyCallable_Check(pFunc)) {

			/* pass through each magnetic moment */
			for (int i1 = 0; i1 < userVars.N1; i1++) {
				for (int i2 = 0; i2 < userVars.N2; i2++) {
					for (int i3 = 0; i3 < userVars.N3; i3++) {
						if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
						    i3 < userVars.N3 - 1) {

							/* number of spin in lattice */
							int indexLattice = IDX(i1, i2, i3);
							/* check if site is magnetic */
							if (latticeParam[indexLattice].magnetic) {

								/* creating parameters to call function (coordinates, indices and time) */
								pArgs = PyTuple_New(7);

								/* time of simulation */
								pValue = PyFloat_FromDouble(t);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getExtForce!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 0, pValue);

								/* the first coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].x) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getExtForce!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 1, pValue);

								/* the second coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].y) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getExtForce!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 2, pValue);

								/* the third coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].z) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getExtForce!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 3, pValue);

								/* the first index */
								pValue = PyLong_FromLong(i1);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getExtForce!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 4, pValue);

								/* the second index */
								pValue = PyLong_FromLong(i2);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getExtForce!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 5, pValue);

								/* the third index */
								pValue = PyLong_FromLong(i3);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getExtForce!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 6, pValue);

								/* call function */
								pValue = PyObject_CallObject(pFunc, pArgs);
								/* check results of calling */
								if (pValue != NULL) {

									/* get results of calling */
									tester = PyArg_ParseTuple(pValue, "ddd", &Fx, &Fy, &Fz);
									/* check if it was able to get */
									if (tester) {
										/* save results (vector of external force) with normalization */
										latticeParam[indexLattice].Fx =
												Fx * userVars.a / (fabs(userVars.J) * userVars.S * userVars.S);
										latticeParam[indexLattice].Fy =
												Fy * userVars.a / (fabs(userVars.J) * userVars.S * userVars.S);
										latticeParam[indexLattice].Fz =
												Fz * userVars.a / (fabs(userVars.J) * userVars.S * userVars.S);
									} else {
										/* output error */
										fprintf(stderr, "Call of python function getExtForce failed!\n");
										return 1;
									}
									Py_DECREF(pValue);

								} else {
									/* output error */
									Py_DECREF(pFunc);
									Py_DECREF(pModulePython);
									PyErr_Print();
									fprintf(stderr, "Call of python function getExtForce failed\n");
									return 1;
								}

							}

						}
					}
				}
			}

		} else {
			/* write error */
			if (PyErr_Occurred())
				PyErr_Print();
			fprintf(stderr, "Cannot find function getExtForce in python file!\n");
			return 1;
		}
		Py_XDECREF(pFunc);

	} else {
		/* write error */
		PyErr_Print();
		fprintf(stderr, "Failed to load python file %s!\n", userVars.scriptFile);
		return 1;
	}

	return 0;

}

int forceSetup(double t) {

	/* select function to setup external force */
	switch (userVars.extForce) {
		case EXTFORCE_HARMTIME:
			/* harmonic law */
			return harmForce(t);
		case EXTFIELD_SCRIPT:
			/* using python script */
			return scriptForce(t);
		default:
			/* return error */
			fprintf(stderr, "Harmonic or sccript mode are necessary to set external force!\n");
			return 1;
	}

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

/**
 * @file forceSetup.h
 * @brief Update external force relative to changed time
 */

#ifndef _FORCE_SETUP_H_
#define _FORCE_SETUP_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../../alltypes.h"
#include "../../utils/indices.h"

/**
 * @brief Update external force relative to changed time for initialization (zero time)
 * @param t time, current simulation time
 */
int forceSetup(double t);

/**
 * @brief setupExtForce
 * @param numbers array with values from paramFile
 * @param indlp index for latticeParam array
 * @param indn index for numbers array
 * @return 0 if ok, otherwise nonzero value
 */
int setupExtForce(double **numbers, int indlp, int indn);

/**
 * @brief setupPhaseAndFreqExtForce
 * @param numbers array with values from paramFile
 * @param indlp index for latticeParam array
 * @param indn index for numbers array
 * @return 0 if ok, otherwise nonzero value
 */
int setupPhaseAndFreqExtForce(double **numbers, int indlp, int indn);

#endif

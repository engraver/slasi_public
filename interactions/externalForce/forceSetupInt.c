/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include <stdio.h>
#include <math.h>
#include "forceSetupInt.h"
#include "../../utils/parallel_utils.h"

int harmForceInt(double t) {

	/* check variables for harmonic external force (if all parameters are in .dat file ) */
	if ((userVars.iFx != -1) && (userVars.iFy != -1) && (userVars.iFx != -1) &&
	    (userVars.iFphase != -1) && (userVars.iFfreq != -1)) {

		/* pass through each magnetic moment to setup harmonic external force */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						/* check if site is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							/* change external force using harmonic law */
							latticeParam[indexLattice].Fx = latticeParam[indexLattice].savedFx *
							                                sin(latticeParam[indexLattice].Ffreq * t +
							                                    latticeParam[indexLattice].Fphase);
							latticeParam[indexLattice].Fy = latticeParam[indexLattice].savedFy *
							                                sin(latticeParam[indexLattice].Ffreq * t +
							                                    latticeParam[indexLattice].Fphase);
							latticeParam[indexLattice].Fz = latticeParam[indexLattice].savedFz *
							                                sin(latticeParam[indexLattice].Ffreq * t +
							                                    latticeParam[indexLattice].Fphase);
						}
					}
				}
			}
		}

		/* check variables for harmonic field (if all parameters are in .cfg file ) */
	} else if ((userVars.bFx) && (userVars.bFy) && (userVars.bFz) &&
	           (userVars.bFphase) && (userVars.bFfreq)) {

		/* pass through each magnetic moment */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						/* check if site is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							/* change external force using harmonic law */
							latticeParam[indexLattice].Fx = userVars.Fx * sin(userVars.Ffreq * t + userVars.Fphase);
							latticeParam[indexLattice].Fy = userVars.Fy * sin(userVars.Ffreq * t + userVars.Fphase);
							latticeParam[indexLattice].Fz = userVars.Fz * sin(userVars.Ffreq * t + userVars.Fphase);
						}
					}
				}
			}
		}

	} else {
		messageToStream("Lack of parameters for external harmonic force!\n", stderr);
		return 1;
	}

	return 0;

}

int forceSetupInt(double t) {

	/* select function to setup external force */
	switch (userVars.extForce) {
		case EXTFORCE_HARMTIME:
			/* harmonic law */
			return harmForceInt(t);
		default:
			/* return error */
			messageToStream("Harmonic mode is necessary to setup external force constantly!\n", stderr);
			return 1;
	}

}
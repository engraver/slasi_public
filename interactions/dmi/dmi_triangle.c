#include "dmi.h"
#include "../../utils/parallel_utils.h"

/* identification of coordinates for gradient */
#define id0 0
#define id1 1
#define id2 2
#define id3 3
#define id4 4
#define id5 5
#define id6 6
/* identification of neighbours */
#define idNeigh1 1
#define idNeigh2 2
#define idNeigh3 3
#define idNeigh4 4
#define idNeigh5 5
#define idNeigh6 6
/* identification of site */
#define idSite1 1
#define idSite2 2
#define idSite3 3
#define idSite4 4
#define idSite5 5
#define idSite6 6

/* forbidden index */
#define forbIndex -1

void dmiBulkFieldTriangle(int i1, int i2, size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *Edmi) {

	/* number of projections of all spins */
	size_t spinsNum = NUMBER_COMP_MAGN*userVars.N1*userVars.N2*userVars.N3;

	/***************************
    * i-th site
    ***************************/
	/* magnetization */
	double mxi = lattice[curIndex];
	double myi = lattice[curIndex + 1];
	double mzi = lattice[curIndex + 2];
	/* coordinates */
	double xi = lattice[curIndex + spinsNum];
	double yi = lattice[curIndex + spinsNum + 1];
	double zi = lattice[curIndex + spinsNum + 2];

	/***************************
    * 1-st neighbouring site
    ***************************/
	if (i1 < userVars.N1 - 2)  {
		curIndex = IDXtrmg1(i1, i2);
	} else {
		curIndex = IDXtrmg(1, i2);
	}
	/* magnetization */
	double mx1 = lattice[curIndex];
	double my1 = lattice[curIndex + 1];
	double mz1 = lattice[curIndex + 2];
	/* coordinates */
	double x1 = lattice[curIndex + spinsNum];
	double y1 = lattice[curIndex + spinsNum + 1];
	double z1 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_1 = latticeParam[indexLattice].dmiB1;
	if ((i1 == userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiB_1 = 0.0;
	}

	/***************************
    * 2-nd neighbouring site
    ***************************/
	if (i1 < userVars.N1 - 2) {
		curIndex = IDXtrmg2(i1, i2);
	} else if ((i1 == userVars.N1 - 2) && IsODD(i2)) {
		/* odd row */
		curIndex = IDXtrmg2(i1, i2);
	} else {
		/* even row */
		curIndex = IDXtrmg(1, i2 + 1);
	}
	/* magnetization */
	double mx2 = lattice[curIndex];
	double my2 = lattice[curIndex + 1];
	double mz2 = lattice[curIndex + 2];
	/* coordinates */
	double x2 = lattice[curIndex + spinsNum];
	double y2 = lattice[curIndex + spinsNum + 1];
	double z2 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_2 = latticeParam[indexLattice].dmiB2;
	if ((i1 == userVars.N1 - 2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		dmiB_2 = 0.0;
	}
	if (i2 == userVars.N2 - 2) {
		dmiB_2 = 0.0;
	}

	/***************************
    * 3-rd neighbouring site
    ***************************/
	if (i1 > 1) {
		curIndex = IDXtrmg3(i1, i2);
	} else if ((i1 == 1) && IsODD(i2)) {
		/* odd row */
		curIndex = IDXtrmg(userVars.N1 - 2, i2 + 1);
	} else {
		/* even row */
		curIndex = IDXtrmg3(i1, i2);
	}
	/* magnetization */
	double mx3 = lattice[curIndex];
	double my3 = lattice[curIndex + 1];
	double mz3 = lattice[curIndex + 2];
	/* coordinates */
	double x3 = lattice[curIndex + spinsNum];
	double y3 = lattice[curIndex + spinsNum + 1];
	double z3 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_3 = latticeParam[indexLattice].dmiB3;
	if ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiB_3 = 0.0;
	}
	if (i2 == userVars.N2 -2) {
		dmiB_3 = 0.0;
	}

	/***************************
    * 4-th neighbouring site
    ***************************/
	if (i1 > 1 ) {
		curIndex = IDXtrmg4(i1, i2);
	} else {
		curIndex = IDXtrmg(userVars.N1 - 2, i2);
	}
	/* magnetization */
	double mx4 = lattice[curIndex];
	double my4 = lattice[curIndex + 1];
	double mz4 = lattice[curIndex + 2];
	/* coordinates */
	double x4 = lattice[curIndex + spinsNum];
	double y4 = lattice[curIndex + spinsNum + 1];
	double z4 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_4 = latticeParam[indexLattice].dmiB4;
	if ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiB_4 = 0.0;
	}

	/***************************
    * 5-th neighbouring site
    ***************************/
	if (i1 > 1 ) {
		curIndex = IDXtrmg5(i1, i2);
	} else if ((i1 == 1) && IsODD(i2)) {
		/* odd row */
		curIndex = IDXtrmg(userVars.N1 - 2, i2 - 1);
	} else {
		/* even row */
		curIndex = IDXtrmg5(i1, i2);
	}
	/* magnetization */
	double mx5 = lattice[curIndex];
	double my5 = lattice[curIndex + 1];
	double mz5 = lattice[curIndex + 2];
	/* coordinates */
	double x5 = lattice[curIndex + spinsNum];
	double y5 = lattice[curIndex + spinsNum + 1];
	double z5 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_5 = latticeParam[indexLattice].dmiB5;
	if ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiB_5 = 0.0;
	}
	if (i2 == 1) {
		dmiB_5 = 0.0;
	}

	/***************************
    * 6-th neighbouring site
    ***************************/
	if (i1 < userVars.N1-2 ) {
		curIndex = IDXtrmg6(i1, i2);
	} else if ((i1 == userVars.N1-2) && IsODD(i2)) {
		/* odd row */
		curIndex = IDXtrmg6(i1, i2);
	} else {
		/* even row */
		curIndex = IDXtrmg(1, i2 - 1);
	}
	/* magnetization */
	double mx6 = lattice[curIndex];
	double my6 = lattice[curIndex + 1];
	double mz6 = lattice[curIndex + 2];
	/* coordinates */
	double x6 = lattice[curIndex + spinsNum];
	double y6 = lattice[curIndex + spinsNum + 1];
	double z6 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_6 = latticeParam[indexLattice].dmiB6;
	if ((i1 == userVars.N1-2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiB_6 = 0.0;
	}
	if (i2 == 1) {
		dmiB_6 = 0.0;
	}

	/* distance r1 - ri*/
	double txi1 = x1 - xi;
	double tyi1 = y1 - yi;
	double tzi1 = z1 - zi;
	/* distance r2 - ri*/
	double txi2 = x2 - xi;
	double tyi2 = y2 - yi;
	double tzi2 = z2 - zi;
	/* distance r3 - ri*/
	double txi3 = x3 - xi;
	double tyi3 = y3 - yi;
	double tzi3 = z3 - zi;
	/* distance r4 - ri*/
	double txi4 = x4 - xi;
	double tyi4 = y4 - yi;
	double tzi4 = z4 - zi;
	/* distance r5 - ri*/
	double txi5 = x5 - xi;
	double tyi5 = y5 - yi;
	double tzi5 = z5 - zi;
	/* distance r6 - ri*/
	double txi6 = x6 - xi;
	double tyi6 = y6 - yi;
	double tzi6 = z6 - zi;

	/* distance |r1 - ri| */
	double ri1 = sqrt( txi1*txi1 + tyi1*tyi1 + tzi1*tzi1 );
	/* distance |r2 - ri| */
	double ri2 = sqrt( txi2*txi2 + tyi2*tyi2 + tzi2*tzi2 );
	/* distance |r3 - ri| */
	double ri3 = sqrt( txi3*txi3 + tyi3*tyi3 + tzi3*tzi3 );
	/* distance |r4 - ri| */
	double ri4 = sqrt( txi4*txi4 + tyi4*tyi4 + tzi4*tzi4 );
	/* distance |r5 - ri| */
	double ri5 = sqrt( txi5*txi5 + tyi5*tyi5 + tzi5*tzi5 );
	/* distance |r6 - ri| */
	double ri6 = sqrt( txi6*txi6 + tyi6*tyi6 + tzi6*tzi6 );

	/* rescaled dmiB_i */
	dmiB_1 *= 1.0 / ri1;
	dmiB_2 *= 1.0 / ri2;
	dmiB_3 *= 1.0 / ri3;
	dmiB_4 *= 1.0 / ri4;
	dmiB_5 *= 1.0 / ri5;
	dmiB_6 *= 1.0 / ri6;

	/* calculate energy of bulk DMI interaction */
	*Edmi += ( dmiB_1 * ( txi1 * (myi * mz1 - mzi * my1) +
	                      tyi1 * (mzi * mx1 - mxi * mz1) +
	                      tzi1 * (mxi * my1 - myi * mx1) ) +
	           dmiB_2 * ( txi2 * (myi * mz2 - mzi * my2) +
	                      tyi2 * (mzi * mx2 - mxi * mz2) +
	                      tzi2 * (mxi * my2 - myi * mx2) ) +
	           dmiB_3 * ( txi3 * (myi * mz3 - mzi * my3) +
	                      tyi3 * (mzi * mx3 - mxi * mz3) +
	                      tzi3 * (mxi * my3 - myi * mx3) ) +
	           dmiB_4 * ( txi4 * (myi * mz4 - mzi * my4) +
	                      tyi4 * (mzi * mx4 - mxi * mz4) +
	                      tzi4 * (mxi * my4 - myi * mx4) ) +
	           dmiB_5 * ( txi5 * (myi * mz5 - mzi * my5) +
	                      tyi5 * (mzi * mx5 - mxi * mz5) +
	                      tzi5 * (mxi * my5 - myi * mx5) ) +
	           dmiB_6 * ( txi6 * (myi * mz6 - mzi * my6) +
	                      tyi6 * (mzi * mx6 - mxi * mz6) +
	                      tzi6 * (mxi * my6 - myi * mx6) )
	         ) / 2.0;

	/* calculate field of bulk DMI interaction */

	*Hx -= 0.5 * ( dmiB_1 * (tzi1 * my1 - tyi1 * mz1) +
	               dmiB_2 * (tzi2 * my2 - tyi2 * mz2) +
	               dmiB_3 * (tzi3 * my3 - tyi3 * mz3) +
	               dmiB_4 * (tzi4 * my4 - tyi4 * mz4) +
	               dmiB_5 * (tzi5 * my5 - tyi5 * mz5) +
	               dmiB_6 * (tzi6 * my6 - tyi6 * mz6)
	);
	*Hy -= 0.5 * ( dmiB_1 * (txi1 * mz1 - tzi1 * mx1) +
	               dmiB_2 * (txi2 * mz2 - tzi2 * mx2) +
	               dmiB_3 * (txi3 * mz3 - tzi3 * mx3) +
	               dmiB_4 * (txi4 * mz4 - tzi4 * mx4) +
	               dmiB_5 * (txi5 * mz5 - tzi5 * mx5) +
	               dmiB_6 * (txi6 * mz6 - tzi6 * mx6)
	);
	*Hz -= 0.5 * ( dmiB_1 * (tyi1 * mx1 - txi1 * my1) +
	               dmiB_2 * (tyi2 * mx2 - txi2 * my2) +
	               dmiB_3 * (tyi3 * mx3 - txi3 * my3) +
	               dmiB_4 * (tyi4 * mx4 - txi4 * my4) +
	               dmiB_5 * (tyi5 * mx5 - txi5 * my5) +
	               dmiB_6 * (tyi6 * mx6 - txi6 * my6)
	);

}

void dmiBulkForceTriangle(int i1, int i2, size_t curIndex, size_t indexLattice, double *Fx, double *Fy, double *Fz) {

	/* number of projections of all spins */
	size_t spinsNum = NUMBER_COMP_MAGN*userVars.N1*userVars.N2*userVars.N3;

	/***************************
    * i-th site
    ***************************/
	/* magnetization */
	double mxi = lattice[curIndex];
	double myi = lattice[curIndex + 1];
	double mzi = lattice[curIndex + 2];
	/* coordinates */
	double xi = lattice[curIndex + spinsNum];
	double yi = lattice[curIndex + spinsNum + 1];
	double zi = lattice[curIndex + spinsNum + 2];

	/***************************
    * 1-st neighbouring site
    ***************************/
	if (i1 < userVars.N1 - 2)  {
		curIndex = IDXtrmg1(i1, i2);
	} else {
		curIndex = IDXtrmg(1, i2);
	}
	/* magnetization */
	double mx1 = lattice[curIndex];
	double my1 = lattice[curIndex + 1];
	double mz1 = lattice[curIndex + 2];
	/* coordinates */
	double x1 = lattice[curIndex + spinsNum];
	double y1 = lattice[curIndex + spinsNum + 1];
	double z1 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_1 = latticeParam[indexLattice].dmiB1;
	if ((i1 == userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiB_1 = 0.0;
	}

	/***************************
    * 2-nd neighbouring site
    ***************************/
	if (i1 < userVars.N1 - 2) {
		curIndex = IDXtrmg2(i1, i2);
	} else if ((i1 == userVars.N1 - 2) && IsODD(i2)) {
		/* odd row */
		curIndex = IDXtrmg2(i1, i2);
	} else {
		/* even row */
		curIndex = IDXtrmg(1, i2 + 1);
	}
	/* magnetization */
	double mx2 = lattice[curIndex];
	double my2 = lattice[curIndex + 1];
	double mz2 = lattice[curIndex + 2];
	/* coordinates */
	double x2 = lattice[curIndex + spinsNum];
	double y2 = lattice[curIndex + spinsNum + 1];
	double z2 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_2 = latticeParam[indexLattice].dmiB2;
	if ((i1 == userVars.N1 - 2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		dmiB_2 = 0.0;
	}
	if (i2 == userVars.N2 - 2) {
		dmiB_2 = 0.0;
	}

	/***************************
    * 3-rd neighbouring site
    ***************************/
	if (i1 > 1) {
		curIndex = IDXtrmg3(i1, i2);
	} else if ((i1 == 1) && IsODD(i2)) {
		/* odd row */
		curIndex = IDXtrmg(userVars.N1 - 2, i2 + 1);
	} else {
		/* even row */
		curIndex = IDXtrmg3(i1, i2);
	}
	/* magnetization */
	double mx3 = lattice[curIndex];
	double my3 = lattice[curIndex + 1];
	double mz3 = lattice[curIndex + 2];
	/* coordinates */
	double x3 = lattice[curIndex + spinsNum];
	double y3 = lattice[curIndex + spinsNum + 1];
	double z3 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_3 = latticeParam[indexLattice].dmiB3;
	if ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiB_3 = 0.0;
	}
	if (i2 == userVars.N2 -2) {
		dmiB_3 = 0.0;
	}

	/***************************
    * 4-th neighbouring site
    ***************************/
	if (i1 > 1 ) {
		curIndex = IDXtrmg4(i1, i2);
	} else {
		curIndex = IDXtrmg(userVars.N1 - 2, i2);
	}
	/* magnetization */
	double mx4 = lattice[curIndex];
	double my4 = lattice[curIndex + 1];
	double mz4 = lattice[curIndex + 2];
	/* coordinates */
	double x4 = lattice[curIndex + spinsNum];
	double y4 = lattice[curIndex + spinsNum + 1];
	double z4 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_4 = latticeParam[indexLattice].dmiB4;
	if ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiB_4 = 0.0;
	}

	/***************************
    * 5-th neighbouring site
    ***************************/
	if (i1 > 1 ) {
		curIndex = IDXtrmg5(i1, i2);
	} else if ((i1 == 1) && IsODD(i2)) {
		/* odd row */
		curIndex = IDXtrmg(userVars.N1 - 2, i2 - 1);
	} else {
		/* even row */
		curIndex = IDXtrmg5(i1, i2);
	}
	/* magnetization */
	double mx5 = lattice[curIndex];
	double my5 = lattice[curIndex + 1];
	double mz5 = lattice[curIndex + 2];
	/* coordinates */
	double x5 = lattice[curIndex + spinsNum];
	double y5 = lattice[curIndex + spinsNum + 1];
	double z5 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_5 = latticeParam[indexLattice].dmiB5;
	if ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiB_5 = 0.0;
	}
	if (i2 == 1) {
		dmiB_5 = 0.0;
	}

	/***************************
    * 6-th neighbouring site
    ***************************/
	if (i1 < userVars.N1-2 ) {
		curIndex = IDXtrmg6(i1, i2);
	} else if ((i1 == userVars.N1-2) && IsODD(i2)) {
		/* odd row */
		curIndex = IDXtrmg6(i1, i2);
	} else {
		/* even row */
		curIndex = IDXtrmg(1, i2 - 1);
	}
	/* magnetization */
	double mx6 = lattice[curIndex];
	double my6 = lattice[curIndex + 1];
	double mz6 = lattice[curIndex + 2];
	/* coordinates */
	double x6 = lattice[curIndex + spinsNum];
	double y6 = lattice[curIndex + spinsNum + 1];
	double z6 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_6 = latticeParam[indexLattice].dmiB6;
	if ((i1 == userVars.N1-2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiB_6 = 0.0;
	}
	if (i2 == 1) {
		dmiB_6 = 0.0;
	}

	/***************************
    * magnetization cross-product
    ***************************/
	/* [mi x m1] */
	double mi1_x = myi * mz1 - mzi * my1;
	double mi1_y = mzi * mx1 - mxi * mz1;
	double mi1_z = mxi * my1 - myi * mx1;
	/* [mi x m2] */
	double mi2_x = myi * mz2 - mzi * my2;
	double mi2_y = mzi * mx2 - mxi * mz2;
	double mi2_z = mxi * my2 - myi * mx2;
	/* [mi x m3] */
	double mi3_x = myi * mz3 - mzi * my3;
	double mi3_y = mzi * mx3 - mxi * mz3;
	double mi3_z = mxi * my3 - myi * mx3;
	/* [mi x m4] */
	double mi4_x = myi * mz4 - mzi * my4;
	double mi4_y = mzi * mx4 - mxi * mz4;
	double mi4_z = mxi * my4 - myi * mx4;
	/* [mi x m5] */
	double mi5_x = myi * mz5 - mzi * my5;
	double mi5_y = mzi * mx5 - mxi * mz5;
	double mi5_z = mxi * my5 - myi * mx5;
	/* [mi x m6] */
	double mi6_x = myi * mz6 - mzi * my6;
	double mi6_y = mzi * mx6 - mxi * mz6;
	double mi6_z = mxi * my6 - myi * mx6;

	/***************************
    * i-th unit vector calculation
    ***************************/
	/* distance r1 - ri*/
	double txi1 = x1 - xi;
	double tyi1 = y1 - yi;
	double tzi1 = z1 - zi;
	/* distance r2 - ri*/
	double txi2 = x2 - xi;
	double tyi2 = y2 - yi;
	double tzi2 = z2 - zi;
	/* distance r3 - ri*/
	double txi3 = x3 - xi;
	double tyi3 = y3 - yi;
	double tzi3 = z3 - zi;
	/* distance r4 - ri*/
	double txi4 = x4 - xi;
	double tyi4 = y4 - yi;
	double tzi4 = z4 - zi;
	/* distance r5 - ri*/
	double txi5 = x5 - xi;
	double tyi5 = y5 - yi;
	double tzi5 = z5 - zi;
	/* distance r6 - ri*/
	double txi6 = x6 - xi;
	double tyi6 = y6 - yi;
	double tzi6 = z6 - zi;

	/* distance |r1 - ri| */
	double ri1 = sqrt( txi1*txi1 + tyi1*tyi1 + tzi1*tzi1 );
	double ri1p3 = ri1 * ri1 * ri1;
	/* distance |r2 - ri| */
	double ri2 = sqrt( txi2*txi2 + tyi2*tyi2 + tzi2*tzi2 );
	double ri2p3 = ri2 * ri2 * ri2;
	/* distance |r3 - ri| */
	double ri3 = sqrt( txi3*txi3 + tyi3*tyi3 + tzi3*tzi3 );
	double ri3p3 = ri3 * ri3 * ri3;
	/* distance |r4 - ri| */
	double ri4 = sqrt( txi4*txi4 + tyi4*tyi4 + tzi4*tzi4 );
	double ri4p3 = ri4 * ri4 * ri4;
	/* distance |r5 - ri| */
	double ri5 = sqrt( txi5*txi5 + tyi5*tyi5 + tzi5*tzi5 );
	double ri5p3 = ri5 * ri5 * ri5;
	/* distance |r6 - ri| */
	double ri6 = sqrt( txi6*txi6 + tyi6*tyi6 + tzi6*tzi6 );
	double ri6p3 = ri6 * ri6 * ri6;

	/***************************
    * 1-st unit vector derivatives
    ***************************/
	/* derivatives of ui1_x over xi, yi, and zi*/
	double dui1x_x = - (tyi1 * tyi1 + tzi1 * tzi1);
	double dui1x_y = txi1 * tyi1;
	double dui1x_z = txi1 * tzi1;
	/* derivatives of ui1_y over xi, yi, and zi*/
	double dui1y_x = txi1 * tyi1;
	double dui1y_y = - (txi1 * txi1 + tzi1 * tzi1);
	double dui1y_z = tyi1 * tzi1;
	/* derivatives of ui1_z over xi, yi, and zi*/
	double dui1z_x = txi1 * tzi1;
	double dui1z_y = tyi1 * tzi1;
	double dui1z_z = - (txi1 * txi1 + tyi1 * tyi1);

	/***************************
    * 2-nd unit vector derivatives
    ***************************/
	/* derivatives of ui2_x over xi, yi, and zi*/
	double dui2x_x = - (tyi2 * tyi2 + tzi2 * tzi2);
	double dui2x_y = txi2 * tyi2;
	double dui2x_z = txi2 * tzi2;
	/* derivatives of ui2_y over xi, yi, and zi*/
	double dui2y_x = txi2 * tyi2;
	double dui2y_y = - (txi2 * txi2 + tzi2 * tzi2);
	double dui2y_z = tyi2 * tzi2;
	/* derivatives of ui2_z over xi, yi, and zi*/
	double dui2z_x = txi2 * tzi2;
	double dui2z_y = tyi2 * tzi2;
	double dui2z_z = - (txi2 * txi2 + tyi2 * tyi2);

	/***************************
    * 3-rd unit vector derivatives
    ***************************/
	/* derivatives of ui3_x over xi, yi, and zi*/
	double dui3x_x = - (tyi3 * tyi3 + tzi3 * tzi3);
	double dui3x_y = txi3 * tyi3;
	double dui3x_z = txi3 * tzi3;
	/* derivatives of ui3_y over xi, yi, and zi*/
	double dui3y_x = txi3 * tyi3;
	double dui3y_y = - (txi3 * txi3 + tzi3 * tzi3);
	double dui3y_z = tyi3 * tzi3;
	/* derivatives of ui3_z over xi, yi, and zi*/
	double dui3z_x = txi3 * tzi3;
	double dui3z_y = tyi3 * tzi3;
	double dui3z_z = - (txi3 * txi3 + tyi3 * tyi3);

	/***************************
    * 4-th unit vector derivatives
    ***************************/
	/* derivatives of ui4_x over xi, yi, and zi*/
	double dui4x_x = - (tyi4 * tyi4 + tzi4 * tzi4);
	double dui4x_y = txi4 * tyi4;
	double dui4x_z = txi4 * tzi4;
	/* derivatives of ui4_y over xi, yi, and zi*/
	double dui4y_x = txi4 * tyi4;
	double dui4y_y = - (txi4 * txi4 + tzi4 * tzi4);
	double dui4y_z = tyi4 * tzi4;
	/* derivatives of ui4_z over xi, yi, and zi*/
	double dui4z_x = txi4 * tzi4;
	double dui4z_y = tyi4 * tzi4;
	double dui4z_z = - (txi4 * txi4 + tyi4 * tyi4);

	/***************************
    * 5-th unit vector derivatives
    ***************************/
	/* derivatives of ui5_x over xi, yi, and zi*/
	double dui5x_x = - (tyi5 * tyi5 + tzi5 * tzi5);
	double dui5x_y = txi5 * tyi5;
	double dui5x_z = txi5 * tzi5;
	/* derivatives of ui5_y over xi, yi, and zi*/
	double dui5y_x = txi5 * tyi5;
	double dui5y_y = - (txi5 * txi5 + tzi5 * tzi5);
	double dui5y_z = tyi5 * tzi5;
	/* derivatives of ui5_z over xi, yi, and zi*/
	double dui5z_x = txi5 * tzi5;
	double dui5z_y = tyi5 * tzi5;
	double dui5z_z = - (txi5 * txi5 + tyi5 * tyi5);

	/***************************
    * 6-th unit vector derivatives
    ***************************/
	/* derivatives of ui6_x over xi, yi, and zi*/
	double dui6x_x = - (tyi6 * tyi6 + tzi6 * tzi6);
	double dui6x_y = txi6 * tyi6;
	double dui6x_z = txi6 * tzi6;
	/* derivatives of ui6_y over xi, yi, and zi*/
	double dui6y_x = txi6 * tyi6;
	double dui6y_y = - (txi6 * txi6 + tzi6 * tzi6);
	double dui6y_z = tyi6 * tzi6;
	/* derivatives of ui6_z over xi, yi, and zi*/
	double dui6z_x = txi6 * tzi6;
	double dui6z_y = tyi6 * tzi6;
	double dui6z_z = - (txi6 * txi6 + tyi6 * tyi6);

	/* rescaled dmiB_i */
	dmiB_1 *= 1.0 / ri1p3;
	dmiB_2 *= 1.0 / ri2p3;
	dmiB_3 *= 1.0 / ri3p3;
	dmiB_4 *= 1.0 / ri4p3;
	dmiB_5 *= 1.0 / ri5p3;
	dmiB_6 *= 1.0 / ri6p3;

	/* calculate force of bulk DMI interaction */

	*Fx -= 0.5 * ( dmiB_1 * (dui1x_x * mi1_x + dui1y_x * mi1_y + dui1z_x * mi1_z) +
	               dmiB_2 * (dui2x_x * mi2_x + dui2y_x * mi2_y + dui2z_x * mi2_z) +
	               dmiB_3 * (dui3x_x * mi3_x + dui3y_x * mi3_y + dui3z_x * mi3_z) +
	               dmiB_4 * (dui4x_x * mi4_x + dui4y_x * mi4_y + dui4z_x * mi4_z) +
	               dmiB_5 * (dui5x_x * mi5_x + dui5y_x * mi5_y + dui5z_x * mi5_z) +
	               dmiB_6 * (dui6x_x * mi6_x + dui6y_x * mi6_y + dui6z_x * mi6_z) );

	*Fy -= 0.5 * ( dmiB_1 * (dui1x_y * mi1_x + dui1y_y * mi1_y + dui1z_y * mi1_z) +
	               dmiB_2 * (dui2x_y * mi2_x + dui2y_y * mi2_y + dui2z_y * mi2_z) +
	               dmiB_3 * (dui3x_y * mi3_x + dui3y_y * mi3_y + dui3z_y * mi3_z) +
	               dmiB_4 * (dui4x_y * mi4_x + dui4y_y * mi4_y + dui4z_y * mi4_z) +
	               dmiB_5 * (dui5x_y * mi5_x + dui5y_y * mi5_y + dui5z_y * mi5_z) +
	               dmiB_6 * (dui6x_y * mi6_x + dui6y_y * mi6_y + dui6z_y * mi6_z) );

	*Fz -= 0.5 * ( dmiB_1 * (dui1x_z * mi1_x + dui1y_z * mi1_y + dui1z_z * mi1_z) +
	               dmiB_2 * (dui2x_z * mi2_x + dui2y_z * mi2_y + dui2z_z * mi2_z) +
	               dmiB_3 * (dui3x_z * mi3_x + dui3y_z * mi3_y + dui3z_z * mi3_z) +
	               dmiB_4 * (dui4x_z * mi4_x + dui4y_z * mi4_y + dui4z_z * mi4_z) +
	               dmiB_5 * (dui5x_z * mi5_x + dui5y_z * mi5_y + dui5z_z * mi5_z) +
	               dmiB_6 * (dui6x_z * mi6_x + dui6y_z * mi6_y + dui6z_z * mi6_z) );

}

void dmiInterfaceFieldTriangle(int i1, int i2, size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *Edmi) {

	/* number of projections of all spins */
	size_t spinsNum = NUMBER_COMP_MAGN*userVars.N1*userVars.N2*userVars.N3;
	/* lattice index for intermediate calculation */
	size_t interIndexLattice;

	/***************************
	* i-th site
	***************************/
	/* magnetization */
	double mxi = lattice[curIndex];
	double myi = lattice[curIndex + 1];
	double mzi = lattice[curIndex + 2];
	/* coordinates */
	double xi = lattice[curIndex + spinsNum];
	double yi = lattice[curIndex + spinsNum + 1];
	double zi = lattice[curIndex + spinsNum + 2];
	/* normal vector of site */
	double nxi = latticeParam[indexLattice].trinx;
	double nyi = latticeParam[indexLattice].triny;
	double nzi = latticeParam[indexLattice].trinz;

	/***************************
	* 1-st neighbouring site
	***************************/
	if (i1 < userVars.N1 - 2)  {
		curIndex = IDXtrmg1(i1, i2);
		interIndexLattice = IDXtr1(i1, i2);
	} else {
		curIndex = IDXtrmg(1, i2);
		interIndexLattice = IDXtr(1, i2);
	}
	/* magnetization */
	double mx1 = lattice[curIndex];
	double my1 = lattice[curIndex + 1];
	double mz1 = lattice[curIndex + 2];
	/* coordinates */
	double x1 = lattice[curIndex + spinsNum];
	double y1 = lattice[curIndex + spinsNum + 1];
	double z1 = lattice[curIndex + spinsNum + 2];
	/* coefficient of interface DMI */
	double dmiN_1 = latticeParam[indexLattice].dmiN1;
	if ((i1 == userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiN_1 = 0.0;
	}
	double nxi1 = latticeParam[interIndexLattice].trinx;
	double nyi1 = latticeParam[interIndexLattice].triny;
	double nzi1 = latticeParam[interIndexLattice].trinz;
	if ((i1 == userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF)) {
		nxi1 = 0.0;
		nyi1 = 0.0;
		nzi1 = 0.0;
	}

	/***************************
	* 2-nd neighbouring site
	***************************/
	if (i1 < userVars.N1 - 2) {
		curIndex = IDXtrmg2(i1, i2);
		interIndexLattice = IDXtr2(i1, i2);
	} else if ((i1 == userVars.N1 - 2) && IsODD(i2)) {
		/* odd row */
		curIndex = IDXtrmg2(i1, i2);
		interIndexLattice = IDXtr2(i1, i2);
	} else {
		/* even row */
		curIndex = IDXtrmg(1, i2 + 1);
		interIndexLattice = IDXtr(1, i2 + 1);
	}
	/* magnetization */
	double mx2 = lattice[curIndex];
	double my2 = lattice[curIndex + 1];
	double mz2 = lattice[curIndex + 2];
	/* coordinates */
	double x2 = lattice[curIndex + spinsNum];
	double y2 = lattice[curIndex + spinsNum + 1];
	double z2 = lattice[curIndex + spinsNum + 2];
	/* coefficient of interface DMI */
	double dmiN_2 = latticeParam[indexLattice].dmiN2;
	if ((i1 == userVars.N1 - 2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		dmiN_2 = 0.0;
	}
	if (i2 == userVars.N2 - 2) {
		dmiN_2 = 0.0;
	}
	double nxi2 = latticeParam[interIndexLattice].trinx;
	double nyi2 = latticeParam[interIndexLattice].triny;
	double nzi2 = latticeParam[interIndexLattice].trinz;
	if ((i1 == userVars.N1 - 2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		nxi2 = 0.0;
		nyi2 = 0.0;
		nzi2 = 0.0;
	}
	if (i2 == userVars.N2 - 2) {
		nxi2 = 0.0;
		nyi2 = 0.0;
		nzi2 = 0.0;
	}

	/***************************
	* 3-rd neighbouring site
	***************************/
	if (i1 > 1) {
		curIndex = IDXtrmg3(i1, i2);
		interIndexLattice = IDXtr3(i1, i2);
	} else if ((i1 == 1) && IsODD(i2)) {
		/* odd row */
		curIndex = IDXtrmg(userVars.N1 - 2, i2 + 1);
		interIndexLattice = IDXtr(i1, i2);
	} else {
		/* even row */
		curIndex = IDXtrmg3(i1, i2);
		interIndexLattice = IDXtr3(i1, i2);
	}
	/* magnetization */
	double mx3 = lattice[curIndex];
	double my3 = lattice[curIndex + 1];
	double mz3 = lattice[curIndex + 2];
	/* coordinates */
	double x3 = lattice[curIndex + spinsNum];
	double y3 = lattice[curIndex + spinsNum + 1];
	double z3 = lattice[curIndex + spinsNum + 2];
	/* coefficient of interface DMI */
	double dmiN_3 = latticeParam[indexLattice].dmiN3;
	if ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiN_3 = 0.0;
	}
	if (i2 == userVars.N2 -2) {
		dmiN_3 = 0.0;
	}
	double nxi3 = latticeParam[interIndexLattice].trinx;
	double nyi3 = latticeParam[interIndexLattice].triny;
	double nzi3 = latticeParam[interIndexLattice].trinz;
	if ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF)) {
		nxi3 = 0.0;
		nyi3 = 0.0;
		nzi3 = 0.0;
	}
	if (i2 == userVars.N2 -2) {
		nxi3 = 0.0;
		nyi3 = 0.0;
		nzi3 = 0.0;
	}

	/***************************
    * 4-th neighbouring site
    ***************************/
	if (i1 > 1 ) {
		curIndex = IDXtrmg4(i1, i2);
		interIndexLattice = IDXtr4(i1, i2);
	} else {
		curIndex = IDXtrmg(userVars.N1 - 2, i2);
		interIndexLattice = IDXtr(userVars.N1 - 2, i2);
	}
	/* magnetization */
	double mx4 = lattice[curIndex];
	double my4 = lattice[curIndex + 1];
	double mz4 = lattice[curIndex + 2];
	/* coordinates */
	double x4 = lattice[curIndex + spinsNum];
	double y4 = lattice[curIndex + spinsNum + 1];
	double z4 = lattice[curIndex + spinsNum + 2];
	/* coefficient of interface DMI */
	double dmiN_4 = latticeParam[indexLattice].dmiN4;
	if ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiN_4 = 0.0;
	}
	double nxi4 = latticeParam[interIndexLattice].trinx;
	double nyi4 = latticeParam[interIndexLattice].triny;
	double nzi4 = latticeParam[interIndexLattice].trinz;
	if ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		nxi4 = 0.0;
		nyi4 = 0.0;
		nzi4 = 0.0;
	}

	/***************************
	* 5-th neighbouring site
	***************************/
	if (i1 > 1 ) {
		curIndex = IDXtrmg5(i1, i2);
		interIndexLattice = IDXtr5(i1, i2);
	} else if ((i1 == 1) && IsODD(i2)) {
		/* odd row */
		curIndex = IDXtrmg(userVars.N1 - 2, i2 - 1);
		interIndexLattice = IDXtr(userVars.N1 - 2, i2 - 1);
	} else {
		/* even row */
		curIndex = IDXtrmg5(i1, i2);
		interIndexLattice = IDXtr5(i1, i2);
	}
	/* magnetization */
	double mx5 = lattice[curIndex];
	double my5 = lattice[curIndex + 1];
	double mz5 = lattice[curIndex + 2];
	/* coordinates */
	double x5 = lattice[curIndex + spinsNum];
	double y5 = lattice[curIndex + spinsNum + 1];
	double z5 = lattice[curIndex + spinsNum + 2];
	/* coefficient of interface DMI */
	double dmiN_5 = latticeParam[indexLattice].dmiN5;
	if ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiN_5 = 0.0;
	}
	if (i2 == 1) {
		dmiN_5 = 0.0;
	}
	double nxi5 = latticeParam[interIndexLattice].trinx;
	double nyi5 = latticeParam[interIndexLattice].triny;
	double nzi5 = latticeParam[interIndexLattice].trinz;
	if ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF)) {
		nxi5 = 0.0;
		nyi5 = 0.0;
		nzi5 = 0.0;
	}
	if (i2 == 1) {
		nxi5 = 0.0;
		nyi5 = 0.0;
		nzi5 = 0.0;
	}


	/***************************
	* 6-th neighbouring site
	***************************/
	if (i1 < userVars.N1-2 ) {
		curIndex = IDXtrmg6(i1, i2);
		interIndexLattice = IDXtr6(i1, i2);
	} else if ((i1 == userVars.N1-2) && IsODD(i2)) {
		/* odd row */
		curIndex = IDXtrmg6(i1, i2);
		interIndexLattice = IDXtr6(i1, i2);
	} else {
		/* even row */
		curIndex = IDXtrmg(1, i2 - 1);
		interIndexLattice = IDXtr(1, i2 - 1);
	}
	/* magnetization */
	double mx6 = lattice[curIndex];
	double my6 = lattice[curIndex + 1];
	double mz6 = lattice[curIndex + 2];
	/* coordinates */
	double x6 = lattice[curIndex + spinsNum];
	double y6 = lattice[curIndex + spinsNum + 1];
	double z6 = lattice[curIndex + spinsNum + 2];
	/* coefficient of interface DMI */
	double dmiN_6 = latticeParam[indexLattice].dmiN6;
	if ((i1 == userVars.N1-2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiN_6 = 0.0;
	}
	if (i2 == 1) {
		dmiN_6 = 0.0;
	}
	double nxi6 = latticeParam[interIndexLattice].trinx;
	double nyi6 = latticeParam[interIndexLattice].triny;
	double nzi6 = latticeParam[interIndexLattice].trinz;
	if ((i1 == userVars.N1-2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF)) {
		nxi6 = 0.0;
		nyi6 = 0.0;
		nzi6 = 0.0;
	}
	if (i2 == 1) {
		nxi6 = 0.0;
		nyi6 = 0.0;
		nzi6 = 0.0;
	}

	/* distance r1 - ri*/
	double txi1 = x1 - xi;
	double tyi1 = y1 - yi;
	double tzi1 = z1 - zi;
	/* distance r2 - ri*/
	double txi2 = x2 - xi;
	double tyi2 = y2 - yi;
	double tzi2 = z2 - zi;
	/* distance r3 - ri*/
	double txi3 = x3 - xi;
	double tyi3 = y3 - yi;
	double tzi3 = z3 - zi;
	/* distance r4 - ri*/
	double txi4 = x4 - xi;
	double tyi4 = y4 - yi;
	double tzi4 = z4 - zi;
	/* distance r5 - ri*/
	double txi5 = x5 - xi;
	double tyi5 = y5 - yi;
	double tzi5 = z5 - zi;
	/* distance r6 - ri*/
	double txi6 = x6 - xi;
	double tyi6 = y6 - yi;
	double tzi6 = z6 - zi;

	/* distance |r1 - ri| */
	double ri1 = sqrt( txi1*txi1 + tyi1*tyi1 + tzi1*tzi1 );
	/* distance |r2 - ri| */
	double ri2 = sqrt( txi2*txi2 + tyi2*tyi2 + tzi2*tzi2 );
	/* distance |r3 - ri| */
	double ri3 = sqrt( txi3*txi3 + tyi3*tyi3 + tzi3*tzi3 );
	/* distance |r4 - ri| */
	double ri4 = sqrt( txi4*txi4 + tyi4*tyi4 + tzi4*tzi4 );
	/* distance |r5 - ri| */
	double ri5 = sqrt( txi5*txi5 + tyi5*tyi5 + tzi5*tzi5 );
	/* distance |r6 - ri| */
	double ri6 = sqrt( txi6*txi6 + tyi6*tyi6 + tzi6*tzi6 );

	/* rescaled dmiB_i */
	dmiN_1 *= 1.0 / ri1;
	dmiN_2 *= 1.0 / ri2;
	dmiN_3 *= 1.0 / ri3;
	dmiN_4 *= 1.0 / ri4;
	dmiN_5 *= 1.0 / ri5;
	dmiN_6 *= 1.0 / ri6;

	/* calculation of new normal vectors considering couple of interaction */

	/* the first normal vector */
	double nxiTot1 = (nxi + nxi1) / 2.0;
	double nyiTot1 = (nyi + nyi1) / 2.0;
	double nziTot1 = (nzi + nzi1) / 2.0;

	/* the second normal vector */
	double nxiTot2 = (nxi + nxi2) / 2.0;
	double nyiTot2 = (nyi + nyi2) / 2.0;
	double nziTot2 = (nzi + nzi2) / 2.0;

	/* the third normal vector */
	double nxiTot3 = (nxi + nxi3) / 2.0;
	double nyiTot3 = (nyi + nyi3) / 2.0;
	double nziTot3 = (nzi + nzi3) / 2.0;

	/* the fourth normal vector */
	double nxiTot4 = (nxi + nxi4) / 2.0;
	double nyiTot4 = (nyi + nyi4) / 2.0;
	double nziTot4 = (nzi + nzi4) / 2.0;

	/* the fifth normal vector */
	double nxiTot5 = (nxi + nxi5) / 2.0;
	double nyiTot5 = (nyi + nyi5) / 2.0;
	double nziTot5 = (nzi + nzi5) / 2.0;

	/* the sixth normal vector */
	double nxiTot6 = (nxi + nxi6) / 2.0;
	double nyiTot6 = (nyi + nyi6) / 2.0;
	double nziTot6 = (nzi + nzi6) / 2.0;

	/* declaration of temporary vectors for cross products (tij and ni)  */

	/* cross product of vectors ti1 and ni */
	double kx1 = tyi1 * nziTot1 - tzi1 * nyiTot1;
	double ky1 = tzi1 * nxiTot1 - txi1 * nziTot1;
	double kz1 = txi1 * nyiTot1 - tyi1 * nxiTot1;

	/* cross product of vectors ti2 and ni */
	double kx2 = tyi2 * nziTot2 - tzi2 * nyiTot2;
	double ky2 = tzi2 * nxiTot2 - txi2 * nziTot2;
	double kz2 = txi2 * nyiTot2 - tyi2 * nxiTot2;

	/* cross product of vectors ti3 and ni */
	double kx3 = tyi3 * nziTot3 - tzi3 * nyiTot3;
	double ky3 = tzi3 * nxiTot3 - txi3 * nziTot3;
	double kz3 = txi3 * nyiTot3 - tyi3 * nxiTot3;

	/* cross product of vectors ti4 and ni */
	double kx4 = tyi4 * nziTot4 - tzi4 * nyiTot4;
	double ky4 = tzi4 * nxiTot4 - txi4 * nziTot4;
	double kz4 = txi4 * nyiTot4 - tyi4 * nxiTot4;

	/* cross product of vectors ti5 and ni */
	double kx5 = tyi5 * nziTot5 - tzi5 * nyiTot5;
	double ky5 = tzi5 * nxiTot5 - txi5 * nziTot5;
	double kz5 = txi5 * nyiTot5 - tyi5 * nxiTot5;

	/* cross product of vectors ti6 and ni */
	double kx6 = tyi6 * nziTot6 - tzi6 * nyiTot6;
	double ky6 = tzi6 * nxiTot6 - txi6 * nziTot6;
	double kz6 = txi6 * nyiTot6 - tyi6 * nxiTot6;

	/* calculate energy of interface DMI interaction */
	*Edmi += ( dmiN_1 * ( kx1 * (myi * mz1 - mzi * my1) +
	                      ky1 * (mzi * mx1 - mxi * mz1) +
	                      kz1 * (mxi * my1 - myi * mx1) ) +
	           dmiN_2 * ( kx2 * (myi * mz2 - mzi * my2) +
	                      ky2 * (mzi * mx2 - mxi * mz2) +
	                      kz2 * (mxi * my2 - myi * mx2) ) +
	           dmiN_3 * ( kx3 * (myi * mz3 - mzi * my3) +
	                      ky3 * (mzi * mx3 - mxi * mz3) +
	                      kz3 * (mxi * my3 - myi * mx3) ) +
	           dmiN_4 * ( kx4 * (myi * mz4 - mzi * my4) +
	                      ky4 * (mzi * mx4 - mxi * mz4) +
	                      kz4 * (mxi * my4 - myi * mx4) ) +
	           dmiN_5 * ( kx5 * (myi * mz5 - mzi * my5) +
	                      ky5 * (mzi * mx5 - mxi * mz5) +
	                      kz5 * (mxi * my5 - myi * mx5) ) +
	           dmiN_6 * ( kx6 * (myi * mz6 - mzi * my6) +
	                      ky6 * (mzi * mx6 - mxi * mz6) +
	                      kz6 * (mxi * my6 - myi * mx6) )
	         ) / 2.0;

	/* calculate field of interface DMI interaction */
	*Hx += 0.5 * ( dmiN_1 * (kz1 * my1 - ky1 * mz1) +
	               dmiN_2 * (kz2 * my2 - ky2 * mz2) +
	               dmiN_3 * (kz3 * my3 - ky3 * mz3) +
	               dmiN_4 * (kz4 * my4 - ky4 * mz4) +
	               dmiN_5 * (kz5 * my5 - ky5 * mz5) +
	               dmiN_6 * (kz6 * my6 - ky6 * mz6)
	);
	*Hy += 0.5 * ( dmiN_1 * (kx1 * mz1 - kz1 * mx1) +
	               dmiN_2 * (kx2 * mz2 - kz2 * mx2) +
	               dmiN_3 * (kx3 * mz3 - kz3 * mx3) +
	               dmiN_4 * (kx4 * mz4 - kz4 * mx4) +
	               dmiN_5 * (kx5 * mz5 - kz5 * mx5) +
	               dmiN_6 * (kx6 * mz6 - kz6 * mx6)
	);
	*Hz += 0.5 * ( dmiN_1 * (ky1 * mx1 - kx1 * my1) +
	               dmiN_2 * (ky2 * mx2 - kx2 * my2) +
	               dmiN_3 * (ky3 * mx3 - kx3 * my3) +
	               dmiN_4 * (ky4 * mx4 - kx4 * my4) +
	               dmiN_5 * (ky5 * mx5 - kx5 * my5) +
	               dmiN_6 * (ky6 * mx6 - kx6 * my6)
	);

}

/* function to calculate normal vectors and partial derivatives of this normal vectors
 * using id of coordinates for gradient */
void calculateNormalVectorsDMI(int i1, int i2, int id, double *nxNorm, double *nyNorm, double *nzNorm,
                               double *nxNormDx, double *nxNormDy, double *nxNormDz,
                               double *nyNormDx, double *nyNormDy, double *nyNormDz,
                               double *nzNormDx, double *nzNormDy, double *nzNormDz) {

	/* check indexes */
	if ((i1 == forbIndex) && (i2 == forbIndex)) {
		return;
	}

	/* number of maximum indexes horizontally and vertically */
	int N1 = userVars.N1 - 2;
	int N2 = userVars.N2 - 2;

	/* switches of triangles */
	double k1 = 1.0, k2 = 1.0, k3 = 1.0, k4 = 1.0, k5 = 1.0, k6 = 1.0;

	/* periodic conditions for switches */

	/* the first switch */
	if ((i1 == N1) && (userVars.periodicBC1 == PBC_OFF)) {
		k1 = 0.0;
	}
	if (i2 == N2) {
		k1 = 0.0;
	}

	/* the second switch */
	if (i2 == N2) {
		k2 = 0.0;
	}
	if ((i1 == N1) && (i2 % 2 == 0) && (userVars.periodicBC1 == PBC_OFF)) {
		k2 = 0.0;
	}
	if ((i1 == 1) && (i2 % 2 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		k2 = 0.0;
	}

	/* the third switch */
	if (i2 == N2) {
		k3 = 0.0;
	}
	if ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		k3 = 0.0;
	}

	/* the fourth switch */
	if ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		k4 = 0.0;
	}
	if (i2 == 1) {
		k4 = 0.0;
	}

	/* the fifth switch */
	if (i2 == 1) {
		k5 = 0.0;
	}
	if ((i1 == 1) && (i2 % 2 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		k5 = 0.0;
	}
	if ((i1 == N1) && (i2 % 2 == 0) && (userVars.periodicBC1 == PBC_OFF)) {
		k5 = 0.0;
	}

	/* the sixth switch */
	if (i2 == 1) {
		k6 = 0.0;
	}
	if ((i1 == N1) && (userVars.periodicBC1 == PBC_OFF)) {
		k6 = 0.0;
	}

	// to debug code
	//fprintf(stderr,"%d %d %1.1f %1.1f %1.1f %1.1f %1.1f %1.1f\n", i1, i2, k1, k2, k3, k4, k5, k6);

	/* find indexes of neighbour sites (coordinates) */
	int indexLattice0 = IDXtrmg(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLattice1 = IDXtrmg1(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLattice2 = IDXtrmg2(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLattice3 = IDXtrmg3(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLattice4 = IDXtrmg4(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLattice5 = IDXtrmg5(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLattice6 = IDXtrmg6(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* find coordinates of neighbour sites */

	/* the current neighbour */
	double x0 = lattice[indexLattice0];
	double y0 = lattice[indexLattice0 + 1];
	double z0 = lattice[indexLattice0 + 2];

	/* the first neighbour */
	double x1 = lattice[indexLattice1];
	double y1 = lattice[indexLattice1 + 1];
	double z1 = lattice[indexLattice1 + 2];

	/* the second neighbour */
	double x2 = lattice[indexLattice2];
	double y2 = lattice[indexLattice2 + 1];
	double z2 = lattice[indexLattice2 + 2];

	/* the third neighbour */
	double x3 = lattice[indexLattice3];
	double y3 = lattice[indexLattice3 + 1];
	double z3 = lattice[indexLattice3 + 2];

	/* the fourth neighbour */
	double x4 = lattice[indexLattice4];
	double y4 = lattice[indexLattice4 + 1];
	double z4 = lattice[indexLattice4 + 2];

	/* the fifth neighbour */
	double x5 = lattice[indexLattice5];
	double y5 = lattice[indexLattice5 + 1];
	double z5 = lattice[indexLattice5 + 2];

	/* the sixth neighbour */
	double x6 = lattice[indexLattice6];
	double y6 = lattice[indexLattice6 + 1];
	double z6 = lattice[indexLattice6 + 2];

	/* calculate components of normal vectors without normalization */

	/* component along x-axis */
	double nx = k1 * (y1 * z2 - z1 * y2 - y1 * z0 + z1 * y0 - y0 * z2 + z0 * y2) +
	            k2 * (y2 * z3 - z2 * y3 - y2 * z0 + z2 * y0 - y0 * z3 + z0 * y3) +
	            k3 * (y3 * z4 - z3 * y4 - y3 * z0 + z3 * y0 - y0 * z4 + z0 * y4) +
	            k4 * (y4 * z5 - z4 * y5 - y4 * z0 + z4 * y0 - y0 * z5 + z0 * y5) +
	            k5 * (y5 * z6 - z5 * y6 - y5 * z0 + z5 * y0 - y0 * z6 + z0 * y6) +
	            k6 * (y6 * z1 - z6 * y1 - y6 * z0 + z6 * y0 - y0 * z1 + z0 * y1);

	/* component along y-axis */
	double ny = k1 * (z1 * x2 - x1 * z2 - z1 * x0 + x1 * z0 - z0 * x2 + x0 * z2) +
	            k2 * (z2 * x3 - x2 * z3 - z2 * x0 + x2 * z0 - z0 * x3 + x0 * z3) +
	            k3 * (z3 * x4 - x3 * z4 - z3 * x0 + x3 * z0 - z0 * x4 + x0 * z4) +
	            k4 * (z4 * x5 - x4 * z5 - z4 * x0 + x4 * z0 - z0 * x5 + x0 * z5) +
	            k5 * (z5 * x6 - x5 * z6 - z5 * x0 + x5 * z0 - z0 * x6 + x0 * z6) +
	            k6 * (z6 * x1 - x6 * z1 - z6 * x0 + x6 * z0 - z0 * x1 + x0 * z1);

	/* component along z-axis */
	double nz = k1 * (x1 * y2 - y1 * x2 - x1 * y0 + y1 * x0 - x0 * y2 + y0 * x2) +
	            k2 * (x2 * y3 - y2 * x3 - x2 * y0 + y2 * x0 - x0 * y3 + y0 * x3) +
	            k3 * (x3 * y4 - y3 * x4 - x3 * y0 + y3 * x0 - x0 * y4 + y0 * x4) +
	            k4 * (x4 * y5 - y4 * x5 - x4 * y0 + y4 * x0 - x0 * y5 + y0 * x5) +
	            k5 * (x5 * y6 - y5 * x6 - x5 * y0 + y5 * x0 - x0 * y6 + y0 * x6) +
	            k6 * (x6 * y1 - y6 * x1 - x6 * y0 + y6 * x0 - x0 * y1 + y0 * x1);

	/* calculate length of normal vector */
	double length = sqrt(nx * nx + ny * ny + nz * nz);
	double length3 = length * length * length;

	/* normalize normal vector and save result */
	*nxNorm = nx / length;
	*nyNorm = ny / length;
	*nzNorm = nz / length;

	/* declaration of partial derivatives of normal vector without normalization */
	double nxDx, nxDy = 0.0, nxDz = 0.0;
	double nyDx = 0.0, nyDy, nyDz = 0.0;
	double nzDx = 0.0, nzDy = 0.0, nzDz;

	/* calculate partial derivatives of normal vector without normalization
	 * using id of coordinates for gradient */

	/* component of normal vector along x-axis with different coordinates */

	/* partial derivative of x-coordinate */
	nxDx = 0.0;
	/* partial derivative of y-coordinate */
	if (id == id1) {
		nxDy = k1 * (z2 - z0) + k6 * (-z6 + z0);
	} else if (id == id2) {
		nxDy = k1 * (-z1 + z0) + k2 * (z3 - z0);
	} else if (id == id3) {
		nxDy = k2 * (-z2 + z0) + k3 * (z4 - z0);
	} else if (id == id4) {
		nxDy = k3 * (-z3 + z0) + k4 * (z5 - z0);
	} else if (id == id5) {
		nxDy = k4 * (-z4 + z0) + k5 * (z6 - z0);
	} else if (id == id6) {
		nxDy = k5 * (-z5 + z0) + k6 * (z1 - z0);
	} else if (id == id0) {
		nxDy = k1 * (z1 - z2) + k2 * (z2 - z3) + k3 * (z3 - z4) + k4 * (z4 -z5) + k5 * (z5 - z6) + k6 * (z6 - z1);
	}
	/* partial derivative of z-coordinate */
	if (id == id1) {
		nxDz = k1 * (-y2 + y0) + k6 * (y6 - y0);
	} else if (id == id2) {
		nxDz = k1 * (y1 - y0) + k2 * (-y3 + y0);
	} else if (id == id3) {
		nxDz = k2 * (y2 - y0) + k3 * (-y4 + y0);
	} else if (id == id4) {
		nxDz = k3 * (y3 - y0) + k4 * (-y5 + y0);
	} else if (id == id5) {
		nxDz = k4 * (y4 - y0) + k5 * (-y6 + y0);
	} else if (id == id6) {
		nxDz = k5 * (y5 - y0) + k6 * (-y1 + y0);
	} else if (id == id0) {
		nxDz = k1 * (-y1 + y2) + k2 * (-y2 + y3) + k3 * (-y3 + y4) + k4 * (-y4 + y5)  + k5 * (-y5 + y6) + k6 * (-y6 + y1);
	}

	/* component of normal vector along y-axis with different coordinates */

	/* partial derivative of x-coordinate */
	if (id == id1) {
		nyDx = k1 * (-z2 + z0) + k6 * (z6 - z0);
	} else if (id == id2) {
		nyDx = k1 * (z1 - z0) + k2 * (-z3 + z0);
	} else if (id == id3) {
		nyDx = k2 * (z2 - z0) + k3 * (-z4 + z0);
	} else if (id == id4) {
		nyDx = k3 * (z3 - z0) + k4 * (-z5 + z0);
	} else if (id == id5) {
		nyDx = k4 * (z4 - z0) + k5 * (-z6 + z0);
	} else if (id == id6) {
		nyDx = k5 * (z5 - z0) + k6 * (-z1 + z0);
	} else if (id == id0) {
		nyDx = k1 * (-z1 + z2) + k2 * (-z2 + z3) + k3 * (-z3 + z4) + k4 * (-z4 + z5) + k5 * (-z5 + z6) + k6 * (-z6 + z1);
	}
	/* partial derivative of y-coordinate */
	nyDy = 0.0;
	/* partial derivative of z-coordinate */
	if (id == id1) {
		nyDz = k1 * (x2 - x0) + k6 * (-x6 + x0);
	} else if (id == id2) {
		nyDz = k1 * (-x1 + x0) + k2 * (x3 - x0);
	} else if (id == id3) {
		nyDz = k2 * (-x2 + x0) + k3 * (x4 - x0);
	} else if (id == id4) {
		nyDz = k3 * (-x3 + x0) + k4 * (x5 - x0);
	} else if (id == id5) {
		nyDz = k4 * (-x4 + x0) + k5 * (x6 - x0);
	} else if (id == id6) {
		nyDz = k5 * (-x5 + x0) + k6 * (x1 - x0);
	} else if (id == id0) {
		nyDz = k1 * (x1 - x2) + k2 * (x2 - x3) + k3 * (x3 - x4) + k4 * (x4 - x5) + k5 * (x5 - x6) + k6 * (x6 - x1);
	}

	/* component of normal vector along z-axis with different coordinates */

	/* partial derivative of x-coordinate */
	if (id == id1) {
		nzDx = k1 * (y2 - y0) + k6 * (-y6 + y0);
	} else if (id == id2) {
		nzDx = k1 * (-y1 + y0) + k2 * (y3 - y0);
	} else if (id == id3) {
		nzDx = k2 * (-y2 + y0) + k3 * (y4 - y0);
	} else if (id == id4) {
		nzDx = k3 * (-y3 + y0) + k4 * (y5 - y0);
	} else if (id == id5) {
		nzDx = k4 * (-y4 + y0) + k5 * (y6 - y0);
	} else if (id == id6) {
		nzDx = k5 * (-y5 + y0) + k6 * (y1 - y0);
	} else if (id == id0) {
		nzDx = k1 * (y1 - y2) + k2 * (y2 - y3) + k3 * (y3 - y4) + k4 * (y4 - y5) + k5 * (y5 - y6) + k6 * (y6 - y1);
	}
	/* partial derivative of y-coordinate */
	if (id == id1) {
		nzDy = k1 * (-x2 + x0) + k6 * (x6 - x0);
	} else if (id == id2) {
		nzDy = k1 * (x1 - x0) + k2 * (-x3 + x0);
	} else if (id == id3) {
		nzDy = k2 * (x2 - x0) + k3 * (-x4 + x0);
	} else if (id == id4) {
		nzDy = k3 * (x3 - x0) + k4 * (-x5 + x0);
	} else if (id == id5) {
		nzDy = k4 * (x4 - x0) + k5 * (-x6 + x0);
	} else if (id == id6) {
		nzDy = k5 * (x5 - x0) + k6 * (-x1 + x0);
	} else if (id == id0) {
		nzDy = k1 * (-x1 + x2) + k2 * (-x2 + x3) + k3 * (-x3 + x4) + k4 * (-x4 + x5) + k5 * (-x5 + x6) + k6 * (-x6 + x1);
	}
	/* partial derivative of z-coordinate */
	nzDz = 0.0;

	/* calculate partial derivatives of normal vector with normalization and save this result */

	/* component of normal vector along x-axis with different coordinates */
	*nxNormDx = (nxDx * (ny * ny + nz * nz) - nx * (ny * nyDx + nz * nzDx)) / length3;
	*nxNormDy = (nxDy * (ny * ny + nz * nz) - nx * (ny * nyDy + nz * nzDy)) / length3;
	*nxNormDz = (nxDz * (ny * ny + nz * nz) - nx * (ny * nyDz + nz * nzDz)) / length3;

	/* component of normal vector along y-axis with different coordinates */
	*nyNormDx = (nyDx * (nx * nx + nz * nz) - ny * (nx * nxDx + nz * nzDx)) / length3;
	*nyNormDy = (nyDy * (nx * nx + nz * nz) - ny * (nx * nxDy + nz * nzDy)) / length3;
	*nyNormDz = (nyDz * (nx * nx + nz * nz) - ny * (nx * nxDz + nz * nzDz)) / length3;

	/* component of normal vector along y-axis with different coordinates */
	*nzNormDx = (nzDx * (nx * nx + ny * ny) - nz * (nx * nxDx + ny * nyDx)) / length3;
	*nzNormDy = (nzDy * (nx * nx + ny * ny) - nz * (nx * nxDy + ny * nyDy)) / length3;
	*nzNormDz = (nzDz * (nx * nx + ny * ny) - nz * (nx * nxDz + ny * nyDz)) / length3;

	//to debug code
	//fprintf(stderr, "%d %d %1.1f %1.1f %1.1f %1.1f %1.1f %1.1f %f %f %f \n", i1, i2, k1, k2, k3, k4, k5, k6, x1, y1, z1);

}

/* function to calculate vectors that connect two sites and partial derivatives of these vectors */
void calculateConnectionVectorsDMI(int i1, int i2, int idNeigh, double *rx, double *ry, double *rz,
                                   double *rxDx, double *rxDy, double *rxDz,
                                   double *ryDx, double *ryDy, double *ryDz,
                                   double *rzDx, double *rzDy, double *rzDz) {

	/* check indexes */
	if ((i1 == forbIndex) && (i2 == forbIndex)) {
		return;
	}

	/* find indexes of current and neighbour sites (coordinates) */
	int indexLatticeJ = IDXtrmg(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLatticeI = 0;
	if (idNeigh == idNeigh1) {
		indexLatticeI = IDXtrmg1(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	} else if (idNeigh == idNeigh2) {
		indexLatticeI = IDXtrmg2(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	} else if (idNeigh == idNeigh3)  {
		indexLatticeI = IDXtrmg3(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	} else if (idNeigh == idNeigh4)  {
		indexLatticeI = IDXtrmg4(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	} else if (idNeigh == idNeigh5)  {
		indexLatticeI = IDXtrmg5(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	} else if (idNeigh == idNeigh6)  {
		indexLatticeI = IDXtrmg6(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	}

	/* find coordinates of sites */
	double xi = lattice[indexLatticeI];
	double yi = lattice[indexLatticeI + 1];
	double zi = lattice[indexLatticeI + 2];
	double xj = lattice[indexLatticeJ];
	double yj = lattice[indexLatticeJ + 1];
	double zj = lattice[indexLatticeJ + 2];

	/* calculate length of vector */
	double length = sqrt((xj - xi) * (xj - xi) + (yj - yi) * (yj - yi) + (zj - zi) * (zj - zi));
	double length3 = length * length * length;

	/* calculate components of vector that connect two sites */
	*rx = (xj - xi) / length;
	*ry = (yj - yi) / length;
	*rz = (zj - zi) / length;

	/* calculate partial derivatives of vector that connect two sites */

	/* component of vector that connect two sites along x-axis with different coordinates */
	*rxDx = - ((yj - yi) * (yj - yi) + (zj - zi) * (zj - zi)) / length3;
	*rxDy = (xj - xi) * (yj - yi) / length3;
	*rxDz = (xj - xi) * (zj - zi) / length3;

	/* component of vector that connect two sites along y-axis with different coordinates */
	*ryDx = (yj - yi) * (xj - xi) / length3;
	*ryDy = - ((xj - xi) * (xj -xi) + (zj - zi) * (zj - zi)) / length3;
	*ryDz = (yj - yi) * (zj - zi) / length3;

	/* component of vector that connect two sites along z-axis with different coordinates */
	*rzDx = (zj - zi) * (xj - xi) / length3;
	*rzDy = (zj - zi) * (yj - yi) / length3;
	*rzDz = - ((xj - xi) * (xj -xi) + (yj - yi) * (yj - yi)) / length3;

}

/* function to calculate vectors that connect two sites without partial derivatives */
void calculateConnectionVectorsDMIWithoutDer(int i1, int i2, int idNeigh, double *rx, double *ry, double *rz) {

	/* check indexes */
	if ((i1 == forbIndex) && (i2 == forbIndex)) {
		return;
	}

	/* find indexes of current and neighbour sites (coordinates) */
	int indexLatticeJ = IDXtrmg(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	int indexLatticeI = 0;
	if (idNeigh == idNeigh1) {
		indexLatticeI = IDXtrmg1(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	} else if (idNeigh == idNeigh2) {
		indexLatticeI = IDXtrmg2(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	} else if (idNeigh == idNeigh3)  {
		indexLatticeI = IDXtrmg3(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	} else if (idNeigh == idNeigh4)  {
		indexLatticeI = IDXtrmg4(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	} else if (idNeigh == idNeigh5)  {
		indexLatticeI = IDXtrmg5(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	} else if (idNeigh == idNeigh6)  {
		indexLatticeI = IDXtrmg6(i1, i2) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	}

	/* find coordinates of sites */
	double xi = lattice[indexLatticeI];
	double yi = lattice[indexLatticeI + 1];
	double zi = lattice[indexLatticeI + 2];
	double xj = lattice[indexLatticeJ];
	double yj = lattice[indexLatticeJ + 1];
	double zj = lattice[indexLatticeJ + 2];

	/* calculate length of vector */
	double length = sqrt((xj - xi) * (xj - xi) + (yj - yi) * (yj - yi) + (zj - zi) * (zj - zi));

	/* calculate components of vector that connect two sites */
	*rx = (xj - xi) / length;
	*ry = (yj - yi) / length;
	*rz = (zj - zi) / length;

}


/* function to calculate intermediate vectors for calculation of DMI forces
 * DMI is along normal vector (interface DMI) */
void calculateIntermediateVector(int i1, int i2, double mix, double miy, double miz, double mjx, double mjy, double mjz,
                                 double *tx, double *ty, double *tz) {

	/* check indexes */
	if ((i1 == forbIndex) && (i2 == forbIndex)) {
		return;
	}

	/* calculate intermediate vector */
	*tx = miy * mjz - miz * mjy;
	*ty = - mix * mjz + miz * mjx;
	*tz = mix * mjy - miy * mjx;

}

void calculateForceDmiInterfaceSite(int i1, int i2, int idNeigh, int idSite,
                                    double nx0, double ny0, double nz0,
                                    double nx0Dx, double ny0Dx, double nz0Dx,
                                    double nx0Dy, double ny0Dy, double nz0Dy,
                                    double nx0Dz, double ny0Dz, double nz0Dz,
                                    double rxDx, double ryDx, double rzDx,
                                    double rxDy, double ryDy, double rzDy,
                                    double rxDz, double ryDz, double rzDz,
                                    double *Ftotx, double *Ftoty, double *Ftotz) {

	/* check indexes */
	if ((i1 == forbIndex) && (i2 == forbIndex)) {
		return;
	}

	/* indexes of neighbours */
	int neighI1, neighJ1;
	int neighI2, neighJ2;
	int neighI3, neighJ3;
	int neighI4, neighJ4;
	int neighI5, neighJ5;
	int neighI6, neighJ6;

	/* number of maximum indexes horizontally and vertically */
	int N1 = userVars.N1 - 2;
	int N2 = userVars.N2 - 2;

	/* calculate indexes of neighbours */

	/* the first neighbour */
	if (i1 != N1) {
		neighI1 = i1 + 1; neighJ1 = i2;
	} else if (userVars.periodicBC1 == PBC_ON) {
		neighI1 = 1; neighJ1 = i2;
	} else {
		neighI1 = forbIndex; neighJ1 = forbIndex;
	}

	/* the second neighbour */
	if (i1 != N1) {
		if (i2 % 2 == 1) {
			neighI2 = i1; neighJ2 = i2 + 1;
		} else {
			neighI2 = i1 + 1; neighJ2 = i2 + 1;
		}
	} else {
		if (i2 % 2 == 1) {
			neighI2 = i1; neighJ2 = i2 + 1;
		} else if (userVars.periodicBC1 == PBC_ON) {
			neighI2 = 1; neighJ2 = i2 + 1;
		} else {
			neighI2 = forbIndex; neighJ2 = forbIndex;
		}
	}

	/* the third neighbour */
	if (i1 != 1) {
		if (i2 % 2 == 1) {
			neighI3 = i1 - 1; neighJ3 = i2 + 1;
		} else {
			neighI3 = i1; neighJ3 = i2 + 1;
		}
	} else {
		if (i2 % 2 == 0) {
			neighI3 = i1; neighJ3 = i2 + 1;
		} else if (userVars.periodicBC1 == PBC_ON) {
			neighI3 = N1; neighJ3 = i2 + 1;
		} else {
			neighI3 = forbIndex; neighJ3 = forbIndex;
		}
	}

	/* the fourth neighbour */
	if (i1 != 1) {
		neighI4 = i1 - 1; neighJ4 = i2;
	} else if (userVars.periodicBC1 == PBC_ON) {
		neighI4 = N1; neighJ4 = i2;
	} else {
		neighI4 = forbIndex; neighJ4 = forbIndex;
	}

	/* the fifth neighbour */
	if (i1 != 1) {
		if (i2 % 2 == 1) {
			neighI5 = i1 - 1; neighJ5 = i2 - 1;
		} else {
			neighI5 = i1; neighJ5 = i2 - 1;
		}
	} else {
		if (i2 % 2 == 0) {
			neighI5 = i1; neighJ5 = i2 - 1;
		} else if (userVars.periodicBC1 == PBC_ON) {
			neighI5 = N1; neighJ5 = i2 - 1;
		} else {
			neighI5 = forbIndex; neighJ5 = forbIndex;
		}
	}

	/* the sixth neighbour */
	if (i1 != N1) {
		if (i2 % 2 == 1) {
			neighI6 = i1; neighJ6 = i2 - 1;
		} else {
			neighI6 = i1 + 1; neighJ6 = i2 - 1;
		}
	} else {
		if (i2 % 2 == 1) {
			neighI6 = i1; neighJ6 = i2 - 1;
		} else if (userVars.periodicBC1 == PBC_ON) {
			neighI6 = 1; neighJ6 = i2 - 1;
		} else {
			neighI6 = forbIndex; neighJ6 = forbIndex;
		}
	}

	/* declaration of vectors that connect two sites */
	double rx1 = 0.0, ry1 = 0.0, rz1 = 0.0;
	double rx2 = 0.0, ry2 = 0.0, rz2 = 0.0;
	double rx3 = 0.0, ry3 = 0.0, rz3 = 0.0;
	double rx4 = 0.0, ry4 = 0.0, rz4 = 0.0;
	double rx5 = 0.0, ry5 = 0.0, rz5 = 0.0;
	double rx6 = 0.0, ry6 = 0.0, rz6 = 0.0;

	/* calculate vectors that connect two sites */
	if (i2 == 1) {
		calculateConnectionVectorsDMIWithoutDer(neighI1, neighJ1, idNeigh4, &rx1, &ry1, &rz1);
		calculateConnectionVectorsDMIWithoutDer(neighI2, neighJ2, idNeigh5, &rx2, &ry2, &rz2);
		calculateConnectionVectorsDMIWithoutDer(neighI3, neighJ3, idNeigh6, &rx3, &ry3, &rz3);
		calculateConnectionVectorsDMIWithoutDer(neighI4, neighJ4, idNeigh1, &rx4, &ry4, &rz4);
	} else if (i2 == N2) {
		calculateConnectionVectorsDMIWithoutDer(neighI1, neighJ1, idNeigh4, &rx1, &ry1, &rz1);
		calculateConnectionVectorsDMIWithoutDer(neighI4, neighJ4, idNeigh1, &rx4, &ry4, &rz4);
		calculateConnectionVectorsDMIWithoutDer(neighI5, neighJ5, idNeigh2, &rx5, &ry5, &rz5);
		calculateConnectionVectorsDMIWithoutDer(neighI6, neighJ6, idNeigh3, &rx6, &ry6, &rz6);
	} else {
		calculateConnectionVectorsDMIWithoutDer(neighI1, neighJ1, idNeigh4, &rx1, &ry1, &rz1);
		calculateConnectionVectorsDMIWithoutDer(neighI2, neighJ2, idNeigh5, &rx2, &ry2, &rz2);
		calculateConnectionVectorsDMIWithoutDer(neighI3, neighJ3, idNeigh6, &rx3, &ry3, &rz3);
		calculateConnectionVectorsDMIWithoutDer(neighI4, neighJ4, idNeigh1, &rx4, &ry4, &rz4);
		calculateConnectionVectorsDMIWithoutDer(neighI5, neighJ5, idNeigh2, &rx5, &ry5, &rz5);
		calculateConnectionVectorsDMIWithoutDer(neighI6, neighJ6, idNeigh3, &rx6, &ry6, &rz6);
	}

	/* declaration of partial derivative for vectors that connect two sites */
	double rx1Dx = 0.0, rx1Dy = 0.0, rx1Dz = 0.0, ry1Dx = 0.0, ry1Dy = 0.0, ry1Dz = 0.0, rz1Dx = 0.0, rz1Dy = 0.0, rz1Dz = 0.0;
	double rx2Dx = 0.0, rx2Dy = 0.0, rx2Dz = 0.0, ry2Dx = 0.0, ry2Dy = 0.0, ry2Dz = 0.0, rz2Dx = 0.0, rz2Dy = 0.0, rz2Dz = 0.0;
	double rx3Dx = 0.0, rx3Dy = 0.0, rx3Dz = 0.0, ry3Dx = 0.0, ry3Dy = 0.0, ry3Dz = 0.0, rz3Dx = 0.0, rz3Dy = 0.0, rz3Dz = 0.0;
	double rx4Dx = 0.0, rx4Dy = 0.0, rx4Dz = 0.0, ry4Dx = 0.0, ry4Dy = 0.0, ry4Dz = 0.0, rz4Dx = 0.0, rz4Dy = 0.0, rz4Dz = 0.0;
	double rx5Dx = 0.0, rx5Dy = 0.0, rx5Dz = 0.0, ry5Dx = 0.0, ry5Dy = 0.0, ry5Dz = 0.0, rz5Dx = 0.0, rz5Dy = 0.0, rz5Dz = 0.0;
	double rx6Dx = 0.0, rx6Dy = 0.0, rx6Dz = 0.0, ry6Dx = 0.0, ry6Dy = 0.0, ry6Dz = 0.0, rz6Dx = 0.0, rz6Dy = 0.0, rz6Dz = 0.0;

	/* finding of necessary site for derivatives
	 * (also with change of sign because the coordinates of sites in formulas changed places) */
	if (idNeigh == idNeigh1) {
		rx1Dx = -rxDx; rx1Dy = -rxDy; rx1Dz = -rxDz;
		ry1Dx = -ryDx; ry1Dy = -ryDy; ry1Dz = -ryDz;
		rz1Dx = -rzDx; rz1Dy = -rzDy; rz1Dz = -rzDz;
	} else if (idNeigh == idNeigh2) {
		rx2Dx = -rxDx; rx2Dy = -rxDy; rx2Dz = -rxDz;
		ry2Dx = -ryDx; ry2Dy = -ryDy; ry2Dz = -ryDz;
		rz2Dx = -rzDx; rz2Dy = -rzDy; rz2Dz = -rzDz;
	} else if (idNeigh == idNeigh3) {
		rx3Dx = -rxDx; rx3Dy = -rxDy; rx3Dz = -rxDz;
		ry3Dx = -ryDx; ry3Dy = -ryDy; ry3Dz = -ryDz;
		rz3Dx = -rzDx; rz3Dy = -rzDy; rz3Dz = -rzDz;
	} else if (idNeigh == idNeigh4) {
		rx4Dx = -rxDx; rx4Dy = -rxDy; rx4Dz = -rxDz;
		ry4Dx = -ryDx; ry4Dy = -ryDy; ry4Dz = -ryDz;
		rz4Dx = -rzDx; rz4Dy = -rzDy; rz4Dz = -rzDz;
	} else if (idNeigh == idNeigh5) {
		rx5Dx = -rxDx; rx5Dy = -rxDy; rx5Dz = -rxDz;
		ry5Dx = -ryDx; ry5Dy = -ryDy; ry5Dz = -ryDz;
		rz5Dx = -rzDx; rz5Dy = -rzDy; rz5Dz = -rzDz;
	} else if (idNeigh == idNeigh6) {
		rx6Dx = -rxDx; rx6Dy = -rxDy; rx6Dz = -rxDz;
		ry6Dx = -ryDx; ry6Dy = -ryDy; ry6Dz = -ryDz;
		rz6Dx = -rzDx; rz6Dy = -rzDy; rz6Dz = -rzDz;
	}

	/* "latticeParam" */
	int indexLattice0 = IDXtr(i1, i2);

	/* "lattice" */
	int curIndex0 = IDXtrmg(i1, i2);
	int curIndex1 = IDXtrmg1(i1, i2);
	int curIndex2 = IDXtrmg2(i1, i2);
	int curIndex3 = IDXtrmg3(i1, i2);
	int curIndex4 = IDXtrmg4(i1, i2);
	int curIndex5 = IDXtrmg5(i1, i2);
	int curIndex6 = IDXtrmg6(i1, i2);

	/* find coefficients of anisotropy and components of magnetic moments for sites */

	/* zero site */
	double mx0 = lattice[curIndex0];
	double my0 = lattice[curIndex0 + 1];
	double mz0 = lattice[curIndex0 + 2];

	/* the first site */
	double d1 = latticeParam[indexLattice0].dmiN1;
	double mx1 = lattice[curIndex1];
	double my1 = lattice[curIndex1 + 1];
	double mz1 = lattice[curIndex1 + 2];
	if ((neighI1 == forbIndex) && (neighJ1 == forbIndex)) {
		d1 = 0.0;
		mx1 = 0.0;
		my1 = 0.0;
		mz1 = 0.0;
	}

	/* the second site */
	double d2 = latticeParam[indexLattice0].dmiN2;
	double mx2 = lattice[curIndex2];
	double my2 = lattice[curIndex2 + 1];
	double mz2 = lattice[curIndex2 + 2];
	if ((neighI2 == forbIndex) && (neighJ2 == forbIndex)) {
		d2 = 0.0;
		mx2 = 0.0;
		my2 = 0.0;
		mz2 = 0.0;
	}

	/* the third site */
	double d3 = latticeParam[indexLattice0].dmiN3;
	double mx3 = lattice[curIndex3];
	double my3 = lattice[curIndex3 + 1];
	double mz3 = lattice[curIndex3 + 2];
	if ((neighI3 == forbIndex) && (neighJ3 == forbIndex)) {
		d3 = 0.0;
		mx3 = 0.0;
		my3 = 0.0;
		mz3 = 0.0;
	}

	/* the fourth site */
	double d4 = latticeParam[indexLattice0].dmiN4;
	double mx4 = lattice[curIndex4];
	double my4 = lattice[curIndex4 + 1];
	double mz4 = lattice[curIndex4 + 2];
	if ((neighI4 == forbIndex) && (neighJ4 == forbIndex)) {
		d4 = 0.0;
		mx4 = 0.0;
		my4 = 0.0;
		mz4 = 0.0;
	}

	/* the fifth site */
	double d5 = latticeParam[indexLattice0].dmiN5;
	double mx5 = lattice[curIndex5];
	double my5 = lattice[curIndex5 + 1];
	double mz5 = lattice[curIndex5 + 2];
	if ((neighI5 == forbIndex) && (neighJ5 == forbIndex)) {
		d5 = latticeParam[indexLattice0].dmiN5;
		mx5 = 0.0;
		my5 = 0.0;
		mz5 = 0.0;
	}

	/* the sixth site */
	double d6 = latticeParam[indexLattice0].dmiN6;
	double mx6 = lattice[curIndex6];
	double my6 = lattice[curIndex6 + 1];
	double mz6 = lattice[curIndex6 + 2];
	if ((neighI6 == forbIndex) && (neighJ6 == forbIndex)) {
		d6 = 0.0;
		mx6 = 0.0;
		my6 = 0.0;
		mz6 = 0.0;
	}

	/* declaration of intermediate vectors for calculation */
	double tx1 = 0.0, ty1 = 0.0, tz1 = 0.0;
	double tx2 = 0.0, ty2 = 0.0, tz2 = 0.0;
	double tx3 = 0.0, ty3 = 0.0, tz3 = 0.0;
	double tx4 = 0.0, ty4 = 0.0, tz4 = 0.0;
	double tx5 = 0.0, ty5 = 0.0, tz5 = 0.0;
	double tx6 = 0.0, ty6 = 0.0, tz6 = 0.0;

	/* calculate intermediate vectors according to boundary condition */
	if (i2 == 1) {
		calculateIntermediateVector(neighI1, neighJ1, mx0, my0, mz0, mx1, my1, mz1, &tx1, &ty1, &tz1);
		calculateIntermediateVector(neighI2, neighJ2, mx0, my0, mz0, mx2, my2, mz2, &tx2, &ty2, &tz2);
		calculateIntermediateVector(neighI3, neighJ3, mx0, my0, mz0, mx3, my3, mz3, &tx3, &ty3, &tz3);
		calculateIntermediateVector(neighI4, neighJ4, mx0, my0, mz0, mx4, my4, mz4, &tx4, &ty4, &tz4);
	} else if (i2 == N2) {
		calculateIntermediateVector(neighI1, neighJ1, mx0, my0, mz0, mx1, my1, mz1, &tx1, &ty1, &tz1);
		calculateIntermediateVector(neighI4, neighJ4, mx0, my0, mz0, mx4, my4, mz4, &tx4, &ty4, &tz4);
		calculateIntermediateVector(neighI5, neighJ5, mx0, my0, mz0, mx5, my5, mz5, &tx5, &ty5, &tz5);
		calculateIntermediateVector(neighI6, neighJ6, mx0, my0, mz0, mx6, my6, mz6, &tx6, &ty6, &tz6);
	} else {
		calculateIntermediateVector(neighI1, neighJ1, mx0, my0, mz0, mx1, my1, mz1, &tx1, &ty1, &tz1);
		calculateIntermediateVector(neighI2, neighJ2, mx0, my0, mz0, mx2, my2, mz2, &tx2, &ty2, &tz2);
		calculateIntermediateVector(neighI3, neighJ3, mx0, my0, mz0, mx3, my3, mz3, &tx3, &ty3, &tz3);
		calculateIntermediateVector(neighI4, neighJ4, mx0, my0, mz0, mx4, my4, mz4, &tx4, &ty4, &tz4);
		calculateIntermediateVector(neighI5, neighJ5, mx0, my0, mz0, mx5, my5, mz5, &tx5, &ty5, &tz5);
		calculateIntermediateVector(neighI6, neighJ6, mx0, my0, mz0, mx6, my6, mz6, &tx6, &ty6, &tz6);
	}

	/* calculation of force for given site */
	*Ftotx -= 0.5 * d1 * (tx1 * (ny0Dx * rz1 + ny0 * rz1Dx - nz0Dx * ry1 - nz0 * ry1Dx)  +
	                      ty1 * (-nx0Dx * rz1 - nx0 * rz1Dx + nz0Dx * rx1 + nz0 * rx1Dx) +
	                      tz1 * (nx0Dx * ry1 + nx0 * ry1Dx - ny0Dx * rx1 - ny0 * rx1Dx)) +
	          0.5 * d2 * (tx2 * (ny0Dx * rz2 + ny0 * rz2Dx - nz0Dx * ry2 - nz0 * ry2Dx)  +
	                      ty2 * (-nx0Dx * rz2 - nx0 * rz2Dx + nz0Dx * rx2 + nz0 * rx2Dx) +
	                      tz2 * (nx0Dx * ry2 + nx0 * ry2Dx - ny0Dx * rx2 - ny0 * rx2Dx)) +
	          0.5 * d3 * (tx3 * (ny0Dx * rz3 + ny0 * rz3Dx - nz0Dx * ry3 - nz0 * ry3Dx)  +
	                      ty3 * (-nx0Dx * rz3 - nx0 * rz3Dx + nz0Dx * rx3 + nz0 * rx3Dx) +
	                      tz3 * (nx0Dx * ry3 + nx0 * ry3Dx - ny0Dx * rx3 - ny0 * rx3Dx)) +
	          0.5 * d4 * (tx4 * (ny0Dx * rz4 + ny0 * rz4Dx - nz0Dx * ry4 - nz0 * ry4Dx)  +
	                      ty4 * (-nx0Dx * rz4 - nx0 * rz4Dx + nz0Dx * rx4 + nz0 * rx4Dx) +
	                      tz4 * (nx0Dx * ry4 + nx0 * ry4Dx - ny0Dx * rx4 - ny0 * rx4Dx)) +
	          0.5 * d5 * (tx5 * (ny0Dx * rz5 + ny0 * rz5Dx - nz0Dx * ry5 - nz0 * ry5Dx)  +
	                      ty5 * (-nx0Dx * rz5 - nx0 * rz5Dx + nz0Dx * rx5 + nz0 * rx5Dx) +
	                      tz5 * (nx0Dx * ry5 + nx0 * ry5Dx - ny0Dx * rx5 - ny0 * rx5Dx)) +
	          0.5 * d6 * (tx6 * (ny0Dx * rz6 + ny0 * rz6Dx - nz0Dx * ry6 - nz0 * ry6Dx)  +
	                      ty6 * (-nx0Dx * rz6 - nx0 * rz6Dx + nz0Dx * rx6 + nz0 * rx6Dx) +
	                      tz6 * (nx0Dx * ry6 + nx0 * ry6Dx - ny0Dx * rx6 - ny0 * rx6Dx));

	*Ftoty -= 0.5 * d1 * (tx1 * (ny0Dy * rz1 + ny0 * rz1Dy - nz0Dy * ry1 - nz0 * ry1Dy)  +
	                      ty1 * (-nx0Dy * rz1 - nx0 * rz1Dy + nz0Dy * rx1 + nz0 * rx1Dy) +
	                      tz1 * (nx0Dy * ry1 + nx0 * ry1Dy - ny0Dy * rx1 - ny0 * rx1Dy)) +
	          0.5 * d2 * (tx2 * (ny0Dy * rz2 + ny0 * rz2Dy - nz0Dy * ry2 - nz0 * ry2Dy)  +
	                      ty2 * (-nx0Dy * rz2 - nx0 * rz2Dy + nz0Dy * rx2 + nz0 * rx2Dy) +
	                      tz2 * (nx0Dy * ry2 + nx0 * ry2Dy - ny0Dy * rx2 - ny0 * rx2Dy)) +
	          0.5 * d3 * (tx3 * (ny0Dy * rz3 + ny0 * rz3Dy - nz0Dy * ry3 - nz0 * ry3Dy)  +
	                      ty3 * (-nx0Dy * rz3 - nx0 * rz3Dy + nz0Dy * rx3 + nz0 * rx3Dy) +
	                      tz3 * (nx0Dy * ry3 + nx0 * ry3Dy - ny0Dy * rx3 - ny0 * rx3Dy)) +
	          0.5 * d4 * (tx4 * (ny0Dy * rz4 + ny0 * rz4Dy - nz0Dy * ry4 - nz0 * ry4Dy)  +
	                      ty4 * (-nx0Dy * rz4 - nx0 * rz4Dy + nz0Dy * rx4 + nz0 * rx4Dy) +
	                      tz4 * (nx0Dy * ry4 + nx0 * ry4Dy - ny0Dy * rx4 - ny0 * rx4Dy)) +
	          0.5 * d5 * (tx5 * (ny0Dy * rz5 + ny0 * rz5Dy - nz0Dy * ry5 - nz0 * ry5Dy)  +
	                      ty5 * (-nx0Dy * rz5 - nx0 * rz5Dy + nz0Dy * rx5 + nz0 * rx5Dy) +
	                      tz5 * (nx0Dy * ry5 + nx0 * ry5Dy - ny0Dy * rx5 - ny0 * rx5Dy)) +
	          0.5 * d6 * (tx6 * (ny0Dy * rz6 + ny0 * rz6Dy - nz0Dy * ry6 - nz0 * ry6Dy)  +
	                      ty6 * (-nx0Dy * rz6 - nx0 * rz6Dy + nz0Dy * rx6 + nz0 * rx6Dy) +
	                      tz6 * (nx0Dy * ry6 + nx0 * ry6Dy - ny0Dy * rx6 - ny0 * rx6Dy));

	*Ftotz -= 0.5 * d1 * (tx1 * (ny0Dz * rz1 + ny0 * rz1Dz - nz0Dz * ry1 - nz0 * ry1Dz)  +
	                      ty1 * (-nx0Dz * rz1 - nx0 * rz1Dz + nz0Dz * rx1 + nz0 * rx1Dz) +
	                      tz1 * (nx0Dz * ry1 + nx0 * ry1Dz - ny0Dz * rx1 - ny0 * rx1Dz)) +
	          0.5 * d2 * (tx2 * (ny0Dz * rz2 + ny0 * rz2Dz - nz0Dz * ry2 - nz0 * ry2Dz)  +
	                      ty2 * (-nx0Dz * rz2 - nx0 * rz2Dz + nz0Dz * rx2 + nz0 * rx2Dz) +
	                      tz2 * (nx0Dz * ry2 + nx0 * ry2Dz - ny0Dz * rx2 - ny0 * rx2Dz)) +
	          0.5 * d3 * (tx3 * (ny0Dz * rz3 + ny0 * rz3Dz - nz0Dz * ry3 - nz0 * ry3Dz)  +
	                      ty3 * (-nx0Dz * rz3 - nx0 * rz3Dz + nz0Dz * rx3 + nz0 * rx3Dz) +
	                      tz3 * (nx0Dz * ry3 + nx0 * ry3Dz - ny0Dz * rx3 - ny0 * rx3Dz)) +
	          0.5 * d4 * (tx4 * (ny0Dz * rz4 + ny0 * rz4Dz - nz0Dz * ry4 - nz0 * ry4Dz)  +
	                      ty4 * (-nx0Dz * rz4 - nx0 * rz4Dz + nz0Dz * rx4 + nz0 * rx4Dz) +
	                      tz4 * (nx0Dz * ry4 + nx0 * ry4Dz - ny0Dz * rx4 - ny0 * rx4Dz)) +
	          0.5 * d5 * (tx5 * (ny0Dz * rz5 + ny0 * rz5Dz - nz0Dz * ry5 - nz0 * ry5Dz)  +
	                      ty5 * (-nx0Dz * rz5 - nx0 * rz5Dz + nz0Dz * rx5 + nz0 * rx5Dz) +
	                      tz5 * (nx0Dz * ry5 + nx0 * ry5Dz - ny0Dz * rx5 - ny0 * rx5Dz)) +
	          0.5 * d6 * (tx6 * (ny0Dz * rz6 + ny0 * rz6Dz - nz0Dz * ry6 - nz0 * ry6Dz)  +
	                      ty6 * (-nx0Dz * rz6 - nx0 * rz6Dz + nz0Dz * rx6 + nz0 * rx6Dz) +
	                      tz6 * (nx0Dz * ry6 + nx0 * ry6Dz - ny0Dz * rx6 - ny0 * rx6Dz));

}

void dmiInterfaceForceTriangle(int i1, int i2, double *Fx, double *Fy, double *Fz) {

	/* indexes of neighbours */
	int neighI1, neighJ1;
	int neighI2, neighJ2;
	int neighI3, neighJ3;
	int neighI4, neighJ4;
	int neighI5, neighJ5;
	int neighI6, neighJ6;

	/* number of maximum indexes horizontally and vertically */
	int N1 = userVars.N1 - 2;
	int N2 = userVars.N2 - 2;

	/* calculate indexes of neighbours */

	/* the first neighbour */
	if (i1 != N1) {
		neighI1 = i1 + 1; neighJ1 = i2;
	} else if (userVars.periodicBC1 == PBC_ON) {
		neighI1 = 1; neighJ1 = i2;
	} else {
		neighI1 = forbIndex; neighJ1 = forbIndex;
	}

	/* the second neighbour */
	if (i1 != N1) {
		if (i2 % 2 == 1) {
			neighI2 = i1; neighJ2 = i2 + 1;
		} else {
			neighI2 = i1 + 1; neighJ2 = i2 + 1;
		}
	} else {
		if (i2 % 2 == 1) {
			neighI2 = i1; neighJ2 = i2 + 1;
		} else if (userVars.periodicBC1 == PBC_ON) {
			neighI2 = 1; neighJ2 = i2 + 1;
		} else {
			neighI2 = forbIndex; neighJ2 = forbIndex;
		}
	}

	/* the third neighbour */
	if (i1 != 1) {
		if (i2 % 2 == 1) {
			neighI3 = i1 - 1; neighJ3 = i2 + 1;
		} else {
			neighI3 = i1; neighJ3 = i2 + 1;
		}
	} else {
		if (i2 % 2 == 0) {
			neighI3 = i1; neighJ3 = i2 + 1;
		} else if (userVars.periodicBC1 == PBC_ON) {
			neighI3 = N1; neighJ3 = i2 + 1;
		} else {
			neighI3 = forbIndex; neighJ3 = forbIndex;
		}
	}

	/* the fourth neighbour */
	if (i1 != 1) {
		neighI4 = i1 - 1; neighJ4 = i2;
	} else if (userVars.periodicBC1 == PBC_ON) {
		neighI4 = N1; neighJ4 = i2;
	} else {
		neighI4 = forbIndex; neighJ4 = forbIndex;
	}

	/* the fifth neighbour */
	if (i1 != 1) {
		if (i2 % 2 == 1) {
			neighI5 = i1 - 1; neighJ5 = i2 - 1;
		} else {
			neighI5 = i1; neighJ5 = i2 - 1;
		}
	} else {
		if (i2 % 2 == 0) {
			neighI5 = i1; neighJ5 = i2 - 1;
		} else if (userVars.periodicBC1 == PBC_ON) {
			neighI5 = N1; neighJ5 = i2 - 1;
		} else {
			neighI5 = forbIndex; neighJ5 = forbIndex;
		}
	}

	/* the sixth neighbour */
	if (i1 != N1) {
		if (i2 % 2 == 1) {
			neighI6 = i1; neighJ6 = i2 - 1;
		} else {
			neighI6 = i1 + 1; neighJ6 = i2 - 1;
		}
	} else {
		if (i2 % 2 == 1) {
			neighI6 = i1; neighJ6 = i2 - 1;
		} else if (userVars.periodicBC1 == PBC_ON) {
			neighI6 = 1; neighJ6 = i2 - 1;
		} else {
			neighI6 = forbIndex; neighJ6 = forbIndex;
		}
	}

	/* declaration of normal vectors */
	double nx0 = 0.0, ny0 = 0.0, nz0 = 0.0;
	double nx1 = 0.0, ny1 = 0.0, nz1 = 0.0;
	double nx2 = 0.0, ny2 = 0.0, nz2 = 0.0;
	double nx3 = 0.0, ny3 = 0.0, nz3 = 0.0;
	double nx4 = 0.0, ny4 = 0.0, nz4 = 0.0;
	double nx5 = 0.0, ny5 = 0.0, nz5 = 0.0;
	double nx6 = 0.0, ny6 = 0.0, nz6 = 0.0;

	/* declaration of partial derivative for normal vectors */
	double nx0Dx = 0.0, nx0Dy = 0.0, nx0Dz = 0.0, ny0Dx = 0.0, ny0Dy = 0.0, ny0Dz = 0.0, nz0Dx = 0.0, nz0Dy = 0.0, nz0Dz = 0.0;
	double nx1Dx = 0.0, nx1Dy = 0.0, nx1Dz = 0.0, ny1Dx = 0.0, ny1Dy = 0.0, ny1Dz = 0.0, nz1Dx = 0.0, nz1Dy = 0.0, nz1Dz = 0.0;
	double nx2Dx = 0.0, nx2Dy = 0.0, nx2Dz = 0.0, ny2Dx = 0.0, ny2Dy = 0.0, ny2Dz = 0.0, nz2Dx = 0.0, nz2Dy = 0.0, nz2Dz = 0.0;
	double nx3Dx = 0.0, nx3Dy = 0.0, nx3Dz = 0.0, ny3Dx = 0.0, ny3Dy = 0.0, ny3Dz = 0.0, nz3Dx = 0.0, nz3Dy = 0.0, nz3Dz = 0.0;
	double nx4Dx = 0.0, nx4Dy = 0.0, nx4Dz = 0.0, ny4Dx = 0.0, ny4Dy = 0.0, ny4Dz = 0.0, nz4Dx = 0.0, nz4Dy = 0.0, nz4Dz = 0.0;
	double nx5Dx = 0.0, nx5Dy = 0.0, nx5Dz = 0.0, ny5Dx = 0.0, ny5Dy = 0.0, ny5Dz = 0.0, nz5Dx = 0.0, nz5Dy = 0.0, nz5Dz = 0.0;
	double nx6Dx = 0.0, nx6Dy = 0.0, nx6Dz = 0.0, ny6Dx = 0.0, ny6Dy = 0.0, ny6Dz = 0.0, nz6Dx = 0.0, nz6Dy = 0.0, nz6Dz = 0.0;

	//fprintf(stderr, "%d %d\n", i1, i2);

	/* calculate normal vectors and partial derivatives according to boundary condition */
	if (i2 == 1) {
		calculateNormalVectorsDMI(i1, i2, id0, &nx0, &ny0, &nz0, &nx0Dx, &nx0Dy, &nx0Dz,
		                          &ny0Dx, &ny0Dy, &ny0Dz, &nz0Dx, &nz0Dy, &nz0Dz);
		calculateNormalVectorsDMI(neighI1, neighJ1, id4, &nx1, &ny1, &nz1, &nx1Dx, &nx1Dy, &nx1Dz,
		                          &ny1Dx, &ny1Dy, &ny1Dz, &nz1Dx, &nz1Dy, &nz1Dz);
		calculateNormalVectorsDMI(neighI2, neighJ2, id5, &nx2, &ny2, &nz2, &nx2Dx, &nx2Dy, &nx2Dz,
		                          &ny2Dx, &ny2Dy, &ny2Dz, &nz2Dx, &nz2Dy, &nz2Dz);
		calculateNormalVectorsDMI(neighI3, neighJ3, id6, &nx3, &ny3, &nz3, &nx3Dx, &nx3Dy, &nx3Dz,
		                          &ny3Dx, &ny3Dy, &ny3Dz, &nz3Dx, &nz3Dy, &nz3Dz);
		calculateNormalVectorsDMI(neighI4, neighJ4, id1, &nx4, &ny4, &nz4, &nx4Dx, &nx4Dy, &nx4Dz,
		                          &ny4Dx, &ny4Dy, &ny4Dz, &nz4Dx, &nz4Dy, &nz4Dz);
	} else if (i2 == N2) {
		calculateNormalVectorsDMI(i1, i2, id0, &nx0, &ny0, &nz0, &nx0Dx, &nx0Dy, &nx0Dz,
		                          &ny0Dx, &ny0Dy, &ny0Dz, &nz0Dx, &nz0Dy, &nz0Dz);
		calculateNormalVectorsDMI(neighI1, neighJ1, id4, &nx1, &ny1, &nz1, &nx1Dx, &nx1Dy, &nx1Dz,
		                          &ny1Dx, &ny1Dy, &ny1Dz, &nz1Dx, &nz1Dy, &nz1Dz);
		calculateNormalVectorsDMI(neighI4, neighJ4, id1, &nx4, &ny4, &nz4, &nx4Dx, &nx4Dy, &nx4Dz,
		                          &ny4Dx, &ny4Dy, &ny4Dz, &nz4Dx, &nz4Dy, &nz4Dz);
		calculateNormalVectorsDMI(neighI5, neighJ5, id2, &nx5, &ny5, &nz5, &nx5Dx, &nx5Dy, &nx5Dz,
		                          &ny5Dx, &ny5Dy, &ny5Dz, &nz5Dx, &nz5Dy, &nz5Dz);
		calculateNormalVectorsDMI(neighI6, neighJ6, id3, &nx6, &ny6, &nz6, &nx6Dx, &nx6Dy, &nx6Dz,
		                          &ny6Dx, &ny6Dy, &ny6Dz, &nz6Dx, &nz6Dy, &nz6Dz);
	} else {
		calculateNormalVectorsDMI(i1, i2, id0, &nx0, &ny0, &nz0, &nx0Dx, &nx0Dy, &nx0Dz,
		                          &ny0Dx, &ny0Dy, &ny0Dz, &nz0Dx, &nz0Dy, &nz0Dz);
		calculateNormalVectorsDMI(neighI1, neighJ1, id4,  &nx1, &ny1, &nz1, &nx1Dx, &nx1Dy, &nx1Dz,
		                          &ny1Dx, &ny1Dy, &ny1Dz, &nz1Dx, &nz1Dy, &nz1Dz);
		calculateNormalVectorsDMI(neighI2, neighJ2, id5, &nx2, &ny2, &nz2, &nx2Dx, &nx2Dy, &nx2Dz,
		                          &ny2Dx, &ny2Dy, &ny2Dz, &nz2Dx, &nz2Dy, &nz2Dz);
		calculateNormalVectorsDMI(neighI3, neighJ3, id6, &nx3, &ny3, &nz3, &nx3Dx, &nx3Dy, &nx3Dz,
		                          &ny3Dx, &ny3Dy, &ny3Dz, &nz3Dx, &nz3Dy, &nz3Dz);
		calculateNormalVectorsDMI(neighI4, neighJ4, id1, &nx4, &ny4, &nz4, &nx4Dx, &nx4Dy, &nx4Dz,
		                          &ny4Dx, &ny4Dy, &ny4Dz, &nz4Dx, &nz4Dy, &nz4Dz);
		calculateNormalVectorsDMI(neighI5, neighJ5, id2, &nx5, &ny5, &nz5, &nx5Dx, &nx5Dy, &nx5Dz,
		                          &ny5Dx, &ny5Dy, &ny5Dz, &nz5Dx, &nz5Dy, &nz5Dz);
		calculateNormalVectorsDMI(neighI6, neighJ6, id3, &nx6, &ny6, &nz6, &nx6Dx, &nx6Dy, &nx6Dz,
		                          &ny6Dx, &ny6Dy, &ny6Dz, &nz6Dx, &nz6Dy, &nz6Dz);
	}

	/* declaration of vectors that connect two sites */
	double rx1 = 0.0, ry1 = 0.0, rz1 = 0.0;
	double rx2 = 0.0, ry2 = 0.0, rz2 = 0.0;
	double rx3 = 0.0, ry3 = 0.0, rz3 = 0.0;
	double rx4 = 0.0, ry4 = 0.0, rz4 = 0.0;
	double rx5 = 0.0, ry5 = 0.0, rz5 = 0.0;
	double rx6 = 0.0, ry6 = 0.0, rz6 = 0.0;

	/* declaration of partial derivative for vectors that connect two sites */
	double rx1Dx = 0.0, rx1Dy = 0.0, rx1Dz = 0.0, ry1Dx = 0.0, ry1Dy = 0.0, ry1Dz = 0.0, rz1Dx = 0.0, rz1Dy = 0.0, rz1Dz = 0.0;
	double rx2Dx = 0.0, rx2Dy = 0.0, rx2Dz = 0.0, ry2Dx = 0.0, ry2Dy = 0.0, ry2Dz = 0.0, rz2Dx = 0.0, rz2Dy = 0.0, rz2Dz = 0.0;
	double rx3Dx = 0.0, rx3Dy = 0.0, rx3Dz = 0.0, ry3Dx = 0.0, ry3Dy = 0.0, ry3Dz = 0.0, rz3Dx = 0.0, rz3Dy = 0.0, rz3Dz = 0.0;
	double rx4Dx = 0.0, rx4Dy = 0.0, rx4Dz = 0.0, ry4Dx = 0.0, ry4Dy = 0.0, ry4Dz = 0.0, rz4Dx = 0.0, rz4Dy = 0.0, rz4Dz = 0.0;
	double rx5Dx = 0.0, rx5Dy = 0.0, rx5Dz = 0.0, ry5Dx = 0.0, ry5Dy = 0.0, ry5Dz = 0.0, rz5Dx = 0.0, rz5Dy = 0.0, rz5Dz = 0.0;
	double rx6Dx = 0.0, rx6Dy = 0.0, rx6Dz = 0.0, ry6Dx = 0.0, ry6Dy = 0.0, ry6Dz = 0.0, rz6Dx = 0.0, rz6Dy = 0.0, rz6Dz = 0.0;

	/* calculate vectors that connect two sites and partial derivatives according to boundary condition */
	if (i2 == 1) {
		calculateConnectionVectorsDMI(neighI1, neighJ1, idNeigh4, &rx1, &ry1, &rz1, &rx1Dx, &rx1Dy, &rx1Dz,
		                              &ry1Dx, &ry1Dy, &ry1Dz, &rz1Dx, &rz1Dy, &rz1Dz);
		calculateConnectionVectorsDMI(neighI2, neighJ2, idNeigh5, &rx2, &ry2, &rz2, &rx2Dx, &rx2Dy, &rx2Dz,
		                              &ry2Dx, &ry2Dy, &ry2Dz, &rz2Dx, &rz2Dy, &rz2Dz);
		calculateConnectionVectorsDMI(neighI3, neighJ3, idNeigh6, &rx3, &ry3, &rz3, &rx3Dx, &rx3Dy, &rx3Dz,
		                              &ry3Dx, &ry3Dy, &ry3Dz, &rz3Dx, &rz3Dy, &rz3Dz);
		calculateConnectionVectorsDMI(neighI4, neighJ4, idNeigh1, &rx4, &ry4, &rz4, &rx4Dx, &rx4Dy, &rx4Dz,
		                              &ry4Dx, &ry4Dy, &ry4Dz, &rz4Dx, &rz4Dy, &rz4Dz);
	} else if (i2 == N2) {
		calculateConnectionVectorsDMI(neighI1, neighJ1, idNeigh4, &rx1, &ry1, &rz1, &rx1Dx, &rx1Dy, &rx1Dz,
		                              &ry1Dx, &ry1Dy, &ry1Dz, &rz1Dx, &rz1Dy, &rz1Dz);
		calculateConnectionVectorsDMI(neighI4, neighJ4, idNeigh1, &rx4, &ry4, &rz4, &rx4Dx, &rx4Dy, &rx4Dz,
		                              &ry4Dx, &ry4Dy, &ry4Dz, &rz4Dx, &rz4Dy, &rz4Dz);
		calculateConnectionVectorsDMI(neighI5, neighJ5, idNeigh2, &rx5, &ry5, &rz5, &rx5Dx, &rx5Dy, &rx5Dz,
		                              &ry5Dx, &ry5Dy, &ry5Dz, &rz5Dx, &rz5Dy, &rz5Dz);
		calculateConnectionVectorsDMI(neighI6, neighJ6, idNeigh3, &rx6, &ry6, &rz6, &rx6Dx, &rx6Dy, &rx6Dz,
		                              &ry6Dx, &ry6Dy, &ry6Dz, &rz6Dx, &rz6Dy, &rz6Dz);
	} else {
		calculateConnectionVectorsDMI(neighI1, neighJ1, idNeigh4,  &rx1, &ry1, &rz1, &rx1Dx, &rx1Dy, &rx1Dz,
		                              &ry1Dx, &ry1Dy, &ry1Dz, &rz1Dx, &rz1Dy, &rz1Dz);
		calculateConnectionVectorsDMI(neighI2, neighJ2, idNeigh5, &rx2, &ry2, &rz2, &rx2Dx, &rx2Dy, &rx2Dz,
		                              &ry2Dx, &ry2Dy, &ry2Dz, &rz2Dx, &rz2Dy, &rz2Dz);
		calculateConnectionVectorsDMI(neighI3, neighJ3, idNeigh6, &rx3, &ry3, &rz3, &rx3Dx, &rx3Dy, &rx3Dz,
		                              &ry3Dx, &ry3Dy, &ry3Dz, &rz3Dx, &rz3Dy, &rz3Dz);
		calculateConnectionVectorsDMI(neighI4, neighJ4, idNeigh1, &rx4, &ry4, &rz4, &rx4Dx, &rx4Dy, &rx4Dz,
		                              &ry4Dx, &ry4Dy, &ry4Dz, &rz4Dx, &rz4Dy, &rz4Dz);
		calculateConnectionVectorsDMI(neighI5, neighJ5, idNeigh2, &rx5, &ry5, &rz5, &rx5Dx, &rx5Dy, &rx5Dz,
		                              &ry5Dx, &ry5Dy, &ry5Dz, &rz5Dx, &rz5Dy, &rz5Dz);
		calculateConnectionVectorsDMI(neighI6, neighJ6, idNeigh3, &rx6, &ry6, &rz6, &rx6Dx, &rx6Dy, &rx6Dz,
		                              &ry6Dx, &ry6Dy, &ry6Dz, &rz6Dx, &rz6Dy, &rz6Dz);
	}

	/* "latticeParam" */
	int indexLattice0 = IDXtr(i1, i2);

	/* "lattice" */
	int curIndex0 = IDXtrmg(i1, i2);
	int curIndex1 = IDXtrmg1(i1, i2);
	int curIndex2 = IDXtrmg2(i1, i2);
	int curIndex3 = IDXtrmg3(i1, i2);
	int curIndex4 = IDXtrmg4(i1, i2);
	int curIndex5 = IDXtrmg5(i1, i2);
	int curIndex6 = IDXtrmg6(i1, i2);

	/* find coefficients of anisotropy and components of magnetic moments for sites */

	/* zero site */
	double mx0 = lattice[curIndex0];
	double my0 = lattice[curIndex0 + 1];
	double mz0 = lattice[curIndex0 + 2];

	/* the first site */
	double d1 = latticeParam[indexLattice0].dmiN1;
	double mx1 = lattice[curIndex1];
	double my1 = lattice[curIndex1 + 1];
	double mz1 = lattice[curIndex1 + 2];
	if ((neighI1 == forbIndex) && (neighJ1 == forbIndex)) {
		d1 = 0.0;
		mx1 = 0.0;
		my1 = 0.0;
		mz1 = 0.0;
	}

	/* the second site */
	double d2 = latticeParam[indexLattice0].dmiN2;
	double mx2 = lattice[curIndex2];
	double my2 = lattice[curIndex2 + 1];
	double mz2 = lattice[curIndex2 + 2];
	if ((neighI2 == forbIndex) && (neighJ2 == forbIndex)) {
		d2 = 0.0;
		mx2 = 0.0;
		my2 = 0.0;
		mz2 = 0.0;
	}

	/* the third site */
	double d3 = latticeParam[indexLattice0].dmiN3;
	double mx3 = lattice[curIndex3];
	double my3 = lattice[curIndex3 + 1];
	double mz3 = lattice[curIndex3 + 2];
	if ((neighI3 == forbIndex) && (neighJ3 == forbIndex)) {
		d3 = 0.0;
		mx3 = 0.0;
		my3 = 0.0;
		mz3 = 0.0;
	}

	/* the fourth site */
	double d4 = latticeParam[indexLattice0].dmiN4;
	double mx4 = lattice[curIndex4];
	double my4 = lattice[curIndex4 + 1];
	double mz4 = lattice[curIndex4 + 2];
	if ((neighI4 == forbIndex) && (neighJ4 == forbIndex)) {
		d4 = 0.0;
		mx4 = 0.0;
		my4 = 0.0;
		mz4 = 0.0;
	}

	/* the fifth site */
	double d5 = latticeParam[indexLattice0].dmiN5;
	double mx5 = lattice[curIndex5];
	double my5 = lattice[curIndex5 + 1];
	double mz5 = lattice[curIndex5 + 2];
	if ((neighI5 == forbIndex) && (neighJ5 == forbIndex)) {
		d5 = latticeParam[indexLattice0].dmiN5;
		mx5 = 0.0;
		my5 = 0.0;
		mz5 = 0.0;
	}

	/* the sixth site */
	double d6 = latticeParam[indexLattice0].dmiN6;
	double mx6 = lattice[curIndex6];
	double my6 = lattice[curIndex6 + 1];
	double mz6 = lattice[curIndex6 + 2];
	if ((neighI6 == forbIndex) && (neighJ6 == forbIndex)) {
		d6 = 0.0;
		mx6 = 0.0;
		my6 = 0.0;
		mz6 = 0.0;
	}

	/* declaration of intermediate vectors for calculation */
	double tx1 = 0.0, ty1 = 0.0, tz1 = 0.0;
	double tx2 = 0.0, ty2 = 0.0, tz2 = 0.0;
	double tx3 = 0.0, ty3 = 0.0, tz3 = 0.0;
	double tx4 = 0.0, ty4 = 0.0, tz4 = 0.0;
	double tx5 = 0.0, ty5 = 0.0, tz5 = 0.0;
	double tx6 = 0.0, ty6 = 0.0, tz6 = 0.0;

	/* calculate intermediate vectors according to boundary condition */
	if (i2 == 1) {
		calculateIntermediateVector(neighI1, neighJ1, mx0, my0, mz0, mx1, my1, mz1, &tx1, &ty1, &tz1);
		calculateIntermediateVector(neighI2, neighJ2, mx0, my0, mz0, mx2, my2, mz2, &tx2, &ty2, &tz2);
		calculateIntermediateVector(neighI3, neighJ3, mx0, my0, mz0, mx3, my3, mz3, &tx3, &ty3, &tz3);
		calculateIntermediateVector(neighI4, neighJ4, mx0, my0, mz0, mx4, my4, mz4, &tx4, &ty4, &tz4);
	} else if (i2 == N2) {
		calculateIntermediateVector(neighI1, neighJ1, mx0, my0, mz0, mx1, my1, mz1, &tx1, &ty1, &tz1);
		calculateIntermediateVector(neighI4, neighJ4, mx0, my0, mz0, mx4, my4, mz4, &tx4, &ty4, &tz4);
		calculateIntermediateVector(neighI5, neighJ5, mx0, my0, mz0, mx5, my5, mz5, &tx5, &ty5, &tz5);
		calculateIntermediateVector(neighI6, neighJ6, mx0, my0, mz0, mx6, my6, mz6, &tx6, &ty6, &tz6);
	} else {
		calculateIntermediateVector(neighI1, neighJ1, mx0, my0, mz0, mx1, my1, mz1, &tx1, &ty1, &tz1);
		calculateIntermediateVector(neighI2, neighJ2, mx0, my0, mz0, mx2, my2, mz2, &tx2, &ty2, &tz2);
		calculateIntermediateVector(neighI3, neighJ3, mx0, my0, mz0, mx3, my3, mz3, &tx3, &ty3, &tz3);
		calculateIntermediateVector(neighI4, neighJ4, mx0, my0, mz0, mx4, my4, mz4, &tx4, &ty4, &tz4);
		calculateIntermediateVector(neighI5, neighJ5, mx0, my0, mz0, mx5, my5, mz5, &tx5, &ty5, &tz5);
		calculateIntermediateVector(neighI6, neighJ6, mx0, my0, mz0, mx6, my6, mz6, &tx6, &ty6, &tz6);
	}

	/* declaration of total DMI force for given site */
	double Ftotx = 0.0, Ftoty = 0.0, Ftotz = 0.0;

	/* calculation of total force */

	/* current site */
	Ftotx -= 0.5 * d1 * (tx1 * (ny0Dx * rz1 + ny0 * rz1Dx - nz0Dx * ry1 - nz0 * ry1Dx)  +
	                     ty1 * (-nx0Dx * rz1 - nx0 * rz1Dx + nz0Dx * rx1 + nz0 * rx1Dx) +
	                     tz1 * (nx0Dx * ry1 + nx0 * ry1Dx - ny0Dx * rx1 - ny0 * rx1Dx)) +
	         0.5 * d2 * (tx2 * (ny0Dx * rz2 + ny0 * rz2Dx - nz0Dx * ry2 - nz0 * ry2Dx)  +
	                     ty2 * (-nx0Dx * rz2 - nx0 * rz2Dx + nz0Dx * rx2 + nz0 * rx2Dx) +
	                     tz2 * (nx0Dx * ry2 + nx0 * ry2Dx - ny0Dx * rx2 - ny0 * rx2Dx)) +
	         0.5 * d3 * (tx3 * (ny0Dx * rz3 + ny0 * rz3Dx - nz0Dx * ry3 - nz0 * ry3Dx)  +
	                     ty3 * (-nx0Dx * rz3 - nx0 * rz3Dx + nz0Dx * rx3 + nz0 * rx3Dx) +
	                     tz3 * (nx0Dx * ry3 + nx0 * ry3Dx - ny0Dx * rx3 - ny0 * rx3Dx)) +
	         0.5 * d4 * (tx4 * (ny0Dx * rz4 + ny0 * rz4Dx - nz0Dx * ry4 - nz0 * ry4Dx)  +
	                     ty4 * (-nx0Dx * rz4 - nx0 * rz4Dx + nz0Dx * rx4 + nz0 * rx4Dx) +
	                     tz4 * (nx0Dx * ry4 + nx0 * ry4Dx - ny0Dx * rx4 - ny0 * rx4Dx)) +
	         0.5 * d5 * (tx5 * (ny0Dx * rz5 + ny0 * rz5Dx - nz0Dx * ry5 - nz0 * ry5Dx)  +
	                     ty5 * (-nx0Dx * rz5 - nx0 * rz5Dx + nz0Dx * rx5 + nz0 * rx5Dx) +
	                     tz5 * (nx0Dx * ry5 + nx0 * ry5Dx - ny0Dx * rx5 - ny0 * rx5Dx)) +
	         0.5 * d6 * (tx6 * (ny0Dx * rz6 + ny0 * rz6Dx - nz0Dx * ry6 - nz0 * ry6Dx)  +
	                     ty6 * (-nx0Dx * rz6 - nx0 * rz6Dx + nz0Dx * rx6 + nz0 * rx6Dx) +
	                     tz6 * (nx0Dx * ry6 + nx0 * ry6Dx - ny0Dx * rx6 - ny0 * rx6Dx));

	Ftoty -= 0.5 * d1 * (tx1 * (ny0Dy * rz1 + ny0 * rz1Dy - nz0Dy * ry1 - nz0 * ry1Dy)  +
	                     ty1 * (-nx0Dy * rz1 - nx0 * rz1Dy + nz0Dy * rx1 + nz0 * rx1Dy) +
	                     tz1 * (nx0Dy * ry1 + nx0 * ry1Dy - ny0Dy * rx1 - ny0 * rx1Dy)) +
	         0.5 * d2 * (tx2 * (ny0Dy * rz2 + ny0 * rz2Dy - nz0Dy * ry2 - nz0 * ry2Dy)  +
	                     ty2 * (-nx0Dy * rz2 - nx0 * rz2Dy + nz0Dy * rx2 + nz0 * rx2Dy) +
	                     tz2 * (nx0Dy * ry2 + nx0 * ry2Dy - ny0Dy * rx2 - ny0 * rx2Dy)) +
	         0.5 * d3 * (tx3 * (ny0Dy * rz3 + ny0 * rz3Dy - nz0Dy * ry3 - nz0 * ry3Dy)  +
	                     ty3 * (-nx0Dy * rz3 - nx0 * rz3Dy + nz0Dy * rx3 + nz0 * rx3Dy) +
	                     tz3 * (nx0Dy * ry3 + nx0 * ry3Dy - ny0Dy * rx3 - ny0 * rx3Dy)) +
	         0.5 * d4 * (tx4 * (ny0Dy * rz4 + ny0 * rz4Dy - nz0Dy * ry4 - nz0 * ry4Dy)  +
	                     ty4 * (-nx0Dy * rz4 - nx0 * rz4Dy + nz0Dy * rx4 + nz0 * rx4Dy) +
	                     tz4 * (nx0Dy * ry4 + nx0 * ry4Dy - ny0Dy * rx4 - ny0 * rx4Dy)) +
	         0.5 * d5 * (tx5 * (ny0Dy * rz5 + ny0 * rz5Dy - nz0Dy * ry5 - nz0 * ry5Dy)  +
	                     ty5 * (-nx0Dy * rz5 - nx0 * rz5Dy + nz0Dy * rx5 + nz0 * rx5Dy) +
	                     tz5 * (nx0Dy * ry5 + nx0 * ry5Dy - ny0Dy * rx5 - ny0 * rx5Dy)) +
	         0.5 * d6 * (tx6 * (ny0Dy * rz6 + ny0 * rz6Dy - nz0Dy * ry6 - nz0 * ry6Dy)  +
	                     ty6 * (-nx0Dy * rz6 - nx0 * rz6Dy + nz0Dy * rx6 + nz0 * rx6Dy) +
	                     tz6 * (nx0Dy * ry6 + nx0 * ry6Dy - ny0Dy * rx6 - ny0 * rx6Dy));

	Ftotz -= 0.5 * d1 * (tx1 * (ny0Dz * rz1 + ny0 * rz1Dz - nz0Dz * ry1 - nz0 * ry1Dz)  +
	                     ty1 * (-nx0Dz * rz1 - nx0 * rz1Dz + nz0Dz * rx1 + nz0 * rx1Dz) +
	                     tz1 * (nx0Dz * ry1 + nx0 * ry1Dz - ny0Dz * rx1 - ny0 * rx1Dz)) +
	         0.5 * d2 * (tx2 * (ny0Dz * rz2 + ny0 * rz2Dz - nz0Dz * ry2 - nz0 * ry2Dz)  +
	                     ty2 * (-nx0Dz * rz2 - nx0 * rz2Dz + nz0Dz * rx2 + nz0 * rx2Dz) +
	                     tz2 * (nx0Dz * ry2 + nx0 * ry2Dz - ny0Dz * rx2 - ny0 * rx2Dz)) +
	         0.5 * d3 * (tx3 * (ny0Dz * rz3 + ny0 * rz3Dz - nz0Dz * ry3 - nz0 * ry3Dz)  +
	                     ty3 * (-nx0Dz * rz3 - nx0 * rz3Dz + nz0Dz * rx3 + nz0 * rx3Dz) +
	                     tz3 * (nx0Dz * ry3 + nx0 * ry3Dz - ny0Dz * rx3 - ny0 * rx3Dz)) +
	         0.5 * d4 * (tx4 * (ny0Dz * rz4 + ny0 * rz4Dz - nz0Dz * ry4 - nz0 * ry4Dz)  +
	                     ty4 * (-nx0Dz * rz4 - nx0 * rz4Dz + nz0Dz * rx4 + nz0 * rx4Dz) +
	                     tz4 * (nx0Dz * ry4 + nx0 * ry4Dz - ny0Dz * rx4 - ny0 * rx4Dz)) +
	         0.5 * d5 * (tx5 * (ny0Dz * rz5 + ny0 * rz5Dz - nz0Dz * ry5 - nz0 * ry5Dz)  +
	                     ty5 * (-nx0Dz * rz5 - nx0 * rz5Dz + nz0Dz * rx5 + nz0 * rx5Dz) +
	                     tz5 * (nx0Dz * ry5 + nx0 * ry5Dz - ny0Dz * rx5 - ny0 * rx5Dz)) +
	         0.5 * d6 * (tx6 * (ny0Dz * rz6 + ny0 * rz6Dz - nz0Dz * ry6 - nz0 * ry6Dz)  +
	                     ty6 * (-nx0Dz * rz6 - nx0 * rz6Dz + nz0Dz * rx6 + nz0 * rx6Dz) +
	                     tz6 * (nx0Dz * ry6 + nx0 * ry6Dz - ny0Dz * rx6 - ny0 * rx6Dz));

	/* calculate total force of other sites according to boundary condition */
	if (i2 == 1) {
		calculateForceDmiInterfaceSite(neighI1, neighJ1, idNeigh4, idSite1, nx1, ny1, nz1,
		                               nx1Dx, ny1Dx, nz1Dx,
		                               nx1Dy, ny1Dy, nz1Dy,
		                               nx1Dz, ny1Dz, nz1Dz,
		                               rx1Dx, ry1Dx, rz1Dx,
		                               rx1Dy, ry1Dy, rz1Dy,
		                               rx1Dz, ry1Dz, rz1Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
		calculateForceDmiInterfaceSite(neighI2, neighJ2, idNeigh5, idSite2, nx2, ny2, nz2,
		                               nx2Dx, ny2Dx, nz2Dx,
		                               nx2Dy, ny2Dy, nz2Dy,
		                               nx2Dz, ny2Dz, nz2Dz,
		                               rx2Dx, ry2Dx, rz2Dx,
		                               rx2Dy, ry2Dy, rz2Dy,
		                               rx2Dz, ry2Dz, rz2Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
		calculateForceDmiInterfaceSite(neighI3, neighJ3, idNeigh6, idSite3, nx3, ny3, nz3,
		                               nx3Dx, ny3Dx, nz3Dx,
		                               nx3Dy, ny3Dy, nz3Dy,
		                               nx3Dz, ny3Dz, nz3Dz,
		                               rx3Dx, ry3Dx, rz3Dx,
		                               rx3Dy, ry3Dy, rz3Dy,
		                               rx3Dz, ry3Dz, rz3Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
		calculateForceDmiInterfaceSite(neighI4, neighJ4, idNeigh1, idSite4, nx4, ny4, nz4,
		                               nx4Dx, ny4Dx, nz4Dx,
		                               nx4Dy, ny4Dy, nz4Dy,
		                               nx4Dz, ny4Dz, nz4Dz,
		                               rx4Dx, ry4Dx, rz4Dx,
		                               rx4Dy, ry4Dy, rz4Dy,
		                               rx4Dz, ry4Dz, rz4Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
	} else if (i2 == N2) {
		calculateForceDmiInterfaceSite(neighI1, neighJ1, idNeigh4, idSite1, nx1, ny1, nz1,
		                               nx1Dx, ny1Dx, nz1Dx,
		                               nx1Dy, ny1Dy, nz1Dy,
		                               nx1Dz, ny1Dz, nz1Dz,
		                               rx1Dx, ry1Dx, rz1Dx,
		                               rx1Dy, ry1Dy, rz1Dy,
		                               rx1Dz, ry1Dz, rz1Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
		calculateForceDmiInterfaceSite(neighI4, neighJ4, idNeigh1, idSite4, nx4, ny4, nz4,
		                               nx4Dx, ny4Dx, nz4Dx,
		                               nx4Dy, ny4Dy, nz4Dy,
		                               nx4Dz, ny4Dz, nz4Dz,
		                               rx4Dx, ry4Dx, rz4Dx,
		                               rx4Dy, ry4Dy, rz4Dy,
		                               rx4Dz, ry4Dz, rz4Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
		calculateForceDmiInterfaceSite(neighI5, neighJ5, idNeigh2, idSite5, nx5, ny5, nz5,
		                               nx5Dx, ny5Dx, nz5Dx,
		                               nx5Dy, ny5Dy, nz5Dy,
		                               nx5Dz, ny5Dz, nz5Dz,
		                               rx5Dx, ry5Dx, rz5Dx,
		                               rx5Dy, ry5Dy, rz5Dy,
		                               rx5Dz, ry5Dz, rz5Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
		calculateForceDmiInterfaceSite(neighI6, neighJ6, idNeigh3, idSite6, nx6, ny6, nz6,
		                               nx6Dx, ny6Dx, nz6Dx,
		                               nx6Dy, ny6Dy, nz6Dy,
		                               nx6Dz, ny6Dz, nz6Dz,
		                               rx6Dx, ry6Dx, rz6Dx,
		                               rx6Dy, ry6Dy, rz6Dy,
		                               rx6Dz, ry6Dz, rz6Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
	} else {
		calculateForceDmiInterfaceSite(neighI1, neighJ1, idNeigh4, idSite1, nx1, ny1, nz1,
		                               nx1Dx, ny1Dx, nz1Dx,
		                               nx1Dy, ny1Dy, nz1Dy,
		                               nx1Dz, ny1Dz, nz1Dz,
		                               rx1Dx, ry1Dx, rz1Dx,
		                               rx1Dy, ry1Dy, rz1Dy,
		                               rx1Dz, ry1Dz, rz1Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
		calculateForceDmiInterfaceSite(neighI2, neighJ2, idNeigh5, idSite2, nx2, ny2, nz2,
		                               nx2Dx, ny2Dx, nz2Dx,
		                               nx2Dy, ny2Dy, nz2Dy,
		                               nx2Dz, ny2Dz, nz2Dz,
		                               rx2Dx, ry2Dx, rz2Dx,
		                               rx2Dy, ry2Dy, rz2Dy,
		                               rx2Dz, ry2Dz, rz2Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
		calculateForceDmiInterfaceSite(neighI3, neighJ3, idNeigh6, idSite3, nx3, ny3, nz3,
		                               nx3Dx, ny3Dx, nz3Dx,
		                               nx3Dy, ny3Dy, nz3Dy,
		                               nx3Dz, ny3Dz, nz3Dz,
		                               rx3Dx, ry3Dx, rz3Dx,
		                               rx3Dy, ry3Dy, rz3Dy,
		                               rx3Dz, ry3Dz, rz3Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
		calculateForceDmiInterfaceSite(neighI4, neighJ4, idNeigh1, idSite4, nx4, ny4, nz4,
		                               nx4Dx, ny4Dx, nz4Dx,
		                               nx4Dy, ny4Dy, nz4Dy,
		                               nx4Dz, ny4Dz, nz4Dz,
		                               rx4Dx, ry4Dx, rz4Dx,
		                               rx4Dy, ry4Dy, rz4Dy,
		                               rx4Dz, ry4Dz, rz4Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
		calculateForceDmiInterfaceSite(neighI5, neighJ5, idNeigh2, idSite5, nx5, ny5, nz5,
		                               nx5Dx, ny5Dx, nz5Dx,
		                               nx5Dy, ny5Dy, nz5Dy,
		                               nx5Dz, ny5Dz, nz5Dz,
		                               rx5Dx, ry5Dx, rz5Dx,
		                               rx5Dy, ry5Dy, rz5Dy,
		                               rx5Dz, ry5Dz, rz5Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
		calculateForceDmiInterfaceSite(neighI6, neighJ6, idNeigh3, idSite6, nx6, ny6, nz6,
		                               nx6Dx, ny6Dx, nz6Dx,
		                               nx6Dy, ny6Dy, nz6Dy,
		                               nx6Dz, ny6Dz, nz6Dz,
		                               rx6Dx, ry6Dx, rz6Dx,
		                               rx6Dy, ry6Dy, rz6Dy,
		                               rx6Dz, ry6Dz, rz6Dz,
		                               &Ftotx, &Ftoty, &Ftotz);
	}

	//to debug code
	//fprintf(stderr, "%d %d %f %f %f\n", i1, i2, Ftotx, Ftoty, Ftotz);

	/* add DMI force for given site to total force */
	*Fx += Ftotx / 2.0;
	*Fy += Ftoty / 2.0;
	*Fz += Ftotz / 2.0;

}
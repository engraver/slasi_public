/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include <Python.h>
#include "scriptDMI.h"

/* initialize all vectors for Dzyaloshinsky-Moria interaction */
int initAllAxis(void) {

	/* declarations of objects to work with Python.h */
	PyObject *pArgs, *pFunc;
	PyObject *pValue;

	/* declarations of variable */
	double D11, D12, D13, D21, D22, D23, D31, D32, D33;
	int tester;

	/* check if module was found */
	if (pModulePython != NULL) {

		/* search of function wint name "getDMI" in module */
		pFunc = PyObject_GetAttrString(pModulePython, "getDMI");
		/* check if function was found */
		if (pFunc && PyCallable_Check(pFunc)) {

			/* pass through each magnetic moment */
			for (int i1 = 0; i1 < userVars.N1; i1++) {
				for (int i2 = 0; i2 < userVars.N2; i2++) {
					for (int i3 = 0; i3 < userVars.N3; i3++) {
						if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
						    i3 < userVars.N3 - 1) {

							/* number of spin in lattice */
							int indexLattice = IDX(i1, i2, i3);
							/* check if node is magnetic */
							if (latticeParam[indexLattice].magnetic) {

								/* neighbor's indexes */
								size_t p1, p2, p3;

								/* calculate neighbor's indices */
								p1 = indexLattice + userVars.N3 * userVars.N2;
								p2 = indexLattice + userVars.N3;
								p3 = indexLattice + 1;

								/* creating parameters to call function (coordinates and indices) */
								pArgs = PyTuple_New(6);

								/* the first coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].x) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getDMI!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 0, pValue);

								/* the second coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].y) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getDMI!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 1, pValue);

								/* the third coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].z) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getDMI!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 2, pValue);

								/* the first index */
								pValue = PyLong_FromLong(i1);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getDMI!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 3, pValue);

								/* the second index */
								pValue = PyLong_FromLong(i2);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getDMI!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 4, pValue);

								/* the third index */
								pValue = PyLong_FromLong(i3);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getDMI!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 5, pValue);

								/* call function */
								pValue = PyObject_CallObject(pFunc, pArgs);
								/* check results of calling */
								if (pValue != NULL) {

									/* get results of calling */
									tester = PyArg_ParseTuple(pValue, "ddddddddd", &D11, &D12, &D13,
									                          &D21, &D22, &D23, &D31, &D32, &D33);
									/* check if it was able to get */
									if (tester) {
										/* save results */
										/* the first vector */
										latticeParam[indexLattice].D11p = D11;
										latticeParam[indexLattice].D12p = D12;
										latticeParam[indexLattice].D13p = D13;

										latticeParam[p1].D11m = (-1.0) * D11;
										latticeParam[p1].D12m = (-1.0) * D12;
										latticeParam[p1].D13m = (-1.0) * D13;

										/* the second vector */
										latticeParam[indexLattice].D21p = D21;
										latticeParam[indexLattice].D22p = D22;
										latticeParam[indexLattice].D23p = D23;

										latticeParam[p2].D21m = (-1.0) * D21;
										latticeParam[p2].D22m = (-1.0) * D22;
										latticeParam[p2].D23m = (-1.0) * D23;

										/* the third vector */
										latticeParam[indexLattice].D31p = D31;
										latticeParam[indexLattice].D32p = D32;
										latticeParam[indexLattice].D33p = D33;

										latticeParam[p3].D31m = (-1.0) * D31;
										latticeParam[p3].D32m = (-1.0) * D32;
										latticeParam[p3].D33m = (-1.0) * D33;

									} else {
										/* output error */
										fprintf(stderr, "Call of python function getDMI failed!\n");
										return 1;
									}
									Py_DECREF(pValue);

								} else {
									/* output error */
									Py_DECREF(pFunc);
									Py_DECREF(pModulePython);
									PyErr_Print();
									fprintf(stderr, "Call of python function getDMI failed\n");
									return 1;
								}

							}

						}
					}
				}
			}

		} else {
			/* output error */
			if (PyErr_Occurred())
				PyErr_Print();
			fprintf(stderr, "Cannot find function getDMI in python file!\n");
			return 1;
		}
		Py_XDECREF(pFunc);

	} else {
		/* output error */
		PyErr_Print();
		fprintf(stderr, "Failed to load python file %s!\n", userVars.scriptFile);
		return 1;
	}

	return 0;

}

int scriptDMI() {

	/* initialize all vectors */
	if (initAllAxis()) {
		return 1;
	}
	return 0;

}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "dmi.h"
#include "../../utils/parallel_utils.h"

int setupDMI(double **numbers, int indlp, int indn) {


	if ( (((userVars.iD11 != -1) && (userVars.iD12 != -1) && (userVars.iD13 != -1) &&
	       (userVars.iD21 != -1) && (userVars.iD22 != -1) && (userVars.iD23 != -1) &&
	       (userVars.iD31 != -1) && (userVars.iD32 != -1) && (userVars.iD33 != -1) &&
	       (userVars.DMItype == DMI_FILE)
	      ) || ((userVars.bD11) && (userVars.bD12) && (userVars.bD13) &&
	            (userVars.bD21) && (userVars.bD22) && (userVars.bD23) &&
	            (userVars.bD31) && (userVars.bD32) && (userVars.bD33) &&
	            (userVars.DMItype == DMI_CONST))) &&
	     userVars.latticeType != LATTICE_CUBIC
			) {
		messageToStream("DMI.C: D11, D12, D13, D21, D22, D23, D31, D32, D33 can be given only for latticeType = cubic!\n", stderr);
		return 1;
	}

	if ((userVars.iD11 != -1) && (userVars.iD12 != -1) && (userVars.iD13 != -1) &&
	    (userVars.iD21 != -1) && (userVars.iD22 != -1) && (userVars.iD23 != -1) &&
	    (userVars.iD31 != -1) && (userVars.iD32 != -1) && (userVars.iD33 != -1) &&
	    (userVars.DMItype == DMI_FILE)) {
		/* from file */
		if (numbers == NULL) {
			char msg[] = "DMI.C: paramFile is not processed, while expected for setupDMI()!\n";
			fprintf(flog, "%s", msg);
			fprintf(stderr, "%s", msg);
			return 1;
		}


		/* neighbor's indexes */
		size_t p1, p2, p3;

		/* calculate neighbor's indices */
		p1 = (size_t)(indlp + userVars.N3 * userVars.N2);
		p2 = (size_t)(indlp + userVars.N3);
		p3 = (size_t)(indlp + 1);

		/* the first axis */
		latticeParam[indlp].D11p = numbers[indn][userVars.iD11];
		latticeParam[indlp].D12p = numbers[indn][userVars.iD12];
		latticeParam[indlp].D13p = numbers[indn][userVars.iD13];

		latticeParam[p1].D11m = (-1.0) * numbers[indn][userVars.iD11];
		latticeParam[p1].D12m = (-1.0) * numbers[indn][userVars.iD12];
		latticeParam[p1].D13m = (-1.0) * numbers[indn][userVars.iD13];

		/* the second axis */
		latticeParam[indlp].D21p = numbers[indn][userVars.iD21];
		latticeParam[indlp].D22p = numbers[indn][userVars.iD22];
		latticeParam[indlp].D23p = numbers[indn][userVars.iD23];

		latticeParam[p2].D21m = (-1.0) * numbers[indn][userVars.iD21];
		latticeParam[p2].D22m = (-1.0) * numbers[indn][userVars.iD22];
		latticeParam[p2].D23m = (-1.0) * numbers[indn][userVars.iD23];

		/* the third axis */
		latticeParam[indlp].D31p = numbers[indn][userVars.iD31];
		latticeParam[indlp].D32p = numbers[indn][userVars.iD32];
		latticeParam[indlp].D33p = numbers[indn][userVars.iD33];

		latticeParam[p3].D31m = (-1.0) * numbers[indn][userVars.iD31];
		latticeParam[p3].D32m = (-1.0) * numbers[indn][userVars.iD32];
		latticeParam[p3].D33m = (-1.0) * numbers[indn][userVars.iD33];

		/* variable to test becomes "true" */
		testDMI = true;
	} else if ((userVars.bD11) && (userVars.bD12) && (userVars.bD13) &&
	           (userVars.bD21) && (userVars.bD22) && (userVars.bD23) &&
	           (userVars.bD31) && (userVars.bD32) && (userVars.bD33) &&
	           (userVars.DMItype == DMI_CONST)) {
		/* constant value */

		/* the first axis */
		latticeParam[indlp].D11p = userVars.D11;
		latticeParam[indlp].D12p = userVars.D12;
		latticeParam[indlp].D13p = userVars.D13;

		latticeParam[indlp].D11m = (-1.0) * userVars.D11;
		latticeParam[indlp].D12m = (-1.0) * userVars.D12;
		latticeParam[indlp].D13m = (-1.0) * userVars.D13;

		/* the second axis */
		latticeParam[indlp].D21p = userVars.D21;
		latticeParam[indlp].D22p = userVars.D22;
		latticeParam[indlp].D23p = userVars.D23;

		latticeParam[indlp].D21m = (-1.0) * userVars.D21;
		latticeParam[indlp].D22m = (-1.0) * userVars.D22;
		latticeParam[indlp].D23m = (-1.0) * userVars.D23;

		/* the third axis */
		latticeParam[indlp].D31p = userVars.D31;
		latticeParam[indlp].D32p = userVars.D32;
		latticeParam[indlp].D33p = userVars.D33;

		latticeParam[indlp].D31m = (-1.0) * userVars.D31;
		latticeParam[indlp].D32m = (-1.0) * userVars.D32;
		latticeParam[indlp].D33m = (-1.0) * userVars.D33;

		/* variable to test becomes "true" */
		testDMI = true;

	} else {
		/* default zero DMI */

		/* the first axis */
		latticeParam[indlp].D11p = 0.0;
		latticeParam[indlp].D12p = 0.0;
		latticeParam[indlp].D13p = 0.0;

		latticeParam[indlp].D11m = 0.0;
		latticeParam[indlp].D12m = 0.0;
		latticeParam[indlp].D13m = 0.0;

		/* the second axis */
		latticeParam[indlp].D21p = 0.0;
		latticeParam[indlp].D22p = 0.0;
		latticeParam[indlp].D23p = 0.0;

		latticeParam[indlp].D21m = 0.0;
		latticeParam[indlp].D22m = 0.0;
		latticeParam[indlp].D23m = 0.0;

		/* the third axis */
		latticeParam[indlp].D31p = 0.0;
		latticeParam[indlp].D32p = 0.0;
		latticeParam[indlp].D33p = 0.0;

		latticeParam[indlp].D31m = 0.0;
		latticeParam[indlp].D32m = 0.0;
		latticeParam[indlp].D33m = 0.0;

	}

	return 0;

}

int setupDMITriangleBulk(double **numbers, int indlp, int indn) {

	if ((userVars.idmiB1 != -1) && (userVars.idmiB2 != -1) && (userVars.idmiB3 != -1) &&
	    (userVars.idmiB4 != -1) && (userVars.idmiB5 != -1) && (userVars.idmiB6 != -1)) {
		/* value from parameter file */
		if (numbers == NULL) {
			char msg[] = "DMI.C: paramFile is not processed, while expected for setupDMITriangleBulk()!\n";
			fprintf(flog, "%s", msg);
			fprintf(stderr, "%s", msg);
			return 1;
		}

		if (userVars.latticeType != LATTICE_TRIANGULAR) {
			messageToStream("DMI.C: dmiBtri, dmiNtri can be given only for latticeType = triangular!\n", stderr);
			return 1;
		}

		latticeParam[indlp].dmiB1 = numbers[indn][userVars.idmiB1];
		latticeParam[indlp].dmiB2 = numbers[indn][userVars.idmiB2];
		latticeParam[indlp].dmiB3 = numbers[indn][userVars.idmiB3];
		latticeParam[indlp].dmiB4 = numbers[indn][userVars.idmiB4];
		latticeParam[indlp].dmiB5 = numbers[indn][userVars.idmiB5];
		latticeParam[indlp].dmiB6 = numbers[indn][userVars.idmiB6];

		testDMI = true;
	} else if (userVars.bdmiBtri) {
		/* constant value */

		if (userVars.latticeType != LATTICE_TRIANGULAR) {
			messageToStream("DMI.C: dmiBtri, dmiNtri can be given only for latticeType = triangular!\n", stderr);
			return 1;
		}

		latticeParam[indlp].dmiB1 = userVars.dmiBtri;
		latticeParam[indlp].dmiB2 = userVars.dmiBtri;
		latticeParam[indlp].dmiB3 = userVars.dmiBtri;
		latticeParam[indlp].dmiB4 = userVars.dmiBtri;
		latticeParam[indlp].dmiB5 = userVars.dmiBtri;
		latticeParam[indlp].dmiB6 = userVars.dmiBtri;

		testDMI = true;
	} /* else {
        char msg[] = "DMI.C: Something wrong with bulk DMI parameters for triangular lattice (Are all of them given in paramFile?)!\n";
        fprintf(flog, "%s", msg);
        fprintf(stderr, "%s", msg);
        return 1;
    } */

	return 0;

}

int setupDMITriangleInterface(double **numbers, int indlp, int indn) {

	if ((userVars.idmiN1 != -1) && (userVars.idmiN2 != -1) && (userVars.idmiN3 != -1) &&
	    (userVars.idmiN4 != -1) && (userVars.idmiN5 != -1) && (userVars.idmiN6 != -1)) {
		/* value from parameter file */
		if (numbers == NULL) {
			char msg[] = "DMI.C: paramFile is not processed, while expected for setupDMITriangleInterface()!\n";
			fprintf(flog, "%s", msg);
			fprintf(stderr, "%s", msg);
			return 1;
		}

		if (userVars.latticeType != LATTICE_TRIANGULAR) {
			messageToStream("DMI.C: dmiNtri, dmiBtri can be given only for latticeType = triangular!\n", stderr);
			return 1;
		}

		latticeParam[indlp].dmiN1 = numbers[indn][userVars.idmiN1];
		latticeParam[indlp].dmiN2 = numbers[indn][userVars.idmiN2];
		latticeParam[indlp].dmiN3 = numbers[indn][userVars.idmiN3];
		latticeParam[indlp].dmiN4 = numbers[indn][userVars.idmiN4];
		latticeParam[indlp].dmiN5 = numbers[indn][userVars.idmiN5];
		latticeParam[indlp].dmiN6 = numbers[indn][userVars.idmiN6];

		testDMI = true;
	} else if (userVars.bdmiBtri) {
		/* constant value */

		if (userVars.latticeType != LATTICE_TRIANGULAR) {
			messageToStream("DMI.C: dmiNtri, dmiBtri can be given only for latticeType = triangular!\n", stderr);
			return 1;
		}

		latticeParam[indlp].dmiN1 = userVars.dmiNtri;
		latticeParam[indlp].dmiN2 = userVars.dmiNtri;
		latticeParam[indlp].dmiN3 = userVars.dmiNtri;
		latticeParam[indlp].dmiN4 = userVars.dmiNtri;
		latticeParam[indlp].dmiN5 = userVars.dmiNtri;
		latticeParam[indlp].dmiN6 = userVars.dmiNtri;

		testDMI = true;
	} /* else {
        char msg[] = "DMI.C: Something wrong with interface DMI parameters for triangular lattice (Are all of them given in paramFile?)!\n";
        fprintf(flog, "%s", msg);
        fprintf(stderr, "%s", msg);
        return 1;
    } */

	return 0;

}

int setupDMICubicBulk(double **numbers, int indlp, int indn) {

	 if (userVars.bdmiBulk) {
	 	/* constant value */
		if (userVars.latticeType != LATTICE_CUBIC) {
			messageToStream("DMI.C: dmiBsqr can be given only for latticeType = triangular!\n", stderr);
			return 1;
		}

		latticeParam[indlp].dmiB1cub = userVars.dmiBulk;
		latticeParam[indlp].dmiB2cub = userVars.dmiBulk;
		latticeParam[indlp].dmiB3cub = userVars.dmiBulk;
		latticeParam[indlp].dmiB4cub = userVars.dmiBulk;
		latticeParam[indlp].dmiB5cub = userVars.dmiBulk;
		latticeParam[indlp].dmiB6cub = userVars.dmiBulk;

		testDMI = true;
	}

	return 0;

}

void dmiField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EdmiAxis1, double *EdmiAxis2,
              double *EdmiAxis3) {

	/* project vectors of Dzyaloshinsky-Moria interaction on orthonormal basis */
	/* the first axis */
	double D11p, D12p, D13p;
	double D11m, D12m, D13m;
	/* the second axis */
	double D21p, D22p, D23p;
	double D21m, D22m, D23m;
	/* the third axis */
	double D31p, D32p, D33p;
	double D31m, D32m, D33m;

	/* if it is curve, then we will use TNB-orthonormal basis */
	if (userVars.dimensionOfSystem == 1) {

		/* the first axis */
		D11p = latticeParam[indexLattice].D11p * latticeParam[indexLattice].tx +
		       latticeParam[indexLattice].D12p * latticeParam[indexLattice].nx +
		       latticeParam[indexLattice].D13p * latticeParam[indexLattice].bx;

		D12p = latticeParam[indexLattice].D11p * latticeParam[indexLattice].ty +
		       latticeParam[indexLattice].D12p * latticeParam[indexLattice].ny +
		       latticeParam[indexLattice].D13p * latticeParam[indexLattice].by;

		D13p = latticeParam[indexLattice].D11p * latticeParam[indexLattice].tz +
		       latticeParam[indexLattice].D12p * latticeParam[indexLattice].nz +
		       latticeParam[indexLattice].D13p * latticeParam[indexLattice].bz;

		D11m = latticeParam[indexLattice].D11m * latticeParam[indexLattice].tx +
		       latticeParam[indexLattice].D12m * latticeParam[indexLattice].nx +
		       latticeParam[indexLattice].D13m * latticeParam[indexLattice].bx;

		D12m = latticeParam[indexLattice].D11m * latticeParam[indexLattice].ty +
		       latticeParam[indexLattice].D12m * latticeParam[indexLattice].ny +
		       latticeParam[indexLattice].D13m * latticeParam[indexLattice].by;

		D13m = latticeParam[indexLattice].D11m * latticeParam[indexLattice].tz +
		       latticeParam[indexLattice].D12m * latticeParam[indexLattice].nz +
		       latticeParam[indexLattice].D13m * latticeParam[indexLattice].bz;

		/* the second axis */
		D21p = latticeParam[indexLattice].D21p * latticeParam[indexLattice].tx +
		       latticeParam[indexLattice].D22p * latticeParam[indexLattice].nx +
		       latticeParam[indexLattice].D23p * latticeParam[indexLattice].bx;

		D22p = latticeParam[indexLattice].D21p * latticeParam[indexLattice].ty +
		       latticeParam[indexLattice].D22p * latticeParam[indexLattice].ny +
		       latticeParam[indexLattice].D23p * latticeParam[indexLattice].by;

		D23p = latticeParam[indexLattice].D21p * latticeParam[indexLattice].tz +
		       latticeParam[indexLattice].D22p * latticeParam[indexLattice].nz +
		       latticeParam[indexLattice].D23p * latticeParam[indexLattice].bz;

		D21m = latticeParam[indexLattice].D21m * latticeParam[indexLattice].tx +
		       latticeParam[indexLattice].D22m * latticeParam[indexLattice].nx +
		       latticeParam[indexLattice].D23m * latticeParam[indexLattice].bx;

		D22m = latticeParam[indexLattice].D21m * latticeParam[indexLattice].ty +
		       latticeParam[indexLattice].D22m * latticeParam[indexLattice].ny +
		       latticeParam[indexLattice].D23m * latticeParam[indexLattice].by;

		D23m = latticeParam[indexLattice].D21m * latticeParam[indexLattice].tz +
		       latticeParam[indexLattice].D22m * latticeParam[indexLattice].nz +
		       latticeParam[indexLattice].D23m * latticeParam[indexLattice].bz;

		/* the third axis */
		D31p = latticeParam[indexLattice].D31p * latticeParam[indexLattice].tx +
		       latticeParam[indexLattice].D32p * latticeParam[indexLattice].nx +
		       latticeParam[indexLattice].D33p * latticeParam[indexLattice].bx;

		D32p = latticeParam[indexLattice].D31p * latticeParam[indexLattice].ty +
		       latticeParam[indexLattice].D32p * latticeParam[indexLattice].ny +
		       latticeParam[indexLattice].D33p * latticeParam[indexLattice].by;

		D33p = latticeParam[indexLattice].D31p * latticeParam[indexLattice].tz +
		       latticeParam[indexLattice].D32p * latticeParam[indexLattice].nz +
		       latticeParam[indexLattice].D33p * latticeParam[indexLattice].bz;

		D31m = latticeParam[indexLattice].D31m * latticeParam[indexLattice].tx +
		       latticeParam[indexLattice].D32m * latticeParam[indexLattice].nx +
		       latticeParam[indexLattice].D33m * latticeParam[indexLattice].bx;

		D32m = latticeParam[indexLattice].D31m * latticeParam[indexLattice].ty +
		       latticeParam[indexLattice].D32m * latticeParam[indexLattice].ny +
		       latticeParam[indexLattice].D33m * latticeParam[indexLattice].by;

		D33m = latticeParam[indexLattice].D31m * latticeParam[indexLattice].tz +
		       latticeParam[indexLattice].D32m * latticeParam[indexLattice].nz +
		       latticeParam[indexLattice].D33m * latticeParam[indexLattice].bz;

		/* if it is not curve, then we we will use default orthonormal basis */
	} else {

		/* the first axis */
		D11p = latticeParam[indexLattice].D11p;
		D12p = latticeParam[indexLattice].D12p;
		D13p = latticeParam[indexLattice].D13p;

		D11m = latticeParam[indexLattice].D11m;
		D12m = latticeParam[indexLattice].D12m;
		D13m = latticeParam[indexLattice].D13m;

		/* the second axis */
		D21p = latticeParam[indexLattice].D21p;
		D22p = latticeParam[indexLattice].D22p;
		D23p = latticeParam[indexLattice].D23p;

		D21m = latticeParam[indexLattice].D21m;
		D22m = latticeParam[indexLattice].D22m;
		D23m = latticeParam[indexLattice].D23m;

		/* the third axis */
		D31p = latticeParam[indexLattice].D31p;
		D32p = latticeParam[indexLattice].D32p;
		D33p = latticeParam[indexLattice].D33p;

		D31m = latticeParam[indexLattice].D31m;
		D32m = latticeParam[indexLattice].D32m;
		D33m = latticeParam[indexLattice].D33m;

	}

	/* neighbor's indexes */
	size_t p1, p2, p3, m1, m2, m3;

	/* calculate neighbor's indices */
	p1 = curIndex + 3 * userVars.N3 * userVars.N2;
	p2 = curIndex + 3 * userVars.N3;
	p3 = curIndex + 3;
	m1 = curIndex - 3 * userVars.N3 * userVars.N2;
	m2 = curIndex - 3 * userVars.N3;
	m3 = curIndex - 3;

	/* declarations of effective fields */
	/* the first axis */
	double effectFieldX1p, effectFieldY1p, effectFieldZ1p;
	double effectFieldX1m, effectFieldY1m, effectFieldZ1m;
	/* the second axis */
	double effectFieldX2p, effectFieldY2p, effectFieldZ2p;
	double effectFieldX2m, effectFieldY2m, effectFieldZ2m;
	/* the third axis */
	double effectFieldX3p, effectFieldY3p, effectFieldZ3p;
	double effectFieldX3m, effectFieldY3m, effectFieldZ3m;

	/* calculate effective field for the first axis */
	effectFieldX1p = D12p * lattice[p1 + 2] - D13p * lattice[p1 + 1];
	effectFieldY1p = D13p * lattice[p1] - D11p * lattice[p1 + 2];
	effectFieldZ1p = D11p * lattice[p1 + 1] - D12p * lattice[p1];

	effectFieldX1m = D12m * lattice[m1 + 2] - D13m * lattice[m1 + 1];
	effectFieldY1m = D13m * lattice[m1] - D11m * lattice[m1 + 2];
	effectFieldZ1m = D11m * lattice[m1 + 1] - D12m * lattice[m1];

	/* calculate energy for the first axis */
	latticeParam[indexLattice].EnergyDMIAxis1Norm = -((effectFieldX1p + effectFieldX1m) * lattice[curIndex] +
	                                                  (effectFieldY1p + effectFieldY1m) * lattice[curIndex + 1] +
	                                                  (effectFieldZ1p + effectFieldZ1m) * lattice[curIndex + 2]);

	*EdmiAxis1 += latticeParam[indexLattice].EnergyDMIAxis1Norm;

	/* calculate effective field for the second axis */
	effectFieldX2p = D22p * lattice[p2 + 2] - D23p * lattice[p2 + 1];
	effectFieldY2p = D23p * lattice[p2] - D21p * lattice[p2 + 2];
	effectFieldZ2p = D21p * lattice[p2 + 1] - D22p * lattice[p2];

	effectFieldX2m = D22m * lattice[m2 + 2] - D23m * lattice[m2 + 1];
	effectFieldY2m = D23m * lattice[m2] - D21m * lattice[m2 + 2];
	effectFieldZ2m = D21m * lattice[m2 + 1] - D22m * lattice[m2];

	/* calculate energy for the second axis */
	latticeParam[indexLattice].EnergyDMIAxis2Norm = -((effectFieldX2p + effectFieldX2m) * lattice[curIndex] +
	                                                  (effectFieldY2p + effectFieldY2m) * lattice[curIndex + 1] +
	                                                  (effectFieldZ2p + effectFieldZ2m) * lattice[curIndex + 2]);

	*EdmiAxis2 += latticeParam[indexLattice].EnergyDMIAxis2Norm;

	/* calculate effective field for the third axis */
	effectFieldX3p = D32p * lattice[p3 + 2] - D33p * lattice[p3 + 1];
	effectFieldY3p = D33p * lattice[p3] - D31p * lattice[p3 + 2];
	effectFieldZ3p = D31p * lattice[p3 + 1] - D32p * lattice[p3];

	effectFieldX3m = D32m * lattice[m3 + 2] - D33m * lattice[m3 + 1];
	effectFieldY3m = D33m * lattice[m3] - D31m * lattice[m3 + 2];
	effectFieldZ3m = D31m * lattice[m3 + 1] - D32m * lattice[m3];

	/* calculate energy for the third axis */
	latticeParam[indexLattice].EnergyDMIAxis3Norm = -((effectFieldX3p + effectFieldX3m) * lattice[curIndex] +
	                                                  (effectFieldY3p + effectFieldY3m) * lattice[curIndex + 1] +
	                                                  (effectFieldZ3p + effectFieldZ3m) * lattice[curIndex + 2]);

	*EdmiAxis3 += latticeParam[indexLattice].EnergyDMIAxis3Norm;

	/* calculate total effective field */
	*Hx += effectFieldX1p + effectFieldX2p + effectFieldX3p +
	       effectFieldX1m + effectFieldX2m + effectFieldX3m;
	*Hy += effectFieldY1p + effectFieldY2p + effectFieldY3p +
	       effectFieldY1m + effectFieldY2m + effectFieldY3m;
	*Hz += effectFieldZ1p + effectFieldZ2p + effectFieldZ3p +
	       effectFieldZ1m + effectFieldZ2m + effectFieldZ3m;

}

void dmiBulkFieldCubic(int i1, int i2, int i3,
		size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *Edmi) {

	/* number of projections of all spins */
	size_t spinsNum = NUMBER_COMP_MAGN*userVars.N1*userVars.N2*userVars.N3;

	/***************************
    * i-th site
    ***************************/
	/* magnetization */
	double mxi = lattice[curIndex];
	double myi = lattice[curIndex + 1];
	double mzi = lattice[curIndex + 2];
	/* coordinates */
	double xi = lattice[curIndex + spinsNum];
	double yi = lattice[curIndex + spinsNum + 1];
	double zi = lattice[curIndex + spinsNum + 2];

	/***************************
    * 1-st neighbouring site
    ***************************/
	if (i1 < userVars.N1 - 2)  {
		curIndex = IDXcubmg1(i1, i2, i3);
	} else {
		curIndex = IDXmg(1, i2, i3);
	}
	/* magnetization */
	double mx1 = lattice[curIndex];
	double my1 = lattice[curIndex + 1];
	double mz1 = lattice[curIndex + 2];
	/* coordinates */
	double x1 = lattice[curIndex + spinsNum];
	double y1 = lattice[curIndex + spinsNum + 1];
	double z1 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_1 = latticeParam[indexLattice].dmiB1cub;
	if ((i1 == userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiB_1 = 0.0;
		x1 = 0.0;
		y1 = 0.0;
		z1 = 0.0;
	}

	/***************************
	* 2-th neighbouring site
	***************************/
	if (i1 > 1) {
		curIndex = IDXcubmg2(i1, i2, i3);
	} else {
		curIndex = IDXmg(userVars.N1 - 2, i2, i3);
	}
	/* magnetization */
	double mx2 = lattice[curIndex];
	double my2 = lattice[curIndex + 1];
	double mz2 = lattice[curIndex + 2];
	/* coordinates */
	double x2 = lattice[curIndex + spinsNum];
	double y2 = lattice[curIndex + spinsNum + 1];
	double z2 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_2 = latticeParam[indexLattice].dmiB2cub;
	if ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) {
		dmiB_2 = 0.0;
		x2 = 0.0;
		y2 = 0.0;
		z2 = 0.0;
	}

	/***************************
    * 3-rd neighbouring site
    ***************************/
	if (i2 < userVars.N2 - 2)  {
		curIndex = IDXcubmg3(i1, i2, i3);
	} else {
		curIndex = IDXmg(i1, 1, i3);
	}
	/* magnetization */
	double mx3 = lattice[curIndex];
	double my3 = lattice[curIndex + 1];
	double mz3 = lattice[curIndex + 2];
	/* coordinates */
	double x3 = lattice[curIndex + spinsNum];
	double y3 = lattice[curIndex + spinsNum + 1];
	double z3 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_3 = latticeParam[indexLattice].dmiB3cub;
	if ((i2 == userVars.N2 - 2) && (userVars.periodicBC2 == PBC_OFF)) {
		dmiB_3 = 0.0;
		x3 = 0.0;
		y3 = 0.0;
		z3 = 0.0;
	}

	/***************************
    * 4-th neighbouring site
    ***************************/
	if (i2 > 1) {
		curIndex = IDXcubmg4(i1, i2, i3);
	} else {
		curIndex = IDXmg(i1, userVars.N2 - 2, i3);
	}
	/* magnetization */
	double mx4 = lattice[curIndex];
	double my4 = lattice[curIndex + 1];
	double mz4 = lattice[curIndex + 2];
	/* coordinates */
	double x4 = lattice[curIndex + spinsNum];
	double y4 = lattice[curIndex + spinsNum + 1];
	double z4 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_4 = latticeParam[indexLattice].dmiB4cub;
	if ((i2 == 1) && (userVars.periodicBC2 == PBC_OFF)) {
		dmiB_4 = 0.0;
		x4 = 0.0;
		y4 = 0.0;
		z4 = 0.0;
	}

	/***************************
	* 5-th neighbouring site
	***************************/
	if (i3 < userVars.N3 - 2)  {
		curIndex = IDXcubmg5(i1, i2, i3);
	} else {
		curIndex = IDXmg(i1, i2, 1);
	}
	/* magnetization */
	double mx5 = lattice[curIndex];
	double my5 = lattice[curIndex + 1];
	double mz5 = lattice[curIndex + 2];
	/* coordinates */
	double x5 = lattice[curIndex + spinsNum];
	double y5 = lattice[curIndex + spinsNum + 1];
	double z5 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_5 = latticeParam[indexLattice].dmiB5cub;
	if ((i3 == userVars.N3 - 2) && (userVars.periodicBC3 == PBC_OFF)) {
		dmiB_5 = 0.0;
		x5 = 0.0;
		y5 = 0.0;
		z5 = 0.0;
	}

	/***************************
    * 6-th neighbouring site
    ***************************/
	if (i3 > 1) {
		curIndex = IDXcubmg6(i1, i2, i3);
	} else {
		curIndex = IDXmg(i1, i2, userVars.N3 - 2);
	}
	/* magnetization */
	double mx6 = lattice[curIndex];
	double my6 = lattice[curIndex + 1];
	double mz6 = lattice[curIndex + 2];
	/* coordinates */
	double x6 = lattice[curIndex + spinsNum];
	double y6 = lattice[curIndex + spinsNum + 1];
	double z6 = lattice[curIndex + spinsNum + 2];
	/* coefficient of bulk DMI */
	double dmiB_6 = latticeParam[indexLattice].dmiB6cub;
	if ((i3 == 1) && (userVars.periodicBC3 == PBC_OFF)) {
		dmiB_6 = 0.0;
		x6 = 0.0;
		y6 = 0.0;
		z6 = 0.0;
	}

	/* distance r1 - ri*/
	double txi1 = x1 - xi;
	double tyi1 = y1 - yi;
	double tzi1 = z1 - zi;
	/* distance r2 - ri*/
	double txi2 = x2 - xi;
	double tyi2 = y2 - yi;
	double tzi2 = z2 - zi;
	/* distance r3 - ri*/
	double txi3 = x3 - xi;
	double tyi3 = y3 - yi;
	double tzi3 = z3 - zi;
	/* distance r4 - ri*/
	double txi4 = x4 - xi;
	double tyi4 = y4 - yi;
	double tzi4 = z4 - zi;
	/* distance r5 - ri*/
	double txi5 = x5 - xi;
	double tyi5 = y5 - yi;
	double tzi5 = z5 - zi;
	/* distance r6 - ri*/
	double txi6 = x6 - xi;
	double tyi6 = y6 - yi;
	double tzi6 = z6 - zi;

	/* distance |r1 - ri| */
	double ri1 = sqrt( txi1*txi1 + tyi1*tyi1 + tzi1*tzi1 );
	/* distance |r2 - ri| */
	double ri2 = sqrt( txi2*txi2 + tyi2*tyi2 + tzi2*tzi2 );
	/* distance |r3 - ri| */
	double ri3 = sqrt( txi3*txi3 + tyi3*tyi3 + tzi3*tzi3 );
	/* distance |r4 - ri| */
	double ri4 = sqrt( txi4*txi4 + tyi4*tyi4 + tzi4*tzi4 );
	/* distance |r5 - ri| */
	double ri5 = sqrt( txi5*txi5 + tyi5*tyi5 + tzi5*tzi5 );
	/* distance |r6 - ri| */
	double ri6 = sqrt( txi6*txi6 + tyi6*tyi6 + tzi6*tzi6 );

	/* to debug code */
	//printf("%f %f %f %f %f %f\n", ri1, ri2, ri3, ri4, ri5, ri6);
	//usleep(500000);

	/* rescaled dmiB_i */
	dmiB_1 *= 1.0 / ri1;
	dmiB_2 *= 1.0 / ri2;
	dmiB_3 *= 1.0 / ri3;
	dmiB_4 *= 1.0 / ri4;
	dmiB_5 *= 1.0 / ri5;
	dmiB_6 *= 1.0 / ri6;

	/* calculate energy of bulk DMI interaction */
	*Edmi += ( dmiB_1 * ( txi1 * (myi * mz1 - mzi * my1) +
	                      tyi1 * (mzi * mx1 - mxi * mz1) +
	                      tzi1 * (mxi * my1 - myi * mx1) ) +
	           dmiB_2 * ( txi2 * (myi * mz2 - mzi * my2) +
	                      tyi2 * (mzi * mx2 - mxi * mz2) +
	                      tzi2 * (mxi * my2 - myi * mx2) ) +
	           dmiB_3 * ( txi3 * (myi * mz3 - mzi * my3) +
	                      tyi3 * (mzi * mx3 - mxi * mz3) +
	                      tzi3 * (mxi * my3 - myi * mx3) ) +
	           dmiB_4 * ( txi4 * (myi * mz4 - mzi * my4) +
	                      tyi4 * (mzi * mx4 - mxi * mz4) +
	                      tzi4 * (mxi * my4 - myi * mx4) ) +
	           dmiB_5 * ( txi5 * (myi * mz5 - mzi * my5) +
	                      tyi5 * (mzi * mx5 - mxi * mz5) +
	                      tzi5 * (mxi * my5 - myi * mx5) ) +
	           dmiB_6 * ( txi6 * (myi * mz6 - mzi * my6) +
	                      tyi6 * (mzi * mx6 - mxi * mz6) +
	                      tzi6 * (mxi * my6 - myi * mx6) )
	         ) / 2.0;

	/* calculate field of bulk DMI interaction */

	*Hx -= 0.5 * ( dmiB_1 * (tzi1 * my1 - tyi1 * mz1) +
	               dmiB_2 * (tzi2 * my2 - tyi2 * mz2) +
	               dmiB_3 * (tzi3 * my3 - tyi3 * mz3) +
	               dmiB_4 * (tzi4 * my4 - tyi4 * mz4) +
	               dmiB_5 * (tzi5 * my5 - tyi5 * mz5) +
	               dmiB_6 * (tzi6 * my6 - tyi6 * mz6)
	);
	*Hy -= 0.5 * ( dmiB_1 * (txi1 * mz1 - tzi1 * mx1) +
	               dmiB_2 * (txi2 * mz2 - tzi2 * mx2) +
	               dmiB_3 * (txi3 * mz3 - tzi3 * mx3) +
	               dmiB_4 * (txi4 * mz4 - tzi4 * mx4) +
	               dmiB_5 * (txi5 * mz5 - tzi5 * mx5) +
	               dmiB_6 * (txi6 * mz6 - tzi6 * mx6)
	);
	*Hz -= 0.5 * ( dmiB_1 * (tyi1 * mx1 - txi1 * my1) +
	               dmiB_2 * (tyi2 * mx2 - txi2 * my2) +
	               dmiB_3 * (tyi3 * mx3 - txi3 * my3) +
	               dmiB_4 * (tyi4 * mx4 - txi4 * my4) +
	               dmiB_5 * (tyi5 * mx5 - txi5 * my5) +
	               dmiB_6 * (tyi6 * mx6 - txi6 * my6)
	);

}

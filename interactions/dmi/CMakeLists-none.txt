########################################################################
#
# Part of SLaSi project
#
########################################################################

target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/dmi.c)
target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/dmi_triangle.c)
target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/scriptDMI.c)

target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/dmi.c)
target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/dmi_triangle.c)
target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/scriptDMI.c)

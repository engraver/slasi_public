#include <stdio.h>
extern "C" {
	#include "dmi.h"
}

/* DMI field for one site (GPU) */
__global__
void spinDMIField(double *d_H, double *d_lattice, double *d_EnergyTotal1, double *d_EnergyTotal2, double *d_EnergyTotal3, LatticeSiteDescr * d_latticeParam, int dimensionSystem, long N2Ext, long N3Ext, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* check boundary condition */
	if ((indexLattice < numberSites) &&
	    (d_lattice[curIndex] * d_lattice[curIndex] + d_lattice[curIndex + 1] * d_lattice[curIndex + 1] + d_lattice[curIndex + 2] * d_lattice[curIndex + 2] > LENG_ZERO)) {

		/* project vectors of Dzyaloshinsky-Moria interaction on orthonormal basis */
		/* the first axis */
		double D11p, D12p, D13p;
		double D11m, D12m, D13m;
		/* the second axis */
		double D21p, D22p, D23p;
		double D21m, D22m, D23m;
		/* the third axis */
		double D31p, D32p, D33p;
		double D31m, D32m, D33m;

		/* if it is curve, then we will use TNB-orthonormal basis */
		if (dimensionSystem == 1) {

			/* the first axis */
			D11p = d_latticeParam[indexLattice].D11p * d_latticeParam[indexLattice].tx +
			       d_latticeParam[indexLattice].D12p * d_latticeParam[indexLattice].nx +
			       d_latticeParam[indexLattice].D13p * d_latticeParam[indexLattice].bx;

			D12p = d_latticeParam[indexLattice].D11p * d_latticeParam[indexLattice].ty +
			       d_latticeParam[indexLattice].D12p * d_latticeParam[indexLattice].ny +
			       d_latticeParam[indexLattice].D13p * d_latticeParam[indexLattice].by;

			D13p = d_latticeParam[indexLattice].D11p * d_latticeParam[indexLattice].tz +
			       d_latticeParam[indexLattice].D12p * d_latticeParam[indexLattice].nz +
			       d_latticeParam[indexLattice].D13p * d_latticeParam[indexLattice].bz;

			D11m = d_latticeParam[indexLattice].D11m * d_latticeParam[indexLattice].tx +
			       d_latticeParam[indexLattice].D12m * d_latticeParam[indexLattice].nx +
			       d_latticeParam[indexLattice].D13m * d_latticeParam[indexLattice].bx;

			D12m = d_latticeParam[indexLattice].D11m * d_latticeParam[indexLattice].ty +
			       d_latticeParam[indexLattice].D12m * d_latticeParam[indexLattice].ny +
			       d_latticeParam[indexLattice].D13m * d_latticeParam[indexLattice].by;

			D13m = d_latticeParam[indexLattice].D11m * d_latticeParam[indexLattice].tz +
			       d_latticeParam[indexLattice].D12m * d_latticeParam[indexLattice].nz +
			       d_latticeParam[indexLattice].D13m * d_latticeParam[indexLattice].bz;

			/* the second axis */
			D21p = d_latticeParam[indexLattice].D21p * d_latticeParam[indexLattice].tx +
			       d_latticeParam[indexLattice].D22p * d_latticeParam[indexLattice].nx +
			       d_latticeParam[indexLattice].D23p * d_latticeParam[indexLattice].bx;

			D22p = d_latticeParam[indexLattice].D21p * d_latticeParam[indexLattice].ty +
			       d_latticeParam[indexLattice].D22p * d_latticeParam[indexLattice].ny +
			       d_latticeParam[indexLattice].D23p * d_latticeParam[indexLattice].by;

			D23p = d_latticeParam[indexLattice].D21p * d_latticeParam[indexLattice].tz +
			       d_latticeParam[indexLattice].D22p * d_latticeParam[indexLattice].nz +
			       d_latticeParam[indexLattice].D23p * d_latticeParam[indexLattice].bz;

			D21m = d_latticeParam[indexLattice].D21m * d_latticeParam[indexLattice].tx +
			       d_latticeParam[indexLattice].D22m * d_latticeParam[indexLattice].nx +
			       d_latticeParam[indexLattice].D23m * d_latticeParam[indexLattice].bx;

			D22m = d_latticeParam[indexLattice].D21m * d_latticeParam[indexLattice].ty +
			       d_latticeParam[indexLattice].D22m * d_latticeParam[indexLattice].ny +
			       d_latticeParam[indexLattice].D23m * d_latticeParam[indexLattice].by;

			D23m = d_latticeParam[indexLattice].D21m * d_latticeParam[indexLattice].tz +
			       d_latticeParam[indexLattice].D22m * d_latticeParam[indexLattice].nz +
			       d_latticeParam[indexLattice].D23m * d_latticeParam[indexLattice].bz;

			/* the third axis */
			D31p = d_latticeParam[indexLattice].D31p * d_latticeParam[indexLattice].tx +
			       d_latticeParam[indexLattice].D32p * d_latticeParam[indexLattice].nx +
			       d_latticeParam[indexLattice].D33p * d_latticeParam[indexLattice].bx;

			D32p = d_latticeParam[indexLattice].D31p * d_latticeParam[indexLattice].ty +
			       d_latticeParam[indexLattice].D32p * d_latticeParam[indexLattice].ny +
			       d_latticeParam[indexLattice].D33p * d_latticeParam[indexLattice].by;

			D33p = d_latticeParam[indexLattice].D31p * d_latticeParam[indexLattice].tz +
			       d_latticeParam[indexLattice].D32p * d_latticeParam[indexLattice].nz +
			       d_latticeParam[indexLattice].D33p * d_latticeParam[indexLattice].bz;

			D31m = d_latticeParam[indexLattice].D31m * d_latticeParam[indexLattice].tx +
			       d_latticeParam[indexLattice].D32m * d_latticeParam[indexLattice].nx +
			       d_latticeParam[indexLattice].D33m * d_latticeParam[indexLattice].bx;

			D32m = d_latticeParam[indexLattice].D31m * d_latticeParam[indexLattice].ty +
			       d_latticeParam[indexLattice].D32m * d_latticeParam[indexLattice].ny +
			       d_latticeParam[indexLattice].D33m * d_latticeParam[indexLattice].by;

			D33m = d_latticeParam[indexLattice].D31m * d_latticeParam[indexLattice].tz +
			       d_latticeParam[indexLattice].D32m * d_latticeParam[indexLattice].nz +
			       d_latticeParam[indexLattice].D33m * d_latticeParam[indexLattice].bz;

		/* if it is not curve, then we we will use default orthonormal basis */
		} else {

			/* the first axis */
			D11p = d_latticeParam[indexLattice].D11p;
			D12p = d_latticeParam[indexLattice].D12p;
			D13p = d_latticeParam[indexLattice].D13p;

			D11m = d_latticeParam[indexLattice].D11m;
			D12m = d_latticeParam[indexLattice].D12m;
			D13m = d_latticeParam[indexLattice].D13m;

			/* the second axis */
			D21p = d_latticeParam[indexLattice].D21p;
			D22p = d_latticeParam[indexLattice].D22p;
			D23p = d_latticeParam[indexLattice].D23p;

			D21m = d_latticeParam[indexLattice].D21m;
			D22m = d_latticeParam[indexLattice].D22m;
			D23m = d_latticeParam[indexLattice].D23m;

			/* the third axis */
			D31p = d_latticeParam[indexLattice].D31p;
			D32p = d_latticeParam[indexLattice].D32p;
			D33p = d_latticeParam[indexLattice].D33p;

			D31m = d_latticeParam[indexLattice].D31m;
			D32m = d_latticeParam[indexLattice].D32m;
			D33m = d_latticeParam[indexLattice].D33m;

		}

		/* neighbor's indexes */
		size_t p1, p2, p3, m1, m2, m3;

		/* calculate neighbor's indices */
		p1 = curIndex + 3 * N3Ext * N2Ext;
		p2 = curIndex + 3 * N3Ext;
		p3 = curIndex + 3;
		m1 = curIndex - 3 * N3Ext * N2Ext;
		m2 = curIndex - 3 * N3Ext;
		m3 = curIndex - 3;

		/* declarations of effective fields */
		/* the first axis */
		double effectFieldX1p, effectFieldY1p, effectFieldZ1p;
		double effectFieldX1m, effectFieldY1m, effectFieldZ1m;
		/* the second axis */
		double effectFieldX2p, effectFieldY2p, effectFieldZ2p;
		double effectFieldX2m, effectFieldY2m, effectFieldZ2m;
		/* the third axis */
		double effectFieldX3p, effectFieldY3p, effectFieldZ3p;
		double effectFieldX3m, effectFieldY3m, effectFieldZ3m;

		/* calculate effective field for the first axis */
		effectFieldX1p = D12p * d_lattice[p1 + 2] - D13p * d_lattice[p1 + 1];
		effectFieldY1p = D13p * d_lattice[p1] - D11p * d_lattice[p1 + 2];
		effectFieldZ1p = D11p * d_lattice[p1 + 1] - D12p * d_lattice[p1];

		effectFieldX1m = D12m * d_lattice[m1 + 2] - D13m * d_lattice[m1 + 1];
		effectFieldY1m = D13m * d_lattice[m1] - D11m * d_lattice[m1 + 2];
		effectFieldZ1m = D11m * d_lattice[m1 + 1] - D12m * d_lattice[m1];

		/* calculate energy for the first axis */
		d_EnergyTotal1[indexLattice] =
				-((effectFieldX1p + effectFieldX1m) * d_lattice[curIndex] +
				  (effectFieldY1p + effectFieldY1m) * d_lattice[curIndex + 1] +
				  (effectFieldZ1p + effectFieldZ1m) * d_lattice[curIndex + 2]);

		/* calculate effective field for the second axis */
		effectFieldX2p = D22p * d_lattice[p2 + 2] - D23p * d_lattice[p2 + 1];
		effectFieldY2p = D23p * d_lattice[p2] - D21p * d_lattice[p2 + 2];
		effectFieldZ2p = D21p * d_lattice[p2 + 1] - D22p * d_lattice[p2];

		effectFieldX2m = D22m * d_lattice[m2 + 2] - D23m * d_lattice[m2 + 1];
		effectFieldY2m = D23m * d_lattice[m2] - D21m * d_lattice[m2 + 2];
		effectFieldZ2m = D21m * d_lattice[m2 + 1] - D22m * d_lattice[m2];

		/* calculate energy for the second axis */
		d_EnergyTotal2[indexLattice] =
				-((effectFieldX2p + effectFieldX2m) * d_lattice[curIndex] +
				  (effectFieldY2p + effectFieldY2m) * d_lattice[curIndex + 1] +
				  (effectFieldZ2p + effectFieldZ2m) * d_lattice[curIndex + 2]);

		/* calculate effective field for the third axis */
		effectFieldX3p = D32p * d_lattice[p3 + 2] - D33p * d_lattice[p3 + 1];
		effectFieldY3p = D33p * d_lattice[p3] - D31p * d_lattice[p3 + 2];
		effectFieldZ3p = D31p * d_lattice[p3 + 1] - D32p * d_lattice[p3];

		effectFieldX3m = D32m * d_lattice[m3 + 2] - D33m * d_lattice[m3 + 1];
		effectFieldY3m = D33m * d_lattice[m3] - D31m * d_lattice[m3 + 2];
		effectFieldZ3m = D31m * d_lattice[m3 + 1] - D32m * d_lattice[m3];

		/* calculate energy for the third axis */
		d_EnergyTotal3[indexLattice] =
				-((effectFieldX3p + effectFieldX3m) * d_lattice[curIndex] +
				  (effectFieldY3p + effectFieldY3m) * d_lattice[curIndex + 1] +
				  (effectFieldZ3p + effectFieldZ3m) * d_lattice[curIndex + 2]);

		/* calculate total effective field */
		d_H[curIndex] += effectFieldX1p + effectFieldX2p + effectFieldX3p + effectFieldX1m +
		       effectFieldX2m + effectFieldX3m;
		d_H[curIndex + 1] += effectFieldY1p + effectFieldY2p + effectFieldY3p + effectFieldY1m +
		       effectFieldY2m + effectFieldY3m;
		d_H[curIndex + 2] += effectFieldZ1p + effectFieldZ2p + effectFieldZ3p + effectFieldZ1m +
		       effectFieldZ2m + effectFieldZ3m;

	}

}

extern "C"
void dmiFieldCUDA(double *d_H, double *d_EnergyTotal1, double *d_Energy_Total2, double *d_Energy_Total3, long numberSites) {

	/* setup the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* wrapper of CUDA function */
	spinDMIField<<<cores,threads>>>(d_H, d_lattice, d_EnergyTotal1, d_Energy_Total2, d_Energy_Total3, d_latticeParam, userVars.dimensionOfSystem, userVars.N2, userVars.N3, numberSites);

}

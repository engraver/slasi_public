#ifndef _DMI_H_
#define _DMI_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../../alltypes.h"
#include "../interactions.h"

/**
 * @brief setupDMI
 * @param numbers array with values from paramFile
 * @param indlp index for latticeParam array
 * @param indn index for numbers array
 * @return 0 if ok, otherwise nonzero value
 */
int setupDMI(double **numbers, int indlp, int indn);

/**
 * @brief setupDMITriangleBulk
 * @param numbers array with values from paramFile
 * @param indlp index for latticeParam array
 * @param indn index for numbers array
 * @return 0 if ok, otherwise nonzero value
 */
int setupDMITriangleBulk(double **numbers, int indlp, int indn);

/**
 * @brief setupDMITriangleInterface
 * @param numbers array with values from paramFile
 * @param indlp index for latticeParam array
 * @param indn index for numbers array
 * @return 0 if ok, otherwise nonzero value
 */
int setupDMITriangleInterface(double **numbers, int indlp, int indn);

/**
 * @brief This function is to setup coefficients of bulk DMI interaction for cubic lattice
 * @param numbers array with values from paramFile
 * @param indlp index for latticeParam array
 * @param indn index for numbers array
 * @return 0 if ok, otherwise nonzero value
 */
int setupDMICubicBulk(double **numbers, int indlp, int indn);

/**
 * @brief This function returns effective field and energy
 * which created by Dzyaloshinsky-Moria interaction of surrounding spins
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Hx effective field on axis x
 * @param Hy effective field on axis y
 * @param Hz effective field on axis z
 * @param EnergyDMI normalized energy of Dzyaloshinsky-Moria interaction
 */
void dmiField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EdmiAxis1, double *EdmiAxis2, double *EdmiAxis3);

/**
 * @brief This function returns effective field and energy
 * which created by bulk Dzyaloshinsky-Moria interaction of surrounding spins in square lattice
 * @param i1,i2,i3 indexes of site
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Hx effective field on axis x
 * @param Hy effective field on axis y
 * @param Hz effective field on axis z
 * @param Edmi normalized energy of Dzyaloshinsky-Moria interaction
 */
void dmiBulkFieldCubic(int i1, int i2, int i3, size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *Edmi);

/**
 * @brief This function returns effective field and energy
 * which created by bulk Dzyaloshinsky-Moria interaction of surrounding spins for triangular lattice
 * @param i1,i2 indexes of node
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Hx effective field on axis x
 * @param Hy effective field on axis y
 * @param Hz effective field on axis z
 * @param Edmi normalized energy of Dzyaloshinsky-Moria interaction
 */
void dmiBulkFieldTriangle(int i1, int i2, size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *Edmi);

/**
 * @brief This function returns force
 * which created by bulk Dzyaloshinsky-Moria interaction of surrounding spins for triangular lattice
 * @param i1,i2 indexes of node
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Fx force on axis x
 * @param Fy force on axis y
 * @param Fz force on axis z
 */
void dmiBulkForceTriangle(int i1, int i2, size_t curIndex, size_t indexLattice, double *Fx, double *Fy, double *Fz);

/**
 * @brief This function returns effective field and energy
 * which created by interface Dzyaloshinsky-Moria interaction of surrounding spins for triangular lattice
 * @param i1,i2 indexes of node
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Hx effective field on axis x
 * @param Hy effective field on axis y
 * @param Hz effective field on axis z
 * @param Edmi normalized energy of Dzyaloshinsky-Moria interaction
 */
void dmiInterfaceFieldTriangle(int i1, int i2, size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *Edmi);

/**
 * @brief This function returns force
 * which created by interface Dzyaloshinsky-Moria interaction of surrounding spins for triangular lattice
 * @param i1,i2 indexes of node
 * @param Fx force on axis x
 * @param Fy force on axis y
 * @param Fz force on axis z
 */
void dmiInterfaceForceTriangle(int i1, int i2, double *Fx, double *Fy, double *Fz);

/**
 * @brief This function of wrapper returns effective field and energy
 * which created by DMI of surrounding spins for cubic lattice (GPU)
 * @param d_H is array of effective field (GPU)
 * @param d_EnergyTotal1 is array of DMI energy for sites along the first axis (GPU)
 * @param d_EnergyTotal2 is array of DMI energy for sites along the second axis (GPU)
 * @param d_EnergyTotal3 is array of DMI energy for sites along the third axis (GPU)
 * @param numberSites is number of sites
 */
void dmiFieldCUDA(double *d_H, double *d_EnergyTotal1, double *d_EnergyTotal2, double *d_EnergyTotal3, long numberSites);


#endif

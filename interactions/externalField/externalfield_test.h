#ifndef _EXTERNALFIELD_TEST_H
#define _EXTERNALFIELD_TEST_H

int init_externalfield_test(void);
int clean_externalfield_test(void);

/* uniform exchange energy and external field for chain (energy) */
char * test_externalfield_energy_1_desc = "UniX test: 2-spin chain energy (exchange interaction and constant external field)\n";
void test_externalfield_energy_1(void);

/* uniform exchange energy and external field for chain (energy) */
char * test_externalfield_energy_2_desc = "UniX test: 4-spin plain energy (exchange interaction and constant external field)\n";
void test_externalfield_energy_2(void);

/* uniform exchange energy and external field for cube (energy) */
char * test_externalfield_energy_3_desc = "UniX test: 8-spin cube energy (exchange interaction and constant external field)\n";
void test_externalfield_energy_3(void);

/* uniform exchange energy and external field for plain with hole (energy) */
char * test_externalfield_energy_4_desc = "UniX test: 8-spin plain energy with hole (exchange interaction and constant external field)\n";
void test_externalfield_energy_4(void);

/* uniform exchange energy and external field parallelepiped with hole (energy) */
char * test_externalfield_energy_5_desc = "UniX test: 16-spin parallelepiped energy with hole (exchange interaction and constant external field)\n";
void test_externalfield_energy_5(void);

#endif
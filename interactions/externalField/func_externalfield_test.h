#ifndef _FUNC_EXTERNALFIELD_TEST_H
#define _FUNC_EXTERNALFIELD_TEST_H

int init_func_externalfield_test(void);
int clean_func_externalfield_test(void);

/* uniform exchange energy and external field for chain (simulation) */
char * test_externalfield_simulation_1_desc = "UniX test: 2-spin chain simulation (exchange interaction and constant external field)\n";
void test_externalfield_simulation_1(void);

/* uniform exchange energy and external field anisotropy for plain (simulation) */
char * test_externalfield_simulation_2_desc = "UniX test: 4-spin plain simulation (exchange interaction and constant external field)\n";
void test_externalfield_simulation_2(void);

/* uniform exchange energy and external field for cube (simulation) */
char * test_externalfield_simulation_3_desc = "UniX test: 8-spin cube simulation (exchange interaction and constant external field)\n";
void test_externalfield_simulation_3(void);

/* uniform exchange energy and external field for cube with random magnetic moments (simulation) */
char * test_externalfield_simulation_4_desc = "UniX test: 8-spin cube simulation with random magnetic moments (exchange interaction and constant external field)\n";
void test_externalfield_simulation_4(void);

#endif

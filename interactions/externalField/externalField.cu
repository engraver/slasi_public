#include "../../alltypes.h"
extern "C"{
	#include "externalField.h"
}

/* interaction of one spin with external uniform field for system (GPU) */
__global__
void spinExternalUniformField(double *d_H, double *d_lattice, double Bx, double By, double Bz, double *d_EnergyTotal, double kExtField, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;
	/* local effective field */
	double HxLocal, HyLocal, HzLocal;

	/* check boundary condition for threads */
	if ((indexLattice < numberSites) &&
	    (d_lattice[curIndex] * d_lattice[curIndex] + d_lattice[curIndex + 1] * d_lattice[curIndex + 1] + d_lattice[curIndex + 2] * d_lattice[curIndex + 2] > LENG_ZERO)) {

		/* calculate effective field */
		HxLocal = kExtField * Bx;
		HyLocal = kExtField * By;
		HzLocal = kExtField * Bz;

		/* calculate energy */
		d_EnergyTotal[indexLattice] += (-1.0) *
									   (HxLocal * d_lattice[curIndex] + HyLocal * d_lattice[curIndex + 1] + HzLocal * d_lattice[curIndex + 2]);

		/* add partial effective field to total field */
		d_H[curIndex] += HxLocal;
		d_H[curIndex + 1] += HyLocal;
		d_H[curIndex + 2] += HzLocal;

	}

}

extern "C"
void externalUniformFieldCUDA(double *d_H, double Bx, double By, double Bz, double *d_EnergyTotal, double kExtField, long numberSites) {

	/* setup the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* wrapper of CUDA function */
	spinExternalUniformField<<<cores, threads>>>(d_H, d_lattice, Bx, By, Bz, d_EnergyTotal, kExtField, numberSites);

}

/* interaction of one spin with external general field for system (GPU) */
__global__
void spinExternalGeneralField(double *d_H, double *d_lattice, double *d_Bamp, double *d_EnergyTotal, LatticeSiteDescr *d_latticeParam, double kExtField, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;
	/* local effective field */
	double HxLocal, HyLocal, HzLocal;

	/* check boundary condition for threads */
	if (indexLattice < numberSites) {

		/* calculate effective field */
		HxLocal = kExtField * d_Bamp[curIndex];
		HyLocal = kExtField * d_Bamp[curIndex + 1];
		HzLocal = kExtField * d_Bamp[curIndex + 2];

		/* calculate energy */
		d_EnergyTotal[indexLattice] += (-1.0) *
		                               (HxLocal * d_lattice[curIndex] + HyLocal * d_lattice[curIndex + 1] + HzLocal * d_lattice[curIndex + 2]);

		/* add partial effective field to total field */
		d_H[curIndex] += HxLocal;
		d_H[curIndex + 1] += HyLocal;
		d_H[curIndex + 2] += HzLocal;

	}

}

extern "C"
void externalGeneralFieldCUDA(double *d_H, double *d_EnergyTotal, LatticeSiteDescr *d_latticeParam, double kExtField, long numberSites) {

	/* setup the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* wrapper of CUDA function */
	spinExternalGeneralField<<<cores, threads>>>(d_H, d_lattice, d_Bamp, d_EnergyTotal, d_latticeParam, kExtField, numberSites);

}

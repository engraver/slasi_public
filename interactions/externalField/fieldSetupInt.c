/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "fieldSetup.h"
#include "../../utils/parallel_utils.h"

double getSincInt(double t, size_t indexLattice) {

	/* calculate argument in sinc() */
	double time_dep = 0.0;
	double tt = (t - latticeParam[indexLattice].fieldSincTShift)/latticeParam[indexLattice].fieldSincTWidth;

	/* calculate sinc() */
	if (fabs(tt) < NEAR_ZERO) {
		time_dep = 1.0;
	}
	else {
		time_dep = sin(tt) / tt;
	}

	/* return value */
	return time_dep;

}

int harmTimeInt(double t) {

	/* check variables for harmonic field (if all parameters are in .dat file ) */
	if ((userVars.iBxamp != -1) && (userVars.iByamp != -1) && (userVars.iBzamp != -1) &&
	    (userVars.iBphase != -1) && (userVars.iBfreq != -1)) {

		/* pass through each magnetic moment to setup harmonic external field */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						int curIndex = IDXmg(i1, i2, i3);
						/* check if node is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							/* change external field using harmonic law */
							Bamp[curIndex] = BampSaved[curIndex] * sin(latticeParam[indexLattice].Bfreq * t +
							                                       latticeParam[indexLattice].Bphase);
							Bamp[curIndex + 1] = BampSaved[curIndex + 1] * sin(latticeParam[indexLattice].Bfreq * t +
							                                       latticeParam[indexLattice].Bphase);
							Bamp[curIndex + 2] = BampSaved[curIndex + 2] * sin(latticeParam[indexLattice].Bfreq * t +
							                                       latticeParam[indexLattice].Bphase);
						}
					}
				}
			}
		}

		/* check variables for harmonic field (if all parameters are in .cfg file ) */
	} else if ((userVars.bBxamp) && (userVars.bByamp) && (userVars.bBzamp) &&
	           (userVars.bBphase) && (userVars.bBfreq)) {

		/* pass through each magnetic moment */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						int curIndex = IDXmg(i1, i2, i3);
						/* check if node is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							/* change external field using harmonic law */
							Bamp[curIndex] =
									userVars.Bxamp * sin(userVars.Bfreq * t + userVars.Bphase);
							Bamp[curIndex + 1] =
									userVars.Byamp * sin(userVars.Bfreq * t + userVars.Bphase);
							Bamp[curIndex + 2] =
									userVars.Bzamp * sin(userVars.Bfreq * t + userVars.Bphase);
						}
					}
				}
			}
		}

	} else {
		/* return error */
		messageToStream("Lack of parameters for external harmonic field!\n", stderr);
		return 1;
	}

	return 0;

}

int sincTimeInt(double t) {

	/* check variables for harmonic field (if all parameters are in .dat file ) */
	if ((userVars.iBxamp != -1) && (userVars.iByamp != -1) && (userVars.iBzamp != -1) &&
	    (userVars.ifieldSincTShift != -1) && (userVars.ifieldSincTShift != -1)) {

		/* go by each magnetic moment to setup sinc external field */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						int curIndex = IDXmg(i1, i2, i3);
						/* check if node is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							double time_dep = getSincInt(t, indexLattice);
							/* change external field using sinc law */
							Bamp[curIndex] = BampSaved[curIndex] * time_dep;
							Bamp[curIndex + 1] = BampSaved[curIndex + 1] * time_dep;
							Bamp[curIndex + 2] = BampSaved[curIndex + 2] * time_dep;
						}
					}
				}
			}
		}

		/* check variables for harmonic field (if all parameters are in .cfg file ) */
	} else if ((userVars.bBxamp) && (userVars.bByamp) && (userVars.bBzamp) &&
	           (userVars.bfieldSincTShift) && (userVars.bfieldSincTWidth)) {

		/* go by each magnetic moment */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						int curIndex = IDXmg(i1, i2, i3);
						/* check if node is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							double time_dep = getSincInt(t, indexLattice);
							/* change external field using sinc */
							Bamp[curIndex] = userVars.Bxamp * time_dep;
							Bamp[curIndex + 1] = userVars.Byamp * time_dep;
							Bamp[curIndex + 2] = userVars.Bzamp * time_dep;
						}
					}
				}
			}
		}

	} else {
		/* return error */
		messageToStream("Lack of parameters for external sinc field!\n", stderr);
		return 1;
	}

	return 0;

}

int constTimeInt(void) {

	/* check variables for constant field */
	if ((userVars.bBxamp) && (userVars.bByamp) && (userVars.bBzamp)) {

		/* set constant external field */
		userVars.Bx = userVars.Bxamp;
		userVars.By = userVars.Byamp;
		userVars.Bz = userVars.Bzamp;

	} else {
		/* return error */
		messageToStream("Lack of parameters for external constant field!\n", stderr);
		return 1;
	}

	return 0;

}

int scriptTimeInt(double t) {

	/* declarations of usual variables */
	double Bxamp, Byamp, Bzamp;
	int tester;

	/* pass through each magnetic moment */
	for (int i1 = 0; i1 < userVars.N1; i1++) {
		for (int i2 = 0; i2 < userVars.N2; i2++) {
			for (int i3 = 0; i3 < userVars.N3; i3++) {
				if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
				    i3 < userVars.N3 - 1) {

					/* number of spin in lattice */
					int indexLattice = IDX(i1, i2, i3);
					int curIndex = IDXmg(i1, i2, i3);

					/* check if site is magnetic */
					if (latticeParam[indexLattice].magnetic) {

						/* time of simulation */
						pValueTime = PyFloat_FromDouble(t);
						/* check value */
						if (!pValueTime) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 0, pValueTime);

						/* the first coordinate */
						pValueCoor1 = PyFloat_FromDouble(*(latticeParam[indexLattice].x) * userVars.a);
						/* check value */
						if (!pValueCoor1) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 1, pValueCoor1);

						/* the second coordinate */
						pValueCoor2 = PyFloat_FromDouble(*(latticeParam[indexLattice].y) * userVars.a);
						/* check value */
						if (!pValueCoor2) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 2, pValueCoor2);

						/* the third coordinate */
						pValueCoor3 = PyFloat_FromDouble(*(latticeParam[indexLattice].z) * userVars.a);
						/* check value */
						if (!pValueCoor3) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 3, pValueCoor3);

						/* the first index */
						pValueInd1 = PyLong_FromLong(i1);
						/* check value */
						if (!pValueInd1) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 4, pValueInd1);

						/* the second index */
						pValueInd2 = PyLong_FromLong(i2);
						/* check value */
						if (!pValueInd2) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 5, pValueInd2);

						/* the third index */
						pValueInd3 = PyLong_FromLong(i3);
						/* check value */
						if (!pValueInd3) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 6, pValueInd3);

						/* call function */
						pValueRes = PyObject_CallObject(pFieldFunc, pFieldArgs);
						/* check results of calling */
						if (pValueRes != NULL) {
							/* get results of calling */
							tester = PyArg_ParseTuple(pValueRes, "ddd", &Bxamp, &Byamp, &Bzamp);
							/* check if it was able to get */
							if (tester) {
								/* save results */
								Bamp[curIndex] = Bxamp;
								Bamp[curIndex + 1] = Byamp;
								Bamp[curIndex + 2] = Bzamp;
							} else {
								/* output error */
								fprintf(stderr, "Call of python function getExtField failed!\n");
							}
							Py_DECREF(pValueRes);
						}

					}

				}
			}
		}
	}

	/* return default value */
	return 0;

}

int fieldSetupInt(double t) {

	/* check process of execution */
	if (!doItInMasterProcess()) {
		return 0;
	}

	/* select function to setup */
	switch (userVars.extField) {
			/* harmonic law */
		case EXTFIELD_HARMTIME:
			return harmTimeInt(t);
			/* constant field */
		case EXTFIELD_CONST:
			return constTimeInt();
			/* law B(t) = B * sinc(a*(t-t0)) */
		case EXTFIELD_SINC:
			return sincTimeInt(t);
			/* via Python script */
		case EXTFIELD_SCRIPT:
			return scriptTimeInt(t);
			/* return error */
		default:
			messageToStream("Harmonic or sinc mode are necessary to setup external field constantly!\n", stderr);
			return 1;
	}

}


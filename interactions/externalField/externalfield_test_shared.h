#ifndef _EXTERNALFIELD_TEST_SHARED_H
#define _EXTERNALFIELD_TEST_SHARED_H

/* function to create files to test magnetic simulator (for magnetic interaction) */
void createMagnFiles(char * nameConfFile, char * nameParamFile, char * rowConfFile, char * rowParamFile);


/* function to launch simulation (for magnetic interaction) */
void launchMagnSimulation(char * nameConfFile, char * nameParamFile);

/* function to delete two files (for magnetic interaction) */
void deleteMagnFiles(char * nameConfFile, char * nameParamFile, char * baseName);

#endif

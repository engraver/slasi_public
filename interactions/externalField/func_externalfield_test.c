#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "CUnit/Basic.h"

#include "../../alltypes.h"
#include "../../simulation.h"
#include "externalfield_test_shared.h"

/* names of configuration files for testing */
#define FILE_EXTR_CFG1 "testextr1.cfg"
#define FILE_EXTR_DAT1 "testextr1.dat"
#define FILE_EXTR_BAS1 "testextr1"

#define FILE_EXTR_CFG2 "testextr2.cfg"
#define FILE_EXTR_DAT2 "testextr2.dat"
#define FILE_EXTR_BAS2 "testextr2"

#define FILE_EXTR_CFG3 "testextr3.cfg"
#define FILE_EXTR_DAT3 "testextr3.dat"
#define FILE_EXTR_BAS3 "testextr3"

#define FILE_EXTR_CFG4 "testextr4.cfg"
#define FILE_EXTR_DAT4 "testextr4.dat"
#define FILE_EXTR_BAS4 "testextr4"

#define PREC_FUNC_TEST_EXTR 0.000001

/* initialization function for tests */
int init_func_externalfield_test(void) {
	return 0;
}

/* function of cleaning for tests */
int clean_func_externalfield_test(void) {
	return 0;
}

/* function to create files to test magnetic simulator (for magnetic interaction) */
void createFuncMagnFiles(char * nameConfFile, char * nameParamFile, char * rowConfFile, char * rowParamFile) {

	/* create configuration file */
	FILE * fid1 = fopen(nameConfFile, "w");
	fprintf(fid1, "%s", rowConfFile);
	fclose(fid1);

	/* create parameter file */
	FILE * fid2 = fopen(nameParamFile, "w");
	fprintf(fid2, "%s", rowParamFile);
	fclose(fid2);

}

/* function to launch simulation (for magnetic interaction) */
void launchFuncMagnSimulation(char * nameConfFile, char * nameParamFile) {

	/* make parameters for launching */
	int argc = 4;
	char **argv;
	argv = (char**) malloc(sizeof(char**) * 4);
	argv[0] = (char*) malloc(sizeof("slasi"));
	argv[0] = "slasi";
	argv[1] = (char*) malloc(sizeof(nameConfFile));
	argv[1] = nameConfFile;
	argv[2] = (char*) malloc(sizeof("-p"));
	argv[2] = "-p";
	argv[3] = (char*) malloc(sizeof(nameParamFile));
	argv[3] = nameParamFile;

	/* launch simulation */
	slasi(argc, argv, false);

}

/* function to check magnetic moments of system */
void checkExtMagnetism(int N1, int N2, int N3, double mx, double my, double mz) {

	/* go through all sites */
	for (int i1 = 1; i1 <= N1; i1++) {
		for (int i2 = 1; i2 <= N2; i2++) {
			for (int i3 = 1; i3 <= N3; i3++) {

				/* check site */
				int indexMagn = IDXmg(i1, i2, i3);
				CU_ASSERT(fabs(lattice[indexMagn] - mx) < PREC_FUNC_TEST_EXTR);
				CU_ASSERT(fabs(lattice[indexMagn + 1] - my) < PREC_FUNC_TEST_EXTR);
				CU_ASSERT(fabs(lattice[indexMagn + 2] - mz) < PREC_FUNC_TEST_EXTR);

			}
		}
	}

}

/* function to delete two files (for magnetic interaction) */
void deleteFuncMagnFiles(char * nameConfFile, char * nameParamFile, char * baseName) {

	/* delete parameter and configuration files */
	remove(nameConfFile);
	remove(nameParamFile);

	/* delete energy logfile which are created during simulation */
	char energy_log [100];
	strcpy(energy_log, baseName);
	strcat(energy_log, "_energy.log");
	remove(energy_log);

	/* delete logfile of initialization which are created during simulation */
	char init_log [100];
	strcpy(init_log, baseName);
	strcat(init_log, "_init.log");
	remove(init_log);

	/* delete logfile of magnetism which are created during simulation */
	char mag_log [100];
	strcpy(mag_log, baseName);
	strcat(mag_log, "_mag.log");
	remove(mag_log);

	/* delete logfile for other parameters which are created during simulation */
	char other_log [100];
	strcpy(other_log, baseName);
	strcat(other_log, "_other.log");
	remove(other_log);

	/* delete file for table of parameters */
	char table_file [100];
	strcpy(table_file, baseName);
	strcat(table_file, ".tbl");
	remove(table_file);

	/* delete the first file of intermediate results */
	char file_inter1 [100];
	strcpy(file_inter1, baseName);
	strcat(file_inter1, ".00000.slsb");
	remove(file_inter1);

	/* delete the second file of intermediate results */
	char file_inter2 [100];
	strcpy(file_inter2, baseName);
	strcat(file_inter2, ".00001.slsb");
	remove(file_inter2);

}

/*
 * @brief yet another test of exchange interaction and external field
 *
 * chain of two magnetic moments with exchange interaction and external field
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3
 * 000 000 000 000
 * 000 0+0 0+0 000
 * 000 000 000 000
 * @endverbatim
 * Here, + magnetic sites with initial values (0.5,0.5,0.707) and (0.707,0.5,0.5).
 * The magnetic system has exchange interaction and constant external magnetic field (0,2.0,0).
 * The magnitude of the magnetic field is Tesla.
 * So, all magnetic moments have to be parallel and directed along axis of external field.
 */
void test_externalfield_simulation_1(void) {

	/* number of spins along axis */
	int N1 = 2;
	int N2 = 1;
	int N3 = 1;

	/* correct values of components of magnetic moments */
	double mx = 0.0;
	double my = 1.0;
	double mz = 0.0;

	/* create files to test magnetic simulator */
	createMagnFiles(FILE_EXTR_CFG1, FILE_EXTR_DAT1, "N1 = 2\nN2 = 1\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 100\nframe = 100\nextField = const\nBxamp = 0\nByamp = 2.0\nBzamp = 0\noutput = SLSB\n", "mx my mz x y z\n0.5 0.5 0.707 0 0 0\n0.707 0.5 0.5 1 0 0\n");

	/* launch simulation */
	launchMagnSimulation(FILE_EXTR_CFG1, FILE_EXTR_DAT1);

	/* check sites */
	checkExtMagnetism(N1, N2, N3, mx, my, mz);

	/* delete created files */
	deleteMagnFiles(FILE_EXTR_CFG1, FILE_EXTR_DAT1, FILE_EXTR_BAS1);

}

/*
 * @brief yet another test of exchange interaction and external field
 *
 * plain of four magnetic moments with exchange interaction and external field
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3
 * 000 000 000 000
 * 000 0+0 0+0 000
 * 000 0+0 0+0 000
 * 000 000 000 000
 * @endverbatim
 * Here, + magnetic sites with initial values (0.5,0.5,0.707), (0.707,0.5,0.5), (1.0,0.0,0.0) and (0.0,0.0,1.0).
 * The magnetic system has exchange interaction and constant external magnetic field (1.41,1.41,0).
 * The magnitude of the magnetic field is Tesla.
 * So, all magnetic moments have to be parallel and directed along axis of external field.
 */
void test_externalfield_simulation_2(void) {

	/* number of spins along axis */
	int N1 = 2;
	int N2 = 2;
	int N3 = 1;

	/* correct values of components of magnetic moments */
	double mx = 0.707107;
	double my = 0.707107;
	double mz = 0.0;

	/* create files to test magnetic simulator */
	createMagnFiles(FILE_EXTR_CFG2, FILE_EXTR_DAT2, "N1 = 2\nN2 = 2\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 200\nframe = 200\nextField = const\nBxamp = 1.41\nByamp = 1.41\nBzamp = 0\noutput = SLSB\n", "mx my mz x y z\n0.5 0.5 0.707 0 0 0\n0.707 0.5 0.5 1 0 0\n1.0 0.0 0.0 0 1 0\n0.0 0.0 1.0 1 1 0\n");

	/* launch simulation */
	launchMagnSimulation(FILE_EXTR_CFG2, FILE_EXTR_DAT2);

	/* check sites */
	checkExtMagnetism(N1, N2, N3, mx, my, mz);

	/* delete created files */
	deleteMagnFiles(FILE_EXTR_CFG2, FILE_EXTR_DAT2, FILE_EXTR_BAS2);

}

/*
 * @brief yet another test of exchange interaction and external field
 *
 * cube of eight magnetic moments with exchange interaction and external field
 *
 * @verbatim
 * Location of magnetic moment components in an array
 * 0    1    2    3
 * 0000 0000 0000 0000
 * 0000 0++0 0++0 0000
 * 0000 0++0 0++0 0000
 * 0000 0000 0000 0000
 * @endverbatim
 * Here, + magnetic site with initial value (0.5,0.5,0.707), (0.5,0.707,0.5), (1.0,0.0,0.0), (0.0,1.0,0.0),
 * (0.0,0.0,1.0), (0.707,0.707,0.0), (0.707,0.0,0.707) and (0.0,0.707,0.707).
 * The magnetic system has exchange interaction and constant external magnetic field (1.41,0.707,0.707).
 * The magnitude of the magnetic field is Tesla.
 * So, all magnetic moments have to be parallel and directed along axis of external field.
 */
void test_externalfield_simulation_3(void) {

	/* number of spins along axis */
	int N1 = 2;
	int N2 = 2;
	int N3 = 2;

	/* correct values of components of magnetic moments */
	double mx = 0.815724;
	double my = 0.409019;
	double mz = 0.409019;

	/* create files to test magnetic simulator */
	createMagnFiles(FILE_EXTR_CFG3, FILE_EXTR_DAT3, "N1 = 2\nN2 = 2\nN3 = 2\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 400\nframe = 400\nextField = const\nBxamp = 1.41\nByamp = 0.707\nBzamp = 0.707\noutput = SLSB\n", "mx my mz x y z\n0.5 0.5 0.707 0 0 0\n0.5 0.707 0.5 1 0 0\n1.0 0.0 0.0 0 1 0\n0.0 1.0 0.0 1 1 0\n0.0 0.0 1.0 0 0 1\n0.707 0.707 0.0 1 0 1\n0.707 0.0 0.707 0 1 1\n0.0 0.707 0.707 1 1 1\n");

	/* launch simulation */
	launchMagnSimulation(FILE_EXTR_CFG3, FILE_EXTR_DAT3);

	/* check sites */
	checkExtMagnetism(N1, N2, N3, mx, my, mz);

	/* delete created files */
	deleteMagnFiles(FILE_EXTR_CFG3, FILE_EXTR_DAT3, FILE_EXTR_BAS3);

}

/*
 * @brief yet another test of exchange interaction and external field
 *
 * cube of eight magnetic moments with exchange interaction and external field
 *
 * @verbatim
 * Location of magnetic moment components in an array
 * 0    1    2    3
 * 0000 0000 0000 0000
 * 0000 0++0 0++0 0000
 * 0000 0++0 0++0 0000
 * 0000 0000 0000 0000
 * @endverbatim
 * Here, + magnetic site with random initial value.
 * The magnetic system has exchange interaction and constant external magnetic field (0.707,0.707,1.41).
 * The magnitude of the magnetic field is Tesla.
 * So, all magnetic moments have to be parallel and directed along axis of external field.
 */
void test_externalfield_simulation_4(void) {

	/* number of spins along axis */
	int N1 = 2;
	int N2 = 2;
	int N3 = 2;

	/* correct values of components of magnetic moments */
	double mx = 0.409019;
	double my = 0.409019;
	double mz = 0.815724;

	/* create files to test magnetic simulator */
	createMagnFiles(FILE_EXTR_CFG4, FILE_EXTR_DAT4, "N1 = 2\nN2 = 2\nN3 = 2\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 400\nframe = 400\nextField = const\nBxamp = 0.707\nByamp = 0.707\nBzamp = 1.41\noutput = SLSB\n", "mx my mz x y z\n0.881079 -0.345588 -0.322908 1 1 1\n-0.803590 0.331591 -0.494258 1 1 2\n-0.375824 -0.896516 0.234553 1 2 1\n0.113463 0.170433 0.978815 1 2 2\n-0.094322 0.284732 0.953955 2 1 1\n0.388846 -0.920063 -0.047774 2 1 2\n-0.456224 0.424891 0.781874 2 2 1\n-0.825027 0.163355 0.540968 2 2 2\n");

	/* launch simulation */
	launchMagnSimulation(FILE_EXTR_CFG4, FILE_EXTR_DAT4);

	/* check sites */
	checkExtMagnetism(N1, N2, N3, mx, my, mz);

	/* delete created files */
	deleteMagnFiles(FILE_EXTR_CFG4, FILE_EXTR_DAT4, FILE_EXTR_BAS4);

}

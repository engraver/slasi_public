#include <stdio.h>
#include <stdlib.h>
#include "externalField.h"

void externalUniformField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyExternal) {

	/* calculate effective field and energy of external field */
	double effectFieldx = userVars.NormExternal * userVars.Bx;
	double effectFieldy = userVars.NormExternal * userVars.By;
	double effectFieldz = userVars.NormExternal * userVars.Bz;

	latticeParam[indexLattice].EnergyExternalNorm =
			(-1.0) * (effectFieldx * lattice[curIndex] + effectFieldy * lattice[curIndex + 1] +
			          effectFieldz * lattice[curIndex + 2]);
	*EnergyExternal += latticeParam[indexLattice].EnergyExternalNorm;

	/* add generated effective field to the total */
	*Hx += effectFieldx;
	*Hy += effectFieldy;
	*Hz += effectFieldz;

}

void externalGeneralField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyExternal) {

	/* calculate effective field and energy of external field */
	double effectFieldx = userVars.NormExternal * Bamp[curIndex];
	double effectFieldy = userVars.NormExternal * Bamp[curIndex + 1];
	double effectFieldz = userVars.NormExternal * Bamp[curIndex + 2];

	/* to debug code */
	//fprintf(stderr, "%f\n", latticeParam[indexLattice].Hxamp);
	//fprintf(stderr, "%f\n", latticeParam[indexLattice].Hyamp);
	//fprintf(stderr, "%f\n", latticeParam[indexLattice].Hzamp);
	//fprintf(stderr, "======\n");

	latticeParam[indexLattice].EnergyExternalNorm =
			(-1.0) * (effectFieldx * lattice[curIndex] + effectFieldy * lattice[curIndex + 1] +
			          effectFieldz * lattice[curIndex + 2]);
	*EnergyExternal += latticeParam[indexLattice].EnergyExternalNorm;

	/* add generated effective field to the total */
	*Hx += effectFieldx;
	*Hy += effectFieldy;
	*Hz += effectFieldz;

}



#include "../../simulation.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* function to create files to test magnetic simulator (for magnetic interaction) */
void createMagnFiles(char * nameConfFile, char * nameParamFile, char * rowConfFile, char * rowParamFile) {

	/* create configuration file */
	FILE * fid1 = fopen(nameConfFile, "w");
	fprintf(fid1, "%s", rowConfFile);
	fclose(fid1);

	/* create parameter file */
	FILE * fid2 = fopen(nameParamFile, "w");
	fprintf(fid2, "%s", rowParamFile);
	fclose(fid2);

}

/* function to launch simulation (for magnetic interaction) */
void launchMagnSimulation(char * nameConfFile, char * nameParamFile) {

	/* make parameters for launching */
	int argc = 4;
	char **argv;
	argv = (char**) malloc(sizeof(char**) * 4);
	argv[0] = (char*) malloc(sizeof("slasi"));
	argv[0] = "slasi";
	argv[1] = (char*) malloc(sizeof(nameConfFile));
	argv[1] = nameConfFile;
	argv[2] = (char*) malloc(sizeof("-p"));
	argv[2] = "-p";
	argv[3] = (char*) malloc(sizeof(nameParamFile));
	argv[3] = nameParamFile;

	/* launch simulation */
	slasi(argc, argv, false);

}

/* function to delete two files (for magnetic interaction) */
void deleteMagnFiles(char * nameConfFile, char * nameParamFile, char * baseName) {

	/* delete parameter and configuration files */
	remove(nameConfFile);
	remove(nameParamFile);

	/* delete energy logfile which are created during simulation */
	char energy_log [100];
	strcpy(energy_log, baseName);
	strcat(energy_log, "_energy.log");
	remove(energy_log);

	/* delete logfile of initialization which are created during simulation */
	char init_log [100];
	strcpy(init_log, baseName);
	strcat(init_log, "_init.log");
	remove(init_log);

	/* delete logfile of magnetism which are created during simulation */
	char mag_log [100];
	strcpy(mag_log, baseName);
	strcat(mag_log, "_mag.log");
	remove(mag_log);

	/* delete logfile for other parameters which are created during simulation */
	char other_log [100];
	strcpy(other_log, baseName);
	strcat(other_log, "_other.log");
	remove(other_log);

	/* delete file for table of parameters */
	char table_file [100];
	strcpy(table_file, baseName);
	strcat(table_file, ".tbl");
	remove(table_file);

	/* delete the first file of intermediate results */
	char file_inter1 [100];
	strcpy(file_inter1, baseName);
	strcat(file_inter1, ".00000.slsb");
	remove(file_inter1);

	/* delete the second file of intermediate results */
	char file_inter2 [100];
	strcpy(file_inter2, baseName);
	strcat(file_inter2, ".00000.inp");
	remove(file_inter2);

	/* delete the third file of intermediate results */
	char file_inter3 [100];
	strcpy(file_inter3, baseName);
	strcat(file_inter3, ".00001.slsb");
	remove(file_inter3);

	/* delete the fourth file of intermediate results */
	char file_inter4 [100];
	strcpy(file_inter4, baseName);
	strcat(file_inter4, ".00001.inp");
	remove(file_inter4);

	/* delete the restart file of intermediate results */
	char file_restart [100];
	strcpy(file_restart, baseName);
	strcat(file_restart, ".restart.slsb");
	remove(file_restart);

}

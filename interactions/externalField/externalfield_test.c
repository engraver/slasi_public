#include <math.h>
#include "CUnit/Basic.h"

#include "../../alltypes.h"
#include "externalfield_test_shared.h"

/* names of configuration files for testing */
#define FILE_EXTRENER_CFG1 "testextrener1.cfg"
#define FILE_EXTRENER_DAT1 "testextrener1.dat"
#define FILE_EXTRENER_BAS1 "testextrener1"

#define FILE_EXTRENER_CFG2 "testextrener2.cfg"
#define FILE_EXTRENER_DAT2 "testextrener2.dat"
#define FILE_EXTRENER_BAS2 "testextrener2"

#define FILE_EXTRENER_CFG3 "testextrener3.cfg"
#define FILE_EXTRENER_DAT3 "testextrener3.dat"
#define FILE_EXTRENER_BAS3 "testextrener3"

#define FILE_EXTRENER_CFG4 "testextrener4.cfg"
#define FILE_EXTRENER_DAT4 "testextrener4.dat"
#define FILE_EXTRENER_BAS4 "testextrener4"

#define FILE_EXTRENER_CFG5 "testextrener5.cfg"
#define FILE_EXTRENER_DAT5 "testextrener5.dat"
#define FILE_EXTRENER_BAS5 "testextrener5"

#define PREC_TEST_EXTR 0.000001

/* initialization function for tests */
int init_externalfield_test(void) {
	return 0;
}

/* function of cleaning for tests */
int clean_externalfield_test(void) {
	return 0;
}

/*
 * @brief yet another test of exchange interaction and external field
 *
 * chain of two magnetic moments with exchange interaction and external field
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3
 * 000 000 000 000
 * 000 0+0 0+0 000
 * 000 000 000 000
 * @endverbatim
 * Here, + magnetic site with initial value (1,0,0).
 * The magnetic system has exchange interaction and constant external magnetic field (1,0,0).
 * The magnitude of the magnetic field is Tesla.
 * So, all magnetic moments have to be parallel and have total normalizing energy of external field -0.741921.
 */
void test_externalfield_energy_1(void) {

	/* correct energy */
	double EnergyCorr = -0.741921;

	/* create files to test magnetic simulator */
	createMagnFiles(FILE_EXTRENER_CFG1, FILE_EXTRENER_DAT1, "N1 = 2\nN2 = 1\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\nextField = const\nBxamp = 1\nByamp = 0\nBzamp = 0\n", "mx my mz x y z\n1 0 0 0 0 0\n1 0 0 1 0 0\n");

	/* launch simulation */
	launchMagnSimulation(FILE_EXTRENER_CFG1, FILE_EXTRENER_DAT1);

	/* check energy of external field */
	CU_ASSERT(fabs(EnergyExternalNorm - EnergyCorr) < PREC_TEST_EXTR);

	/* delete created files */
	deleteMagnFiles(FILE_EXTRENER_CFG1, FILE_EXTRENER_DAT1, FILE_EXTRENER_BAS1);

}

/*
 * @brief yet another test of exchange interaction and external field
 *
 * plain of four magnetic moments with exchange interaction and external field
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3
 * 000 000 000 000
 * 000 0+0 0+0 000
 * 000 0+0 0+0 000
 * 000 000 000 000
 * @endverbatim
 * Here, + magnetic site with initial value (0.707,0.707,0).
 * The magnetic system has exchange interaction and constant external magnetic field (0.707,0.707,0).
 * The magnitude of the magnetic field is Tesla.
 * So, all magnetic moments have to be parallel and have total normalizing energy of external field -1.483617.
 */
void test_externalfield_energy_2(void) {

	/* correct energy */
	double EnergyCorr = -1.483617;

	/* create files to test magnetic simulator */
	createMagnFiles(FILE_EXTRENER_CFG2, FILE_EXTRENER_DAT2, "N1 = 2\nN2 = 2\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\nextField = const\nBxamp = 0.707\nByamp = 0.707\nBzamp = 0\n", "mx my mz x y z\n0.707 0.707 0 0 0 0\n0.707 0.707 0 1 0 0\n0.707 0.707 0 0 1 0\n0.707 0.707 0 1 1 0\n");

	/* launch simulation */
	launchMagnSimulation(FILE_EXTRENER_CFG2, FILE_EXTRENER_DAT2);

	/* check energy of external field */
	CU_ASSERT(fabs(EnergyExternalNorm - EnergyCorr) < PREC_TEST_EXTR);

	/* delete created files */
	deleteMagnFiles(FILE_EXTRENER_CFG2, FILE_EXTRENER_DAT2, FILE_EXTRENER_BAS2);

}

/*
 * @brief yet another test of exchange interaction and external field
 *
 * cube of eight magnetic moments with exchange interaction and external field
 *
 * @verbatim
 * Location of magnetic moment components in an array
 * 0    1    2    3
 * 0000 0000 0000 0000
 * 0000 0++0 0++0 0000
 * 0000 0++0 0++0 0000
 * 0000 0000 0000 0000
 * @endverbatim
 * Here, + magnetic site with initial value (0.707,0.5,0.5).
 * The magnetic system has exchange interaction and constant external magnetic field (0.707,0.5,0.5).
 * The magnitude of the magnetic field is Tesla.
 * So, all magnetic moments have to be parallel and have total normalizing energy of external field -2.967235.
 */
void test_externalfield_energy_3(void) {

	/* correct energy */
	double EnergyCorr = -2.967235;

	/* create files to test magnetic simulator */
	createMagnFiles(FILE_EXTRENER_CFG3, FILE_EXTRENER_DAT3, "N1 = 2\nN2 = 2\nN3 = 2\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\nextField = const\nBxamp = 0.707\nByamp = 0.0\nBzamp = 0.707\n", "mx my mz x y z\n0.707 0.0 0.707 0 0 0\n0.707 0.0 0.707 1 0 0\n0.707 0.0 0.707 0 1 0\n0.707 0.0 0.707 1 1 0\n0.707 0.0 0.707 0 0 1\n0.707 0.0 0.707 1 0 1\n0.707 0.0 0.707 0 1 1\n0.707 0.0 0.707 1 1 1\n");

	/* launch simulation */
	launchMagnSimulation(FILE_EXTRENER_CFG3, FILE_EXTRENER_DAT3);

	/* check energy of external field */
	CU_ASSERT(fabs(EnergyExternalNorm - EnergyCorr) < PREC_TEST_EXTR);

	/* delete created files */
	deleteMagnFiles(FILE_EXTRENER_CFG3, FILE_EXTRENER_DAT3, FILE_EXTRENER_BAS3);

}

/*
 * @brief yet another test of exchange interaction and external field
 *
 * plain of eight magnetic moments with hole of nonmagnetic spin
 *
 * @verbatim
 * Location of magnetic moment components in an array
 *  0   1   2   3   4
 * 000 000 000 000 000
 * 000 0+0 0+0 0+0 000
 * 000 0+0 0-0 0+0 000
 * 000 0+0 0+0 0+0 000
 * 000 000 000 000 000
 * @endverbatim
 * Here, + magnetic site with initial value (0.707,0.5,0.5) and - non-magnetic site with initial value (0,0,0).
 * The magnetic system has exchange interaction and constant external magnetic field (0.707,0.5,0.5).
 * The magnitude of the magnetic field is Tesla.
 * So, all magnetic moments have to be parallel and have total normalizing energy of external field -2.967459.
 */
void test_externalfield_energy_4(void) {

	/* correct energy */
	double EnergyCorr = -2.967459;

	/* create files to test magnetic simulator */
	createMagnFiles(FILE_EXTRENER_CFG4, FILE_EXTRENER_DAT4, "N1 = 3\nN2 = 3\nN3 = 1\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\nextField = const\nBxamp = 0.707\nByamp = 0.5\nBzamp = 0.5\nmagnInit = file\ntypeMagn = file\n", "mx my mz x y z magn\n0.707 0.5 0.5 1 1 1 1\n0.707 0.5 0.5 1 2 1 1\n0.707 0.5 0.5 1 3 1 1\n0.707 0.5 0.5 2 1 1 1\n0.0 0.0 0.0 2 2 1 0\n0.707 0.5 0.5 2 3 1 1\n0.707 0.5 0.5 3 1 1 1\n0.707 0.5 0.5 3 2 1 1\n0.707 0.5 0.5 3 3 1 1\n");

	/* launch simulation */
	launchMagnSimulation(FILE_EXTRENER_CFG4, FILE_EXTRENER_DAT4);

	/* check energy of external field */
	CU_ASSERT(fabs(EnergyExternalNorm - EnergyCorr) < PREC_TEST_EXTR);

	/* delete created files */
	deleteMagnFiles(FILE_EXTRENER_CFG4, FILE_EXTRENER_DAT4, FILE_EXTRENER_BAS4);

}

/*
 * @brief yet another test of exchange interaction and external field
 *
 * parallelepiped of sixteen magnetic moments with hole of nonmagnetic spin
 *
 * @verbatim
 * Location of magnetic moment components in an array
 * 0    1    2    3
 * 0000 0000 0000 0000 0000
 * 0000 0++0 0++0 0++0 0000
 * 0000 0++0 0--0 0++0 0000
 * 0000 0++0 0++0 0++0 0000
 * 0000 0000 0000 0000 0000
 * @endverbatim
 * Here, + magnetic site with initial value (0.5,0.5,0.707) and - non-magnetic site with initial value (0,0,0).
 * The magnetic system has exchange interaction and constant external magnetic field (0.5,0.5,0.707).
 * The magnitude of the magnetic field is Tesla.
 * So, all magnetic moments have to be parallel and have total normalizing energy of external field -5.934918.
 */
void test_externalfield_energy_5(void) {

	/* correct energy */
	double EnergyCorr = -5.934918;

	/* create files to test magnetic simulator */
	createMagnFiles(FILE_EXTRENER_CFG5, FILE_EXTRENER_DAT5, "N1 = 3\nN2 = 3\nN3 = 2\nJ = 1e-10\nmagnInit = file\ngilbertDamping=0.5\nt0 = 0\ntfin = 1\nframe = 1\nextField = const\nBxamp = 0.5\nByamp = 0.5\nBzamp = 0.707\nmagnInit = file\ntypeMagn = file\n", "mx my mz x y z magn\n0.5 0.5 0.707 1 1 1 1\n0.5 0.5 0.707 1 1 2 1\n0.5 0.5 0.707 1 2 1 1\n0.5 0.5 0.707 1 2 2 1\n0.5 0.5 0.707 1 3 1 1\n0.5 0.5 0.707 1 3 2 1\n0.0 0.0 0.0 2 1 1 0\n0.0 0.0 0.0 2 1 2 0\n0.5 0.5 0.707 2 2 1 1\n0.5 0.5 0.707 2 2 2 1\n0.5 0.5 0.707 2 3 1 1\n0.5 0.5 0.707 2 3 2 1\n0.5 0.5 0.707 3 1 1 1\n0.5 0.5 0.707 3 1 2 1\n0.5 0.5 0.707 3 2 1 1\n0.5 0.5 0.707 3 2 2 1\n0.5 0.5 0.707 3 3 1 1\n0.5 0.5 0.707 3 3 2 1\n");

	/* launch simulation */
	launchMagnSimulation(FILE_EXTRENER_CFG5, FILE_EXTRENER_DAT5);

	/* check energy of external field */
	CU_ASSERT(fabs(EnergyExternalNorm - EnergyCorr) < PREC_TEST_EXTR);

	/* delete created files */
	deleteMagnFiles(FILE_EXTRENER_CFG5, FILE_EXTRENER_DAT5, FILE_EXTRENER_BAS5);

}


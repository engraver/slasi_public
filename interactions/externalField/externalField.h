#ifndef _EXTERNALFIELD_H_
#define _EXTERNALFIELD_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../../alltypes.h"
#include "../interactions.h"

/**
 * @brief This function returns effective field and energy
 * which created by external uniform (constant) field
 * @param curIndex current spin index in array "lattice"
 * @param indexLattice current index in array "latticeParam"
 * @param Hx effective field on axis x
 * @param Hy effective field on axis y
 * @param Hz effective field on axis z
 * @param EnergyExternal normalized external energy
 */
void externalUniformField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyExternal);

/**
 * @brief This function returns effective field and energy
 * which created by external general (any) field
 * @param curIndex current spin index in array "lattice"
 * @param indexLattice current index in array "latticeParam"
 * @param Hx effective field on axis x
 * @param Hy effective field on axis y
 * @param Hz effective field on axis z
 * @param EnergyExternal normalized external energy
 */
void externalGeneralField(size_t curIndex, size_t indexLattice, double *Hx, double *Hy, double *Hz, double *EnergyExternal);

/**
 * @brief This function of wrapper returns effective field and energy
 * which created by external uniform field (using GPU)
 * @param d_H is array of effective field (GPU)
 * @param Bx, By, Bz are components of external magnetic field
 * @param d_EnergyTotal is array of energy for sites (GPU)
 * @param kExtField is proportionality factor
 * @param numberSites is number of sites
 */
void externalUniformFieldCUDA(double *d_H, double Bx, double By, double Bz, double *d_EnergyTotal, double kExtField, long numberSites);

/**
 * @brief This function of wrapper returns effective field and energy
 * which created by external general (any) field (using GPU)
 * @param d_H is array of effective field (GPU)
 * @param d_latticeParam is array of parameters for sites (GPU)
 * @param d_EnergyTotal is array of energy for sites (GPU)
 * @param kExtField is proportionality factor
 * @param numberSites is number of sites
 */
void externalGeneralFieldCUDA(double *d_H, double *d_EnergyTotal, LatticeSiteDescr *d_latticeParam, double kExtField, long numberSites);

#endif

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include <Python.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "fieldSetup.h"
#include "../../utils/parallel_utils.h"

int harmTime(double t) {

	/* check variables for harmonic field (if all parameters are in .dat file ) */
	if ((userVars.iBxamp != -1) && (userVars.iByamp != -1) && (userVars.iBzamp != -1) &&
	    (userVars.iBphase != -1) && (userVars.iBfreq != -1)) {

		/* pass through each magnetic moment */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						int curIndex = IDXmg(i1, i2, i3);
						/* check if site is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							/* to save amplitude */
							BampSaved[curIndex] = Bamp[curIndex];
							BampSaved[curIndex + 1] = Bamp[curIndex + 1];
							BampSaved[curIndex + 2] = Bamp[curIndex + 2];
						}
					}
				}
			}
		}

		/* pass through each magnetic moment to setup harmonic external field */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						int curIndex = IDXmg(i1, i2, i3);
						/* check if site is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							/* change external field using harmonic law */
							Bamp[curIndex] = BampSaved[curIndex] *
							                                   sin(latticeParam[indexLattice].Bfreq * t +
							                                       latticeParam[indexLattice].Bphase);
							Bamp[curIndex + 1] = BampSaved[curIndex + 1] *
							                                   sin(latticeParam[indexLattice].Bfreq * t +
							                                       latticeParam[indexLattice].Bphase);
							Bamp[curIndex + 2] = BampSaved[curIndex + 2] *
							                                   sin(latticeParam[indexLattice].Bfreq * t +
							                                       latticeParam[indexLattice].Bphase);
						}
					}
				}
			}
		}

		/* check variables for harmonic field (if all parameters are in .cfg file ) */
	} else if ((userVars.bBxamp) && (userVars.bByamp) && (userVars.bBzamp) &&
	           (userVars.bBphase) && (userVars.bBfreq)) {

		/* pass through each magnetic moment */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						int curIndex = IDXmg(i1, i2, i3);
						/* check if site is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							/* change external field using harmonic law */
							Bamp[curIndex] =
									userVars.Bxamp * sin(userVars.Bfreq * t + userVars.Bphase);
							Bamp[curIndex + 1] =
									userVars.Byamp * sin(userVars.Bfreq * t + userVars.Bphase);
							Bamp[curIndex + 2] =
									userVars.Bzamp * sin(userVars.Bfreq * t + userVars.Bphase);
						}
					}
				}
			}
		}

	} else {
		/* return error */
		fprintf(stderr, "(file %s | line %d) Lack of parameters for external harmonic field!\n", __FILE__, __LINE__);
		return 1;
	}

	return 0;

}

double getSinc(double t, size_t indexLattice) {

	/* calculate argument in sinc() */
	double time_dep = 0.0;
	double tt = (t - latticeParam[indexLattice].fieldSincTShift)/latticeParam[indexLattice].fieldSincTWidth;

	/* calculate sinc() */
	if (fabs(tt) < NEAR_ZERO) {
		time_dep = 1.0;
	}
	else {
		time_dep = sin(tt) / tt;
	}

	/* return value */
	return time_dep;

}

int sincTime(double t) {

	/* check variables for harmonic field (if all parameters are in .dat file ) */
	if ((userVars.iBxamp != -1) && (userVars.iByamp != -1) && (userVars.iBzamp != -1) &&
	    (userVars.ifieldSincTShift != -1) && (userVars.ifieldSincTShift != -1)) {
		/* process each magnetic moment */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						int curIndex = IDXmg(i1, i2, i3);
						/* check if site is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							/* to save amplitude */
							BampSaved[curIndex] = Bamp[curIndex];
							BampSaved[curIndex + 1] = Bamp[curIndex + 1];
							BampSaved[curIndex + 2] = Bamp[curIndex + 2];
						}
					}
				}
			}
		}

		/* process each magnetic moment to setup sinc external field */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						int curIndex = IDXmg(i1, i2, i3);
						/* check if site is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							double time_dep = getSinc(t, indexLattice);
							/* change external field using sinc law */
							Bamp[curIndex] = BampSaved[curIndex] * time_dep;
							Bamp[curIndex + 1] = BampSaved[curIndex + 1] * time_dep;
							Bamp[curIndex + 2] = BampSaved[curIndex + 2] * time_dep;
						}
					}
				}
			}
		}

		/* check variables for harmonic field (if all parameters are in .cfg file ) */
	} else if ((userVars.bBxamp) && (userVars.bByamp) && (userVars.bBzamp) &&
	           (userVars.bfieldSincTShift) && (userVars.bfieldSincTWidth)) {

		/* go by each magnetic moment */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {
						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						int curIndex = IDXmg(i1, i2, i3);
						/* check if site is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							double time_dep = getSinc(t, indexLattice);
							/* change external field using sinc */
							Bamp[curIndex] = userVars.Bxamp * time_dep;
							Bamp[curIndex + 1] = userVars.Byamp * time_dep;
							Bamp[curIndex + 2] = userVars.Bzamp * time_dep;
						}
					}
				}
			}
		}

	} else {
		/* return error */
		fprintf(stderr, "(file %s | line %d) Lack of parameters for sinc time field!\n", __FILE__, __LINE__);
		return 1;
	}

	return 0;

}

/* Python objects to call function */
/* PyObject *pFieldArgs, *pFieldFunc; */

int scriptTime(double t) {

	/* declarations of usual variables */
	double Bxamp, Byamp, Bzamp;
	int tester;
	/* declarations of Python variables */
	PyObject *pValueRes = NULL, *pValueTime = NULL, *pValueCoor1 = NULL,
	*pValueCoor2 = NULL, *pValueCoor3 = NULL, *pValueInd1 = NULL, *pValueInd2 = NULL, *pValueInd3 = NULL;

	/* pass through each magnetic moment */
	for (int i1 = 0; i1 < userVars.N1; i1++) {
		for (int i2 = 0; i2 < userVars.N2; i2++) {
			for (int i3 = 0; i3 < userVars.N3; i3++) {
				if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
				    i3 < userVars.N3 - 1) {

					/* number of spin in lattice */
					int indexLattice = IDX(i1, i2, i3);
					int curIndex = IDXmg(i1, i2, i3);

					/* check if site is magnetic */
					if (latticeParam[indexLattice].magnetic) {

						/* time of simulation */
						pValueTime = PyFloat_FromDouble(t);
						/* check value */
						if (!pValueTime) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 0, pValueTime);

						/* the first coordinate */
						pValueCoor1 = PyFloat_FromDouble(*(latticeParam[indexLattice].x));
						/* check value */
						if (!pValueCoor1) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 1, pValueCoor1);

						/* the second coordinate */
						pValueCoor2 = PyFloat_FromDouble(*(latticeParam[indexLattice].y));
						/* check value */
						if (!pValueCoor2) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 2, pValueCoor2);

						/* the third coordinate */
						pValueCoor3 = PyFloat_FromDouble(*(latticeParam[indexLattice].z));
						/* check value */
						if (!pValueCoor3) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 3, pValueCoor3);

						/* the first index */
						pValueInd1 = PyLong_FromLong(i1);
						/* check value */
						if (!pValueInd1) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 4, pValueInd1);

						/* the second index */
						pValueInd2 = PyLong_FromLong(i2);
						/* check value */
						if (!pValueInd2) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 5, pValueInd2);

						/* the third index */
						pValueInd3 = PyLong_FromLong(i3);
						/* check value */
						if (!pValueInd3) {
							/* output error */
							Py_DECREF(pFieldArgs);
							fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
						}
						/* put new parameter */
						PyTuple_SetItem(pFieldArgs, 6, pValueInd3);

						/* call function */
						pValueRes = PyObject_CallObject(pFieldFunc, pFieldArgs);
						/* check results of calling */
						if (pValueRes != NULL) {
							/* get results of calling */
							tester = PyArg_ParseTuple(pValueRes, "ddd", &Bxamp, &Byamp, &Bzamp);
							/* check if it was able to get */
							if (tester) {
								/* save results */
								Bamp[curIndex] = Bxamp;
								Bamp[curIndex + 1] = Byamp;
								Bamp[curIndex + 2] = Bzamp;
							} else {
								/* output error */
								fprintf(stderr, "Call of python function getExtField failed!\n");
							}
							Py_DECREF(pValueRes);
						}

					}

				}
			}
		}
	}

	/* free memory from Python variables */
	Py_XDECREF(pValueTime);
	Py_XDECREF(pValueCoor1);
	Py_XDECREF(pValueCoor2);
	Py_XDECREF(pValueCoor3);
	Py_XDECREF(pValueInd1);
	Py_XDECREF(pValueInd2);
	Py_XDECREF(pValueInd3);
	Py_XDECREF(pValueRes);

	/* return default value */
	return 0;

}

int constTime(void) {

	/* check variables for constant field */
	if ((userVars.bBxamp) && (userVars.bByamp) && (userVars.bBzamp)) {

		/* set constant external field */
		userVars.Bx = userVars.Bxamp;
		userVars.By = userVars.Byamp;
		userVars.Bz = userVars.Bzamp;

	} else {
		/* return error */
		fprintf(stderr, "(file %s | line %d) Lack of parameters for external constant field!\n", __FILE__, __LINE__);
		return 1;
	}

	return 0;

}

int fieldSetup(double t) {

	/* select function to setup */
	switch (userVars.extField) {
		/* harmonic law */
		case EXTFIELD_HARMTIME:
			return harmTime(t);
			/* using python script */
		case EXTFIELD_SCRIPT:
			return scriptTime(t);
			/* constant field */
		case EXTFIELD_CONST:
			return constTime();
			/* law B(t) = B * sinc(a*(t-t0)) */
		case EXTFIELD_SINC:
			return sincTime(t);
			/* return error */
		default:
			fprintf(stderr, "(file %s | line %d) Harmonic, sinc, script or constant mode are necessary to set external field!\n", __FILE__, __LINE__);
			return 1;
	}

}

int setupExternalField(double **numbers, int indlp, int indn) {

	/* calculate current index */
	int curIndex = NUMBER_COMP_FIELD * indlp;

	if ((numbers) && (userVars.iBxamp != -1) && (userVars.iByamp != -1) && (userVars.iBzamp != -1)) {
		/* fill array of external field using .dat file */
		Bamp[curIndex] = numbers[indn][userVars.iBxamp];
		Bamp[curIndex + 1] = numbers[indn][userVars.iByamp];
		Bamp[curIndex + 2] = numbers[indn][userVars.iBzamp];
	} else {
		/* default zero field */
		Bamp[curIndex] = 0.0;
		Bamp[curIndex + 1] = 0.0;
		Bamp[curIndex + 2] = 0.0;
	}

	return 0;

}

int setupSincTField(double **numbers, int indlp, int indn) {

	/* calculate current index */
	int curIndex = NUMBER_COMP_FIELD * indlp;

	if ((numbers) && (userVars.iBxamp != -1) && (userVars.iByamp != -1) && (userVars.iBzamp != -1)) {

		Bamp[curIndex] = numbers[indn][userVars.iBxamp];
		Bamp[curIndex + 1] = numbers[indn][userVars.iByamp];
		Bamp[curIndex + 2] = numbers[indn][userVars.iBzamp];
		if ((userVars.ifieldSincTWidth != -1) && (userVars.ifieldSincTShift != -1)) {
			latticeParam[indlp].fieldSincTWidth = numbers[indn][userVars.ifieldSincTWidth];
			latticeParam[indlp].fieldSincTShift = numbers[indn][userVars.ifieldSincTShift];
		} else if ((userVars.ifieldSincTWidth != -1) || (userVars.ifieldSincTShift != -1)) {
			/* return and write error */
			fprintf(flog, "(file %s | line %d) Bsinc_twidth and Bsinc_tshift should be given simultaneously in paramFile if they are given!\n", __FILE__, __LINE__);
			fprintf(stderr, "(file %s | line %d) Bsinc_twidth and Bsinc_tshift should be given simultaneously in paramFile if they are given!\n", __FILE__, __LINE__);
			return 1;
		}

	} else if (!numbers && userVars.bBxamp && userVars.bByamp && userVars.bBzamp) {

		Bamp[curIndex] = userVars.Bxamp;
		Bamp[curIndex + 1] = userVars.Byamp;
		Bamp[curIndex + 2] = userVars.Bzamp;
		if (userVars.bfieldSincTWidth && userVars.bfieldSincTShift) {
			latticeParam[indlp].fieldSincTWidth = userVars.fieldSincTWidth;
			latticeParam[indlp].fieldSincTShift = userVars.fieldSincTShift;
		} else {
			/* return and write error */
			fprintf(flog, "(file %s | line %d) Bsinc_twidth and Bsinc_tshift should be given simultaneously in paramFile if they are given!\n", __FILE__, __LINE__);
			fprintf(stderr, "(file %s | line %d) Bsinc_twidth and Bsinc_tshift should be given simultaneously in paramFile if they are given!\n", __FILE__, __LINE__);
			return 1;
		}

	} else {
		/* default zero field */
		Bamp[curIndex] = 0.0;
		Bamp[curIndex + 1] = 0.0;
		Bamp[curIndex + 2] = 0.0;
	}

	return 0;

}

int setupPhaseAndFreqExtField(double **numbers, int indlp, int indn) {

	if ((userVars.bBphase) && (userVars.bBfreq)) {

		/* from .cfg file */
		latticeParam[indlp].Bphase = userVars.Bphase;
		latticeParam[indlp].Bfreq = userVars.Bfreq;

	} else if ((numbers) && (userVars.iBphase != -1) && (userVars.iBfreq != -1)) {

		/* from .dat file */
		//if (numbers == NULL) {
		fprintf(flog, "(file %s | line %d) paramFile is not processed, while expected for setupPhaseAndFreqExtField()!\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) paramFile is not processed, while expected for setupPhaseAndFreqExtField()!\n", __FILE__, __LINE__);
		return 1;
		//}

		latticeParam[indlp].Bphase = numbers[indn][userVars.iBphase];
		latticeParam[indlp].Bfreq = numbers[indn][userVars.iBfreq];

	} else {
		/* return and write error */
		fprintf(flog, "(file %s | line %d) Something wrong with phase or frequency of external field 'Bfreq' or 'Bphase' (Are all of them given in paramFile or configuration file?)!\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Something wrong with phase or frequency of external field 'Bfreq' or 'Bphase' (Are all of them given in paramFile or configuration file?)!\n", __FILE__, __LINE__);
		return 1;
	}

	return 0;

}

int initExtScriptField(void) {

	/* check mode of external field and id of process */
	if ((userVars.extField != EXTFIELD_SCRIPT) || (!doItInMasterProcess())) {
		return 0;
	}

	/* check module */
	if (pModulePython != NULL) {
		/* call function */
		pFieldFunc = PyObject_GetAttrString(pModulePython, "getExtField");
		/* check if function was found */
		if (pFieldFunc && PyCallable_Check(pFieldFunc)) {
			/* create arguments */
			pFieldArgs = PyTuple_New(7);
		} else {
			/* output error */
			fprintf(stderr, "Cannot convert arguments for function getExtField!\n");
			Py_DECREF(pFieldFunc);
			return 1;
		}
	} else {
		/* output error */
		fprintf(stderr, "Python module does not load!\n");
		return 1;
	}

	/* initialization of Python variables */
	pValueRes = NULL;
	pValueTime = NULL;
	pValueCoor1 = NULL;
	pValueCoor2 = NULL;
	pValueCoor3 = NULL;
	pValueInd1 = NULL;
	pValueInd2 = NULL;
	pValueInd3 = NULL;

	/* default value */
	return 0;

}

void finalizeExtScriptField(void) {

	/* check mode of external field and id of process */
	if ((userVars.extField != EXTFIELD_SCRIPT) || (!doItInMasterProcess())) {
		return;
	}

	/* free function variables */
	Py_DECREF(pFieldFunc);
	Py_DECREF(pFieldArgs);

}

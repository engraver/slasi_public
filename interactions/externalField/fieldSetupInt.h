/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

/**
 * @file fieldSetup.h
 * @brief Update magnetic field relative to changed time
 */

#ifndef _FIELD_SETUP_INT_H_
#define _FIELD_SETUP_INT_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../../alltypes.h"
#include "../../utils/indices.h"

/**
 * @brief Update magnetic field relative to changed time for intergator (any time)
 * @param t time, current simulation time
 */
int fieldSetupInt(double t);

#endif
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

/**
 * @file fieldSetup.h
 * @brief Update magnetic field relative to changed time
 */

#ifndef _FIELD_SETUP_H_
#define _FIELD_SETUP_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../../alltypes.h"
#include "../../utils/indices.h"

/**
 * @brief Update magnetic field relative to changed time for initialization (zero time)
 * @param t time, current simulation time
 */
int fieldSetup(double t);

/**
 * @brief Updates `latticeParam` field amplitudes
 * @param numbers array, read from paramfile
 * @param indlp index in `latticeParam`
 * @param indn index in `numbers`
 * @return 0 in correct case
 */
int setupExternalField(double **numbers, int indlp, int indn);

/**
 * @brief Updates `latticeParam` for sinc field parameters
 * @param numbers array, read from paramfile
 * @param indlp index in `latticeParam`
 * @param indn index in `numbers`
 * @return 0 in correct case
 */
int setupSincTField(double **numbers, int indlp, int indn);

/**
 * @brief Updates `latticeParam` for harmonic field parameters
 * @param numbers array, read from paramfile
 * @param indlp index in `latticeParam`
 * @param indn index in `numbers`
 * @return 0 in correct case
 */
int setupPhaseAndFreqExtField(double **numbers, int indlp, int indn);

/**
 * @brief Function to initialize Python variables to read values of external field from script
 * @return 0 in correct case
 */
int initExtScriptField(void);

/**
 * @brief Function to finalize Python variables to read values of external field from script
 * @return 0 in correct case
 */
void finalizeExtScriptField(void);

#endif

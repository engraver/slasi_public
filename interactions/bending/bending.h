#ifndef _BENDING_H
#define _BENDING_H

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../../alltypes.h"
#include "../interactions.h"
#include "../../utils/indices.h"

/**
 * @brief setupBendingTriangle
 * @param numbers array with values from paramFile
 * @param indlp index for latticeParam array
 * @param indn index for numbers array
 * @return 0 if ok, otherwise nonzero value
 */
int setupBendingTriangle(double **numbers, int indlp, int indn);

/**
 * @brief This function returns force and energy
 * which created by bending interaction of one node
 * @param i1 first index of node
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Fx force of bending interaction on axis x
 * @param Fy force of bending interaction on axis y
 * @param Fz force of bending interaction on axis z
 * @param Ebend energy of bending
 */
void bendingForceAndEnergy(int i1, size_t curIndex, size_t indexLattice, double *Fx, double *Fy, double *Fz, double *Ebend);

/**
 * @brief This function returns force and energy
 * which created by bending interaction of one node for triangular lattice
 * @param i1 first index of node
 * @param curIndex current spin index
 * @param indexLattice current index of lattice
 * @param Fx force of bending interaction on axis x
 * @param Fy force of bending interaction on axis y
 * @param Fz force of bending interaction on axis z
 * @param Ebend energy of bending
 */
void bendingForceAndEnergyTriangle(int i1, int i2, size_t curIndex, size_t indexLattice, double *Fx, double *Fy, double *Fz, double *Ebend);

#endif

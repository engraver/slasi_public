#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "bending.h"
#include "../../utils/parallel_utils.h"

int setupBendingTriangle(double **numbers, int indlp, int indn) {

	if (userVars.bbetaTri) {
		if (userVars.latticeType != LATTICE_TRIANGULAR) {
			messageToStream("BENDING.C: betaTri can be given only for latticeType = triangular!\n", stderr);
			return 1;
		}

		/* constant value */
		latticeParam[indlp].betaU1 = userVars.betaTri;
		latticeParam[indlp].betaU2 = userVars.betaTri;
		latticeParam[indlp].betaU3 = userVars.betaTri;
		latticeParam[indlp].betaD1 = userVars.betaTri;
		latticeParam[indlp].betaD2 = userVars.betaTri;
		latticeParam[indlp].betaD3 = userVars.betaTri;
	} /* else {
        char msg[] = "BENDING.C: Something wrong with parameters of bending for triangular lattice (Are all of them given in paramFile?)!\n";
        fprintf(flog, "%s", msg);
        fprintf(stderr, "%s", msg);
        return 1;
    } */

	return 0;

}

void bendingForceAndEnergy(int i1, size_t curIndex1, size_t indexLattice1, double *Fx, double *Fy, double *Fz, double *Ebend) {

	/* declarations of additional variables */
	
	/* number of projections of all spins */
	size_t spinsNum = NUMBER_COMP_MAGN*userVars.N1*userVars.N2*userVars.N3;
	size_t curIndex, indexLattice;
    
	/*********************
     * i1-th site
    *********************/
	/* coordinates */
	double xi = lattice[curIndex1 + spinsNum];
	double yi = lattice[curIndex1 + spinsNum + 1];
	double zi = lattice[curIndex1 + spinsNum + 2];
	/* tangential vector */
	double tix = latticeParam[indexLattice1].tx;
	double tiy = latticeParam[indexLattice1].ty;
	double tiz = latticeParam[indexLattice1].tz;
	/* bending coefficient */
	double bi = latticeParam[indexLattice1].beta;
    
	/*********************
     * (i1-1)-th site
    *********************/
	if (i1 > 1) {
		curIndex     = IDXmg(i1 - 1, 1, 1);
		indexLattice =   IDX(i1 - 1, 1, 1);
	} else {
		curIndex     = IDXmg(userVars.N1 - 2, 1, 1);
		indexLattice =   IDX(userVars.N1 - 2, 1, 1);
	}
	/* coordinates */
	double xim1 = lattice[curIndex + spinsNum];
	double yim1 = lattice[curIndex + spinsNum + 1];
	double zim1 = lattice[curIndex + spinsNum + 2];
	/* tangential vector */
	double tixm1 = latticeParam[indexLattice].tx;
	double tiym1 = latticeParam[indexLattice].ty;
	double tizm1 = latticeParam[indexLattice].tz;
	/* bending coefficient */
	double bim1 = latticeParam[indexLattice].beta;
	if ((userVars.periodicBC1 == PBC_OFF) && (i1 == 1)) {
		bim1 = 0.0;
	}
    
	/*********************
     * (i1-2)-th site
    *********************/
	if (i1 > 2) {
		curIndex     = IDXmg(i1 - 2, 1, 1);
		indexLattice =   IDX(i1 - 2, 1, 1);
	} else if (i1 == 2) {
		curIndex     = IDXmg(userVars.N1 - 2, 1, 1);
		indexLattice =   IDX(userVars.N1 - 2, 1, 1);
	} else {
		curIndex     = IDXmg(userVars.N1 - 3, 1, 1);
		indexLattice =   IDX(userVars.N1 - 3, 1, 1);
	}
	/* tangential vector */
	double tixm2 = latticeParam[indexLattice].tx;
	double tiym2 = latticeParam[indexLattice].ty;
	double tizm2 = latticeParam[indexLattice].tz;
	/* bending coefficient */
	double bim2 = latticeParam[indexLattice].beta;
	if ((userVars.periodicBC1 == PBC_OFF) && ((i1 == 1) || (i1 == 2))) {
		bim2 = 0.0;
	}
    
	/*********************
     * (i1+1)-th site
    *********************/
	if (i1 < userVars.N1 - 2) {
		curIndex     = IDXmg(i1 + 1, 1, 1);
		indexLattice =   IDX(i1 + 1, 1, 1);
	} else {
		curIndex     = IDXmg(1, 1, 1);
		indexLattice =   IDX(1, 1, 1);
	}
	/* coordinates */
	double xip1 = lattice[curIndex + spinsNum];
	double yip1 = lattice[curIndex + spinsNum + 1];
	double zip1 = lattice[curIndex + spinsNum + 2];
	/* tangential vector */
	double tixp1 = latticeParam[indexLattice].tx;
	double tiyp1 = latticeParam[indexLattice].ty;
	double tizp1 = latticeParam[indexLattice].tz;
        
	double delta_tixm2 = tixm1 - tixm2;
	double delta_tiym2 = tiym1 - tiym2;
	double delta_tizm2 = tizm1 - tizm2;
    
	double delta_tixm1 = tix - tixm1;
	double delta_tiym1 = tiy - tiym1;
	double delta_tizm1 = tiz - tizm1;
    
	double delta_tix = tixp1 - tix;
	double delta_tiy = tiyp1 - tiy;
	double delta_tiz = tizp1 - tiz;
        
	/* derivatives */
	double dx = xi-xim1;
	double dy = yi-yim1;
	double dz = zi-zim1;
	double dr = sqrt( dx*dx + dy*dy + dz*dz );
	double dr3 = dr*dr*dr; 
	double dtxim1_dx = 1.0/dr - dx*dx/dr3;
	double dtxim1_dy = -dx*dy/dr3;
	double dtxim1_dz = -dx*dz/dr3;
	double dtyim1_dx = dtxim1_dy;
	double dtyim1_dy = 1.0/dr - dy*dy/dr3;
	double dtyim1_dz = -dy*dz/dr3;
	double dtzim1_dx = dtxim1_dz;
	double dtzim1_dy = dtyim1_dz;
	double dtzim1_dz = 1.0/dr - dz*dz/dr3;
	dx = xip1-xi;
	dy = yip1-yi;
	dz = zip1-zi;
	dr = sqrt( dx*dx + dy*dy + dz*dz );
	dr3 = dr*dr*dr; 
	double dtxi_dx = -1.0/dr + dx*dx/dr3;
	double dtxi_dy = dx*dy/dr3;
	double dtxi_dz = dx*dz/dr3;
	double dtyi_dx = dtxi_dy;
	double dtyi_dy = -1.0/dr + dy*dy/dr3;
	double dtyi_dz = dy*dz/dr3;
	double dtzi_dx = dtxi_dz;
	double dtzi_dy = dtyi_dz;
	double dtzi_dz = -1.0/dr + dz*dz/dr3;	
	
	/* forces */
	*Fx -= ( dtxim1_dx * ( bim2*delta_tixm2 - bim1*delta_tixm1 ) + dtxi_dx * ( bim1*delta_tixm1 - bi*delta_tix ) +
	         dtyim1_dx * ( bim2*delta_tiym2 - bim1*delta_tiym1 ) + dtyi_dx * ( bim1*delta_tiym1 - bi*delta_tiy ) +
	         dtzim1_dx * ( bim2*delta_tizm2 - bim1*delta_tizm1 ) + dtzi_dx * ( bim1*delta_tizm1 - bi*delta_tiz )
	);
	*Fy -= ( dtxim1_dy * ( bim2*delta_tixm2 - bim1*delta_tixm1 ) + dtxi_dy * ( bim1*delta_tixm1 - bi*delta_tix ) +
	         dtyim1_dy * ( bim2*delta_tiym2 - bim1*delta_tiym1 ) + dtyi_dy * ( bim1*delta_tiym1 - bi*delta_tiy ) +
	         dtzim1_dy * ( bim2*delta_tizm2 - bim1*delta_tizm1 ) + dtzi_dy * ( bim1*delta_tizm1 - bi*delta_tiz )
	);
	*Fz -= ( dtxim1_dz * ( bim2*delta_tixm2 - bim1*delta_tixm1 ) + dtxi_dz * ( bim1*delta_tixm1 - bi*delta_tix ) +
	         dtyim1_dz * ( bim2*delta_tiym2 - bim1*delta_tiym1 ) + dtyi_dz * ( bim1*delta_tiym1 - bi*delta_tiy ) +
	         dtzim1_dz * ( bim2*delta_tizm2 - bim1*delta_tizm1 ) + dtzi_dz * ( bim1*delta_tizm1 - bi*delta_tiz )
	);

	/* energy */
	*Ebend += bi*( delta_tix*delta_tix + delta_tiy*delta_tiy + delta_tiz*delta_tiz);

}

void bendingForceAndEnergyTriangle(int i1, int i2, size_t curIndex, size_t indexLattice, double *Fx, double *Fy, double *Fz, double *Ebend) {

	/* declarations of additional variables */
	
	/* number of projections of all spins */
	size_t spinsNum = NUMBER_COMP_MAGN*userVars.N1*userVars.N2*userVars.N3;
    
	/***************************
     * i-th site
    ***************************/
    /* coordinates */
	double xi = lattice[curIndex + spinsNum];
	double yi = lattice[curIndex + spinsNum + 1];
	double zi = lattice[curIndex + spinsNum + 2];
	
	/* up normal vector */
	double normiU_x;
	double normiU_y;
	double normiU_z;
	int Ui;
	if ((i2 < userVars.N2 - 2) && (i1 < userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF) ) {
		normiU_x = latticeParam[indexLattice].trinxU;
		normiU_y = latticeParam[indexLattice].trinyU;
		normiU_z = latticeParam[indexLattice].trinzU;
		Ui = 1;
	} else if ((i2 < userVars.N2 - 2) && (userVars.periodicBC1 == PBC_ON) ) {
		normiU_x = latticeParam[indexLattice].trinxU;
		normiU_y = latticeParam[indexLattice].trinyU;
		normiU_z = latticeParam[indexLattice].trinzU;
		Ui = 1;
	} else {
		normiU_x = 0.0;
		normiU_y = 0.0;
		normiU_z = 0.0;
		Ui = 0;
	}
	
	/* down normal vector */
	double normiD_x;
	double normiD_y;
	double normiD_z;
	int Di;
	if ((i2 > 1) && (i1 < userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF) ) {
		normiD_x = latticeParam[indexLattice].trinxD;
		normiD_y = latticeParam[indexLattice].trinyD;
		normiD_z = latticeParam[indexLattice].trinzD;
		Di = 1;
	} else if ((i2 > 1) && (userVars.periodicBC1 == PBC_ON) ) {
		normiD_x = latticeParam[indexLattice].trinxD;
		normiD_y = latticeParam[indexLattice].trinyD;
		normiD_z = latticeParam[indexLattice].trinzD;
		Di = 1;
	} else {
		normiD_x = 0.0;
		normiD_y = 0.0;
		normiD_z = 0.0;
		Di = 0;
	}

	/* bending coefficients for up triangle
	double betaUi_1 = 0.0;
	double betaUi_2 = 0.0;*/
	double betaUi_3 = latticeParam[indexLattice].betaU3;
	if ((i2 == 1) || (i2 == userVars.N2 - 2) || ((i1 == userVars.N1 -2) && (userVars.periodicBC1 == PBC_OFF)) ) {
		betaUi_3 = 0.0;
	}

	/* bending coefficients for down triangle */
	double betaDi_1 = latticeParam[indexLattice].betaD1;
	/* double betaDi_2 = 0.0; */
	double betaDi_3 = latticeParam[indexLattice].betaD3;
	if ((i2 == 1) || ((i1 == userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF) ) ) {
		betaDi_1 = 0.0;
		betaDi_3 = 0.0;
	} else if ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		betaDi_3 = 0.0;
	} else if ((i1 == userVars.N1 - 3) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		betaDi_1 = 0.0;
	}
	
	/***************************
     * 1-st neighbouring site <-- only for force calculation
    ***************************/
	if (i1 < userVars.N1 - 2) {
		curIndex = IDXtrmg1(i1, i2);
		indexLattice = IDXtr1(i1, i2);
	} else {
		curIndex = IDXtrmg(1, i2);
		indexLattice = IDXtr(1, i2);
	}		
	/* coordinates */
	double x1 = lattice[curIndex + spinsNum];
	double y1 = lattice[curIndex + spinsNum + 1];
	double z1 = lattice[curIndex + spinsNum + 2];
	
	/***************************
     * 2-nd neighbouring site
    ***************************/
	if (i1 < userVars.N1 - 2) {
		curIndex = IDXtrmg2(i1, i2);
		indexLattice = IDXtr2(i1, i2);
	} else if ((i1 == userVars.N1 - 2) && IsODD(i2) ) {
		/* odd row */
		curIndex = IDXtrmg2(i1, i2);
		indexLattice = IDXtr2(i1, i2);
	} else {
		/* even row */
		curIndex = IDXtrmg(1, i2 + 1);
		indexLattice = IDXtr(1, i2 + 1);
	}
	/* coordinates */
	double x2 = lattice[curIndex + spinsNum];
	double y2 = lattice[curIndex + spinsNum + 1];
	double z2 = lattice[curIndex + spinsNum + 2];
	
	/* down normal vector */
	double norm2D_x;
	double norm2D_y;
	double norm2D_z;
	if ((i2 < userVars.N2 - 2) && (i1 < userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm2D_x = latticeParam[indexLattice].trinxD;
		norm2D_y = latticeParam[indexLattice].trinyD;
		norm2D_z = latticeParam[indexLattice].trinzD;
	} else if ((i2 < userVars.N2 - 2) && (userVars.periodicBC1 == PBC_ON) ) {
		norm2D_x = latticeParam[indexLattice].trinxD;
		norm2D_y = latticeParam[indexLattice].trinyD;
		norm2D_z = latticeParam[indexLattice].trinzD;
	} else {
		norm2D_x = 0.0;
		norm2D_y = 0.0;
		norm2D_z = 0.0;
	}

	/* bending coefficients for down triangle
	double betaD2_1 = 0.0;
	double betaD2_2 = 0.0;*/
	double betaD2_3 = latticeParam[indexLattice].betaD3;
	if ((i2 == userVars.N2 - 2) || ((i1 == userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF)) ) {
		betaD2_3 = 0.0;
	} else if ((i1 == userVars.N1 - 3) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		betaD2_3 = 0.0;
	}
	
	/***************************
     * 3-rd neighbouring site
    ***************************/
	if (i1 > 1) {
		curIndex = IDXtrmg3(i1, i2);
		indexLattice = IDXtr3(i1, i2);
	} else if ((i1 == 1) && IsODD(i2) ) {
		/* odd row */
		curIndex = IDXtrmg(userVars.N1 - 2, i2 + 1);
		indexLattice = IDXtr(userVars.N1 - 2, i2 + 1);
	} else {
		/* even row */
		curIndex = IDXtrmg3(i1, i2);
		indexLattice = IDXtr3(i1, i2);
	}
	/* coordinates */
	double x3 = lattice[curIndex + spinsNum];
	double y3 = lattice[curIndex + spinsNum + 1];
	double z3 = lattice[curIndex + spinsNum + 2];
	/* up normal vectors */
	double norm3U_x;
	double norm3U_y;
	double norm3U_z;
	if ((i2 < userVars.N2 - 3) && (i1 < userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm3U_x = latticeParam[indexLattice].trinxU;
		norm3U_y = latticeParam[indexLattice].trinyU;
		norm3U_z = latticeParam[indexLattice].trinzU;
	} else if ((i2 < userVars.N2 - 3) && (i1 == userVars.N1 - 2) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm3U_x = latticeParam[indexLattice].trinxU;
		norm3U_y = latticeParam[indexLattice].trinyU;
		norm3U_z = latticeParam[indexLattice].trinzU;
	} else if ((i2 < userVars.N2 - 3) && (userVars.periodicBC1 == PBC_ON) ) {
		norm3U_x = latticeParam[indexLattice].trinxU;
		norm3U_y = latticeParam[indexLattice].trinyU;
		norm3U_z = latticeParam[indexLattice].trinzU;
	} else {
		norm3U_x = 0.0;
		norm3U_y = 0.0;
		norm3U_z = 0.0;
	}
	
	/* down normal vector */
	double norm3D_x;
	double norm3D_y;
	double norm3D_z;
	int D3;
	if ((i2 < userVars.N2 - 2) && (i1 < userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm3D_x = latticeParam[indexLattice].trinxD;
		norm3D_y = latticeParam[indexLattice].trinyD;
		norm3D_z = latticeParam[indexLattice].trinzD;
		D3 = 1;
	} else if ((i2 < userVars.N2 - 2) && (i1 == userVars.N1 - 2) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm3D_x = latticeParam[indexLattice].trinxD;
		norm3D_y = latticeParam[indexLattice].trinyD;
		norm3D_z = latticeParam[indexLattice].trinzD;
		D3 = 1;
	} else if ((i2 < userVars.N2 - 2) && (userVars.periodicBC1 == PBC_ON)) {
		norm3D_x = latticeParam[indexLattice].trinxD;
		norm3D_y = latticeParam[indexLattice].trinyD;
		norm3D_z = latticeParam[indexLattice].trinzD;
		D3 = 1;
	} else {
		norm3D_x = 0.0;
		norm3D_y = 0.0;
		norm3D_z = 0.0;
		D3 = 0;
	}
	if ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm3D_x = 0.0;
		norm3D_y = 0.0;
		norm3D_z = 0.0;
		D3 = 0;
	}
	/* bending coefficients for up tringle
	double betaU3_1 = 0.0;
	double betaU3_2 = 0.0;*/
	double betaU3_3 = latticeParam[indexLattice].betaU3;
	if ((i2 > userVars.N2 - 3) || ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF)) || ((i1 == userVars.N1 - 2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF)) ) {
		betaU3_3 = 0.0;
	}
	
	/* bending coefficients for down tringle*/
	double betaD3_1 = latticeParam[indexLattice].betaD1;
	/* double betaD3_2 = 0.0; */
	double betaD3_3 = latticeParam[indexLattice].betaD3;
	if ((i2 == userVars.N2 - 2) || ((i1 == userVars.N1 - 2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF)) ||((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF))) {
		betaD3_1 = 0.0;
		betaD3_3 = 0.0;
	} else if ((i1 == userVars.N1 - 2) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF) ){
		betaD3_1 = 0.0;
	} else if ((i1 == 1) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF) ){
		betaD3_3 = 0.0;
	}	
	
	/***************************
     * 4-th neighbouring site
    ***************************/
	if (i1 > 1) {
		curIndex = IDXtrmg4(i1, i2);
		indexLattice = IDXtr4(i1, i2);
	} else {
		curIndex = IDXtrmg(userVars.N1 - 2, i2);
		indexLattice = IDXtr(userVars.N1 - 2, i2);
	}
	/* coordinates */
	double x4 = lattice[curIndex + spinsNum];
	double y4 = lattice[curIndex + spinsNum + 1];
	double z4 = lattice[curIndex + spinsNum + 2];
	
	/* up normal vectors */
	double norm4U_x;
	double norm4U_y;
	double norm4U_z;
	int U4;
	if ((i2 < userVars.N2 - 2) && (i1 > 1) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm4U_x = latticeParam[indexLattice].trinxU;
		norm4U_y = latticeParam[indexLattice].trinyU;
		norm4U_z = latticeParam[indexLattice].trinzU;
		U4 = 1;
	} else if ((i2 < userVars.N2 - 2) && (userVars.periodicBC1 == PBC_ON) ) {
		norm4U_x = latticeParam[indexLattice].trinxU;
		norm4U_y = latticeParam[indexLattice].trinyU;
		norm4U_z = latticeParam[indexLattice].trinzU;
		U4 = 1;
	} else {
		norm4U_x = 0.0;
		norm4U_y = 0.0;
		norm4U_z = 0.0;
		U4 = 0;
	}
	
	/* down normal vector */
	double norm4D_x;
	double norm4D_y;
	double norm4D_z;
	int D4;
	if ((i2 > 1) && (i1 > 1) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm4D_x = latticeParam[indexLattice].trinxD;
		norm4D_y = latticeParam[indexLattice].trinyD;
		norm4D_z = latticeParam[indexLattice].trinzD;
		D4 = 1;
	} else if ((i2 > 1) && (userVars.periodicBC1 == PBC_ON) ) {
		norm4D_x = latticeParam[indexLattice].trinxD;
		norm4D_y = latticeParam[indexLattice].trinyD;
		norm4D_z = latticeParam[indexLattice].trinzD;
		D4 = 1;
	} else {
		norm4D_x = 0.0;
		norm4D_y = 0.0;
		norm4D_z = 0.0;
		D4 = 0;
	}
	/* bending coefficients for up tringle
	double betaU4_1 = 0.0;
	double betaU4_2 = 0.0; */
	double betaU4_3 = latticeParam[indexLattice].betaU3;
	if ((i2 == 1) || (i2 == userVars.N2 - 2) || ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) ) {
		betaU4_3 = 0.0;
	}
	
	/* bending coefficients for down tringle*/
	double betaD4_1 = latticeParam[indexLattice].betaD1;
	/* double betaD4_2 = 0.0; */
	double betaD4_3 = latticeParam[indexLattice].betaD3;
	if ((i2 == 1) || ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF) )) {
		betaD4_1 = 0.0;
		betaD4_3 = 0.0;
	} else if ((i1 == userVars.N1 - 2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		betaD4_1 = 0.0;
	} else if ((i1 == 2) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		betaD4_3 = 0.0;
	}
	
	/***************************
     * 5-th neighbouring site
    ***************************/
	if (i1 > 1) {
		curIndex = IDXtrmg5(i1, i2);
		indexLattice = IDXtr5(i1, i2);
	} else if ((i1 == 1) && IsODD(i2) ) {
		/* odd row */
		curIndex = IDXtrmg(userVars.N1 - 2, i2 - 1);
		indexLattice = IDXtr(userVars.N1 - 2, i2 - 1);
	} else {
		/* even row */
		curIndex = IDXtrmg5(i1, i2);
		indexLattice = IDXtr5(i1, i2);
	}
	/* coordinates */
	double x5 = lattice[curIndex + spinsNum];
	double y5 = lattice[curIndex + spinsNum + 1];
	double z5 = lattice[curIndex + spinsNum + 2];
	
	/* up normal vectors */
	double norm5U_x;
	double norm5U_y;
	double norm5U_z;
	int U5;
	if ((i2 > 1) && (i1 < userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm5U_x = latticeParam[indexLattice].trinxU;
		norm5U_y = latticeParam[indexLattice].trinyU;
		norm5U_z = latticeParam[indexLattice].trinzU;
		U5 = 1;
	} else if ((i2 >1) && (i1 == userVars.N1 - 2) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm5U_x = latticeParam[indexLattice].trinxU;
		norm5U_y = latticeParam[indexLattice].trinyU;
		norm5U_z = latticeParam[indexLattice].trinzU;
		U5 = 1;
	} else if ((i2 >1) && (userVars.periodicBC1 == PBC_ON) ) {
		norm5U_x = latticeParam[indexLattice].trinxU;
		norm5U_y = latticeParam[indexLattice].trinyU;
		norm5U_z = latticeParam[indexLattice].trinzU;
		U5 = 1;
	} else {
		norm5U_x = 0.0;
		norm5U_y = 0.0;
		norm5U_z = 0.0;
		U5 = 0;
	}
	if ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm5U_x = 0.0;
		norm5U_y = 0.0;
		norm5U_z = 0.0;
		U5 = 0;
	}
	
	/* down normal vector */
	double norm5D_x;
	double norm5D_y;
	double norm5D_z;
	if ((i2 > 2) && (i1 < userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm5D_x = latticeParam[indexLattice].trinxD;
		norm5D_y = latticeParam[indexLattice].trinyD;
		norm5D_z = latticeParam[indexLattice].trinzD;
	} else if ((i2 > 2) && (i1 == userVars.N1 - 2) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm5D_x = latticeParam[indexLattice].trinxD;
		norm5D_y = latticeParam[indexLattice].trinyD;
		norm5D_z = latticeParam[indexLattice].trinzD;
	} else if ((i2 > 2) && (userVars.periodicBC1 == PBC_ON) ) {
		norm5D_x = latticeParam[indexLattice].trinxD;
		norm5D_y = latticeParam[indexLattice].trinyD;
		norm5D_z = latticeParam[indexLattice].trinzD;
	} else {
		norm5D_x = 0.0;
		norm5D_y = 0.0;
		norm5D_z = 0.0;
	}
	/* bending coefficients for up tringle
	double betaU5_1 = 0.0;
	double betaU5_2 = 0.0;*/
	double betaU5_3 = latticeParam[indexLattice].betaU3;
	if ((i2 < 2) || ((i1 == 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF)) || ((i1 == userVars.N1 - 2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF)) ) {
		betaU5_3 = 0.0;
	}
	
	/* bending coefficients for down tringle
	double betaD5_1 = 0.0;
	double betaD5_2 = 0.0;
	double betaD5_3 = 0.0;
	*/
	
	/***************************
     * 6-th neighbouring site
    ***************************/
	if (i1 < userVars.N1 - 2) {
		curIndex = IDXtrmg6(i1, i2);
		indexLattice = IDXtr6(i1, i2);
	} else if ((i1 == userVars.N1 - 2) && IsODD(i2) ) {
		/* odd row */
		curIndex = IDXtrmg6(i1, i2);
		indexLattice = IDXtr6(i1, i2);
	} else {
		/* even row */
		curIndex = IDXtrmg(1, i2 - 1);
		indexLattice = IDXtr(1, i2 - 1);
	}
	/* coordinates */
	double x6 = lattice[curIndex + spinsNum];
	double y6 = lattice[curIndex + spinsNum + 1];
	double z6 = lattice[curIndex + spinsNum + 2];
	
	/* up normal vectors */
	double norm6U_x;
	double norm6U_y;
	double norm6U_z;
	if ((i2 > 1) && (i1 < userVars.N1 - 2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm6U_x = latticeParam[indexLattice].trinxU;
		norm6U_y = latticeParam[indexLattice].trinyU;
		norm6U_z = latticeParam[indexLattice].trinzU;
	} else if ((i2 >1) && (userVars.periodicBC1 == PBC_ON) ) {
		norm6U_x = latticeParam[indexLattice].trinxU;
		norm6U_y = latticeParam[indexLattice].trinyU;
		norm6U_z = latticeParam[indexLattice].trinzU;
	} else {
		norm6U_x = 0.0;
		norm6U_y = 0.0;
		norm6U_z = 0.0;
	}
	
	/* bending coefficients for up tringle
	double betaU6_1 = 0.0;
	double betaU6_2 = 0.0;
	double betaU6_3 = 0.0;
	*/
		
	/***************************
     * 7-th neighbouring site == 4-3
    ***************************/
	double norm7D_x;
	double norm7D_y;
	double norm7D_z;
	if ((i2 == userVars.N2 - 2) || ((i1 == 1) && (userVars.periodicBC1 == PBC_OFF)) ) {
		norm7D_x = 0.0;
		norm7D_y = 0.0;
		norm7D_z = 0.0;
	}  
	if ((i1 == 1) && (i2 < userVars.N2 - 2) && (userVars.periodicBC1 == PBC_ON) ) {
		if (IsODD(i2)) {
			indexLattice = IDXtr(userVars.N1 - 3, i2 + 1);
		} else {
			indexLattice = IDXtr(userVars.N1 - 2, i2 + 1);
		}
		norm7D_x = latticeParam[indexLattice].trinxD;
		norm7D_y = latticeParam[indexLattice].trinyD;
		norm7D_z = latticeParam[indexLattice].trinzD;
	}
	if ((i1 == 2) && (i2 < userVars.N2 - 2) && IsODD(i2) && (userVars.periodicBC1 == PBC_ON) ) {
		indexLattice = IDXtr(userVars.N1 - 2, i2 + 1);
		norm7D_x = latticeParam[indexLattice].trinxD;
		norm7D_y = latticeParam[indexLattice].trinyD;
		norm7D_z = latticeParam[indexLattice].trinzD;
	}
	if ((i1 == 2) && (i2 < userVars.N2 - 2) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm7D_x = 0.0;
		norm7D_y = 0.0;
		norm7D_z = 0.0;
	} else if ((i1 == 2) && (i2 < userVars.N2 - 2) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		indexLattice = IDXtr(1, i2 + 1);
		norm7D_x = latticeParam[indexLattice].trinxD;
		norm7D_y = latticeParam[indexLattice].trinyD;
		norm7D_z = latticeParam[indexLattice].trinzD;
	}
	if ((i1 > 2) && (i2 < userVars.N2 - 2)) {
		if (IsODD(i2)) {
			indexLattice = IDXtr(i1 - 2, i2 + 1);
		} else {
			indexLattice = IDXtr(i1 - 1, i2 + 1);
		}
		norm7D_x = latticeParam[indexLattice].trinxD;
		norm7D_y = latticeParam[indexLattice].trinyD;
		norm7D_z = latticeParam[indexLattice].trinzD;
	}	
	
	/* bending coefficients for down tringle */
	double betaD7_1 = latticeParam[indexLattice].betaD1;
	/* 
	double betaD7_2 = 0.0;
	double betaD7_3 = 0.0;
	*/
	
	/***************************
     * 8-th neighbouring site == 5-4
    ***************************/
	double norm8U_x;
	double norm8U_y;
	double norm8U_z;
	if ((i2 == 1) || ((i1 == 1) && (i2 > 1) && (userVars.periodicBC1 == PBC_OFF) ) ) {
		norm8U_x = 0.0;
		norm8U_y = 0.0;
		norm8U_z = 0.0;
	}
	if ((i1 == 1) && (i2 > 1) && (userVars.periodicBC1 == PBC_ON) ) {
		if (IsODD(i2)) {
			indexLattice = IDXtr(userVars.N1 - 3, i2 - 1);
		} else {
			indexLattice = IDXtr(userVars.N1 - 2, i2 - 1);
		}
		norm8U_x = latticeParam[indexLattice].trinxU;
		norm8U_y = latticeParam[indexLattice].trinyU;
		norm8U_z = latticeParam[indexLattice].trinzU;
	}
	if ((i1 == 2) && (i2 > 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_ON) ) {
		indexLattice = IDXtr(userVars.N1 - 2, i2 - 1);
		norm8U_x = latticeParam[indexLattice].trinxU;
		norm8U_y = latticeParam[indexLattice].trinyU;
		norm8U_z = latticeParam[indexLattice].trinzU;
	}
	if ((i1 == 2) && (i2 > 1) && IsODD(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		norm8U_x = 0.0;
		norm8U_y = 0.0;
		norm8U_z = 0.0;
	} else if ((i1 == 2) && (i2 > 1) && IsEVEN(i2) && (userVars.periodicBC1 == PBC_OFF) ) {
		indexLattice = IDXtr(1, i2 - 1);
		norm8U_x = latticeParam[indexLattice].trinxU;
		norm8U_y = latticeParam[indexLattice].trinyU;
		norm8U_z = latticeParam[indexLattice].trinzU;
	}
	if ((i1 > 2) && (i2 > 1)) {
		if (IsODD(i2)) {
			indexLattice = IDXtr(i1 - 2, i2 - 1);
		} else {
			indexLattice = IDXtr(i1 - 1, i2 - 1);
		}
		norm8U_x = latticeParam[indexLattice].trinxU;
		norm8U_y = latticeParam[indexLattice].trinyU;
		norm8U_z = latticeParam[indexLattice].trinzU;
	}
	
	/* bending coefficients for up tringle
	double betaU8_1 = 0.0;
	double betaU8_2 = 0.0;
	double betaU8_3 = 0.0;
	*/
	
	/***************************
     * Derivatives calculatio for i-th up and down triangles
    ***************************/
    
	/* derivative of up triangle with respect to xi */
	double DnormiU_xDx;
	double DnormiU_yDx;
	double DnormiU_zDx;
	/* derivative of down triangle with respect to xi */
	double DnormiD_xDx;
	double DnormiD_yDx;
	double DnormiD_zDx;
	/* derivative of up triangle with respect to yi */
	double DnormiU_xDy;
	double DnormiU_yDy;
	double DnormiU_zDy;
	/* derivative of down triangle with respect to yi */
	double DnormiD_xDy;
	double DnormiD_yDy;
	double DnormiD_zDy;
	/* derivative of up triangle with respect to zi */
	double DnormiU_xDz;
	double DnormiU_yDz;
	double DnormiU_zDz;
	/* derivative of down triangle with respect to zi */
	double DnormiD_xDz;
	double DnormiD_yDz;
	double DnormiD_zDz;
    
	/* distance r1 - ri*/
	double xi1;
	double yi1;
	double zi1;
	/* distance r2 - ri*/
	double xi2;
	double yi2;
	double zi2;
	/* distance r6 - ri*/
	double xi6;
	double yi6;
	double zi6;
	
	/* cross-product [r1 -ri, r2 - ri]*/
	double crossi12_x;
	double crossi12_y;
	double crossi12_z;
	/* cross-product [r6 -ri, r1 - ri]*/
	double crossi61_x;
	double crossi61_y;
	double crossi61_z;
				
	/* derivative normalization of up triangle*/
	double normUi;
	double normUi2;
	double normUi3;
	/* derivative normalization of down triangle*/
	double normDi;
	double normDi2;
	double normDi3;		
    
	/* i-th up triangle */
	if (Ui == 0) {
		/* derivative with respect to xi */
		DnormiU_xDx = 0.0;
		DnormiU_yDx = 0.0;
		DnormiU_zDx = 0.0;
		/* derivative with respect to yi */
		DnormiU_xDy = 0.0;
		DnormiU_yDy = 0.0;
		DnormiU_zDy = 0.0;
		/* derivative with respect to zi */
		DnormiU_xDz = 0.0;
		DnormiU_yDz = 0.0;
		DnormiU_zDz = 0.0;
	} else {
		/* distance */
		xi1 = x1 - xi;
		yi1 = y1 - yi;
		zi1 = z1 - zi;
		xi2 = x2 - xi;
		yi2 = y2 - yi;
		zi2 = z2 - zi;
		
		/* cross-product [r1 -ri, r2 - ri]*/
		crossi12_x = yi1 * zi2 - zi1 * yi2;
		crossi12_y = zi1 * xi2 - xi1 * zi2;
		crossi12_z = xi1 * yi2 - yi1 * xi2;
		
		/* derivative normalization */
		normUi = sqrt(crossi12_x * crossi12_x + crossi12_y * crossi12_y + crossi12_z * crossi12_z);
		normUi2 = normUi * normUi;
		normUi3 = normUi * normUi * normUi;	
		
		/* derivative with respect to xi */
		DnormiU_xDx = - crossi12_x * (crossi12_y * (zi2 - zi1) + crossi12_z * (yi1 - yi2)) / normUi3;
		DnormiU_yDx = ((zi2 - zi1) * normUi2 - crossi12_y * (crossi12_y * (zi2 - zi1) + crossi12_z * (yi1 - yi2))) / normUi3;
		DnormiU_zDx = ((yi1 - yi2) * normUi2 - crossi12_z * (crossi12_y * (zi2 - zi1) + crossi12_z * (yi1 - yi2))) / normUi3;
		/* derivative with respect to yi */
		DnormiU_xDy = ((zi1 - zi2) * normUi2 - crossi12_x * (crossi12_x * (zi1 - zi2) + crossi12_z * (xi2 - xi1))) / normUi3;
		DnormiU_yDy = - crossi12_y * (crossi12_x * (zi1 - zi2) + crossi12_z * (xi2 - xi1)) / normUi3;
		DnormiU_zDy = ((xi2 - xi1) * normUi2 - crossi12_z * (crossi12_x * (zi1 - zi2) + crossi12_z * (xi2 - xi1))) / normUi3;
		/* derivative with respect to zi */
		DnormiU_xDz = ((yi2 - yi1) * normUi2 - crossi12_x * (crossi12_x * (yi2 - yi1) + crossi12_y * (xi1 - xi2))) / normUi3;
		DnormiU_yDz = ((xi1 - xi2) * normUi2 - crossi12_y * (crossi12_x * (yi2 - yi1) + crossi12_y * (xi1 - xi2))) / normUi3;
		DnormiU_zDz = - crossi12_z * (crossi12_x * (yi2 - yi1) + crossi12_y * (xi1 - xi2)) / normUi3;
	}

	/* i-th down triangle */
	if (Di == 0) {
		/* derivative with respect to xi */
		DnormiD_xDx = 0.0;
		DnormiD_yDx = 0.0;
		DnormiD_zDx = 0.0;
		
		/* derivative with respect to yi */
		DnormiD_xDy = 0.0;
		DnormiD_yDy = 0.0;
		DnormiD_zDy = 0.0;
		
		/* derivative with respect to zi */
		DnormiD_xDz = 0.0;
		DnormiD_yDz = 0.0;
		DnormiD_zDz = 0.0;
	} else {
		/* distance */
		xi1 = x1 - xi;
		yi1 = y1 - yi;
		zi1 = z1 - zi;
		xi6 = x6 - xi;
		yi6 = y6 - yi;
		zi6 = z6 - zi;
		
		/* cross-product [r6 -ri, r1 - ri]*/
		crossi61_x = yi6 * zi1 - zi6 * yi1;
		crossi61_y = zi6 * xi1 - xi6 * zi1;
		crossi61_z = xi6 * yi1 - yi6 * xi1;
		
		/* derivative normalization */
		normDi = sqrt(crossi61_x * crossi61_x + crossi61_y * crossi61_y + crossi61_z * crossi61_z);		
		normDi2 = normDi * normDi;
		normDi3 = normDi * normDi * normDi;	
		
		/* derivative with respect to xi */
		DnormiD_xDx = - crossi61_x * (crossi61_y * (zi1 - zi6) + crossi61_z * (yi6 - yi1)) / normDi3;
		DnormiD_yDx = ((zi1 - zi6) * normDi2 - crossi61_y * (crossi61_y * (zi1 - zi6) + crossi61_z * (yi6 - yi1))) / normDi3;
		DnormiD_zDx = ((yi6 - yi1) * normDi2 - crossi61_z * (crossi61_y * (zi1 - zi6) + crossi61_z * (yi6 - yi1))) / normDi3;
		/* derivative with respect to yi */
		DnormiD_xDy = ((zi6 - zi1) * normDi2 - crossi61_x * (crossi61_x * (zi6 - zi1) + crossi61_z * (xi1 - xi6))) / normDi3;
		DnormiD_yDy = - crossi61_y * (crossi61_x * (zi6 - zi1) + crossi61_z * (xi1 - xi6)) / normDi3;
		DnormiD_zDy = ((xi1 - xi6) * normDi2 - crossi61_z * (crossi61_x * (zi6 - zi1) + crossi61_z * (xi1 - xi6))) / normDi3;
		/* derivative with respect to zi */
		DnormiD_xDz = ((yi1 - yi6) * normDi2 - crossi61_x * (crossi61_x * (yi1 - yi6) + crossi61_y * (xi6 - xi1))) / normDi3;
		DnormiD_yDz = ((xi6 - xi1) * normDi2 - crossi61_y * (crossi61_x * (yi1 - yi6) + crossi61_y * (xi6 - xi1))) / normDi3;
		DnormiD_zDz = - crossi61_z * (crossi61_x * (yi1 - yi6) + crossi61_y * (xi6 - xi1)) / normDi3;
	}

	/***************************
     * Derivatives calculatio for 2-nd down triangle
    ***************************/
	
	/* derivative with respect to xi */
	double Dnorm2D_xDx = 0.0;
	double Dnorm2D_yDx = 0.0;
	double Dnorm2D_zDx = 0.0;
	/* derivative with respect to yi */
	double Dnorm2D_xDy = 0.0;
	double Dnorm2D_yDy = 0.0;
	double Dnorm2D_zDy = 0.0;
	/* derivative with respect to zi */
	double Dnorm2D_xDz = 0.0;
	double Dnorm2D_yDz = 0.0;
	double Dnorm2D_zDz = 0.0;	
	
	/***************************
     * Derivatives calculatio for 3-rd up and down triangles
    ***************************/
	
	/* derivative of up triangle with respect to xi */
	double Dnorm3U_xDx = 0.0;
	double Dnorm3U_yDx = 0.0;
	double Dnorm3U_zDx = 0.0;
	/* derivative of down triangle with respect to xi */
	double Dnorm3D_xDx;
	double Dnorm3D_yDx;
	double Dnorm3D_zDx;
	/* derivative of up triangle with respect to yi */
	double Dnorm3U_xDy = 0.0;
	double Dnorm3U_yDy = 0.0;
	double Dnorm3U_zDy = 0.0;
	/* derivative of down triangle with respect to yi */
	double Dnorm3D_xDy;
	double Dnorm3D_yDy;
	double Dnorm3D_zDy;
	/* derivative of up triangle with respect to zi */
	double Dnorm3U_xDz = 0.0;
	double Dnorm3U_yDz = 0.0;
	double Dnorm3U_zDz = 0.0;
	/* derivative of down triangle with respect to zi */
	double Dnorm3D_xDz;
	double Dnorm3D_yDz;
	double Dnorm3D_zDz;
    
	/* distance ri - r3 */
	double x3i;
	double y3i;
	double z3i;
	/* distance r2 - r3 */
	double x32;
	double y32;
	double z32;
	
	/* cross-product [ri -r3, r2 - r3]*/
	double cross3i2_x;
	double cross3i2_y;
	double cross3i2_z;
	
	/* derivative normalization */
	double normD3;
	double normD32;
	double normD33;		
	
	/* 3-rd down triangle */
	if (D3 == 0) {
		/* derivative with respect to xi */
		Dnorm3D_xDx = 0.0;
		Dnorm3D_yDx = 0.0;
		Dnorm3D_zDx = 0.0;
		/* derivative with respect to yi */
		Dnorm3D_xDy = 0.0;
		Dnorm3D_yDy = 0.0;
		Dnorm3D_zDy = 0.0;
		/* derivative with respect to zi */
		Dnorm3D_xDz = 0.0;
		Dnorm3D_yDz = 0.0;
		Dnorm3D_zDz = 0.0;
	} else {
		/* distance */
		x3i = xi - x3;
		y3i = yi - y3;
		z3i = zi - z3;
		x32 = x2 - x3;
		y32 = y2 - y3;
		z32 = z2 - z3;
		
		/* cross-product [ri -r3, r2 - r3]*/
		cross3i2_x = y3i * z32 - z3i * y32;
		cross3i2_y = z3i * x32 - x3i * z32;
		cross3i2_z = x3i * y32 - y3i * x32;
		
		/* derivative normalization */
		normD3 = sqrt(cross3i2_x * cross3i2_x + cross3i2_y * cross3i2_y + cross3i2_z * cross3i2_z);		
		normD32 = normD3 * normD3;
		normD33 = normD3 * normD3 * normD3;
		
		/* derivative with respect to xi */
		Dnorm3D_xDx = - cross3i2_x * (y32 * cross3i2_z - z32 * cross3i2_y) / normD33;
		Dnorm3D_yDx = - (z32 * normD32 + cross3i2_y * (y32 * cross3i2_z - z32 * cross3i2_y)) / normD33;
		Dnorm3D_zDx = + (y32 * normD32 - cross3i2_z * (y32 * cross3i2_z - z32 * cross3i2_y)) / normD33;
		/* derivative with respect to yi */
		Dnorm3D_xDy = + (z32 * normD32 - cross3i2_x * (z32 * cross3i2_x - x32 * cross3i2_z)) / normD33;
		Dnorm3D_yDy = - cross3i2_y * (z32 * cross3i2_x - x32 * cross3i2_z) / normD33;
		Dnorm3D_zDy = - (x32 * normD32 + cross3i2_z * (z32 * cross3i2_x - x32 * cross3i2_z)) / normD33;
		/* derivative with respect to zi */
		Dnorm3D_xDz = - (y32 * normD32 + cross3i2_x * (x32 * cross3i2_y - y32 * cross3i2_x)) / normD33;
		Dnorm3D_yDz = + (x32 * normD32 - cross3i2_y * (x32 * cross3i2_y - y32 * cross3i2_x)) / normD33;
		Dnorm3D_zDz = - cross3i2_z * (x32 * cross3i2_y - y32 * cross3i2_x) / normD33;
	}
	
	/***************************
     * Derivatives calculation for 4-th up and down triangles
    ***************************/
	
	/* derivative of up triangle with respect to xi */
	double Dnorm4U_xDx;
	double Dnorm4U_yDx;
	double Dnorm4U_zDx;
	/* derivative of down triangle with respect to xi */
	double Dnorm4D_xDx;
	double Dnorm4D_yDx;
	double Dnorm4D_zDx;
	/* derivative of up triangle with respect to yi */
	double Dnorm4U_xDy;
	double Dnorm4U_yDy;
	double Dnorm4U_zDy;
	/* derivative of down triangle with respect to yi */
	double Dnorm4D_xDy;
	double Dnorm4D_yDy;
	double Dnorm4D_zDy;
	/* derivative of up triangle with respect to zi */
	double Dnorm4U_xDz;
	double Dnorm4U_yDz;
	double Dnorm4U_zDz;
	/* derivative of down triangle with respect to zi */
	double Dnorm4D_xDz;
	double Dnorm4D_yDz;
	double Dnorm4D_zDz;
    
	/* distance ri - r4*/
	double x4i;
	double y4i;
	double z4i;
	/* distance r3 - r4*/
	double x43;
	double y43;
	double z43;
	/* distance r5 - r4*/
	double x45;
	double y45;
	double z45;
	
	/* cross-product [ri -r4, r3 - r4]*/
	double cross4i3_x;
	double cross4i3_y;
	double cross4i3_z;
	/* cross-product [r5 -r4, ri - r4]*/
	double cross45i_x;
	double cross45i_y;
	double cross45i_z;
	
	/* derivative normalization of up triangle*/
	double normU4;
	double normU42;
	double normU43;	
	/* derivative normalization of down triangle*/
	double normD4;
	double normD42;
	double normD43;
		
	/* 4-th up triangle */
	if (U4 == 0) {
		/* derivative with respect to xi */
		Dnorm4U_xDx = 0.0;
		Dnorm4U_yDx = 0.0;
		Dnorm4U_zDx = 0.0;
		/* derivative with respect to yi */
		Dnorm4U_xDy = 0.0;
		Dnorm4U_yDy = 0.0;
		Dnorm4U_zDy = 0.0;
		/* derivative with respect to zi */
		Dnorm4U_xDz = 0.0;
		Dnorm4U_yDz = 0.0;
		Dnorm4U_zDz = 0.0;
	} else {
		/* distance */
		x4i = xi - x4;
		y4i = yi - y4;
		z4i = zi - z4;
		x43 = x3 - x4;
		y43 = y3 - y4;
		z43 = z3 - z4;
		
		/* cross-product [ri -r4, r3 - r4]*/
		cross4i3_x = y4i * z43 - z4i * y43;
		cross4i3_y = z4i * x43 - x4i * z43;
		cross4i3_z = x4i * y43 - y4i * x43;
		
		/* derivative normalization */
		normU4 = sqrt(cross4i3_x * cross4i3_x + cross4i3_y * cross4i3_y + cross4i3_z * cross4i3_z);		
		normU42 = normU4 * normU4;
		normU43 = normU4 * normU4 * normU4;
		
		/* derivative with respect to xi */
		Dnorm4U_xDx = - cross4i3_x * (y43 * cross4i3_z - z43 * cross4i3_y) / normU43;
		Dnorm4U_yDx = - (z43 * normU42 + cross4i3_y * (y43 * cross4i3_z - z43 * cross4i3_y)) / normU43;
		Dnorm4U_zDx = + (y43 * normU42 - cross4i3_z * (y43 * cross4i3_z - z43 * cross4i3_y)) / normU43;
		/* derivative with respect to yi */
		Dnorm4U_xDy = + (z43 * normU42 - cross4i3_x * (z43 * cross4i3_x - x43 * cross4i3_z)) / normU43;
		Dnorm4U_yDy = - cross4i3_y * (z43 * cross4i3_x - x43 * cross4i3_z) / normU43;
		Dnorm4U_zDy = - (x43 * normU42 + cross4i3_z * (z43 * cross4i3_x - x43 * cross4i3_z)) / normU43;
		/* derivative with respect to zi */
		Dnorm4U_xDz = - (y43 * normU42 + cross4i3_x * (x43 * cross4i3_y - y43 * cross4i3_x)) / normU43;
		Dnorm4U_yDz = + (x43 * normU42 - cross4i3_y * (x43 * cross4i3_y - y43 * cross4i3_x)) / normU43;
		Dnorm4U_zDz = - cross4i3_z * (x43 * cross4i3_y - y43 * cross4i3_x) / normU43;
	}
	
	/* 4-th down triangle */
	if (D4 == 0) {
		/* derivative with respect to xi */
		Dnorm4D_xDx = 0.0;
		Dnorm4D_yDx = 0.0;
		Dnorm4D_zDx = 0.0;
		/* derivative with respect to yi */
		Dnorm4D_xDy = 0.0;
		Dnorm4D_yDy = 0.0;
		Dnorm4D_zDy = 0.0;
		/* derivative with respect to zi */
		Dnorm4D_xDz = 0.0;
		Dnorm4D_yDz = 0.0;
		Dnorm4D_zDz = 0.0;
	} else {
		/* distance */
		x4i = xi - x4;
		y4i = yi - y4;
		z4i = zi - z4;
		x45 = x5 - x4;
		y45 = y5 - y4;
		z45 = z5 - z4;
		
		/* cross-product [r5 -r4, ri - r4]*/
		cross45i_x = y45 * z4i - z45 * y4i;
		cross45i_y = z45 * x4i - x45 * z4i;
		cross45i_z = x45 * y4i - y45 * x4i;
		
		/* derivative normalization */
		normD4 = sqrt(cross45i_x * cross45i_x + cross45i_y * cross45i_y + cross45i_z * cross45i_z);		
		normD42 = normD4 * normD4;
		normD43 = normD4 * normD4 * normD4;
		
		/* derivative with respect to xi */
		Dnorm4D_xDx = - cross45i_x * (z45 * cross45i_y - y45 * cross45i_z) / normD43;
		Dnorm4D_yDx = + (z45 * normD42 - cross45i_y * (z45 * cross45i_y - y45 * cross45i_z)) / normD43;
		Dnorm4D_zDx = - (y45 * normD42 + cross45i_z * (z45 * cross45i_y - y45 * cross45i_z)) / normD43;
		/* derivative with respect to yi */
		Dnorm4D_xDy = - (z45 * normD42 + cross45i_x * (x45 * cross45i_z - z45 * cross45i_x)) / normD43;
		Dnorm4D_yDy = - cross45i_y * (x45 * cross45i_z - z45 * cross45i_x) / normD43;
		Dnorm4D_zDy = + (x45 * normD42 - cross45i_z * (x45 * cross45i_z - z45 * cross45i_x)) / normD43;;
		/* derivative with respect to zi */
		Dnorm4D_xDz = + (y45 * normD42 - cross45i_x * (y45 * cross45i_x - x45 * cross45i_y)) / normD43;
		Dnorm4D_yDz = - (x45 * normD42 + cross45i_y * (y45 * cross45i_x - x45 * cross45i_y)) / normD43;
		Dnorm4D_zDz = - cross45i_z * (y45 * cross45i_x - x45 * cross45i_y) / normD43;
	}
	
	/***************************
     * Derivatives calculation for 5-th up and down triangles
    ***************************/
	
	/* derivative of up triangle with respect to xi */
	double Dnorm5U_xDx;
	double Dnorm5U_yDx;
	double Dnorm5U_zDx;
	/* derivative of down triangle with respect to xi */
	double Dnorm5D_xDx = 0.0;
	double Dnorm5D_yDx = 0.0;
	double Dnorm5D_zDx = 0.0;
	/* derivative of up triangle with respect to yi */
	double Dnorm5U_xDy;
	double Dnorm5U_yDy;
	double Dnorm5U_zDy;
	/* derivative of down triangle with respect to yi */
	double Dnorm5D_xDy = 0.0;
	double Dnorm5D_yDy = 0.0;
	double Dnorm5D_zDy = 0.0;
	/* derivative of up triangle with respect to zi */
	double Dnorm5U_xDz;
	double Dnorm5U_yDz;
	double Dnorm5U_zDz;
	/* derivative of down triangle with respect to zi */
	double Dnorm5D_xDz = 0.0;
	double Dnorm5D_yDz = 0.0;
	double Dnorm5D_zDz = 0.0;
    
	/* distance ri - r5 */
	double x5i;
	double y5i;
	double z5i;
	/* distance r6 - r5 */
	double x56;
	double y56;
	double z56;
	
	/* cross-product [r6 -r5, ri - r5]*/
	double cross56i_x;
	double cross56i_y;
	double cross56i_z;
	
	/* derivative normalization */
	double normU5;
	double normU52;
	double normU53;
	
	/* 5-th up triangle */
	if (U5 == 0) {
		/* derivative with respect to xi */
		Dnorm5U_xDx = 0.0;
		Dnorm5U_yDx = 0.0;
		Dnorm5U_zDx = 0.0;
		/* derivative with respect to yi */
		Dnorm5U_xDy = 0.0;
		Dnorm5U_yDy = 0.0;
		Dnorm5U_zDy = 0.0;
		/* derivative with respect to zi */
		Dnorm5U_xDz = 0.0;
		Dnorm5U_yDz = 0.0;
		Dnorm5U_zDz = 0.0;
	} else {
		/* distance */
		x5i = xi - x5;
		y5i = yi - y5;
		z5i = zi - z5;
		x56 = x6 - x5;
		y56 = y6 - y5;
		z56 = z6 - z5;
		
		/* cross-product [r6 -r5, ri - r5]*/
		cross56i_x = y5i * z56 - z5i * y56;
		cross56i_y = z5i * x56 - x5i * z56;
		cross56i_z = x5i * y56 - y5i * x56;
		
		/* derivative normalization */
		normU5 = sqrt(cross56i_x * cross56i_x + cross56i_y * cross56i_y + cross56i_z * cross56i_z);		
		normU52 = normU5 * normU5;
		normU53 = normU5 * normU5 * normU5;
		
		/* derivative with respect to xi */
		Dnorm5U_xDx = + cross56i_x * (y56 * cross56i_z - z56 * cross56i_y) / normU53;
		Dnorm5U_yDx = + (z56 * normU52 + cross56i_y * (y56 * cross56i_z - z56 * cross56i_y)) / normU53;
		Dnorm5U_zDx = - (y56 * normU52 - cross56i_z * (y56 * cross56i_z - z56 * cross56i_y)) / normU53;
		/* derivative with respect to yi */
		Dnorm5U_xDy = - (z56 * normU52 - cross56i_x * (z56 * cross56i_x - x56 * cross56i_z)) / normU53;
		Dnorm5U_yDy = + cross56i_y * (z56 * cross56i_x - x56 * cross56i_z) / normU53;
		Dnorm5U_zDy = + (x56 * normU52 + cross56i_z * (z56 * cross56i_x - x56 * cross56i_z)) / normU53;
		/* derivative with respect to zi */
		Dnorm5U_xDz = + (y56 * normU52 + cross56i_x * (x56 * cross56i_y - y56 * cross56i_x)) / normU53;
		Dnorm5U_yDz = - (x56 * normU52 - cross56i_y * (x56 * cross56i_y - y56 * cross56i_x)) / normU53;
		Dnorm5U_zDz = + cross56i_z * (x56 * cross56i_y - y56 * cross56i_x) / normU53;
	}
	
	/***************************
     * Derivatives calculatio for 6-th up triangle
    ***************************/
	
	/* derivative with respect to xi */
	double Dnorm6U_xDx = 0.0;
	double Dnorm6U_yDx = 0.0;
	double Dnorm6U_zDx = 0.0;
	/* derivative with respect to yi */
	double Dnorm6U_xDy = 0.0;
	double Dnorm6U_yDy = 0.0;
	double Dnorm6U_zDy = 0.0;
	/* derivative with respect to zi */
	double Dnorm6U_xDz = 0.0;
	double Dnorm6U_yDz = 0.0;
	double Dnorm6U_zDz = 0.0;
	
	/***************************
     * Derivatives calculatio for 7-th down triangle
    ***************************/
    
	/* derivative with respect to xi */
	double Dnorm7D_xDx = 0.0;
	double Dnorm7D_yDx = 0.0;
	double Dnorm7D_zDx = 0.0;
	/* derivative with respect to yi */
	double Dnorm7D_xDy = 0.0;
	double Dnorm7D_yDy = 0.0;
	double Dnorm7D_zDy = 0.0;
	/* derivative with respect to zi */
	double Dnorm7D_xDz = 0.0;
	double Dnorm7D_yDz = 0.0;
	double Dnorm7D_zDz = 0.0;
	
	/***************************
     * Derivatives calculatio for 8-th up triangle
    ***************************/
	
	/* derivative with respect to xi */
	double Dnorm8U_xDx = 0.0;
	double Dnorm8U_yDx = 0.0;
	double Dnorm8U_zDx = 0.0;
	/* derivative with respect to yi */
	double Dnorm8U_xDy = 0.0;
	double Dnorm8U_yDy = 0.0;
	double Dnorm8U_zDy = 0.0;
	/* derivative with respect to zi */
	double Dnorm8U_xDz = 0.0;
	double Dnorm8U_yDz = 0.0;
	double Dnorm8U_zDz = 0.0;
	
	/***************************
     * Energy calculation
    ***************************/
	*Ebend -= 0.5 * ( betaUi_3 * (normiU_x * normiD_x + normiU_y * normiD_y + normiU_z * normiD_z) +
	                  betaU3_3 * (norm3U_x * norm3D_x + norm3U_y * norm3D_y + norm3U_z * norm3D_z) +
	                  betaU4_3 * (norm4U_x * norm4D_x + norm4U_y * norm4D_y + norm4U_z * norm4D_z) +
	                  betaU5_3 * (norm5U_x * norm5D_x + norm5U_y * norm5D_y + norm5U_z * norm5D_z) +
	                  betaD7_1 * (norm4U_x * norm7D_x + norm4U_y * norm7D_y + norm4U_z * norm7D_z) +
	                  betaD3_1 * (normiU_x * norm3D_x + normiU_y * norm3D_y + normiU_z * norm3D_z) +
	                  betaD4_1 * (norm5U_x * norm4D_x + norm5U_y * norm4D_y + norm5U_z * norm4D_z) +
	                  betaDi_1 * (norm6U_x * normiD_x + norm6U_y * normiD_y + norm6U_z * normiD_z) +
	                  betaD3_3 * (norm4U_x * norm3D_x + norm4U_y * norm3D_y + norm4U_z * norm3D_z) +
	                  betaD2_3 * (normiU_x * norm2D_x + normiU_y * norm2D_y + normiU_z * norm2D_z) +
	                  betaDi_3 * (norm5U_x * normiD_x + norm5U_y * normiD_y + norm5U_z * normiD_z) +
	                  betaD4_3 * (norm8U_x * norm4D_x + norm8U_y * norm4D_y + norm8U_z * norm4D_z)
	                );
	
	/***************************
     * Force calculation
    ***************************/
    
	*Fx += betaUi_3 * (DnormiU_xDx * normiD_x + DnormiU_yDx * normiD_y + DnormiU_zDx * normiD_z +
	                   DnormiD_xDx * normiU_x + DnormiD_yDx * normiU_y + DnormiD_zDx * normiU_z ) +
	       betaU3_3 * (Dnorm3U_xDx * norm3D_x + Dnorm3U_yDx * norm3D_y + Dnorm3U_zDx * norm3D_z +
	                   Dnorm3D_xDx * norm3U_x + Dnorm3D_yDx * norm3U_y + Dnorm3D_zDx * norm3U_z ) +
	       betaU4_3 * (Dnorm4U_xDx * norm4D_x + Dnorm4U_yDx * norm4D_y + Dnorm4U_zDx * norm4D_z +
	                   Dnorm4D_xDx * norm4U_x + Dnorm4D_yDx * norm4U_y + Dnorm4D_zDx * norm4U_z ) +
	       betaU5_3 * (Dnorm5U_xDx * norm5D_x + Dnorm5U_yDx * norm5D_y + Dnorm5U_zDx * norm5D_z +
	                   Dnorm5D_xDx * norm5U_x + Dnorm5D_yDx * norm5U_y + Dnorm5D_zDx * norm5U_z ) +
	       betaD7_1 * (Dnorm4U_xDx * norm7D_x + Dnorm4U_yDx * norm7D_y + Dnorm4U_zDx * norm7D_z +
	                   Dnorm7D_xDx * norm4U_x + Dnorm7D_yDx * norm4U_y + Dnorm7D_zDx * norm4U_z ) +
	       betaD3_1 * (DnormiU_xDx * norm3D_x + DnormiU_yDx * norm3D_y + DnormiU_zDx * norm3D_z +
	                   Dnorm3D_xDx * normiU_x + Dnorm3D_yDx * normiU_y + Dnorm3D_zDx * normiU_z ) +
	       betaD4_1 * (Dnorm5U_xDx * norm4D_x + Dnorm5U_yDx * norm4D_y + Dnorm5U_zDx * norm4D_z + 
	                   Dnorm4D_xDx * norm5U_x + Dnorm4D_yDx * norm5U_y + Dnorm4D_zDx * norm5U_z ) +
	       betaDi_1 * (DnormiD_xDx * norm6U_x + DnormiD_yDx * norm6U_y + DnormiD_zDx * norm6U_z +
	                   Dnorm6U_xDx * normiD_x + Dnorm6U_yDx * normiD_y + Dnorm6U_zDx * normiD_z ) +
	       betaD3_3 * (Dnorm4U_xDx * norm3D_x + Dnorm4U_yDx * norm3D_y + Dnorm4U_zDx * norm3D_z +
	                   Dnorm3D_xDx * norm4U_x + Dnorm3D_yDx * norm4U_y + Dnorm3D_zDx * norm4U_z ) +
	       betaD2_3 * (DnormiU_xDx * norm2D_x + DnormiU_yDx * norm2D_y + DnormiU_zDx * norm2D_z +
	                   Dnorm2D_xDx * normiU_x + Dnorm2D_yDx * normiU_y + Dnorm2D_zDx * normiU_z ) +
	       betaDi_3 * (Dnorm5U_xDx * normiD_x + Dnorm5U_yDx * normiD_y + Dnorm5U_zDx * normiD_z + 
	                   DnormiD_xDx * norm5U_x + DnormiD_yDx * norm5U_y + DnormiD_zDx * norm5U_z ) +
	       betaD4_3 * (Dnorm4D_xDx * norm8U_x + Dnorm4D_yDx * norm8U_y + Dnorm4D_zDx * norm8U_z +
	                   Dnorm8U_xDx * norm4D_x + Dnorm8U_yDx * norm4D_y + Dnorm8U_zDx * norm4D_z );
	       
	*Fy += betaUi_3 * (DnormiU_xDy * normiD_x + DnormiU_yDy * normiD_y + DnormiU_zDy * normiD_z +
	                   DnormiD_xDy * normiU_x + DnormiD_yDy * normiU_y + DnormiD_zDy * normiU_z ) +
	       betaU3_3 * (Dnorm3U_xDy * norm3D_x + Dnorm3U_yDy * norm3D_y + Dnorm3U_zDy * norm3D_z +
	                   Dnorm3D_xDy * norm3U_x + Dnorm3D_yDy * norm3U_y + Dnorm3D_zDy * norm3U_z ) +
	       betaU4_3 * (Dnorm4U_xDy * norm4D_x + Dnorm4U_yDy * norm4D_y + Dnorm4U_zDy * norm4D_z +
	                   Dnorm4D_xDy * norm4U_x + Dnorm4D_yDy * norm4U_y + Dnorm4D_zDy * norm4U_z ) +
	       betaU5_3 * (Dnorm5U_xDy * norm5D_x + Dnorm5U_yDy * norm5D_y + Dnorm5U_zDy * norm5D_z +
	                   Dnorm5D_xDy * norm5U_x + Dnorm5D_yDy * norm5U_y + Dnorm5D_zDy * norm5U_z ) +
	       betaD7_1 * (Dnorm4U_xDy * norm7D_x + Dnorm4U_yDy * norm7D_y + Dnorm4U_zDy * norm7D_z +
	                   Dnorm7D_xDy * norm4U_x + Dnorm7D_yDy * norm4U_y + Dnorm7D_zDy * norm4U_z ) +
	       betaD3_1 * (DnormiU_xDy * norm3D_x + DnormiU_yDy * norm3D_y + DnormiU_zDy * norm3D_z +
	                   Dnorm3D_xDy * normiU_x + Dnorm3D_yDy * normiU_y + Dnorm3D_zDy * normiU_z ) +
	       betaD4_1 * (Dnorm5U_xDy * norm4D_x + Dnorm5U_yDy * norm4D_y + Dnorm5U_zDy * norm4D_z + 
	                   Dnorm4D_xDy * norm5U_x + Dnorm4D_yDy * norm5U_y + Dnorm4D_zDy * norm5U_z ) +
	       betaDi_1 * (DnormiD_xDy * norm6U_x + DnormiD_yDy * norm6U_y + DnormiD_zDy * norm6U_z +
	                   Dnorm6U_xDy * normiD_x + Dnorm6U_yDy * normiD_y + Dnorm6U_zDy * normiD_z ) +
	       betaD3_3 * (Dnorm4U_xDy * norm3D_x + Dnorm4U_yDy * norm3D_y + Dnorm4U_zDy * norm3D_z +
	                   Dnorm3D_xDy * norm4U_x + Dnorm3D_yDy * norm4U_y + Dnorm3D_zDy * norm4U_z ) +
	       betaD2_3 * (DnormiU_xDy * norm2D_x + DnormiU_yDy * norm2D_y + DnormiU_zDy * norm2D_z +
	                   Dnorm2D_xDy * normiU_x + Dnorm2D_yDy * normiU_y + Dnorm2D_zDy * normiU_z ) +
	       betaDi_3 * (Dnorm5U_xDy * normiD_x + Dnorm5U_yDy * normiD_y + Dnorm5U_zDy * normiD_z + 
	                   DnormiD_xDy * norm5U_x + DnormiD_yDy * norm5U_y + DnormiD_zDy * norm5U_z ) +
	       betaD4_3 * (Dnorm4D_xDy * norm8U_x + Dnorm4D_yDy * norm8U_y + Dnorm4D_zDy * norm8U_z +
	                   Dnorm8U_xDy * norm4D_x + Dnorm8U_yDy * norm4D_y + Dnorm8U_zDy * norm4D_z );
	
	*Fz += betaUi_3 * (DnormiU_xDz * normiD_x + DnormiU_yDz * normiD_y + DnormiU_zDz * normiD_z +
	                   DnormiD_xDz * normiU_x + DnormiD_yDz * normiU_y + DnormiD_zDz * normiU_z ) +
	       betaU3_3 * (Dnorm3U_xDz * norm3D_x + Dnorm3U_yDz * norm3D_y + Dnorm3U_zDz * norm3D_z +
	                   Dnorm3D_xDz * norm3U_x + Dnorm3D_yDz * norm3U_y + Dnorm3D_zDz * norm3U_z ) +
	       betaU4_3 * (Dnorm4U_xDz * norm4D_x + Dnorm4U_yDz * norm4D_y + Dnorm4U_zDz * norm4D_z +
	                   Dnorm4D_xDz * norm4U_x + Dnorm4D_yDz * norm4U_y + Dnorm4D_zDz * norm4U_z ) +
	       betaU5_3 * (Dnorm5U_xDz * norm5D_x + Dnorm5U_yDz * norm5D_y + Dnorm5U_zDz * norm5D_z +
	                   Dnorm5D_xDz * norm5U_x + Dnorm5D_yDz * norm5U_y + Dnorm5D_zDz * norm5U_z ) +
	       betaD7_1 * (Dnorm4U_xDz * norm7D_x + Dnorm4U_yDz * norm7D_y + Dnorm4U_zDz * norm7D_z +
	                   Dnorm7D_xDz * norm4U_x + Dnorm7D_yDz * norm4U_y + Dnorm7D_zDz * norm4U_z ) +
	       betaD3_1 * (DnormiU_xDz * norm3D_x + DnormiU_yDz * norm3D_y + DnormiU_zDz * norm3D_z +
	                   Dnorm3D_xDz * normiU_x + Dnorm3D_yDz * normiU_y + Dnorm3D_zDz * normiU_z ) +
	       betaD4_1 * (Dnorm5U_xDz * norm4D_x + Dnorm5U_yDz * norm4D_y + Dnorm5U_zDz * norm4D_z + 
	                   Dnorm4D_xDz * norm5U_x + Dnorm4D_yDz * norm5U_y + Dnorm4D_zDz * norm5U_z ) +
	       betaDi_1 * (DnormiD_xDz * norm6U_x + DnormiD_yDz * norm6U_y + DnormiD_zDz * norm6U_z +
	                   Dnorm6U_xDz * normiD_x + Dnorm6U_yDz * normiD_y + Dnorm6U_zDz * normiD_z ) +
	       betaD3_3 * (Dnorm4U_xDz * norm3D_x + Dnorm4U_yDz * norm3D_y + Dnorm4U_zDz * norm3D_z +
	                   Dnorm3D_xDz * norm4U_x + Dnorm3D_yDz * norm4U_y + Dnorm3D_zDz * norm4U_z ) +
	       betaD2_3 * (DnormiU_xDz * norm2D_x + DnormiU_yDz * norm2D_y + DnormiU_zDz * norm2D_z +
	                   Dnorm2D_xDz * normiU_x + Dnorm2D_yDz * normiU_y + Dnorm2D_zDz * normiU_z ) +
	       betaDi_3 * (Dnorm5U_xDz * normiD_x + Dnorm5U_yDz * normiD_y + Dnorm5U_zDz * normiD_z + 
	                   DnormiD_xDz * norm5U_x + DnormiD_yDz * norm5U_y + DnormiD_zDz * norm5U_z ) +
	       betaD4_3 * (Dnorm4D_xDz * norm8U_x + Dnorm4D_yDz * norm8U_y + Dnorm4D_zDz * norm8U_z +
	                   Dnorm8U_xDz * norm4D_x + Dnorm8U_yDz * norm4D_y + Dnorm8U_zDz * norm4D_z ); 

}
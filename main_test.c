#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <stdbool.h>
#include "CUnit/Basic.h"

#include "alltypes.h"
#include "utils/log_test.h"
#include "utils/indices_test.h"
#include "utils/norm_test.h"
#include "utils/parallel_utils.h"
#include "interactions/exchange/func_uniformexchange_test.h"
#include "interactions/exchange/uniformexchange_test.h"
#include "interactions/anisotropy/func_anisotropy_test.h"
#include "interactions/anisotropy/anisotropy_test.h"
#include "interactions/externalField/func_externalfield_test.h"
#include "interactions/externalField/externalfield_test.h"

/* output of processTestCMDoptions function */

/* no options */
#define IND_NOTH 0
/* help option */
#define IND_HELP 1
/* test option */
#define IND_TEST 2
/* launch option */
#define IND_LAUNCH 3
/* incorrect option */
#define IND_ERROR 4
/* long option */
#define IND_LONG 5

void log_tests(CU_pSuite * pSuite) {

	/* create suite */
	*pSuite = CU_add_suite("Log tests", init_log_test, clean_log_test);
	if (NULL == *pSuite) {
		CU_cleanup_registry();
		exit(CU_get_error());
	}

	/* add the tests to the suite */
	if (
			NULL == CU_add_test(*pSuite, test_create_log_file_desc, test_create_log_file)
			)
	{
		CU_cleanup_registry();
		exit(CU_get_error());
	}

}

void indices_tests(CU_pSuite * pSuite) {

	/* create suite */
	*pSuite = CU_add_suite("Indices tests", init_indices_test, clean_indices_test);
	if (NULL == *pSuite) {
		CU_cleanup_registry();
		exit(CU_get_error());
	}

	/* add the tests to the suite */
	if ( 
			NULL == CU_add_test(*pSuite, test_IDX_1_desc, test_IDX_1)
			|| NULL == CU_add_test(*pSuite, test_IDX_2_desc, test_IDX_2)
			|| NULL == CU_add_test(*pSuite, test_IDXmg_1_desc, test_IDXmg_1)
			|| NULL == CU_add_test(*pSuite, test_IDXmg_2_desc, test_IDXmg_2)
			|| NULL == CU_add_test(*pSuite, test_IDXcrd_1_desc, test_IDXcrd_1)
			|| NULL == CU_add_test(*pSuite, test_IDXcrd_2_desc, test_IDXcrd_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtr_1_desc, test_IDXtr_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtr_2_desc, test_IDXtr_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg_1_desc, test_IDXtrmg_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg_2_desc, test_IDXtrmg_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtrcrd_1_desc, test_IDXtrcrd_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtrcrd_2_desc, test_IDXtrcrd_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtr1_1_desc, test_IDXtr1_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtr1_2_desc, test_IDXtr1_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg1_1_desc, test_IDXtrmg1_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg1_2_desc, test_IDXtrmg1_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtr2_1_desc, test_IDXtr2_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtr2_2_desc, test_IDXtr2_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg2_1_desc, test_IDXtrmg2_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg2_2_desc, test_IDXtrmg2_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtr3_1_desc, test_IDXtr3_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtr3_2_desc, test_IDXtr3_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg3_1_desc, test_IDXtrmg3_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg3_2_desc, test_IDXtrmg3_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtr4_1_desc, test_IDXtr4_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtr4_2_desc, test_IDXtr4_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg4_1_desc, test_IDXtrmg4_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg4_2_desc, test_IDXtrmg4_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtr5_1_desc, test_IDXtr5_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtr5_2_desc, test_IDXtr5_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg5_1_desc, test_IDXtrmg5_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg5_2_desc, test_IDXtrmg5_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtr6_1_desc, test_IDXtr6_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtr6_2_desc, test_IDXtr6_2)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg6_1_desc, test_IDXtrmg6_1)
			|| NULL == CU_add_test(*pSuite, test_IDXtrmg6_2_desc, test_IDXtrmg6_2)
			)
	{
		CU_cleanup_registry();
		exit(CU_get_error());
	}

}

void normSites_tests(CU_pSuite * pSuite) {

	/* create suite */
	*pSuite = CU_add_suite("Tests of function to get normal vectors of sites for triangular lattice:", init_norm_test, clean_norm_test);
	if (NULL == *pSuite) {
		CU_cleanup_registry();
		exit(CU_get_error());
	}

	/* add the tests to the suite */
	if  (
			NULL == CU_add_test(*pSuite, test_normSites_1_desc, test_normSites_1)
			|| NULL == CU_add_test(*pSuite, test_normSites_2_desc, test_normSites_2)
			)
	{
		CU_cleanup_registry();
		exit(CU_get_error());
	}


}

void normTriangles_tests(CU_pSuite * pSuite) {

	/* create suite */
	*pSuite = CU_add_suite("Tests of function to get normal vectors of triangles fot triangular lattice:", init_norm_test, clean_norm_test);
	if (NULL == *pSuite) {
		CU_cleanup_registry();
		exit(CU_get_error());
	}

	/* add the tests to the suite */
	if (
			NULL == CU_add_test(*pSuite, test_normTriangles_1_desc, test_normTriangles_1)
			|| NULL == CU_add_test(*pSuite, test_normTriangles_2_desc, test_normTriangles_2)
			)
	{
		CU_cleanup_registry();
		exit(CU_get_error());
	}

}

void func_uniformexchange_tests(CU_pSuite * pSuite) {

	/* create suite */
	*pSuite = CU_add_suite("Functional tests of uniform exchange in cube lattice:", init_func_uniformexchange_test, clean_func_uniformexchange_test);
	if (NULL == *pSuite) {
		CU_cleanup_registry();
		exit(CU_get_error());
	}

	/* add the tests to the suite */
	if (
			NULL == CU_add_test(*pSuite, test_uniformexchange_simulation_1_desc, test_uniformexchange_simulation_1)
			|| NULL == CU_add_test(*pSuite, test_uniformexchange_simulation_2_desc, test_uniformexchange_simulation_2)
			|| NULL == CU_add_test(*pSuite, test_uniformexchange_simulation_3_desc, test_uniformexchange_simulation_3)
			|| NULL == CU_add_test(*pSuite, test_uniformexchange_simulation_4_desc, test_uniformexchange_simulation_4)
			)
	{
		CU_cleanup_registry();
		exit(CU_get_error());
	}

}

void uniformexchange_tests(CU_pSuite * pSuite) {

	/* create suite */
	*pSuite = CU_add_suite("Unit tests of uniform exchange in cube lattice:", init_uniformexchange_test, clean_uniformexchange_test);
	if (NULL == *pSuite) {
		CU_cleanup_registry();
		exit(CU_get_error());
	}

	/* add the tests to the suite */
	if  (
			NULL == CU_add_test(*pSuite, test_uniformexchange_energy_1_desc, test_uniformexchange_energy_1)
			|| NULL == CU_add_test(*pSuite, test_uniformexchange_energy_2_desc, test_uniformexchange_energy_2)
			|| NULL == CU_add_test(*pSuite, test_uniformexchange_energy_3_desc, test_uniformexchange_energy_3)
			|| NULL == CU_add_test(*pSuite, test_uniformexchange_energy_4_desc, test_uniformexchange_energy_4)
			|| NULL == CU_add_test(*pSuite, test_uniformexchange_energy_5_desc, test_uniformexchange_energy_5)
			|| NULL == CU_add_test(*pSuite, test_uniformexchange_energy_6_desc, test_uniformexchange_energy_6)
			|| NULL == CU_add_test(*pSuite, test_uniformexchange_energy_7_desc, test_uniformexchange_energy_7)
			|| NULL == CU_add_test(*pSuite, test_uniformexchange_energy_8_desc, test_uniformexchange_energy_8)
			)
	{
		CU_cleanup_registry();
		exit(CU_get_error());
	}

}

void func_anisotropy_tests(CU_pSuite * pSuite) {

	/* create suite */
	*pSuite = CU_add_suite("Tests of anisotropy in cube lattice:", init_func_anisotropy_test, clean_func_anisotropy_test);
	if (NULL == *pSuite) {
		CU_cleanup_registry();
		exit(CU_get_error());
	}

	/* add the tests to the suite */
	if  (
			NULL == CU_add_test(*pSuite, test_anisotropy_simulation_1_desc, test_anisotropy_simulation_1)
			|| NULL == CU_add_test(*pSuite, test_anisotropy_simulation_2_desc, test_anisotropy_simulation_2)
			|| NULL == CU_add_test(*pSuite, test_anisotropy_simulation_3_desc, test_anisotropy_simulation_3)
			|| NULL == CU_add_test(*pSuite, test_anisotropy_simulation_4_desc, test_anisotropy_simulation_4)
			)
	{
		CU_cleanup_registry();
		exit(CU_get_error());
	}

}


void anisotropy_tests(CU_pSuite * pSuite) {

	/* create suite */
	*pSuite = CU_add_suite("Tests of anisotropy in cube lattice:", init_anisotropy_test, clean_anisotropy_test);
	if (NULL == *pSuite) {
		CU_cleanup_registry();
		exit(CU_get_error());
	}

	/* add the tests to the suite */
	if  (
			NULL == CU_add_test(*pSuite, test_anisotropy_energy_1_desc, test_anisotropy_energy_1)
			|| NULL == CU_add_test(*pSuite, test_anisotropy_energy_2_desc, test_anisotropy_energy_2)
			|| NULL == CU_add_test(*pSuite, test_anisotropy_energy_3_desc, test_anisotropy_energy_3)
			|| NULL == CU_add_test(*pSuite, test_anisotropy_energy_4_desc, test_anisotropy_energy_4)
			|| NULL == CU_add_test(*pSuite, test_anisotropy_energy_5_desc, test_anisotropy_energy_5)
			)
	{
		CU_cleanup_registry();
		exit(CU_get_error());
	}

}

void func_externalfield_tests(CU_pSuite * pSuite) {

	/* create suite */
	*pSuite = CU_add_suite("Tests of anisotropy in cube lattice:", init_func_externalfield_test, clean_func_externalfield_test);
	if (NULL == *pSuite) {
		CU_cleanup_registry();
		exit(CU_get_error());
	}

	/* add the tests to the suite */
	if  (
			NULL == CU_add_test(*pSuite, test_externalfield_simulation_1_desc, test_externalfield_simulation_1)
			|| NULL == CU_add_test(*pSuite, test_externalfield_simulation_2_desc, test_externalfield_simulation_2)
			|| NULL == CU_add_test(*pSuite, test_externalfield_simulation_3_desc, test_externalfield_simulation_3)
			|| NULL == CU_add_test(*pSuite, test_externalfield_simulation_4_desc, test_externalfield_simulation_4)
			)
	{
		CU_cleanup_registry();
		exit(CU_get_error());
	}

}

void externalfield_tests(CU_pSuite * pSuite) {

	/* create suite */
	*pSuite = CU_add_suite("Tests of anisotropy in cube lattice:", init_externalfield_test, clean_externalfield_test);
	if (NULL == *pSuite) {
		CU_cleanup_registry();
		exit(CU_get_error());
	}

	/* add the tests to the suite */
	if  (
			NULL == CU_add_test(*pSuite, test_externalfield_energy_1_desc, test_externalfield_energy_1)
			|| NULL == CU_add_test(*pSuite, test_externalfield_energy_2_desc, test_externalfield_energy_2)
			|| NULL == CU_add_test(*pSuite, test_externalfield_energy_3_desc, test_externalfield_energy_3)
			|| NULL == CU_add_test(*pSuite, test_externalfield_energy_4_desc, test_externalfield_energy_4)
			|| NULL == CU_add_test(*pSuite, test_externalfield_energy_5_desc, test_externalfield_energy_5)
			)
	{
		CU_cleanup_registry();
		exit(CU_get_error());
	}

}

/** @brief Prints help to stdout
 */
void print_help(void) {

	/* print message for helping */
	printf("Spin Lattice Simulator SLaSi\nTesting Engine\n\n"
	       "Usage: $ ./test [OPTIONS]\n\n"
	       "Options:\n"
	       "-h --help\n"
	       "    Prints this message and exit\n"
	       "-t --list-tests\n"
	       "    Prints list of available tests\n"
	       "-l --launch PARAMS\n"
	       "    Starts selected tests, given in PARAMS\n"
	);
}

/** @brief Prints list of available tests to stdout
 */
void print_tests(void) {

	/* print types of tests */
	printf("Spin Lattice Simulator SLaSi\nTesting Engine\n\n"
	       "Here, the list of available tests:\n"
	       "  log         logger\n"
	       "  indices     indices in cubic and triangular lattice\n"
	       "  norm        verification of normal vectors calculation in triangular lattice\n"
	       "  energy      all tests, related to energy verification.\n"
	       "              Do not use energy and subkeys simultaneously\n"
	       "    uniex     uniform exchange\n"
	       "    ani       anisotropy\n"
	       "    extfield  external magnetic field\n"
	       "  functional  running of test simulations"
	       "\n"
	       "Usage example:\n"
	       "  $ ./test --tests indices/norm/ani\n"
	);

}

/** @brief Finds test with a given id in list of requested
 * 
 * Finds substring `id` in string `tests` with delimiter '/'
 * 
 * @param tests c-string with list of requested tests. Should not be 
 *              NULL.
 * @param id    c-string with test for check is it requested or no.
 *              Should not be NULL.
 * 
 * @retval true/1  Test `id` is requested
 * @retval false/0 Test `id` is not requested
 */
bool find_test(char * restrict tests, const char * id) {

	char * token = NULL;

	/* find token in row */
	token = strtok(tests, "/ \n\t\r");
	while (token != NULL) {
		if (strcmp(token, id) == 0) {
			/* return result if answer is positive */
			printf("### '%s' '%s'\n", token, id);
			return true;
		}
		token = strtok(NULL, "| \n\t\r");
	}

	/* return result if answer is negative */
	return false;

}

/** @brief Processing command line arguments for the testing module
 * 
 * Parser for command line argumens using getopt library. Testing module
 * supports a few parameters, shown help, list of available test suites 
 * (not always suites in the source code) and launcher.
 * 
 * @param argc  Argument of `main` function
 * @param argv  Argument of `main` function
 * @param tests Pointer to c-string where list of tests for launcing is
 *              placed. If not NULL, then fried and reallocated.
 * 
 * @retval 1 Successful processing
 * @retval 0 Error 
 */
int processTestCMDoptions(int argc, char **argv, char ** tests) {

	/* options for terminal */
	static struct option long_options[] = {
			{"help",        no_argument,       0, 'h' },
			{"list-tests",  no_argument,       0, 't' },
			{"launch",      required_argument, 0, 'l' },
			{0,         0,                     0,  0  }
	};
	
	/* find all options */
	while (1) {

		int option_index = 0;
		int c = getopt_long(argc, argv, "hl:t", long_options, &option_index);
		
		if (c == -1) {
			/* default arguments */
			break;
		}
		
		if (c == '?') {
			/* error */
			return 0;
		}
		
		switch (c) {
			case 0:
				/* long option is given */
				printf("option %s", long_options[option_index].name);
				return IND_LONG;
			
			case 'h':
				/* print help */
				print_help();
				return IND_HELP;
			
			case 't':
				/* list of all tests */
				print_tests();
				return IND_TEST;

			case 'l':
				/* select tests */
				if (*tests) {
					free(tests);
				}
				*tests = malloc(strlen(optarg) + 1);
				memset(*tests, 0, strlen(optarg) + 1);
				strcpy(*tests, optarg);
				return IND_LAUNCH;
				
			default:
				/* smth going wrong */
				fprintf(stderr, "Wrong action in getopt\n");
				return IND_ERROR;
		}

	}
	
	return IND_NOTH;

}

int main(int argc, char * argv[]) {

	/* initialize environment (parallel calculation) */
	envInit("", true);

	char * tests = NULL;
	/* launch terminal options and receive result */
	int res = processTestCMDoptions(argc, argv, &tests);
	/* check if result is error */
	if (res == IND_ERROR) {
		return 1;
	}
	/* stop execution if this launching is for helping or testing */
	if ((res == IND_TEST) || (res == IND_HELP)) {
		return 0;
	}

	/* declaration of suites for unit tests */
	CU_pSuite pSuite_log_test = NULL;
	CU_pSuite pSuite_indices_test = NULL;
	CU_pSuite pSuite_normTriangles_test = NULL;
	CU_pSuite pSuite_normSites_test = NULL;
	CU_pSuite pSuite_uniformexchange_test = NULL;
	CU_pSuite pSuite_anisotropy_test = NULL;
	CU_pSuite pSuite_externalfield_test = NULL;

	/* declaration of suites for functional tests */
	CU_pSuite pSuite_func_uniformexchange_test = NULL;
	CU_pSuite pSuite_func_anisotropy_test = NULL;
	CU_pSuite pSuite_func_externalfield_test = NULL;

	/* initialize the CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry()) {
		if (tests) {
			free(tests);
		}
		return CU_get_error();
	}

	/* registering tests */
	if ((!tests) && (res == IND_NOTH)) {

		/* adding all tests into run (only unit tests) */
		log_tests(&pSuite_log_test);
		indices_tests(&pSuite_indices_test);
		normSites_tests(&pSuite_normSites_test);
		normTriangles_tests(&pSuite_normTriangles_test);
		uniformexchange_tests(&pSuite_uniformexchange_test);
		anisotropy_tests(&pSuite_anisotropy_test);
		externalfield_tests(&pSuite_externalfield_test);

	} else if (res == IND_LAUNCH) {

		/* launch specified tests for user */

		/* unit tests */
		if (find_test(tests, "log")) {
			log_tests(&pSuite_log_test);
		} else if (find_test(tests, "norm")) {
			normSites_tests(&pSuite_normSites_test);
			normTriangles_tests(&pSuite_normTriangles_test);
		} else if (find_test(tests, "indices")) {
			indices_tests(&pSuite_indices_test);
		} else if (find_test(tests, "uniex")) {
			uniformexchange_tests(&pSuite_uniformexchange_test);
		} else if (find_test(tests, "ani")) {
			anisotropy_tests(&pSuite_anisotropy_test);
		} else if (find_test(tests, "extfield")) {
			externalfield_tests(&pSuite_externalfield_test);
		} else if (find_test(tests, "energy")) {
			uniformexchange_tests(&pSuite_uniformexchange_test);
			anisotropy_tests(&pSuite_anisotropy_test);
			externalfield_tests(&pSuite_externalfield_test);
		}

		/* functional tests */
		if (find_test(tests, "functional")) {
			func_uniformexchange_tests(&pSuite_func_uniformexchange_test);
			func_anisotropy_tests(&pSuite_func_anisotropy_test);
			func_externalfield_tests(&pSuite_func_externalfield_test);
		}

	}

	/* finalize environment (parallel calculation) */
	envFinalize(true);

	/* run all tests using the CUnit Basic interface */
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();

	/* clean registry of CUnit */
	CU_cleanup_registry();

	/* free all tests */
	if (tests) {
		free(tests);
	}

	/* return error of testing */
	return CU_get_error();

}

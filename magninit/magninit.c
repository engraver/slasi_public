/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include "magninit.h"
#include "../alltypes.h"
#include "../utils/indices.h"
#include "../utils/parallel_utils.h"

/**
 * @brief Initializer of random magnetization distribution. Function
 * `rand` from standard C librari is used for getting values for magnetic
 * angles theta and phi. `srand(time(0))` is called somewhere else.
 */
int randomi(void) {

	for (int i1 = 0; i1 < userVars.N1; i1++) {
		for (int i2 = 0; i2 < userVars.N2; i2++) {
			for (int i3 = 0; i3 < userVars.N3; i3++) {
				if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
				    i3 < userVars.N3 - 1) {
					int indlp = IDX(i1, i2, i3);

					/* initialization for magnetic sites only */
					if (latticeParam[indlp].magnetic) {
						/* magnetic angles */
						double theta = ((double) rand() / RAND_MAX) * M_PI;
						double phi = ((double) rand() / RAND_MAX) * 2.0 * M_PI;

						/* lattice initialization */
						*(latticeParam[indlp].mx) = sin(theta) * cos(phi);
						*(latticeParam[indlp].my) = sin(theta) * sin(phi);
						*(latticeParam[indlp].mz) = cos(theta);

					}

				}
			}
		}
	}

	return 0;

}

/**
 * @brief Initializer of uniform magnetization distribution in laboratory
 * reference frame. Uses `mx0`, `my0` and `mz0` fields of `userVars`.
 */
int uniformi(void) {

	if (!userVars.bmx0 || !userVars.bmy0 || !userVars.bmz0) {
		/* Printing error if uniform magnetization is selected, but direction
		 * is not given */
		char msg[] = "All mx0, my0 and mz0 variables should be set up!\n";
		fprintf(flog, "%s", msg);
		fprintf(stderr, "%s", msg);
		return 1;
	}

	/* normalize vector giving direction of magnetization */
	double len = sqrt(userVars.mx0 * userVars.mx0 + userVars.my0 * userVars.my0 + userVars.mz0 * userVars.mz0);
	userVars.mx0 *= 1. / len;
	userVars.my0 *= 1. / len;
	userVars.mz0 *= 1. / len;

	for (int i1 = 0; i1 < userVars.N1; i1++) {
		for (int i2 = 0; i2 < userVars.N2; i2++) {
			for (int i3 = 0; i3 < userVars.N3; i3++) {
				if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
				    i3 < userVars.N3 - 1) {
					int indlp = IDX(i1, i2, i3);

					/* initialization for magnetic sites only */
					if (latticeParam[indlp].magnetic) {
						/* lattice initialization */
						*(latticeParam[indlp].mx) = userVars.mx0;
						*(latticeParam[indlp].my) = userVars.my0;
						*(latticeParam[indlp].mz) = userVars.mz0;
					}

				}
			}
		}
	}
	return 0;

}

/**
 * @brief Initializer of magnetic distribution in laticeParam (mx, my, mz) using function
 * which has name "GetMagn" and is located in file .py (name of this file is in userVars.scriptFile)
 */
int script(void) {

	/* declarations of objects to work with Python.h */
	PyObject *pArgs, *pFunc;
	PyObject *pValue;

	/* declarations of variable */
	double mx, my, mz;
	int res;

	/* check if module was found */
	if (pModulePython != NULL) {

		/* search of function wint name "getMagn" in module */
		pFunc = PyObject_GetAttrString(pModulePython, "getMagn");
		/* check if function was found */
		if (pFunc && PyCallable_Check(pFunc)) {

			/* pass through each magnetic moment */
			for (int i1 = 0; i1 < userVars.N1; i1++) {
				for (int i2 = 0; i2 < userVars.N2; i2++) {
					for (int i3 = 0; i3 < userVars.N3; i3++) {
						if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
						    i3 < userVars.N3 - 1) {

							/* number of spin in lattice */
							int indexLattice = IDX(i1, i2, i3);
							/* check if node is magnetic */
							if (latticeParam[indexLattice].magnetic) {

								/* creating parameters to call function (coordinates and indices) */
								pArgs = PyTuple_New(6);

								/* the first coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].x) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getMagn!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 0, pValue);

								/* the second coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].y) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getMagn!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 1, pValue);

								/* the third coordinate */
								pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].z) * userVars.a);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getMagn!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 2, pValue);

								/* the first index */
								pValue = PyLong_FromLong(i1);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getMagn!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 3, pValue);

								/* the second index */
								pValue = PyLong_FromLong(i2);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getMagn!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 4, pValue);

								/* the third index */
								pValue = PyLong_FromLong(i3);
								/* check value */
								if (!pValue) {
									/* output error */
									Py_DECREF(pArgs);
									Py_DECREF(pModulePython);
									fprintf(stderr, "Cannot convert arguments for function getMagn!\n");
									return 1;
								}
								/* put new parameter */
								PyTuple_SetItem(pArgs, 5, pValue);

								/* call function */
								pValue = PyObject_CallObject(pFunc, pArgs);
								/* check results of calling */
								if (pValue != NULL) {

									/* get results of calling */
									res = PyArg_ParseTuple(pValue, "ddd", &mx, &my, &mz);
									/* check if it was able to get */
									if (res) {
										/* save results */
										double len = sqrt(mx*mx + my*my + mz*mz);
										if (fabs(len) < 1e-5) {
											/* output error */
											PyErr_Print();
											fprintf(stderr, "(file %s | line %d) Script %s returns zero magnetic moment for site (%d, %d, %d) with coords (%lg, %lg, %lg)!\n", __FILE__, __LINE__, userVars.scriptFile, i1, i2, i3, *(latticeParam[indexLattice].x), *(latticeParam[indexLattice].y), *(latticeParam[indexLattice].z));
											return 1;
										}
										*(latticeParam[indexLattice].mx) = mx/len;
										*(latticeParam[indexLattice].my) = my/len;
										*(latticeParam[indexLattice].mz) = mz/len;
									} else {
										/* output error */
										fprintf(stderr, "Call of python function getMagn failed!\n");
										return 1;
									}
									Py_DECREF(pValue);

								} else {
									/* output error */
									Py_DECREF(pFunc);
									Py_DECREF(pModulePython);
									PyErr_Print();
									fprintf(stderr, "Call of python function getMagn failed\n");
									return 1;
								}

							}

						}
					}
				}
			}

		} else {
			/* output error */
			if (PyErr_Occurred())
				PyErr_Print();
			fprintf(stderr, "Cannot find function getMagn in python file!\n");
			return 1;
		}
		Py_XDECREF(pFunc);

	} else {
		/* output error */
		PyErr_Print();
		fprintf(stderr, "Failed to load python file %s!\n", userVars.scriptFile);
		return 1;
	}

	return 0;

}

/**
 * @brief Reads input magnetization distribution from the provided slsb file
 * @return `0` in case of absence of errors
 */
int slsb(void) {

	/* open file to read amplitudes of magnetic moment */
	FILE * fid = fopen(userVars.magnFile, "rb");

	/* check opening of file */
	if (fid) {

		/* temporary values for reading */
		int itmp;
		int N1Inter, N2Inter, N3Inter;
		int launchTypeInter;
		double dtmp;
		unsigned long long lltmp;

		/* read variables about type and size of lattice */
		fread(&itmp, sizeof(int), 1, fid); /* scalar field 1 */
		fread(&itmp, sizeof(int), 1, fid); /* scalar field 2 */
		fread(&N1Inter, sizeof(int), 1, fid); /* scalar field 3 */
		fread(&N2Inter, sizeof(int), 1, fid); /* scalar field 4 */
		fread(&N3Inter, sizeof(int), 1, fid); /* scalar field 5 */

		/* check number of sites */
		if ((N1Inter != userVars.N1 - 2) || (N2Inter != userVars.N2 - 2) || (N3Inter != userVars.N3 - 2)) {
			fprintf(stderr, "(file %s | line %d) Number of sites is incorrect!\n", __FILE__, __LINE__);
			return 1;
		}

		/* read variable about type of launch */
		fread(&itmp, sizeof(int), 1, fid); /* scalar field 6 */
		fread(&launchTypeInter, sizeof(int), 1, fid); /* scalar field 7 */

		/* read different energies */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 8 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 9 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 10 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 11 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 12 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 13 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 14 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 15 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 16 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 17 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 18 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 19 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 20 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 21 */

		/* read total magnetic moments for each axis */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 22 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 23 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 24 */

		/* read uniform external field */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 25 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 26 */
		fread(&dtmp, sizeof(double), 1, fid); /* scalar field 27 */

		/* simulation parameters */
		if (userVars.launch == LAUNCH_SIMULATION) {
			/* read time variables */
			fread(&dtmp, sizeof(double), 1, fid); /* scalar field 28 */
			fread(&dtmp, sizeof(double), 1, fid); /* scalar field 29 */
			fread(&dtmp, sizeof(double), 1, fid); /* scalar field 30 */
			fread(&dtmp, sizeof(double), 1, fid); /* scalar field 31 */
			fread(&lltmp, sizeof(unsigned long long), 1, fid); /* scalar field 32 */
			fread(&lltmp, sizeof(unsigned long long), 1, fid); /* scalar field 33 */
		} else {
			/* return error */
			fprintf(stderr, "(file %s | line %d) Launch type is not \"simulation\", not implemented.\n", __FILE__, __LINE__);
			return 1;
		}

		/* read indicator and size of vector field */
		int ind, size;
		fread(&ind, sizeof(int), 1, fid);
		fread(&size, sizeof(int), 1, fid);

		/* declaration of additional variables */
		double varX, varY, varZ;
		size_t curIndex = 0;

		/* go through all sites */
		int i1, i2, i3;
		for (i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
					/* find index */
					if (userVars.latticeType == LATTICE_CUBIC) {
						curIndex = (size_t)IDXmg(i1, i2, i3);
					} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
						curIndex = (size_t)IDXtrmg(i1, i2);
					}
					/* read variables for array */
					fread(&varX, sizeof(double), 1, fid);
					fread(&varY, sizeof(double), 1, fid);
					fread(&varZ, sizeof(double), 1, fid);
					/* handle "nan" value */
					if (isnan(varX) || isnan(varY) || isnan(varZ)) {
						/* non-magnetic site */
						varX = 0.0;
						varY = 0.0;
						varZ = 0.0;
					}
					/* save result in array */
					lattice[curIndex] = varX;
					lattice[curIndex + 1] = varY;
					lattice[curIndex + 2] = varZ;
				}
			}
		}

	} else {
		/* write error */
		char str[2 * MAX_STR_LEN];
		sprintf(str, "MAGNINIT.C: cannot open the slsb file with magnetization distribution '%s'!", userVars.magnFile);
		messageToStream(str, stderr);
		return 1;
	}

	/* close file and finish function */
	fclose(fid);
	return 0;

}

int magnInit(void) {

	char msg[] = "Initial distrubution code %d is not implemented\n";

	/* Select method of magnetization initialization depending on switcher
	 * `magnInit` in the input file */
	switch (userVars.magnInit) {
		case MI_RANDOM:
			return randomi();
			break;
		case MI_UNIFORM:
			return uniformi();
			break;
		case MI_SCRIPT:
			return script();
			break;
		case MI_SLSB:
			return slsb();
			break;
		default:
			fprintf(flog, msg, userVars.magnInit);
			fprintf(stderr, msg, userVars.magnInit);
			return 1;
	}

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

/**
 * @file magninit.h
 * @author Oleksandr Pylypovskyi, Artem Tomilo
 * @date 12 Jan 2018
 * @brief Initialization of magnetization distribution
 */

#ifndef _MAGN_INIT_H_
#define _MAGN_INIT_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../alltypes.h"

/**
 * @brief Initialization of magnetization distribution.
 * Function works with `userVars`, so does not take any arguments.
 */
int magnInit(void);

#endif

#ifndef _BACKEND_MEMORY_H_
#define _BACKEND_MEMORY_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../alltypes.h"

/** Function to set memory using usual way (Linux system calls)
 * without any parameters
 * return code of error
 * */
int setMemoryUsual(void);

/** Function to clean memory using usual way (Linux system calls)
 * without any parameters
 * return no values
 * */
void cleanMemoryUsual(void);

/** Function to set memory using CUDA library
 * without any parameters
 * return code of error
 * */
int setMemoryCUDA(void);

/** Function to clean memory using CUDA libary
 * without any parameters
 * return no values
 * */
void cleanMemoryCUDA(void);


#endif
#include "backend_memory.h"
#include "interface_memory.h"

int setupArrays(void) {
	return setMemoryCUDA();
}

void freeArrays(void) {
	cleanMemoryCUDA();
}

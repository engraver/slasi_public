/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

/**
 * @file processing.h
 * @author Oleksandr Pylypovskyi
 * @date 12 Jan 2018
 * @brief Prepare default values and check user input
 */

#ifndef _PROCESSING_H_
#define _PROCESSING_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../alltypes.h"

/**
 * @brief Default values for userVars
 */
void setDefaults(void);

/**
 * @brief Initialization of LatticeSiteDescr structures for the lattice
 * @return 0 if everything ok and nonzero value otherwise
 */
int initLattice(double ** numbers);

/**
 * @brief Check correctness of input data
 * @return 0 if everything ok and nonzero value otherwise
 */
int printGeneralInfoToLog(void);

/**
 * @brief Normalize physical data for easier integration
 */

void NormalizeLatticeParams(void);

/**
 * @brief calculate energy of uniform state for correction
 */

void CalculateEnergyOfUniformState(void);

/**
 * @brief set uniform field under some conditions
 */

void setExternalUniformField(void);

#endif

#ifndef _INTERFACE_MEMORY_H_
#define _INTERFACE_MEMORY_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../alltypes.h"

/**
 * @brief Create arrays describing spin lattice
 * @return 0 if everything ok and nonzero value otherwise
 */
int setupArrays(void);

/**
 * @brief Free memory from arrays describing spin lattice
 */

void freeArrays(void);

#endif
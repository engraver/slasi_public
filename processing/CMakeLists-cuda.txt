########################################################################
#
# Part of SLaSi project
#
########################################################################

target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/processing.c)
target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/backend_memory.c)
target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/interface_memory_cuda.c)

target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/processing.c)
target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/backend_memory.c)
target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/interface_memory_cuda.c)

#include <sys/mman.h>
#include <stdlib.h>
#include "../alltypes.h"
#include "backend_memory.h"

int setMemoryUsual(void) {

	/* array for magnetic moments and coordinates */
	spinsNum = userVars.N1 * userVars.N2 * userVars.N3;
	size_t bytesNum = NUMBER_COMP * spinsNum * sizeof(LatticeSiteSync);
	lattice = (LatticeSiteSync *) mmap(NULL, bytesNum, PROT_READ | PROT_WRITE,
	                                   MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	if (!lattice) {
		fprintf(flog, "(file %s | line %d) Cannot allocate memory for 'lattice' array!\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Cannot allocate memory for 'lattice' array!\n", __FILE__, __LINE__);
		return 1;
	}

	/* array for lattice parameters */
	bytesNum = spinsNum * sizeof(LatticeSiteDescr);
	latticeParam = (LatticeSiteDescr *) mmap(NULL, bytesNum, PROT_READ | PROT_WRITE,
	                                         MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	if (!lattice) {
		fprintf(flog, "(file %s | line %d) Cannot allocate memory for 'lattice' array!\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Cannot allocate memory for 'lattice' array!\n", __FILE__, __LINE__);
		return 1;
	}

	/* array to save amplitude for magnetic field */
	bytesNum = NUMBER_COMP_FIELD * spinsNum * sizeof(double);
	Bamp = (double *) mmap(NULL, bytesNum, PROT_READ | PROT_WRITE,
	                       MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	BampSaved = (double *) mmap(NULL, bytesNum, PROT_READ | PROT_WRITE,
	                            MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	if ((!Bamp) || (!BampSaved)) {
		fprintf(flog, "(file %s | line %d) Cannot allocate memory for 'Bamp' array!\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Cannot allocate memory for 'Bamp' array!\n", __FILE__, __LINE__);
		return 1;
	}

	/* structure to save connections between sites */
	connectivity = NULL;
	connElements = 0;

	/* allocate memory to save connections between sites */
	nodeIndices = (int*) mmap(NULL, NUMBER_COMP * spinsNum * sizeof(int), PROT_READ | PROT_WRITE,
	                          MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

	/* array for dipole interaction */
	dipole_cacheSize = 0;
	dipole_R3coef   = NULL;
	dipole_RxR5coef = NULL;
	dipole_RyR5coef = NULL;
	dipole_RzR5coef = NULL;

	return 0;

}

void cleanMemoryUsual(void) {

	/* array for magnetic moments and coordinates */
	spinsNum = userVars.N1 * userVars.N2 * userVars.N3;
	size_t bytesNum = NUMBER_COMP * spinsNum * sizeof(LatticeSiteSync);

	/* release memory of "lattice" array */
	munmap(lattice, bytesNum);
	lattice = NULL;

	/* release memory of "latticeParam" array */
	bytesNum = spinsNum * sizeof(LatticeSiteDescr);
	munmap(latticeParam, bytesNum);
	latticeParam = NULL;

	/* release memory of nodeIndices array */
	munmap(nodeIndices, NUMBER_COMP * spinsNum * sizeof(int));
	nodeIndices = NULL;

	/* release memory of "Bamp" and "BampSaved" arrays */
	bytesNum = NUMBER_COMP_FIELD * spinsNum * sizeof(double);
	munmap(Bamp, bytesNum);
	munmap(BampSaved, bytesNum);

	/* clear array to save connections between sites */
	for (int i = 0; i < connElements; i ++) {
		free(connectivity[i]);
		connectivity[i] = NULL;
	}
	free(connectivity);
	connectivity = NULL;

}
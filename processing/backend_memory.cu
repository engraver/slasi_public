#include "../alltypes.h"
extern "C" {
	#include "backend_memory.h"
}

/* check function that is offered by Nvidia */
inline cudaError_t checkCuda(cudaError_t result) {
#if defined(DEBUG) || defined(_DEBUG)
	if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n",
            cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
#endif
	return result;
}

int setMemoryCUDA(void) {

	/* array for magnetic moments and coordinates */
	spinsNum = userVars.N1 * userVars.N2 * userVars.N3;
	size_t bytesNum = NUMBER_COMP * spinsNum * sizeof(LatticeSiteSync);
	checkCuda( cudaMallocHost((LatticeSiteSync **)&lattice, bytesNum) );
	if (!lattice) {
		fprintf(flog, "(file %s | line %d) Cannot allocate memory for 'lattice' array!\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Cannot allocate memory for 'lattice' array!\n", __FILE__, __LINE__);
		return 1;
	}

	/* array for lattice parameters */
	bytesNum = spinsNum * sizeof(LatticeSiteDescr);
	checkCuda( cudaMallocHost((LatticeSiteDescr **)&latticeParam, bytesNum) );
	if (!lattice) {
		fprintf(flog, "(file %s | line %d) Cannot allocate memory for 'lattice' array!\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Cannot allocate memory for 'lattice' array!\n", __FILE__, __LINE__);
		return 1;
	}

	/* array to save amplitude for magnetic field */
	bytesNum = NUMBER_COMP_FIELD * spinsNum * sizeof(double);
	checkCuda( cudaMallocHost((double **)&Bamp, bytesNum) );
	checkCuda( cudaMallocHost((double **)&BampSaved, bytesNum) );
	if ((!Bamp) || (!BampSaved)) {
		fprintf(flog, "(file %s | line %d) Cannot allocate memory for 'Bamp' array!\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Cannot allocate memory for 'Bamp' array!\n", __FILE__, __LINE__);
		return 1;
	}

	/* structure to save connections between sites */
	connectivity = NULL;
	connElements = 0;

	/* allocate memory to save connections between sites */
	checkCuda( cudaMallocHost((int **)&nodeIndices, NUMBER_COMP * spinsNum * sizeof(int)) );

	/* array for dipole interaction */
	dipole_cacheSize = 0;
	dipole_R3coef   = NULL;
	dipole_RxR5coef = NULL;
	dipole_RyR5coef = NULL;
	dipole_RzR5coef = NULL;

	return 0;

}

void cleanMemoryCUDA(void) {

	/* release memory of "lattice" array */
	cudaFreeHost(lattice);
	lattice = NULL;

	/* release memory of "latticeParam" array */
	cudaFreeHost(latticeParam);
	latticeParam = NULL;

	/* release memory of nodeIndices array */
	cudaFreeHost(nodeIndices);
	nodeIndices = NULL;

	/* release memory of "Bamp" and "BampSaved" arrays */
	cudaFreeHost(Bamp);
	cudaFreeHost(BampSaved);

	/* clear array to save connections between sites */
	for (int i = 0; i < connElements; i ++) {
		free(connectivity[i]);
		connectivity[i] = NULL;
	}
	free(connectivity);
	connectivity = NULL;

}
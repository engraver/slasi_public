/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include "processing.h"
#include "memory.h"
#include "../magninit/magninit.h"
#include "../utils/units.h"
#include "../utils/indices.h"
#include "../readdata/geomfrompy.h"

#include "../interactions/interactions.h"
#include "../interactions/externalField/fieldSetup.h"
#include "../interactions/anisotropy/scriptAnisotropy.h"
#include "../interactions/dmi/scriptDMI.h"
#include "../interactions/exchange/scriptExchange.h"
#include "../utils/parallel_utils.h"

#include "../lattice/cubic.h"
#include "../lattice/triangular.h"

void setupNorm(void) {

	userVars.muB = MUB_N;
	userVars.bmuB = true;

	userVars.hbar = HBAR_N;
	userVars.bhbar = true;

	userVars.mu0 = MU0_N;
	userVars.bmu0 = true;

	strcpy(userVars.u_distance, DISTANCE_N);
	strcpy(userVars.u_time, TIME_N);
	strcpy(userVars.u_energy, ENERGY_N);
	strcpy(userVars.u_circfreq, CIRCFREQ_N);
	strcpy(userVars.u_mu0, MU0_UN);
	strcpy(userVars.u_mub, MUB_UN);
	strcpy(userVars.u_hbar, HBAR_UN);

}

void setupSI(void) {

	userVars.muB = MUB_SI;
	userVars.bmuB = true;

	userVars.hbar = HBAR_SI;
	userVars.bhbar = true;

	userVars.mu0 = MU0_SI;
	userVars.bmu0 = true;

	strcpy(userVars.u_distance, DISTANCE_SI);
	strcpy(userVars.u_time, TIME_SI);
	strcpy(userVars.u_energy, ENERGY_SI);
	strcpy(userVars.u_circfreq, CIRCFREQ_SI);
	strcpy(userVars.u_mu0, MU0_USI);
	strcpy(userVars.u_mub, MUB_USI);
	strcpy(userVars.u_hbar, HBAR_USI);

}

void setDefaults(void) {

	/* make all fields zero, then we don't need to set boolean fields to
	 * false manually */
	memset(&userVars, 0, sizeof(UserVars));

	/* unit of measurement */
	userVars.units = UNIT_SI;

	/* variables to test */
	testDMI = false;
	testAnisotropy = false;

	/* initial error and integration step is zero*/
	errorStep = 0.0;
	intStep = 0.0;

	/* indicator for triangular lattice is zero */
	indVarTri = false;

	/* mechanical parameters */
	userVars.mechDamping = 3e-9;
	userVars.mass = 0.0;

	userVars.imechDamping = -1;
	userVars.imass = -1;

	userVars.bmechDamping = true;
	userVars.bmass = true;

	userVars.typeMechParam = MECHPARAM_CONST;

	/* normal vectors for square lattice */
	userVars.nrmX = 0.0;
	userVars.nrmY = 0.0;
	userVars.nrmZ = 0.0;

	userVars.bnrmX = true;
	userVars.bnrmY = true;
	userVars.bnrmZ = true;

	/* velocity */
	userVars.vx = 0.0;
	userVars.vy = 0.0;
	userVars.vz = 0.0;

	userVars.bvx = true;
	userVars.bvy = true;
	userVars.bvz = true;

	userVars.ivx = -1;
	userVars.ivy = -1;
	userVars.ivz = -1;

	/* variables for flexible system */
	userVars.beta = 0.0;
	userVars.lambda = 2e-10;

	userVars.betaTri = 0.0;
	userVars.bbetaTri = false;

	userVars.bbeta = false;
	userVars.blambda = false;

	userVars.ibeta = -1;
	userVars.ilambda = -1;

	userVars.typeFlexParam = FLEXPARAM_CONST;
	userVars.flexOnOff = FLEX_OFF;
	userVars.latticeType = LATTICE_CUBIC;

	/* variable for flexible system (triangular lattice) */
	userVars.ilambda1 = -1;
	userVars.ilambda2 = -1;
	userVars.ilambda3 = -1;
	userVars.ilambda4 = -1;
	userVars.ilambda5 = -1;
	userVars.ilambda6 = -1;

	userVars.blambdaTri = true;
	userVars.lambdaTri = 0.0;

	/* parameters for external field */
	userVars.Bxamp = 0.0;
	userVars.Byamp = 0.0;
	userVars.Bzamp = 0.0;

	userVars.bBxamp = true;
	userVars.bByamp = true;
	userVars.bBzamp = true;

	userVars.iBxamp = -1;
	userVars.iByamp = -1;
	userVars.iBzamp = -1;

	userVars.Bphase = 0.0;
	userVars.Bfreq  = 0.0;
	userVars.fieldSincTWidth = 0.0;
	userVars.fieldSincTShift = 0.0;

	userVars.iBphase = -1;
	userVars.iBfreq = -1;
	
	userVars.ifieldSincTWidth = -1;
	userVars.ifieldSincTShift = -1;

	/* parameters for external force */
	userVars.Fx = 0.0;
	userVars.Fy = 0.0;
	userVars.Fz = 0.0;

	userVars.bFx = true;
	userVars.bFy = true;
	userVars.bFz = true;

	userVars.iFx = -1;
	userVars.iFy = -1;
	userVars.iFz = -1;

	userVars.extForce = EXTFORCE_CONST;

	/* parameters for frequency characteristics of external force */
	userVars.Ffreq = 0.0;
	userVars.bFfreq = true;
	userVars.iFfreq = -1;

	userVars.Fphase = 0.0;
	userVars.bFphase = true;
	userVars.iFphase = -1;

	/* magnetization */
	userVars.iMagn = -1;
	userVars.typeMagn = MAGN_ALL;

	/* variables for Dzyaloshinsky-Moria interaction */

	/* the first axis */
	userVars.D11 = 0.0;
	userVars.D12 = 0.0;
	userVars.D13 = 0.0;
	userVars.bD11 = false;
	userVars.bD12 = false;
	userVars.bD13 = false;
	userVars.iD11 = -1;
	userVars.iD12 = -1;
	userVars.iD13 = -1;

	/* the second axis */
	userVars.D21 = 0.0;
	userVars.D22 = 0.0;
	userVars.D23 = 0.0;
	userVars.bD21 = false;
	userVars.bD22 = false;
	userVars.bD23 = false;
	userVars.iD21 = -1;
	userVars.iD22 = -1;
	userVars.iD23 = -1;

	/* the third axis */
	userVars.D31 = 0.0;
	userVars.D32 = 0.0;
	userVars.D33 = 0.0;
	userVars.bD31 = false;
	userVars.bD32 = false;
	userVars.bD33 = false;
	userVars.iD31 = -1;
	userVars.iD32 = -1;
	userVars.iD33 = -1;

	/* type of Dzyaloshinsky-Moria interaction */
	userVars.DMItype = DMI_CONST;

	/* variables for bulk Dzyaloshinsky-Moria interaction (triangular lattice) */

	userVars.dmiBtri = 0.0;
	userVars.bdmiBtri = false;

	userVars.idmiB1 = -1;
	userVars.idmiB2 = -1;
	userVars.idmiB3 = -1;
	userVars.idmiB4 = -1;
	userVars.idmiB5 = -1;
	userVars.idmiB6 = -1;

	/* variables for interface Dzyaloshinsky-Moria interaction (triangular lattice) */

	userVars.dmiNtri = 0.0;
	userVars.bdmiNtri = false;

	userVars.idmiN1 = -1;
	userVars.idmiN2 = -1;
	userVars.idmiN3 = -1;
	userVars.idmiN4 = -1;
	userVars.idmiN5 = -1;
	userVars.idmiN6 = -1;

	/* variables for bulk Dzyaloshinsky-Moria interaction (square lattice) */

	userVars.dmiBulk = 0.0;
	userVars.bdmiBulk = false;

	/* variables for anisotropy */

	/* the first vector */
	userVars.e11 = 1.0;
	userVars.e12 = 0.0;
	userVars.e13 = 0.0;
	userVars.be11 = true;
	userVars.be12 = true;
	userVars.be13 = true;
	userVars.ie11 = -1;
	userVars.ie12 = -1;
	userVars.ie13 = -1;

	/* the second vector */
	userVars.e21 = 0.0;
	userVars.e22 = 1.0;
	userVars.e23 = 0.0;
	userVars.be21 = true;
	userVars.be22 = true;
	userVars.be23 = true;
	userVars.ie21 = -1;
	userVars.ie22 = -1;
	userVars.ie23 = -1;

	/* the third vector */
	userVars.e31 = 0.0;
	userVars.e32 = 0.0;
	userVars.e33 = 1.0;
	userVars.be31 = true;
	userVars.be32 = true;
	userVars.be33 = true;
	userVars.ie31 = -1;
	userVars.ie32 = -1;
	userVars.ie33 = -1;

	/* type of anisotropy */
	userVars.anisotropyType = ANIS_CONST;

	/* geometrical parameters */
	userVars.N1 = 0;
	userVars.N2 = 0;
	userVars.N3 = 0;

	userVars.a = 0.3;
	userVars.ba = true;

	userVars.ix = -1;
	userVars.iy = -1;
	userVars.iz = -1;

	/* mechanical parameters */
	userVars.mass = 0;
	userVars.imass = -1;

	/* magnetic parameters */
	userVars.imx = -1;
	userVars.imy = -1;
	userVars.imz = -1;

	userVars.iJm1 = -1;
	userVars.iJp1 = -1;
	userVars.iJm2 = -1;
	userVars.iJp2 = -1;
	userVars.iJm3 = -1;
	userVars.iJp3 = -1;

	userVars.g = 2.0;
	userVars.bg = true;

	userVars.muB = 1.0;
	userVars.bmuB = true;

	userVars.hbar = 1.0;
	userVars.bhbar = true;

	userVars.S = 0.5;
	userVars.bS = true;

	userVars.igilbertDamping = -1;

	/* anisotropy */
	userVars.ianiK1 = -1;
	userVars.ianiK2 = -1;
	userVars.ianiK3 = -1;
	userVars.ie11 = -1;
	userVars.ie12 = -1;
	userVars.ie13 = -1;
	userVars.ie21 = -1;
	userVars.ie22 = -1;
	userVars.ie23 = -1;
	userVars.ie31 = -1;
	userVars.ie32 = -1;
	userVars.ie33 = -1;

	userVars.aniK1 = 0.0;
	userVars.aniK2 = 0.0;
	userVars.aniK3 = 0.0;

	userVars.ianiInterK1m1 = -1;
	userVars.ianiInterK1p1 = -1;
	userVars.ianiInterK1m2 = -1;
	userVars.ianiInterK1p2 = -1;
	userVars.ianiInterK1m3 = -1;
	userVars.ianiInterK1p3 = -1;
	userVars.ianiInterK2m1 = -1;
	userVars.ianiInterK2p1 = -1;
	userVars.ianiInterK2m2 = -1;
	userVars.ianiInterK2p2 = -1;
	userVars.ianiInterK2m3 = -1;
	userVars.ianiInterK2p3 = -1;
	userVars.ianiInterK3m1 = -1;
	userVars.ianiInterK3p1 = -1;
	userVars.ianiInterK3m2 = -1;
	userVars.ianiInterK3p2 = -1;
	userVars.ianiInterK3m3 = -1;
	userVars.ianiInterK3p3 = -1;

	/* status of dipole interaction */
	userVars.dipOnOff = DIP_OFF;
	userVars.bdipOnOff = true;

	/* status of exchange interaction */
	userVars.exchOnOff = EXCH_ON;
	userVars.bexchOnOff = true;

	/* status of DMI */
	userVars.dmiOnOff = DMI_OFF;
	userVars.bdmiOnOff = true;

	/* type of exchange interaction */
	userVars.exchType = EXCH_UNIFORM;

	/* other parameters (precision of integrator) */
	userVars.critAngle = 10.0;
	userVars.bcritAngle = true;

	userVars.integrationStep = 1e-5;
	userVars.bintegrationStep = true;

	userVars.precision = 1e-11;
	userVars.bprecision = true;

	userVars.stopDmDt = -1;
	userVars.bstopDmDt = true;

	/* fixed huge value for optimization */
	userVars.lagrangeDistance = 1e5;//5000.0;
	userVars.blagrangeDistance = true;

	/* initial frame number */
	frameNumber = 0;

	/* value to stop optimization */
	userVars.stopValue = 0.0001;
	userVars.bstopValue = true;

	/* period to update optimization */
	userVars.updatePeriod = 1;
	userVars.bupdatePeriod = true;
	/* initial number of frames for optimization */
	numberFrames = 0;

	/* initialization parameters */

	/* magnetic distribution */
	userVars.magnInit = MI_UNIFORM;

	/* exchange integral coefficient distribution */
	userVars.exchInit = EI_UNIFORM;

	/* other parameters */
	userVars.fixedStep = FIXEDSTEP_OFF;
	userVars.bfixedStep = true;
	userVars.saveSLS = SLSSAVE_ON;
	userVars.saveBin = BINSAVE_ON;
	userVars.writingEnergy = WRITINGENERGY_OFF;
	userVars.writingBasis = WRITINGBASIS_OFF;
	userVars.writingExtField = WRITINGEXTFIELD_OFF;
	userVars.integrationMethod = INTEGRATION_METHOD_RKF;
	userVars.normalOnOff = NORMAL_ON;
	userVars.extField = EXTFIELD_CONST;
	userVars.launch = LAUNCH_SIMULATION;
	userVars.initPython = INIT_PYTHON_ON;
	//userVars.optimizationMethod = LBFGS;
	userVars.lengthResidue = 1e-4;
	userVars.restartOnOff = RESTART_OFF;

	/* default value to create .inp and .slsb files */
	userVars.backupPeriod = 1;
	userVars.snapshotPeriodMultiplier = 1;

	/* type of output files */
	userVars.outputFileType = UCD_ASCII;

}

int setupGilbertDamping(double **numbers, int indlp, int indn) {

	if (userVars.bgilbertDamping) {
		/* Gilbert damping alpha is given in the input file */
		latticeParam[indlp].gilbertDamping = userVars.gilbertDamping;
	} else if (userVars.igilbertDamping != -1) {
		/* Gilbert damping per lattice site is given in `paramFile` */
		if (numbers == NULL) {
			fprintf(flog, "(file %s | line %d) paramFile is not processed, while expected for gilbertDamping!\n", __FILE__, __LINE__);
			fprintf(stderr, "(file %s | line %d) paramFile is not processed, while expected for gilbertDamping!\n", __FILE__, __LINE__);
			return 1;
		}
		latticeParam[indlp].gilbertDamping = numbers[indn][userVars.igilbertDamping];
	} else {
		fprintf(flog, "(file %s | line %d) Gilbert damping 'gilbertDamping' is missing!\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Gilbert damping 'gilbertDamping' is missing!", __FILE__, __LINE__);
		return 1;
	}

	return 0;

}

int setupCoords(double **numbers, int indlp, int indn, int i1, int i2, int i3) {

	if (!numbers || (userVars.ix == -1 && userVars.iy == -1 && userVars.iz == -1)) {
		/* automatic coordinates according to the site indices */
		if (userVars.latticeType == LATTICE_CUBIC) {
			*(latticeParam[indlp].x) = i1;
			*(latticeParam[indlp].y) = i2;
			*(latticeParam[indlp].z) = i3;
		} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
			*(latticeParam[indlp].x) = i1 + 0.5 * ((i2 + 1) % 2);
			*(latticeParam[indlp].y) = 0.5 * sqrt(3) * i2;
			*(latticeParam[indlp].z) = i3;
		}
	} else if (userVars.ix >= 0 && userVars.iy >= 0 && userVars.iz >= 0) {
		*(latticeParam[indlp].x) = numbers[indn][userVars.ix] / userVars.a;
		*(latticeParam[indlp].y) = numbers[indn][userVars.iy] / userVars.a;
		*(latticeParam[indlp].z) = numbers[indn][userVars.iz] / userVars.a;
	} else {
		fprintf(flog, "(file %s | line %d) Cannot process sites coordinates! Check are they given either in cfg or dat file.\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Cannot process sites coordinates! Check are they given either in cfg or dat file.\n", __FILE__, __LINE__);
		return 1;
	}

	return 0;

}

int setupVelocities(double **numbers, int indlp, int indn) {

	if ((userVars.bvx) && (userVars.bvy) && (userVars.bvz) &&
	    (userVars.ix == -1) && (userVars.iy == -1) && (userVars.iz == -1)) {
		/* constant value */
		*(latticeParam[indlp].vx) = userVars.vx;
		*(latticeParam[indlp].vy) = userVars.vy;
		*(latticeParam[indlp].vz) = userVars.vz;
	} else if ((numbers) && (userVars.ix != -1) && (userVars.iy != -1) && (userVars.iz != -1)) {
		*(latticeParam[indlp].vx) = numbers[indn][userVars.ivx];
		*(latticeParam[indlp].vy) = numbers[indn][userVars.ivy];
		*(latticeParam[indlp].vz) = numbers[indn][userVars.ivz];
	} else {
		fprintf(flog, "(file %s | line %d) Cannot process sites initial velocities! Check are they given either in cfg or dat file.\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Cannot process sites initial velocities! Check are they given either in cfg or dat file.\n", __FILE__, __LINE__);
		return 1;
	}

	return 0;

}

int setupMagnDefault(double **numbers, int indlp, int indn) {

	if (!numbers || (userVars.imx == -1 && userVars.imy == -1 && userVars.imz == -1)) {
		/* default direction along Z */
		*(latticeParam[indlp].mx) = 0.0;
		*(latticeParam[indlp].my) = 0.0;
		*(latticeParam[indlp].mz) = 1.0;
	} else if (userVars.imx >= 0 && userVars.imy >= 0 && userVars.imz >= 0) {
		/* from file */
		*(latticeParam[indlp].mx) = numbers[indn][userVars.imx];
		*(latticeParam[indlp].my) = numbers[indn][userVars.imy];
		*(latticeParam[indlp].mz) = numbers[indn][userVars.imz];
	} else {
		fprintf(flog, "(file %s | line %d) cannot process magnetic moments 'mx', 'my', 'mz'! Check are they given either in cfg or dat file.\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) cannot process magnetic moments 'mx', 'my', 'mz'! Check are they given either in cfg or dat file.\n", __FILE__, __LINE__);
		return 1;
	}

	return 0;

}

int setupMagnActivity(double **numbers, int indlp, int indn) {

	if ((numbers) && (userVars.iMagn != -1) && (userVars.typeMagn == MAGN_FILE)) {
		/* from file */
		/* check and assign value from file */
		if (numbers[indn][userVars.iMagn] > 0.49999) {
			latticeParam[indlp].magnetic = 1;
		} else if (numbers[indn][userVars.iMagn] <= 0.49999) {
			latticeParam[indlp].magnetic = 0;
		} else {
			fprintf(flog, "(file %s | line %d) Definition of nonzero spin availability in the given site should be by number 0 or 1!\n", __FILE__,__LINE__);
			fprintf(stderr, "(file %s | line %d) Definition of nonzero spin availability in the given site should be by number 0 or 1!\n", __FILE__, __LINE__);
			return 1;
		}
	} else if (userVars.typeMagn == MAGN_ALL) {
		/* default active node */
		latticeParam[indlp].magnetic = 1;
	} else {
		/* return and write error */
		fprintf(flog, "(file %s | line %d) Cannot process list of magnetic sites! Check are they given either in cfg or dat file.\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Cannot process list of magnetic sites! Check are they given either in cfg or dat file.\n", __FILE__, __LINE__);
		return 1;
	}

	return 0;

}


int setupFlexibleParameters(double **numbers, int indlp, int indn) {

	if ((userVars.ibeta != -1) && (userVars.ilambda != -1) &&
	    (numbers) && (userVars.typeFlexParam == FLEXPARAM_FILE)) {
		/* from file */
		latticeParam[indlp].beta = numbers[indn][userVars.ibeta];
		latticeParam[indlp].lambda = numbers[indn][userVars.ilambda];
	} else if ((userVars.bbeta) && (userVars.blambda) &&
	           (userVars.typeFlexParam == FLEXPARAM_CONST)) {
		/* constant value */
		latticeParam[indlp].lambda = userVars.lambda;
		latticeParam[indlp].beta = userVars.beta;
	} /* else {
        char msg[] = "PROCESSING.C: Something wrong with flexible parameters (Are all of them given in paramFile?)!\n";
		fprintf(flog, "%s", msg);
		fprintf(stderr, "%s", msg);
		return 1;
    } */

	return 0;

}


int setupMechParameters(double **numbers, int indlp, int indn) {

	if ((userVars.imass != -1) && (userVars.imechDamping != -1) &&
	    (numbers) && (userVars.typeMechParam == MECHPARAM_FILE)) {
		/* from file */
		latticeParam[indlp].mass = numbers[indn][userVars.imass];
		latticeParam[indlp].mechDamping = numbers[indn][userVars.imechDamping];
	} else if ((userVars.bmass) && (userVars.bmechDamping) &&
	           (userVars.typeMechParam == MECHPARAM_CONST)) {
		/* constant value */
		latticeParam[indlp].mass = userVars.mass;
		latticeParam[indlp].mechDamping = userVars.mechDamping;
	} else {
		/* message about error */
		fprintf(flog, "(file %s | line %d) Something wrong with mechanical parameters (Are all of them given in paramFile?)!\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Something wrong with mechanical parameters (Are all of them given in paramFile?)!\n", __FILE__, __LINE__);
		return 1;
	}

	return 0;

}

int setupNormalVectorsSquareLattice(double **numbers, int indlp, int indn) {

	if (numbers && (userVars.inrmX == -1) && (userVars.inrmY == -1) && (userVars.inrmZ == -1)) {
		/* from .dat file */
		latticeParam[indlp].nrmX = numbers[indn][userVars.inrmX];
		latticeParam[indlp].nrmY = numbers[indn][userVars.inrmY];
		latticeParam[indlp].nrmZ = numbers[indn][userVars.inrmZ];
	} else if ((userVars.bnrmX) && (userVars.bnrmY) && (userVars.bnrmZ)) {
		/* from .cfg file */
		latticeParam[indlp].nrmX = userVars.nrmX;
		latticeParam[indlp].nrmY = userVars.nrmY;
		latticeParam[indlp].nrmZ = userVars.nrmZ;
	} else {
		/* message about error */
		fprintf(flog, "(file %s | line %d) Something wrong with normal vectors for square lattice (Are all of them given in paramFile (nrmX, nrmY, nrmZ)?)!", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Something wrong with normal vectors for square lattice (Are all of them given in paramFile (nrmX, nrmY, nrmZ)?)!", __FILE__, __LINE__);
		return 1;
	}

	return 0;

}

int initLattice(double **numbers) {

	/* choose system of units */
	if (userVars.units == UNIT_SI) {
		setupSI();
	} else if (userVars.units == UNIT_NORM) {
		setupNorm();
	}

	/* write information about specific anisotropy for flexible system */
	if (userVars.flexOnOff == FLEX_ON) {
		char msg[] = "PROCESSING: Warning: only tangential anisotropy is implemented for 1-d flexible systems!\n";
		fprintf(flog, "#%s", msg);
		fprintf(stderr, "%s", msg);
	}

	/* check condition for middle point method */
	if ((userVars.integrationMethod == INTEGRATION_METHOD_MIDDLE_POINT) &&
	   (userVars.fixedStep != FIXEDSTEP_ON)) {
		fprintf(flog, "(file %s | line %d) Warning: fixedStep should be specified for middle point!\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Warning: fixedStep should be specified for middle point!\n", __FILE__, __LINE__);
	}

	/* fill pointers of "latticeParam" for magnetic moments, coordinates and velocities */
	for (int i3 = 0; i3 < userVars.N3; i3++) {
		for (int i2 = 0; i2 < userVars.N2; i2++) {
			for (int i1 = 0; i1 < userVars.N1; i1++) {
				size_t indlp = (size_t)IDX(i1, i2, i3);
				/* pointers to the `lattice` */
				latticeParam[indlp].mx = lattice + 3 * indlp;
				latticeParam[indlp].my = lattice + 3 * indlp + 1;
				latticeParam[indlp].mz = lattice + 3 * indlp + 2;
				latticeParam[indlp].x = lattice + 3 * (spinsNum + indlp);
				latticeParam[indlp].y = lattice + 3 * (spinsNum + indlp) + 1;
				latticeParam[indlp].z = lattice + 3 * (spinsNum + indlp) + 2;
				latticeParam[indlp].vx = lattice + 3 * (2 * spinsNum + indlp);
				latticeParam[indlp].vy = lattice + 3 * (2 * spinsNum + indlp) + 1;
				latticeParam[indlp].vz = lattice + 3 * (2 * spinsNum + indlp) + 2;
			}
		}
	}

	/* go through all sites (set general parameters) */
	for (int i1 = 0; i1 < userVars.N1; i1++) {
		for (int i2 = 0; i2 < userVars.N2; i2++) {
			for (int i3 = 0; i3 < userVars.N3; i3++) {
				if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
				    i3 < userVars.N3 - 1) {

					int indlp = IDX(i1, i2, i3);
					int ind = (((i1 - 1) * (userVars.N2 - 2) + i2 - 1) * (userVars.N3 - 2) + i3 - 1);

					/* indices */
					latticeParam[indlp].n1 = i1;
					latticeParam[indlp].n2 = i2;
					latticeParam[indlp].n3 = i3;
					latticeParam[indlp].magnetic = true;

					/* initialize using main way (constant or from file) */

					/* initialize coordinates (only if it is not restarting) */
					if (userVars.restartOnOff == RESTART_OFF) {
						if (setupCoords(numbers, indlp, ind, i1, i2, i3)) {
							return 1;
						}
					}

					/* initialize components of velocity */
					if (setupVelocities(numbers, indlp, ind)) {
						return 1;
					}

					/* initialize magnetic moments (only if it is not restarting) */
					if ((userVars.magnInit == MI_FILE) && (userVars.restartOnOff == RESTART_OFF)) {
						if (setupMagnDefault(numbers, indlp, ind)) {
							return 1;
						}
					}

					/* initialize exchange integral */
					if (userVars.exchInit != EI_SCRIPT) {
						if (setupExchange(numbers, indlp, ind)) {
							return 1;
						}
					}

					/* initialize Gilbert damping */
					if (setupGilbertDamping(numbers, indlp, ind)) {
						return 1;
					}

					/* initialize coefficient of anisotropy */
					if (setupAniK(numbers, indlp, ind)) {
						return 1;
					}

					/* initialize ionic coefficient of anisotropy */
					if (setupInterAniK(numbers, indlp, ind)) {
						return 1;
					}

					/* initialize normal vectors for square lattice */
					/* if (setupNormalVectorsSquareLattice(numbers, indlp, ind)) {
                        return 1;
                    } */

					/* initialize external force */
					if ((userVars.extForce == EXTFORCE_CONST) || (userVars.extForce == EXTFORCE_HARMTIME)) {
						if (setupExtForce(numbers, indlp, ind)) {
							return 1;
						}
					}

					/* initialize frequency and phase of harmonic external force  */
					if (userVars.extForce == EXTFORCE_HARMTIME) {
						if (setupPhaseAndFreqExtForce(numbers, indlp, ind)) {
							return 1;
						}
					}

					/* initialize axes of anisotropy (vectors) */
					/* it is possible if system is not flexible */
					if (((userVars.anisotropyType == ANIS_FILE) || (userVars.anisotropyType == ANIS_CONST))
					    && (userVars.flexOnOff == FLEX_OFF)) {
						if (setupAnisotropyAxes(numbers, indlp, ind)) {
							return 1;
						}
					}

					/* default values of anisotropy axes for flexible system */
					if (userVars.flexOnOff == FLEX_ON) {

						latticeParam[indlp].e11 = 1.0;
						latticeParam[indlp].e12 = 0.0;
						latticeParam[indlp].e13 = 0.0;

						latticeParam[indlp].e21 = 0.0;
						latticeParam[indlp].e22 = 0.0;
						latticeParam[indlp].e23 = 0.0;

						latticeParam[indlp].e31 = 0.0;
						latticeParam[indlp].e32 = 0.0;
						latticeParam[indlp].e33 = 0.0;

						testAnisotropy = true;

					}

					/* initialize external field */
					if ((userVars.extField == EXTFIELD_FILE) || (userVars.extField == EXTFIELD_HARMTIME)) {
						if (setupExternalField(numbers, indlp, ind)) {
							return 1;
						}
					}
					
					/* initialize sinc time field */
					if (userVars.extField == EXTFIELD_SINC) {
						if (setupSincTField(numbers, indlp, ind)) {
							return 1;
						}
					}

					/* initialize frequency and phase of harmonic external field  */
					if (userVars.extField == EXTFIELD_HARMTIME) {
						if (setupPhaseAndFreqExtField(numbers, indlp, ind)) {
							return 1;
						}
					}

					/* initialize parameters for flexible system */
					if (setupFlexibleParameters(numbers, indlp, ind)) {
						return 1;
					}

					/* initialize mechanical parameters */
					if (setupMechParameters(numbers, indlp, ind)) {
						return 1;
					}

					/* initialize magnetization */
					if (setupMagnActivity(numbers, indlp, ind)) {
						return 1;
					}

					/* initialize vectors for Dzyaloshinsky-Moria interaction */
					if ((userVars.DMItype == DMI_FILE) || (userVars.DMItype == DMI_CONST)) {
						if (setupDMI(numbers, indlp, ind)) {
							return 1;
						}
					}

					/* initialize coefficients of bulk DMI interaction for cubic lattice */
					if (setupDMICubicBulk(numbers, indlp, ind)) {
						return 1;
					}

				}
			}
		}
	}

	/* go through all sites (set parameters for triangular lattice) */
	if (userVars.latticeType == LATTICE_TRIANGULAR) {
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {

						int indlp = IDX(i1, i2, i3);
						int ind = (((i1 - 1) * (userVars.N2 - 2) + i2 - 1) * (userVars.N3 - 2) + i3 - 1);

						/* initialize parameters of bending (triangular lattice) */
						if (setupExchangeTriangle(numbers, indlp, ind)) {
							return 1;
						}

						/* initialize parameters of bending (triangular lattice) */
						if (setupBendingTriangle(numbers, indlp, ind)) {
							return 1;
						}

						/* initialize parameters for flexible system (triangular lattice) */
						if (setupStretchingTriangle(numbers, indlp, ind)) {
							return 1;
						}

						/* initialize vectors for bulk Dzyaloshinsky-Moria interaction (triangular lattice) */
						if (setupDMITriangleBulk(numbers, indlp, ind)) {
							return 1;
						}

						/* initialize vectors for interface Dzyaloshinsky-Moria interaction (triangular lattice) */
						if (setupDMITriangleInterface(numbers, indlp, ind)) {
							return 1;
						}

					}
				}
			}
		}
	}

	/* go through all sites (set parameters for square lattice) */
	if (userVars.latticeType == LATTICE_CUBIC) {

		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {

						int indlp = IDX(i1, i2, i3);
						int ind = (((i1 - 1) * (userVars.N2 - 2) + i2 - 1) * (userVars.N3 - 2) + i3 - 1);

						/* initialize normal vectors for square lattice */
						if (setupNormalVectorsSquareLattice(numbers, indlp, ind)) {
							return 1;
						}

					}
				}
			}
		}

	}

	/* check whether Python script provides coordinates and geometry or not */
	if (pModulePython) {
		/* try to call "getGeomAndCoords" */
		int res = getCoordsAndGeomPy();
		if (res == 1) {
			/* problem with python script */
			printf("(file %s | line %d) Problem with Python script (field scriptFile in configuration file)!\n", __FILE__, __LINE__);
			return 1;
		}
		if (res == 2) {
			/* try to call "getGeom" */
			res = getGeomPy();
			if (res == 1) {
				/* problem with python script */
				printf("(file %s | line %d) Problem with Python script (field scriptFile in configuration file)!\n", __FILE__, __LINE__);
				return 1;
			}
		}
	}

	/* check whether exchange interaction supports */
	if (userVars.exchInit == EI_SCRIPT) {
		/* check whether Python script provides coordinates and geometry or not */
		if (pModulePython) {
			/* try to call "getExchange" */
			int res = getExchange();
			if (res == 1) {
				/* problem with python script */
				printf("(file %s | line %d) Problem with Python script (field scriptFile in configuration file)!\n", __FILE__, __LINE__);
				return 1;
			}
		}
	}

	/* calculate number of magnetic sites */
	magnSitesNum = 0;
	size_t indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					indexLattice = (size_t)IDX(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				/* magnetic moments */
				if (latticeParam[indexLattice].magnetic) {
					magnSitesNum++;
				}
			}
		}
	}

	/* initialize magnetic moments (only if is not restarting) */
	if ((userVars.magnInit != MI_FILE) && (userVars.restartOnOff == RESTART_OFF)) {
		if (magnInit()) {
			return 1;
		}
	}

	/* initialize Python variables for external magnetic field */
	if (initExtScriptField()) {
		return 1;
	}
	/* initialize external magnetic field */
	if (userVars.extField != EXTFIELD_FILE) {
		if (fieldSetup(userVars.t0)) {
			return 1;
		}
	}
	/* remove Python variables for external magnetic field */
	finalizeExtScriptField();

	/* initialize external force */
	if (userVars.extForce != EXTFORCE_CONST) {
		if (forceSetup(userVars.t0)) {
			return 1;
		}
	}

	/* initialize vectors for Dzyaloshinsky-Moria interaction */
	if (userVars.DMItype == DMI_SCRIPT) {
		if (scriptDMI()) {
			return 1;
		}
		/* variable to test becomes "true" */
		testDMI = true;
	}

	/* initialize vectors for anisotropy */
	/* it is possible if system is not flexible */
	if ((userVars.anisotropyType == ANIS_SCRIPT) && (userVars.flexOnOff == FLEX_OFF)) {
		if (scriptAnisotropy()) {
			return 1;
		}
		/* variable to test becomes "true" */
		testAnisotropy = true;
	}

	/* check test variable for DMI */
	if (!testDMI) {
		/* return and write error */
		/*char msg[] = "PROCESSING.C: Something wrong with DMI parameters (Are all of them given in paramFile or Python script?)!\n";
		fprintf(flog, "%s", msg);
		fprintf(stderr, "%s", msg);
        return 1;*/
	}

	/* check test variable for anisotropy */
	if (!testAnisotropy) {
		/* return and write error */
		/*char msg[] = "PROCESSING.C: Something wrong with parameters for anisotropy (Are all of them given in paramFile or Python script?)!\n";
		fprintf(flog, "%s", msg);
		fprintf(stderr, "%s", msg);
        return 1;*/
	}

	/* check availability of variables for triangular lattice */
	if ((indVarTri) && (userVars.latticeType == LATTICE_CUBIC)) {
		/* if user enters variables for triangular lattice,
		 * but lattice is not triangular, it will be error */
		fprintf(flog, "(file %s | line %d) Lattice is not triangular, but you have entered variable for triangular lattice (Are these variables necessary for you?)!\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) Lattice is not triangular, but you have entered variable for triangular lattice (Are these variables necessary for you?)!\n", __FILE__, __LINE__);
		return 1;
	}

	/* initialize connectivity graph */
	if (userVars.latticeType == LATTICE_CUBIC) {
		connectivityCubic();
	} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
		connectivityTriangular();
	}

	/* default value to return */
	return 0;

}

int printGeneralInfoToLog(void) {

	if (!(userVars.N1 > 3 && userVars.N2 > 3 && userVars.N3 > 3)) {
		if (!(userVars.N1 >= 3 && userVars.N2 == 3 && userVars.N3 == 3) &&
		    !(userVars.N1 > 3 && userVars.N2 > 3 && userVars.N3 == 3)) {
			/* dimension of ultra thin film */
			fprintf(flog, "(file %s | line %d): 2D systems should be in N2-N3 surface (N1 == 1)!\nSpin chains should have N1 and N2 dimensions equal to 1!\n", __FILE__, __LINE__);
			fprintf(stderr, "(file %s | line %d): 2D systems should be in N2-N3 surface (N1 == 1)!\nSpin chains should have N1 and N2 dimensions equal to 1!\n", __FILE__, __LINE__);
			return 1;
		}
	}

	/* find dimension of system of magnetic moments */
	userVars.dimensionOfSystem = 3;
	if (userVars.N3 == 3)
		userVars.dimensionOfSystem--;
	if (userVars.N2 == 3)
		userVars.dimensionOfSystem--;

	/* printing to init log */
	if (userVars.restartOnOff == RESTART_OFF) {

		fprintf(flog, "# Unit system: ");
		if (userVars.units == UNIT_NORM) {
			fprintf(flog, "reduced\n");
		} else if (userVars.units == UNIT_SI) {
			fprintf(flog, "SI\n");
		}
		fprintf(flog, "####### Lattice parameters #######\n");
		fprintf(flog, "# Lattice size N1      = %6d\n", userVars.N1 - 2);
		fprintf(flog, "# Lattice size N2      = %6d\n", userVars.N2 - 2);
		fprintf(flog, "# Lattice size N3      = %6d\n", userVars.N3 - 2);
		fprintf(flog, "# Lattice constant a   = %16g %s\n", userVars.a, userVars.u_distance);
		fprintf(flog, "# Lande factor g       = %16g\n", userVars.g);
		fprintf(flog, "# Spin length S        = %16g\n", userVars.S);
		fprintf(flog, "# Planck constant hbar = %16g %s\n", userVars.hbar, userVars.u_hbar);
		fprintf(flog, "# Bohr magneton muB    = %16g %s\n", userVars.muB, userVars.u_mub);
		fprintf(flog, "# Vacuum permeability  = %16g %s\n", userVars.mu0, userVars.u_mu0);
		if (userVars.dipOnOff == DIP_ON) {
			fprintf(flog, "# Dipolar interaction is ENABLED\n");
		} else {
			fprintf(flog, "# Dipolar interaction is DISABLED\n");
		}
		switch (userVars.magnInit) {
			case MI_FILE:
				fprintf(flog, "# Initial magnetization distribution is given by file\n");
				break;
			case MI_RANDOM:
				fprintf(flog, "# Initial magnetization distribution is random\n");
				break;
			case MI_UNIFORM:
				fprintf(flog, "# Initial magnetization distribution along (%lg, %lg, %lg)\n", userVars.mx0,
				        userVars.my0,
				        userVars.mz0);
				break;
			case MI_SLSB:
				fprintf(flog, "# Initial magnetization distribution is given by file '%s'\n", userVars.magnFile);
				break;
			case MI_SCRIPT:
				fprintf(flog, "# Initial magnetization distribution is given by Python script '%s'\n", userVars.magnFile);
				break;
		}
		fprintf(flog, "# File with lattice parameters paramFile = '%s'\n", userVars.paramFile);
		CalculateEnergyOfUniformState();
		fprintf(flog, "# Sites with nonzero spin = %ld\n", userVars.nonzeroSpinsNum);
		fprintf(flog, "####### Integrator parameters #######\n");
		fprintf(flog, "# Starting time = %lg %s\n", userVars.t0, userVars.u_time);
		fprintf(flog, "# Final time = %lg %s\n", userVars.tfin, userVars.u_time);
		fprintf(flog, "# Frame time = %lg %s\n", userVars.frame, userVars.u_time);
		fprintf(flog, "# Integration method ");
		switch (userVars.integrationMethod) {
			case INTEGRATION_METHOD_RKF:
				fprintf(flog, "Runge-Kutta-Fehlber of 4-5 order\n");
				break;
			case INTEGRATION_METHOD_MIDDLE_POINT:
				fprintf(flog, "Middle point\n");
				break;
			default:
				fprintf(flog, "UNDEFINED\n");
		}
		if (userVars.fixedStep == FIXEDSTEP_ON) {
			fprintf(flog, "# Fixed step is ENABLED\n");
		} else {
			fprintf(flog, "# Fixed step is DISABLED\n");
		}
		fprintf(flog, "# Maximal angle of rotation per internal integrator step (degrees) = %lg\n", userVars.critAngle);
		fprintf(flog, "# Stopping |d\\vec{m}/dt| = %lg %s\n", userVars.stopDmDt, userVars.u_circfreq);
		fprintf(flog, "# Initial integration step = %lg\n", userVars.integrationStep);
		fprintf(flog, "# Integrator precision = %lg\n", userVars.precision);
		fprintf(flog, "#\n");
		fflush(flog);

	}

	/* printing to .tbl file */
	if (userVars.restartOnOff == RESTART_OFF) {

		//char * ftblname[MAX_STR_LEN];
		char *ftblname = malloc(strlen(userVars.projName) + 5);
		sprintf(ftblname, "%s.tbl", userVars.projName);
		/*strcpy(ftblname, userVars.projName);
		strcat(ftblname, ".tbl");*/
		FILE *ftbl = fopen(ftblname, "w");
		free(ftblname);
		fprintf(ftbl, "# %s\n#\n", "Table of spin lattice parameters");

		/* column numbers */
		fprintf(ftbl, "#      1      2      3      4");
		fprintf(ftbl,
		        "                5                6                7                8                9                9");
		fprintf(ftbl, "               10               11");
		fprintf(ftbl, "               12               13               14");
		fprintf(ftbl, "               15               16               17");
		fprintf(ftbl, "               18               19               20");
		fprintf(ftbl,
		        "               21               22               23               24               25               26");
		fprintf(ftbl, "               27               28               29");
		fprintf(ftbl,
		        "               30               31               32               33               34               35");
		fprintf(ftbl,
		        "               36               37               38               39               40               41");
		fprintf(ftbl,
		        "               42               43               44               45               46               47");
		fprintf(ftbl,
		        "               48               49               50               51               52               53");
		fprintf(ftbl,
		        "               54               55               56               57               58               59");
		fprintf(ftbl,
		        "               60               61               62               63               64               65");
		fprintf(ftbl, "               66               67               68               69");

		fprintf(ftbl, "\n");

		/* headers */
		fprintf(ftbl, "#  Index Coord1 Coord2 Coord3");
		fprintf(ftbl,
		        "                x                y                z               mx               my               mz");
		fprintf(ftbl, "             mass   gilbertDamping");
		fprintf(ftbl, "              e11              e12              e13");
		fprintf(ftbl, "              e21              e22              e23");
		fprintf(ftbl, "              e31              e32              e33");
		fprintf(ftbl,
		        "              Jm1              Jp1              Jm2              Jp2              Jm3              Jp3");
		fprintf(ftbl, "            aniK1            aniK2            aniK3");
		fprintf(ftbl,
		        "     aniInterK1m1     aniInterK1p1     aniInterK1m2     aniInterK1p2     aniInterK1m3     aniInterK1p3");
		fprintf(ftbl,
		        "     aniInterK2m1     aniInterK2p1     aniInterK2m2     aniInterK2p2     aniInterK2m3     aniInterK2p3");
		fprintf(ftbl,
		        "     aniInterK3m1     aniInterK3p1     aniInterK3m2     aniInterK3p2     aniInterK3m3     aniInterK3p3");
		fprintf(ftbl,
		        "             D11p             D12p             D13p             D11m             D12m             D13m");
		fprintf(ftbl,
		        "             D21p             D22p             D23p             D21m             D22m             D23m");
		fprintf(ftbl,
		        "             D31p             D32p             D33p             D31m             D32m             D33m");
		fprintf(ftbl, "               Bx               By               Bz        magnActiv");

		fprintf(ftbl, "\n");

		/* columns */
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {

					/*
                    int indlp = IDX(i1, i2, i3);
					int ind = (((i1 - 1) * (userVars.N2 - 2) + i2 - 1) * (userVars.N3 - 2) + i3 - 1);

					fprintf(ftbl, "  %6d %6d %6d %6d %16g %16g %16g %16g %16g %16g", ind, i1, i2, i3,
							*(latticeParam[indlp].x), *(latticeParam[indlp].y), *(latticeParam[indlp].z),
							*(latticeParam[indlp].mx), *(latticeParam[indlp].my), *(latticeParam[indlp].mz)
					);

					fprintf(ftbl, " %16g %16g", latticeParam[indlp].mass, latticeParam[indlp].gilbertDamping
					);

					fprintf(ftbl, " %16g %16g %16g %16g %16g %16g %16g %16g %16g", latticeParam[indlp].e11,
							latticeParam[indlp].e12, latticeParam[indlp].e13, latticeParam[indlp].e21,
							latticeParam[indlp].e22, latticeParam[indlp].e23, latticeParam[indlp].e31,
							latticeParam[indlp].e32, latticeParam[indlp].e33
					);

					fprintf(ftbl, " %16g %16g %16g %16g %16g %16g", latticeParam[indlp].Jm1, latticeParam[indlp].Jp1,
							latticeParam[indlp].Jm2, latticeParam[indlp].Jp2, latticeParam[indlp].Jm3,
							latticeParam[indlp].Jp3
					);

					fprintf(ftbl, " %16g %16g %16g", latticeParam[indlp].aniK1, latticeParam[indlp].aniK2,
							latticeParam[indlp].aniK3
					);

					fprintf(ftbl, " %16g %16g %16g %16g %16g %16g", latticeParam[indlp].aniInterK1m1,
							latticeParam[indlp].aniInterK1m1, latticeParam[indlp].aniInterK1m2,
							latticeParam[indlp].aniInterK1m2, latticeParam[indlp].aniInterK1m3,
							latticeParam[indlp].aniInterK1m3
					);

					fprintf(ftbl, " %16g %16g %16g %16g %16g %16g", latticeParam[indlp].aniInterK2m1,
							latticeParam[indlp].aniInterK2m1, latticeParam[indlp].aniInterK2m2,
							latticeParam[indlp].aniInterK2m2, latticeParam[indlp].aniInterK2m3,
							latticeParam[indlp].aniInterK2m3
					);

					fprintf(ftbl, " %16g %16g %16g %16g %16g %16g", latticeParam[indlp].aniInterK3m1,
							latticeParam[indlp].aniInterK3m1, latticeParam[indlp].aniInterK3m2,
							latticeParam[indlp].aniInterK3m2, latticeParam[indlp].aniInterK3m3,
							latticeParam[indlp].aniInterK3m3
					);

					fprintf(ftbl, " %16g %16g %16g %16g %16g %16g", latticeParam[indlp].D11p, latticeParam[indlp].D12p,
							latticeParam[indlp].D13p, latticeParam[indlp].D11m, latticeParam[indlp].D12m,
							latticeParam[indlp].D13m
					);

					fprintf(ftbl, " %16g %16g %16g %16g %16g %16g", latticeParam[indlp].D21p, latticeParam[indlp].D22p,
							latticeParam[indlp].D23p, latticeParam[indlp].D21m, latticeParam[indlp].D22m,
							latticeParam[indlp].D23m
					);

					fprintf(ftbl, " %16g %16g %16g %16g %16g %16g", latticeParam[indlp].D31p, latticeParam[indlp].D32p,
							latticeParam[indlp].D33p, latticeParam[indlp].D31m, latticeParam[indlp].D32m,
							latticeParam[indlp].D33m
					);

					fprintf(ftbl, " %16g %16g %16g", latticeParam[indlp].Bxamp, latticeParam[indlp].Byamp,
							latticeParam[indlp].Bzamp
					);

					fprintf(ftbl, " %16i", latticeParam[indlp].magnetic
                    ); */

					fprintf(ftbl, "\n");
					fflush(ftbl);
				}
			}
		}
		fclose(ftbl);

	}

	return 0;

}

/* function to calculate average exchange integral in magnetic system */
double calculateAverageExchangeIntegral(void) {

	/* select specific type of exchange interaction */
	if (userVars.exchType == EXCH_UNIFORM) {
		/* check exchange integral of uniform distribution */
		return fabs(userVars.J);
	} else if (userVars.exchType == EXCH_INHOMOGENEOUS) {

		/* calculate total number of coefficients */
		int totalNumbers = 6 * (userVars.N1 - 2) * (userVars.N2 - 2) * (userVars.N3 - 2);
		/* accumulator of exchange integral */
		double Jsum = 0.0;

		/* pass through each magnetic moment */
		for (int i1 = 0; i1 < userVars.N1; i1++) {
			for (int i2 = 0; i2 < userVars.N2; i2++) {
				for (int i3 = 0; i3 < userVars.N3; i3++) {
					if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
					    i3 < userVars.N3 - 1) {

						/* number of spin in lattice */
						int indexLattice = IDX(i1, i2, i3);
						/* check if node is magnetic */
						if (latticeParam[indexLattice].magnetic) {
							/* calculate sum of all exchange integral coefficients */
							if (userVars.latticeType == LATTICE_CUBIC) {
								Jsum += fabs(latticeParam[indexLattice].Jp1) +
										fabs(latticeParam[indexLattice].Jm1) +
										fabs(latticeParam[indexLattice].Jp2) +
										fabs(latticeParam[indexLattice].Jm2) +
										fabs(latticeParam[indexLattice].Jp3) +
										fabs(latticeParam[indexLattice].Jm3);
							} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
								Jsum += fabs(latticeParam[indexLattice].J1) +
								        fabs(latticeParam[indexLattice].J2) +
								        fabs(latticeParam[indexLattice].J3) +
								        fabs(latticeParam[indexLattice].J4) +
								        fabs(latticeParam[indexLattice].J5) +
								        fabs(latticeParam[indexLattice].J6);
							}
						}

					}
				}
			}
		}

		/* return average exchange integral */
		return Jsum / totalNumbers;

	}

	/* return default answer */
	return 0.0;

}

void NormalizeLatticeParams(void) {

	/* auxiliary variable */
	/* double gyro = userVars.g * userVars.muB / userVars.hbar; */
	//double a = userVars.a;
	double damping = userVars.gilbertDamping;
	double Javer = calculateAverageExchangeIntegral();

	/* normalizing of energy */
	if (userVars.exchOnOff == EXCH_ON) {
		userVars.NormCoefficientEnergy = Javer * userVars.S * userVars.S;
	} else {
		userVars.NormCoefficientEnergy = 1e15 * userVars.mu0 * userVars.g * userVars.g * userVars.muB * userVars.muB /
		                                 (userVars.a * userVars.a * userVars.a);
	}

	/* normalizing of time */
	userVars.NormCoefficientTime = userVars.NormCoefficientEnergy /
	                               (userVars.hbar * userVars.S * (1.0 + damping * damping));
	userVars.t0Norm = userVars.t0 * userVars.NormCoefficientTime;
	userVars.tfinNorm = userVars.tfin * userVars.NormCoefficientTime;
	userVars.frameNorm = userVars.frame * userVars.NormCoefficientTime;
	userVars.integrationStepNorm = userVars.integrationStep * userVars.NormCoefficientTime;
	userVars.stopDmDt = userVars.stopDmDt * userVars.NormCoefficientTime;

	/* normalizing of dipole interaction, 1e15 from unit conversion */
	userVars.NormDipole = (userVars.muB * userVars.muB * userVars.g * userVars.g * userVars.mu0 * userVars.S * userVars.S) /
	                      (4 * M_PI * userVars.NormCoefficientEnergy * userVars.a * userVars.a * userVars.a);
	if ((userVars.units == UNIT_SI) && (userVars.exchOnOff == EXCH_ON)) {
		userVars.NormDipole *= 1e15;
	}

	/* normalizing of external field */
	userVars.NormExternal = (userVars.muB * userVars.g * userVars.S) / userVars.NormCoefficientEnergy;

	/* normalizing of Dzyaloshinsky-Moria interaction */
	userVars.NormDMI = (userVars.S * userVars.S) / userVars.NormCoefficientEnergy;

	for (int i1 = 0; i1 < userVars.N1; i1++) {
		for (int i2 = 0; i2 < userVars.N2; i2++) {
			for (int i3 = 0; i3 < userVars.N3; i3++) {
				if (i1 > 0 && i1 < userVars.N1 - 1 && i2 > 0 && i2 < userVars.N2 - 1 && i3 > 0 &&
				    i3 < userVars.N3 - 1) {
					int indlp = IDX(i1, i2, i3);

					/* division of the coefficient of anisotropy
					 into the coefficient of exchange interaction for normalization */
					latticeParam[indlp].aniK1 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].aniK2 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].aniK3 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);

					/* normalization of the coefficient of stretching, bending and mechanical damping parameter
					 * for energy of flexible system */
					latticeParam[indlp].beta /= userVars.NormCoefficientEnergy;
					latticeParam[indlp].lambda /= userVars.NormCoefficientEnergy;
					latticeParam[indlp].mechDamping *= 2.0 * userVars.a * userVars.a * userVars.NormCoefficientTime /
					                                   userVars.NormCoefficientEnergy;

					/* normalization of the coefficients of stretching
					 * for energy of flexible system (triangular lattice) */
					if (userVars.latticeType == LATTICE_TRIANGULAR) {
						latticeParam[indlp].lambda1 /= userVars.NormCoefficientEnergy;
						latticeParam[indlp].lambda2 /= userVars.NormCoefficientEnergy;
						latticeParam[indlp].lambda3 /= userVars.NormCoefficientEnergy;
						latticeParam[indlp].lambda4 /= userVars.NormCoefficientEnergy;
						latticeParam[indlp].lambda5 /= userVars.NormCoefficientEnergy;
						latticeParam[indlp].lambda6 /= userVars.NormCoefficientEnergy;
					}

					/* normalization of the coefficients of bending
 					* for energy of flexible system (triangular lattice) */
					if (userVars.latticeType == LATTICE_TRIANGULAR) {
						latticeParam[indlp].betaU1 /= userVars.NormCoefficientEnergy;
						latticeParam[indlp].betaU2 /= userVars.NormCoefficientEnergy;
						latticeParam[indlp].betaU3 /= userVars.NormCoefficientEnergy;
						latticeParam[indlp].betaD1 /= userVars.NormCoefficientEnergy;
						latticeParam[indlp].betaD2 /= userVars.NormCoefficientEnergy;
						latticeParam[indlp].betaD3 /= userVars.NormCoefficientEnergy;
					}

					/* normalization of external force */
					latticeParam[indlp].Fx *= userVars.a / userVars.NormCoefficientEnergy;
					latticeParam[indlp].Fy *= userVars.a / userVars.NormCoefficientEnergy;
					latticeParam[indlp].Fz *= userVars.a / userVars.NormCoefficientEnergy;

					/* division by normalization coefficient
					of Dzyaloshinsky-Moria vectors for simplification of calculations */
					/* the first axis */
					latticeParam[indlp].D11p /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].D12p /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].D13p /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);

					latticeParam[indlp].D11m /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].D12m /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].D13m /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);

					/* the second axis */
					latticeParam[indlp].D21p /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].D22p /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].D23p /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);

					latticeParam[indlp].D21m /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].D22m /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].D23m /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);

					/* the second axis */
					latticeParam[indlp].D31p /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].D32p /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].D33p /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);

					latticeParam[indlp].D31m /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].D32m /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].D33m /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					
					/* bulk DMI in triangular lattice */
					latticeParam[indlp].dmiB1 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].dmiB2 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].dmiB3 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].dmiB4 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].dmiB5 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].dmiB6 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);

					/* interface DMI in triangular lattice */
					latticeParam[indlp].dmiN1 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].dmiN2 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].dmiN3 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].dmiN4 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].dmiN5 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);
					latticeParam[indlp].dmiN6 /= userVars.NormCoefficientEnergy / (userVars.S * userVars.S);

					if (userVars.exchType == EXCH_INHOMOGENEOUS) {
						if (userVars.latticeType == LATTICE_CUBIC) {
							/* exchange integral for cubic lattice */
							latticeParam[indlp].Jp1 /= Javer;
							latticeParam[indlp].Jm1 /= Javer;
							latticeParam[indlp].Jp2 /= Javer;
							latticeParam[indlp].Jm2 /= Javer;
							latticeParam[indlp].Jp3 /= Javer;
							latticeParam[indlp].Jm3 /= Javer;
						} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
							/* exchange integral for triangular lattice */
							latticeParam[indlp].J1 /= Javer;
							latticeParam[indlp].J2 /= Javer;
							latticeParam[indlp].J3 /= Javer;
							latticeParam[indlp].J4 /= Javer;
							latticeParam[indlp].J5 /= Javer;
							latticeParam[indlp].J6 /= Javer;
						}
					}

				}
			}
		}
	}

}

double CalculateMagneticMoment(size_t index) {

	/* calculate length of magnetic moment vector */
	return sqrt(lattice[index] * lattice[index] + lattice[index + 1] * lattice[index + 1] +
	            lattice[index + 2] * lattice[index + 2]);

}

void CalculateEnergyOfUniformState(void) {

	/* initial amount of interactions is equal to zero */
	userVars.nonzeroSpinsNum = 0;
	/* size_t p1, p2, p3, m1, m2, m3 */
	size_t curIndex;

	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {

				curIndex = IDXmg(i1, i2, i3);
				/* if spin is magnetic, then we will continue to work */
				if (CalculateMagneticMoment(curIndex) >= NEAR_ZERO) {
					userVars.nonzeroSpinsNum++;
					/* improved algorithm */
					/* p1 = curIndex + 3;
					p2 = curIndex + 3 * userVars.N3;
					p3 = curIndex + 3 * userVars.N3 * userVars.N2;
					m1 = curIndex - 3;
					m2 = curIndex - 3 * userVars.N3;
					m3 = curIndex - 3 * userVars.N3 * userVars.N2; */
					/* if neighboring spins are also magnetic, then we will increase amount of exchange interactions */
					/*if (CalculateMagneticMoment(p1) >= NEAR_ZERO)
						nonzeroSpinsNum++;
					if (CalculateMagneticMoment(p2) >= NEAR_ZERO)
						nonzeroSpinsNum++;
					if (CalculateMagneticMoment(p3) >= NEAR_ZERO)
						nonzeroSpinsNum++;
					if (CalculateMagneticMoment(m1) >= NEAR_ZERO)
						nonzeroSpinsNum++;
					if (CalculateMagneticMoment(m2) >= NEAR_ZERO)
						nonzeroSpinsNum++;
					if (CalculateMagneticMoment(m3) >= NEAR_ZERO)
						nonzeroSpinsNum++;*/
				}

			}
		}
	}
	/* reduce number of interactions twice because of repetitions */
	//nonzeroSpinsNum = nonzeroSpinsNum / 2;
	/* calculate value of normalized energy of uniform state for correction
	 * (with and without exchange interaction) */
	if (userVars.exchOnOff == EXCH_ON) {
		EnergyCorrectionNorm = userVars.nonzeroSpinsNum;
	} else {
		EnergyCorrectionNorm = 0.0;
	}

}

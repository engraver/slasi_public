#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <getopt.h>
#include <Python.h> 

#include "simulation.h"
#include "simulation_int.h"
#include "utils/utils.h"
#include "utils/parallel_utils.h"
#include "utils/synchronize.h"
#include "readdata/readdata.h"
#include "processing/processing.h"
#include "processing/interface_memory.h"
#include "integration/integration.h"
#include "interactions/dipole/dipole.h"
#include "alltypes.h"

/********************************************************************
 *
 * Function bodies
 *
 *******************************************************************/

int processCMDoptions(int argc, char **argv) {

	opterr = 0;
	int c;

	userVars.paramFile = malloc(1);
	userVars.paramFile[0] = 0;
	userVars.pathToWrite = malloc(1);
	userVars.pathToWrite[0] = 0;
	silentMode = false;
	userVars.units = UNIT_SI;

	/* reset function getopt */
	optind = 0;

	/* find all options */
	while ((c = getopt(argc, argv, "p:P:shu:r:")) != -1) {

		/* go through all options */
		switch (c) {
			case 'p':
				/* paramFile */
				userVars.paramFile = realloc(userVars.paramFile, strlen(optarg) + 1);
				strcpy(userVars.paramFile, optarg);
				break;
			case 'P':
				/* path to write */
				userVars.pathToWrite = realloc(userVars.pathToWrite, strlen(optarg) + 1);
				strcpy(userVars.pathToWrite, optarg);
				break;
			case 's':
				/* silent mode */
				silentMode = true;
				break;
			case 'u':
				/* units */
				if (strcmp(optarg, "SI") == 0) {
					userVars.units = UNIT_SI;
				} else if (strcmp(optarg, "norm") == 0) {
					userVars.units = UNIT_NORM;
				} else {
					fprintf(stderr, "Option -u requires an argument, units system, SI or norm, but '%s' given.\n",
					        optarg);
					return 1;
				}
				break;
			case 'r':
				/* path of file to restart simulation*/
				userVars.restartFile = realloc(userVars.restartFile, strlen(optarg) + 1);
				strcpy(userVars.restartFile, optarg);
				/* indicator to launch restarting */
				userVars.restartOnOff = RESTART_ON;
				break;
			case 'h':
				/* print help */
				messageToStream("Spin Lattice Simulator SLaSi\n\n"
				                "Usage: $ slasi mySimulation.cfg\n"
				                "Options:\n"
				                "  -p paramFile.dat   Table with parameters of lattice per each site\n"
				                "  -P PATH            Another path for saving files instead of current directory\n"
				                "  -s                 Silent mode (print only error messages to console)\n"
				                "  -r file.slsb       Restart with the given file as initial state\n"
						, stdout);
				break;
			case '?':
				if (optopt == 'p') {
					messageToStream("Option -p requires an argument, name of paramFile (table with values per lattice site).\n", stderr);
				}
				if (optopt == 'P') {
					messageToStream("Option -P requires an argument, path where simulation should be stored.\n", stderr);
				}
				if (optopt == 'u') {
					messageToStream("Option -u requires an argument, units system, SI or norm.\n", stderr);
				}
				if (optopt == 'r') {
					messageToStream("Option -r requires an argument, name of slsb file as initial state.\n", stderr);
				}
				return 1;
			default:
				return 1;
		}

	}

	return 0;

}

int processSimulationParameters(int argc, char **argv) {

	/* check id of process for execution of function (for MPI) */
	if (!doItInMasterProcess()) {
		return 0;
	}

	/* check number of elements for array "argv" */
	if (argc < 2) {
		messageToStream("SIMULATION.C: Configuration file is not specified!", stderr);
		return 1;
	}

	/* reading input file with general parameters */
	if (!silentMode) {
		messageToStream("Reading configuration file...", stdout);
	}
	if (readConfig(argv[1])) {
		fclose(flog);
		messageToStream("SIMULATION.C: readInp() failed.", stderr);
		return 1;
	}

	/* checking for triangular lattice (only curve surface) */
	if ((userVars.N3 != 1) && (userVars.latticeType == LATTICE_TRIANGULAR)) {
		messageToStream("SIMULATION.C: N3 must be only 1 for triangular lattice (only curve surface)!", stdout);
		return 1;
	}
	/* increase each dimension by 2 for lateral zero arrays */
	userVars.N1 += 2;
	userVars.N2 += 2;
	userVars.N3 += 2;
	/* translate degrees into radians */
	userVars.critAngle = (userVars.critAngle * M_PI) / 180.0;

	/* create logfile and rewrite input file into logfile */
	initLog(argv[1]);

	/* create zero system array ("lattice" and "latticeParam") */
	if (setupArrays()) {
		fclose(flog);
		messageToStream("SIMULATION.C: setupArrays() failed.", stderr);
		return 1;
	}

	return 0;

}

int initializeArrays(void) {

	/* check id of process for execution of function (for MPI) */
	if (!doItInMasterProcess()) {
		return 0;
	}

	/* reading paramFile if it exists and write data to array of magnetic spins */
	double **numbers = NULL;
	if (userVars.paramFile[0]) {
		if (!silentMode) {
			printf("Reading paramFile...\n");
		}
		numbers = (double **) malloc(spinsNum * sizeof(double *));
		size_t i;
		for (i = 0; i < spinsNum; i++) {
			numbers[i] = (double *) malloc(MAX_HEADERS_NUM * sizeof(double));
		}
		if (readLatticeParams(userVars.paramFile, numbers)) {
			fclose(flog);
			messageToStream("SIMULATION.C: readLatticeParams() failed.", stderr);
			return 1;
		}
	}

	/* initialize lattice parameters */
	if (initLattice(numbers)) {
		freeArrays();
		fclose(flog);
		messageToStream("SIMULATION.C: initLattice() failed.", stderr);
		return 1;
	}

	/* free memory in case of unsuccessful reading */
	if (numbers) {
		size_t i;
		for (i = 0; i < spinsNum; i++) {
			free(numbers[i]);
			numbers[i] = NULL;
		}
		free(numbers);
		numbers = NULL;
	}

	/* read binary file for restarting */
	if (userVars.restartOnOff == RESTART_ON) {
		if (!silentMode) {
			messageToStream("Reading of binary file to restart...", stdout);
		}
		if (readAllBin(userVars.restartFile)) {
			fclose(flog);
			messageToStream("SIMULATION.C: readAllBin() failed.", stderr);
			return 1;
		}
	}

	return 0;

}

int initPython(void) {

	/* initialization of Python interpreter */
	Py_Initialize();

	/* creating name of module using filename  */
	char modname[MAX_STR_LEN];
	strcpy(modname, userVars.scriptFile);
	modname[strlen(modname) - 3] = 0;

	if (strlen(modname) > 0) {
		pNamePython = PyUnicode_DecodeFSDefault(modname);
		pModulePython = PyImport_Import(pNamePython);
		if (pModulePython == NULL) {
			PyErr_Print();
			sprintf(generalstr, "(%s, %d) Cannot load Python module:", __FILE__, __LINE__);
			messageToStream(generalstr, stderr);
			messageToStream(userVars.scriptFile, stderr);
			return 1;
		}
	}

	return 0;

}


int doInitialState(void) {

	/* check id of process for execution of function (for MPI) */
	if (!doItInMasterProcess()) {
		return 0;
	}

	/* write message about preparation work */
	if (!silentMode) {
		messageToStream("Preparation work...", stdout);
	}

	/* normalize vectors of magnetic moments */
	NormalizeVectorsInitial();

	/* check data before integration */
	if (printGeneralInfoToLog()) {
		freeArrays();
		fclose(flog);
		messageToStream("SIMULATION.C: printGeneralInfoToLog() failed.", stderr);
		return 1;
	}

	/* prepare data for integration */

	/* update orthonormal basis */
	UpdateOrthonormalBasis();
	/* update main direction for triangular lattice */
	if (userVars.latticeType == LATTICE_TRIANGULAR) {
		UpdateMainDirectionTriangle();
	}
	/* update normal vectors for triangular flexible lattice */
	if ((userVars.latticeType == LATTICE_TRIANGULAR) && (userVars.dimensionOfSystem == 2)) {
		UpdateNormalVectorTriangle();
		UpdateNormalVectorSite();
	}
	/* normalize parameters of system */
	NormalizeLatticeParams();
	/* calculate energy of uniform state for exchange interaction */
	CalculateEnergyOfUniformState();

	/* default result of execution */
	return 0;

}

int runSimulation(void) {

	/* variable to measure time */
	clock_t t = 0;

	/* only for master process */
	if (doItInMasterProcess()) {
		/* measure begin time */
		t = clock();
	}

	if (userVars.launch == LAUNCH_SIMULATION) {

		/* create arrays for simulation */
		createSimulationArrays();

		/* cache dipole variables (distances) to speed up calculation */
		if (userVars.flexOnOff == FLEX_OFF && userVars.dipOnOff == DIP_ON) {
			cacheDipoleVariables();
		}

		/* write message about begin of simulation */
		if (!silentMode) {
			messageToStream("Launching simulation...", stdout);
		}

		/* launch magnetic simulation */
		if (LaunchTimeDynamics(userVars.t0Norm, userVars.tfinNorm + 0.01 * userVars.frameNorm,
				               userVars.frameNorm, userVars.precision, true)) {
			destroySimulationArrays();
			fclose(flog);
			messageToStream("SIMULATION.C: LaunchTimeDynamics() failed.", stderr);
			return 1;
		}
		
		/* destroy arrays for simulation */
		destroySimulationArrays();

	} else if (userVars.launch == LAUNCH_MINIMIZATION) {

		/* energy minimization */
		/* launch optimization (search for minimum energy) */
		if (!silentMode) {
			messageToStream("Launching energy minimization...", stdout);
		}
		if (LaunchOptimization(true)) {
			freeArrays();
			fclose(flog);
			messageToStream("SIMULATION.C: LaunchOptimization() failed.", stderr);
			return 1;
		}

	} else if (userVars.launch == LAUNCH_HYSTERESIS) {

		/* hysteresis calculation */
		/* launch optimization (search for minimum energy) */
		if (!silentMode)  {
			messageToStream("Launching hysteresis...", stdout);
		}
		if (LaunchHysteresis(true)) {
			freeArrays();
			fclose(flog);
			messageToStream("SIMULATION.C: LaunchHysteresis() failed.", stderr);
			return 1;
		}

	}

	/* only for master process */
	if (doItInMasterProcess()) {
		/* measure difference between end and begin time */
		t = clock() - t;
		/* write time of execution */
		double tt =  ((double) t) / CLOCKS_PER_SEC;
		fprintf(flog, "# Finished in %lg seconds (%lg hours) of calculations.\n", tt, tt/60/60);
		fclose(flog);
	}

	return 0;

}

void initLog(char *inputFile) {

	/* write initial lines */
	fprintf(flog, "# SLaSi v%s\n# \n", VERSION);
	fprintf(flog, "# Input file: %s\n", inputFile);

	/* open inputfile and rewrite into logfile */
	FILE *fid = fopen(inputFile, "r");
	while (!feof(fid)) {
		char *str = NULL;
		size_t lineLength;
		ssize_t ind = getline(&str, &lineLength, fid);
		if (ind >= 0) {
			fprintf(flog, "# %s", str);
		}
		free(str);
	}
	fclose(fid);

	/* write the first row in logfile */
	fprintf(flog, "# \n#\n");

}

int slasi(int argc, char **argv, bool indEnvParallel) {

	/* measure time */
	srand((unsigned int)time(0));
	/* set default parameter */
	setDefaults();
	pModulePython = NULL;
	pNamePython = NULL;

	/* initialize environment */
	envInit(argv[1], indEnvParallel);

	/* read variable from terminal */
	if (doItInMasterProcess()) {
		if (argc >= 2) {

			if (argc == 2 && !strcmp("-h", argv[1])) {
				processCMDoptions(argc, argv);
				return 0;
			}

			if (processCMDoptions(argc - 1, argv + 1)) {
				return 1;
			}

			/* read scalar variables from binary file for additional data about names os file */
			if (userVars.restartOnOff == RESTART_ON) {
				readScalarVariablesBin(userVars.restartFile);
			}

			/* create descriptors for logfiles */
			createDescriptors(argv[1]);

		} else {
			/* return error */
			messageToStream("The first argument should be input file or -h key for help.\n", stderr);
			return 0;
		}

		/* write pid to special file */
		FILE * pid = fopen(FILENAME_PID, "w");
		fprintf(pid, "%d\n", getpid());
		fclose(pid);

	}

	/* print SLaSi version */
	if (!silentMode) {
		char buffer[DEFAULT_STR_LEN];
		int res = sprintf(buffer, "SLaSi v%s\n", VERSION);
		if (res > 0 && res < DEFAULT_STR_LEN - 1) {
			messageToStream(buffer, stdout);
		}
	}

	/* read configuration */
	if (processSimulationParameters(argc, argv)) {
		messageToStream("SIMULATION.C: processSimulationParameters() failed.", stderr);
		return 1;
	}

	/* initialize of Python environment if it is necessary */
	if ((userVars.initPython == INIT_PYTHON_ON) && (doItInMasterProcess())) {
		if (initPython()) {
			sprintf(generalstr, "(%s, %d) Failed to initialize Python library!", __FILE__, __LINE__);
			messageToStream(generalstr, stderr);
			return 1;
		}
	}

	/* initialize arrays for simulation */
	if (initializeArrays()) {
		sprintf(generalstr, "(%s, %d) initializeArrays() failed.", __FILE__, __LINE__);
		messageToStream(generalstr, stderr);
		return 1;
	}

	/* synchronize data ("lattice", "latticeParam", "Bamp" and "userVars") for processes with creation */
	synchronizeData(true);

	/* make "preparation work" (normalization, calculation of important vectors) */
	if (doInitialState()) {
		messageToStream("SIMULATION.C: initialState() failed.", stderr);
		return 1;
	}

	/* synchronize data ("lattice", "latticeParam" and "userVars") for processes */
	synchronizeData(false);

	/* make "main work" (simulation) */
	waitProcesses();
	int res = runSimulation();

	/* free memory */
	freeMemory();

	/* read about end of working with checking of id */
	if (!silentMode) {
		messageToStream("Finished!", stdout);
	}

	/* finalize environment */
	envFinalize(indEnvParallel);

	/* return result of working */
	return res;

}

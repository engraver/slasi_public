#ifndef _INDEX_NUMS_H_
#define _INDEX_NUMS_H_

#define CONN_FIRST_NGBR   0
#define CONN_SECOND_NGBR  1
#define CONN_THIRD_NGBR   2
#define CONN_FOURTH_NGBR  3
#define CONN_FIFTH_NGBR   4
#define CONN_SIXTH_NGBR   5
#define CONN_SEVENTH_NGBR 6
#define CONN_EIGHTH_NGBR  7

#endif
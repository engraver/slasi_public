#include <memory.h>
#include <stdlib.h>
#include "../alltypes.h"
#include "triangular.h"
#include "index_nums.h"


void connectivityTriangular(void) {

	/* setup indices of lattice sites for cell topology */
	int ind = 0;
	size_t indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			indexLattice = (size_t)IDXtr(i1, i2);
			nodeIndices[indexLattice] = ind;
			ind ++;
		}
	}

	connNeighbours = 3;

	maxConnElements = (2*(userVars.N2-2) - 2)*(userVars.N1-2 - 1); /* check '-2' or '-3' or '-1' should be here */
	connectivity = malloc(maxConnElements*sizeof(int*));
	memset(connectivity, 0, sizeof(int**)*maxConnElements);

	int i1, i2;
	connElements = 0;
	for (i1 = 1; i1 <= userVars.N1 - 2; i1 ++) {
		for (i2 = 1; i2 <= userVars.N2 - 1; i2 ++) {
			size_t indxi = IDXtr(i1, i2);
			size_t indx1 = IDXtr1(i1, i2);
			size_t indx2 = IDXtr2(i1, i2);
			size_t indx6 = IDXtr6(i1, i2);

			if (latticeParam[indxi].magnetic && latticeParam[indx1].magnetic && latticeParam[indx2].magnetic) {
				connectivity[connElements] = (int*)malloc(connNeighbours*sizeof(int));
				connectivity[connElements][CONN_FIRST_NGBR]  = nodeIndices[indxi];
				connectivity[connElements][CONN_SECOND_NGBR] = nodeIndices[indx1];
				connectivity[connElements][CONN_THIRD_NGBR]  = nodeIndices[indx2];
				connElements ++;
			}

			if (latticeParam[indx6].magnetic && latticeParam[indx1].magnetic && latticeParam[indxi].magnetic) {
				connectivity[connElements] = (int*)malloc(connNeighbours*sizeof(int));
				connectivity[connElements][CONN_FIRST_NGBR]  = nodeIndices[indx6];
				connectivity[connElements][CONN_SECOND_NGBR] = nodeIndices[indx1];
				connectivity[connElements][CONN_THIRD_NGBR]  = nodeIndices[indxi];
				connElements ++;
			}
		}
	}

}

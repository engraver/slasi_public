#include <memory.h>
#include <stdlib.h>
#include "../alltypes.h"
#include "cubic.h"
#include "index_nums.h"

#define NEIGHBOURS_IN_SPIN_CHAIN 2
#define NEIGHBOURS_IN_SQUARE_LATTICE 4
#define NEIGHBOURS_IN_BULK 8

void setupSpinChainConnectivity(void) {

	maxConnElements = (userVars.N1 - 3);
	connectivity = malloc(maxConnElements*sizeof(int*));
	memset(connectivity, 0, sizeof(int**)*maxConnElements);

	int i;
	connElements = 0;
	for (i = 1; i <= maxConnElements; i ++) {
		size_t indx1 = (size_t)IDX(i, 1, 1);
		size_t indx2 = (size_t)IDX(i + 1, 1, 1);
		if (latticeParam[indx1].magnetic && latticeParam[indx2].magnetic) {
			connectivity[connElements] = (int*)malloc(connNeighbours*sizeof(int));
			connectivity[connElements][CONN_FIRST_NGBR] = nodeIndices[indx1];
			connectivity[connElements][CONN_SECOND_NGBR] = nodeIndices[indx2];
			connElements ++;
        }
    }

}

void setupCylindricalConnectivity(void) {

	maxConnElements = (userVars.N1 - 3) * (userVars.N2 - 3) ;
	connectivity = malloc(maxConnElements*sizeof(int*));
	memset(connectivity, 0, sizeof(int**)*maxConnElements);

	int i1, i2;
	connElements = 0;
	for (i1 = 1; i1 <= userVars.N1 - 2; i1 ++) {
		for (i2 = 1; i2 <= userVars.N2 - 2; i2 ++) {
			size_t indx1 = (size_t)IDX(i1, i2, 1);
			size_t indx2 = (size_t)IDX(i1 + 1, i2, 1);
			size_t indx3 = (size_t)IDX(i1 + 1, i2 + 1, 1);
			size_t indx4 = (size_t)IDX(i1, i2 + 1, 1);
			if (latticeParam[indx1].magnetic && latticeParam[indx2].magnetic && latticeParam[indx3].magnetic && latticeParam[indx4].magnetic) {
				connectivity[connElements] = (int*)malloc(connNeighbours*sizeof(int));
				connectivity[connElements][CONN_FIRST_NGBR]  = nodeIndices[indx1];
				connectivity[connElements][CONN_SECOND_NGBR] = nodeIndices[indx2];
				connectivity[connElements][CONN_THIRD_NGBR]  = nodeIndices[indx3];
				connectivity[connElements][CONN_FOURTH_NGBR] = nodeIndices[indx4];
				connElements ++;
			}
		}
	}

}

void setupCubicConnectivity(void) {

	maxConnElements = (userVars.N1 - 3) * (userVars.N2 - 3) * (userVars.N3 - 3) ;
	connectivity = malloc(maxConnElements*sizeof(int*));
	memset(connectivity, 0, sizeof(int**)*maxConnElements);

	int i1, i2, i3;
	connElements = 0;
	for (i1 = 1; i1 <= userVars.N1 - 2; i1 ++) {
		for (i2 = 1; i2 <= userVars.N2 - 2; i2 ++) {
			for (i3 = 1; i3 <= userVars.N3 - 2; i3 ++) {
				size_t indx1 = (size_t)IDX(i1, i2, i3);
				size_t indx2 = (size_t)IDX(i1 + 1, i2, i3);
				size_t indx3 = (size_t)IDX(i1 + 1, i2 + 1, i3);
				size_t indx4 = (size_t)IDX(i1, i2 + 1, i3);
				size_t indx5 = (size_t)IDX(i1, i2, i3 + 1);
				size_t indx6 = (size_t)IDX(i1 + 1, i2, i3 + 1);
				size_t indx7 = (size_t)IDX(i1 + 1, i2 + 1, i3 + 1);
				size_t indx8 = (size_t)IDX(i1, i2 + 1, i3 + 1);
				if (latticeParam[indx1].magnetic && latticeParam[indx2].magnetic && latticeParam[indx3].magnetic && latticeParam[indx4].magnetic && latticeParam[indx5].magnetic && latticeParam[indx6].magnetic && latticeParam[indx7].magnetic && latticeParam[indx8].magnetic) {
					connectivity[connElements] = (int*)malloc(connNeighbours*sizeof(int));
					connectivity[connElements][CONN_FIRST_NGBR]   = nodeIndices[indx1];
					connectivity[connElements][CONN_SECOND_NGBR]  = nodeIndices[indx2];
					connectivity[connElements][CONN_THIRD_NGBR]   = nodeIndices[indx3];
					connectivity[connElements][CONN_FOURTH_NGBR]  = nodeIndices[indx4];
					connectivity[connElements][CONN_FIFTH_NGBR]   = nodeIndices[indx5];
					connectivity[connElements][CONN_SIXTH_NGBR]   = nodeIndices[indx6];
					connectivity[connElements][CONN_SEVENTH_NGBR] = nodeIndices[indx7];
					connectivity[connElements][CONN_EIGHTH_NGBR]  = nodeIndices[indx8];
					connElements ++;
				}
			}
		}
	}

}

void connectivityCubic(void) {

	/* setup indices of lattice sites for cell topology */
	int ind = 0;
	size_t indexLattice = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				indexLattice = (size_t)IDX(i1, i2, i3);
				nodeIndices[indexLattice] = ind;
				ind ++;
			}
		}
	}

	if ((userVars.N2 == 3) && (userVars.N3 == 3)) {
		/* spin chain */
		connNeighbours = NEIGHBOURS_IN_SPIN_CHAIN;
		setupSpinChainConnectivity();
	} else if (userVars.N3 == 3) {
		/* square lattice */
		connNeighbours = NEIGHBOURS_IN_SQUARE_LATTICE;
		setupCylindricalConnectivity();
	} else {
		/* bulk */
		connNeighbours = NEIGHBOURS_IN_BULK;
		setupCubicConnectivity();
	}

}

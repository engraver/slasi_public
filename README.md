# slasi_public

SLaSi is a spin-lattice simulator developed by [RiTM](http://ritm.knu.ua) group. [Project site](http://slasi.knu.ua).

This work represents an independent, third generation of the spin-lattice simulation tool, launched by the work of Jean Caputo et al for effective 2D spin-lattice simulations (Jean-Guy Caputo, Yuri Gaididei, Franz G. Mertens, Denis D. Sheka. _Vortex Polarity Switching by a Spin-Polarized Current_, Physical Review Letters, **98**, 056604 (2007), DOI: [10.1103/PhysRevLett.98.056604](https://doi.org/10.1103/PhysRevLett.98.056604)).

SLaSi is a powerful tool to analyze complex lattices in 3D including exchange, anisotropy, dipolar interaction and other contributions to the Hamiltonian in quasi-classical approach.

First SLaSi usage by generations:

- **Gen1** (Fortran code): Jean-Guy Caputo, Yuri Gaididei, Franz G. Mertens, Denis D. Sheka. _Vortex Polarity Switching by a Spin-Polarized Current_, Physical Review Letters, **98**, 056604 (2007), DOI: [10.1103/PhysRevLett.98.056604](https://doi.org/10.1103/PhysRevLett.98.056604)
- **Gen2**: Oleksandr V. Pylypovskyi, Denis D. Sheka, Yuri Gaididei. _Bloch point structure in a magnetic nanosphere_, Physical Review B, **85**, 224401 (2012), DOI: [10.1103/PhysRevB.85.224401](https://doi.org/10.1103/PhysRevB.85.224401)
- **Gen3**: Oleksandr V. Pylypovskyi, Denys Y. Kononenko, Kostiantyn V. Yershov, Ulrich K. Rößler, Artem V. Tomilo, Jürgen Fassbender, Jeroen van den Brink, Denys Makarov, and Denis D. Sheka. _Curvilinear One-Dimensional Antiferromagnets_, Nano Letters DOI: [10.1021/acs.nanolett.0c03246](https://doi.org/10.1021/acs.nanolett.0c03246)


If you are benefited from this code, please cite this repository and the following work (may be a subject of change in future):
- Oleksandr V. Pylypovskyi, Denys Y. Kononenko, Kostiantyn V. Yershov, Ulrich K. Rößler, Artem V. Tomilo, Jürgen Fassbender, Jeroen van den Brink, Denys Makarov, and Denis D. Sheka. _Curvilinear One-Dimensional Antiferromagnets_, Nano Letters DOI: [10.1021/acs.nanolett.0c03246](https://doi.org/10.1021/acs.nanolett.0c03246)
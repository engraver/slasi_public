cmake_minimum_required (VERSION 3.1)

project(SLaSi LANGUAGES CXX C)

Configure_file(${CMAKE_SOURCE_DIR}/parallel.none ${CMAKE_SOURCE_DIR}/parallel.h COPYONLY)
if(NOT DEFINED PARALLEL)
	set(PARALLEL NONE)
endif(NOT DEFINED PARALLEL)

if (${PARALLEL} STREQUAL MPI)
	find_package(MPI REQUIRED)
	set(CMAKE_C_COMPILER mpicc)
	configure_file(${CMAKE_SOURCE_DIR}/parallel.mpi ${CMAKE_SOURCE_DIR}/parallel.h COPYONLY)
endif (${PARALLEL} STREQUAL MPI)

if (${PARALLEL} STREQUAL CUDA)
	find_package(CUDA)
	set(CUDA_PROPAGATE_HOST_FLAGS OFF)
	set(CUDA_LIBRARIES PUBLIC ${CUDA_LIBRARIES})
endif (${PARALLEL} STREQUAL CUDA)

set(CMAKE_C_FLAGS "-D_GNU_SOURCE -O2 -W -Werror -g -Wno-unused-parameter -Wno-unused-result -Wsign-compare -DNDEBUG -fwrapv -Wall -Wstrict-prototypes -Xlinker -export-dynamic")

link_directories(${PROJECT_SOURCE_DIR}/libs/lib/python3.5/config-3.5m ${PROJECT_SOURCE_DIR}/libs/lib)

if (${PARALLEL} STREQUAL CUDA)
	cuda_add_executable(slasi main.c simulation.c
		utils/parallel_utils_memory_cuda.cu
		integration/integutils_cuda_memory.cu
		integration/integutils_cuda_diff.cu
		integration/diffutils_cuda.cu
		interactions/anisotropy/uniaxialAnisotropy.cu
		interactions/exchange/uniformexchange_triangle.cu
		interactions/exchange/uniformexchange.cu
		interactions/exchange/inhomexchange.cu
		interactions/dipole/dipole.cu
		interactions/externalField/externalField.cu
		interactions/dmi/dmi.cu
		interactions/stretching/stretching.cu
		processing/backend_memory.cu
	)
	cuda_add_executable(test_slasi main_test.c simulation.c
			utils/parallel_utils_memory_cuda.cu
			integration/integutils_cuda_memory.cu
			integration/integutils_cuda_diff.cu
			integration/diffutils_cuda.cu
			interactions/anisotropy/uniaxialAnisotropy.cu
			interactions/exchange/uniformexchange_triangle.cu
			interactions/exchange/uniformexchange.cu
			interactions/exchange/inhomexchange.cu
			interactions/dipole/dipole.cu
			interactions/externalField/externalField.cu
			interactions/dmi/dmi.cu
			interactions/stretching/stretching.cu
			processing/backend_memory.cu
	)
else(${PARALLEL} STREQUAL CUDA)
	add_executable(slasi main.c simulation.c)
	add_executable(test_slasi main_test.c simulation.c)
endif(${PARALLEL} STREQUAL CUDA)

target_compile_features(slasi PRIVATE c_std_99)
target_compile_features(test_slasi PRIVATE c_std_99)
if (${PARALLEL} STREQUAL CUDA)
	cuda_select_nvcc_arch_flags(ARCH_FLAGS)
endif(${PARALLEL} STREQUAL CUDA)

target_include_directories(slasi PRIVATE ${PROJECT_SOURCE_DIR}/libs/include/python3.5m ${PROJECT_SOURCE_DIR}/libs/include)
target_include_directories(test_slasi  PRIVATE ${PROJECT_SOURCE_DIR}/libs/include/python3.5m ${PROJECT_SOURCE_DIR}/libs/include)

target_sources(slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/globalvar.c)
target_sources(test_slasi PRIVATE ${CMAKE_CURRENT_LIST_DIR}/globalvar.c)

if (${PARALLEL} STREQUAL NONE)
	include(integration/CMakeLists-none.txt)
	include(interactions/CMakeLists-none.txt)
	include(utils/CMakeLists-none.txt)
	include(processing/CMakeLists-none.txt)
endif(${PARALLEL} STREQUAL NONE)
if (${PARALLEL} STREQUAL CUDA)
	include(integration/CMakeLists-cuda.txt)
	include(interactions/CMakeLists-cuda.txt)
	include(utils/CMakeLists-cuda.txt)
	include(processing/CMakeLists-cuda.txt)
endif(${PARALLEL} STREQUAL CUDA)
if (${PARALLEL} STREQUAL MPI)
	include(integration/CMakeLists-mpi.txt)
	include(interactions/CMakeLists-mpi.txt)
	include(utils/CMakeLists-mpi.txt)
	include(processing/CMakeLists-mpi.txt)
endif(${PARALLEL} STREQUAL MPI)
include(lattice/CMakeLists.txt)
include(magninit/CMakeLists.txt)
include(readdata/CMakeLists.txt)
include(writing/CMakeLists.txt)

target_link_libraries(slasi PRIVATE m python3.5m pthread dl util nlopt)
target_link_libraries(test_slasi  PRIVATE m python3.5m pthread dl util nlopt cunit)

configure_file(${CMAKE_SOURCE_DIR}/convSLS.in ${CMAKE_SOURCE_DIR}/convSLS)

add_custom_command(TARGET slasi POST_BUILD 
                COMMAND ${CMAKE_COMMAND} -E copy
                ${CMAKE_SOURCE_DIR}/convSLS
                ${CMAKE_CURRENT_BINARY_DIR}/convSLS)

#set_property(TARGET slasi PROPERTY C_STANDART 99)

#if (MPI)
#	add_custom_command(
#		OUTPUT ${CMAKE_SOURCE_DIR}/parallel.h
#		COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/parallel.mpi ${CMAKE_SOURCE_DIR}/parallel.h
#		DEPENDS ${CMAKE_SOURCE_DIR}/parallel.mpi
#	)
#endif (MPI)

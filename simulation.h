#ifndef _SIMULATION_H_
#define _SIMULATION_H_

#include <stdbool.h>

/**
 * @brief Replacement of main() funciton to start simulation independently on the environment
 * @param argc number of command line arguments
 * @param argv command line arguments
 * @param indEnd indicator to finalize or initialize environment
 */ 
int slasi(int argc, char **argv, bool indEnv);

/**
 * @brief Reads configuration `*.cfg` and paramfile `*.dat` files with simulation parameters
 * @param argc number of command line arguments (from `main()`)
 * @param argv list of command line arguments (from `main()`)
 * @return `0` for success and `1` for error
 */
int processSimulationParameters(int argc, char **argv);

#endif

#include "synchronize.h"
#include "../alltypes.h"

#ifdef PARALLEL_MPI
#include <mpi.h>
#endif

#ifdef PARALLEL_NONE

void synchronizeUserVars(void) {
}

void synchronizeLattice(void) {
}

void synchronizeBamp(void) {
}

void synchronizeLatticeParam(void) {
}

void synchronizeSystemArrayFullMagn(double *systemArray) {
}

void synchronizeSystemArrayPieceMagn(double *systemArray) {
}

void synchronizeSystemArrayFull(double *systemArray) {
}

void synchronizeSystemArrayPiece(double *systemArray) {
}

void synchronizeScalarValue(double *scalarValue) {
}

void synchronizeScalarValueAdd(double *scalarValue) {
}

void synchronizeScalarValueMax(double *scalarValue) {
}

void synchronizeProcesses(void) {
}

#endif

#ifdef PARALLEL_MPI
/* synchronize structure "userVars"
 * ("userVars" was calculated by master process and
 * this function will send this array to other processes) */
void synchronizeUserVars(void) {

	if (rank != RANK_MASTER) {
		/* receive "userVars" */
		MPI_Recv(&userVars, sizeof(UserVars), MPI_BYTE, RANK_MASTER, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	} else {
		/* send "userVars" */
		for (int i = 0; i < numberProc; i++) {
			if (i != RANK_MASTER) {
				MPI_Send(&userVars, sizeof(UserVars), MPI_BYTE, i, 0, MPI_COMM_WORLD);
			}
		}
	}

}

/* synchronize array "lattice"
 * ("lattice" was calculated by master process and
 * this function will send this array to other processes) */
void synchronizeLattice(void) {

	/* calculate number of spins */
	long spinsNumber = userVars.N1 * userVars.N2 * userVars.N3;

	if (rank != RANK_MASTER) {
		/* receive "lattice" */
		MPI_Recv(lattice, NUMBER_COMP * spinsNumber * sizeof(LatticeSiteSync), MPI_BYTE, RANK_MASTER, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	} else {
		/* send "lattice" */
		for (int i = 0; i < numberProc; i++) {
			if (i != RANK_MASTER) {
				MPI_Send(lattice, NUMBER_COMP * spinsNumber * sizeof(LatticeSiteSync), MPI_BYTE, i, 0, MPI_COMM_WORLD);
			}
		}
	}

}

/* synchronize array "Bamp"
 * ("Bamp" was calculated by master process and
 * this function will send this array to other processes) */
void synchronizeBamp(void) {

	/* calculate number of spins */
	long spinsNumber = userVars.N1 * userVars.N2 * userVars.N3;

	if (rank != RANK_MASTER) {
		/* receive "Bamp" */
		MPI_Recv(Bamp, NUMBER_COMP_FIELD * spinsNumber * sizeof(double), MPI_BYTE, RANK_MASTER, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	} else {
		/* send "Bamp" */
		for (int i = 0; i < numberProc; i++) {
			if (i != RANK_MASTER) {
				MPI_Send(Bamp, NUMBER_COMP_FIELD * spinsNumber * sizeof(double), MPI_BYTE, i, 0, MPI_COMM_WORLD);
			}
		}
	}

}

/* synchronize system array (coordinates, velocities, magnetic moments)
 * (array was calculated by master process and
 * this function will send this array to other processes) */
void synchronizeSystemArrayFullMagn(double * systemArray) {

	/* number of components in system array */
	size_t compNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	if (rank != RANK_MASTER) {
		/* receive array */
		MPI_Recv(systemArray, compNumber, MPI_DOUBLE, RANK_MASTER, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	} else {
		/* send array */
		for (int i = 0; i < numberProc; i++) {
			if (i != RANK_MASTER) {
				MPI_Send(systemArray, compNumber, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
			}
		}
	}

}

/* synchronize system array (coordinates, velocities, magnetic moments)
 * (array was calculated in parallel and
 * this function will combine different path of this array
 * in one array of master process) */
void synchronizeSystemArrayPieceMagn(double * systemArray) {

	/* begin shift for slave process */
	long beginShift;
	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	if (rank != RANK_MASTER) {
		/* calculate begin shifts */
		beginShift = IDXmg(rank * (userVars.N1 - 2) / numberProc + 1, 1, 1);
		/* send elements of system array */
		/* magnetic moments */
		MPI_Send(systemArray + beginShift, spinsNumber - beginShift, MPI_DOUBLE, RANK_MASTER, 0, MPI_COMM_WORLD);
	} else {
		/* go trough all processes */
		for (int i = 0; i < numberProc; i++) {
			if (i != RANK_MASTER) {
				/* calculate begin and end shifts */
				beginShift = IDXmg(i * (userVars.N1 - 2) / numberProc + 1, 1, 1);
				/* receive elements of system array  */
				/* magnetic moments */
				MPI_Recv(systemArray + beginShift, spinsNumber - beginShift, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			}
		}
	}

}

/* synchronize system array (coordinates, velocities, magnetic moments)
 * (array was calculated by master process and
 * this function will send this array to other processes) */
void synchronizeSystemArrayFull(double * systemArray) {

	/* number of components in system array */
	size_t compNumber = NUMBER_COMP * userVars.N1 * userVars.N2 * userVars.N3;

	if (rank != RANK_MASTER) {
		/* receive array */
		MPI_Recv(systemArray, compNumber, MPI_DOUBLE, RANK_MASTER, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	} else {
		/* send array */
		for (int i = 0; i < numberProc; i++) {
			if (i != RANK_MASTER) {
				MPI_Send(systemArray, compNumber, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
			}
		}
	}

}

/* synchronize system array (coordinates, velocities, magnetic moments)
 * (array was calculated in parallel and
 * this function will combine different path of this array
 * in one array of master process) */
void synchronizeSystemArrayPiece(double * systemArray) {

	/* begin shift for slave process */
	long beginShift;
	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	if (rank != RANK_MASTER) {
		/* calculate begin shifts */
		beginShift = IDXmg(rank * (userVars.N1 - 2) / numberProc + 1, 1, 1);
		/* send elements of system array */
		/* magnetic moments */
		MPI_Send(systemArray + beginShift, spinsNumber - beginShift, MPI_DOUBLE, RANK_MASTER, 0, MPI_COMM_WORLD);
		/* coordinates */
		MPI_Send(systemArray + spinsNumber + beginShift, spinsNumber - beginShift, MPI_DOUBLE, RANK_MASTER, 0, MPI_COMM_WORLD);
		/* velocities */
		MPI_Send(systemArray + 2 * spinsNumber + beginShift, spinsNumber - beginShift, MPI_DOUBLE, RANK_MASTER, 0, MPI_COMM_WORLD);
	} else {
		/* go trough all processes */
		for (int i = 0; i < numberProc; i++) {
			if (i != RANK_MASTER) {
				/* calculate begin and end shifts */
				beginShift = IDXmg(i * (userVars.N1 - 2) / numberProc + 1, 1, 1);
				/* receive elements of system array  */
				/* magnetic moments */
				MPI_Recv(systemArray + beginShift, spinsNumber - beginShift, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				/* coordinates */
				MPI_Recv(systemArray + spinsNumber + beginShift, spinsNumber - beginShift, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				/* velocities */
				MPI_Recv(systemArray + 2 * spinsNumber + beginShift, spinsNumber - beginShift, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			}
		}
	}

}

/* synchronize array "latticeParam"
 * ("latticeParam" was calculated by master process and
 * this function will send this array to other processes) */
void synchronizeLatticeParam(void) {

	/* calculate number of spins */
	long spinsNumber = userVars.N1 * userVars.N2 * userVars.N3;

	if (rank != RANK_MASTER) {
		/* receive "lattice" */
		MPI_Recv(latticeParam, spinsNumber * sizeof(LatticeSiteDescr), MPI_BYTE, RANK_MASTER, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	} else {
		/* send "lattice" */
		for (int i = 0; i < numberProc; i++) {
			if (i != RANK_MASTER) {
				MPI_Send(latticeParam, spinsNumber * sizeof(LatticeSiteDescr), MPI_BYTE, i, 0, MPI_COMM_WORLD);
			}
		}
	}

}

/* synchronize scalar value of magnetic system
 * (scalar value was calculated of master process and
 * this function will send this value to other processes) */
void synchronizeScalarValue(double *scalarValue) {

	if (rank != RANK_MASTER) {
		/* receive scalar value */
		MPI_Recv(scalarValue, 1, MPI_DOUBLE, RANK_MASTER, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	} else {
		/* go trough all processes */
		for (int i = 0; i < numberProc; i++) {
			if (i != RANK_MASTER) {
				/* send scalar value */
				MPI_Send(scalarValue, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
			}
		}
	}

}

/* synchronize scalar value of magnetic system
 * (scalar value was calculated of different processes and
 * this function will send this value to master process and
 * after that will add values in one master variable) */
void synchronizeScalarValueAdd(double *scalarValue) {

	if (rank != RANK_MASTER) {
		/* send scalar value */
		MPI_Send(scalarValue, 1, MPI_DOUBLE, RANK_MASTER, 0, MPI_COMM_WORLD);
	} else {
		/* temporary scalar value */
		double tempScalarValue;
		/* go trough all processes */
		for (int i = 0; i < numberProc; i++) {
			if (i != RANK_MASTER) {
				/* receive scalar value */
				MPI_Recv(&tempScalarValue, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				/* add received scalar value to master variable */
				*scalarValue += tempScalarValue;
			}
		}
	}

}

/* synchronize scalar value of magnetic system
 * (scalar value was calculated of different processes and
 * this function will send this value to master process and
 * after that will find maximum value among all processes and
 * save this value in one master variable) */
void synchronizeScalarValueMax(double *scalarValue) {

	if (rank != RANK_MASTER) {
		/* send scalar value */
		MPI_Send(scalarValue, 1, MPI_DOUBLE, RANK_MASTER, 0, MPI_COMM_WORLD);
	} else {
		/* initial maximal scalar value */
		double maxScalarValue = *scalarValue;
		/* temporary maximal scalar value */
		double tempScalarValue = 0;
		/* go trough all processes */
		for (int i = 0; i < numberProc; i++) {
			if (i != RANK_MASTER) {
				/* receive scalar value */
				MPI_Recv(&tempScalarValue, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				/* compare temporary value using current maximal value */
				if (tempScalarValue > maxScalarValue) {
					maxScalarValue = tempScalarValue;
				}
			}
		}
		/* save maximal value */
		*scalarValue = maxScalarValue;
	}

}

/* synchronize MPI processes for correct work */
void synchronizeProcesses() {

	/* make barrier to synchronize */
	MPI_Barrier(MPI_COMM_WORLD);

}

#endif

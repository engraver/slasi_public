#ifndef INDICES_TEST_H_
#define INDICES_TEST_H_

int init_indices_test(void);

int clean_indices_test(void);

char * test_IDX_1_desc = "IDX test: spin chain 8x1x1";
void test_IDX_1(void);

char * test_IDX_2_desc = "IDX test: spin stripe 8x3x1";
void test_IDX_2(void);

char * test_IDXmg_1_desc = "IDXmg test: spin chain 8x1x1";
void test_IDXmg_1(void);

char * test_IDXmg_2_desc = "IDXmg test: spin stripe 8x3x1";
void test_IDXmg_2(void);

char * test_IDXcrd_1_desc = "IDXcoor test: spin chain 8x1x1";
void test_IDXcrd_1(void);

char * test_IDXcrd_2_desc = "IDXcoor test: spin stripe 8x3x1";
void test_IDXcrd_2(void);

char * test_IDXtr_1_desc = "IDXtr test: position in the triangular lattice (on the edge of lattice)";
void test_IDXtr_1(void);

char * test_IDXtr_2_desc = "IDXtr test: position in the triangular lattice (within lattice)";
void test_IDXtr_2(void);

char * test_IDXtrmg_1_desc = "IDXtrmg test: position in the triangular lattice (on the edge of lattice)";
void test_IDXtrmg_1(void);

char * test_IDXtrmg_2_desc = "IDXtrmg test: position in the triangular lattice (within lattice)";
void test_IDXtrmg_2(void);

char * test_IDXtrcrd_1_desc = "IDXtrcrd test: position in the triangular lattice (on the edge of lattice)";
void test_IDXtrcrd_1(void);

char * test_IDXtrcrd_2_desc = "IDXtrcrd test: position in the triangular lattice (within lattice)";
void test_IDXtrcrd_2(void);

char * test_IDXtr1_1_desc = "IDXtr1 test: position in the triangular lattice of the first neighbour (even line)";
void test_IDXtr1_1(void);

char * test_IDXtr1_2_desc = "IDXtr1 test: position in the triangular lattice of the first neighbour (odd line)";
void test_IDXtr1_2(void);

char * test_IDXtrmg1_1_desc = "IDXtrmg1 test: position in the triangular lattice of the first neighbour (even line)";
void test_IDXtrmg1_1(void);

char * test_IDXtrmg1_2_desc = "IDXtrmg1 test: position in the triangular lattice of the first neighbour (odd line)";
void test_IDXtrmg1_2(void);

char * test_IDXtr2_1_desc = "IDXtr2 test: position in the triangular lattice of the second neighbour (even line)";
void test_IDXtr2_1(void);

char * test_IDXtr2_2_desc = "IDXtr2 test: position in the triangular lattice of the second neighbour (odd line)";
void test_IDXtr2_2(void);

char * test_IDXtrmg2_1_desc = "IDXtrmg2 test: position in the triangular lattice of the second neighbour (even line)";
void test_IDXtrmg2_1(void);

char * test_IDXtrmg2_2_desc = "IDXtrmg2 test: position in the triangular lattice of the second neighbour (odd line)";
void test_IDXtrmg2_2(void);

char * test_IDXtr3_1_desc = "IDXtr3 test: position in the triangular lattice of the third neighbour (even line)";
void test_IDXtr3_1(void);

char * test_IDXtr3_2_desc = "IDXtr3 test: position in the triangular lattice of the third neighbour (odd line)";
void test_IDXtr3_2(void);

char * test_IDXtrmg3_1_desc = "IDXtrmg3 test: position in the triangular lattice of the third neighbour (even line)";
void test_IDXtrmg3_1(void);

char * test_IDXtrmg3_2_desc = "IDXtrmg3 test: position in the triangular lattice of the third neighbour (odd line)";
void test_IDXtrmg3_2(void);

char * test_IDXtr4_1_desc = "IDXtr4 test: position in the triangular lattice of the fourth neighbour (even line)";
void test_IDXtr4_1(void);

char * test_IDXtr4_2_desc = "IDXtr4 test: position in the triangular lattice of the fourth neighbour (odd line)";
void test_IDXtr4_2(void);

char * test_IDXtrmg4_1_desc = "IDXtrmg4 test: position in the triangular lattice of the fourth neighbour (even line)";
void test_IDXtrmg4_1(void);

char * test_IDXtrmg4_2_desc = "IDXtrmg4 test: position in the triangular lattice of the fourth neighbour (odd line)";
void test_IDXtrmg4_2(void);

char * test_IDXtr5_1_desc = "IDXtr5 test: position in the triangular lattice of the fifth neighbour (even line)";
void test_IDXtr5_1(void);

char * test_IDXtr5_2_desc = "IDXtr5 test: position in the triangular lattice of the fifth neighbour (odd line)";
void test_IDXtr5_2(void);

char * test_IDXtrmg5_1_desc = "IDXtrmg5 test: position in the triangular lattice of the fifth neighbour (even line)";
void test_IDXtrmg5_1(void);

char * test_IDXtrmg5_2_desc = "IDXtrmg5 test: position in the triangular lattice of the fifth neighbour (odd line)";
void test_IDXtrmg5_2(void);

char * test_IDXtr6_1_desc = "IDXtr6 test: position in the triangular lattice of the sixth neighbour (even line)";
void test_IDXtr6_1(void);

char * test_IDXtr6_2_desc = "IDXtr6 test: position in the triangular lattice of the sixth neighbour (odd line)";
void test_IDXtr6_2(void);

char * test_IDXtrmg6_1_desc = "IDXtrmg6 test: position in the triangular lattice of the sixth neighbour (even line)";
void test_IDXtrmg6_1(void);

char * test_IDXtrmg6_2_desc = "IDXtrmg6 test: position in the triangular lattice of the sixth neighbour (odd line)";
void test_IDXtrmg6_2(void);

#endif

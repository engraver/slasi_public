#ifndef SLASI_SYNCHRONIZE_H
#define SLASI_SYNCHRONIZE_H

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

/**
 * @brief synchronize structure "userVars" for all processes
 */
void synchronizeUserVars(void);

/**
 * @brief synchronize array "lattice" for all processes
 */
void synchronizeLattice(void);

/**
 * @brief synchronize array "Bamp" for all processes
 */
void synchronizeBamp(void);

/**
 * @brief synchronize array of structure "latticeParam" for all processes
 */
void synchronizeLatticeParam(void);

/**
 * @brief synchronize system array (magnetic moments, coordinates, velocities)
 * (array was calculated by master process and
 * this function will send this array to other processes)
 * @param systemArray is pointer to system array to synchronize
 */
void synchronizeSystemArrayFullMagn(double *systemArray);

/**
 * @brief synchronize system array for all processes (magnetic moments, coordinates, velocities)
 * (array was calculated in parallel and
 * this function will combine different piece of this array
 * in one array of master process)
 * @param systemArray is pointer to system array to synchronize
 */
void synchronizeSystemArrayPieceMagn(double *systemArray);

/**
 * @brief synchronize system array (magnetic moments, coordinates, velocities)
 * (array was calculated by master process and
 * this function will send this array to other processes)
 * @param systemArray is pointer to system array to synchronize
 */
void synchronizeSystemArrayFull(double *systemArray);

/**
 * @brief synchronize system array for all processes (magnetic moments, coordinates, velocities)
 * (array was calculated in parallel and
 * this function will combine different piece of this array
 * in one array of master process)
 * @param systemArray is pointer to system array to synchronize
 */
void synchronizeSystemArrayPiece(double *systemArray);

/**
 * @brief synchronize scalar value of magnetic system
 * (scalar value was calculated of master process and
 * this function will send this value to other processes)
 * @param scalarValue is pointer to scalar value to synchronize */
void synchronizeScalarValue(double *scalarValue);

/**
 * @brief synchronize scalar value of magnetic system
 * (scalar value was calculated of different processes and
 * this function will send this value to master process and
 * after that will add values in one master variable)
 * @param scalarValue is pointer to scalar value to synchronize
 */
void synchronizeScalarValueAdd(double *scalarValue);

/**
 * synchronize scalar value of magnetic system
 * (scalar value was calculated of different processes and
 * this function will send this value to master process and
 * after that will find maximum value among all processes and
 * save this value in one master variable)
 * @param scalarValue is pointer to scalar value to synchronize
 * */
void synchronizeScalarValueMax(double *scalarValue);

/**
 * synchronize MPI processes for correct work
 * */
void synchronizeProcesses(void);

#endif

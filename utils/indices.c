#include "indices.h"
#include "../alltypes.h"

/** implementation of inline functions to find neighbours of current magnetic node (array "latticeParam") */

/** the first neighbour (triangular lattice) */
inline int IDXtr1(int i1, int i2) {

	int curIndex = 1 + userVars.N3 * (i2 + i1 * userVars.N2);
	return curIndex + userVars.N2 * userVars.N3;

}

/** the second neighbour (triangular lattice) */
inline int IDXtr2(int i1, int i2) {

	int curIndex = 1 + userVars.N3 * (i2 + i1 * userVars.N2);
	return curIndex + IsEVEN(i2) * userVars.N2 * userVars.N3 + userVars.N3;

}

/** the third neighbour (triangular lattice) */
inline int IDXtr3(int i1, int i2) {

	int curIndex = 1 + userVars.N3 * (i2 + i1 * userVars.N2);
	return curIndex - IsODD(i2) * userVars.N2 * userVars.N3 + userVars.N3;

}

/** the fourth neighbour (triangular lattice) */
inline int IDXtr4(int i1, int i2) {

	int curIndex = 1 + userVars.N3 * (i2 + i1 * userVars.N2);
	return curIndex - userVars.N2 * userVars.N3;

}

/** the fifth neighbour (triangular lattice) */
inline int IDXtr5(int i1, int i2) {

	int curIndex = 1 + userVars.N3 * (i2 + i1 * userVars.N2);
	return curIndex - IsODD(i2) * userVars.N2 * userVars.N3 - userVars.N3;

}

/** the sixth neighbour (triangular lattice) */
inline int IDXtr6(int i1, int i2) {

	int curIndex = 1 + userVars.N3 * (i2 + i1 * userVars.N2);
	return curIndex + IsEVEN(i2) * userVars.N2 * userVars.N3 - userVars.N3;

}

/** the first neighbour (cubic lattice) */
inline int IDXcub1(int i1, int i2, int i3) {

	return i3 + userVars.N3 * (i2 + i1 * userVars.N2) + userVars.N3 * userVars.N2;

}

/** the second neighbour (cubic lattice) */
inline int IDXcub2(int i1, int i2, int i3) {

	return i3 + userVars.N3 * (i2 + i1 * userVars.N2) - userVars.N3 * userVars.N2;

}

/** the third neighbour (cubic lattice) */
inline int IDXcub3(int i1, int i2, int i3) {

	return i3 + userVars.N3 * (i2 + i1 * userVars.N2) + userVars.N3;

}

/** the fourth neighbour (cubic lattice) */
inline int IDXcub4(int i1, int i2, int i3) {

	return i3 + userVars.N3 * (i2 + i1 * userVars.N2) - userVars.N3;

}

/** the fifth neighbour (cubic lattice) */
inline int IDXcub5(int i1, int i2, int i3) {

	return i3 + userVars.N3 * (i2 + i1 * userVars.N2) + 1;

}

/** the sixth neighbour (cubic lattice) */
inline int IDXcub6(int i1, int i2, int i3) {

	return i3 + userVars.N3 * (i2 + i1 * userVars.N2) - 1;

}


/** implementation of inline functions to find neighbours of current magnetic node (array "lattice") */

/** the first neighbour (triangular lattice) */
inline int IDXtrmg1(int i1, int i2) {

	int curIndex = 1 + userVars.N3 * (i2 + i1 * userVars.N2);
	return 3 * (curIndex + userVars.N2 * userVars.N3);

}

/** the second neighbour (triangular lattice) */
inline int IDXtrmg2(int i1, int i2) {

	int curIndex = 1 + userVars.N3 * (i2 + i1 * userVars.N2);
	return 3 * (curIndex + IsEVEN(i2) * userVars.N2 * userVars.N3 + userVars.N3);

}

/** the third neighbour (triangular lattice) */
inline int IDXtrmg3(int i1, int i2) {

	int curIndex = 1 + userVars.N3 * (i2 + i1 * userVars.N2);
	return 3 * (curIndex - IsODD(i2) * userVars.N2 * userVars.N3 + userVars.N3);

}

/** the fourth neighbour (triangular lattice) */
inline int IDXtrmg4(int i1, int i2) {

	int curIndex = 1 + userVars.N3 * (i2 + i1 * userVars.N2);
	return 3 * (curIndex - userVars.N2 * userVars.N3);

}

/** the fifth neighbour (triangular lattice) */
inline int IDXtrmg5(int i1, int i2) {

	int curIndex = 1 + userVars.N3 * (i2 + i1 * userVars.N2);
	return 3 * (curIndex - IsODD(i2) * userVars.N2 * userVars.N3 - userVars.N3);

}

/** the sixth neighbour (triangular lattice) */
inline int IDXtrmg6(int i1, int i2) {

	int curIndex = 1 + userVars.N3 * (i2 + i1 * userVars.N2);
	return 3 * (curIndex + IsEVEN(i2) * userVars.N2 * userVars.N3 - userVars.N3);

}

/** the first neighbour (cubic lattice) */
inline int IDXcubmg1(int i1, int i2, int i3) {

	return 3 * (i3 + userVars.N3 * (i2 + i1 * userVars.N2) + userVars.N3 * userVars.N2);

}

/** the second neighbour (cubic lattice) */
inline int IDXcubmg2(int i1, int i2, int i3) {

	return 3 * (i3 + userVars.N3 * (i2 + i1 * userVars.N2) - userVars.N3 * userVars.N2);

}

/** the third neighbour (cubic lattice) */
inline int IDXcubmg3(int i1, int i2, int i3) {

	return 3 * (i3 + userVars.N3 * (i2 + i1 * userVars.N2) + userVars.N3);

}

/** the fourth neighbour (cubic lattice) */
inline int IDXcubmg4(int i1, int i2, int i3) {

	return 3 * (i3 + userVars.N3 * (i2 + i1 * userVars.N2) - userVars.N3);

}

/** the fifth neighbour (cubic lattice) */
inline int IDXcubmg5(int i1, int i2, int i3) {

	return 3 * (i3 + userVars.N3 * (i2 + i1 * userVars.N2) + 1);

}

/** the sixth neighbour (cubic lattice) */
inline int IDXcubmg6(int i1, int i2, int i3) {

	return 3 * (i3 + userVars.N3 * (i2 + i1 * userVars.N2) - 1);

}

/** implementation of inline functions to find indexes of current node */

/** index of current node in array "latticeParam" (triangular lattice) */
inline int IDXtr(int i1, int i2) {
	return 1 + userVars.N3 * (i2 + i1 * userVars.N2);
}

/** index of current node in array "latticeParam" (square lattice) */
inline int IDXsqr(int i1, int i2) {
	return 1 + userVars.N3 * (i2 + i1 * userVars.N2);
}

/** index of current node in array "lattice" (triangular lattice) */
inline int IDXtrmg(int i1, int i2) {
	return 3 * (1 + userVars.N3 * (i2 + i1 * userVars.N2));
}

/** index of current node in array "lattice" (square lattice) */
inline int IDXsqrmg(int i1, int i2) {
	return 3 * (1 + userVars.N3 * (i2 + i1 * userVars.N2));
}


/** implementation of inline functions to find indexes (for coordinates) of current node */

/** index of current node in array "lattice" */
inline int IDXtrcrd(int i1, int i2) {
	return 3 * (1 + userVars.N3 * (i2 + i1 * userVars.N2)) + NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
}


#ifndef _PARALLEL_UTILS_H_
#define _PARALLEL_UTILS_H_

#include <stdio.h>
#include <stdbool.h>

/**
 * @brief Initialize environment (parallel libraries etc)
 * @param indEnvParallel indicator for initialization and finalization of environment (parallel calculation)
 */
void envInit(char * projCaption, bool indEnvParallel);

/**
 * @brief Helper function for `envInit`
 */
void envInitShared(char * projCaption);


/**
  * @brief Finalize environment
  * @param indEnv indicator for initialization and finalization of environment (parallel calculation)
  */
void envFinalize(bool indEnvParallel);

void envFinalizeShared(void);

/**
 * @brief function to write message into the given stream
 * @param msg message to write
 * @param stream steam like stdout, stderr or file
 * */
void messageToStream(char * msg, FILE * stream);

/**
 * @brief send data from master process to other processes
 */
void sendData(void);

/**
 * @brief function to create system arrays ("lattice" and "latticeParam") for processes which are not master
 */
void createSystemArraysOther(void);

/**
 * @brief function to free memory
 */
void freeMemory(void);

/**
 * @brief Returns true only if it is called in master MPI process on in a single process of non-parallel program
 * @return true if it is called in master MPI process (or in single process program) and false otherwise
 */
bool doItInMasterProcess(void);

/**
 * @brief Synchronize data between processes
 */
void synchronizeData(bool flag);

/**
 * @brief Synchronize execution of processes
 */
void waitProcesses(void);

/**
 * @brief Create arrays for simulation
 */
void createSimulationArrays(void);

/**
 * @brief Destroy arrays for simulation
 */
void destroySimulationArrays(void);

#endif

#include <stdlib.h>
#include <time.h>
#include <sys/mman.h>

#include "utils.h"
#include <mpi.h>
#include "../alltypes.h"
#include "../processing/processing.h"
#include "../processing/interface_memory.h"
#include "../utils/synchronize.h"
#include "parallel_utils.h"

void envInit(char * projCaption, bool indEnvParallel) {

	/* check indicator of environment */
	if (indEnvParallel) {

		/* create MPI processes */
		MPI_Init(NULL, NULL);
		/* identify this process and number of processes */
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		MPI_Comm_size(MPI_COMM_WORLD, &numberProc);

	}

	envInitShared(projCaption);

}

void envFinalize(bool indEnvParallel) {

	envFinalizeShared();

	/* check indicator of environment */
	if (!indEnvParallel) {
		return;
	}

	/* destroy MPI processes */
	MPI_Finalize();

}

void messageToStream(char * msg, FILE * stream) {

	/* write message if process is master */
	if (rank == RANK_MASTER) {
		fprintf(stream, "%s\n", msg);
	}

}

void sendData(void) {

	/* synchronize "lattice" */
	synchronizeLattice();
	/* synchronize "latticeParam" */
	synchronizeLatticeParam();
	/* synchronize "Bamp" */
	synchronizeBamp();

}


void createSystemArraysOther(void) {

	if (rank != RANK_MASTER) {
		/* create zero system array ("lattice", "latticeParam" and "Bamp") */
		setupArrays();
	}

}

void freeMemory(void) {

	/* free arrays */
	freeArrays();
	if (rank == RANK_MASTER) {
		/* free rows of "userVars" for names of files */
		free(userVars.paramFile);
		free(userVars.pathToWrite);
	}

}

bool doItInMasterProcess(void) {

	/* check if this is master process */
	if (rank == RANK_MASTER) {
		return true;
	} else {
		return false;
	}

}

void synchronizeData(bool flag) {

	/* synchronize "userVars" */
	synchronizeUserVars();
	/* check if creation of arrays is needed */
	if (flag) {
		/* create system zero arrays ("lattice", "latticeParam" and "Bamp") for other processes */
		createSystemArraysOther();
	}
	/* send data from master process to other processes after reading */
	sendData();

}

void waitProcesses(void) {
	MPI_Barrier(MPI_COMM_WORLD);
}

void createSimulationArrays(void) {

	/* sizes of arrays */
	unsigned long sizeOfArrsInBytes = NUMBER_COMP * sizeof(double) * (unsigned long)(userVars.N1 * userVars.N2 * userVars.N3);
	unsigned long sizeOfArrsInBytesInt = NUMBER_COMP * sizeof(int) * (unsigned long)(userVars.N1 * userVars.N2 * userVars.N3);
	unsigned long sizeOfArrsInBytesConn = MAX_CONN_NUM * sizeof(int) * (unsigned long)(connElements);

	/* create arrays for sudden changes of magnetic moments, coordinates and velocities */
	if (userVars.integrationMethod == INTEGRATION_METHOD_RKF) {
		m_dm5 = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
		                        MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		m_dm4 = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
		                        MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		/* create arrays for intermediate values of RKF45 */
		for (int i = 0; i < RKFARRAYS_NUM; i++) {
			m_k[i] = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
			                         MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		}
	} else if (userVars.integrationMethod == INTEGRATION_METHOD_MIDDLE_POINT) {
		m_dmFinal = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
		                             MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		m_dmHalf = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
		                           MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		latticeHalf = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
		                              MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	}

	/* create additional vectors for VTK XML BASE64 format */
	addDoubleVectorXML = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
	                                     MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	addIntVectorXML = (int *) mmap(NULL, sizeOfArrsInBytesInt, PROT_READ | PROT_WRITE,
	                                     MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	addIntVectorConnXML = (int *) mmap(NULL, sizeOfArrsInBytesConn, PROT_READ | PROT_WRITE,
	                                   MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

	/* allocate memory for arrays of dipole interaction */
	if (userVars.flexOnOff == FLEX_OFF && userVars.dipOnOff == DIP_ON) {
		dipole_cacheSize = userVars.N1 * userVars.N2 * userVars.N3;
		dipole_R3coef = (double **) mmap(NULL, dipole_cacheSize * sizeof(double *), PROT_READ | PROT_WRITE,
		                                 MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		dipole_RxR5coef = (double **) mmap(NULL, dipole_cacheSize * sizeof(double *), PROT_READ | PROT_WRITE,
		                                   MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		dipole_RyR5coef = (double **) mmap(NULL, dipole_cacheSize * sizeof(double *), PROT_READ | PROT_WRITE,
		                                   MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		dipole_RzR5coef = (double **) mmap(NULL, dipole_cacheSize * sizeof(double *), PROT_READ | PROT_WRITE,
		                                   MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		for (size_t i = 0; i < dipole_cacheSize; i++) {
			dipole_R3coef[i] = (double *) mmap(NULL, dipole_cacheSize * sizeof(double), PROT_READ | PROT_WRITE,
			                                   MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
			dipole_RxR5coef[i] = (double *) mmap(NULL, dipole_cacheSize * sizeof(double), PROT_READ | PROT_WRITE,
			                                     MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
			dipole_RyR5coef[i] = (double *)  mmap(NULL, dipole_cacheSize * sizeof(double), PROT_READ | PROT_WRITE,
			                                      MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
			dipole_RzR5coef[i] = (double *)  mmap(NULL, dipole_cacheSize * sizeof(double), PROT_READ | PROT_WRITE,
			                                      MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		}
	}

}

void destroySimulationArrays(void) {

	/* sizes of arrays */
	unsigned long sizeOfArrsInBytes = NUMBER_COMP * sizeof(double) * (unsigned long)(userVars.N1 * userVars.N2 * userVars.N3);
	unsigned long sizeOfArrsInBytesInt = NUMBER_COMP * sizeof(int) * (unsigned long)(userVars.N1 * userVars.N2 * userVars.N3);
	unsigned long sizeOfArrsInBytesConn = MAX_CONN_NUM * sizeof(int) * (unsigned long)(connElements);

	/* clear arrays of sudden changes */
	if (userVars.integrationMethod == INTEGRATION_METHOD_RKF) {
		munmap(m_dm5, sizeOfArrsInBytes);
		munmap(m_dm4, sizeOfArrsInBytes);
		for (int i = 0; i < RKFARRAYS_NUM; i++) {
			munmap(m_k[i], sizeOfArrsInBytes);
		}
	} else if (userVars.integrationMethod == INTEGRATION_METHOD_MIDDLE_POINT) {
		munmap(m_dmFinal, sizeOfArrsInBytes);
		munmap(m_dmHalf, sizeOfArrsInBytes);
		munmap(latticeHalf, sizeOfArrsInBytes);
	}

	/* clear additional vectors for VTK XML BASE64 format */
	munmap(addDoubleVectorXML, sizeOfArrsInBytes);
	munmap(addIntVectorXML, sizeOfArrsInBytesInt);
	munmap(addIntVectorConnXML, sizeOfArrsInBytesConn);

	/* clear memory for arrays of dipole interaction */
	if (userVars.flexOnOff == FLEX_OFF && userVars.dipOnOff == DIP_ON) {
		dipole_cacheSize = userVars.N1 * userVars.N2 * userVars.N3;
		for (size_t i = 0; i < dipole_cacheSize; i++) {
			munmap(dipole_R3coef[i], dipole_cacheSize);
			munmap(dipole_RxR5coef[i], dipole_cacheSize);
			munmap(dipole_RyR5coef[i], dipole_cacheSize);
			munmap(dipole_RzR5coef[i], dipole_cacheSize);
		}
		munmap(dipole_R3coef, dipole_cacheSize);
		munmap(dipole_RxR5coef, dipole_cacheSize);
		munmap(dipole_RyR5coef, dipole_cacheSize);
		munmap(dipole_RzR5coef, dipole_cacheSize);
		dipole_R3coef = NULL;
		dipole_RxR5coef = NULL;
		dipole_RyR5coef = NULL;
		dipole_RzR5coef = NULL;
	}

}

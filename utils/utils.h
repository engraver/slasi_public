#ifndef _UTILS_H_
#define _UTILS_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif
#include "../alltypes.h"

/**
 * @brief function createDescriptors finds the last character '.' in the line and then writes everything to that character in userVars.projName
 * and then creates or find (for restarting) descriptors of the following form: <projectname>_<somenumber>.log
 *
 * @param inputfilename - string which has the name of the project (there must be a character ".")
 * @return 0 in the case of absence of errors; otherwise prints message
 */
int createDescriptors(char *inputfilename);

/**
 * @brief update orthonormal basis for different calculation
 */
void UpdateOrthonormalBasis(void);

/**
 * @brief update vector of main direction of triangular lattice for different calculation
 */
void UpdateMainDirectionTriangle(void);

/**
 * @brief update normal vectors (triangle) of triangular lattice for different calculation
 */
void UpdateNormalVectorTriangle(void);

/**
 * @brief update normal vectors (site) of triangular lattice for different calculation
 */
void UpdateNormalVectorSite(void);

/**
 * @brief normalize vectors before simulation
 */
void NormalizeVectorsInitial(void);

#endif

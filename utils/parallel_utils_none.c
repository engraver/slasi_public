#include <stdlib.h>
#include <sys/mman.h>

#include "../alltypes.h"
#include "../processing/processing.h"
#include "../processing/interface_memory.h"
#include "parallel_utils.h"

void envInit(char * projCaption, bool indEnvParallel) {
	envInitShared(projCaption);
}

void envFinalize(bool indEnvParallel) {
	envFinalizeShared();
}

void messageToStream(char * msg, FILE * stream) {

	/* write message */
	fprintf(stream, "%s\n", msg);

}

void sendData(void) {

}

void createSystemArraysOther(void) {

}

void freeMemory(void) {

	/* free arrays */
	freeArrays();
	/* free rows of "userVars" for names of files */
	free(userVars.paramFile);
	free(userVars.pathToWrite);
	free(userVars.projName);

}

bool doItInMasterProcess(void) {
	return true;
}

void synchronizeData(bool flag) {

}

void waitProcesses(void) {
}

void createSimulationArrays(void) {

	/* time dynamics */
	unsigned long sizeOfArrsInBytes = NUMBER_COMP * sizeof(double) * (unsigned long)(userVars.N1 * userVars.N2 * userVars.N3);
	unsigned long sizeOfArrsInBytesInt = NUMBER_COMP * sizeof(int) * (unsigned long)(userVars.N1 * userVars.N2 * userVars.N3);
	unsigned long sizeOfArrsInBytesConn = MAX_CONN_NUM * sizeof(int) * (unsigned long)(connElements);

	/* create arrays for sudden changes of magnetic moments, coordinates and velocities */
	if (userVars.integrationMethod == INTEGRATION_METHOD_RKF) {
		m_dm5 = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
		                        MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		m_dm4 = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
		                        MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		for (int i = 0; i < RKFARRAYS_NUM; i++) {
			m_k[i] = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
			                         MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		}
	} else if (userVars.integrationMethod == INTEGRATION_METHOD_MIDDLE_POINT) {
		m_dmFinal = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
		                            MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		m_dmHalf = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
		                           MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		latticeHalf = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
		                              MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	}

	/* create additional vectors for VTK XML BASE64 format */
	addDoubleVectorXML = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
	                                     MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	addIntVectorXML = (int *) mmap(NULL, sizeOfArrsInBytesInt, PROT_READ | PROT_WRITE,
	                               MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	addIntVectorConnXML = (int *) mmap(NULL, sizeOfArrsInBytesConn, PROT_READ | PROT_WRITE,
	                                   MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

	/* allocate memory for arrays of dipole interaction */
	if (userVars.flexOnOff == FLEX_OFF && userVars.dipOnOff == DIP_ON) {
		dipole_cacheSize = userVars.N1 * userVars.N2 * userVars.N3;
		dipole_R3coef = (double **) mmap(NULL, dipole_cacheSize * sizeof(double *), PROT_READ | PROT_WRITE,
		        MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		dipole_RxR5coef = (double **) mmap(NULL, dipole_cacheSize * sizeof(double *), PROT_READ | PROT_WRITE,
				MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		dipole_RyR5coef = (double **) mmap(NULL, dipole_cacheSize * sizeof(double *), PROT_READ | PROT_WRITE,
				MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		dipole_RzR5coef = (double **) mmap(NULL, dipole_cacheSize * sizeof(double *), PROT_READ | PROT_WRITE,
				MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		for (size_t i = 0; i < dipole_cacheSize; i++) {
			dipole_R3coef[i] = (double *) mmap(NULL, dipole_cacheSize * sizeof(double), PROT_READ | PROT_WRITE,
			        MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
			dipole_RxR5coef[i] = (double *) mmap(NULL, dipole_cacheSize * sizeof(double), PROT_READ | PROT_WRITE,
					MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
			dipole_RyR5coef[i] = (double *)  mmap(NULL, dipole_cacheSize * sizeof(double), PROT_READ | PROT_WRITE,
					MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
			dipole_RzR5coef[i] = (double *)  mmap(NULL, dipole_cacheSize * sizeof(double), PROT_READ | PROT_WRITE,
					MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		}
	}

}

void destroySimulationArrays(void) {

	/* sizes of arrays */
	unsigned long sizeOfArrsInBytes = NUMBER_COMP * sizeof(double) * (unsigned long)(userVars.N1 * userVars.N2 * userVars.N3);
	unsigned long sizeOfArrsInBytesInt = NUMBER_COMP * sizeof(int) * (unsigned long)(userVars.N1 * userVars.N2 * userVars.N3);
	unsigned long sizeOfArrsInBytesConn = MAX_CONN_NUM * sizeof(int) * (unsigned long)(connElements);

	/* clear arrays of magnetic moments */
	munmap(m_dm5, sizeOfArrsInBytes);
	munmap(m_dm4, sizeOfArrsInBytes);
	for (int i = 0; i < RKFARRAYS_NUM; i++) {
		munmap(m_k[i], sizeOfArrsInBytes);
	}

	/* clear additional vectors for VTK XML BASE64 format */
	munmap(addDoubleVectorXML, sizeOfArrsInBytes);
	munmap(addIntVectorXML, sizeOfArrsInBytesInt);
	munmap(addIntVectorConnXML, sizeOfArrsInBytesConn);

	/* clear memory for arrays of dipole interaction */
	if (userVars.flexOnOff == FLEX_OFF && userVars.dipOnOff == DIP_ON) {
		dipole_cacheSize = userVars.N1 * userVars.N2 * userVars.N3;
		for (size_t i = 0; i < dipole_cacheSize; i++) {
			munmap(dipole_R3coef[i], dipole_cacheSize);
			munmap(dipole_RxR5coef[i], dipole_cacheSize);
			munmap(dipole_RyR5coef[i], dipole_cacheSize);
			munmap(dipole_RzR5coef[i], dipole_cacheSize);
		}
		munmap(dipole_R3coef, dipole_cacheSize);
		munmap(dipole_RxR5coef, dipole_cacheSize);
		munmap(dipole_RyR5coef, dipole_cacheSize);
		munmap(dipole_RzR5coef, dipole_cacheSize);
		dipole_R3coef = NULL;
		dipole_RxR5coef = NULL;
		dipole_RyR5coef = NULL;
		dipole_RzR5coef = NULL;
	}

}

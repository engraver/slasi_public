#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "../alltypes.h"
#include "parallel_utils.h"
#include "utils.h"
#include "../readdata/readdata.h"

void envInitShared(char * projCaption) {

	/* check id of process for execution of function (for MPI) */
	if (!doItInMasterProcess()) {
		return;
	}

	/* initial string values for reading from terminal */
	char str[MAX_STR_LEN];
	memset(str, 0, MAX_STR_LEN);

	char * streduced = malloc(MAX_STR_LEN);
	memset(streduced, 0, MAX_STR_LEN);

	/* handle row */
	strcpy(str, projCaption);
	long max = -1, i = 0;
	while (str[i] != '\0') {
		if (str[i] == '.') {
			max = i;
		}
		i++;
	}

	i = 0;
	while (i <= max - 1) {
		streduced[i] = str[i];
		i++;
	}

	/* save processed row as project name */
	userVars.projName = streduced;
	char temp[MAX_STR_LEN];
	memset(temp, 0, MAX_STR_LEN);
	if (numberOfFile)
		sprintf(temp, "_%llu", numberOfFile);
	strcat(userVars.projName, temp);

	/* free arrays */
	memset(str, 0, sizeof(str));

	/* modifying environment for python */
	char currentDir[MAX_STR_LEN];
	if (getcwd(currentDir, MAX_STR_LEN) == NULL) {
		messageToStream("Cannot get current directory!", stdout);
	}
	char *currentPYTHONPATH = getenv("PYTHONPATH");
	char *newPYTHONPATH;
	unsigned long newPYTHONPATHlen = 10 * MAX_STR_LEN;
	if (currentPYTHONPATH) {
		newPYTHONPATHlen += strlen(currentPYTHONPATH);
	}
	newPYTHONPATH = malloc(newPYTHONPATHlen);
	strcpy(newPYTHONPATH, currentDir);
	strcat(newPYTHONPATH, ":");
	if (currentPYTHONPATH) {
		strcat(newPYTHONPATH, currentPYTHONPATH);
	}
	setenv("PYTHONPATH", newPYTHONPATH, 1);
	free(newPYTHONPATH);

}

void envFinalizeShared(void) {

	/* finalize Python objects to work with scripts */
	if (pNamePython) {
		Py_DECREF(pNamePython);
	}
	if (pModulePython) {
		Py_DECREF(pModulePython);
	}
	Py_Finalize();

}

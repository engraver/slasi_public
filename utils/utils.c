#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "utils.h"
#include "indices.h"

void createNameLogFile(char *logfilename, char *projname, char *prefix) {

	/* selecting logfile number so that it does not exist */
	FILE *fid;

	/* creation initial names of file */
	size_t nlfnlen = strlen(logfilename) + MAX_STR_LEN;
	char *newlogfilename = malloc(nlfnlen);
	sprintf(newlogfilename, "%s_%s.log", projname, prefix);
	fid = fopen(newlogfilename, "r");

	/* selection with begin */
	size_t size = 1;
	while (fid != NULL) {

		/* creating new name of logfile */
		fclose(fid);
		memset(newlogfilename, 0, nlfnlen);
		sprintf(newlogfilename, "%s_%s_%lu.log", projname, prefix, size);

		/* increase number of file */
		size++;
		/* checking this name */
		fid = fopen(newlogfilename, "r");

	}

	/* transfer answer (number) */
	numberOfFile = size - 1;
	strcpy(logfilename, newlogfilename); // possible memory leak if newlogfilename is very long
	free(newlogfilename);

}

int createLogFiles(char *projnamereduced) {

	/* name of logfile for commented rows */
	char logfilename[100];
	/* name of logfile for energy */
	char logfilenameEnergy[100];
	/* name of logfile for magnetic moments */
	char logfilenameMag[100];
	/* name of logfile for other parameters */
	char logfilenameOther[100];

	/* creating names of logfiles from generated string (this is name of project) */

	/* logfile for commented rows */
	memset(logfilename, 0, sizeof(logfilename));
	strcpy(logfilename, projnamereduced);
	strcat(logfilename, ".log");

	/* logfile for energy */
	memset(logfilenameEnergy, 0, sizeof(logfilenameEnergy));
	strcpy(logfilenameEnergy, projnamereduced);
	strcat(logfilenameEnergy, ".log");

	/* logfile for magnetic moments */
	memset(logfilenameMag, 0, sizeof(logfilenameMag));
	strcpy(logfilenameMag, projnamereduced);
	strcat(logfilenameMag, ".log");

	/* logfile for other fields */
	memset(logfilenameOther, 0, sizeof(logfilenameOther));
	strcpy(logfilenameOther, projnamereduced);
	strcat(logfilenameOther, ".log");

	/* logfile for commented row */
	createNameLogFile(logfilename, projnamereduced, "init");
	/* logfile for energy */
	createNameLogFile(logfilenameEnergy, projnamereduced, "energy");
	/* logfile for magnetic moments */
	createNameLogFile(logfilenameMag, projnamereduced, "mag");
	/* logfile for other fields */
	createNameLogFile(logfilenameOther, projnamereduced, "other");

	/* creating files to write results */
	flog = fopen(logfilename, "w");
	flogEnergy = fopen(logfilenameEnergy, "w");
	flogMag = fopen(logfilenameMag, "w");
	flogOther = fopen(logfilenameOther, "w");

	/* to check correct creation of files */
	if ((flog == NULL) || (flogEnergy == NULL) || (flogOther == NULL) || (flogMag == NULL)) {
		fprintf(stderr, "Problems with creating of a log file '%s'.\n", logfilename);
		return 1;
	}

	return 0;

}

int findLogFiles(char *projnamereduced) {

	/* name of logfile for commented rows */
	char logfilenameInit[100];
	/* name of logfile for energy */
	char logfilenameEnergy[100];
	/* name of logfile for magnetic moments */
	char logfilenameMag[100];
	/* name of logfile for other parameters */
	char logfilenameOther[100];

	/* create named of logfile */

	/* init log */
	if (numberOfFile != 0) {
		sprintf(logfilenameInit, "%s_%s_%llu.log", projnamereduced, "init", numberOfFile);
	} else {
		sprintf(logfilenameInit, "%s_%s.log", projnamereduced, "init");
	}

	/* energy log */
	if (numberOfFile != 0) {
		sprintf(logfilenameEnergy, "%s_%s_%llu.log", projnamereduced, "energy", numberOfFile);
	} else {
		sprintf(logfilenameEnergy, "%s_%s.log", projnamereduced, "energy");
	}

	/* magnetic log */
	if (numberOfFile != 0) {
		sprintf(logfilenameMag, "%s_%s_%llu.log", projnamereduced, "mag", numberOfFile);
	} else {
		sprintf(logfilenameMag, "%s_%s.log", projnamereduced, "mag");
	}

	/* log for other variables */
	if (numberOfFile != 0) {
		sprintf(logfilenameOther, "%s_%s_%llu.log", projnamereduced, "other", numberOfFile);
	} else {
		sprintf(logfilenameOther, "%s_%s.log", projnamereduced, "other");
	}

	/* creating files to write results */
	flog = fopen(logfilenameInit, "a");
	flogEnergy = fopen(logfilenameEnergy, "a");
	flogMag = fopen(logfilenameMag, "a");
	flogOther = fopen(logfilenameOther, "a");

	/* to check correct creation of files */
	if ((flog == NULL) || (flogEnergy == NULL) || (flogOther == NULL) || (flogMag == NULL)) {
		fprintf(stderr, "Problems with creating of a log files.\n");
		return 1;
	}

	/* return default value */
	return 0;

}

int createDescriptors(char *inputfilename) {

	/* full project name */
	char projnamefully[100];
	/* reduce project name (without dot) */
	char projnamereduced[100];
	/* variable for checking of work */
	int tester = 0;

	/* finding dot in input file */
	memset(projnamefully, 0, sizeof(projnamefully));
	strcpy(projnamefully, inputfilename);
	int max = -1;
	int i = 0;
	while (projnamefully[i] != '\0') {
		if (projnamefully[i] == '.') {
			max = i;
		}
		i++;
	}

	/* deleting all symbols after point inclusive */
	memset(projnamereduced, 0, sizeof(projnamereduced));
	i = 0;
	while (i <= max - 1) {
		projnamereduced[i] = projnamefully[i];
		i++;
	}

	if (max != -1) {

		if (userVars.restartOnOff == RESTART_OFF) {
			/* create logfiles */
			createLogFiles(projnamereduced);
		} else {
			/* find logfiles (for restarting) */
			findLogFiles(projnamereduced);
		}


	} else {
		/* checking */
		fprintf(stderr, "You have entered wrong file name '%s' (without extension).\n", projnamefully);
		tester = 1;
	}

	/*return code of error */
	return tester;

}


void UpdateOrthonormalBasis(void) {

	/* define geometric quantities */
	size_t spinsNumber = (size_t)(NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3);
	size_t indexLattice, curIndex;
	double xi, yi, zi;
	double xip, yip, zip;
	double xim, yim, zim;
	double tx, ty, tz;
	double bx, by, bz;
	double nx, ny, nz;
	double txm, tym, tzm;
	/* define main and reserved lengths of vectors */
	double lengthVector, lengthVectorReserved;

	/* we set orthonormal basis on default */
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {

				/* find out which spin we work with */
				indexLattice = IDX(i1, i2, i3);

				latticeParam[indexLattice].tx = 1.0;
				latticeParam[indexLattice].ty = 0.0;
				latticeParam[indexLattice].tz = 0.0;

				latticeParam[indexLattice].nx = 0.0;
				latticeParam[indexLattice].ny = 1.0;
				latticeParam[indexLattice].nz = 0.0;

				latticeParam[indexLattice].bx = 0.0;
				latticeParam[indexLattice].by = 0.0;
				latticeParam[indexLattice].bz = 1.0;

			}
		}
	}

	/* go through all the vectors, except for first and last vectors */
	for (int i1 = 2; (i1 < userVars.N1 - 2) && (userVars.dimensionOfSystem == 1); i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {

				/* find out which spin we work with */
				indexLattice = IDX(i1, i2, i3);
				curIndex = IDXmg(i1, i2, i3);

				/* find the current, previous and next spin */
				xi = lattice[curIndex + spinsNumber];
				yi = lattice[curIndex + spinsNumber + 1];
				zi = lattice[curIndex + spinsNumber + 2];

				curIndex = IDXmg(i1 + 1, i2, i3);
				xip = lattice[curIndex + spinsNumber];
				yip = lattice[curIndex + spinsNumber + 1];
				zip = lattice[curIndex + spinsNumber + 2];

				curIndex = IDXmg(i1 - 1, i2, i3);
				xim = lattice[curIndex + spinsNumber];
				yim = lattice[curIndex + spinsNumber + 1];
				zim = lattice[curIndex + spinsNumber + 2];

				/* calculate vector of tangent for past and present spins */
				lengthVector = sqrt((xip - xi) * (xip - xi) + (yip - yi) * (yip - yi) + (zip - zi) * (zip - zi));
				tx = (xip - xi) / lengthVector;
				ty = (yip - yi) / lengthVector;
				tz = (zip - zi) / lengthVector;

				lengthVectorReserved = sqrt((xim - xi) * (xim - xi) +
				                            (yim - yi) * (yim - yi) + (zim - zi) * (zim - zi));
				txm = (xi - xim) / lengthVectorReserved;
				tym = (yi - yim) / lengthVectorReserved;
				tzm = (zi - zim) / lengthVectorReserved;

				/* calculate vector of binormal knowing tangent vectors */
				bx = tz * tym - ty * tzm;
				by = tx * tzm - tz * txm;
				bz = ty * txm - tx * tym;
				lengthVectorReserved = sqrt(bx * bx + by * by + bz * bz);

				/* if tangential vector and vector of binormal are zero, then we leave default values */
				if ((lengthVector >= NEAR_ZERO) && (lengthVectorReserved >= NEAR_ZERO)) {
					latticeParam[indexLattice].tx = tx;
					latticeParam[indexLattice].ty = ty;
					latticeParam[indexLattice].tz = tz;
				}

				/* if vector of binormal is zero, then we leave default values */
				if (lengthVectorReserved >= NEAR_ZERO) {
					latticeParam[indexLattice].bx = bx / lengthVector;
					latticeParam[indexLattice].by = by / lengthVector;
					latticeParam[indexLattice].bz = bz / lengthVector;
				}

				/* calculate vector of the centripetal knowing tangent vector and vector of binormal */
				nx = by * tz - bz * ty;
				ny = bz * tx - bx * tz;
				nz = bx * ty - by * tx;
				lengthVector = sqrt(nx * nx + ny * ny + nz * nz);

				/* if normal vector is zero, then we leave default values */
				if (lengthVector >= NEAR_ZERO) {
					latticeParam[indexLattice].nx = nx / lengthVector;
					latticeParam[indexLattice].ny = ny / lengthVector;
					latticeParam[indexLattice].nz = nz / lengthVector;
				}
			}
		}
	}


	/* find TNB-basis for the first vector */
	for (int i2 = 1; (i2 < userVars.N2 - 1) && (userVars.dimensionOfSystem == 1); i2++) {
		for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {

			/* the first vector */
			int i1 = 1;
			/* find out which spin we work with */
			indexLattice = IDX(i1, i2, i3);
			curIndex = IDXmg(i1, i2, i3);

			if (userVars.periodicBC1 == PBC_OFF) {

				/* components of tangential vector */
				tx = latticeParam[IDX(i1 + 1, i2, i3)].tx;
				ty = latticeParam[IDX(i1 + 1, i2, i3)].ty;
				tz = latticeParam[IDX(i1 + 1, i2, i3)].tz;

				/* if vector is first, then assign it to second and penultimate vector respectively */
				lengthVector = sqrt(tx * tx + ty * ty + tz * tz);
				if (lengthVector >= NEAR_ZERO) {
					latticeParam[indexLattice].tx = tx;
					latticeParam[indexLattice].ty = ty;
					latticeParam[indexLattice].tz = tz;
				}

				latticeParam[indexLattice].bx = latticeParam[IDX(i1 + 1, i2, i3)].bx;
				latticeParam[indexLattice].by = latticeParam[IDX(i1 + 1, i2, i3)].by;
				latticeParam[indexLattice].bz = latticeParam[IDX(i1 + 1, i2, i3)].bz;

				latticeParam[indexLattice].nx = latticeParam[IDX(i1 + 1, i2, i3)].nx;
				latticeParam[indexLattice].ny = latticeParam[IDX(i1 + 1, i2, i3)].ny;
				latticeParam[indexLattice].nz = latticeParam[IDX(i1 + 1, i2, i3)].nz;

			} else {

				/* find the current, previous and next spin */
				xi = lattice[curIndex + spinsNumber];
				yi = lattice[curIndex + spinsNumber + 1];
				zi = lattice[curIndex + spinsNumber + 2];

				curIndex = IDXmg(i1 + 1, i2, i3);
				xip = lattice[curIndex + spinsNumber];
				yip = lattice[curIndex + spinsNumber + 1];
				zip = lattice[curIndex + spinsNumber + 2];

				/* previous magnetic node is at end of the curve line */
				curIndex = IDXmg(userVars.N1 - 2, i2, i3);
				xim = lattice[curIndex + spinsNumber];
				yim = lattice[curIndex + spinsNumber + 1];
				zim = lattice[curIndex + spinsNumber + 2];

				/* calculate vector of tangent for previous and present spins */
				lengthVector = sqrt((xip - xi) * (xip - xi) + (yip - yi) * (yip - yi) + (zip - zi) * (zip - zi));
				tx = (xip - xi) / lengthVector;
				ty = (yip - yi) / lengthVector;
				tz = (zip - zi) / lengthVector;

				lengthVectorReserved = sqrt((xim - xi) * (xim - xi) +
				                            (yim - yi) * (yim - yi) + (zim - zi) * (zim - zi));
				txm = (xi - xim) / lengthVectorReserved;
				tym = (yi - yim) / lengthVectorReserved;
				tzm = (zi - zim) / lengthVectorReserved;

				/* calculate vector of binormal knowing tangent vectors */
				bx = tz * tym - ty * tzm;
				by = tx * tzm - tz * txm;
				bz = ty * txm - tx * tym;
				lengthVectorReserved = sqrt(bx * bx + by * by + bz * bz);

				/* if tangential vector and vector of binormal are zero, then we leave default values */
				if ((lengthVector >= NEAR_ZERO) && (lengthVectorReserved >= NEAR_ZERO)) {
					latticeParam[indexLattice].tx = tx;
					latticeParam[indexLattice].ty = ty;
					latticeParam[indexLattice].tz = tz;
				}

				/* if vector of binormal is zero, then we leave default values */
				if (lengthVectorReserved >= NEAR_ZERO) {
					latticeParam[indexLattice].bx = bx / lengthVector;
					latticeParam[indexLattice].by = by / lengthVector;
					latticeParam[indexLattice].bz = bz / lengthVector;
				}

				/* calculate vector of the centripetal knowing tangent vector and vector of binormal */
				nx = by * tz - bz * ty;
				ny = bz * tx - bx * tz;
				nz = bx * ty - by * tx;
				lengthVector = sqrt(nx * nx + ny * ny + nz * nz);

				/* if normal vector is zero, then we leave default values */
				if (lengthVector >= NEAR_ZERO) {
					latticeParam[indexLattice].nx = nx / lengthVector;
					latticeParam[indexLattice].ny = ny / lengthVector;
					latticeParam[indexLattice].nz = nz / lengthVector;
				}

			}
		}
	}

	/* find TNB-basis for the last vector */
	for (int i2 = 1; (i2 < userVars.N2 - 1) && (userVars.dimensionOfSystem == 1); i2++) {
		for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {

			/* the last vector */
			int i1 = userVars.N1 - 2;
			/* find out which spin we work with */
			indexLattice = IDX(i1, i2, i3);
			curIndex = IDXmg(i1, i2, i3);

			if (userVars.periodicBC1 == PBC_OFF) {

				/* components of tangential vector */
				tx = latticeParam[IDX(i1 - 1, i2, i3)].tx;
				ty = latticeParam[IDX(i1 - 1, i2, i3)].ty;
				tz = latticeParam[IDX(i1 - 1, i2, i3)].tz;

				/* if vector is last, then assign it to behind previous and penultimate vector respectively */
				lengthVector = sqrt(tx * tx + ty * ty + tz * tz);
				if (lengthVector >= NEAR_ZERO) {
					latticeParam[indexLattice].tx = tx;
					latticeParam[indexLattice].ty = ty;
					latticeParam[indexLattice].tz = tz;
				}

				latticeParam[indexLattice].bx = latticeParam[IDX(i1 - 1, i2, i3)].bx;
				latticeParam[indexLattice].by = latticeParam[IDX(i1 - 1, i2, i3)].by;
				latticeParam[indexLattice].bz = latticeParam[IDX(i1 - 1, i2, i3)].bz;

				latticeParam[indexLattice].nx = latticeParam[IDX(i1 - 1, i2, i3)].nx;
				latticeParam[indexLattice].ny = latticeParam[IDX(i1 - 1, i2, i3)].ny;
				latticeParam[indexLattice].nz = latticeParam[IDX(i1 - 1, i2, i3)].nz;

			} else {

				/* find the current, previous and next spin */
				xi = lattice[curIndex + spinsNumber];
				yi = lattice[curIndex + spinsNumber + 1];
				zi = lattice[curIndex + spinsNumber + 2];

				/* next magnetic node is at begin of the curve line */
				curIndex = IDXmg(1, i2, i3);
				xip = lattice[curIndex + spinsNumber];
				yip = lattice[curIndex + spinsNumber + 1];
				zip = lattice[curIndex + spinsNumber + 2];

				curIndex = IDXmg(i1 - 1, i2, i3);
				xim = lattice[curIndex + spinsNumber];
				yim = lattice[curIndex + spinsNumber + 1];
				zim = lattice[curIndex + spinsNumber + 2];

				/* calculate vector of tangent for previous and present spins */
				lengthVector = sqrt((xip - xi) * (xip - xi) + (yip - yi) * (yip - yi) + (zip - zi) * (zip - zi));
				tx = (xip - xi) / lengthVector;
				ty = (yip - yi) / lengthVector;
				tz = (zip - zi) / lengthVector;

				lengthVectorReserved = sqrt((xim - xi) * (xim - xi) +
				                            (yim - yi) * (yim - yi) + (zim - zi) * (zim - zi));
				txm = (xi - xim) / lengthVectorReserved;
				tym = (yi - yim) / lengthVectorReserved;
				tzm = (zi - zim) / lengthVectorReserved;

				/* calculate vector of binormal knowing tangent vectors */
				bx = tz * tym - ty * tzm;
				by = tx * tzm - tz * txm;
				bz = ty * txm - tx * tym;
				lengthVectorReserved = sqrt(bx * bx + by * by + bz * bz);

				/* if tangential vector and vector of binormal are zero, then we leave default values */
				if ((lengthVector >= NEAR_ZERO) && (lengthVectorReserved >= NEAR_ZERO)) {
					latticeParam[indexLattice].tx = tx;
					latticeParam[indexLattice].ty = ty;
					latticeParam[indexLattice].tz = tz;
				}

				/* if vector of binormal is zero, then we leave default values */
				if (lengthVectorReserved >= NEAR_ZERO) {
					latticeParam[indexLattice].bx = bx / lengthVector;
					latticeParam[indexLattice].by = by / lengthVector;
					latticeParam[indexLattice].bz = bz / lengthVector;
				}

				/* calculate vector of the centripetal knowing tangent vector and vector of binormal */
				nx = by * tz - bz * ty;
				ny = bz * tx - bx * tz;
				nz = bx * ty - by * tx;
				lengthVector = sqrt(nx * nx + ny * ny + nz * nz);

				/* if normal vector is zero, then we leave default values */
				if (lengthVector >= NEAR_ZERO) {
					latticeParam[indexLattice].nx = nx / lengthVector;
					latticeParam[indexLattice].ny = ny / lengthVector;
					latticeParam[indexLattice].nz = nz / lengthVector;
				}

			}

		}
	}

}

void UpdateMainDirectionTriangle(void) {

	/* define geometric quantities */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	size_t indexLattice, curIndex;
	double tx, ty, tz;
	double xi, yi, zi;
	double xip, yip, zip;
	/* define main lengths of vectors */
	double lengthVector;

	/* we set orthonormal basis on default */
	for (int i1 = 1; (i1 < userVars.N1 - 1) && (userVars.dimensionOfSystem == 2); i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {

				/* find out which spin we work with */
				indexLattice = IDX(i1, i2, i3);
				latticeParam[indexLattice].tx = 1.0;
				latticeParam[indexLattice].ty = 0.0;
				latticeParam[indexLattice].tz = 0.0;

			}
		}
	}

	/* if the surface curve is not closed */
	for (int i1 = 1; (i1 < userVars.N1 - 2) && (userVars.dimensionOfSystem == 2); i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {

				/* find out which spin we work with */
				indexLattice = IDX(i1, i2, i3);
				curIndex = IDXmg(i1, i2, i3);

				/* find the current and next spin */
				xi = lattice[curIndex + spinsNumber];
				yi = lattice[curIndex + spinsNumber + 1];
				zi = lattice[curIndex + spinsNumber + 2];

				xip = lattice[curIndex + spinsNumber + (size_t)(3 * userVars.N3 * userVars.N2)];
				yip = lattice[curIndex + spinsNumber + (size_t)(3 * userVars.N3 * userVars.N2 + 1)];
				zip = lattice[curIndex + spinsNumber + (size_t)(3 * userVars.N3 * userVars.N2 + 2)];

				/* calculate vector of main direction */
				lengthVector = sqrt((xip - xi) * (xip - xi) + (yip - yi) * (yip - yi) + (zip - zi) * (zip - zi));
				tx = (xip - xi) / lengthVector;
				ty = (yip - yi) / lengthVector;
				tz = (zip - zi) / lengthVector;

				lengthVector = sqrt(tx * tx + ty * ty + tz * tz);
				if (lengthVector >= NEAR_ZERO) {
					latticeParam[indexLattice].tx = tx;
					latticeParam[indexLattice].ty = ty;
					latticeParam[indexLattice].tz = tz;
				}

				/* if vector is last, then assign it to second and penultimate vector respectively */
				if (i1 == userVars.N1 - 3) {
					lengthVector = sqrt(tx * tx + ty * ty + tz * tz);
					if (lengthVector >= NEAR_ZERO) {
						latticeParam[indexLattice + userVars.N3 * userVars.N2].tx = tx;
						latticeParam[indexLattice + userVars.N3 * userVars.N2].ty = ty;
						latticeParam[indexLattice + userVars.N3 * userVars.N2].tz = tz;
					}
				}

			}
		}
	}

}

/** function to receive vector product
 * ax, ay, az - the first vector
 * bx, by, bz - the second vector
 * cx, cy, cz - received vector
 ** */
void vectorProduct(double ax, double ay, double az, double bx, double by, double bz, double *cx, double *cy, double *cz) {

	/* vector product by component */
	*cx = ay * bz - az * by;
	*cy = az * bx - ax * bz;
	*cz = ax * by - ay * bx;

}

/** function to receive radius vector of (i,j) node in triangular lattice
 * i, j - indexes of node
 * x, y, z - components of radius vector
 ** */
void radiusVector(int i, int j, double *x, double *y, double *z) {

	/* index of node for coordinates */
	int indexCoord = IDXtrcrd(i, j);
	/* component of radius vector */
	*x = lattice[indexCoord];
	*y = lattice[indexCoord + 1];
	*z = lattice[indexCoord + 2];

}

/* to debug code */
//bool firstLaunchNormal = true;

/** function to update normal vectors for triangular lattice (triangle)
 ** */
void UpdateNormalVectorTriangle(void) {

    /****************
    * additional variables
    **************** */
	/* number of maximum indexes horizontally and vertically */
	int N1 = userVars.N1 - 2;
	int N2 = userVars.N2 - 2;
	/* intermediate vector */
	double interVx, interVy, interVz;
	/* length of vector */
	double length;

	/****************
    * (i1,i2), where i1 is 1...N1-1 and i2 is 1...N2-1 (upper vector)
    **************** */
    /* go through all the nodes */
	for (int i = 1; i <= N1 - 1; i++) {
		for (int j = 1; j <= N2 - 1; j++) {

			if (j % 2 == 1) {

				/* radius vectors */
				double x_ip1_j, y_ip1_j, z_ip1_j;
				radiusVector(i + 1, j, &x_ip1_j, &y_ip1_j, &z_ip1_j);

				double x_i_j, y_i_j, z_i_j;
				radiusVector(i, j, &x_i_j, &y_i_j, &z_i_j);

				double x_i_jp1, y_i_jp1, z_i_jp1;
				radiusVector(i, j + 1, &x_i_jp1, &y_i_jp1, &z_i_jp1);

				/* intermediate vectors */
				vectorProduct(x_ip1_j - x_i_j, y_ip1_j - y_i_j, z_ip1_j - z_i_j,
				              x_i_jp1 - x_i_j, y_i_jp1 - y_i_j, z_i_jp1 - z_i_j,
				              &interVx, &interVy, &interVz);

				/* length vector */
				length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

				/* normal vectors */
				latticeParam[IDXtr(i, j)].trinxU = interVx / length;
				latticeParam[IDXtr(i, j)].trinyU = interVy / length;
				latticeParam[IDXtr(i, j)].trinzU = interVz / length;

			} else {

				/* radius vectors */
				double x_ip1_j, y_ip1_j, z_ip1_j;
				radiusVector(i + 1, j, &x_ip1_j, &y_ip1_j, &z_ip1_j);

				double x_i_j, y_i_j, z_i_j;
				radiusVector(i, j, &x_i_j, &y_i_j, &z_i_j);

				double x_ip1_jp1, y_ip1_jp1, z_ip1_jp1;
				radiusVector(i + 1, j + 1, &x_ip1_jp1, &y_ip1_jp1, &z_ip1_jp1);

				/* intermediate vectors */
				vectorProduct(x_ip1_j - x_i_j, y_ip1_j - y_i_j, z_ip1_j - z_i_j,
				              x_ip1_jp1 - x_i_j, y_ip1_jp1 - y_i_j, z_ip1_jp1 - z_i_j,
				              &interVx, &interVy, &interVz);

				/* length vector */
				length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

				/* normal vectors */
				latticeParam[IDXtr(i, j)].trinxU = interVx / length;
				latticeParam[IDXtr(i, j)].trinyU = interVy / length;
				latticeParam[IDXtr(i, j)].trinzU = interVz / length;

			}

		}
	}

	/****************
    * (N1,j), where j is 1...N2-1 (upper vector)
    **************** */
	/* check boundary condition */
	if (userVars.periodicBC1 == PBC_ON) {

		/* go through all the nodes */
		for (int j = 1; j <= N2 - 1; j++) {

			if (j % 2 == 1) {

				/* radius vectors */
				double x_1_j, y_1_j, z_1_j;
				radiusVector(1, j, &x_1_j, &y_1_j, &z_1_j);

				double x_N1_j, y_N1_j, z_N1_j;
				radiusVector(N1, j, &x_N1_j, &y_N1_j, &z_N1_j);

				double x_N1_jp1, y_N1_jp1, z_N1_jp1;
				radiusVector(N1, j + 1, &x_N1_jp1, &y_N1_jp1, &z_N1_jp1);

				/* intermediate vectors */
				vectorProduct(x_1_j - x_N1_j, y_1_j - y_N1_j, z_1_j - z_N1_j,
				              x_N1_jp1 - x_N1_j, y_N1_jp1 - y_N1_j, z_N1_jp1 - z_N1_j,
				              &interVx, &interVy, &interVz);

				/* length vector */
				length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

				/* normal vectors */
				latticeParam[IDXtr(N1, j)].trinxU = interVx / length;
				latticeParam[IDXtr(N1, j)].trinyU = interVy / length;
				latticeParam[IDXtr(N1, j)].trinzU = interVz / length;

			} else {

				/* radius vectors */
				double x_1_j, y_1_j, z_1_j;
				radiusVector(1, j, &x_1_j, &y_1_j, &z_1_j);

				double x_N1_j, y_N1_j, z_N1_j;
				radiusVector(N1, j, &x_N1_j, &y_N1_j, &z_N1_j);

				double x_1_jp1, y_1_jp1, z_1_jp1;
				radiusVector(1, j + 1, &x_1_jp1, &y_1_jp1, &z_1_jp1);

				/* intermediate vectors */
				vectorProduct(x_1_j - x_N1_j, y_1_j - y_N1_j, z_1_j - z_N1_j,
				              x_1_jp1 - x_N1_j, y_1_jp1 - y_N1_j, z_1_jp1 - z_N1_j,
				              &interVx, &interVy, &interVz);

				/* length vector */
				length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

				/* normal vectors */
				latticeParam[IDXtr(N1, j)].trinxU = interVx / length;
				latticeParam[IDXtr(N1, j)].trinyU = interVy / length;
				latticeParam[IDXtr(N1, j)].trinzU = interVz / length;

			}

		}

	}

	/****************
    * (i,j), where i is 1...N1-1 and j is 2...N2 (lower vector)
    **************** */
	/* go through all the nodes */
	for (int i = 1; i <= N1 - 1; i++) {
		for (int j = 2; j <= N2; j++) {

			if (j % 2 == 1) {

				/* radius vectors */
				double x_i_jm1, y_i_jm1, z_i_jm1;
				radiusVector(i, j - 1, &x_i_jm1, &y_i_jm1, &z_i_jm1);

				double x_i_j, y_i_j, z_i_j;
				radiusVector(i, j, &x_i_j, &y_i_j, &z_i_j);

				double x_ip1_j, y_ip1_j, z_ip1_j;
				radiusVector(i + 1, j, &x_ip1_j, &y_ip1_j, &z_ip1_j);

				/* intermediate vectors */
				vectorProduct(x_i_jm1 - x_i_j, y_i_jm1 - y_i_j, z_i_jm1 - z_i_j,
				              x_ip1_j - x_i_j, y_ip1_j - y_i_j, z_ip1_j - z_i_j,
				              &interVx, &interVy, &interVz);

				/* length vector */
				length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

				/* normal vectors */
				latticeParam[IDXtr(i, j)].trinxD = interVx / length;
				latticeParam[IDXtr(i, j)].trinyD = interVy / length;
				latticeParam[IDXtr(i, j)].trinzD = interVz / length;

			} else {

				/* radius vectors */
				double x_ip1_jm1, y_ip1_jm1, z_ip1_jm1;
				radiusVector(i + 1, j - 1, &x_ip1_jm1, &y_ip1_jm1, &z_ip1_jm1);

				double x_i_j, y_i_j, z_i_j;
				radiusVector(i, j, &x_i_j, &y_i_j, &z_i_j);

				double x_ip1_j, y_ip1_j, z_ip1_j;
				radiusVector(i + 1, j, &x_ip1_j, &y_ip1_j, &z_ip1_j);

				/* intermediate vectors */
				vectorProduct(x_ip1_jm1 - x_i_j, y_ip1_jm1 - y_i_j, z_ip1_jm1 - z_i_j,
				              x_ip1_j - x_i_j, y_ip1_j - y_i_j, z_ip1_j - z_i_j,
				              &interVx, &interVy, &interVz);

				/* length vector */
				length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

				/* normal vectors */
				latticeParam[IDXtr(i, j)].trinxD = interVx / length;
				latticeParam[IDXtr(i, j)].trinyD = interVy / length;
				latticeParam[IDXtr(i, j)].trinzD = interVz / length;

			}

			/* to debug code */
			/* if (firstLaunchNormal) {
            	fprintf(stderr, "%i %i ", i, j);
				fprintf(stderr, "%f ", latticeParam[IDXtr(i, j)].trinxD);
				fprintf(stderr, "%f ", latticeParam[IDXtr(i, j)].trinyD);
				fprintf(stderr, "%f\n", latticeParam[IDXtr(i, j)].trinzD);
			} */

		}
	}

	/****************
    * (N1,j), where j is 2...N2 (lower vector)
    **************** */
	/* check boundary condition */
	if (userVars.periodicBC1 == PBC_ON) {

		/* go through all the nodes */
		for (int j = 2; j <= N2; j++) {

			if (j % 2 == 1) {

				/* radius vectors */
				double x_N1_jm1, y_N1_jm1, z_N1_jm1;
				radiusVector(N1, j - 1, &x_N1_jm1, &y_N1_jm1, &z_N1_jm1);

				double x_N1_j, y_N1_j, z_N1_j;
				radiusVector(N1, j, &x_N1_j, &y_N1_j, &z_N1_j);

				double x_1_j, y_1_j, z_1_j;
				radiusVector(1, j, &x_1_j, &y_1_j, &z_1_j);

				/* intermediate vectors */
				vectorProduct(x_N1_jm1 - x_N1_j, y_N1_jm1 - y_N1_j, z_N1_jm1 - z_N1_j,
				              x_1_j - x_N1_j, y_1_j - y_N1_j, z_1_j - z_N1_j,
				              &interVx, &interVy, &interVz);

				/* length vector */
				length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

				/* normal vectors */
				latticeParam[IDXtr(N1, j)].trinxD = interVx / length;
				latticeParam[IDXtr(N1, j)].trinyD = interVy / length;
				latticeParam[IDXtr(N1, j)].trinzD = interVz / length;

			} else {

				/* radius vectors */
				double x_1_jm1, y_1_jm1, z_1_jm1;
				radiusVector(1, j - 1, &x_1_jm1, &y_1_jm1, &z_1_jm1);

				double x_N1_j, y_N1_j, z_N1_j;
				radiusVector(N1, j, &x_N1_j, &y_N1_j, &z_N1_j);

				double x_1_j, y_1_j, z_1_j;
				radiusVector(1, j, &x_1_j, &y_1_j, &z_1_j);

				/* intermediate vectors */
				vectorProduct(x_1_jm1 - x_N1_j, y_1_jm1 - y_N1_j, z_1_jm1 - z_N1_j,
				              x_1_j - x_N1_j, y_1_j - y_N1_j, z_1_j - z_N1_j,
				              &interVx, &interVy, &interVz);

				/* length vector */
				length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

				/* normal vectors */
				latticeParam[IDXtr(N1, j)].trinxD = interVx / length;
				latticeParam[IDXtr(N1, j)].trinyD = interVy / length;
				latticeParam[IDXtr(N1, j)].trinzD = interVz / length;

			}

		}

	}

	/* to debug code */
	//firstLaunchNormal = false;

}

/** function to find radius-vectors of neighbours using index of "latticeParam"
 * index - index of site in "latticeParam"
 * rx, ry, rz - components of radius vector
 ** */
void radiusVectorLattice(int index, double * rx, double * ry, double * rz) {

	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* receive components of radius-vector */
	*rx = lattice[index + spinsNumber];
	*ry = lattice[index + spinsNumber + 1];
	*rz = lattice[index + spinsNumber + 2];

}

/** function to calculate intermediate vector for function "UpdateNormalVectorSite"
 * rxi, ryi, rzi - radius-vector of the "first" neighbour
 * rxj, ryj, rzj - radius-vector of the "second" neighbour
 * rxSite, rySite, rzSite - radius-vector of the current site
 * tx, ty, tz - intermediate vector
 ** */
void calculateInterVector(double rxi, double ryi, double rzi, double rxj, double ryj, double rzj,
                          double rxSite, double rySite, double rzSite, double *tx, double *ty, double *tz) {

	/* calculate the first cross product */
	double vectorProdX1, vectorProdY1, vectorProdZ1;
	vectorProduct(rxi, ryi, rzi, rxj, ryj, rzj, &vectorProdX1, &vectorProdY1, &vectorProdZ1);

	/* calculate the second cross product */
	double vectorProdX2, vectorProdY2, vectorProdZ2;
	vectorProduct(rxi, ryi, rzi, rxSite, rySite, rzSite, &vectorProdX2, &vectorProdY2, &vectorProdZ2);

	/* calculate the third cross product */
	double vectorProdX3, vectorProdY3, vectorProdZ3;
	vectorProduct(rxSite, rySite, rzSite, rxj, ryj, rzj, &vectorProdX3, &vectorProdY3, &vectorProdZ3);

	/* calculate total vector */
	*tx = vectorProdX1 - vectorProdX2 - vectorProdX3;
	*ty = vectorProdY1 - vectorProdY2 - vectorProdY3;
	*tz = vectorProdZ1 - vectorProdZ2 - vectorProdZ3;

}

/** function to update normal vectors for triangular lattice (site)
 ** */
void UpdateNormalVectorSite(void) {

	/****************
	* additional variables
	**************** */
	/* number of maximum indexes horizontally and vertically */
	int N1 = userVars.N1 - 2;
	int N2 = userVars.N2 - 2;
	/* indexes of neighbours */
	int neighIndex1, neighIndex2, neighIndex3, neighIndex4, neighIndex5, neighIndex6;
	/* index of current site */
	int curIndex, indexLattice;
	/* radius-vector of site */
	double rxi, ryi, rzi;
	/* radius-vectors (neighbours) */
	double rx1, ry1, rz1; /* the first vector */
	double rx2, ry2, rz2; /* the second vector */
	double rx3, ry3, rz3; /* the third vector */
	double rx4, ry4, rz4; /* the fourth vector */
	double rx5, ry5, rz5; /* the fifth vector */
	double rx6, ry6, rz6; /* the sixth vector */
	/* intermediate vectors for normalization */
	double tx1, ty1, tz1; /* the first vector */
	double tx2, ty2, tz2; /* the second vector */
	double tx3, ty3, tz3; /* the third vector */
	double tx4, ty4, tz4; /* the fourth vector */
	double tx5, ty5, tz5; /* the fifth vector */
	double tx6, ty6, tz6; /* the sixth vector */
	/* indicators to include addition */
	double ind1, ind2, ind3, ind4, ind5, ind6;
	/* intermediate vectors for normalization */
	double interVx, interVy, interVz;
	/* length of vector */
	double length;

	/* initialization of variables */

	/* directional vectors */
	rx1 = 0.0; ry1 = 0.0; rz1 = 0.0;
	rx2 = 0.0; ry2 = 0.0; rz2 = 0.0;
	rx3 = 0.0; ry3 = 0.0; rz3 = 0.0;
	rx4 = 0.0; ry4 = 0.0; rz4 = 0.0;
	rx5 = 0.0; ry5 = 0.0; rz5 = 0.0;
	rx6 = 0.0; ry6 = 0.0; rz6 = 0.0;

	/* intermediate vectors for calculation */
	tx1 = 0.0; ty1 = 0.0; tz1 = 0.0;
	tx2 = 0.0; ty2 = 0.0; tz2 = 0.0;
	tx3 = 0.0; ty3 = 0.0; tz3 = 0.0;
	tx4 = 0.0; ty4 = 0.0; tz4 = 0.0;
	tx5 = 0.0; ty5 = 0.0; tz5 = 0.0;
	tx6 = 0.0; ty6 = 0.0; tz6 = 0.0;

	/****************
	* (i,j), where i is 2...N1-1 and j is 2...N2-1
	**************** */
	for (int i = 2; i <= N1 - 1; i++) {
		for (int j = 2; j <= N2 - 1; j++) {

			/* find indexes of neighbours and current site */
			curIndex = IDXtrmg(i, j);
			indexLattice = IDXtr(i, j);
			neighIndex1 = IDXtrmg1(i, j);
			neighIndex2 = IDXtrmg2(i, j);
			neighIndex3 = IDXtrmg3(i, j);
			neighIndex4 = IDXtrmg4(i, j);
			neighIndex5 = IDXtrmg5(i, j);
			neighIndex6 = IDXtrmg6(i, j);

			//if ((number == 1) && (id == ID_MASTER)) {
			//	printf("%f %f %f\n", lattice[curIndex], lattice[curIndex + 1], lattice[curIndex + 2]);
			//}

			/* find radius-vectors of neighbours and current site using indexes of "latticeParam" with boundary condition */
			radiusVectorLattice(curIndex, &rxi, &ryi, &rzi);
			radiusVectorLattice(neighIndex1, &rx1, &ry1, &rz1);
			radiusVectorLattice(neighIndex2, &rx2, &ry2, &rz2);
			radiusVectorLattice(neighIndex3, &rx3, &ry3, &rz3);
			radiusVectorLattice(neighIndex4, &rx4, &ry4, &rz4);
			radiusVectorLattice(neighIndex5, &rx5, &ry5, &rz5);
			radiusVectorLattice(neighIndex6, &rx6, &ry6, &rz6);

			/* include additions according to boundary conditions */
			ind1 = 1.0;
			ind2 = 1.0;
			ind3 = 1.0;
			ind4 = 1.0;
			ind5 = 1.0;
			ind6 = 1.0;

			/* calculate intermediate vectors to find result with boundary condition */
			calculateInterVector(rx1, ry1, rz1, rx2, ry2, rz2, rxi, ryi, rzi, &tx1, &ty1, &tz1);
			calculateInterVector(rx2, ry2, rz2, rx3, ry3, rz3, rxi, ryi, rzi, &tx2, &ty2, &tz2);
			calculateInterVector(rx3, ry3, rz3, rx4, ry4, rz4, rxi, ryi, rzi, &tx3, &ty3, &tz3);
			calculateInterVector(rx4, ry4, rz4, rx5, ry5, rz5, rxi, ryi, rzi, &tx4, &ty4, &tz4);
			calculateInterVector(rx5, ry5, rz5, rx6, ry6, rz6, rxi, ryi, rzi, &tx5, &ty5, &tz5);
			calculateInterVector(rx6, ry6, rz6, rx1, ry1, rz1, rxi, ryi, rzi, &tx6, &ty6, &tz6);

			/* find normal vectors of site (not normalized) */
			interVx = ind1 * tx1 + ind2 * tx2 + ind3 * tx3 + ind4 * tx4 + ind5 * tx5 + ind6 * tx6;
			interVy = ind1 * ty1 + ind2 * ty2 + ind3 * ty3 + ind4 * ty4 + ind5 * ty5 + ind6 * ty6;
			interVz = ind1 * tz1 + ind2 * tz2 + ind3 * tz3 + ind4 * tz4 + ind5 * tz5 + ind6 * tz6;

			/* find length of normal vectors (not normalized) */
			length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

			/* normalize normal vectors and return result */
			latticeParam[indexLattice].trinx = interVx / length;
			latticeParam[indexLattice].triny = interVy / length;
			latticeParam[indexLattice].trinz = interVz / length;

		}
	}

	/****************
	* (i,j), where i is 2...N1-1 and j is 1
	*************/
	for (int i = 2; i <= N1 - 1; i++) {

		/* reset indexes of neighbour sites */
		neighIndex5 = 0; neighIndex6 = 0;
		/* reset indicators to include additions */
		ind4 = 0.0; ind5 = 0.0; ind6 = 0.0;

		/* find indexes of neighbours and current site with boundary conditions  */
		curIndex = IDXtrmg(i, 1);
		indexLattice = IDXtr(i, 1);
		neighIndex1 = IDXtrmg1(i, 1);
		neighIndex2 = IDXtrmg2(i, 1);
		neighIndex3 = IDXtrmg3(i, 1);
		neighIndex4 = IDXtrmg4(i, 1);
		if (userVars.periodicBC2 == PBC_ON) {
			neighIndex5 = IDXtrmg5(i, 1);
			neighIndex6 = IDXtrmg6(i, 1);
		}

		/* find radius-vectors of neighbours and current site using indexes of "latticeParam" with boundary condition */
		radiusVectorLattice(curIndex, &rxi, &ryi, &rzi);
		radiusVectorLattice(neighIndex1, &rx1, &ry1, &rz1);
		radiusVectorLattice(neighIndex2, &rx2, &ry2, &rz2);
		radiusVectorLattice(neighIndex3, &rx3, &ry3, &rz3);
		radiusVectorLattice(neighIndex4, &rx4, &ry4, &rz4);
		if (userVars.periodicBC2 == PBC_ON) {
			radiusVectorLattice(neighIndex5, &rx5, &ry5, &rz5);
			radiusVectorLattice(neighIndex6, &rx6, &ry6, &rz6);
		}

		/* include additions according to boundary conditions */
		ind1 = 1.0;
		ind2 = 1.0;
		ind3 = 1.0;
		if (userVars.periodicBC2 == PBC_ON) {
			ind4 = 1.0;
			ind5 = 1.0;
			ind6 = 1.0;
		}

		/* calculate intermediate vectors to find result with boundary condition */
		calculateInterVector(rx1, ry1, rz1, rx2, ry2, rz2, rxi, ryi, rzi, &tx1, &ty1, &tz1);
		calculateInterVector(rx2, ry2, rz2, rx3, ry3, rz3, rxi, ryi, rzi, &tx2, &ty2, &tz2);
		calculateInterVector(rx3, ry3, rz3, rx4, ry4, rz4, rxi, ryi, rzi, &tx3, &ty3, &tz3);
		if (userVars.periodicBC2 == PBC_ON) {
			calculateInterVector(rx4, ry4, rz4, rx5, ry5, rz5, rxi, ryi, rzi, &tx4, &ty4, &tz4);
			calculateInterVector(rx5, ry5, rz5, rx6, ry6, rz6, rxi, ryi, rzi, &tx5, &ty5, &tz5);
			calculateInterVector(rx6, ry6, rz6, rx1, ry1, rz1, rxi, ryi, rzi, &tx6, &ty6, &tz6);
		}

		/* find normal vectors of site (not normalized) */
		interVx = ind1 * tx1 + ind2 * tx2 + ind3 * tx3 + ind4 * tx4 + ind5 * tx5 + ind6 * tx6;
		interVy = ind1 * ty1 + ind2 * ty2 + ind3 * ty3 + ind4 * ty4 + ind5 * ty5 + ind6 * ty6;
		interVz = ind1 * tz1 + ind2 * tz2 + ind3 * tz3 + ind4 * tz4 + ind5 * tz5 + ind6 * tz6;

		/* find length of normal vectors (not normalized) */
		length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

		/* normalize normal vectors and return result */
		latticeParam[indexLattice].trinx = interVx / length;
		latticeParam[indexLattice].triny = interVy / length;
		latticeParam[indexLattice].trinz = interVz / length;

	}

	/****************
	* (i,j), where i is 2...N1-1 and j is N2
	*************/
	for (int i = 2; i <= N1 - 1; i++) {

		/* reset indexes of neighbour sites */
		neighIndex2 = 0; neighIndex3 = 0;
		/* reset indicators to include additions */
		ind1 = 0.0; ind2 = 0.0; ind3 = 0.0;

		/* find indexes of neighbours and current site with boundary condition */
		curIndex = IDXtrmg(i, N2);
		indexLattice = IDXtr(i, N2);
		neighIndex1 = IDXtrmg1(i, N2);
		if (userVars.periodicBC2 == PBC_ON) {
			neighIndex2 = IDXtrmg2(i, N2);
			neighIndex3 = IDXtrmg3(i, N2);
		}
		neighIndex4 = IDXtrmg4(i, N2);
		neighIndex5 = IDXtrmg5(i, N2);
		neighIndex6 = IDXtrmg6(i, N2);

		/* find radius-vectors of neighbours and current site using indexes of "latticeParam" with boundary condition */
		radiusVectorLattice(curIndex, &rxi, &ryi, &rzi);
		radiusVectorLattice(neighIndex1, &rx1, &ry1, &rz1);
		if (userVars.periodicBC2 == PBC_ON) {
			radiusVectorLattice(neighIndex2, &rx2, &ry2, &rz2);
			radiusVectorLattice(neighIndex3, &rx3, &ry3, &rz3);
		}
		radiusVectorLattice(neighIndex4, &rx4, &ry4, &rz4);
		radiusVectorLattice(neighIndex5, &rx5, &ry5, &rz5);
		radiusVectorLattice(neighIndex6, &rx6, &ry6, &rz6);

		/* include additions according to boundary conditions */
		if (userVars.periodicBC2 == PBC_ON) {
			ind1 = 1.0;
			ind2 = 1.0;
			ind3 = 1.0;
		}
		ind4 = 1.0;
		ind5 = 1.0;
		ind6 = 1.0;

		/* calculate intermediate vectors to find result with boundary condition */
		if (userVars.periodicBC2 == PBC_ON) {
			calculateInterVector(rx1, ry1, rz1, rx2, ry2, rz2, rxi, ryi, rzi, &tx1, &ty1, &tz1);
			calculateInterVector(rx2, ry2, rz2, rx3, ry3, rz3, rxi, ryi, rzi, &tx2, &ty2, &tz2);
			calculateInterVector(rx3, ry3, rz3, rx4, ry4, rz4, rxi, ryi, rzi, &tx3, &ty3, &tz3);
		}
		calculateInterVector(rx4, ry4, rz4, rx5, ry5, rz5, rxi, ryi, rzi, &tx4, &ty4, &tz4);
		calculateInterVector(rx5, ry5, rz5, rx6, ry6, rz6, rxi, ryi, rzi, &tx5, &ty5, &tz5);
		calculateInterVector(rx6, ry6, rz6, rx1, ry1, rz1, rxi, ryi, rzi, &tx6, &ty6, &tz6);

		/* find normal vectors of site (not normalized) */
		interVx = ind1 * tx1 + ind2 * tx2 + ind3 * tx3 + ind4 * tx4 + ind5 * tx5 + ind6 * tx6;
		interVy = ind1 * ty1 + ind2 * ty2 + ind3 * ty3 + ind4 * ty4 + ind5 * ty5 + ind6 * ty6;
		interVz = ind1 * tz1 + ind2 * tz2 + ind3 * tz3 + ind4 * tz4 + ind5 * tz5 + ind6 * tz6;

		/* find length of normal vectors (not normalized) */
		length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

		/* normalize normal vectors and return result */
		latticeParam[indexLattice].trinx = interVx / length;
		latticeParam[indexLattice].triny = interVy / length;
		latticeParam[indexLattice].trinz = interVz / length;

	}

	/****************
	* (i,j), where i is 1 and j is 2..N2-1
	*************/
	for (int j = 2; j <= N2 - 1; j++) {

		/* reset indexes of neighbour sites */
		neighIndex3 = 0; neighIndex4 = 0; neighIndex5 = 0;
		/* reset indicators to include additions */
		ind2 = 0.0; ind3 = 0.0; ind4 = 0.0; ind5 = 0.0;

		/* find indexes of neighbours and current site with boundary condition */
		curIndex = IDXtrmg(1, j);
		indexLattice = IDXtr(1, j);
		neighIndex1 = IDXtrmg1(1, j);
		neighIndex2 = IDXtrmg2(1, j);
		if ((j % 2 == 0) || (userVars.periodicBC1 == PBC_ON) ) {
			neighIndex3 = IDXtrmg3(1, j);
		}
		if (userVars.periodicBC1 == PBC_ON) {
			neighIndex4 = IDXtrmg4(1, j);
		}
		if ((j % 2 == 0) || (userVars.periodicBC1 == PBC_ON) ) {
			neighIndex5 = IDXtrmg5(1, j);
		}
		neighIndex6 = IDXtrmg6(1, j);

		radiusVectorLattice(curIndex, &rxi, &ryi, &rzi);
		radiusVectorLattice(neighIndex1, &rx1, &ry1, &rz1);
		radiusVectorLattice(neighIndex2, &rx2, &ry2, &rz2);
		if ((j % 2 == 0) || (userVars.periodicBC1 == PBC_ON) ) {
			radiusVectorLattice(neighIndex3, &rx3, &ry3, &rz3);
		}
		if (userVars.periodicBC1 == PBC_ON) {
			radiusVectorLattice(neighIndex4, &rx4, &ry4, &rz4);
		}
		if ((j % 2 == 0) || (userVars.periodicBC1 == PBC_ON) ) {
			radiusVectorLattice(neighIndex5, &rx5, &ry5, &rz5);
		}
		radiusVectorLattice(neighIndex6, &rx6, &ry6, &rz6);

		/* include additions according to periodic conditions*/
		ind1 = 1.0;
		if ((j % 2 == 0) || (userVars.periodicBC1 == PBC_ON) ) {
			ind2 = 1.0;
		}
		if (userVars.periodicBC1 == PBC_ON) {
			ind3 = 1.0;
			ind4 = 1.0;
		}
		if ((j % 2 == 0) || (userVars.periodicBC1 == PBC_ON) ) {
			ind5 = 1.0;
		}
		ind6 = 1.0;

		/* calculate intermediate vectors to find result with boundary condition */
		calculateInterVector(rx1, ry1, rz1, rx2, ry2, rz2, rxi, ryi, rzi, &tx1, &ty1, &tz1);
		if ((j % 2 == 0) || (userVars.periodicBC1 == PBC_ON) ) {
			calculateInterVector(rx2, ry2, rz2, rx3, ry3, rz3, rxi, ryi, rzi, &tx2, &ty2, &tz2);
		}
		if (userVars.periodicBC1 == PBC_ON) {
			calculateInterVector(rx3, ry3, rz3, rx4, ry4, rz4, rxi, ryi, rzi, &tx3, &ty3, &tz3);
			calculateInterVector(rx4, ry4, rz4, rx5, ry5, rz5, rxi, ryi, rzi, &tx4, &ty4, &tz4);
		}
		if ((j % 2 == 0) || (userVars.periodicBC1 == PBC_ON) ) {
			calculateInterVector(rx5, ry5, rz5, rx6, ry6, rz6, rxi, ryi, rzi, &tx5, &ty5, &tz5);
		}
		calculateInterVector(rx6, ry6, rz6, rx1, ry1, rz1, rxi, ryi, rzi, &tx6, &ty6, &tz6);

		/* find normal vectors of site (not normalized) */
		interVx = ind1 * tx1 + ind2 * tx2 + ind3 * tx3 + ind4 * tx4 + ind5 * tx5 + ind6 * tx6;
		interVy = ind1 * ty1 + ind2 * ty2 + ind3 * ty3 + ind4 * ty4 + ind5 * ty5 + ind6 * ty6;
		interVz = ind1 * tz1 + ind2 * tz2 + ind3 * tz3 + ind4 * tz4 + ind5 * tz5 + ind6 * tz6;

		/* find length of normal vectors (not normalized) */
		length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

		/* normalize normal vectors and return result */
		latticeParam[indexLattice].trinx = interVx / length;
		latticeParam[indexLattice].triny = interVy / length;
		latticeParam[indexLattice].trinz = interVz / length;

	}

	/****************
	* (i,j), where i is N1 and j is 2..N2-1
	*************/
	for (int j = 2; j <= N2 - 1; j++) {

		/* reset indexes of neighbour sites */
		neighIndex1 = 0; neighIndex2 = 0; neighIndex6 = 0;
		/* reset indicators to include additions */
		ind1 = 0.0; ind2 = 0.0; ind5 = 0.0; ind6 = 0.0;

		/* find indexes of neighbours and current site with boundary condition */
		curIndex = IDXtrmg(N1, j);
		indexLattice = IDXtr(N1, j);
		if (userVars.periodicBC1 == PBC_ON) {
			neighIndex1 = IDXtrmg1(N1, j);
		}
		if ((j % 2 == 1) || (userVars.periodicBC1 == PBC_ON) ) {
			neighIndex2 = IDXtrmg2(N1, j);
		}
		neighIndex3 = IDXtrmg3(N1, j);
		neighIndex4 = IDXtrmg4(N1, j);
		neighIndex5 = IDXtrmg5(N1, j);
		if ((j % 2 == 1) || (userVars.periodicBC1 == PBC_ON) ) {
			neighIndex6 = IDXtrmg6(N1, j);
		}

		/* find radius-vectors of neighbours and current site using indexes of "latticeParam" with boundary condition */
		radiusVectorLattice(curIndex, &rxi, &ryi, &rzi);
		if (userVars.periodicBC1 == PBC_ON) {
			radiusVectorLattice(neighIndex1, &rx1, &ry1, &rz1);
		}
		if ((j % 2 == 1) || (userVars.periodicBC1 == PBC_ON) ) {
			radiusVectorLattice(neighIndex2, &rx2, &ry2, &rz2);
		}
		radiusVectorLattice(neighIndex3, &rx3, &ry3, &rz3);
		radiusVectorLattice(neighIndex4, &rx4, &ry4, &rz4);
		radiusVectorLattice(neighIndex5, &rx5, &ry5, &rz5);
		if ((j % 2 == 1) || (userVars.periodicBC1 == PBC_ON) ) {
			radiusVectorLattice(neighIndex6, &rx6, &ry6, &rz6);
		}

		/* include additions according to boundary conditions */
		if (userVars.periodicBC1 == PBC_ON) {
			ind1 = 1.0;
		}
		if ((j % 2 == 1) || (userVars.periodicBC1 == PBC_ON) ) {
			ind2 = 1.0;
		}
		ind3 = 1.0;
		ind4 = 1.0;
		if ((j % 2 == 1) || (userVars.periodicBC1 == PBC_ON) ) {
			ind5 = 1.0;
		}
		if (userVars.periodicBC1 == PBC_ON) {
			ind6 = 1.0;
		}

		/* calculate intermediate vectors to find result with boundary condition */
		if (userVars.periodicBC1 == PBC_ON) {
			calculateInterVector(rx1, ry1, rz1, rx2, ry2, rz2, rxi, ryi, rzi, &tx1, &ty1, &tz1);
		}
		if ((j % 2 == 1) || (userVars.periodicBC1 == PBC_ON) ) {
			calculateInterVector(rx2, ry2, rz2, rx3, ry3, rz3, rxi, ryi, rzi, &tx2, &ty2, &tz2);
		}
		calculateInterVector(rx3, ry3, rz3, rx4, ry4, rz4, rxi, ryi, rzi, &tx3, &ty3, &tz3);
		calculateInterVector(rx4, ry4, rz4, rx5, ry5, rz5, rxi, ryi, rzi, &tx4, &ty4, &tz4);
		if ((j % 2 == 1) || (userVars.periodicBC1 == PBC_ON) ) {
			calculateInterVector(rx5, ry5, rz5, rx6, ry6, rz6, rxi, ryi, rzi, &tx5, &ty5, &tz5);
		}
		if (userVars.periodicBC1 == PBC_ON) {
			calculateInterVector(rx6, ry6, rz6, rx1, ry1, rz1, rxi, ryi, rzi, &tx6, &ty6, &tz6);
		}

		/* find normal vectors of site (not normalized) */
		interVx = ind1 * tx1 + ind2 * tx2 + ind3 * tx3 + ind4 * tx4 + ind5 * tx5 + ind6 * tx6;
		interVy = ind1 * ty1 + ind2 * ty2 + ind3 * ty3 + ind4 * ty4 + ind5 * ty5 + ind6 * ty6;
		interVz = ind1 * tz1 + ind2 * tz2 + ind3 * tz3 + ind4 * tz4 + ind5 * tz5 + ind6 * tz6;

		/* find length of normal vectors (not normalized) */
		length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

		/* normalize normal vectors and return result */
		latticeParam[indexLattice].trinx = interVx / length;
		latticeParam[indexLattice].triny = interVy / length;
		latticeParam[indexLattice].trinz = interVz / length;

	}

	/****************
	* (i,j), where i is 1 and j is 1
	*************/

	/* reset indexes of neighbour sites */
	neighIndex3 = 0; neighIndex4 = 0; neighIndex5 = 0; neighIndex6 = 0;
	/* reset indicators to include additions */
	ind2 = 0.0; ind3 = 0.0; ind4 = 0.0; ind5 = 0.0; ind6 = 0.0;

	/* find indexes of neighbours and current site with boundary condition */
	curIndex = IDXtrmg(1, 1);
	indexLattice = IDXtr(1, 1);
	neighIndex1 = IDXtrmg1(1, 1);
	neighIndex2 = IDXtrmg2(1, 1);
	if (userVars.periodicBC1 == PBC_ON) {
		neighIndex3 = IDXtrmg3(1, 1);
		neighIndex4 = IDXtrmg4(1, 1);
	}
	if (userVars.periodicBC2 == PBC_ON) {
		neighIndex5 = IDXtrmg5(1, 1);
		neighIndex6 = IDXtrmg6(1, 1);
	}

	/* find radius-vectors of neighbours and current site using indexes of "latticeParam" with boundary condition */
	radiusVectorLattice(curIndex, &rxi, &ryi, &rzi);
	radiusVectorLattice(neighIndex1, &rx1, &ry1, &rz1);
	radiusVectorLattice(neighIndex2, &rx2, &ry2, &rz2);
	if (userVars.periodicBC1 == PBC_ON) {
		radiusVectorLattice(neighIndex3, &rx3, &ry3, &rz3);
		radiusVectorLattice(neighIndex4, &rx4, &ry4, &rz4);
	}
	if (userVars.periodicBC2 == PBC_ON) {
		radiusVectorLattice(neighIndex5, &rx5, &ry5, &rz5);
		radiusVectorLattice(neighIndex6, &rx6, &ry6, &rz6);
	}

	/* include additions according to boundary conditions */
	ind1 = 1.0;
	if (userVars.periodicBC1 == PBC_ON) {
		ind2 = 1.0;
		ind3 = 1.0;
	}
	if ((userVars.periodicBC1 == PBC_ON) && (userVars.periodicBC2 == PBC_ON)) {
		ind4 = 1.0;
	}
	if (userVars.periodicBC2 == PBC_ON) {
		ind5 = 1.0;
		ind6 = 1.0;
	}

	/* calculate intermediate vectors to find result with boundary condition */
	calculateInterVector(rx1, ry1, rz1, rx2, ry2, rz2, rxi, ryi, rzi, &tx1, &ty1, &tz1);
	if (userVars.periodicBC1 == PBC_ON) {
		calculateInterVector(rx2, ry2, rz2, rx3, ry3, rz3, rxi, ryi, rzi, &tx2, &ty2, &tz2);
		calculateInterVector(rx3, ry3, rz3, rx4, ry4, rz4, rxi, ryi, rzi, &tx3, &ty3, &tz3);
	}
	if ((userVars.periodicBC1 == PBC_ON) && (userVars.periodicBC2 == PBC_ON)) {
		calculateInterVector(rx4, ry4, rz4, rx5, ry5, rz5, rxi, ryi, rzi, &tx4, &ty4, &tz4);
	}
	if (userVars.periodicBC2 == PBC_ON) {
		calculateInterVector(rx5, ry5, rz5, rx6, ry6, rz6, rxi, ryi, rzi, &tx5, &ty5, &tz5);
		calculateInterVector(rx6, ry6, rz6, rx1, ry1, rz1, rxi, ryi, rzi, &tx6, &ty6, &tz6);
	}

	/* find normal vectors of site (not normalized) */
	interVx = ind1 * tx1 + ind2 * tx2 + ind3 * tx3 + ind4 * tx4 + ind5 * tx5 + ind6 * tx6;
	interVy = ind1 * ty1 + ind2 * ty2 + ind3 * ty3 + ind4 * ty4 + ind5 * ty5 + ind6 * ty6;
	interVz = ind1 * tz1 + ind2 * tz2 + ind3 * tz3 + ind4 * tz4 + ind5 * tz5 + ind6 * tz6;

	/* find length of normal vectors (not normalized) */
	length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

	/* normalize normal vectors and return result */
	latticeParam[indexLattice].trinx = interVx / length;
	latticeParam[indexLattice].triny = interVy / length;
	latticeParam[indexLattice].trinz = interVz / length;

	/****************
	* (i,j), where i is N1 and j is 1
	*************/

	/* reset indexes of neighbour sites */
	neighIndex1 = 0; neighIndex5 = 0; neighIndex6 = 0;
	/* reset indicators to include additions */
	ind4 = 0.0; ind5 = 0.0; ind6 = 0.0; ind1 = 0.0;

	/* find indexes of neighbours and current site with boundary condition */
	curIndex = IDXtrmg(N1, 1);
	indexLattice = IDXtr(N1, 1);
	if (userVars.periodicBC1 == PBC_ON) {
		neighIndex1 = IDXtrmg1(N1, 1);
	}
	neighIndex2 = IDXtrmg2(N1, 1);
	neighIndex3 = IDXtrmg3(N1, 1);
	neighIndex4 = IDXtrmg4(N1, 1);
	if (userVars.periodicBC2 == PBC_ON) {
		neighIndex5 = IDXtrmg5(N1, 1);
		neighIndex6 = IDXtrmg6(N1, 1);
	}

	/* find radius-vectors of neighbours and current site using indexes of "latticeParam" with boundary condition */
	radiusVectorLattice(curIndex, &rxi, &ryi, &rzi);
	if (userVars.periodicBC1 == PBC_ON) {
		radiusVectorLattice(neighIndex1, &rx1, &ry1, &rz1);
	}
	radiusVectorLattice(neighIndex2, &rx2, &ry2, &rz2);
	radiusVectorLattice(neighIndex3, &rx3, &ry3, &rz3);
	radiusVectorLattice(neighIndex4, &rx4, &ry4, &rz4);
	if (userVars.periodicBC2 == PBC_ON) {
		radiusVectorLattice(neighIndex5, &rx5, &ry5, &rz5);
		radiusVectorLattice(neighIndex6, &rx6, &ry6, &rz6);
	}

	/* include additions according to boundary conditions */
	if (userVars.periodicBC1 == PBC_ON) {
		ind1 = 1.0;
	}
	ind2 = 1.0;
	ind3 = 1.0;
	if (userVars.periodicBC2 == PBC_ON) {
		ind4 = 1.0;
		ind5 = 1.0;
	}
	if ((userVars.periodicBC1 == PBC_ON) && (userVars.periodicBC2 == PBC_ON)) {
		ind6 = 1.0;
	}

	/* calculate intermediate vectors to find result with boundary condition */
	if (userVars.periodicBC1 == PBC_ON) {
		calculateInterVector(rx1, ry1, rz1, rx2, ry2, rz2, rxi, ryi, rzi, &tx1, &ty1, &tz1);
	}
	calculateInterVector(rx2, ry2, rz2, rx3, ry3, rz3, rxi, ryi, rzi, &tx2, &ty2, &tz2);
	calculateInterVector(rx3, ry3, rz3, rx4, ry4, rz4, rxi, ryi, rzi, &tx3, &ty3, &tz3);
	if (userVars.periodicBC2 == PBC_ON) {
		calculateInterVector(rx4, ry4, rz4, rx5, ry5, rz5, rxi, ryi, rzi, &tx4, &ty4, &tz4);
		calculateInterVector(rx5, ry5, rz5, rx6, ry6, rz6, rxi, ryi, rzi, &tx5, &ty5, &tz5);
	}
	if ((userVars.periodicBC1 == PBC_ON) && (userVars.periodicBC2 == PBC_ON)) {
		calculateInterVector(rx6, ry6, rz6, rx1, ry1, rz1, rxi, ryi, rzi, &tx6, &ty6, &tz6);
	}

	/* find normal vectors of site (not normalized) */
	interVx = ind1 * tx1 + ind2 * tx2 + ind3 * tx3 + ind4 * tx4 + ind5 * tx5 + ind6 * tx6;
	interVy = ind1 * ty1 + ind2 * ty2 + ind3 * ty3 + ind4 * ty4 + ind5 * ty5 + ind6 * ty6;
	interVz = ind1 * tz1 + ind2 * tz2 + ind3 * tz3 + ind4 * tz4 + ind5 * tz5 + ind6 * tz6;

	/* find length of normal vectors (not normalized) */
	length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

	/* normalize normal vectors and return result */
	latticeParam[indexLattice].trinx = interVx / length;
	latticeParam[indexLattice].triny = interVy / length;
	latticeParam[indexLattice].trinz = interVz / length;

	/****************
	* (i,j), where i is 1 and j is N2
	*************/

	/* reset indexes of neighbour sites */
	neighIndex2 = 0; neighIndex3 = 0; neighIndex4 = 0; neighIndex5 = 0;
	/* reset indicators to include additions */
	ind1 = 0.0; ind2 = 0.0; ind3 = 0.0; ind4 = 0.0; ind5 = 0.0;

	/* find indexes of neighbours and current site with boundary condition */
	curIndex = IDXtrmg(1, N2);
	indexLattice = IDXtr(1, N2);
	neighIndex1 = IDXtrmg1(1, N2);
	if (userVars.periodicBC2 == PBC_ON) {
		neighIndex2 = IDXtrmg2(1, N2);
		neighIndex3 = IDXtrmg3(1, N2);
	}
	if (userVars.periodicBC1 == PBC_ON) {
		neighIndex4 = IDXtrmg4(1, N2);
	}
	if ((N2 % 2 == 0) || (userVars.periodicBC1 == PBC_ON)) {
		neighIndex5 = IDXtrmg5(1, N2);
	}
	neighIndex6 = IDXtrmg6(1, N2);

	/* find radius-vectors of neighbours and current site using indexes of "latticeParam" with boundary condition */
	radiusVectorLattice(curIndex, &rxi, &ryi, &rzi);
	radiusVectorLattice(neighIndex1, &rx1, &ry1, &rz1);
	if (userVars.periodicBC2 == PBC_ON) {
		radiusVectorLattice(neighIndex2, &rx2, &ry2, &rz2);
		radiusVectorLattice(neighIndex3, &rx3, &ry3, &rz3);
	}
	if (userVars.periodicBC1 == PBC_ON) {
		radiusVectorLattice(neighIndex4, &rx4, &ry4, &rz4);
	}
	if ((N2 % 2 == 0) || (userVars.periodicBC1 == PBC_ON)) {
		radiusVectorLattice(neighIndex5, &rx5, &ry5, &rz5);
	}
	radiusVectorLattice(neighIndex6, &rx6, &ry6, &rz6);

	/* include additions according to boundary conditions */
	if (userVars.periodicBC2 == PBC_ON) {
		ind1 = 1.0;
		ind2 = 1.0;
	}
	if ((userVars.periodicBC1 == PBC_ON) && (userVars.periodicBC2 == PBC_ON)) {
		ind3 = 1.0;
	}
	if (userVars.periodicBC1 == PBC_ON) {
		ind4 = 1.0;
	}
	if ((N2 % 2 == 0) || (userVars.periodicBC1 == PBC_ON)) {
		ind5 = 1.0;
	}
	ind6 = 1.0;

	/* calculate intermediate vectors to find result with boundary condition */
	if (userVars.periodicBC2 == PBC_ON) {
		calculateInterVector(rx1, ry1, rz1, rx2, ry2, rz2, rxi, ryi, rzi, &tx1, &ty1, &tz1);
		calculateInterVector(rx2, ry2, rz2, rx3, ry3, rz3, rxi, ryi, rzi, &tx2, &ty2, &tz2);
	}
	if ((userVars.periodicBC1 == PBC_ON) && (userVars.periodicBC2 == PBC_ON)) {
		calculateInterVector(rx3, ry3, rz3, rx4, ry4, rz4, rxi, ryi, rzi, &tx3, &ty3, &tz3);
	}
	if (userVars.periodicBC1 == PBC_ON) {
		calculateInterVector(rx4, ry4, rz4, rx5, ry5, rz5, rxi, ryi, rzi, &tx4, &ty4, &tz4);
	}
	if ((N2 % 2 == 0) || (userVars.periodicBC1 == PBC_ON)) {
		calculateInterVector(rx5, ry5, rz5, rx6, ry6, rz6, rxi, ryi, rzi, &tx5, &ty5, &tz5);
	}
	calculateInterVector(rx6, ry6, rz6, rx1, ry1, rz1, rxi, ryi, rzi, &tx6, &ty6, &tz6);

	/* find normal vectors of site (not normalized) */
	interVx = ind1 * tx1 + ind2 * tx2 + ind3 * tx3 + ind4 * tx4 + ind5 * tx5 + ind6 * tx6;
	interVy = ind1 * ty1 + ind2 * ty2 + ind3 * ty3 + ind4 * ty4 + ind5 * ty5 + ind6 * ty6;
	interVz = ind1 * tz1 + ind2 * tz2 + ind3 * tz3 + ind4 * tz4 + ind5 * tz5 + ind6 * tz6;

	/* find length of normal vectors (not normalized) */
	length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

	/* normalize normal vectors and return result */
	latticeParam[indexLattice].trinx = interVx / length;
	latticeParam[indexLattice].triny = interVy / length;
	latticeParam[indexLattice].trinz = interVz / length;

	/****************
	* (i,j), where i is N1 and j is N2
	*************/

	/* initialization of variables */

	/* reset indexes of neighbour sites */
	neighIndex1 = 0; neighIndex2 = 0; neighIndex3 = 0; neighIndex6 = 0;
	/* reset indicators to include additions */
	ind1 = 0.0; ind2 = 0.0; ind3 = 0.0; ind5 = 0.0; ind6 = 0.0;

	/* find indexes of neighbours and current site with boundary condition */
	curIndex = IDXtrmg(N1, N2);
	indexLattice = IDXtr(N1, N2);
	if (userVars.periodicBC1 == PBC_ON) {
		neighIndex1 = IDXtrmg1(N1, N2);
	}
	if (userVars.periodicBC2 == PBC_ON) {
		neighIndex2 = IDXtrmg2(N1, N2);
		neighIndex3 = IDXtrmg3(N1, N2);
	}
	neighIndex4 = IDXtrmg4(N1, N2);
	neighIndex5 = IDXtrmg5(N1, N2);
	if ((N2 % 2 == 1) || (userVars.periodicBC1 == PBC_ON)) {
		neighIndex6 = IDXtrmg6(N1, N2);
	}

	/* find radius-vectors of neighbours and current site using indexes of "latticeParam" with boundary condition */
	radiusVectorLattice(curIndex, &rxi, &ryi, &rzi);
	if (userVars.periodicBC1 == PBC_ON) {
		radiusVectorLattice(neighIndex1, &rx1, &ry1, &rz1);
	}
	if (userVars.periodicBC2 == PBC_ON) {
		radiusVectorLattice(neighIndex2, &rx2, &ry2, &rz2);
		radiusVectorLattice(neighIndex3, &rx3, &ry3, &rz3);
	}
	radiusVectorLattice(neighIndex4, &rx4, &ry4, &rz4);
	radiusVectorLattice(neighIndex5, &rx5, &ry5, &rz5);
	if ((N2 % 2 == 1) || (userVars.periodicBC1 == PBC_ON)) {
		radiusVectorLattice(neighIndex6, &rx6, &ry6, &rz6);
	}

	/* include additions according to boundary conditions */
	if ((userVars.periodicBC1 == PBC_ON) && (userVars.periodicBC2 == PBC_ON)) {
		ind1 = 1.0;
	}
	if (userVars.periodicBC2 == PBC_ON) {
		ind2 = 1.0;
		ind3 = 1.0;
	}
	ind4 = 1.0;
	if ((N2 % 2 == 1) || (userVars.periodicBC1 == PBC_ON)) {
		ind5 = 1.0;
	}
	if (userVars.periodicBC1 == PBC_ON) {
		ind6 = 1.0;
	}

	/* calculate intermediate vectors to find result with boundary condition */
	if ((userVars.periodicBC1 == PBC_ON) && (userVars.periodicBC2 == PBC_ON)) {
		calculateInterVector(rx1, ry1, rz1, rx2, ry2, rz2, rxi, ryi, rzi, &tx1, &ty1, &tz1);
	}
	if (userVars.periodicBC2 == PBC_ON) {
		calculateInterVector(rx2, ry2, rz2, rx3, ry3, rz3, rxi, ryi, rzi, &tx2, &ty2, &tz2);
		calculateInterVector(rx3, ry3, rz3, rx4, ry4, rz4, rxi, ryi, rzi, &tx3, &ty3, &tz3);
	}
	calculateInterVector(rx4, ry4, rz4, rx5, ry5, rz5, rxi, ryi, rzi, &tx4, &ty4, &tz4);
	if ((N2 % 2 == 1) || (userVars.periodicBC1 == PBC_ON)) {
		calculateInterVector(rx5, ry5, rz5, rx6, ry6, rz6, rxi, ryi, rzi, &tx5, &ty5, &tz5);
	}
	if (userVars.periodicBC1 == PBC_ON) {
		calculateInterVector(rx6, ry6, rz6, rx1, ry1, rz1, rxi, ryi, rzi, &tx6, &ty6, &tz6);
	}

	/* find normal vectors of site (not normalized) */
	interVx = ind1 * tx1 + ind2 * tx2 + ind3 * tx3 + ind4 * tx4 + ind5 * tx5 + ind6 * tx6;
	interVy = ind1 * ty1 + ind2 * ty2 + ind3 * ty3 + ind4 * ty4 + ind5 * ty5 + ind6 * ty6;
	interVz = ind1 * tz1 + ind2 * tz2 + ind3 * tz3 + ind4 * tz4 + ind5 * tz5 + ind6 * tz6;

	/* find length of normal vectors (not normalized) */
	length = sqrt(interVx * interVx + interVy * interVy + interVz * interVz);

	/* normalize normal vectors and return result */
	latticeParam[indexLattice].trinx = interVx / length;
	latticeParam[indexLattice].triny = interVy / length;
	latticeParam[indexLattice].trinz = interVz / length;

}

void NormalizeVectorsInitial(void) {

	/* variables to go through all nodes */
	size_t indexLattice, curIndex;
	/* length of vector of magnetic moment */
	double lengthVector;

	/* pass through each magnetic moment */
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {

				indexLattice = IDX(i1, i2, i3);
				/* if the node is magnetic, then go on */
				if (latticeParam[indexLattice].magnetic) {
					/* normalize per unit*/
					curIndex = IDXmg(i1, i2, i3);
					lengthVector = sqrt(
							lattice[curIndex] * lattice[curIndex] +
							lattice[curIndex + 1] * lattice[curIndex + 1] +
							lattice[curIndex + 2] * lattice[curIndex + 2]);
					lattice[curIndex] /= lengthVector;
					lattice[curIndex + 1] /= lengthVector;
					lattice[curIndex + 2] /= lengthVector;
				}

			}
		}
	}

}

#include <stdlib.h>

#include "../alltypes.h"
#include "../processing/processing.h"
#include "../processing/interface_memory.h"
#include "parallel_utils.h"
#include "parallel_utils_memory_cuda.h"

void envInit(char * projCaption, bool indEnvParallel) {
	envInitShared(projCaption);
}

void envFinalize(bool indEnvParallel) {
	envFinalizeShared();
}

void messageToStream(char * msg, FILE * stream) {

	/* write message */
	fprintf(stream, "%s\n", msg);

}

void sendData(void) {

}

void createSystemArraysOther(void) {

}

void freeMemory(void) {

	/* free arrays */
	freeArrays();
	/* free rows of "userVars" for names of files */
	free(userVars.paramFile);
	free(userVars.pathToWrite);
	free(userVars.projName);

}

bool doItInMasterProcess(void) {
	return true;
}

void synchronizeData(bool flag) {

}

void waitProcesses(void) {
}

void createSimulationArrays(void) {
	createSimulationArraysCUDA();
}

void destroySimulationArrays(void) {
	destroySimulationArraysCUDA();
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "CUnit/Basic.h"

#include "../alltypes.h"
#include "utils.h"
#include "utils_int.h"

int init_log_test(void) {
	return 0;
}

int clean_log_test(void) {
	return 0;
}

void test_create_log_file(void) {
	
	/* create different names and check whether dot was cleaned correctly */

	FILE *fid; /* pointer to file */
	char inputName[100]; /* name of file */

	/* name of file logfile */
	memset(inputName,0,sizeof(inputName));
	strcpy(inputName, "logfile");
	CU_ASSERT( createLogFiles(inputName) == 0);

	/* checking */
	fid = fopen("logfile_energy.log", "r");
	CU_ASSERT(fid != NULL);
	fclose(fid);
	remove("logfile_energy.log");

	fid = fopen("logfile_init.log", "r");
	CU_ASSERT(fid != NULL);
	fclose(fid);
	remove("logfile_init.log");

	fid = fopen("logfile_mag.log", "r");
	CU_ASSERT(fid != NULL);
	fclose(fid);
	remove("logfile_mag.log");

	fid = fopen("logfile_other.log", "r");
	CU_ASSERT(fid != NULL);
	fclose(fid);
	remove("logfile_other.log");

	/* name of file myresults */
	memset(inputName, 0, sizeof(inputName));
	strcpy(inputName, "myresults");
	CU_ASSERT(createLogFiles(inputName) == 0);

	/* checking */
	fid = fopen("myresults_energy.log", "r");
	CU_ASSERT(fid != NULL);
	remove("myresults_energy.log");

	fid = fopen("myresults_init.log", "r");
	CU_ASSERT(fid != NULL);
	remove("myresults_init.log");

	fid = fopen("myresults_mag.log", "r");
	CU_ASSERT(fid != NULL);
	remove("myresults_mag.log");

	fid = fopen("myresults_other.log", "r");
	CU_ASSERT(fid != NULL);
	remove("myresults_other.log");

}


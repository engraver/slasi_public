#ifndef INDECES_H
#define INDECES_H

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

/**
 * @brief declarations of inline functions to find neighbours of current magnetic node (array "latticeParam")
 * */
int IDXtr1(int i1, int i2);
int IDXtr2(int i1, int i2);
int IDXtr3(int i1, int i2);
int IDXtr4(int i1, int i2);
int IDXtr5(int i1, int i2);
int IDXtr6(int i1, int i2);

int IDXcub1(int i1, int i2, int i3);
int IDXcub2(int i1, int i2, int i3);
int IDXcub3(int i1, int i2, int i3);
int IDXcub4(int i1, int i2, int i3);
int IDXcub5(int i1, int i2, int i3);
int IDXcub6(int i1, int i2, int i3);

/**
 * @brief declarations of inline functions to find neighbours of current magnetic node (array "lattice")
 * */
int IDXtrmg1(int i1, int i2);
int IDXtrmg2(int i1, int i2);
int IDXtrmg3(int i1, int i2);
int IDXtrmg4(int i1, int i2);
int IDXtrmg5(int i1, int i2);
int IDXtrmg6(int i1, int i2);

int IDXcubmg1(int i1, int i2, int i3);
int IDXcubmg2(int i1, int i2, int i3);
int IDXcubmg3(int i1, int i2, int i3);
int IDXcubmg4(int i1, int i2, int i3);
int IDXcubmg5(int i1, int i2, int i3);
int IDXcubmg6(int i1, int i2, int i3);

/**
 * @brief declarations of inline functions to find indexes of current node
 * */
int IDXtr(int i1, int i2);
int IDXtrmg(int i1, int i2);

/**
 * @brief declarations of inline functions to find indexes (for coordinates) of current node
 * */
int IDXtrcrd(int i1, int i2);

/**
 * @brief macros for indexing nodes in rectangular lattices
 * */
#define IDX(i1, i2, i3) (((i1)*userVars.N2 + (i2))*userVars.N3 + (i3)) /**< indices to find current magnetic node in array of instances of structure "latticeParam" */
#define IDXmg(i1, i2, i3) 3*(((i1)*userVars.N2 + (i2))*userVars.N3 + (i3)) /**< indices to find magnetic moments of current node in array "lattice" */
#define IDXcoor(i1, i2, i3) 3*(((i1)*userVars.N2 + (i2))*userVars.N3 + (i3))+3*userVars.N1*userVars.N2*userVars.N3 /**< indices to find coordinates of current node in array "lattice" */

/**
 * @brief macros for indexing nodes in rectangular lattices (GPU version)
 * */
#define IDXGPU(i1, i2, i3) (((i1)*N2Ext + (i2))*N3Ext + (i3)) /**< indices to find current magnetic node in array of instances of structure "latticeParam" */
#define IDXmgGPU(i1, i2, i3) 3*(((i1)*N2Ext + (i2))*N3Ext + (i3)) /**< indices to find magnetic moments of current node in array "lattice" */

#define IsODD(ind) ((ind) & 1)
#define IsEVEN(ind) (!((ind) & 1))

#endif

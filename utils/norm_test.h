#ifndef NORM_TEST_H
#define NORM_TEST_H

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

int init_norm_test(void);

int clean_norm_test(void);

char * test_normTriangles_1_desc = "Test for normal vectors of triangles: flat surface 5x5";
void test_normTriangles_1(void);

char * test_normTriangles_2_desc = "Test for normal vectors of triangles: cylinder 50x5";
void test_normTriangles_2(void);

char * test_normSites_1_desc = "Test for normal vectors of sites: sinusoidal surface 20x7";
void test_normSites_1(void);

char * test_normSites_2_desc = "Test for normal vectors of sites: cylinder 100x5";
void test_normSites_2(void);

#endif

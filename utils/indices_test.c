#include <stdio.h>
#include <string.h>
#include "CUnit/Basic.h"

#include "../alltypes.h"
#include "utils.h"
#include "utils_int.h"

int init_indices_test(void) {
	return 0;
}

int clean_indices_test(void) {
	return 0;
}

/*
 * @brief yet another test of IDX(i1, i2, i3)
 * 
 * Spin chain of 8 spins. 
 * All data is correct
 * 
 * Slices:
 * @verbatim 
 *  0   1   2   3   4   5   6   7   8   9   10   
 * 000 000 000 000 000 000 000 000 000 000 000 
 * 000 0+0 0+0 0+0 0+0 0+0 0+0 0X0 0+0 0+0 000
 * 000 000 000 000 000 000 000 000 000 000 000
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. X is the target site.
 * 
 * Spin X has indices (7, 1, 1) and position in `latticeParam` array
 * 3*3*7 + 3 + 2 - 1 = 67 
 */
void test_IDX_1(void) {
	userVars.N1 = 10;
	userVars.N2 = 3;
	userVars.N3 = 3;
	
	int ind = IDX(7, 1, 1);
	CU_ASSERT(ind == 67);
}

/*
 * @brief yet another test of IDX(i1, i2, i3)
 *
 * stripe which has size 3 * 8
 * All data is correct
 *
 * Slices:
 * @verbatim
 *  0   1   2   3   4   5   6   7   8   9   10
 * 000 000 000 000 000 000 000 000 000 000 000
 * 000 0+0 0X0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 000
 * 000 0+0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 000
 * 000 0+0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 000
 * 000 000 000 000 000 000 000 000 000 000 000
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. X is the target site.
 *
 * Spin X has indices (2, 1, 1) and position in `latticeParam` array
 * 3*5*2 + 3 + 2 - 1 = 34
 */
void test_IDX_2(void) {

	userVars.N1 = 10;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDX(2, 1, 1);
	CU_ASSERT(ind == 34);

}

/*
 * @brief yet another test of IDXmg(i1, i2, i3)
 * 
 * Spin chain of 9 spins. 
 * All data is correct
 * 
 * Slices:
 * @verbatim 
 *  0   1   2   3   4   5   6   7   8   9   10   
 * 000 000 000 000 000 000 000 000 000 000 000 
 * 000 0+0 0+0 0+0 0+0 0+0 0+0 0X0 0+0 0+0 000
 * 000 000 000 000 000 000 000 000 000 000 000
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. X is the target site.
 * 
 * Spin X has indices (7, 1, 1) and position in `latticeParam` array 
 * 3*3*7 + 3 + 2 - 1 = 67 
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 67*3 = 201 index of mx component
 */
void test_IDXmg_1(void) {

	userVars.N1 = 10;
	userVars.N2 = 3;
	userVars.N3 = 3;
	
	int ind = IDXmg(7, 1, 1);
	CU_ASSERT(ind == 201);

}

/*
 * @brief yet another test of IDXmg(i1, i2, i3)
 *
 * stripe which has size 3 * 8
 * All data is correct
 *
 * Slices:
 * @verbatim
 *  0   1   2   3   4   5   6   7   8   9   10
 * 000 000 000 000 000 000 000 000 000 000 000
 * 000 0+0 0X0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 000
 * 000 0+0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 000
 * 000 0+0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 000
 * 000 000 000 000 000 000 000 000 000 000 000
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. X is the target site.
 *
 * Spin X has indices (2, 1, 1) and position in `latticeParam` array
 * 3*5*2 + 3 + 2 - 1 = 34
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 34*3 = 102 index of mx component
 */
void test_IDXmg_2(void) {

	userVars.N1 = 10;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXmg(2, 1, 1);
	CU_ASSERT(ind == 102);

}

/*
 * @brief yet another test of IDXcoor(i1, i2, i3)
 *
 * Spin chain of 9 spins.
 * All data is correct
 *
 * Slices:
i * @verbatim
 *  0   1   2   3   4   5   6   7   8   9   10
 * 000 000 000 000 000 000 000 000 000 000 000
 * 000 0+0 0+0 0+0 0+0 0+0 0+0 0X0 0+0 0+0 000
 * 000 000 000 000 000 000 000 000 000 000 000
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. X is the target site.
 *
 * Spin X has indices (7, 1, 1) and position in `latticeParam` array
 * 3*3*7 + 3 + 2 - 1 = 67
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site and bias on 3*userVars.N1*userVars.N2*userVars.N3 numbers,
 * because components of magnetic moments are the first in the array,
 * thus, one has 67*3 + 3*3*3*10 = 471 index of x component
 */
void test_IDXcrd_1(void) {

	userVars.N1 = 10;
	userVars.N2 = 3;
	userVars.N3 = 3;

	int ind = IDXcoor(7, 1, 1);
	CU_ASSERT(ind == 471);

}

/*
 * @brief yet another test of IDXcoor(i1, i2, i3)
 *
 * stripe which has size 3 * 8
 * All data is correct
 *
 * Slices:
 * @verbatim
 *  0   1   2   3   4   5   6   7   8   9   10
 * 000 000 000 000 000 000 000 000 000 000 000
 * 000 0+0 0X0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 000
 * 000 0+0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 000
 * 000 0+0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 0+0 000
 * 000 000 000 000 000 000 000 000 000 000 000
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. X is the target site.
 *
 * Spin X has indices (2, 1, 1) and position in `latticeParam` array
 * 3*5*2 + 3 + 2 - 1 = 34
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site and bias on 3*userVars.N1*userVars.N2*userVars.N3 numbers,
 * because components of magnetic moments are the first in the array,
 * thus, one has 34*3 + 3*3*5*10 = 552 index of x component
 */
void test_IDXcrd_2(void) {

	userVars.N1 = 10;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXcoor(2, 1, 1);
	CU_ASSERT(ind == 552);

}

/*
 * @brief yet another test of IDXtr(i1, i2)
 * 
 * Triangular lattice of the following structure in 1-2 plane:
 * 
 * @verbatim 
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0 
 *  \ / \ / \ / \ / \
 *   0---+---+---X---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. X is the target site. 
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *  0   1   2   3   4
 * 000 000 000 000 000
 * 000 0+0 0+0 0+0 000
 * 000 0+0 0+0 0X0 000
 * 000 0+0 0+0 0+0 000
 * 000 000 000 000 000
 * @endverbatim
 * 
 * Spin X has indices (3, 2) and position in `latticeParam` array 
 * 3*5*3 + 3*2 + 2 - 1 = 52
 */
void test_IDXtr_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;
	
	int ind = IDXtr(3, 2);
	CU_ASSERT(ind == 52);

}

/*
 * @brief yet another test of IDXtr(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---+---X---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. X is the target site.
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *  0   1   2   3   4
 * 000 000 000 000 000
 * 000 0+0 0+0 0+0 000
 * 000 0+0 0X0 0+0 000
 * 000 0+0 0+0 0+0 000
 * 000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (2, 2) and position in `latticeParam` array
 * 3*5*2 + 3*2 + 2 - 1 = 37
 */
void test_IDXtr_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtr(2, 2);
	CU_ASSERT(ind == 37);

}

/*
 * @brief yet another test of IDXtrmg(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---+---+---X---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. X is the target site.
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *  0   1   2   3   4
 * 000 000 000 000 000
 * 000 0+0 0+0 0+0 000
 * 000 0+0 0+0 0X0 000
 * 000 0+0 0+0 0+0 000
 * 000 000 000 000 000
 * @endverbatim
 * Spin X has indices (3, 2) and position in `latticeParam` array
 * 3*5*3 + 3*2 + 2 - 1 = 52
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 52*3 = 156 index of mx component
 */
void test_IDXtrmg_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtrmg(3, 2);
	CU_ASSERT(ind == 156);

}

/*
 * @brief yet another test of IDXtrmg(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---+---X---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. X is the target site.
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *  0   1   2   3   4
 * 000 000 000 000 000
 * 000 0+0 0+0 0+0 000
 * 000 0+0 0X0 0+0 000
 * 000 0+0 0+0 0+0 000
 * 000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (2, 2) and position in `latticeParam` array
 * 3*5*2 + 3*2 + 2 - 1 = 37.
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 37*3 = 111 index of mx component
 */
void test_IDXtrmg_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtrmg(2, 2);
	CU_ASSERT(ind == 111);

}

/*
 * @brief yet another test of IDXtrcrd(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---+---+---X---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. X is the target site.
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *  0   1   2   3   4
 * 000 000 000 000 000
 * 000 0+0 0+0 0+0 000
 * 000 0+0 0+0 0X0 000
 * 000 0+0 0+0 0+0 000
 * 000 000 000 000 000
 * @endverbatim
 * Spin X has indices (3, 2) and position in `latticeParam` array
 * 3*5*3 + 3*2 + 2 - 1 = 52
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site and bias on 3*userVars.N1*userVars.N2*userVars.N3 numbers,
 * because components of magnetic moments are the first in the array,
 * thus, one has 52*3 + 3*3*5*10 = 381 index of x component
 */
void test_IDXtrcrd_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtrcrd(3, 2);
	CU_ASSERT(ind == 381);

}

/*
 * @brief yet another test of IDXtrcrd(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---+---X---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. X is the target site.
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *  0   1   2   3   4
 * 000 000 000 000 000
 * 000 0+0 0+0 0+0 000
 * 000 0+0 0X0 0+0 000
 * 000 0+0 0+0 0+0 000
 * 000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (2, 2) and position in `latticeParam` array
 * 3*5*2 + 3*2 + 2 - 1 = 37.
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site and bias on 3*userVars.N1*userVars.N2*userVars.N3 numbers,
 * because components of magnetic moments are the first in the array,
 * thus, one has 37*3 + 3*3*5*5 = 336 index of x component
 */
void test_IDXtrcrd_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtrcrd(2, 2);
	CU_ASSERT(ind == 336);

}

/*
 * @brief yet another test of IDXtr1(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---+---C---X---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 1st neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 4  000 000 000 000 000
 * 3  000 0+0 0+0 0+0 000
 * 2  000 0+0 0C0 0X0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (3, 2) and position in `latticeParam` array
 * 3*5*3 + 3*2 + 2 - 1 = 52
 */
void test_IDXtr1_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtr1(2, 2);
	CU_ASSERT(ind == 52);

}

/*
 * @brief yet another test of IDXtr1(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 * 0---0---0---0---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---C---X---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 1st neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 5  000 000 000 000 000
 * 4  000 0+0 0+0 0+0 000
 * 3  000 0+0 0C0 0X0 000
 * 2  000 0+0 0+0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (3, 3) and position in `latticeParam` array
 * 3*6*3 + 3*3 + 2 - 1 = 64
 */
void test_IDXtr1_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 6;
	userVars.N3 = 3;

	int ind = IDXtr1(2, 3);
	CU_ASSERT(ind == 64);

}

/*
 * @brief yet another test of IDXtrmg1(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---+---C---X---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 1st neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 4  000 000 000 000 000
 * 3  000 0+0 0+0 0+0 000
 * 2  000 0+0 0C0 0X0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (3, 2) and position in `latticeParam` array
 * 3*5*3 + 3*2 + 2 - 1 = 52
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 52*3 = 156 index of mx component
 */
void test_IDXtrmg1_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtrmg1(2, 2);
	CU_ASSERT(ind == 156);

}

/*
 * @brief yet another test of IDXtrmg1(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 * 0---0---0---0---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---C---X---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 1st neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 5  000 000 000 000 000
 * 4  000 0+0 0+0 0+0 000
 * 3  000 0+0 0C0 0X0 000
 * 2  000 0+0 0+0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (3, 3) and position in `latticeParam` array
 * 3*6*3 + 3*3 + 2 - 1 = 64
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 64*3 = 192 index of mx component
 */
void test_IDXtrmg1_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 6;
	userVars.N3 = 3;

	int ind = IDXtrmg1(2, 3);
	CU_ASSERT(ind == 192);

}

/*
 * @brief yet another test of IDXtr2(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---X---0
 *  \ / \ / \ / \ / \
 *   0---+---C---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 2nd neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 4  000 000 000 000 000
 * 3  000 0+0 0+0 0X0 000
 * 2  000 0+0 0C0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (3, 3) and position in `latticeParam` array
 * 3*5*3 + 3*3 + 2 - 1 = 55
 */
void test_IDXtr2_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtr2(2, 2);
	CU_ASSERT(ind == 55);

}

/*
 * @brief yet another test of IDXtr2(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 * 0---0---0---0---0
 *  \ / \ / \ / \ / \
 *   0---+---X---+---0
 *  / \ / \ / \ / \ /
 * 0---+---C---+---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 2nd neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 5  000 000 000 000 000
 * 4  000 0+0 0X0 0+0 000
 * 3  000 0+0 0C0 0+0 000
 * 2  000 0+0 0+0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (2, 4) and position in `latticeParam` array
 * 3*6*2 + 3*4 + 2 - 1 = 49
 */
void test_IDXtr2_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 6;
	userVars.N3 = 3;

	int ind = IDXtr2(2, 3);
	CU_ASSERT(ind == 49);

}

/*
 * @brief yet another test of IDXtrmg2(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---X---0
 *  \ / \ / \ / \ / \
 *   0---+---C---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 2nd neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 4  000 000 000 000 000
 * 3  000 0+0 0+0 0X0 000
 * 2  000 0+0 0C0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (3, 3) and position in `latticeParam` array
 * 3*5*3 + 3*3 + 2 - 1 = 55
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 55*3 = 165 index of mx component
 */
void test_IDXtrmg2_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtrmg2(2, 2);
	CU_ASSERT(ind == 165);

}

/*
 * @brief yet another test of IDXtrmg2(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 * 0---0---0---0---0
 *  \ / \ / \ / \ / \
 *   0---+---X---+---0
 *  / \ / \ / \ / \ /
 * 0---+---C---+---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 2nd neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 5  000 000 000 000 000
 * 4  000 0+0 0X0 0+0 000
 * 3  000 0+0 0C0 0+0 000
 * 2  000 0+0 0+0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (2, 4) and position in `latticeParam` array
 * 3*6*2 + 3*4 + 2 - 1 = 49
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 49*3 = 147 index of mx component
 */
void test_IDXtrmg2_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 6;
	userVars.N3 = 3;

	int ind = IDXtrmg2(2, 3);
	CU_ASSERT(ind == 147);

}

/*
 * @brief yet another test of IDXtr3(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---X---+---0
 *  \ / \ / \ / \ / \
 *   0---+---C---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 3rd neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 4  000 000 000 000 000
 * 3  000 0+0 0X0 0+0 000
 * 2  000 0+0 0C0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (2, 3) and position in `latticeParam` array
 * 3*5*2 + 3*3 + 2 - 1 = 40
 */
void test_IDXtr3_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtr3(2, 2);
	CU_ASSERT(ind == 40);

}

/*
 * @brief yet another test of IDXtr3(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 * 0---0---0---0---0
 *  \ / \ / \ / \ / \
 *   0---X---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---C---+---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 3rd neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 5  000 000 000 000 000
 * 4  000 0X0 0+0 0+0 000
 * 3  000 0+0 0C0 0+0 000
 * 2  000 0+0 0+0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (1, 4) and position in `latticeParam` array
 * 3*6*1 + 3*4 + 2 - 1 = 31
 */
void test_IDXtr3_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 6;
	userVars.N3 = 3;

	int ind = IDXtr3(2, 3);
	CU_ASSERT(ind == 31);

}

/*
 * @brief yet another test of IDXtrmg3(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---X---+---0
 *  \ / \ / \ / \ / \
 *   0---+---C---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 3rd neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 4  000 000 000 000 000
 * 3  000 0+0 0X0 0+0 000
 * 2  000 0+0 0C0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (2, 3) and position in `latticeParam` array
 * 3*5*2 + 3*3 + 2 - 1 = 40
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 40*3 = 120 index of mx component
 */
void test_IDXtrmg3_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtrmg3(2, 2);
	CU_ASSERT(ind == 120);

}

/*
 * @brief yet another test of IDXtrmg3(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 * 0---0---0---0---0
 *  \ / \ / \ / \ / \
 *   0---X---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---C---+---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 3rd neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 5  000 000 000 000 000
 * 4  000 0X0 0+0 0+0 000
 * 3  000 0+0 0C0 0+0 000
 * 2  000 0+0 0+0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (1, 4) and position in `latticeParam` array
 * 3*6*1 + 3*4 + 2 - 1 = 31
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 31*3 = 93 index of mx component
 */
void test_IDXtrmg3_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 6;
	userVars.N3 = 3;

	int ind = IDXtrmg3(2, 3);
	CU_ASSERT(ind == 93);

}

/*
 * @brief yet another test of IDXtr4(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---X---C---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 4th neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 4  000 000 000 000 000
 * 3  000 0+0 0+0 0+0 000
 * 2  000 0X0 0C0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (1, 2) and position in `latticeParam` array
 * 3*5*1 + 3*2 + 2 - 1 = 22
 */
void test_IDXtr4_1(void) {
	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtr4(2, 2);
	CU_ASSERT(ind == 22);
}

/*
 * @brief yet another test of IDXtr4(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 * 0---0---0---0---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---X---C---+---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 4th neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 5  000 000 000 000 000
 * 4  000 0+0 0+0 0+0 000
 * 3  000 0X0 0C0 0+0 000
 * 2  000 0+0 0+0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (1, 3) and position in `latticeParam` array
 * 3*6*1 + 3*3 + 2 - 1 = 28
 */
void test_IDXtr4_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 6;
	userVars.N3 = 3;

	int ind = IDXtr4(2, 3);
	CU_ASSERT(ind == 28);

}

/*
 * @brief yet another test of IDXtrmg4(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---X---C---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 4th neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 4  000 000 000 000 000
 * 3  000 0+0 0+0 0+0 000
 * 2  000 0X0 0C0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (1, 2) and position in `latticeParam` array
 * 3*5*1 + 3*2 + 2 - 1 = 22
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 22*3 = 66 index of mx component
 */
void test_IDXtrmg4_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtrmg4(2, 2);
	CU_ASSERT(ind == 66);

}

/*
 * @brief yet another test of IDXtrmg4(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 * 0---0---0---0---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---X---C---+---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 4th neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 5  000 000 000 000 000
 * 4  000 0+0 0+0 0+0 000
 * 3  000 0X0 0C0 0+0 000
 * 2  000 0+0 0+0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (1, 3) and position in `latticeParam` array
 * 3*6*1 + 3*3 + 2 - 1 = 28
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 28*3 = 84 index of mx component
 */
void test_IDXtrmg4_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 6;
	userVars.N3 = 3;

	int ind = IDXtrmg4(2, 3);
	CU_ASSERT(ind == 84);

}

/*
 * @brief yet another test of IDXtr5(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---+---C---+---0
 *  / \ / \ / \ / \ /
 * 0---+---X---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 5th neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 4  000 000 000 000 000
 * 3  000 0+0 0+0 0+0 000
 * 2  000 000 0C0 0+0 000
 * 1  000 0+0 0X0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (2, 1) and position in `latticeParam` array
 * 3*5*2 + 3*1 + 2 - 1 = 34
 */
void test_IDXtr5_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtr5(2, 2);
	CU_ASSERT(ind == 34);

}

/*
 * @brief yet another test of IDXtr5(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 * 0---0---0---0---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---C---+---0
 *  \ / \ / \ / \ / \
 *   0---X---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 5th neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 5  000 000 000 000 000
 * 4  000 0+0 0+0 0+0 000
 * 3  000 0+0 0C0 0+0 000
 * 2  000 0X0 0+0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (1, 2) and position in `latticeParam` array
 * 3*6*1 + 3*2 + 2 - 1 = 25
 */
void test_IDXtr5_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 6;
	userVars.N3 = 3;

	int ind = IDXtr5(2, 3);
	CU_ASSERT(ind == 25);

}

/*
 * @brief yet another test of IDXtrmg5(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---+---C---+---0
 *  / \ / \ / \ / \ /
 * 0---+---X---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 5th neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 4  000 000 000 000 000
 * 3  000 0+0 0+0 0+0 000
 * 2  000 000 0C0 0+0 000
 * 1  000 0+0 0X0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (2, 1) and position in `latticeParam` array
 * 3*5*2 + 3*1 + 2 - 1 = 34
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 34*3 = 102 index of mx component
 */
void test_IDXtrmg5_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtrmg5(2, 2);
	CU_ASSERT(ind == 102);

}

/*
 * @brief yet another test of IDXtrmg5(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 * 0---0---0---0---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---C---+---0
 *  \ / \ / \ / \ / \
 *   0---X---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 5th neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 5  000 000 000 000 000
 * 4  000 0+0 0+0 0+0 000
 * 3  000 0+0 0C0 0+0 000
 * 2  000 0X0 0+0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (1, 2) and position in `latticeParam` array
 * 3*6*1 + 3*2 + 2 - 1 = 25
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 25*3 = 75 index of mx component
 */
void test_IDXtrmg5_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 6;
	userVars.N3 = 3;

	int ind = IDXtrmg5(2, 3);
	CU_ASSERT(ind == 75);

}

/*
 * @brief yet another test of IDXtr6(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---+---C---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---X---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 5th neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 4  000 000 000 000 000
 * 3  000 0+0 0+0 0+0 000
 * 2  000 000 0C0 0+0 000
 * 1  000 0+0 0+0 0X0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (3, 1) and position in `latticeParam` array
 * 3*5*3 + 3*1 + 2 - 1 = 49
 */
void test_IDXtr6_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtr6(2, 2);
	CU_ASSERT(ind == 49);

}

/*
 * @brief yet another test of IDXtr6(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 * 0---0---0---0---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---C---+---0
 *  \ / \ / \ / \ / \
 *   0---+---X---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 6th neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 5  000 000 000 000 000
 * 4  000 0+0 0+0 0+0 000
 * 3  000 0+0 0C0 0+0 000
 * 2  000 0+0 0X0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (2, 2) and position in `latticeParam` array
 * 3*6*2 + 3*2 + 2 - 1 = 43
 */
void test_IDXtr6_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 6;
	userVars.N3 = 3;

	int ind = IDXtr6(2, 3);
	CU_ASSERT(ind == 43);

}

/*
 * @brief yet another test of IDXtrmg6(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 *   0---0---0---0---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---+---C---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---X---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 5th neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 4  000 000 000 000 000
 * 3  000 0+0 0+0 0+0 000
 * 2  000 000 0C0 0+0 000
 * 1  000 0+0 0+0 0X0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (3, 1) and position in `latticeParam` array
 * 3*5*3 + 3*1 + 2 - 1 = 49
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 49*3 = 147 index of mx component
 */
void test_IDXtrmg6_1(void) {

	userVars.N1 = 5;
	userVars.N2 = 5;
	userVars.N3 = 3;

	int ind = IDXtrmg6(2, 2);
	CU_ASSERT(ind == 147);

}

/*
 * @brief yet another test of IDXtrmg6(i1, i2)
 *
 * Triangular lattice of the following structure in 1-2 plane:
 *
 * @verbatim
 * 0---0---0---0---0
 *  \ / \ / \ / \ / \
 *   0---+---+---+---0
 *  / \ / \ / \ / \ /
 * 0---+---C---+---0
 *  \ / \ / \ / \ / \
 *   0---+---X---+---0
 *  / \ / \ / \ / \ /
 * 0---+---+---+---0
 *  \ / \ / \ / \ / \
 *   0---0---0---0---0
 * @endverbatim
 * Here, 0 is nonmagnetic site and + is magnetic. C is the current site.
 * X is the target site (the 6th neighbour).
 * Actually, it is considered as a set of cuts in 2-3 planes:
 * @verbatim
 *     0   1   2   3   4
 * 5  000 000 000 000 000
 * 4  000 0+0 0+0 0+0 000
 * 3  000 0+0 0C0 0+0 000
 * 2  000 0+0 0X0 0+0 000
 * 1  000 0+0 0+0 0+0 000
 * 0  000 000 000 000 000
 * @endverbatim
 *
 * Spin X has indices (2, 2) and position in `latticeParam` array
 * 3*6*2 + 3*2 + 2 - 1 = 43
 * But there are three positions (mx, my, mz) in `lattice` array for the
 * each site, thus, one has 43*3 = 129 index of mx component
 */
void test_IDXtrmg6_2(void) {

	userVars.N1 = 5;
	userVars.N2 = 6;
	userVars.N3 = 3;

	int ind = IDXtrmg6(2, 3);
	CU_ASSERT(ind == 129);

}

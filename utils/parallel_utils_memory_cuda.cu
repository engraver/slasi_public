#include "../alltypes.h"
#include <sys/mman.h>
extern "C" {
	#include "parallel_utils_memory_cuda.h"
}

/* to debug code */
//__global__
//void printArray(double *array) {
//	printf("%f %f %f\n", array[0], array[1], array[2]);
//}

/* function to create square two-dimensional array in GPU */
void createTwoDimensionalArray(double ** d_arrayHost, double ** d_array, size_t size) {

	/* create array of pointers */
	cudaMalloc((void **) &d_array, size * sizeof(double *));
	d_arrayHost = (double **) malloc(size * sizeof(double *));

	/* fill array of pointers */
	for (size_t i = 0; i < size; i++) {
		cudaMalloc(&d_arrayHost[i], size * sizeof(double));
		cudaMemset(d_arrayHost[i], 0, size * sizeof(double));
	}
	/* copy array of pointers to GPU */
	cudaMemcpy(d_array, d_arrayHost, size * sizeof(double *), cudaMemcpyHostToDevice);

}

/* function to clear square two-dimensional array in GPU */
void clearTwoDimensionalArray(double ** d_arrayHost, double ** d_array, size_t size) {

	/* clear arrays that hides behind pointers */
	for (size_t i = 0; i < size; i++) {
		cudaFree(d_arrayHost[i]);
	}

	/* clear arrays of pointers */
	cudaFree(d_array);
	free(d_arrayHost);

}

/* function to create arrays for simulation */
extern "C"
void createSimulationArraysCUDA(void) {

	/* sizes of arrays */
	unsigned long sizeOfArrsInBytes = NUMBER_COMP * sizeof(double) * (unsigned long)(userVars.N1 * userVars.N2 * userVars.N3);
	unsigned long sizeOfArrsInBytesInt = NUMBER_COMP * sizeof(int) * (unsigned long)(userVars.N1 * userVars.N2 * userVars.N3);
	unsigned long sizeOfArrsInBytesConn = MAX_CONN_NUM * sizeof(int) * (unsigned long)(connElements);

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;

	if (userVars.integrationMethod == INTEGRATION_METHOD_RKF) {
		/* create array for sudden changes of magnetic moments, coordinates and velocities (GPU) */
		cudaMalloc(&d_dm5, NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		cudaMemset(d_dm5, 0, NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

		/* create arrays for intermediate values of RKF45 (pointers in GPU and main memory) */
		cudaMalloc((void **) &d_k, 6 * sizeof(double *));
		for (int i = 0; i < 6; i++) {
			cudaMalloc(&d_kHost[i], NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
			cudaMemset(d_kHost[i], 0, NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		}
		cudaMemcpy(d_k, d_kHost, 6 * sizeof(double *), cudaMemcpyHostToDevice);
	} else if (userVars.integrationMethod == INTEGRATION_METHOD_MIDDLE_POINT) {
		/* create final array for sudden changes of magnetic moments, coordinates and velocities (GPU) */
		cudaMalloc(&d_dmFinal, NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		cudaMemset(d_dmFinal, 0, NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

		/* create array for sudden changes of magnetic moments, coordinates and velocities (GPU) in middle point */
		cudaMalloc(&d_dmHalf, NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		cudaMemset(d_dmHalf, 0, NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

		/* create array for magnetic moments (GPU) in middle point */
		cudaMalloc(&d_latticeHalf, NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		cudaMemset(d_latticeHalf, 0, NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	}

	/* create array of effective field */
	cudaMalloc(&d_H, NUMBER_COMP_MAGN * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	cudaMemset(d_H, 0, NUMBER_COMP_MAGN * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

	/* create array of force */
	cudaMalloc(&d_F, NUMBER_COMP_MAGN * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	cudaMemset(d_F, 0, NUMBER_COMP_MAGN * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

	/* create array of exchange energy (GPU and main memory) */
	cudaMalloc(&d_EnergyExchangeArray, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	cudaMemset(d_EnergyExchangeArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	EnergyExchangeArray = (double *) malloc(sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	memset(EnergyExchangeArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

	/* create array of anisotropy energy along the first axis (GPU and main memory) */
	cudaMalloc(&d_EnergyAnisotropyArrayAxisK1, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	cudaMemset(d_EnergyAnisotropyArrayAxisK1, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	EnergyAnisotropyArrayAxisK1 = (double *) malloc(sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	memset(EnergyAnisotropyArrayAxisK1, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

	/* create array of anisotropy energy along the second axis (GPU and main memory) */
	cudaMalloc(&d_EnergyAnisotropyArrayAxisK2, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	cudaMemset(d_EnergyAnisotropyArrayAxisK2, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	EnergyAnisotropyArrayAxisK2 = (double *) malloc(sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	memset(EnergyAnisotropyArrayAxisK2, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

	/* create array of anisotropy energy along the third axis (GPU and main memory) */
	cudaMalloc(&d_EnergyAnisotropyArrayAxisK3, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	cudaMemset(d_EnergyAnisotropyArrayAxisK3, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	EnergyAnisotropyArrayAxisK3 = (double *) malloc(sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	memset(EnergyAnisotropyArrayAxisK3, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

	/* create array of dipole energy (GPU and main memory) */
	cudaMalloc(&d_EnergyDipoleArray, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	cudaMemset(d_EnergyDipoleArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	EnergyDipoleArray = (double *) malloc(sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	memset(EnergyDipoleArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

	/* create array of external field energy (GPU and main memory) */
	cudaMalloc(&d_EnergyExternalArray, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	cudaMemset(d_EnergyExternalArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	EnergyExternalArray = (double *) malloc(sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	memset(EnergyExternalArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

	/* it is only for cubic system */
	if (userVars.latticeType == LATTICE_CUBIC) {
		/* create array of DMI energy along the first axis (GPU and main memory) */
		cudaMalloc(&d_EnergyDMIAxis1Array, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		cudaMemset(d_EnergyDMIAxis1Array, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		EnergyDMIAxis1Array = (double *) malloc(sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		memset(EnergyDMIAxis1Array, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

		/* create array of DMI energy along the second axis (GPU and main memory) */
		cudaMalloc(&d_EnergyDMIAxis2Array, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		cudaMemset(d_EnergyDMIAxis2Array, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		EnergyDMIAxis2Array = (double *) malloc(sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		memset(EnergyDMIAxis2Array, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

		/* create array of DMI energy along the third axis (GPU and main memory) */
		cudaMalloc(&d_EnergyDMIAxis3Array, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		cudaMemset(d_EnergyDMIAxis3Array, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		EnergyDMIAxis3Array = (double *) malloc(sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		memset(EnergyDMIAxis3Array, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	}

	/* it is only for system with triangular lattice */
	if (userVars.latticeType == LATTICE_TRIANGULAR) {
		/* create array of interface DMI energy (GPU and main memory) */
		cudaMalloc(&d_EnergyDMITriangleInterfaceArray, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		cudaMemset(d_EnergyDMITriangleInterfaceArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		EnergyDMITriangleInterfaceArray = (double *) malloc(sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		memset(EnergyDMITriangleInterfaceArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

		/* create array of bulk DMI energy (GPU and main memory) */
		cudaMalloc(&d_EnergyDMITriangleBulkArray, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		cudaMemset(d_EnergyDMITriangleBulkArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		EnergyDMITriangleBulkArray = (double *) malloc(sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		memset(EnergyDMITriangleBulkArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

		/* create array of stretching energy (GPU and main memory) */
		cudaMalloc(&d_EnergyStretchingArray, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		cudaMemset(d_EnergyStretchingArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		EnergyStretchingArray = (double *) malloc(sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		memset(EnergyStretchingArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

		/* create array of bending energy (GPU and main memory) */
		cudaMalloc(&d_EnergyBendingArray, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		cudaMemset(d_EnergyBendingArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		EnergyBendingArray = (double *) malloc(sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
		memset(EnergyBendingArray, 0, sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	}

	/* create array of changes of magnetic moments */
	DmDt = (double *) malloc(NUMBER_COMP_MAGN * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	memset(DmDt, 0, NUMBER_COMP_MAGN * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

	/* create array of changes of coordinates */
	DrDt = (double *) malloc(NUMBER_COMP_MAGN * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	memset(DrDt, 0, NUMBER_COMP_MAGN * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

	/* create array for intermadiate magnetic moments, coordinate and velocities of RKF45 */
	cudaMalloc(&d_mCh, NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	cudaMemset(d_mCh, 0, NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);

	/* copy array "lattice" to device */
	cudaMalloc(&d_lattice, NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
	cudaMemcpy(d_lattice, lattice, NUMBER_COMP * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3, cudaMemcpyHostToDevice);

	/* copy array "latticeParam" to device */
	cudaMalloc(&d_latticeParam, sizeof(LatticeSiteDescr) * userVars.N1 * userVars.N2 * userVars.N3);
	cudaMemcpy(d_latticeParam, latticeParam, numberSites * sizeof(LatticeSiteDescr), cudaMemcpyHostToDevice);

	/* copy array "Bamp" to device */
	cudaMalloc(&d_Bamp, NUMBER_COMP_FIELD * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3);
   	cudaMemcpy(d_Bamp, Bamp, NUMBER_COMP_FIELD * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3, cudaMemcpyHostToDevice);

	/* create additional vectors for VTK XML BASE64 format */
	addDoubleVectorXML = (double *) mmap(NULL, sizeOfArrsInBytes, PROT_READ | PROT_WRITE,
	                                     MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	addIntVectorXML = (int *) mmap(NULL, sizeOfArrsInBytesInt, PROT_READ | PROT_WRITE,
	                               MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	addIntVectorConnXML = (int *) mmap(NULL, sizeOfArrsInBytesConn, PROT_READ | PROT_WRITE,
	                                   MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

	/* allocate memory for arrays of dipole interaction */
	if (userVars.flexOnOff == FLEX_OFF && userVars.dipOnOff == DIP_ON) {
		dipole_cacheSize = userVars.N1 * userVars.N2 * userVars.N3;
		createTwoDimensionalArray(d_dipole_R3coefHost, d_dipole_R3coef, dipole_cacheSize);
		createTwoDimensionalArray(d_dipole_RxR5coefHost, d_dipole_RxR5coef, dipole_cacheSize);
		createTwoDimensionalArray(d_dipole_RyR5coefHost, d_dipole_RyR5coef, dipole_cacheSize);
		createTwoDimensionalArray(d_dipole_RzR5coefHost, d_dipole_RzR5coef, dipole_cacheSize);
	}

	/* to debug code */
	//dim3 cores(1, 1);
	//dim3 threads(1, 1);
	//printArray<<<cores,threads>>>(d_lattice);

}

/* function to destroy arrays for simulation */
extern "C"
void destroySimulationArraysCUDA(void) {

	/* sizes of arrays */
	unsigned long sizeOfArrsInBytes = NUMBER_COMP * sizeof(double) * (unsigned long)(userVars.N1 * userVars.N2 * userVars.N3);
	unsigned long sizeOfArrsInBytesInt = NUMBER_COMP * sizeof(int) * (unsigned long)(userVars.N1 * userVars.N2 * userVars.N3);
	unsigned long sizeOfArrsInBytesConn = MAX_CONN_NUM * sizeof(int) * (unsigned long)(connElements);

	if (userVars.integrationMethod == INTEGRATION_METHOD_RKF) {
		/* clear array of "small changes" */
		cudaFree(d_dm5);
		/* clear arrays of intermediate values for RKF45 (main memory and GPU) */
		for (int i = 0; i < 6; i++) {
			cudaFree(d_kHost[i]);
		}
		cudaFree(d_k);
	} else if (userVars.integrationMethod == INTEGRATION_METHOD_RKF) {
		/* clear array of "small" changes */
		cudaFree(d_dmHalf);
		cudaFree(d_dmFinal);
		/* clear array of magnetic moments in middle point */
		cudaFree(d_latticeHalf);
	}

	/* clear array of effective field */
	cudaFree(d_H);

	/* clear array of force */
	cudaFree(d_F);

	/* clear array of energies (all kinds) for sites (GPU) */
	cudaFree(d_EnergyExchangeArray);
	cudaFree(d_EnergyAnisotropyArrayAxisK1);
	cudaFree(d_EnergyAnisotropyArrayAxisK2);
	cudaFree(d_EnergyAnisotropyArrayAxisK3);
	cudaFree(d_EnergyDipoleArray);
	cudaFree(d_EnergyExternalArray);
	/* it is only for cubic system */
	if (userVars.latticeType == LATTICE_CUBIC) {
		cudaFree(d_EnergyDMIAxis1Array);
		cudaFree(d_EnergyDMIAxis2Array);
		cudaFree(d_EnergyDMIAxis3Array);
	}
	/* it is only for system with triangular lattice */
	if (userVars.latticeType == LATTICE_TRIANGULAR) {
		cudaFree(d_EnergyDMITriangleInterfaceArray);
		cudaFree(d_EnergyDMITriangleBulkArray);
		cudaFree(d_EnergyStretchingArray);
		cudaFree(d_EnergyBendingArray);
	}

	/* clear array of energies (all kinds) for sites (main memory) */
	free(EnergyExchangeArray);
	free(EnergyAnisotropyArrayAxisK1);
	free(EnergyAnisotropyArrayAxisK2);
	free(EnergyAnisotropyArrayAxisK3);
	free(EnergyDipoleArray);
	free(EnergyExternalArray);
	/* it is only for cubic system */
	if (userVars.latticeType == LATTICE_CUBIC) {
		free(EnergyDMIAxis1Array);
		free(EnergyDMIAxis2Array);
		free(EnergyDMIAxis3Array);
	}
	/* it is only for system with triangular lattice */
	if (userVars.latticeType == LATTICE_TRIANGULAR) {
		free(EnergyDMITriangleInterfaceArray);
		free(EnergyDMITriangleBulkArray);
		free(EnergyStretchingArray);
		free(EnergyBendingArray);
	}

	/* clear arrays for changes of magnetic moments and coordinates */
	free(DmDt);
	free(DrDt);

	/* clear array for intermediate magnetic moments, coordinate and velocities of RKF45 */
	cudaFree(d_mCh);

	/* clear arrays "lattice", "latticeParam" and "Bamp" in GPU */
	cudaFree(d_lattice);
	cudaFree(d_latticeParam);
	cudaFree(d_Bamp);

	/* clear additional vectors for VTK XML BASE64 format */
	munmap(addDoubleVectorXML, sizeOfArrsInBytes);
	munmap(addIntVectorXML, sizeOfArrsInBytesInt);
	munmap(addIntVectorConnXML, sizeOfArrsInBytesConn);

	/* clear memory for arrays of dipole interaction */
	if (userVars.flexOnOff == FLEX_OFF && userVars.dipOnOff == DIP_ON) {
		clearTwoDimensionalArray(d_dipole_R3coefHost, d_dipole_R3coef, dipole_cacheSize);
		clearTwoDimensionalArray(d_dipole_RxR5coefHost, d_dipole_RxR5coef, dipole_cacheSize);
		clearTwoDimensionalArray(d_dipole_RyR5coefHost, d_dipole_RyR5coef, dipole_cacheSize);
		clearTwoDimensionalArray(d_dipole_RzR5coefHost, d_dipole_RzR5coef, dipole_cacheSize);
	}

}

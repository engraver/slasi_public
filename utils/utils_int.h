#ifndef _UTILS_INT_H_
#define _UTILS_INT_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif
#include "../alltypes.h"

/**
 * @brief function to create log files for magnetic simulator
 * @param inputName is base string for names of log files
 * */
int createLogFiles(char *inputName);

#endif
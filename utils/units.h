#ifndef _UNITS_H_
#define _UNITS_H_

#define ENERGY_SI   "<pJ>"
#define TIME_SI     "<ps>"
#define DISTANCE_SI "<nm>"
#define CIRCFREQ_SI "<Trad^{-1}>"

#define ENERGY_N    "<1>"
#define TIME_N      "<1>"
#define DISTANCE_N  "<1>"
#define CIRCFREQ_N  "<1>"

/** Planck constant, pJ*ps */
#define HBAR_SI 1.054571800e-10
#define HBAR_N 1.0
#define HBAR_USI "<pJ*ps>"
#define HBAR_UN  "<1>"

/** Vacuum permeability, N/A^2 */
#define MU0_SI  1.2566370614359173e-06
#define MU0_N   1.0
#define MU0_USI "<N/A^2>"
#define MU0_UN  "<1>"

/** Bohr magneton, pJ/T */
#define MUB_SI 927.400968e-14
#define MUB_N 1.0
#define MUB_USI "<pJ/T>"
#define MUB_UN  "<1>"

#endif

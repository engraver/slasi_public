#ifndef PARALLEL_UTILS_MEMORY_CUDA
#define PARALLEL_UTILS_MEMORY_CUDA

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

/**
 * @brief Create arrays for simulation in GPU (CUDA)
 */
void createSimulationArraysCUDA(void);

/**
 * @brief Create arrays for simulation in GPU (CUDA)
 */
void destroySimulationArraysCUDA(void);

#endif

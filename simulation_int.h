#ifndef SIMULATION_INT_H
#define SIMULATION_INT_H

/**
 * @brief Analyzes command line options and set ups internal environment of simulator
 * @param argc number of command line arguments (from `main()`)
 * @param argv list of command line arguments (from `main()`)
 * @return `0` for success and `1` for error
 */
int processCMDoptions(int argc, char **argv);

/**
 * @brief Prepare initial state of the system (normalization of vectors, local basis calculation, initial energy etc)
 * @return `0` for success and `1` for error
 */
int initialState(void);


/**
 * @brief Starts the given simulation job
 * @return `0` for success and `1` for error
 */
int runSimulation(void);
/**
 * @brief Starts writing log file with a general information.
 * @param inputFile Configuration file with `*.cfg` extension
 */
void initLog(char *inputFile);

#endif // SIMULATION_INT_H

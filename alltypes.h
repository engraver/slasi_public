/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

/**
 * @file alltypes.h
 * @author Oleksandr Pylypovskyi
 * @author Artem Tomilo
 * @brief All types and variables which should be accessible from everywhere
 */

#ifndef _ALL_TYPES_H_
#define _ALL_TYPES_H_

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327
#endif

#include "parallel.h"

#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include <math.h>
#include <Python.h>
#include "utils/indices.h"

#ifdef __INTEL_COMPILER
#define SLS_NAN NAN
#else
#define SLS_NAN NAN
#endif

#define DEFAULT_STR_LEN 1000 /**< default string length used */
#define MAX_HEADERS_NUM 100 /**< maximum number of columns in paramFile */
#define _USE_MATH_DEFINES /**< connecting mathematical library for number pi */
#define NEAR_ZERO 0.000000000000000001 /**< number that is close to zero for some comparisons */
#define LENG_ZERO 0.00001 /**< number that is close to zero to compare with length of vectors */
#define MAX_STR_LEN 1001 /**< maximum length for the input line */
#define VERSION "1.0" /**< SLaSi version */
#define ZERO_SHIFT 0 /**< macros for zero drift during working with array */
#define RKFARRAYS_NUM 6 /**< number of arrays for intermediate values in RKF45 */

#define H_MAX 0.1 /**< maximal integration step for simulation (in internal time units) */
#define H_MIN 0.00001 /**< minimal integration step for simulation (in internal time units) */
#define H_MIN 0.00001 /**< minimal integration step for simulation (in internal time units) */
#define NUMBER_COMP 9 /**< number of components in array "lattice "*/
#define NUMBER_COMP_MAGN 3 /**< number of magnetic moments (only) in array "lattice" */
#define NUMBER_QUANTITIES 3 /**< number of different physical quantities in array "lattice" */
#define NUMBER_COMP_FIELD 3 /**< number of components (external field) in array "Bamp" */
#define MAX_CONN_NUM 8 /**< maximal number of one geometric element in system */

#define RANK_MASTER 0 /**< id of master process (for MPI) */
#define THREADS_CORE 512 /**< number of threads for one core (GPU) */

#define FILENAME_PID "process.pid" /**< name of file to write pid of SLaSi */

/** constants to write UCD output files */

/** types of cell */
#define CELL_LIN 1
#define CELL_TRI 2
#define CELL_QUA 3
#define CELL_TET 4
#define CELL_PYR 5
#define CELL_PRI 6
#define CELL_HEX 7

/** size of UCD text row */
#define UCD_STRING_SIZE 1024

/** Status of calculation of dipole-dipole interaction.
 * Is governed by `dipOnOff` variable
 */
typedef enum _diponoff {
      DIP_ON = 1 /**< enabled, value `ON` */
    , DIP_ON_NOCACHE /**< disable cache for multipliers (for large scale tasks) */
    , DIP_OFF     /**< disabled, value `OFF`, default */
} DipOnOff;

/** Status of calculation of exchange interaction.
 * Is governed by `exchOnOff` variable
 */
typedef enum _exchonoff {
	  EXCH_ON = 1    /**< enabled, value `ON`, default  */
	, EXCH_OFF     /**< disabled, value `OFF` */
} ExchOnOff;

/** Status of calculation of DMI (Dzyaloshinskii–Moriya interaction).
 * Is governed by `dMIOnOff` variable
 */
typedef enum _dmionoff {
	  DMI_ON = 1 /**< enabled, value `ON` */
	, DMI_OFF     /**< disabled, value `OFF`, default */
} DMIOnOff;

/** Status of calculation of interactions and
 * additional differential equation for flexible system.
 * Is governed by `flexOnOff` variable
 */
typedef enum _flexonoff {
	FLEX_ON = 1, /**< enabled, value `ON` */
	FLEX_OFF     /**< disabled, value `OFF`, default */
} FlexOnOff;

/** Status for saving files with magnetization distribution in text format
 * Is governed by `saveSLS` variable
 */
typedef enum _savesls {
	SLSSAVE_ON = 1, /**< enabled, value `ON`, default */
	SLSSAVE_OFF     /**< disabled, value `OFF` */
} SaveSLS;

/** Status for saving files with magnetization distribution in binary format
 * Is governed by `SaveBin` variable
 */
typedef enum _saveBin {
	BINSAVE_ON = 1, /**< enabled, value `ON`, default */
	BINSAVE_OFF     /**< disabled, value `OFF` */
} SaveBin;

/** Status for normalizing of magnetic moments to the unit length in an integrator.
 * Is governed by `normalOnOff` variable
 */
typedef enum _normalonoff {
	NORMAL_ON = 1, /**< enabled, value `ON`, default */
	NORMAL_OFF     /**< disabled, value `OFF` */
} NormalOnOff;

/** How initial magnetization distribution is given.
 * Is governed by `magnInit` variable
 */
typedef enum _magnInit {
	MI_FILE = 0, /**< by paramFile, value `file` default */
	MI_RANDOM,   /**< random, value `random` */
	MI_UNIFORM,  /**< uniform, value `uniform` requires `mx0`, `my0`, `mz0` variables */
    MI_SCRIPT,    /**< by python script, value `script` requires python file with
				 * `getMagn` function which takes three real numbers (coordinates) and
				 * three integers (indeces) and gives three real numbers (components of the magnetic moment) */
    MI_SLSB      /**< Reading from slsb slasi output. `magnFile = proj.12345.slsb` should be provided.  */
} MagnInit;

/** How exchange integral distribution is given.
 * Is governed by `exchInit` variable
 */
typedef enum _exchInit {
	EI_UNIFORM = 0,  /**< uniform, value `uniform` requires `J` variable */
	EI_FILE,         /**< by paramFile, value `file` default */
	EI_SCRIPT,       /**< by python script, value `script` requires python file with
				       * `getExchange` function which takes three real numbers (coordinates) and
				       * three integers (indeces) and gives six real numbers (exchange integral for six neighbours) */
} ExchInit;

/** Fixed integration step for testing purposes.
 * Is governed by `fixedStep` variable */
typedef enum _fixedStep {
	FIXEDSTEP_ON = 1, /**< enabled, value `ON` */
	FIXEDSTEP_OFF     /**< disabled, value `OFF`, default */
} FixedStep;

/** Integration method.
 * Is governed by `integrationMethod` variable
 */
typedef enum _integrationMethod {
	INTEGRATION_METHOD_RKF = 1, /**< value `RKF`, default */
	INTEGRATION_METHOD_MIDDLE_POINT
} IntegrationMethod;

/** Method for minimization (nlopt)
* Is governed by `minMethod` variable
*/
typedef enum _minMethod {
	TNEWTON_RESTART = 1, /**< Newton's method with restarting, value NewtonRestart, default */
	TNEWTON_PRECOND_RESTART, /**< modified Newton's method with restarting, value NewtonPreRestart */
	TNEWTON, /**< Newton's method, value Newton */
	LBFGS /**< Limited-memory Broyden–Fletcher–Goldfarb–Shanno algorithm, value LBFGS */
} MinMethod;

/** Saves energy per lattice site during simulation.
 * Is governed by `writingEnergy` variable.
 */
typedef enum _writingEnergy {
	WRITINGENERGY_ON = 1, /**< enabled, value `ON` */
	WRITINGENERGY_OFF     /**< disabled, value `OFF`, default */
} WritingEnergy;

/** Writes discrete local reference frame into each snapshot.
 * Is governed by `writingBasis` variable.
 */
typedef enum _writingBasis {
	WRITINGBASIS_ON = 1, /**< enabled, value `ON` */
	WRITINGBASIS_OFF     /**< disabled, value `OFF`, default */
} WritingBasis;

/** Saves amplitude of external magnetic field per lattice site during simulation.
 * Is governed by `writingExternalField` variable.
 */
typedef enum _writingExtField {
	WRITINGEXTFIELD_ON = 1, /**< enabled, value `ON` */
	WRITINGEXTFIELD_OFF     /**< disabled, value `OFF`, default */
} WritingExtField;

/** Gives type of external magnetic field.
 * Is governed by `extField` variable.
 */
typedef enum _extField {
	EXTFIELD_CONST,     /**< Constant, value `const` */
	EXTFIELD_HARMTIME,  /**< Harmonic like sin(omega*t), value `harmTime` */
	EXTFIELD_SCRIPT,    /**< Python script, value `script` */
	EXTFIELD_FILE,      /**< Param file, value `file` */
	EXTFIELD_SINC		/**< Uniform magnetic field with sinc amplitude in time, value `sinct` */
} ExtField;

/** Gives type of external force for flexible system.
 * Is governed by `extField` variable.
 */
typedef enum _extForce {
	EXTFORCE_CONST,     /**< Constant, value `const` */
	EXTFORCE_HARMTIME,  /**< Harmonic like sin(omega*t), value `harmTime` */
	EXTFORCE_SCRIPT,    /**< Python script, value `script` */
} ExtForce;

/** Gives type of exchange interaction.
 * Is governed by `exchType` variable.
 */
typedef enum _exchType {
	EXCH_UNIFORM, /**< uniform exchange interaction */
	EXCH_INHOMOGENEOUS /**< inhomogeneous exchange interaction */
} ExchType;

/** Gives type of Dzyaloshinsky-Moria interaction.
 * Is governed by `DMItype` variable.
 */
typedef enum _DMItype {
	DMI_CONST,     /**< Constant, value `const` */
	DMI_SCRIPT,    /**< Python script, value `script` */
	DMI_FILE       /**< Param file, value 'file' */
} DMIType;

/** Gives type of anisotropy.
 * Is governed by `anisotropyType` variable.
 */
typedef enum _AnisotropyType {
	ANIS_CONST,     /**< Constant, value `const` */
	ANIS_SCRIPT,    /**< Python script, value `script` */
	ANIS_FILE       /**< Param file, value 'file' */
} AnisotropyType;

/** Gives type of launch of this program.
 * Is governed by `launch` variable.
 */
typedef enum _launch {
	LAUNCH_SIMULATION = 0, /**< Simulation of magnetic system using LLG equation, value "simulation" */
	LAUNCH_MINIMIZATION, /**< Search for minimum energy using NLopt, value "optimization" */
	LAUNCH_HYSTERESIS /**< Hysteresis modeling (search of the minimum energy from different external fields), value "hysteresis" */
} Launch;

/** Gives type of magnetization in this program.
 * Is governed by `typeMagn` variable.
 */
typedef enum _typeMagn {
	MAGN_ALL = 0, 	/**< all magnetic sites have magnetization */
	MAGN_FILE 		/**< magnetization of magnetic sites is given from paramFile */
} TypeMagn;

/** Gives type of parameters for flexible system in this program.
 * Is governed by `typeFlexParam` variable.
 */
typedef enum _typeFlexParam {
	FLEXPARAM_CONST = 0, /**< constant, value `const`  */
	FLEXPARAM_FILE /**< param file, value 'file'  */
} TypeFlexParam;

/** Gives type of mechanical parameters in this program.
 * Is governed by `typeMechParam` variable.
 */
typedef enum _typeMechParam {
	MECHPARAM_CONST = 0, /**< constant, value `const`  */
	MECHPARAM_FILE /**< param file, value 'file'  */
} TypeMechParam;

/** Gives periodic boundary conditions.
 * Is governed by `periodicBC1`, `periodicBC2`, `periodicBC3` variables.
 */
typedef enum _periodicBC {
	PBC_OFF, /**< Default, no periodic BCs */
	PBC_ON /**< Periodic BC for the given axis */
} PeriodicBC;

/** Type of lattice during simulation.
 * Is governed by `latticeType` variable
 */
typedef enum _latticeType {
	LATTICE_CUBIC, 		/**< Default, cubic lattice */
	LATTICE_TRIANGULAR  /**< Triangular lattice */
} LatticeType;

/** Gives units for values in simulation.
 * Is governed by `units` variable in cfg file.
 */
typedef enum _units {
	UNIT_NORM, /**< Reduced units system, `muB = hbar = 1`, value `norm` */
	UNIT_SI    /**< SI units, value `SI`, default */
} Units;

/** Gives indicator to launch restarting
 *  or it is not necessary
 */
typedef enum _restartOnOff {
	RESTART_ON, /**< launch restarting */
	RESTART_OFF /**< restarting is not necessary */
} RestartOnOff;

/** Gives indicator to initialize Python environment
 */
typedef enum _initPython {
	INIT_PYTHON_ON, /**< launch initialization */
	INIT_PYTHON_OFF /**< don't Python initialization */
} InitPython;

/** Gives indicator of file format to write backup files
 */
typedef enum _outputFileType {
	VTK_XMLBASE64, /**< VTK XML base64 format */
	VTK_BINARY,    /**< binary VTK format */
	UCD_ASCII      /**< ASCII UCD format */
} OutputFileType;

/** Identifiers for fields in binary output
 */
typedef enum _bin_fields {
	BIN_MAGN = 1, /**< Group of mx0, my0, mz0, mx1, my1, mz1, ... */
	BIN_COORDS = 2, /**< Group of coords x0, y0, z0, x1, y1, z1, ... */
	BIN_TNB = 3, /**< Group of vectors for TNB-basis t0x, t0y, t0z, u0x, u0y, u0z, b0x, b0y, b0z,
							t1x, t1y, t1z, u1x, u1y, u1z, b1x, b1y, b1z, ... */
	BIN_SPIN = 4, /**< Group of total energy of one spin Espin0, Espin2, ... */
	BIN_EXCHANGE = 5, /**< Group of exchange energy Eexch0, Eexch1, ... */
	BIN_ANISOTROPY = 6, /**< Group energy of anisotropy EaniK1(0), EaniK2(0), EaniK3(0),
											EaniK1(1), EaniK2(1), EaniK3(1), ... */
	BIN_DIP = 7, /**< Group energy of dipole interaction Edip0, Edip1, ... */
	BIN_EXT = 8, /**< Group energy of external field Eext0, Eext1, ... */
	BIN_DMI = 9, /**< Group energy of DMI EDMIK1(0), EDMIK2(0), EDMIK3(0),
											EDMIK1(1), EDMIK2(1), EDMIK3(1), ... */
    BIN_STR = 10, /**< Group energy of stretching interaction Estr0, Estr1, Estr2 ... */
    BIN_BEND = 11, /**< Group energy of bending interaction Ebend0, Ebend1, Ebend2 ... */
    BIN_EXTFIELD = 12 /**< Group amplitudes of magnetic field Bx0, By0, Bz0, ... */
} BinFields;

/**
 * All user-defined parameters and dimensional parameters
 */
typedef struct _uservars {

	/** Integrator parameters */

	/** Fixed integration step during calculations */
	FixedStep fixedStep;
	bool bfixedStep; /**< "true" if set */
	/** Integration method (RKF45 etc) */
	IntegrationMethod integrationMethod;
	/** Maximal rotation angle for magnetization during integration */
	double critAngle;
	bool bcritAngle; /**< "true" if variable critAngle is set */
	/** Stopping criteria by torque dynamics, |dm/dt| */
	double stopDmDt;
	bool bstopDmDt; /**< "true" if variable stopDmDt is set */
	/** Initial integration step */
	double integrationStep;
	bool bintegrationStep; /**< "true" if variable integrationStep is set */
	/** initial normalized integration step */
	double integrationStepNorm;
	/** Integrator precision */
	double precision;
	bool bprecision; /**< "true" if variable precision is set */
	/** Starting time */
	double t0;
	bool bt0; /**< "true" if variable t0 is set */
	/** Final, maximal time */
	double tfin;
	bool btfin; /**< "true" if variable tfin is set */
	/** Frame for snapshot saving */
	double frame;
	bool bframe; /**< "true" if variable frame is set */
	/** normalized time for integration */
	double t0Norm; 	/**< Starting time */
	double tfinNorm; /**< Final, maximal time */
	double frameNorm; 	/**< Frame for snapshot saving */
	
	/** defines how much more often log file should be updated than snapshots saved */
	int snapshotPeriodMultiplier;
	bool bsnapshotPeriodMultiplier;

	/** Numbers for normalization */

	double NormCoefficientTime; /** normalizing factor ( J*S*S*gyro/(1.0+damping*damping) ) */
	double NormCoefficientEnergy; /** normalizing factor J*S*S */
	double NormDipole; /** normalizing factor muB*muB*g*g/J */
	double NormExternal; /** normalizing factor muB*g/(J*S) */
	double NormDMI; /** normalizing factor 1/J */

	/** Parameters for flexible system */

	FlexOnOff flexOnOff; /**< toggle to enable or disable flexible system */
	double beta; /**<  modulus of the bending rigidity */
	bool bbeta; /**< "true" if variable beta is set */
	int ibeta; /**< number of column of beta in paramFile  */
	double lambda; /**< stretching rigidity constant */
	bool blambda; /**< "true" if variable lambda is set */
	int ilambda; /**< number of column of lambda in paramFile */
	double lambdaTri; /**< constant of stretching for triangular lattice */
	bool blambdaTri; /**< "true" if variable lambdaTri is set */
	/** index of stretching rigidity constants for triangular lattice in parameter file */
	int ilambda1, ilambda2, ilambda3, ilambda4, ilambda5, ilambda6;
	double betaTri; /**< constant of bending for triangular lattice */
	bool bbetaTri; /**< "true" if variable betaTri is set */
	TypeFlexParam typeFlexParam; /**< type of setting parameters for flexible system */
	LatticeType latticeType; /**< type of lattice for simulation */

	/** Variables for Dzyaloshinsky-Moria interaction */

	DMIOnOff dmiOnOff; /**< status of Dzyaloshinsky-Moria interaction */
	bool bdmiOnOff; /**< "true" if variable dmiOnOff is set */
	/* the first axis */
	double D11, D12, D13;
	bool bD11, bD12, bD13; /**< "true" if variables D11, D12, D12 are set */
 	int iD11, iD12, iD13; /**< number of column of the first axis in paramFile */
	/* the second axis */
	double D21, D22, D23;
	bool bD21, bD22, bD23; /**< "true" if variables D21, D22, D23 are set */
	int iD21, iD22, iD23; /**< number of column of the second axis in paramFile */
	/* the third axis */
	double D31, D32, D33;
	bool bD31, bD32, bD33; /**< "true" if variables D31, D32, D33 are set */
	int iD31, iD32, iD33; /**< number of column of the third axis in paramFile */
	double dmiBtri; /**< coefficient of bulk Dzyaloshinsky-Moria interaction for triangular lattice (general) */
	bool bdmiBtri; /**< "true" if variable bdmiBtri is set */
    /** number of column in paramFile to set coefficient of bulk Dzyaloshinsky-Moria interaction
     *  for triangular lattice (every neighbor) */
	int idmiB1, idmiB2, idmiB3, idmiB4, idmiB5, idmiB6;
	double dmiNtri; /**< coefficient of interface Dzyaloshinsky-Moria interaction for triangular lattice (general) */
	bool bdmiNtri; /**< "true" if variable bdmiNtri is set */
	/** number of column in paramFile to set coefficient of interface Dzyaloshinsky-Moria interaction
 *  for triangular lattice (every neighbor) */
	int idmiN1, idmiN2, idmiN3, idmiN4, idmiN5, idmiN6;
	double dmiBulk; /**< coefficient of bulk Dzyaloshinsky-Moria interaction for cubic lattice (general) */
	bool bdmiBulk; /**< "true" if variable bdmiBulk is set */
	/* type of Dzyaloshinsky-Moria interaction */
	DMIType DMItype;

    /* Geometrical parameters */

    /**
     * @brief is the number of lattice sites along the first axis @f$\hat{\vec{e}}_1@f$ (curvilinear in general case).
     * Should be not less than 1, i.e. if you need a one-dimensional system, it should be along @f$\hat{\vec{e}}_1@f$.
     *
     *@code N1 = 25 @endcode
     */
    int N1;

    /**
     * @brief is the number of lattice sites along the second axis @f$\hat{\vec{e}}_2@f$ (curvilinear in general case).
     *
     *@code N2 = 25 @endcode
     */
    int N2;

    /**
     * @brief is the number of lattice sites along the second axis @f$\hat{\vec{e}}_3@f$ (curvilinear in general case).
     *
     *@code N3 = 25 @endcode
     */
    int N3;
	bool bN1, bN2, bN3; /**< "true" if relevant variables are setted up */

    /**
     * @brief is the latice constant in nm.
     *
     * @code a = 0.3 @endcode
     */
	double a;
	bool ba; /**< "true" if variable a is set */
	int ix, iy, iz; /**< column in paramsFile  */

    /**
     * @brief set/unset periodic boundary conditions along the @f$\hat{\vec{e}}_1@f$ axis.
     * ``OFF`` by default.
     *
     * @code
     * periodicBC1 = ON
     * periodicBC1 = OFF
     * @endcode
     */
    PeriodicBC periodicBC1;

    /**
     * @brief set/unset periodic boundary conditions along the @f$\hat{\vec{e}}_2@f$ axis.
     * ``OFF`` by default.
     *
     * @code
     * periodicBC2 = ON
     * periodicBC2 = OFF
     * @endcode
     */
    PeriodicBC periodicBC2;

    /**
     * @brief set/unset periodic boundary conditions along the @f$\hat{\vec{e}}_3@f$ axis.
     * ``OFF`` by default.
     *
     * @code
     * periodicBC3 = ON
     * periodicBC3 = OFF
     * @endcode
     */
    PeriodicBC periodicBC3;
	double vx, vy, vz; /**< components of velocity vector */
	int ivx, ivy, ivz; /**< indexes of variables vx, vy, vz */
	bool bvx, bvy, bvz; /**< "true" if variables vx, vy, vz are set */
	int dimensionOfSystem; 	/**< dimension of system of magnetic moments */
	double nrmX, nrmY, nrmZ; /**< normal vectors for square lattice */
	int inrmX, inrmY, inrmZ; /**< indexes of normals for square lattice */
	bool bnrmX, bnrmY, bnrmZ; /**< "true" if variables nrmX, nrmY, nrmZ are set */

	/** Parameters for optimization */

	/* indicator how magnetic simulator will be launched */
	Launch launch;
	/* fixed huge value for optimization */
	double lagrangeDistance;
	bool blagrangeDistance; /**< "true" if variable lagrangeDistance is set */
	/* period to update optimization */
	int updatePeriod;
	bool bupdatePeriod; /**< "true" if variable updatePeriod is set */
	/* value to stop optimization */
	double stopValue;
	bool bstopValue; /**< "true" if variable stopValue is set */
	/* indicator which method will be used for optimization */
	MinMethod minMethod;
	double lengthResidue; /**< max residue for length of unit magnetic moment to save energy/snapshot */
	bool blengthResidue; /**< "true" if variable lengthResidue is set */

	/** Mechanical parameters */

	/** mass of the atom */
	double mass;
	bool bmass; /**< "true" if variable mass is set */
	int imass; /**< column in paramsFile */
	/** mechanical damping */
	double mechDamping;
	bool bmechDamping; /**< "true" if variable mechDamping is set */
	int imechDamping; /**< column in paramsFile */
	/** type of mechanical parameters */
	TypeMechParam typeMechParam;

	/** Parameters for external force */

	/* type of external force */
	ExtForce extForce;
	/* vector of external force */
	double Fx, Fy, Fz;
	bool bFx, bFy, bFz; /**< "true" if variables Fx, Fy, Fz are set */
	int iFx, iFy, iFz; /**< column in paramsFile */
	/* frequency characteristics of external force (frequency and phase) */
	double Ffreq, Fphase;
	bool bFfreq, bFphase; /**< "true" if variables Ffreq, Fphase are set */
	int iFfreq, iFphase; /**< column in paramsFile */

	/** Magnetic parameters */

	int imx, imy, imz; /**< column in paramsFile  */
	/**<Exchange integral (if the same for all pairs of sites) */
	double J;
	ExchOnOff exchOnOff; /**< indicator to turn off exchange interaction */
	bool bexchOnOff; /**< "true" if variable exchOnOff is set */
	ExchType exchType; /**< type of exchange interaction */
	ExchInit exchInit; /**< type of exchange initialization */
	bool bJ; /**< "true" if variable J is set */
	int iJm1; /**< column in paramsFile (cubic lattice) */
	int iJp1; /**< column in paramsFile (cubic lattice) */
	int iJm2; /**< column in paramsFile (cubic lattice) */
	int iJp2; /**< column in paramsFile (cubic lattice) */
	int iJm3; /**< column in paramsFile (cubic lattice) */
	int iJp3; /**< column in paramsFile (cubic lattice) */
	int iJ1; /**< column in paramsFile (triangular lattice) */
	int iJ2; /**< column in paramsFile (triangular lattice) */
	int iJ3; /**< column in paramsFile (triangular lattice) */
	int iJ4; /**< column in paramsFile (triangular lattice) */
	int iJ5; /**< column in paramsFile (triangular lattice) */
	int iJ6; /**< column in paramsFile (triangular lattice) */
	/** Lande factor */
	double g;
	bool bg; /**< "true" if variable g is set */
	/** Bohr magneton */
	double muB;
	bool bmuB; /**< "true" if variable muB is set */
	/** Planck constant */
	double hbar;
	bool bhbar; /**< "true" if variable hbar is set */
	/** Vacuum permeability */
	double mu0;
	bool bmu0; /**< "true" if variable mu0 is set */
	/** Spin length */
	double S;
	bool bS; /**< "true" if variable S is set */
	/** Gilbert damping constant alpha */
	double gilbertDamping;
	bool bgilbertDamping; /**< "true" if variable gilbertDamping is set */
	int igilbertDamping; /**< column in paramsFile  */
	/** Single-ion anisotropy */
	double aniK1, aniK2, aniK3;
	bool baniK1, baniK2, baniK3; /**< "true" if anisotropies are set */
	int ianiK1, ianiK2, ianiK3; /**< column in paramsFile  */
	/* the first axis */
	double e11, e12, e13;
	bool be11, be12, be13; /**< "true" if variables e11, e12, e13 are set */
	int ie11, ie12, ie13; /**< column in paramsFile  */
	/* the second axis */
	double e21, e22, e23;
	bool be21, be22, be23; /**< "true" if variables e21, e22, e23 are set */
	int ie21, ie22, ie23; /**< column in paramsFile  */
	/* the third axis */
	double e31, e32, e33;
	bool be31, be32, be33; /**< "true" if variables e31, e32, e33 are set */
	int ie31, ie32, ie33; /**< column in paramsFile  */
	/* type of anisotropy */
	AnisotropyType anisotropyType;
	/** Inter-ion anisotropy */
	double aniInterK1, aniInterK2, aniInterK3;
	bool baniInterK1, baniInterK2, baniInterK3; /**< "true" if anisotropies are set */
	int ianiInterK1m1, ianiInterK1p1, ianiInterK1m2, ianiInterK1p2, ianiInterK1m3, ianiInterK1p3; /**< column in paramsFile  */
	int ianiInterK2m1, ianiInterK2p1, ianiInterK2m2, ianiInterK2p2, ianiInterK2m3, ianiInterK2p3; /**< column in paramsFile  */
	int ianiInterK3m1, ianiInterK3p1, ianiInterK3m2, ianiInterK3p2, ianiInterK3m3, ianiInterK3p3; /**< column in paramsFile  */
	/** Enable or disable dipolar interaction */
	DipOnOff dipOnOff;
	bool bdipOnOff; /**< "true" if dipOnOff is set */
	size_t nonzeroSpinsNum; /** number of nonzero spins in the lattice */

	/** Magnetization */

	int iMagn; /**< column in paramsFile for magnetization */
	TypeMagn typeMagn; /**< type of magnetization */
	/** Type of external magnetic field */
	ExtField extField;
	/** Field amplitude */
	double Bxamp, Byamp, Bzamp;
	int iBxamp, iByamp, iBzamp; /**< column in paramsFile  */
	bool bBxamp, bByamp, bBzamp; /**< "true" if variables Bxamp, Byamp, Bzamp are set */
	double Bx, By, Bz; /**< current magnetic field (uniform case) */
	WritingExtField writingExtField;
	/* refresh rate for hysteresis */
	int refreshRate;
	bool brefreshRate; /**< "true" if variable refreshRate is set */
	/** Harmonic field */
	double Bfreq; /**< Field frequency */
	bool bBfreq; /**< "true" if variable is set */
	int iBfreq; /**< column in paramsFile  */
	double Bphase; /**< Field phase */
	bool bBphase; /**< "true" if variable is set */
	int iBphase; /**< column in paramsFile  */
	/** Sinc field in time */
	double fieldSincTWidth; /**< characteristic width of sinc */
	double fieldSincTShift; /**< time shift for sinc */
	bool bfieldSincTWidth;  /**< "true" if variable is set */
	int ifieldSincTWidth; /**< column in paramsFile  */
	bool bfieldSincTShift;  /**< "true" if variable is set */
	int ifieldSincTShift; /**< column in paramsFile */

	/** Other parameters */

	/** Project name */
	char *projName;
	/** Enables/disables saving of snapshots in text format */
	SaveSLS saveSLS;
	/** Enables/disables saving of snapshots in binary format */
	SaveBin saveBin;
	/** Normalize magnetic moments to 1 */
	NormalOnOff normalOnOff;
	/** Non-SLSB snapshot count after which slsb will be saved for backup/restart purposes */
	int backupPeriod; bool bbackupPeriod;
	/** variable to initialize Python environment */
	InitPython initPython;
	/** path to description of geometrical and magnetic parameters (python script or usual file with columns) */
	char *paramFile;
	/** path to file for restarting of simulation (.slsb file of past launching) */
	char *restartFile;
	/** Path to python file for python scripting */
	char scriptFile[MAX_STR_LEN];
	/** Path to python file for magnetic initialization */
	char magnFile[MAX_STR_LEN];
	/** Path to write results of integration */
	char *pathToWrite;
	/** Format of output file */
	OutputFileType outputFileType;
	/** Initial distribution */
	MagnInit magnInit;
	/** indicator that determines whether writing energy for each magnetic spin */
	WritingEnergy writingEnergy;
	/** indicator that determines whether writing orthonormal basis for each magnetic spin  */
	WritingBasis writingBasis;
	/** indicator to launch restarting */
	RestartOnOff restartOnOff;
	/** initial magnetization direction for MI_UNIFORM */
	double mx0, my0, mz0;
	bool bmx0, bmy0, bmz0; /**< "true" if variables mx0, my0, mz0 are setted up */

	/** Units for simulation */

	/* Units */
	Units units;
	char u_energy[16];
	char u_time[16];
	char u_distance[16];
	char u_circfreq[16];
	char u_mub[16];
	char u_hbar[16];
	char u_mu0[16];

} UserVars;

/**
 * @brief Parameters of the each lattice site
 */
typedef struct _latticeSite {
	bool magnetic;
	/* geometrical parameters */
	int n1, n2, n3; /**< position in the lattice */
	double * x; /**< x coordinate */
	double * y; /**< y coordinate */
	double * z; /**< z coordinate */
	double * vx; /**< x-component of velocity */
	double * vy; /**< y-component of velocity */
	double * vz; /**< z-component of velocity */
	//double x, y, z; /**< coordinate */
	/* parameters for flexible system */
	double beta; /**<  modulus of the bending rigidity */
	double lambda; /**< stretching rigidity constant */
    double lambda1, lambda2, lambda3, lambda4, lambda5, lambda6; /**< stretching rigidity constants for triangular lattice */
	double betaU1, betaU2, betaU3, betaD1, betaD2, betaD3; /**< constants of bending for triangular lattice */
    /* mechanical parameters */
	double mass; /**< mass of the atom */
	double mechDamping; /**< mechanical damping */
	/* magnetic parameters */
	//double mx, my, mz; /**< Magnetic moment direction */
	double * mx; /**< x magnetic moment component */
	double * my; /**< y magnetic moment component */
	double * mz; /**< z magnetic moment component */
	double S; /**< Spin length */
	double gilbertDamping; /**< Gilbert damping constant alpha */
	double Jp1, Jm1, Jp2, Jm2, Jp3, Jm3; /**< Exchange constants for the nearest neighbours (cubic lattice) */
	double J1, J2, J3, J4, J5, J6; /**< Exchange constants for the nearest neighbours (triangular lattice) */
	double aniK1, aniK2, aniK3; /**< Single-ion anisotropy */
	double e11, e12, e13; /**< Direction of the first single-ion anisotropy */
	double e21, e22, e23; /**< Direction of the second single-ion anisotropy */
	double e31, e32, e33; /**< Direction of the third single-ion anisotropy */
	double tx, ty, tz; /**< the first unit vector according to Fren's formulas */
	double bx, by, bz; /**< the second unit vector according to Fren's formulas */
	double nx, ny, nz; /**< the third unit vector according to Fren's formulas */
	double nrmX, nrmY, nrmZ; /**< normal vectors for square lattice */
	double aniInterK1p1, aniInterK1m1, aniInterK1p2, aniInterK1m2, aniInterK1p3, aniInterK1m3;  /**< Inter-ion anisotropy */
	double aniInterK2p1, aniInterK2m1, aniInterK2p2, aniInterK2m2, aniInterK2p3, aniInterK2m3;  /**< Inter-ion anisotropy */
	double aniInterK3p1, aniInterK3m1, aniInterK3p2, aniInterK3m2, aniInterK3p3, aniInterK3m3;  /**< Inter-ion anisotropy */
	double trinxD, trinyD, trinzD; /**< the lower normal vector for triangular lattice (triangle) */
	double trinxU, trinyU, trinzU; /**< the upper normal vector for triangular lattice (triangle) */
	double trinx, triny, trinz; /**< the normal vector for triangular lattice (site) */
	/* coefficients of Dzyaloshinsky-Moria interaction */
	/* the first axis */
	double D11p, D12p, D13p;
	double D11m, D12m, D13m;
	/* the second axis */
	double D21p, D22p, D23p;
	double D21m, D22m, D23m;
	/* the third axis */
	double D31p, D32p, D33p;
	double D31m, D32m, D33m;
    /* coefficients of bulk Dzyaloshinsky-Moria interaction (triangular lattice) */
    double dmiB1, dmiB2, dmiB3, dmiB4, dmiB5, dmiB6;
	/* coefficients of bulk Dzyaloshinsky-Moria interaction (square lattice) */
	double dmiB1cub, dmiB2cub, dmiB3cub, dmiB4cub, dmiB5cub, dmiB6cub;
	/* coefficients of interface Dzyaloshinsky-Moria interaction (triangular lattice) */
	double dmiN1, dmiN2, dmiN3, dmiN4, dmiN5, dmiN6;
	/* external field */
	double Bfreq, Bphase;
	double fieldSincTWidth, fieldSincTShift;
	/* external force */
	double Fx, Fy, Fz, Ffreq, Fphase;
	/* saved amplitude of external force for calculation */
	double savedFx, savedFy, savedFz;
	/* energy of magnetic spin and subspecies of this energy */
	double EnergyOfSpin;
	double EnergyExchange;
	double EnergyAnisotropyAxisK1;
	double EnergyAnisotropyAxisK2;
	double EnergyAnisotropyAxisK3;
	double EnergyDipole;
	double EnergyExternal;
	double EnergyDMIAxis1;
	double EnergyDMIAxis2;
	double EnergyDMIAxis3;
	double EnergyStretching;
	double EnergyBending;
	/* normalized energy of magnetic spin for integration */
	double EnergyExchangeNorm;
	double EnergyAnisotropyNormAxisK1;
	double EnergyAnisotropyNormAxisK2;
	double EnergyAnisotropyNormAxisK3;
	double EnergyDipoleNorm;
	double EnergyExternalNorm;
	double EnergyDMIAxis1Norm;
	double EnergyDMIAxis2Norm;
	double EnergyDMIAxis3Norm;
	double EnergyStretchingNorm;
	double EnergyBendingNorm;
	/* neighbors */
	int neighborsNum;
	struct _latticeSite * neighbors;
} LatticeSiteDescr;

/**
 * @brief Characteristics of lattice site needed synchronization between threads
 */
typedef double LatticeSiteSync;

/**
 * @brief declaration of global variables for all project
 * */

/** energy of system of magnetic spins and subspecies of this energy */
extern double EnergyOfSystem;
extern double EnergyExchange;
extern double EnergyAnisotropyAxisK1;
extern double EnergyAnisotropyAxisK2;
extern double EnergyAnisotropyAxisK3;
extern double EnergyDipole;
extern double EnergyExternal;
extern double EnergyDMIAxis1;
extern double EnergyDMIAxis2;
extern double EnergyDMIAxis3;
extern double EnergyDMITriangleBulk;
extern double EnergyDMITriangleInterface;
extern double EnergyStretching;
extern double EnergyBending;

/** variables to test */
extern bool testDMI;
extern bool testAnisotropy;

/** number to debug */
extern long number;

/** normalized energy of uniform state for correction */
extern double EnergyCorrectionNorm;
/** normalized energy for integration and subspecies of this energy */
extern double EnergyOfSystemNorm;
extern double EnergyExchangeNorm;
extern double EnergyAnisotropyNormAxisK1;
extern double EnergyAnisotropyNormAxisK2;
extern double EnergyAnisotropyNormAxisK3;
extern double EnergyDipoleNorm;
extern double EnergyExternalNorm;
extern double EnergyDMIAxis1Norm;
extern double EnergyDMIAxis2Norm;
extern double EnergyDMIAxis3Norm;
extern double EnergyDMITriangleBulkNorm;
extern double EnergyDMITriangleInterfaceNorm;
extern double EnergyStretchingNorm;
extern double EnergyBendingNorm;

/** indicator if user entered variable for triangular lattice */
extern bool indVarTri;
/** error of step RKF45 */
extern double errorStep;
/** integration step */
extern double intStep;
/** maximum value of magnetic derivative during integration */
extern double MaxDmDt;
/** maximum value of velocity during integration */
extern double MaxDrDt;
/** general magnetic moments on each axis */
extern double mxTot, myTot, mzTot;
/** number of frames for optimization */
extern int numberFrames;

/** time of execution for current snapshot */
extern double timeSnap;

/** minimum and maximum integration step during magnetic simulation */
extern double integrationStepMin;
extern double integrationStepMax;
/** current frame number of file (.slsb) */
extern unsigned long long frameNumber;
/** number in suffix of ".log" files for simulations with the same names */
extern unsigned long long numberOfFile;
/** rank of process */
extern int rank;
/** number of processes */
extern int numberProc;

extern size_t spinsNum; /**< number of spins in the lattice */

/** Array with components of magnetic moments, coordinates, velocities. For lattice of
 * N sites it contains 6N elements: 3N for (mx, my, mz)_i, 3N for (x, y, z)_i,
 * 3N for (vx, vy, vz)_i, i = 1,...,N. Structure is the following:
 * mx0, my0, mz0, mx1, my1, mz1, mx2, my2, mz2, ..., x0, y0, z0, x1, y1, z1, ...,
 * vx0, vy0, vz0, vx1, vy1, vz1, ...
 */
extern LatticeSiteSync *lattice;

/** Array of structures with material parameters
 */
extern LatticeSiteDescr *latticeParam;

extern UserVars userVars; /**< Instance of UserVars */
extern FILE * flog; /**< Log of commented rows for input parameters */
extern FILE * flogEnergy; /**< Log of energy of magnetic system */
extern FILE * flogMag; /**< Log of magnetic moments for magnetic system */
extern FILE * flogOther; /**< Log for other parameters of magnetic system */
extern bool silentMode; /**< Silent mode, do not print anything to terminal except error messages */

/** array for lattice connectivity */
extern int ** connectivity;
/** actual number of lattice cells */
extern int connElements;
/** maximal number of lattice cells for the given space */
extern int maxConnElements;
/** Number of neighbouring sites in the given lattice type */
extern int connNeighbours;
/** indices of lattice sites */
extern int * nodeIndices;
/** number of magnetic sites */
extern int magnSitesNum;

/** general buffer to save temporary rows */
extern char generalstr[DEFAULT_STR_LEN];

/** Python objects to read variables using script */
extern PyObject *pModulePython, *pNamePython;
/** Python objects to call function for external field */
/** Function variables */
extern PyObject *pFieldArgs, *pFieldFunc;
/** Usual variables */
extern PyObject *pValueTime, *pValueCoor1, *pValueCoor2, *pValueCoor3, *pValueInd1, *pValueInd2, *pValueInd3, *pValueRes;

/** "system" arrays to make simulation (RKF method) for MPI and usual versions (main memory) */
/* 6 arrays for intermediate values of RKF45 */
extern double *m_k[6];
/* arrays for small changes of fourth and fifth order */
extern double *m_dm5, *m_dm4;

/** "system" arrays to make simulation (middle point method) for MPI and usual versions (main memory) */
/* values of intermediate and final small changes */
extern double *m_dmHalf, *m_dmFinal;
/* values of magnetic moments in middle point */
extern double *latticeHalf;

/** array to save amplitude (usual and saved for 'difficult' case) of external magnetic field (Bx, By, Bz!) for every site */
extern double * Bamp, * BampSaved;

/** "system" arrays to make simulation (RKF method) for CUDA version (GPU) */
/* 6 arrays for intermediate values of RKF45 (pointers in GPU) */
extern double **d_k;
/* 6 arrays for intermediate values of RKF45 (pointers in the main memory) */
extern double *d_kHost[6];
/* arrays for small changes of the fifth order */
extern double *d_dm5;
/* array "lattice" which was copied to device memory */
extern double *d_lattice;
/* array "latticeParam" which was copied to device memory */
extern LatticeSiteDescr *d_latticeParam;
/** array for effective field of system */
extern double *d_H;
/** array for force of system */
extern double *d_F;

/** "system" arrays to make simulation (middle point method) for MPI and usual versions (main memory) */
/* values of intermediate and final small changes */
extern double *d_dmHalf, *d_dmFinal;
/* values of magnetic moments in middle point */
extern double *d_latticeHalf;

/** array to save amplitude of external magnetic field (Bx, By, Bz!) for every site */
extern double * d_Bamp;

/** array of normalized energy (all kinds) for every site (GPU) */
extern double * d_EnergyExchangeArray;
extern double * d_EnergyAnisotropyArrayAxisK1;
extern double * d_EnergyAnisotropyArrayAxisK2;
extern double * d_EnergyAnisotropyArrayAxisK3;
extern double * d_EnergyDipoleArray;
extern double * d_EnergyExternalArray;
extern double * d_EnergyDMIAxis1Array;
extern double * d_EnergyDMIAxis2Array;
extern double * d_EnergyDMIAxis3Array;
extern double * d_EnergyDMITriangleBulkArray;
extern double * d_EnergyDMITriangleInterfaceArray;
extern double * d_EnergyStretchingArray;
extern double * d_EnergyBendingArray;

/** array of normalized energy (all kinds) for every site (main memory) */
extern double * EnergyExchangeArray;
extern double * EnergyAnisotropyArrayAxisK1;
extern double * EnergyAnisotropyArrayAxisK2;
extern double * EnergyAnisotropyArrayAxisK3;
extern double * EnergyDipoleArray;
extern double * EnergyExternalArray;
extern double * EnergyDMIAxis1Array;
extern double * EnergyDMIAxis2Array;
extern double * EnergyDMIAxis3Array;
extern double * EnergyDMITriangleBulkArray;
extern double * EnergyDMITriangleInterfaceArray;
extern double * EnergyStretchingArray;
extern double * EnergyBendingArray;

/** array of change of magnetic moments with change of time (for every site) */
extern double * DmDt;
/** array of maximal change of coordinates with change of time (for every site) */
extern double * DrDt;
/** array for intermediate magnetic moments, coordinates and velocities for RKF45 */
extern double * d_mCh;

/** variables and arrays to cache data for dipole interaction */

/** size of cache */
extern size_t dipole_cacheSize;
/** r^3 coefficients for dipole interaction */
extern double ** dipole_R3coef;
/** rx*r^5 coefficients for dipole interaction */
extern double ** dipole_RxR5coef;
/** ry*r^5 coefficients for dipole interaction */
extern double ** dipole_RyR5coef;
/** rz*r^5 coefficients for dipole interaction */
extern double ** dipole_RzR5coef;

/** r^3 coefficients for dipole interaction in GPU (pointers in main memory) */
extern double ** d_dipole_R3coefHost;
/** r^3 coefficients for dipole interaction in GPU (pointers in GPU) */
extern double ** d_dipole_R3coef;
/** rx*r^5 coefficients for dipole interaction in GPU (pointers in main memory) */
extern double ** d_dipole_RxR5coefHost;
/** rx*r^5 coefficients for dipole interaction in GPU (pointers in GPU) */
extern double ** d_dipole_RxR5coef;
/** ry*r^5 coefficients for dipole interaction in GPU (pointers in main memory) */
extern double ** d_dipole_RyR5coefHost;
/** ry*r^5 coefficients for dipole interaction in GPU (pointers in GPU) */
extern double ** d_dipole_RyR5coef;
/** rz*r^5 coefficients for dipole interaction in GPU (pointers in main memory) */
extern double ** d_dipole_RzR5coefHost;
/** rz*r^5 coefficients for dipole interaction in GPU (pointers in GPU) */
extern double ** d_dipole_RzR5coef;

/** additional vectors to work with VTK XML base64 format */
extern double * addDoubleVectorXML;
extern int * addIntVectorXML;
extern int * addIntVectorConnXML;

#endif

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
//#include <nlopt.h>
#ifdef PARALLEL_MPI
#include <mpi.h>
#endif

#include "integration.h"
#include "../interactions/interactions.h"
#include "../interactions/externalField/externalField.h"
#include "../interactions/externalField/fieldSetup.h"
#include "../utils/utils.h"
#include "integutils.h"
#include "../utils/parallel_utils.h"
#include "diffequation.h"
#include "../writing/writing.h"
#include "integrator.h"

//double FunctionEnergy(unsigned n, const double *m, double *grad, void *my_func_data) {

	/* total normalized energy (with special supplement) and energy of one node */
	//double Enode, Etot = 0.0;
	/* intermediate value for calculation */
	//double interValue;
	/* initial value of different types of energy */
	//EnergyOfSystemNorm = 0.0;
	//EnergyExchangeNorm = 0.0;
	//EnergyExternalNorm = 0.0;
	//EnergyDipoleNorm = 0.0;
	//EnergyAnisotropyNormAxisK1 = 0.0;
	//EnergyAnisotropyNormAxisK2 = 0.0;
	//EnergyAnisotropyNormAxisK3 = 0.0;
	//EnergyDMIAxis1Norm = 0.0;
	//EnergyDMIAxis2Norm = 0.0;
	//EnergyDMIAxis3Norm = 0.0;
	/* full effective field (against gradient) of one node */
	//double Hx, Hy, Hz;
	/* number of spin in "latticeParam" and "lattice" */
	//size_t indexLattice, curIndex;

	/* initial value of gradients */
	//if (grad)
	//	memset(grad, 0, n);

	//double interValuet = 0;
	/* pass through each magnetic moment */
	//int i1, i2, i3;
	//for (i1 = 1; i1 < userVars.N1 - 1; i1++) {
	//	for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
	//		for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

				/* number of spin in array "latticeParam" */
				//indexLattice = IDX(i1, i2, i3);
				/* check if node is magnetic */
				//if (latticeParam[indexLattice].magnetic) {

					/* initial value of effective field */
					//Hx = 0.0;
					//Hy = 0.0;
					//Hz = 0.0;
					/* number of spin in array "lattice" */
					//curIndex = IDXmg(i1, i2, i3);
					/* find energy and effective field of one node */
					//GetEnergyAndHeffNode(i1, i2, i3, &Hx, &Hy, &Hz, &Enode);

					/* intermediate calculations */
					//interValue = 1 - m[curIndex] * m[curIndex] -
					//             m[curIndex + 1] * m[curIndex + 1] - m[curIndex + 2] * m[curIndex + 2];
					//interValuet += fabs(interValue);

					/* save gradient (with special supplement) for NLopt */
					//if (grad) {
					//	grad[curIndex] = -Hx - 4 * m[curIndex] * userVars.lagrangeDistance * interValue;
					//	grad[curIndex + 1] = -Hy - 4 * m[curIndex + 1] * userVars.lagrangeDistance * interValue;
					//	grad[curIndex + 2] = -Hz - 4 * m[curIndex + 2] * userVars.lagrangeDistance * interValue;
					//}

					/* save total energy for NLopt with constraint */
					//Etot += Enode;
					//Etot += userVars.lagrangeDistance * interValue * interValue;
					/* save total energy for writing */
					//EnergyOfSystemNorm += Enode;

				//}

			//}
		//}
	//}

	// to debug code
	//fprintf(stderr, "%f\n", grad[curIndex]);
	//fprintf(stderr, "%f\n", grad[curIndex + 1]);
	//fprintf(stderr, "%f\n", grad[curIndex + 2]);
	//fprintf(stderr, "%f\n", Etot);
	//fprintf(stderr, "=======\n");
	//sleep(1);

	//if (grad) {
	//	size_t spinsNumber = 3 * userVars.N1 * userVars.N2 * userVars.N3;
	//	/* write results with some period */
	//	if (((numberFrames - 1) % userVars.updatePeriod == 0) && (userVars.launch == LAUNCH_MINIMIZATION) &&
	//	    (interValuet / spinsNumber < userVars.lengthResidue)) {
	//		/* handle results before writing */
	//		CalculateTotalMagneticMoments(spinsNumber);
	//		WriteFrame(0.0, true);
	//		/* add 1 to number of frames */
	//		numberFrames++;
	//	}
	//}

	/* return total energy (with special supplement) */
	//return Etot;

//}

void CalculateTotalMagneticMoments(size_t amount) {

	/* initial value */
	mxTot = 0.0;
	myTot = 0.0;
	mzTot = 0.0;

	/* go through all sites */
	int i1, i2, i3;
	for ( i1 = 1; i1 <= userVars.N1 - 2; i1++) {
		for (  i2 = 1; i2 <= userVars.N2 - 2; i2++) {
			for (  i3 = 1; i3 <= userVars.N3 - 2; i3++) {
				/* find current site */
				long curIndex = IDXmg(i1, i2, i3);
				/* find total magnetic moments */
				mxTot += lattice[curIndex];
				myTot += lattice[curIndex + 1];
				mzTot += lattice[curIndex + 2];
			}
		}
	}

}

/* function to update global external field using middle site */
void updateGlobalExternalField(void) {

	/* find of index for "middle site" */
	int i1 = (userVars.N1 - 1) / 2;
	int i2 = (userVars.N2 - 1) / 2;
	int i3 = (userVars.N3 - 1) / 2;

	/* calculate index in "Bxamp" */
	int curIndex = IDXmg(i1, i2, i3);

	/* update global external field */
	userVars.Bx = Bamp[curIndex];
	userVars.By = Bamp[curIndex + 1];
	userVars.Bz = Bamp[curIndex + 2];

}

int callIntegrateFunctions(double tCur, double frame, size_t spinsNumber, double prec,
						   void (*pointerFunc)(double *, double *, size_t, double, double)) {

	/* initial code of integrator */
	int codeIntegrator = 0;

	/* integrate functions */
	if (userVars.integrationMethod == INTEGRATION_METHOD_RKF) {
		codeIntegrator = IntegrateFunctionsRKF(tCur - frame, tCur, spinsNumber, prec, pointerFunc);
	} else if (userVars.integrationMethod == INTEGRATION_METHOD_MIDDLE_POINT) {
		codeIntegrator = IntegrateFunctionsMiddlePoint(tCur - frame, tCur, spinsNumber, prec, pointerFunc);
	}

	/* return code of integrator */
	return codeIntegrator;

}

int LaunchTimeDynamics(double t0, double tfin, double frame, double prec, bool flog_ON) {

	/* variables for code of errors */
	int codeIntegrator = 0;
	int codeWrite = 0;
	/* total code to return back */
	int codeTotal = 0;
	/* current time */
	double tCur = t0 + frame;
	/* number of components of spins in system */
	size_t spinsNumber = (size_t)(NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3);
	/* variables to measure time of snapshot */
	clock_t initTime = 0, finalTime = 0;

	/* initialize Python variables */
	if (initExtScriptField()) {
		return 1;
	}

	/* crating array of intermediate values for RKF45 */
	double * RKF;
	createDoubleArrays(&RKF, spinsNumber);

	/* initialize Python variables */
	if (initExtScriptField()) {
		return 1;
	}

	/* initial value of maximum changes of magnetic moments */
	MaxDmDt = 0.0;

	/* find out initial energy and other parameters of magnetic system */
	findEnergy(RKF, spinsNumber, t0, 0.0);
	/* add all energies of system */
	addAllEnergies();

	/* handle results of simulation */
	if ((doItInMasterProcess()) &&  (userVars.restartOnOff == RESTART_OFF)) {
		/* calculate total magnetic moment */
		CalculateTotalMagneticMoments(spinsNumber);
		/* record results after this frame */
		codeWrite = WriteFrame(t0, flog_ON, 1);
	}

	/* launch simulation with frames */
	waitProcesses();
	while ((tCur <= tfin) && (!codeIntegrator) && (!codeWrite)) {

		/* only for master process */
		if (doItInMasterProcess()) {
			/* measure begin time */
			initTime = clock();
		}

		/* launch magnetic simulation for certain frame */
		if (userVars.latticeType == LATTICE_CUBIC) {
			codeIntegrator = callIntegrateFunctions(tCur, frame, spinsNumber, prec, DiffEquation);
		} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
			codeIntegrator = callIntegrateFunctions(tCur, frame, spinsNumber, prec, DiffEquationTriangle);
		}

		/* update global external field */
		if (userVars.extField != EXTFIELD_CONST) {
			updateGlobalExternalField();
		}

		/* only for master process */
		if (doItInMasterProcess()) {
			/* measure final time */
			finalTime = clock();
			/* calculate time difference */
			timeSnap = ((double) (finalTime - initTime)) / CLOCKS_PER_SEC;
		}

		/* add time for new frame */
		tCur = tCur + frame;
		/* modify result indicator for the last frame */
		if (tCur > tfin) {
			codeIntegrator = 1;
		}

		/* handle results of simulation */
		if (doItInMasterProcess()) {
			/* calculate total magnetic moment */
			CalculateTotalMagneticMoments(spinsNumber);
			/* record results after this frame */
			codeWrite = WriteFrame(tCur - frame, flog_ON, codeIntegrator);
		}

	}

	/* wait MPI processes */
	waitProcesses();

	/* create total code of error */
	if (((codeIntegrator) || (codeWrite)) && (tCur < tfin)) {
		codeTotal = 1;
	}

	/* remove Python variables */
	finalizeExtScriptField();
	/* delete intermediate RKF-arrays */
	deleteDoubleArrays(RKF);

	/* return code of error */
	return codeTotal;

}

int LaunchOptimization(bool flog_ON) {

	/* check external field */
	//if (userVars.extField == EXTFIELD_HARMTIME) {
	//	fprintf(stderr, "External field must be constant, from file or from script to launch optimization!\n");
	//	return 1;
	//}

	/* initial frame number */
	//frameNumber = 0;

	/* number of components of magnetic moments */
	//size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	/* create instance of structure for optimization */
	//nlopt_opt opt = nlopt_create(NLOPT_LD_TNEWTON_RESTART, spinsNumber);
	//if (userVars.minMethod == TNEWTON_PRECOND_RESTART) {
	//	opt = nlopt_create(NLOPT_LD_TNEWTON_PRECOND_RESTART, spinsNumber);
	//} else if (userVars.minMethod == TNEWTON) {
	//	opt = nlopt_create(NLOPT_LD_TNEWTON, spinsNumber);
	//} else if (userVars.minMethod == LBFGS) {
	//	opt = nlopt_create(NLOPT_LD_LBFGS, spinsNumber);
	//}

	/* put some parameters */
	//nlopt_set_min_objective(opt, FunctionEnergy, NULL);
	//nlopt_set_xtol_rel(opt, userVars.stopValue);
	/* initial number of frames */
	//numberFrames = 1;
	/* launch optimization */
	//int res = nlopt_optimize(opt, lattice, &EnergyOfSystemNorm);
	//if (res < 0) {
	//	fprintf(stderr, "NLopt failed with error code %i!\n", res);
	//	nlopt_destroy(opt);
	//	return 1;
	//}

	/* normalize and handle results */
	//NormalizeVectors();
	//CalculateTotalMagneticMoments(spinsNumber);
	/* write final results */
	//if (WriteFrame(0.0, true))
	//	return 1;

	/* destroy instance */
	//nlopt_destroy(opt);

	return 0;
}

int LaunchHysteresis(bool flog_ON) {

	/* check external parameters */
	//if ((!userVars.bBxamp) || (!userVars.bByamp) || (!userVars.bBzamp) || (!userVars.brefreshRate)) {
	//	fprintf(stderr, "WRITING.C: Not enough parameters for hysteresis!\n");
	//	return 1;
	//}

	/* initial frame number */
	//frameNumber = 0;

	/* make external field constant */
	//userVars.extField = EXTFIELD_CONST;

	/* number of components of spins */
	//size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	/* create instance of structure for optimization */
	//nlopt_opt opt = opt = nlopt_create(NLOPT_LD_TNEWTON_RESTART, spinsNumber);
	//if (userVars.minMethod == TNEWTON_PRECOND_RESTART) {
	//	opt = nlopt_create(NLOPT_LD_TNEWTON_PRECOND_RESTART, spinsNumber);
	//} else if (userVars.minMethod == TNEWTON) {
	//	opt = nlopt_create(NLOPT_LD_TNEWTON, spinsNumber);
	//} else if (userVars.minMethod == LBFGS) {
	//	opt = nlopt_create(NLOPT_LD_LBFGS, spinsNumber);
	//}
	/* put some parameters */
	//nlopt_set_min_objective(opt, FunctionEnergy, NULL);
	//nlopt_set_xtol_rel(opt, userVars.stopValue);

	/* initial value for external field */
	//userVars.Bx = 0.0;
	//userVars.By = 0.0;
	//userVars.Bz = 0.0;

	/* increase external field */
	//for (int i = 1; i <= userVars.refreshRate; i++) {
	/* launch optimization */
	//	int res = nlopt_optimize(opt, lattice, &EnergyOfSystemNorm);
	//	if (res < 0) {
	//		fprintf(stderr, "NLopt failed with error code %i!\n", res);
	//		nlopt_destroy(opt);
	//		return 1;
	//	}
	/* normalize and handle results */
	//	NormalizeVectors();
	//	CalculateTotalMagneticMoments(spinsNumber);
	/* write results */
	//	if (WriteFrame(0.0, true))
	//		return 1;
	/* increase external field */
	//	userVars.Bx += userVars.Bxamp / userVars.refreshRate;
	//	userVars.By += userVars.Byamp / userVars.refreshRate;
	//	userVars.Bz += userVars.Bzamp / userVars.refreshRate;
	//}

	/* decrease external field */
	//for (int i = 1; i <= 2 * userVars.refreshRate; i++) {
	//	int res = nlopt_optimize(opt, lattice, &EnergyOfSystemNorm);
	//	if (res < 0) {
	//		fprintf(stderr, "NLopt failed with error code %i!\n", res);
	//		nlopt_destroy(opt);
	//		return 1;
	//	}
	/* normalize and handle results */
	//	NormalizeVectors();
	//	CalculateTotalMagneticMoments(spinsNumber);
	/* write results */
	//	if (WriteFrame(0.0, true))
	//		return 1;
	/* decrease external field */
	//	userVars.Bx -= userVars.Bxamp / userVars.refreshRate;
	//	userVars.By -= userVars.Byamp / userVars.refreshRate;
	//	userVars.Bz -= userVars.Bzamp / userVars.refreshRate;
	//}

	/* increase external field again */
	//for (int i = 1; i <= 2 * userVars.refreshRate + 1; i++) {
	/* launch optimization */
	//	int res = nlopt_optimize(opt, lattice, &EnergyOfSystemNorm);
	//	if (res < 0) {
	//		fprintf(stderr, "NLopt failed with error code %i!\n", res);
	//		nlopt_destroy(opt);
	//		return 1;
	//	}
	/* normalize and handle results */
	//	NormalizeVectors();
	//	CalculateTotalMagneticMoments(spinsNumber);
	/* write results */
	//	if (WriteFrame(0.0, true))
	//		return 1;
	/* increase external field */
	//	userVars.Bx += userVars.Bxamp / userVars.refreshRate;
	//	userVars.By += userVars.Byamp / userVars.refreshRate;
	//	userVars.Bz += userVars.Bzamp / userVars.refreshRate;
	//}

	/* destroy instance */
	//nlopt_destroy(opt);

	return 0;

}

#ifndef SMALLCHANGES_H
#define SMALLCHANGES_H

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../alltypes.h"

/* wrapper of function to calculate "small changes" */
void calculateSmallChangesCUDA(double *d_H, double *d_m, double *d_dm, double step, double gilbertDamping, long numberSites);

/* function to fill zero array of effective field */
void fillZeroEffective(void);

/* function to fill zero arrays of energies */
void fillZeroEnergies(void);

/* function to synchronize execution of CUDA-threads */
void synchronizeThreads(void);

#endif

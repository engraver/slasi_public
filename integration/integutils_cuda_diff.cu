#include "../alltypes.h"
extern "C" {
	#include "integutils_cuda_diff.h"
}

void CallFunction(double *k, double *m, size_t amount, double t, double h,
                  void (*pointerFunc)(double *, double *, size_t, double, double h)) {

	/* call function that contains equations for Cauchy problem */
	(*pointerFunc)(k, m, amount, t, h);

}

extern "C"
void CalculateGrid0CUDA(double *k, double *m, double t, size_t amount, double h,
                        void (*pointerFunc)(double *, double *, size_t, double, double)) {

	
	/* calculate zero grid of intermediate values for Runge-Kutta-Fehlberg */
	CallFunction(k, m, amount, t, h, pointerFunc);

}

__global__
void showArray(double *m, long numberSites) {

	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	if (indexLattice < numberSites) { 
		printf("%li %.10f %.10f %.10f\n", indexLattice, m[curIndex], m[curIndex + 1], m[curIndex + 2]);
	}

}

/* debug function */
/* extern "C"
void showArrayCUDA(double *array) {

	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;

	printf("%li\n", numberSites);
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	showArray<<<cores,threads>>>(array, numberSites);	
	cudaDeviceSynchronize();

} */

/* function to calculate the first grid using GPU */
__global__
void addGrid1(double *d_mCh, double *m, double *kpast1, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* calculate number of spins */
	long spinsNumber = 3 * numberSites;

	/* check boundary condition for threads */
	if (indexLattice < numberSites) {

		/* add two vectors for intermediate values of RKF45 */

		/* magnetic moments */
		d_mCh[curIndex] = m[curIndex] +
		                  (1.0 / 4.0) * kpast1[curIndex];
		d_mCh[curIndex + 1] = m[curIndex + 1] +
		                      (1.0 / 4.0) * kpast1[curIndex + 1];
		d_mCh[curIndex + 2] = m[curIndex + 2] +
		                      (1.0 / 4.0) * kpast1[curIndex + 2];

		/* coordinates */
		d_mCh[curIndex + spinsNumber] = m[curIndex + spinsNumber] +
		                                (1.0 / 4.0) * kpast1[curIndex + spinsNumber];
		d_mCh[curIndex + spinsNumber + 1] = m[curIndex + spinsNumber + 1] +
		                                    (1.0 / 4.0) * kpast1[curIndex + spinsNumber + 1];
		d_mCh[curIndex + spinsNumber + 2] = m[curIndex + spinsNumber + 2] +
		                                    (1.0 / 4.0) * kpast1[curIndex + spinsNumber + 2];

		/* velocities */
		d_mCh[curIndex + 2 * spinsNumber] = m[curIndex + 2 * spinsNumber] +
		                                    (1.0 / 4.0) * kpast1[curIndex + 2 * spinsNumber];
		d_mCh[curIndex + 2 * spinsNumber + 1] = m[curIndex + 2 * spinsNumber + 1] +
		                                        (1.0 / 4.0) * kpast1[curIndex + 2 * spinsNumber + 1];
		d_mCh[curIndex + 2 * spinsNumber + 2] = m[curIndex + 2 * spinsNumber + 2] +
		                                        (1.0 / 4.0) * kpast1[curIndex + 2 * spinsNumber + 2];

	}

}

extern "C"
void CalculateGrid1CUDA(double *k, double *kpast1, double *m, double t, size_t amount, double h,
                        void (*pointerFunc)(double *, double *, size_t, double, double)) {

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;

	/* set the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* calculate the first grid of intermediate values for Runge-Kutta-Fehlberg */
	addGrid1<<<cores,threads>>>(d_mCh, m, kpast1, numberSites);
	cudaDeviceSynchronize();

	/* to debug code */
	//if (number == 1) {
	//	printf("mCh\n");
	//	showArray<<<cores,threads>>>(d_mCh, numberSites);
	//}

	/* call function of differential equation */
	CallFunction(k, d_mCh, amount, t + (1.0 / 4.0) * h, h, pointerFunc);

}

/* function to calculate the second grid using GPU */
__global__
void addGrid2(double *d_mCh, double *m, double *kpast1, double *kpast2, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* calculate number of spins */
	long spinsNumber = 3 * numberSites;

	/* check boundary condition for threads */
	if (indexLattice < numberSites) {

		/* add two vectors for intermediate values of RKF45 */

		/* magnetic moments */
		d_mCh[curIndex] = m[curIndex] +
		                  (9.0 / 32.0) * kpast1[curIndex] +
		                  (3.0 / 32.0) * kpast2[curIndex];
		d_mCh[curIndex + 1] = m[curIndex + 1] +
		                      (9.0 / 32.0) * kpast1[curIndex + 1] +
		                      (3.0 / 32.0) * kpast2[curIndex + 1];
		d_mCh[curIndex + 2] = m[curIndex + 2] +
		                      (9.0 / 32.0) * kpast1[curIndex + 2] +
		                      (3.0 / 32.0) * kpast2[curIndex + 2];

		/* coordinates */
		d_mCh[curIndex + spinsNumber] = m[curIndex + spinsNumber] +
		                                (9.0 / 32.0) * kpast1[curIndex + spinsNumber] +
		                                (3.0 / 32.0) * kpast2[curIndex + spinsNumber];
		d_mCh[curIndex + spinsNumber + 1] = m[curIndex + spinsNumber + 1] +
		                                    (9.0 / 32.0) * kpast1[curIndex + spinsNumber + 1] +
		                                    (3.0 / 32.0) * kpast2[curIndex + spinsNumber + 1];
		d_mCh[curIndex + spinsNumber + 2] = m[curIndex + spinsNumber + 2] +
		                                    (9.0 / 32.0) * kpast1[curIndex + spinsNumber + 2] +
		                                    (3.0 / 32.0) * kpast2[curIndex + spinsNumber + 2];

		/* velocities */
		d_mCh[curIndex + 2 * spinsNumber] = m[curIndex + 2 * spinsNumber] +
		                                    (9.0 / 32.0) * kpast1[curIndex + 2 * spinsNumber] +
		                                    (3.0 / 32.0) * kpast2[curIndex + 2 * spinsNumber];
		d_mCh[curIndex + 2 * spinsNumber + 1] = m[curIndex + 2 * spinsNumber + 1] +
		                                        (9.0 / 32.0) * kpast1[curIndex + 2 * spinsNumber + 1] +
		                                        (3.0 / 32.0) * kpast2[curIndex + 2 * spinsNumber + 1];
		d_mCh[curIndex + 2 * spinsNumber + 2] = m[curIndex + 2 * spinsNumber + 2] +
		                                        (9.0 / 32.0) * kpast1[curIndex + 2 * spinsNumber + 2] +
		                                        (3.0 / 32.0) * kpast2[curIndex + 2 * spinsNumber + 2];

	}

	__syncthreads();

}

extern "C"
void CalculateGrid2CUDA(double *k, double *kpast1, double *kpast2, double *m, double t, size_t amount, double h,
                        void (*pointerFunc)(double *, double *, size_t, double, double)) {

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;

	/* set the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* calculate the second grid of intermediate values for Runge-Kutta-Fehlberg */
	addGrid2<<<cores,threads>>>(d_mCh, m, kpast1, kpast2, numberSites);
	cudaDeviceSynchronize();

	/* call function of differential equation */
	CallFunction(k, d_mCh, amount, t + (3.0 / 8.0) * h, h, pointerFunc);

}

/* function to calculate the third grid using GPU */
__global__
void addGrid3(double *d_mCh, double *m, double *kpast1, double *kpast2, double *kpast3, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* calculate number of spins */
	long spinsNumber = 3 * numberSites;

	/* check boundary condition for threads */
	if (indexLattice < numberSites) {

		/* add two vectors for intermediate values of RKF45 */

		/* magnetic moments */
		d_mCh[curIndex] = m[curIndex] +
		                  (7296.0 / 2197.0) * kpast1[curIndex] -
		                  (7200.0 / 2197.0) * kpast2[curIndex] +
		                  (1932.0 / 2197.0) * kpast3[curIndex];
		d_mCh[curIndex + 1] = m[curIndex + 1] +
		                      (7296.0 / 2197.0) * kpast1[curIndex + 1] -
		                      (7200.0 / 2197.0) * kpast2[curIndex + 1] +
		                      (1932.0 / 2197.0) * kpast3[curIndex + 1];
		d_mCh[curIndex + 2] = m[curIndex + 2] +
		                      (7296.0 / 2197.0) * kpast1[curIndex + 2] -
		                      (7200.0 / 2197.0) * kpast2[curIndex + 2] +
		                      (1932.0 / 2197.0) * kpast3[curIndex + 2];

		/* coordinates */
		d_mCh[curIndex + spinsNumber] = m[curIndex + spinsNumber] +
		                                (7296.0 / 2197.0) * kpast1[curIndex + spinsNumber] -
		                                (7200.0 / 2197.0) * kpast2[curIndex + spinsNumber] +
		                                (1932.0 / 2197.0) * kpast3[curIndex + spinsNumber];
		d_mCh[curIndex + spinsNumber + 1] = m[curIndex + spinsNumber + 1] +
		                                    (7296.0 / 2197.0) * kpast1[curIndex + spinsNumber + 1] -
		                                    (7200.0 / 2197.0) * kpast2[curIndex + spinsNumber + 1] +
		                                    (1932.0 / 2197.0) * kpast3[curIndex + spinsNumber + 1];
		d_mCh[curIndex + spinsNumber + 2] = m[curIndex + spinsNumber + 2] +
		                                    (7296.0 / 2197.0) * kpast1[curIndex + spinsNumber + 2] -
		                                    (7200.0 / 2197.0) * kpast2[curIndex + spinsNumber + 2] +
		                                    (1932.0 / 2197.0) * kpast3[curIndex + spinsNumber + 2];

		/* velocities */
		d_mCh[curIndex + 2 * spinsNumber] = m[curIndex + 2 * spinsNumber] +
		                                    (7296.0 / 2197.0) * kpast1[curIndex + 2 * spinsNumber] -
		                                    (7200.0 / 2197.0) * kpast2[curIndex + 2 * spinsNumber] +
		                                    (1932.0 / 2197.0) * kpast3[curIndex + 2 * spinsNumber];
		d_mCh[curIndex + 2 * spinsNumber + 1] = m[curIndex + 2 * spinsNumber + 1] +
		                                        (7296.0 / 2197.0) * kpast1[curIndex + 2 * spinsNumber + 1] -
		                                        (7200.0 / 2197.0) * kpast2[curIndex + 2 * spinsNumber + 1] +
		                                        (1932.0 / 2197.0) * kpast3[curIndex + 2 * spinsNumber + 1];
		d_mCh[curIndex + 2 * spinsNumber + 2] = m[curIndex + 2 * spinsNumber + 2] +
		                                        (7296.0 / 2197.0) * kpast1[curIndex + 2 * spinsNumber + 2] -
		                                        (7200.0 / 2197.0) * kpast2[curIndex + 2 * spinsNumber + 2] +
		                                        (1932.0 / 2197.0) * kpast3[curIndex + 2 * spinsNumber + 2];

	}

}

extern "C"
void CalculateGrid3CUDA(double *k, double *kpast1, double *kpast2, double *kpast3, double *m, double t, size_t amount, double h,
                        void (*pointerFunc)(double *, double *, size_t, double, double)) {

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;

	/* set the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* calculate the third grid of intermediate values for Runge-Kutta-Fehlberg */
	addGrid3<<<cores,threads>>>(d_mCh, m, kpast1, kpast2, kpast3, numberSites);
	cudaDeviceSynchronize();

	/* call function of differential equation */
	CallFunction(k, d_mCh, amount, t + (12.0 / 13.0) * h, h, pointerFunc);

}

/* function to calculate the fourth grid using GPU */
__global__
void addGrid4(double *d_mCh, double *m, double *kpast1, double *kpast2, double *kpast3, double *kpast4, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* calculate number of spins */
	long spinsNumber = 3 * numberSites;

	/* check boundary condition for threads */
	if (indexLattice < numberSites) {

		/* add two vectors for intermediate values of RKF45 */

		/* magnetic moments */
		d_mCh[curIndex] = m[curIndex] -
		                  (845.0 / 4104.0) * kpast1[curIndex] +
		                  (3680.0 / 513.0) * kpast2[curIndex] -
		                  8.0 * kpast3[curIndex] +
		                  (439.0 / 216.0) * kpast4[curIndex];
		d_mCh[curIndex + 1] = m[curIndex + 1] -
		                      (845.0 / 4104.0) * kpast1[curIndex + 1] +
		                      (3680.0 / 513.0) * kpast2[curIndex + 1] -
		                      8.0 * kpast3[curIndex + 1] +
		                      (439.0 / 216.0) * kpast4[curIndex + 1];
		d_mCh[curIndex + 2] = m[curIndex + 2] -
		                      (845.0 / 4104.0) * kpast1[curIndex + 2] +
		                      (3680.0 / 513.0) * kpast2[curIndex + 2] -
		                      8.0 * kpast3[curIndex + 2] +
		                      (439.0 / 216.0) * kpast4[curIndex + 2];

		/* coordinates */
		d_mCh[curIndex + spinsNumber] = m[curIndex + spinsNumber] -
		                                (845.0 / 4104.0) * kpast1[curIndex + spinsNumber] +
		                                (3680.0 / 513.0) * kpast2[curIndex + spinsNumber] -
		                                8.0 * kpast3[curIndex + spinsNumber] +
		                                (439.0 / 216.0) * kpast4[curIndex + spinsNumber];
		d_mCh[curIndex + spinsNumber + 1] = m[curIndex + spinsNumber + 1] -
		                                    (845.0 / 4104.0) * kpast1[curIndex + spinsNumber + 1] +
		                                    (3680.0 / 513.0) * kpast2[curIndex + spinsNumber + 1] -
		                                    8.0 * kpast3[curIndex + spinsNumber + 1] +
		                                    (439.0 / 216.0) * kpast4[curIndex + spinsNumber + 1];
		d_mCh[curIndex + spinsNumber + 2] = m[curIndex + spinsNumber + 2] -
		                                    (845.0 / 4104.0) * kpast1[curIndex + spinsNumber + 2] +
		                                    (3680.0 / 513.0) * kpast2[curIndex + spinsNumber + 2] -
		                                    8.0 * kpast3[curIndex + spinsNumber + 2] +
		                                    (439.0 / 216.0) * kpast4[curIndex + spinsNumber + 2];

		/* velocities */
		d_mCh[curIndex + 2 * spinsNumber] = m[curIndex + 2 * spinsNumber] -
		                                    (845.0 / 4104.0) * kpast1[curIndex + 2 * spinsNumber] +
		                                    (3680.0 / 513.0) * kpast2[curIndex + 2 * spinsNumber] -
		                                    8.0 * kpast3[curIndex + 2 * spinsNumber] +
		                                    (439.0 / 216.0) * kpast4[curIndex + 2 * spinsNumber];
		d_mCh[curIndex + 2 * spinsNumber + 1] = m[curIndex + 2 * spinsNumber + 1] -
		                                        (845.0 / 4104.0) * kpast1[curIndex + 2 * spinsNumber + 1] +
		                                        (3680.0 / 513.0) * kpast2[curIndex + 2 * spinsNumber + 1] -
		                                        8.0 * kpast3[curIndex + 2 * spinsNumber + 1] +
		                                        (439.0 / 216.0) * kpast4[curIndex + 2 * spinsNumber + 1];
		d_mCh[curIndex + 2 * spinsNumber + 2] = m[curIndex + 2 * spinsNumber + 2] -
		                                        (845.0 / 4104.0) * kpast1[curIndex + 2 * spinsNumber + 2] +
		                                        (3680.0 / 513.0) * kpast2[curIndex + 2 * spinsNumber + 2] -
		                                        8.0 * kpast3[curIndex + 2 * spinsNumber + 2] +
		                                        (439.0 / 216.0) * kpast4[curIndex + 2 * spinsNumber + 2];

	}

}

extern "C"
void CalculateGrid4CUDA(double *k, double *kpast1, double *kpast2, double *kpast3, double *kpast4, double *m, double t,
                        size_t amount, double h, void (*pointerFunc)(double *, double *, size_t, double, double)) {

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;

	/* set the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* calculate the fourth grid of intermediate values for Runge-Kutta-Fehlberg */
	addGrid4<<<cores,threads>>>(d_mCh, m, kpast1, kpast2, kpast3, kpast4, numberSites);
	cudaDeviceSynchronize();

	/* call function of differential equation */
	CallFunction(k, d_mCh, amount, t + h, h, pointerFunc);

}

/* function to calculate the fifth grid using GPU */
__global__
void addGrid5(double *d_mCh, double *m, double *kpast1, double *kpast2, double *kpast3, double *kpast4, double *kpast5, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* calculate number of spins */
	long spinsNumber = 3 * numberSites;

	/* check boundary condition for threads */
	if (indexLattice < numberSites) {

		/* add two vectors for intermediate values of RKF45 */

		/* magnetic moments */
		d_mCh[curIndex] = m[curIndex] -
		                  (11.0 / 40.0) * kpast1[curIndex] +
		                  (1859.0 / 4104.0) * kpast2[curIndex] -
		                  (3544.0 / 2565.0) * kpast3[curIndex] +
		                  2.0 * kpast4[curIndex] -
		                  (8.0 / 27.0) * kpast5[curIndex];
		d_mCh[curIndex + 1] = m[curIndex + 1] -
		                      (11.0 / 40.0) * kpast1[curIndex + 1] +
		                      (1859.0 / 4104.0) * kpast2[curIndex + 1] -
		                      (3544.0 / 2565.0) * kpast3[curIndex + 1] +
		                      2.0 * kpast4[curIndex + 1] -
		                      (8.0 / 27.0) * kpast5[curIndex + 1];
		d_mCh[curIndex + 2] = m[curIndex + 2] -
		                      (11.0 / 40.0) * kpast1[curIndex + 2] +
		                      (1859.0 / 4104.0) * kpast2[curIndex + 2] -
		                      (3544.0 / 2565.0) * kpast3[curIndex + 2] +
		                      2.0 * kpast4[curIndex + 2] -
		                      (8.0 / 27.0) * kpast5[curIndex + 2];

		/* coordinates */
		d_mCh[curIndex + spinsNumber] = m[curIndex + spinsNumber] -
		                                (11.0 / 40.0) * kpast1[curIndex + spinsNumber] +
		                                (1859.0 / 4104.0) * kpast2[curIndex + spinsNumber] -
		                                (3544.0 / 2565.0) * kpast3[curIndex + spinsNumber] +
		                                2.0 * kpast4[curIndex + spinsNumber] -
		                                (8.0 / 27.0) * kpast5[curIndex + spinsNumber];
		d_mCh[curIndex + spinsNumber + 1] = m[curIndex + spinsNumber + 1] -
		                                    (11.0 / 40.0) * kpast1[curIndex + spinsNumber + 1] +
		                                    (1859.0 / 4104.0) * kpast2[curIndex + spinsNumber + 1] -
		                                    (3544.0 / 2565.0) * kpast3[curIndex + spinsNumber + 1] +
		                                    2.0 * kpast4[curIndex + spinsNumber + 1] -
		                                    (8.0 / 27.0) * kpast5[curIndex + spinsNumber + 1];
		d_mCh[curIndex + spinsNumber + 2] = m[curIndex + spinsNumber + 2] -
		                                    (11.0 / 40.0) * kpast1[curIndex + spinsNumber + 2] +
		                                    (1859.0 / 4104.0) * kpast2[curIndex + spinsNumber + 2] -
		                                    (3544.0 / 2565.0) * kpast3[curIndex + spinsNumber + 2] +
		                                    2.0 * kpast4[curIndex + spinsNumber + 2] -
		                                    (8.0 / 27.0) * kpast5[curIndex + spinsNumber + 2];

		/* velocities */
		d_mCh[curIndex + 2 * spinsNumber] = m[curIndex + 2 * spinsNumber] +
		                                    (11.0 / 40.0) * kpast1[curIndex + 2 * spinsNumber] +
		                                    (1859.0 / 4104.0) * kpast2[curIndex + 2 * spinsNumber] -
		                                    (3544.0 / 2565.0) * kpast3[curIndex + 2 * spinsNumber] +
		                                    2.0 * kpast4[curIndex + 2 * spinsNumber] -
		                                    (8.0 / 27.0) * kpast5[curIndex + 2 * spinsNumber];
		d_mCh[curIndex + 2 * spinsNumber + 1] = m[curIndex + 2 * spinsNumber + 1] +
		                                        (11.0 / 40.0) * kpast1[curIndex + 2 * spinsNumber + 1] +
		                                        (1859.0 / 4104.0) * kpast2[curIndex + 2 * spinsNumber + 1] -
		                                        (3544.0 / 2565.0) * kpast3[curIndex + 2 * spinsNumber + 1] +
		                                        2.0 * kpast4[curIndex + 2 * spinsNumber + 1] -
		                                        (8.0 / 27.0) * kpast5[curIndex + 2 * spinsNumber + 1];
		d_mCh[curIndex + 2 * spinsNumber + 2] = m[curIndex + 2 * spinsNumber + 2] +
		                                        (11.0 / 40.0) * kpast1[curIndex + 2 * spinsNumber + 2] +
		                                        (1859.0 / 4104.0) * kpast2[curIndex + 2 * spinsNumber + 2] -
		                                        (3544.0 / 2565.0) * kpast3[curIndex + 2 * spinsNumber + 2] +
		                                        2.0 * kpast4[curIndex + 2 * spinsNumber + 2] -
		                                        (8.0 / 27.0) * kpast5[curIndex + 2 * spinsNumber + 2];

	}

}

extern "C"
void CalculateGrid5CUDA(double *k, double *kpast1, double *kpast2, double *kpast3, double *kpast4, double *kpast5, double *m,
                        double t, size_t amount, double h, void (*pointerFunc)(double *, double *, size_t, double, double)) {

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;

	/* set the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* calculate the fifth grid of intermediate values for Runge-Kutta-Fehlberg */
	addGrid5<<<cores,threads>>>(d_mCh, m, kpast1, kpast2, kpast3, kpast4, kpast5, numberSites);
	cudaDeviceSynchronize();

	/* call function of differential equation */
	CallFunction(k, d_mCh, amount, t + (1.0 / 2.0) * h, h, pointerFunc);

}

/* function to fill via zero of all "global" energies */
void fillZeroGlobalEnergies() {

	/* fill using zero */
	EnergyExchangeNorm = 0.0;
	EnergyAnisotropyNormAxisK1 = 0.0;
	EnergyAnisotropyNormAxisK2 = 0.0;
	EnergyAnisotropyNormAxisK3 = 0.0;
	EnergyDipoleNorm = 0.0;
	EnergyDMIAxis1Norm = 0.0;
	EnergyDMIAxis2Norm = 0.0;
	EnergyDMIAxis3Norm = 0.0;
	EnergyDMITriangleInterfaceNorm = 0.0;
	EnergyDMITriangleBulkNorm = 0.0;
	EnergyStretchingNorm = 0.0;
	EnergyBendingNorm = 0.0;
	EnergyExternalNorm = 0.0;

}

/* function to add energy of one kind for all sites to total energy */
void addOneKindEnergy(double *EnergyTotal, double *EnergyArray) {

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;
	/* go through all sites */
	for (long i = 0; i < numberSites; i++) {
		*EnergyTotal += EnergyArray[i];
	}

}

extern "C"
void addAllEnergiesCUDA(void) {

	/* fill zero of global energies */
	fillZeroGlobalEnergies();
	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;

	/* copy array of different energies */
	cudaMemcpy(EnergyExchangeArray, d_EnergyExchangeArray, numberSites * sizeof(double), cudaMemcpyDeviceToHost);
	cudaMemcpy(EnergyAnisotropyArrayAxisK1, d_EnergyAnisotropyArrayAxisK1, numberSites * sizeof(double), cudaMemcpyDeviceToHost);
	cudaMemcpy(EnergyAnisotropyArrayAxisK2, d_EnergyAnisotropyArrayAxisK2, numberSites * sizeof(double), cudaMemcpyDeviceToHost);
	cudaMemcpy(EnergyAnisotropyArrayAxisK3, d_EnergyAnisotropyArrayAxisK3, numberSites * sizeof(double), cudaMemcpyDeviceToHost);
	cudaMemcpy(EnergyDipoleArray, d_EnergyDipoleArray, numberSites * sizeof(double), cudaMemcpyDeviceToHost);
	cudaMemcpy(EnergyExternalArray, d_EnergyExternalArray, numberSites * sizeof(double), cudaMemcpyDeviceToHost);
	/* it is only for cubic system */
	if (userVars.latticeType == LATTICE_CUBIC) {
		cudaMemcpy(EnergyDMIAxis1Array, d_EnergyDMIAxis1Array, numberSites * sizeof(double), cudaMemcpyDeviceToHost);
		cudaMemcpy(EnergyDMIAxis2Array, d_EnergyDMIAxis2Array, numberSites * sizeof(double), cudaMemcpyDeviceToHost);
		cudaMemcpy(EnergyDMIAxis3Array, d_EnergyDMIAxis3Array, numberSites * sizeof(double), cudaMemcpyDeviceToHost);
	}
	/* it is only for system with triangular lattice */
	if (userVars.latticeType == LATTICE_TRIANGULAR) {
		cudaMemcpy(EnergyDMITriangleBulkArray, d_EnergyDMITriangleBulkArray, numberSites * sizeof(double), cudaMemcpyDeviceToHost);
		cudaMemcpy(EnergyDMITriangleInterfaceArray, d_EnergyDMITriangleInterfaceArray, numberSites * sizeof(double), cudaMemcpyDeviceToHost);
		cudaMemcpy(EnergyStretchingArray, d_EnergyStretchingArray, numberSites * sizeof(double), cudaMemcpyDeviceToHost);
		cudaMemcpy(EnergyBendingArray, d_EnergyBendingArray, numberSites * sizeof(double), cudaMemcpyDeviceToHost);
	}

	/* add energies of sites to total energy */
	addOneKindEnergy(&EnergyExchangeNorm, EnergyExchangeArray);
	addOneKindEnergy(&EnergyAnisotropyNormAxisK1, EnergyAnisotropyArrayAxisK1);
	addOneKindEnergy(&EnergyAnisotropyNormAxisK2, EnergyAnisotropyArrayAxisK2);
	addOneKindEnergy(&EnergyAnisotropyNormAxisK3, EnergyAnisotropyArrayAxisK3);
	addOneKindEnergy(&EnergyDipoleNorm, EnergyDipoleArray);
	addOneKindEnergy(&EnergyExternalNorm, EnergyExternalArray);
	/* it is only for cubic system */
	if (userVars.latticeType == LATTICE_CUBIC) {
		addOneKindEnergy(&EnergyDMIAxis1Norm, EnergyDMIAxis1Array);
		addOneKindEnergy(&EnergyDMIAxis2Norm, EnergyDMIAxis2Array);
		addOneKindEnergy(&EnergyDMIAxis3Norm, EnergyDMIAxis3Array);
	}
	/* it is only for system with triangular lattice */
	if (userVars.latticeType == LATTICE_TRIANGULAR) {
		addOneKindEnergy(&EnergyDMITriangleBulkNorm, EnergyDMITriangleBulkArray);
		addOneKindEnergy(&EnergyDMITriangleInterfaceNorm, EnergyDMITriangleInterfaceArray);
		addOneKindEnergy(&EnergyStretchingNorm, EnergyStretchingArray);
		addOneKindEnergy(&EnergyBendingNorm, EnergyBendingArray);
	}

	/* divide energy on 2 (except energy of external magnetic field) */
	EnergyExchangeNorm *= 0.5;
	EnergyAnisotropyNormAxisK1 *= 0.5;
	EnergyAnisotropyNormAxisK2 *= 0.5;
	EnergyAnisotropyNormAxisK3 *= 0.5;
	EnergyDipoleNorm *= 0.5;
	EnergyDMIAxis1Norm *= 0.5;
	EnergyDMIAxis2Norm *= 0.5;
	EnergyDMIAxis3Norm *= 0.5;
	EnergyDMITriangleInterfaceNorm *= 0.5;
	EnergyDMITriangleBulkNorm *= 0.5;
	EnergyStretchingNorm *= 0.5;
	EnergyBendingNorm *= 0.5;
	/* calculate total normalized energy */
	EnergyOfSystemNorm = EnergyExchangeNorm
	                     + EnergyAnisotropyNormAxisK1
	                     + EnergyAnisotropyNormAxisK2
	                     + EnergyAnisotropyNormAxisK3
	                     + EnergyDipoleNorm
	                     + EnergyDMIAxis1Norm
	                     + EnergyDMIAxis2Norm
	                     + EnergyDMIAxis3Norm
	                     + EnergyDMITriangleInterfaceNorm
	                     + EnergyDMITriangleBulkNorm
	                     + EnergyStretchingNorm
	                     + EnergyBendingNorm
	                     + EnergyExternalNorm;

}

/* function to normalize all components of one magnetic moment */
__global__
void normalizeVectorsSpin(double *m, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* check boundary condition for threads */
	if (indexLattice < numberSites) {

		/* find length of magnetic vector */
		double lengthVector = sqrt(m[curIndex] * m[curIndex] +
		                           m[curIndex + 1] * m[curIndex + 1] +
		                           m[curIndex + 2] * m[curIndex + 2]);

		/* normalize vector of magnetic moment */
		if (lengthVector > LENG_ZERO) {
			m[curIndex] /= lengthVector;
			m[curIndex + 1] /= lengthVector;
			m[curIndex + 2] /= lengthVector;
		}

	}

}

extern "C"
void NormalizeVectorsCUDA(void) {

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;

	/* set the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* normalize vectors */
	normalizeVectorsSpin<<<cores,threads>>>(d_lattice, numberSites);
	cudaDeviceSynchronize();

}

/* function to calculate "small changes" (magnetic) of one site */
__global__
void calculateSmallChangesMagnSite(double **k, double *dm5, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* constants for Runge-Kutta-Fehlberg */
	const double c0 = 16.0 / 135.0;
	const double c1 = 0.0;
	const double c2 = 6656.0 / 12825.0;
	const double c3 = 28561.0 / 56430.0;
	const double c4 = -9.0 / 50.0;
	const double c5 = 2.0 / 55.0;

	/* check boundary condition for threads */
	if (indexLattice < numberSites) {

		/* small changes of the fifth order */
		dm5[curIndex] = c0 * k[0][curIndex] + c1 * k[1][curIndex] + c2 * k[2][curIndex] +
		                c3 * k[3][curIndex] + c4 * k[4][curIndex] + c5 * k[5][curIndex];
		dm5[curIndex + 1] = c0 * k[0][curIndex + 1] + c1 * k[1][curIndex + 1] + c2 * k[2][curIndex + 1] +
		                    c3 * k[3][curIndex + 1] + c4 * k[4][curIndex + 1] + c5 * k[5][curIndex + 1];
		dm5[curIndex + 2] = c0 * k[0][curIndex + 2] + c1 * k[1][curIndex + 2] + c2 * k[2][curIndex + 2] +
		                    c3 * k[3][curIndex + 2] + c4 * k[4][curIndex + 2] + c5 * k[5][curIndex + 2];

		//printf("dm4 %li %f %f %f\n", indexLattice, dm4[curIndex], dm4[curIndex + 1], dm4[curIndex + 2]);
		//printf("dm5 %li %f %f %f\n", indexLattice, dm5[curIndex], dm5[curIndex + 1], dm5[curIndex + 2]);
		//printf("k0 %li %f %f %f\n", indexLattice, k[0][curIndex], k[0][curIndex + 1], k[0][curIndex + 2]);
		//printf("k2 %li %f %f %f\n", indexLattice, k[2][curIndex], k[2][curIndex + 2], k[2][curIndex + 2]);

	}

}

extern "C"
void calculateSmallChangesMagnCUDA(double **k, double *dm5) {

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;

	/* set the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* find "small changes" (magnetic) */
	calculateSmallChangesMagnSite<<<cores,threads>>>(k, dm5, numberSites);

}

/* function to calculate "small changes" (coordinates and velocities) of one site */
__global__
void calculateSmallChangesOtherSite(double **k, double *dm5, long spinsNumber, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* constants for Runge-Kutta-Fehlberg */
	const double c0 = 16.0 / 135.0;
	const double c1 = 0.0;
	const double c2 = 6656.0 / 12825.0;
	const double c3 = 28561.0 / 56430.0;
	const double c4 = -9.0 / 50.0;
	const double c5 = 2.0 / 55.0;

	/* check boundary condition for threads */
	if (indexLattice < numberSites) {

		/* small changes of the fifth order */

		/* coordinates */
		dm5[curIndex + spinsNumber] = c0 * k[0][curIndex + spinsNumber] + c1 * k[1][curIndex + spinsNumber] + c2 * k[2][curIndex + spinsNumber] +
		                              c3 * k[3][curIndex + spinsNumber] + c4 * k[4][curIndex + spinsNumber] + c5 * k[5][curIndex + spinsNumber];
		dm5[curIndex + spinsNumber + 1] = c0 * k[0][curIndex + spinsNumber + 1] + c1 * k[1][curIndex + spinsNumber + 1] + c2 * k[2][curIndex + spinsNumber + 1] +
		                                  c3 * k[3][curIndex + spinsNumber + 1] + c4 * k[4][curIndex + spinsNumber + 1] + c5 * k[5][curIndex + spinsNumber + 1];
		dm5[curIndex + spinsNumber + 2] = c0 * k[0][curIndex + spinsNumber + 2] + c1 * k[1][curIndex + spinsNumber + 2] + c2 * k[2][curIndex + spinsNumber + 2] +
		                                  c3 * k[3][curIndex + spinsNumber + 2] + c4 * k[4][curIndex + spinsNumber + 2] + c5 * k[5][curIndex + spinsNumber + 2];
		/* velocities */
		dm5[curIndex + 2 * spinsNumber] = c0 * k[0][curIndex + 2 * spinsNumber] + c1 * k[1][curIndex + 2 * spinsNumber] + c2 * k[2][curIndex + 2 * spinsNumber] +
		                                  c3 * k[3][curIndex + 2 * spinsNumber] + c4 * k[4][curIndex + 2 * spinsNumber] + c5 * k[5][curIndex + 2 * spinsNumber];
		dm5[curIndex + 2 * spinsNumber + 1] = c0 * k[0][curIndex + 2 * spinsNumber + 1] + c1 * k[1][curIndex + 2 * spinsNumber + 1] + c2 * k[2][curIndex + 2 * spinsNumber + 1] +
		                                      c3 * k[3][curIndex + 2 * spinsNumber + 1] + c4 * k[4][curIndex + 2 * spinsNumber + 1] + c5 * k[5][curIndex + 2 * spinsNumber + 1];
		dm5[curIndex + 2 * spinsNumber + 2] = c0 * k[0][curIndex + 2 * spinsNumber + 2] + c1 * k[1][curIndex + 2 * spinsNumber + 2] + c2 * k[2][curIndex + 2 * spinsNumber + 2] +
		                                      c3 * k[3][curIndex + 2 * spinsNumber + 2] + c4 * k[4][curIndex + 2 * spinsNumber + 2] + c5 * k[5][curIndex + 2 * spinsNumber + 2];

	}

}

extern "C"
void calculateSmallChangesOtherCUDA(double **k, double *dm5) {

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;
	/* number of components of spins in system */
	long spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* set the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* find "small changes" (coordinates and velocities) */
	calculateSmallChangesOtherSite<<<cores,threads>>>(k, dm5, spinsNumber, numberSites);
	cudaDeviceSynchronize();

}

/* function to add "small changes" (magnetic moments) of one site */
__global__
void addSmallChangesMagnOne(double *m, double *dm, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* check boundary condition for threads */
	if (indexLattice < numberSites) {

		/* add "small changes" for magnetic moments */
		m[curIndex] += dm[curIndex];
		m[curIndex + 1] += dm[curIndex + 1];
		m[curIndex + 2] += dm[curIndex + 2];

	}

}

/* function to add "small changes" (coordinates and velocities) of one site */
__global__
void addSmallChangesOtherOne(double *m, double *dm, long spinsNumber, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* check boundary condition for threads */
	if (indexLattice < numberSites) {

		/* add "small changes" for coordinates */
		m[curIndex + spinsNumber] += dm[curIndex + spinsNumber];
		m[curIndex + spinsNumber + 1] += dm[curIndex + spinsNumber + 1];
		m[curIndex + spinsNumber + 2] += dm[curIndex + spinsNumber + 2];

		/* add "small changes" for velocity */
		m[curIndex + 2 * spinsNumber] += dm[curIndex + 2 * spinsNumber];
		m[curIndex + 2 * spinsNumber + 1] += dm[curIndex + 2 * spinsNumber + 1];
		m[curIndex + 2 * spinsNumber + 2] += dm[curIndex + 2 * spinsNumber + 2];

	}

}

extern "C"
void addSmallChangesCUDA(double *m, double *dm5) {

	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;

	/* set the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	//showArray<<<cores,threads>>>(dm5, numberSites);
	//cudaDeviceSynchronize();
	/* add "small changes" (magnetic moments) */
	addSmallChangesMagnOne<<<cores,threads>>>(m, dm5, numberSites);
	/* it is only for flexible system */
	if (userVars.flexOnOff == FLEX_ON) {
		/* add "small changes" (coordinates and velocities) */
		addSmallChangesOtherOne<<<cores,threads>>>(m, dm5, spinsNumber, numberSites);
	}
	cudaDeviceSynchronize();

}

/* function to add "small changes" (magnetic moments) of one site to new array */
__global__
void addSmallChangesToArrayMagnOne(double *latticeNew, double *lattice, double *dm,
								   long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* check boundary condition for threads */
	if (indexLattice < numberSites) {

		/* add "small changes" for magnetic moments */
		latticeNew[curIndex] = lattice[curIndex] + dm[curIndex];
		latticeNew[curIndex + 1] = lattice[curIndex + 1] + dm[curIndex + 1];
		latticeNew[curIndex + 2] = lattice[curIndex + 2] + dm[curIndex + 2];

	}

}

/* function to add "small changes" (coordinates and velocities) of one site to new array */
__global__
void addSmallChangesToArrayOtherOne(double *latticeNew, double *lattice, double *dm,
									long spinsNumber, long numberSites) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* check boundary condition for threads */
	if (indexLattice < numberSites) {

		/* add "small changes" for coordinates */
		latticeNew[curIndex + spinsNumber] = lattice[curIndex + spinsNumber] +
				dm[curIndex + spinsNumber];
		latticeNew[curIndex + spinsNumber + 1] = lattice[curIndex + spinsNumber + 1] +
				dm[curIndex + spinsNumber + 1];
		latticeNew[curIndex + spinsNumber + 2] = lattice[curIndex + spinsNumber + 2] +
				dm[curIndex + spinsNumber + 2];

		/* add "small changes" for velocity */
		latticeNew[curIndex + 2 * spinsNumber] = lattice[curIndex + 2 * spinsNumber] +
			   dm[curIndex + 2 * spinsNumber];
		latticeNew[curIndex + 2 * spinsNumber + 1] = lattice[curIndex + 2 * spinsNumber + 1] +
				dm[curIndex + 2 * spinsNumber + 1];
		latticeNew[curIndex + 2 * spinsNumber + 2] = lattice[curIndex + 2 * spinsNumber + 2] +
				dm[curIndex + 2 * spinsNumber + 2];

	}

}

extern "C"
void addSmallChangesToArrayCUDA(double *latticeNew, double *lattice, double *dm) {

	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;

	/* set the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	//showArray<<<cores,threads>>>(dm5, numberSites);
	//cudaDeviceSynchronize();
	/* add "small changes" (magnetic moments) */
	addSmallChangesToArrayMagnOne<<<cores,threads>>>(latticeNew, lattice, dm, numberSites);
	/* it is only for flexible system */
	if (userVars.flexOnOff == FLEX_ON) {
		/* add "small changes" (coordinates and velocities) */
		addSmallChangesToArrayOtherOne<<<cores,threads>>>(latticeNew, lattice, dm, spinsNumber, numberSites);
	}
	cudaDeviceSynchronize();

}

double CalculateMaxDmDtCUDA(double *dm, double h) {

	/* size of array for changes */
	long size = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	/* max change of magnetic moments */
	double Max;

	/* copy array from device to main memory */
	cudaMemcpy(DmDt, dm, sizeof(double) * size, cudaMemcpyDeviceToHost);
	
	/* go through all values to find maximum */
	Max = fabs(DmDt[0]);
	for (size_t curIndex = 0; curIndex <= size - 3; curIndex += 3) {
		if (fabs(DmDt[curIndex]) > Max) {
			Max = fabs(DmDt[curIndex]);
		} 
		if (fabs(DmDt[curIndex + 1]) > Max) {
			Max = fabs(DmDt[curIndex + 1]);
		}
		if (fabs(DmDt[curIndex + 2]) > Max) {
			Max = fabs(DmDt[curIndex + 2]);
		}
	}


	/* return maximal magnetic inclination */
	return Max / h;


}

double CalculateMaxDrDtCUDA(double *dm, double h) {

	/* size of array for changes */
	long size = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	/* max change of magnetic moments */
	double Max;

	/* copy array from device to main memory */
	cudaMemcpy(DrDt, dm + size * sizeof(double), sizeof(double) * size, cudaMemcpyDeviceToHost);

	/* go through all values to find maximum */
	Max = fabs(DrDt[0]);
	for (size_t curIndex = 0; curIndex <= size - 3; curIndex += 3) {
		if (fabs(DrDt[curIndex]) > Max) {
			Max = DrDt[curIndex];		
		}
		if (fabs(DrDt[curIndex + 1]) > Max) {
			Max = DrDt[curIndex + 1];
		}
		if (fabs(DrDt[curIndex + 2]) > Max) {
			Max = DrDt[curIndex + 2];
		}
	}

	/* return maximal changes of coordinates */
	return Max / h;

}


#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "integration.h"
#include "../interactions/interactions.h"
#include "../interactions/externalField/externalField.h"
#include "../utils/utils.h"
#include "../utils/synchronize.h"

void CallFunction(double *k, double *m, size_t amount, double t, double h,
                  void (*pointerFunc)(double *, double *, size_t, double, double h)) {

	/* call function that contains equations for Cauchy problem */
	(*pointerFunc)(k, m, amount, t, h);

}

void CalculateGrid0(double *k, double *m, double t, size_t amount, double h,
                    void (*pointerFunc)(double *, double *, size_t, double, double)) {

	/* calculate zero grid of intermediate values for Runge-Kutta-Fehlberg */
	CallFunction(k, m, amount, t, h, pointerFunc);

}

void CalculateGrid1(double *k, double *kpast1, double *m, double t, size_t amount, double h,
                    void (*pointerFunc)(double *, double *, size_t, double, double), double * mCh) {

	/* create array for variables of the fifth grid*/
	//double mCh[amount];
	/* current index */
	long curIndex;

	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* go through all sites */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

				/* find current index */
				curIndex = IDXmg(i1, i2, i3);

				/* calculate the first grid of intermediate values for Runge-Kutta-Fehlberg (magnetic moments) */
				mCh[curIndex] = m[curIndex] +
				                (1.0 / 4.0) * kpast1[curIndex];
				mCh[curIndex + 1] = m[curIndex + 1] +
				                    (1.0 / 4.0) * kpast1[curIndex + 1];
				mCh[curIndex + 2] = m[curIndex + 2] +
				                    (1.0 / 4.0) * kpast1[curIndex + 2];

			}
		}
	}

	/* only for flexible system */
	if (userVars.flexOnOff == FLEX_ON) {

		/* go through all sites */
		int i1, i2, i3;
		for (i1 = begin; i1 <= end; i1++) {
			for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

					/* find current index */
					curIndex = IDXmg(i1, i2, i3);

					/* calculate the first grid of intermediate values for Runge-Kutta-Fehlberg (coordinates) */
					mCh[curIndex + spinsNumber] = m[curIndex + spinsNumber] +
					                              (1.0 / 4.0) * kpast1[curIndex + spinsNumber];
					mCh[curIndex + spinsNumber + 1] = m[curIndex + spinsNumber + 1] +
					                                  (1.0 / 4.0) * kpast1[curIndex + spinsNumber + 1];
					mCh[curIndex + spinsNumber + 2] = m[curIndex + spinsNumber + 2] +
					                                  (1.0 / 4.0) * kpast1[curIndex + spinsNumber + 2];

					/* calculate the first grid of intermediate values for Runge-Kutta-Fehlberg (velocity) */
					mCh[curIndex + 2 * spinsNumber] = m[curIndex + 2 * spinsNumber] +
					                                  (1.0 / 4.0) * kpast1[curIndex + 2 * spinsNumber];
					mCh[curIndex + 2 * spinsNumber + 1] = m[curIndex + 2 * spinsNumber + 1] +
					                                      (1.0 / 4.0) * kpast1[curIndex + 2 * spinsNumber + 1];
					mCh[curIndex + 2 * spinsNumber + 2] = m[curIndex + 2 * spinsNumber + 2] +
					                                      (1.0 / 4.0) * kpast1[curIndex + 2 * spinsNumber + 2];

				}
			}
		}

	}

	CallFunction(k, mCh, amount, t + (1.0 / 4.0) * h, h, pointerFunc);

}

void CalculateGrid2(double *k, double *kpast1, double *kpast2, double *m, double t, size_t amount, double h,
                    void (*pointerFunc)(double *, double *, size_t, double, double), double * mCh) {

	/* create array for variables of the fifth grid*/
	//double mCh[amount];
	/* current index */
	long curIndex;

	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* go through all sites */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

				/* find current index */
				curIndex = IDXmg(i1, i2, i3);

				/* calculate the second grid of intermediate values for Runge-Kutta-Fehlberg (magnetic moments) */
				mCh[curIndex] = m[curIndex] +
				                (9.0 / 32.0) * kpast1[curIndex] +
				                (3.0 / 32.0) * kpast2[curIndex];
				mCh[curIndex + 1] = m[curIndex + 1] +
				                    (9.0 / 32.0) * kpast1[curIndex + 1] +
				                    (3.0 / 32.0) * kpast2[curIndex + 1];
				mCh[curIndex + 2] = m[curIndex + 2] +
				                    (9.0 / 32.0) * kpast1[curIndex + 2] +
				                    (3.0 / 32.0) * kpast2[curIndex + 2];

			}
		}
	}

	/* only for flexible system */
	if (userVars.flexOnOff == FLEX_ON) {

		/* go through all sites */
		int i1, i2, i3;
		for (i1 = begin; i1 <= end; i1++) {
			for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

					/* find current index */
					curIndex = IDXmg(i1, i2, i3);

					/* calculate the second grid of intermediate values for Runge-Kutta-Fehlberg (coordinates) */
					mCh[curIndex + spinsNumber] = m[curIndex + spinsNumber] +
					                              (9.0 / 32.0) * kpast1[curIndex + spinsNumber] +
					                              (3.0 / 32.0) * kpast2[curIndex + spinsNumber];
					mCh[curIndex + spinsNumber + 1] = m[curIndex + spinsNumber + 1] +
					                                  (9.0 / 32.0) * kpast1[curIndex + spinsNumber + 1] +
					                                  (3.0 / 32.0) * kpast2[curIndex + spinsNumber + 1];
					mCh[curIndex + spinsNumber + 2] = m[curIndex + spinsNumber + 2] +
					                                  (9.0 / 32.0) * kpast1[curIndex + spinsNumber + 2] +
					                                  (3.0 / 32.0) * kpast2[curIndex + spinsNumber + 2];

					/* calculate the second grid of intermediate values for Runge-Kutta-Fehlberg (velocity) */
					mCh[curIndex + 2 * spinsNumber] = m[curIndex + 2 * spinsNumber] +
					                                  (9.0 / 32.0) * kpast1[curIndex + 2 * spinsNumber] +
					                                  (3.0 / 32.0) * kpast2[curIndex + 2 * spinsNumber];
					mCh[curIndex + 2 * spinsNumber + 1] = m[curIndex + 2 * spinsNumber + 1] +
					                                      (9.0 / 32.0) * kpast1[curIndex + 2 * spinsNumber + 1] +
					                                      (3.0 / 32.0) * kpast2[curIndex + 2 * spinsNumber + 1];
					mCh[curIndex + 2 * spinsNumber + 2] = m[curIndex + spinsNumber + 2] +
					                                      (9.0 / 32.0) * kpast1[curIndex + 2 * spinsNumber + 2] +
					                                      (3.0 / 32.0) * kpast2[curIndex + 2 * spinsNumber + 2];

				}
			}
		}

	}

	CallFunction(k, mCh, amount, t + (3.0 / 8.0) * h, h, pointerFunc);

}

void CalculateGrid3(double *k, double *kpast1, double *kpast2, double *kpast3, double *m, double t, size_t amount, double h,
                    void (*pointerFunc)(double *, double *, size_t, double, double), double * mCh) {

	/* create array for variables of the fifth grid*/
	//double mCh[amount];
	/* current index */
	long curIndex;

	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* go through all sites */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

				/* find current index */
				curIndex = IDXmg(i1, i2, i3);

				/* calculate the third grid of intermediate values for Runge-Kutta-Fehlberg (magnetic moments) */
				mCh[curIndex] = m[curIndex] +
				                (7296.0 / 2197.0) * kpast1[curIndex] -
				                (7200.0 / 2197.0) * kpast2[curIndex] +
				                (1932.0 / 2197.0) * kpast3[curIndex];
				mCh[curIndex + 1] = m[curIndex + 1] +
				                    (7296.0 / 2197.0) * kpast1[curIndex + 1] -
				                    (7200.0 / 2197.0) * kpast2[curIndex + 1] +
				                    (1932.0 / 2197.0) * kpast3[curIndex + 1];
				mCh[curIndex + 2] = m[curIndex + 2] +
				                    (7296.0 / 2197.0) * kpast1[curIndex + 2] -
				                    (7200.0 / 2197.0) * kpast2[curIndex + 2] +
				                    (1932.0 / 2197.0) * kpast3[curIndex + 2];

			}
		}
	}

	/* only for flexible system */
	if (userVars.flexOnOff == FLEX_ON) {

		/* go through all sites */
		int i1, i2, i3;
		for (i1 = begin; i1 <= end; i1++) {
			for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

					/* find current index */
					curIndex = IDXmg(i1, i2, i3);

					/* calculate the third grid of intermediate values for Runge-Kutta-Fehlberg (coordinates) */
					mCh[curIndex + spinsNumber] = m[curIndex + spinsNumber] +
					                              (7296.0 / 2197.0) * kpast1[curIndex + spinsNumber] -
					                              (7200.0 / 2197.0) * kpast2[curIndex + spinsNumber] +
					                              (1932.0 / 2197.0) * kpast3[curIndex + spinsNumber];
					mCh[curIndex + spinsNumber + 1] = m[curIndex + spinsNumber + 1] +
					                                  (7296.0 / 2197.0) * kpast1[curIndex + spinsNumber + 1] -
					                                  (7200.0 / 2197.0) * kpast2[curIndex + spinsNumber + 1] +
					                                  (1932.0 / 2197.0) * kpast3[curIndex + spinsNumber + 1];
					mCh[curIndex + spinsNumber + 2] = m[curIndex + spinsNumber + 2] +
					                                  (7296.0 / 2197.0) * kpast1[curIndex + spinsNumber + 2] -
					                                  (7200.0 / 2197.0) * kpast2[curIndex + spinsNumber + 2] +
					                                  (1932.0 / 2197.0) * kpast3[curIndex + spinsNumber + 2];

					/* calculate the third grid of intermediate values for Runge-Kutta-Fehlberg (velocity) */
					mCh[curIndex + 2 * spinsNumber] = m[curIndex + 2 * spinsNumber] +
					                                  (7296.0 / 2197.0) * kpast1[curIndex + 2 * spinsNumber] -
					                                  (7200.0 / 2197.0) * kpast2[curIndex + 2 * spinsNumber] +
					                                  (1932.0 / 2197.0) * kpast3[curIndex + 2 * spinsNumber];
					mCh[curIndex + 2 * spinsNumber + 1] = m[curIndex + 2 * spinsNumber + 1] +
					                                      (7296.0 / 2197.0) * kpast1[curIndex + 2 * spinsNumber + 1] -
					                                      (7200.0 / 2197.0) * kpast2[curIndex + 2 * spinsNumber + 1] +
					                                      (1932.0 / 2197.0) * kpast3[curIndex + 2 * spinsNumber + 1];
					mCh[curIndex + 2 * spinsNumber + 2] = m[curIndex + 2 * spinsNumber + 2] +
					                                      (7296.0 / 2197.0) * kpast1[curIndex + 2 * spinsNumber + 2] -
					                                      (7200.0 / 2197.0) * kpast2[curIndex + 2 * spinsNumber + 2] +
					                                      (1932.0 / 2197.0) * kpast3[curIndex + 2 * spinsNumber + 2];

				}
			}
		}

	}

	CallFunction(k, mCh, amount, t + (12.0 / 13.0) * h, h, pointerFunc);

}

void CalculateGrid4(double *k, double *kpast1, double *kpast2, double *kpast3, double *kpast4, double *m, double t,
                    size_t amount, double h, void (*pointerFunc)(double *, double *, size_t, double, double), double * mCh) {

	/* create array for variables of the fifth grid*/
	//double mCh[amount];
	/* current index */
	long curIndex;

	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* go through all sites */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

				/* find current index */
				curIndex = IDXmg(i1, i2, i3);

				/* calculate the fourth grid of intermediate values for Runge-Kutta-Fehlberg (magnetic moments) */
				mCh[curIndex] = m[curIndex] -
				                (845.0 / 4104.0) * kpast1[curIndex] +
				                (3680.0 / 513.0) * kpast2[curIndex] -
				                8.0 * kpast3[curIndex] +
				                (439.0 / 216.0) * kpast4[curIndex];
				mCh[curIndex + 1] = m[curIndex + 1] -
				                    (845.0 / 4104.0) * kpast1[curIndex + 1] +
				                    (3680.0 / 513.0) * kpast2[curIndex + 1] -
				                    8.0 * kpast3[curIndex + 1] +
				                    (439.0 / 216.0) * kpast4[curIndex + 1];
				mCh[curIndex + 2] = m[curIndex + 2] -
				                    (845.0 / 4104.0) * kpast1[curIndex + 2] +
				                    (3680.0 / 513.0) * kpast2[curIndex + 2] -
				                    8.0 * kpast3[curIndex + 2] +
				                    (439.0 / 216.0) * kpast4[curIndex + 2];

			}
		}
	}

	/* only for flexible system */
	if (userVars.flexOnOff == FLEX_ON) {

		/* go through all sites */
		int i1, i2, i3;
		for (i1 = begin; i1 <= end; i1++) {
			for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

					/* find current index */
					curIndex = IDXmg(i1, i2, i3);

					/* calculate the fourth grid of intermediate values for Runge-Kutta-Fehlberg (coordinates) */
					mCh[curIndex + spinsNumber] = m[curIndex + spinsNumber] -
					                              (845.0 / 4104.0) * kpast1[curIndex + spinsNumber] +
					                              (3680.0 / 513.0) * kpast2[curIndex + spinsNumber] -
					                              8.0 * kpast3[curIndex + spinsNumber] +
					                              (439.0 / 216.0) * kpast4[curIndex + spinsNumber];
					mCh[curIndex + spinsNumber + 1] = m[curIndex + spinsNumber + 1] -
					                                  (845.0 / 4104.0) * kpast1[curIndex + spinsNumber + 1] +
					                                  (3680.0 / 513.0) * kpast2[curIndex + spinsNumber + 1] -
					                                  8.0 * kpast3[curIndex + spinsNumber + 1] +
					                                  (439.0 / 216.0) * kpast4[curIndex + spinsNumber + 1];
					mCh[curIndex + spinsNumber + 2] = m[curIndex + spinsNumber + 2] -
					                                  (845.0 / 4104.0) * kpast1[curIndex + spinsNumber + 2] +
					                                  (3680.0 / 513.0) * kpast2[curIndex + spinsNumber + 2] -
					                                  8.0 * kpast3[curIndex + spinsNumber + 2] +
					                                  (439.0 / 216.0) * kpast4[curIndex + spinsNumber + 2];

					/* calculate the fourth grid of intermediate values for Runge-Kutta-Fehlberg (velocity) */
					mCh[curIndex + 2 * spinsNumber] = m[curIndex + 2 * spinsNumber] -
					                                  (845.0 / 4104.0) * kpast1[curIndex + 2 * spinsNumber] +
					                                  (3680.0 / 513.0) * kpast2[curIndex + 2 * spinsNumber] -
					                                  8.0 * kpast3[curIndex + 2 * spinsNumber] +
					                                  (439.0 / 216.0) * kpast4[curIndex + 2 * spinsNumber];
					mCh[curIndex + 2 * spinsNumber + 1] = m[curIndex + 2 * spinsNumber + 1] -
					                                      (845.0 / 4104.0) * kpast1[curIndex + 2 * spinsNumber + 1] +
					                                      (3680.0 / 513.0) * kpast2[curIndex + 2 * spinsNumber + 1] -
					                                      8.0 * kpast3[curIndex + 2 * spinsNumber + 1] +
					                                      (439.0 / 216.0) * kpast4[curIndex + 2 * spinsNumber + 1];
					mCh[curIndex + 2 * spinsNumber + 2] = m[curIndex + 2 * spinsNumber + 2] -
					                                      (845.0 / 4104.0) * kpast1[curIndex + 2 * spinsNumber + 2] +
					                                      (3680.0 / 513.0) * kpast2[curIndex + 2 * spinsNumber + 2] -
					                                      8.0 * kpast3[curIndex + 2 * spinsNumber + 2] +
					                                      (439.0 / 216.0) * kpast4[curIndex + 2 * spinsNumber + 2];

				}
			}
		}

	}

	CallFunction(k, mCh, amount, t + h, h, pointerFunc);

}

void CalculateGrid5(double *k, double *kpast1, double *kpast2, double *kpast3, double *kpast4, double *kpast5, double *m,
                    double t, size_t amount, double h, void (*pointerFunc)(double *, double *, size_t, double, double), double * mCh) {

	/* create array for variables of the fifth grid*/
	//double mCh[amount];
	/* current index */
	long curIndex;

	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* go through all sites */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

				/* find current index */
				curIndex = IDXmg(i1, i2, i3);

				/* calculate the fifth grid of intermediate values for Runge-Kutta-Fehlberg (magnetic moments) */
				mCh[curIndex] = m[curIndex] -
				                (11.0 / 40.0) * kpast1[curIndex] +
				                (1859.0 / 4104.0) * kpast2[curIndex] -
				                (3544.0 / 2565.0) * kpast3[curIndex] +
				                2.0 * kpast4[curIndex] -
				                (8.0 / 27.0) * kpast5[curIndex];
				mCh[curIndex + 1] = m[curIndex + 1] -
				                    (11.0 / 40.0) * kpast1[curIndex + 1] +
				                    (1859.0 / 4104.0) * kpast2[curIndex + 1] -
				                    (3544.0 / 2565.0) * kpast3[curIndex + 1] +
				                    2.0 * kpast4[curIndex + 1] -
				                    (8.0 / 27.0) * kpast5[curIndex + 1];
				mCh[curIndex + 2] = m[curIndex + 2] -
				                    (11.0 / 40.0) * kpast1[curIndex + 2] +
				                    (1859.0 / 4104.0) * kpast2[curIndex + 2] -
				                    (3544.0 / 2565.0) * kpast3[curIndex + 2] +
				                    2.0 * kpast4[curIndex + 2] -
				                    (8.0 / 27.0) * kpast5[curIndex + 2];

			}
		}
	}

	/* only for flexible system */
	if (userVars.flexOnOff == FLEX_ON) {

		/* go through all sites */
		int i1, i2, i3;
		for (i1 = begin; i1 <= end; i1++) {
			for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

					/* find current index */
					curIndex = IDXmg(i1, i2, i3);

					/* calculate the fifth grid of intermediate values for Runge-Kutta-Fehlberg (coordinates) */
					mCh[curIndex + spinsNumber] = m[curIndex + spinsNumber] -
					                              (11.0 / 40.0) * kpast1[curIndex + spinsNumber] +
					                              (1859.0 / 4104.0) * kpast2[curIndex + spinsNumber] -
					                              (3544.0 / 2565.0) * kpast3[curIndex + spinsNumber] +
					                              2.0 * kpast4[curIndex + spinsNumber] -
					                              (8.0 / 27.0) * kpast5[curIndex + spinsNumber];
					mCh[curIndex + spinsNumber + 1] = m[curIndex + spinsNumber + 1] -
					                                  (11.0 / 40.0) * kpast1[curIndex + spinsNumber + 1] +
					                                  (1859.0 / 4104.0) * kpast2[curIndex + spinsNumber + 1] -
					                                  (3544.0 / 2565.0) * kpast3[curIndex + spinsNumber + 1] +
					                                  2.0 * kpast4[curIndex + spinsNumber + 1] -
					                                  (8.0 / 27.0) * kpast5[curIndex + spinsNumber + 1];
					mCh[curIndex + spinsNumber + 2] = m[curIndex + spinsNumber + 2] -
					                                  (11.0 / 40.0) * kpast1[curIndex + spinsNumber + 2] +
					                                  (1859.0 / 4104.0) * kpast2[curIndex + spinsNumber + 2] -
					                                  (3544.0 / 2565.0) * kpast3[curIndex + spinsNumber + 2] +
					                                  2.0 * kpast4[curIndex + spinsNumber + 2] -
					                                  (8.0 / 27.0) * kpast5[curIndex + spinsNumber + 2];

					/* calculate the fifth grid of intermediate values for Runge-Kutta-Fehlberg (velocities) */
					mCh[curIndex + 2 * spinsNumber] = m[curIndex + 2 * spinsNumber] -
					                                  (11.0 / 40.0) * kpast1[curIndex + 2 * spinsNumber] +
					                                  (1859.0 / 4104.0) * kpast2[curIndex + 2 * spinsNumber] -
					                                  (3544.0 / 2565.0) * kpast3[curIndex + 2 * spinsNumber] +
					                                  2.0 * kpast4[curIndex + 2 * spinsNumber] -
					                                  (8.0 / 27.0) * kpast5[curIndex + 2 * spinsNumber];
					mCh[curIndex + 2 * spinsNumber + 1] = m[curIndex + 2 * spinsNumber + 1] -
					                                      (11.0 / 40.0) * kpast1[curIndex + 2 * spinsNumber + 1] +
					                                      (1859.0 / 4104.0) * kpast2[curIndex + 2 * spinsNumber + 1] -
					                                      (3544.0 / 2565.0) * kpast3[curIndex + 2 * spinsNumber + 1] +
					                                      2.0 * kpast4[curIndex + 2 * spinsNumber + 1] -
					                                      (8.0 / 27.0) * kpast5[curIndex + 2 * spinsNumber + 1];
					mCh[curIndex + 2 * spinsNumber + 2] = m[curIndex + 2 * spinsNumber + 2] -
					                                      (11.0 / 40.0) * kpast1[curIndex + 2 * spinsNumber + 2] +
					                                      (1859.0 / 4104.0) * kpast2[curIndex + 2 * spinsNumber + 2] -
					                                      (3544.0 / 2565.0) * kpast3[curIndex + 2 * spinsNumber + 2] +
					                                      2.0 * kpast4[curIndex + 2 * spinsNumber + 2] -
					                                      (8.0 / 27.0) * kpast5[curIndex + 2 * spinsNumber + 2];

				}
			}
		}

	}

	CallFunction(k, mCh, amount, t + (1.0 / 2.0) * h, h, pointerFunc);

}

/* function to synchronize all energies (for MPI processes) */
void synchronizeAllEnergies(void) {

	/* synchronize all kinds of energy */
	synchronizeScalarValueAdd(&EnergyExchangeNorm);
	synchronizeScalarValueAdd(&EnergyAnisotropyNormAxisK1);
	synchronizeScalarValueAdd(&EnergyAnisotropyNormAxisK2);
	synchronizeScalarValueAdd(&EnergyAnisotropyNormAxisK3);
	synchronizeScalarValueAdd(&EnergyDipoleNorm);
	synchronizeScalarValueAdd(&EnergyDMIAxis1Norm);
	synchronizeScalarValueAdd(&EnergyDMIAxis2Norm);
	synchronizeScalarValueAdd(&EnergyDMIAxis3Norm);
	synchronizeScalarValueAdd(&EnergyDMITriangleInterfaceNorm);
	synchronizeScalarValueAdd(&EnergyDMITriangleBulkNorm);
	synchronizeScalarValueAdd(&EnergyStretchingNorm);
	synchronizeScalarValueAdd(&EnergyBendingNorm);
	synchronizeScalarValueAdd(&EnergyExternalNorm);

}

void addAllEnergies(void) {

	/* synchronize energies */
	synchronizeAllEnergies();
	/* divide energy on 2 (except energy of external magnetic field) */
	EnergyExchangeNorm *= 0.5;
	EnergyAnisotropyNormAxisK1 *= 0.5;
	EnergyAnisotropyNormAxisK2 *= 0.5;
	EnergyAnisotropyNormAxisK3 *= 0.5;
	EnergyDipoleNorm *= 0.5;
	EnergyDMIAxis1Norm *= 0.5;
	EnergyDMIAxis2Norm *= 0.5;
	EnergyDMIAxis3Norm *= 0.5;
	EnergyDMITriangleInterfaceNorm *= 0.5;
	EnergyDMITriangleBulkNorm *= 0.5;
	EnergyStretchingNorm *= 0.5;
	EnergyBendingNorm *= 0.5;
	/* calculate total normalized energy */
	EnergyOfSystemNorm = EnergyExchangeNorm
	                     + EnergyAnisotropyNormAxisK1
	                     + EnergyAnisotropyNormAxisK2
	                     + EnergyAnisotropyNormAxisK3
	                     + EnergyDipoleNorm
	                     + EnergyDMIAxis1Norm
	                     + EnergyDMIAxis2Norm
	                     + EnergyDMIAxis3Norm
	                     + EnergyDMITriangleInterfaceNorm
	                     + EnergyDMITriangleBulk
	                     + EnergyStretchingNorm
	                     + EnergyBendingNorm
	                     + EnergyExternalNorm;

}

double CalculateError(double *dm4, double *dm5) {

	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* find initial index */
	long curIndex = IDXmg(begin, 1, 1);
	/* initial magnetic error */
	double errorMagnetic = fabs(dm4[curIndex] - dm5[curIndex]);
	/* current errors */
	double error1, error2, error3;

	/* go through all sites */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
				/* find current index */
				curIndex = IDXmg(i1, i2, i3);
				/* find current errors */
				error1 = fabs(dm4[curIndex] - dm5[curIndex]);
				error2 = fabs(dm4[curIndex + 1] - dm5[curIndex + 1]);
				error3 = fabs(dm4[curIndex + 2] - dm5[curIndex + 2]);
				/* find the largest error of magnetic moments */
				if (error1 > errorMagnetic) {
					errorMagnetic = error1;
				}
				if (error2 > errorMagnetic) {
					errorMagnetic = error2;
				}
				if (error3 > errorMagnetic) {
					errorMagnetic = error3;
				}
			}
		}
	}

	/* synchronize maximal magnetic error */
	synchronizeScalarValueMax(&errorMagnetic);
	synchronizeScalarValue(&errorMagnetic);

	if (userVars.flexOnOff == FLEX_ON) {

		/* find initial index */
		curIndex = IDXmg(begin, 1, 1);
		/* initial coordinate error */
		double errorCoordinate = fabs(dm4[curIndex + spinsNumber] - dm5[curIndex + spinsNumber]);

		/* go through all sites */
		int i1, i2, i3;
		for (i1 = begin; i1 <= end; i1++) {
			for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
					/* find current index */
					curIndex = IDXmg(i1, i2, i3);
					/* find current errors */
					error1 = fabs(dm4[curIndex + spinsNumber] - dm5[curIndex + spinsNumber]);
					error2 = fabs(dm4[curIndex + spinsNumber + 1] - dm5[curIndex + spinsNumber + 1]);
					error3 = fabs(dm4[curIndex + spinsNumber + 2] - dm5[curIndex + spinsNumber + 2]);
					/* find the largest error of coordinates */
					if (error1 > errorMagnetic) {
						errorMagnetic = error1;
					}
					if (error2 > errorMagnetic) {
						errorMagnetic = error2;
					}
					if (error3 > errorMagnetic) {
						errorMagnetic = error3;
					}
				}
			}
		}

		/* synchronize maximal error of coordinate */
		synchronizeScalarValueMax(&errorCoordinate);
		synchronizeScalarValue(&errorCoordinate);

		/* find initial index */
		curIndex = IDXmg(begin, 1, 1);
		/* initial velocity error */
		double errorVelocity = fabs(dm4[curIndex + 2 * spinsNumber] - dm5[curIndex + 2 * spinsNumber]);

		/* go through all sites */
		for (i1 = begin; i1 <= end; i1++) {
			for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
					/* find current index */
					curIndex = IDXmg(i1, i2, i3);
					/* find current errors */
					error1 = fabs(dm4[curIndex + 2 * spinsNumber] - dm5[curIndex + 2 * spinsNumber]);
					error2 = fabs(dm4[curIndex + 2 * spinsNumber + 1] - dm5[curIndex + 2 * spinsNumber + 1]);
					error3 = fabs(dm4[curIndex + 2 * spinsNumber + 2] - dm5[curIndex + 2 * spinsNumber + 2]);
					/* find the largest error of coordinates */
					if (error1 > errorVelocity) {
						errorVelocity = error1;
					}
					if (error2 > errorVelocity) {
						errorVelocity = error2;
					}
					if (error3 > errorVelocity) {
						errorVelocity = error3;
					}
				}
			}
		}

		/* synchronize maximal error of velocity */
		synchronizeScalarValueMax(&errorVelocity);
		synchronizeScalarValue(&errorVelocity);

		/* weighted error based on magnetic moment, velocity and coordinates */
		return (errorMagnetic + errorCoordinate + errorVelocity) / 3.0;

	}

	/* return only magnetic error */
	return errorMagnetic;

}

double CalculateMaxDmDt(double *dm, double h) {

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* find initial index */
	long curIndex = IDXmg(begin, 1, 1);
	/* find initial maximal value */
	double Max = fabs(dm[curIndex]);

	/* go through all sites */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
				/* find current index */
				curIndex = IDXmg(i1, i2, i3);
				/* find the largest magnetic inclination for magnetic moments */
				if (fabs(dm[curIndex]) > Max) {
					Max = fabs(dm[curIndex]);
				}
				if (fabs(dm[curIndex + 1]) > Max) {
					Max = fabs(dm[curIndex + 1]);
				}
				if (fabs(dm[curIndex + 2]) > Max) {
					Max = fabs(dm[curIndex + 2]);
				}
			}
		}
	}

	/* synchronize maximal magnetic inclination */
	synchronizeScalarValueMax(&Max);
	synchronizeScalarValue(&Max);

	/* return maximal magnetic inclination */
	return Max / h;

}

double CalculateMaxDrDt(double *dm, double h) {

	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* find initial index */
	long curIndex = IDXmg(begin, 1, 1);
	/* find initial maximal value */
	double Max = fabs(dm[curIndex + spinsNumber]);

	/* go through all sites */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
				/* find current index */
				curIndex = IDXmg(i1, i2, i3);
				/* find the largest inclination in space for magnetic moments */
				if (fabs(dm[curIndex + spinsNumber]) > Max) {
					Max = fabs(dm[curIndex + spinsNumber]);
				}
				if (fabs(dm[curIndex + spinsNumber + 1]) > Max) {
					Max = fabs(dm[curIndex + spinsNumber + 1]);
				}
				if (fabs(dm[curIndex + spinsNumber + 2]) > Max) {
					Max = fabs(dm[curIndex + spinsNumber + 2]);
				}
			}
		}
	}

	/* synchronize maximal inclination in space */
	synchronizeScalarValueMax(&Max);
	synchronizeScalarValue(&Max);

	/* return maximal inclination in space */
	return Max / h;

}

double CalculateMaxAngle(double *dm) {

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* find initial index */
	long curIndex = IDXmg(begin, 1, 1);

	/* initial inclination */
	double MaxIncl = sqrt(dm[curIndex] * dm[curIndex] +
	                      dm[curIndex + 1] * dm[curIndex + 1] + dm[curIndex + 2] * dm[curIndex + 2]);
	/* current inclination */
	double Incl;

	/* go through all sites */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
				/* find current index */
				curIndex = IDXmg(i1, i2, i3);
				/* find the largest inclination for magnetic moments */
				Incl = dm[curIndex] * dm[curIndex] +
				       dm[curIndex + 1] * dm[curIndex + 1] + dm[curIndex + 2] * dm[curIndex + 2];
				if (Incl > MaxIncl * MaxIncl)
					MaxIncl = sqrt(Incl);
			}
		}
	}

	/* calculate maximal angle */
	double MaxAngle = fabs(MaxIncl - pow(MaxIncl, 3.0) / 3.0 + pow(MaxIncl, 5.0) / 5.0);

	/* synchronize maximal angle */
	synchronizeScalarValueMax(&MaxAngle);
	synchronizeScalarValue(&MaxAngle);

	/* return maximal angle */
	return MaxAngle;

}

void NormalizeVectors(void) {

	/* variables to go through all nodes */
	size_t indexLattice, curIndex;
	/* length of vector of magnetic moment */
	double lengthVector;

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* pass through each magnetic moment */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

				indexLattice = IDX(i1, i2, i3);
				/* if the node is magnetic, then go on */
				if (latticeParam[indexLattice].magnetic) {
					/* normalize per unit*/
					curIndex = IDXmg(i1, i2, i3);
					lengthVector = sqrt(
							lattice[curIndex] * lattice[curIndex] +
							lattice[curIndex + 1] * lattice[curIndex + 1] +
							lattice[curIndex + 2] * lattice[curIndex + 2]);
					lattice[curIndex] /= lengthVector;
					lattice[curIndex + 1] /= lengthVector;
					lattice[curIndex + 2] /= lengthVector;
				}

			}
		}
	}

}

void calculateSmallChangesMagn(double **k, double *dm4, double *dm5) {

	/* constants for Runge-Kutta-Fehlberg */
	const double b0 = 25.0 / 216.0;
	const double b1 = 0.0;
	const double b2 = 1408.0 / 2565.0;
	const double b3 = 2197.0 / 4104.0;
	const double b4 = -1.0 / 5.0;
	const double b5 = 0.0;

	const double c0 = 16.0 / 135.0;
	const double c1 = 0.0;
	const double c2 = 6656.0 / 12825.0;
	const double c3 = 28561.0 / 56430.0;
	const double c4 = -9.0 / 50.0;
	const double c5 = 2.0 / 55.0;

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* go through all sites */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
				/* find current index */
				long curIndex = IDXmg(i1, i2, i3);
				/* small changes of the fourth order */
				dm4[curIndex] = b0 * k[0][curIndex] + b1 * k[1][curIndex] + b2 * k[2][curIndex] +
				                b3 * k[3][curIndex] + b4 * k[4][curIndex] + b5 * k[5][curIndex];
				dm4[curIndex + 1] = b0 * k[0][curIndex + 1] + b1 * k[1][curIndex + 1] + b2 * k[2][curIndex + 1] +
				                    b3 * k[3][curIndex + 1] + b4 * k[4][curIndex + 1] + b5 * k[5][curIndex + 1];
				dm4[curIndex + 2] = b0 * k[0][curIndex + 2] + b1 * k[1][curIndex + 2] + b2 * k[2][curIndex + 2] +
				                    b3 * k[3][curIndex + 2] + b4 * k[4][curIndex + 2] + b5 * k[5][curIndex + 2];
				/* small changes of the fifth order */
				dm5[curIndex] = c0 * k[0][curIndex] + c1 * k[1][curIndex] + c2 * k[2][curIndex] +
				                c3 * k[3][curIndex] + c4 * k[4][curIndex] + c5 * k[5][curIndex];
				dm5[curIndex + 1] = c0 * k[0][curIndex + 1] + c1 * k[1][curIndex + 1] + c2 * k[2][curIndex + 1] +
				                    c3 * k[3][curIndex + 1] + c4 * k[4][curIndex + 1] + c5 * k[5][curIndex + 1];
				dm5[curIndex + 2] = c0 * k[0][curIndex + 2] + c1 * k[1][curIndex + 2] + c2 * k[2][curIndex + 2] +
				                    c3 * k[3][curIndex + 2] + c4 * k[4][curIndex + 2] + c5 * k[5][curIndex + 2];
			}
		}
	}

}

void calculateSmallChangesOther(double **k, double *dm4, double *dm5) {

	/* constants for Runge-Kutta-Fehlberg */
	const double b0 = 25.0 / 216.0;
	const double b1 = 0.0;
	const double b2 = 1408.0 / 2565.0;
	const double b3 = 2197.0 / 4104.0;
	const double b4 = -1.0 / 5.0;
	const double b5 = 0.0;

	const double c0 = 16.0 / 135.0;
	const double c1 = 0.0;
	const double c2 = 6656.0 / 12825.0;
	const double c3 = 28561.0 / 56430.0;
	const double c4 = -9.0 / 50.0;
	const double c5 = 2.0 / 55.0;

	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* go through all sites */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

				/* find current index */
				long curIndex = IDXmg(i1, i2, i3);

				/* small changes of the fourth order */

				/* coordinates */
				dm4[curIndex + spinsNumber] = b0 * k[0][curIndex + spinsNumber] + b1 * k[1][curIndex + spinsNumber] + b2 * k[2][curIndex + spinsNumber] +
				                              b3 * k[3][curIndex + spinsNumber] + b4 * k[4][curIndex + spinsNumber] + b5 * k[5][curIndex + spinsNumber];
				dm4[curIndex + spinsNumber + 1] = b0 * k[0][curIndex + spinsNumber + 1] + b1 * k[1][curIndex + spinsNumber + 1] + b2 * k[2][curIndex + spinsNumber + 1] +
				                                  b3 * k[3][curIndex + spinsNumber + 1] + b4 * k[4][curIndex + spinsNumber + 1] + b5 * k[5][curIndex + spinsNumber + 1];
				dm4[curIndex + spinsNumber + 2] = b0 * k[0][curIndex + spinsNumber + 2] + b1 * k[1][curIndex + spinsNumber + 2] + b2 * k[2][curIndex + spinsNumber + 2] +
				                                  b3 * k[3][curIndex + spinsNumber + 2] + b4 * k[4][curIndex + spinsNumber + 2] + b5 * k[5][curIndex + spinsNumber + 2];

				/* velocities */
				dm4[curIndex + 2 * spinsNumber] = b0 * k[0][curIndex + 2 * spinsNumber] + b1 * k[1][curIndex + 2 * spinsNumber] + b2 * k[2][curIndex + 2 * spinsNumber] +
				                                  b3 * k[3][curIndex + 2 * spinsNumber] + b4 * k[4][curIndex + 2 * spinsNumber] + b5 * k[5][curIndex + 2 * spinsNumber];
				dm4[curIndex + 2 * spinsNumber + 1] = b0 * k[0][curIndex + 2 * spinsNumber + 1] + b1 * k[1][curIndex + 2 * spinsNumber + 1] + b2 * k[2][curIndex + 2 * spinsNumber + 1] +
				                                      b3 * k[3][curIndex + 2 * spinsNumber + 1] + b4 * k[4][curIndex + 2 * spinsNumber + 1] + b5 * k[5][curIndex + 2 * spinsNumber + 1];
				dm4[curIndex + 2 * spinsNumber + 2] = b0 * k[0][curIndex + 2 * spinsNumber + 2] + b1 * k[1][curIndex + 2 * spinsNumber + 2] + b2 * k[2][curIndex + 2 * spinsNumber + 2] +
				                                      b3 * k[3][curIndex + 2 * spinsNumber + 2] + b4 * k[4][curIndex + 2 * spinsNumber + 2] + b5 * k[5][curIndex + 2 * spinsNumber + 2];


				/* small changes of the fifth order */

				/* coordinates */
				dm5[curIndex + spinsNumber] = c0 * k[0][curIndex + spinsNumber] + c1 * k[1][curIndex + spinsNumber] + c2 * k[2][curIndex + spinsNumber] +
				                              c3 * k[3][curIndex + spinsNumber] + c4 * k[4][curIndex + spinsNumber] + c5 * k[5][curIndex + spinsNumber];
				dm5[curIndex + spinsNumber + 1] = c0 * k[0][curIndex + spinsNumber + 1] + c1 * k[1][curIndex + spinsNumber + 1] + c2 * k[2][curIndex + spinsNumber + 1] +
				                                  c3 * k[3][curIndex + spinsNumber + 1] + c4 * k[4][curIndex + spinsNumber + 1] + c5 * k[5][curIndex + spinsNumber + 1];
				dm5[curIndex + spinsNumber + 2] = c0 * k[0][curIndex + spinsNumber + 2] + c1 * k[1][curIndex + spinsNumber + 2] + c2 * k[2][curIndex + spinsNumber + 2] +
				                                  c3 * k[3][curIndex + spinsNumber + 2] + c4 * k[4][curIndex + spinsNumber + 2] + c5 * k[5][curIndex + spinsNumber + 2];

				/* velocities */
				dm5[curIndex + 2 * spinsNumber] = c0 * k[0][curIndex + 2 * spinsNumber] + c1 * k[1][curIndex + 2 * spinsNumber] + c2 * k[2][curIndex + 2 * spinsNumber] +
				                                  c3 * k[3][curIndex + 2 * spinsNumber] + c4 * k[4][curIndex + 2 * spinsNumber] + c5 * k[5][curIndex + 2 * spinsNumber];
				dm5[curIndex + 2 * spinsNumber + 1] = c0 * k[0][curIndex + 2 * spinsNumber + 1] + c1 * k[1][curIndex + 2 * spinsNumber + 1] + c2 * k[2][curIndex + 2 * spinsNumber + 1] +
				                                      c3 * k[3][curIndex + 2 * spinsNumber + 1] + c4 * k[4][curIndex + 2 * spinsNumber + 1] + c5 * k[5][curIndex + 2 * spinsNumber + 1];
				dm5[curIndex + 2 * spinsNumber + 2] = c0 * k[0][curIndex + 2 * spinsNumber + 2] + c1 * k[1][curIndex + 2 * spinsNumber + 2] + c2 * k[2][curIndex + 2 * spinsNumber + 2] +
				                                      c3 * k[3][curIndex + 2 * spinsNumber + 2] + c4 * k[4][curIndex + 2 * spinsNumber + 2] + c5 * k[5][curIndex + 2 * spinsNumber + 2];

			}
		}
	}

}

void synchronizeSystemArray(double *m) {

	if (userVars.flexOnOff != FLEX_ON) {
		/* synchronize only magnetic moments and external field */
		synchronizeSystemArrayPieceMagn(m);
		synchronizeSystemArrayFullMagn(m);
		synchronizeBamp();
	} else {
		/* synchronize magnetic moments, coordinates, velocities and external field */
		synchronizeSystemArrayPiece(m);
		synchronizeSystemArrayFull(m);
		synchronizeBamp();
	}

}

void addSmallChanges(double *m, double *dm5) {

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* go through all sites (magnetic moments) */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
				/* find current index */
				long curIndex = IDXmg(i1, i2, i3);
				/* add "small changes" for magnetic moments */
				m[curIndex] += dm5[curIndex];
				m[curIndex + 1] += dm5[curIndex + 1];
				m[curIndex + 2] += dm5[curIndex + 2];
			}
		}
	}

	if (userVars.flexOnOff == FLEX_ON) {

		/* go through all sites (coordinates and velocity) */
		int i1, i2, i3;	
		for (i1 = begin; i1 <= end; i1++) {
			for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
					/* find current index */
					long curIndex = IDXmg(i1, i2, i3);
					/* add "small changes" for coordinates*/
					m[curIndex + spinsNumber] += dm5[curIndex + spinsNumber];
					m[curIndex + spinsNumber + 1] += dm5[curIndex + spinsNumber + 1];
					m[curIndex + spinsNumber + 2] += dm5[curIndex + spinsNumber + 2];
					/* add "small changes" for velocity */
					m[curIndex + 2 * spinsNumber] += dm5[curIndex + 2 * spinsNumber];
					m[curIndex + 2 * spinsNumber + 1] += dm5[curIndex + 2 * spinsNumber + 1];
					m[curIndex + 2 * spinsNumber + 2] += dm5[curIndex + 2 * spinsNumber + 2];
				}
			}
		}

	}

}

void addSmallChangesToArray(double *latticeNew, double *lattice, double *dm) {

	/* begin and end sites for MPI process */
	int begin = rank * (userVars.N1 - 2) / numberProc + 1;
	int end = (rank + 1) * (userVars.N1 - 2) / numberProc;

	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* go through all sites (magnetic moments) */
	int i1, i2, i3;
	for (i1 = begin; i1 <= end; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
				/* find current index */
				long curIndex = IDXmg(i1, i2, i3);
				/* add "small changes" for magnetic moments */
				latticeNew[curIndex] = lattice[curIndex] + dm[curIndex];
				latticeNew[curIndex + 1] = lattice[curIndex + 1] + dm[curIndex + 1];
				latticeNew[curIndex + 2] = lattice[curIndex + 2] + dm[curIndex + 2];
			}
		}
	}

	if (userVars.flexOnOff == FLEX_ON) {

		/* go through all sites (coordinates and velocity) */
		int i1, i2, i3;
		for (i1 = begin; i1 <= end; i1++) {
			for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
					/* find current index */
					long curIndex = IDXmg(i1, i2, i3);
					/* add "small changes" for coordinates */
					latticeNew[curIndex + spinsNumber] = lattice[curIndex + spinsNumber] +
					                                     dm[curIndex + spinsNumber];
					latticeNew[curIndex + spinsNumber + 1] = lattice[curIndex + spinsNumber + 1] +
					                                         dm[curIndex + spinsNumber + 1];
					latticeNew[curIndex + spinsNumber + 2] = lattice[curIndex + spinsNumber + 2] +
					                                         dm[curIndex + spinsNumber + 2];
					/* add "small changes" for velocity */
					latticeNew[curIndex + 2 * spinsNumber] = lattice[curIndex + 2 * spinsNumber] +
					                                         dm[curIndex + 2 * spinsNumber];
					latticeNew[curIndex + 2 * spinsNumber + 1] = lattice[curIndex + 2 * spinsNumber + 1] +
					                                             dm[curIndex + 2 * spinsNumber + 1];
					latticeNew[curIndex + 2 * spinsNumber + 2] = lattice[curIndex + 2 * spinsNumber + 2] +
					                                             dm[curIndex + 2 * spinsNumber + 2];
				}
			}
		}

	}

}

void createDoubleArrays(double ** array, size_t size) {

	*array = (double *) malloc(sizeof(double) * size);
	memset(*array, 0, sizeof(double) * size);

}

void deleteDoubleArrays(double *array) {
	free(array);
}

void copySystemArray(void) {
}

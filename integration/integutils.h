#ifndef _INTEGUTILS_H_
#define _INTEGUTILS_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../alltypes.h"
#include "../utils/indices.h"

/**
 * @brief This function calculates the first grid of intermediate values for Runge-Kutti-Feelberg
 * @param k the first array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of the variables for the Cauchy problem
 * @param amount number of variables for the Cauchy problem
 * @param t current time
 * @param h current integration step
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 */
void CalculateGrid0(double *k, double *m, double t, size_t amount, double h, void (*pointerFunc) (double *, double *, size_t, double, double));

/**
 * @brief This function calculates the second grid of intermediate values for Runge-Kutti-Feelberg
 * @param kpast1 the first array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param k the second array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of the variables for the Cauchy problem
 * @param amount number of variables for the Cauchy problem
 * @param t current time
 * @param h current integration step
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 */
void CalculateGrid1(double *k, double *kpast1, double *m, double t, size_t amount, double h, void (*pointerFunc) (double *, double *, size_t, double, double), double * mCh);

/**
 * @brief This function calculates the third grid of intermediate values for Runge-Kutti-Feelberg
 * @param kpast2 the first array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast1 the second array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param k the third array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of the variables for the Cauchy problem
 * @param amount number of variables for the Cauchy problem
 * @param t current time
 * @param h current integration step
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 * @param mCh double array for helping purposes (should be of size `amount`)
 */
void CalculateGrid2(double *k, double *kpast1, double *kpast2, double *m, double t, size_t amount, double h, void (*pointerFunc) (double *, double *, size_t, double, double), double * mCh);

/**
 * @brief This function calculates the fourth grid of intermediate values for Runge-Kutti-Feelberg
 * @param kpast3 the first array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast2 the second array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast1 the third array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param k the fourth array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of the variables for the Cauchy problem
 * @param amount number of variables for the Cauchy problem
 * @param t current time
 * @param h current integration step
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 * @param mCh double array for helping purposes (should be of size `amount`)
 */
void CalculateGrid3(double *k, double *kpast1, double *kpast2, double *kpast3,  double *m, double t, size_t amount, double h, void (*pointerFunc) (double *, double *, size_t, double, double), double * mCh);

/**
 * @brief This function calculates the fifth grid of intermediate values for Runge-Kutti-Feelberg
 * @param kpast4 the first array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast3 the second array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast2 the third array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast1 the fourth array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param k the fifth array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of the variables for the Cauchy problem
 * @param amount number of variables for the Cauchy problem
 * @param t current time
 * @param h current integration step
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 * @param mCh double array for helping purposes (should be of size `amount`)
 */
void CalculateGrid4(double *k, double *kpast1, double *kpast2, double *kpast3, double *kpast4, double *m, double t, size_t amount, double h, void (*pointerFunc) (double *, double *, size_t, double, double), double * mCh);

/**
 * @brief This function calculates the sixth grid of intermediate values for Runge-Kutti-Feelberg
 * @param kpast4 the first array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast4 the second array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast3 the third array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast2 the fourth array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast1 the fifth array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param k the sixth array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of the variables for the Cauchy problem
 * @param amount number of variables for the Cauchy problem
 * @param t current time
 * @param h current integration step
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 * @param mCh double array for helping purposes (should be of size `amount`)
 */
void CalculateGrid5(double *k, double *kpast1, double *kpast2, double *kpast3, double *kpast4, double *kpast5, double *m, double t, size_t amount, double h, void (*pointerFunc) (double *, double *, size_t, double, double), double * mCh);

/**
 * @brief This function calculates errors of the Runge-Kutta-Feelberg method
 * @param dm4 array of values that must be added to the variables after the completion of the step and has fourth order of precision
 * @param dm5 array of values that must be added to the variables after the completion of the step and has fifth order of precision
 */
double CalculateError(double *dm4, double *dm5);

/**
 * @brief This function finds the maximum magnetic value that must be added
 * after the completion of the step for the Runge-Kutta-Feelberg method
 * @param dm5 array of values that must be added to the variables after the completion of the step and has fifth order of precision
 * @param h step of integration
 */
double CalculateMaxDmDt(double *dm5, double h);

/**
 * @brief This function finds the maximum value of coordinates that must be added
 * after the completion of the step for the Runge-Kutta-Feelberg method
 * @param dm5 array of values that must be added to the variables after the completion of the step and has fifth order of precision
 * @param h step of integration
 */
double CalculateMaxDrDt(double *dm5, double h);

/**
 * @brief This function finds the maximum angle of deviation of magnetic moments
 * @param dm array of values that must be added to the variables after the completion of the step and has fifth order of precision
 */
double CalculateMaxAngle(double *dm);

/**
 * @brief This function adds all kinds of energy to find total energy
 */
void addAllEnergies(void);

/**
 * @brief This function normalizes vectors of magnetic moments (array "lattice")
 */
void NormalizeVectors(void);

/**
 * @brief This function calculates "small changes" of magnetic vectors
 * @param k is 6 arrays of intermediate values for RKF45
 * @param dm4 is "small changes" of fourth order for RKF45
 * @param dm5 is "small changes" of fifth order for RKF45
 */
void calculateSmallChangesMagn(double **k, double *dm4, double *dm5);

/**
 * @brief This function calculates "small changes" of coordinates and velocities
 * @param k is 6 arrays of intermediate values for RKF45
 * @param dm4 is "small changes" of fourth order for RKF45
 * @param dm5 is "small changes" of fifth order for RKF45
 */
void calculateSmallChangesOther(double **k, double *dm4, double *dm5);

/**
 * @brief This function synchronizes "system array" for MPI processes
 * @param m is "system array"
 */
void synchronizeSystemArray(double *m);

/**
 * @brief This function adds "small changes" to vector of state of dynamic system
 * @param m is vector of current state
 * @param dm5 is "small changes" of fifth order for RKF45
 */
void addSmallChanges(double *m, double *dm5);

/**
 * @brief This function adds "small changes" to new array of lattice
 * @param latticeNew is new array of lattice
 * @param lattice is old array of lattice
 * @param dm is array of "small changes"
 */
void addSmallChangesToArray(double *latticeNew, double *lattice, double *dm);

/**
 * @brief This function creates array of given size
 * @param array is pointer to pointer of array
 * @param size is size of array
 */
void createDoubleArrays(double **array, size_t size);

/**
 * @brief This function deletes array
 * @param array is pointer of array
 */
void deleteDoubleArrays(double *array);

/**
 * @brief This function copies system array if it is necessary (CUDA)
 */
void copySystemArray(void);

#endif

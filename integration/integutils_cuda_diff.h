#ifndef INTEGUTILS_CUDA_DIFF_H
#define INTEGUTILS_CUDA_DIFF_H

#include <stddef.h>

/**
 * @brief This function calculates the first grid of intermediate values for Runge-Kutti-Feelberg (CUDA implementation)
 * @param k the first array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of the variables for the Cauchy problem
 * @param amount number of variables for the Cauchy problem
 * @param t current time
 * @param h current integration step
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 */
void CalculateGrid0CUDA(double *k, double *m, double t, size_t amount, double h, void (*pointerFunc) (double *, double *, size_t, double, double));

/**
 * @brief This function calculates the second grid of intermediate values for Runge-Kutti-Feelberg (CUDA implementation)
 * @param kpast1 the first array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param k the second array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of the variables for the Cauchy problem
 * @param amount number of variables for the Cauchy problem
 * @param t current time
 * @param h current integration step
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 */
void CalculateGrid1CUDA(double *k, double *kpast1, double *m, double t, size_t amount, double h, void (*pointerFunc) (double *, double *, size_t, double, double));

/**
 * @brief This function calculates the third grid of intermediate values for Runge-Kutti-Feelberg (CUDA implementation)
 * @param kpast2 the first array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast1 the second array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param k the third array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of the variables for the Cauchy problem
 * @param amount number of variables for the Cauchy problem
 * @param t current time
 * @param h current integration step
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 * @param mCh double array for helping purposes (should be of size `amount`)
 */
void CalculateGrid2CUDA(double *k, double *kpast1, double *kpast2, double *m, double t, size_t amount, double h, void (*pointerFunc) (double *, double *, size_t, double, double));

/**
 * @brief This function calculates the fourth grid of intermediate values for Runge-Kutti-Feelberg (CUDA implementation)
 * @param kpast3 the first array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast2 the second array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast1 the third array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param k the fourth array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of the variables for the Cauchy problem
 * @param amount number of variables for the Cauchy problem
 * @param t current time
 * @param h current integration step
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 * @param mCh double array for helping purposes (should be of size `amount`)
 */
void CalculateGrid3CUDA(double *k, double *kpast1, double *kpast2, double *kpast3,  double *m, double t, size_t amount, double h, void (*pointerFunc) (double *, double *, size_t, double, double));

/**
 * @brief This function calculates the fifth grid of intermediate values for Runge-Kutti-Feelberg (CUDA implementation)
 * @param kpast4 the first array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast3 the second array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast2 the third array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast1 the fourth array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param k the fifth array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of the variables for the Cauchy problem
 * @param amount number of variables for the Cauchy problem
 * @param t current time
 * @param h current integration step
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 * @param mCh double array for helping purposes (should be of size `amount`)
 */
void CalculateGrid4CUDA(double *k, double *kpast1, double *kpast2, double *kpast3, double *kpast4, double *m, double t, size_t amount, double h, void (*pointerFunc) (double *, double *, size_t, double, double));

/**
 * @brief This function calculates the sixth grid of intermediate values for Runge-Kutti-Feelberg (CUDA implementation)
 * @param kpast4 the first array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast4 the second array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast3 the third array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast2 the fourth array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param kpast1 the fifth array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param k the sixth array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of the variables for the Cauchy problem
 * @param amount number of variables for the Cauchy problem
 * @param t current time
 * @param h current integration step
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 * @param mCh double array for helping purposes (should be of size `amount`)
 */
void CalculateGrid5CUDA(double *k, double *kpast1, double *kpast2, double *kpast3, double *kpast4, double *kpast5, double *m, double t, size_t amount, double h, void (*pointerFunc) (double *, double *, size_t, double, double));

/**
 * @brief This function adds all kinds of energy to find total energy (CUDA implementation)
 */
void addAllEnergiesCUDA(void);

/**
 * @brief This function normalizes vectors of magnetic moments (CUDA implementation)
 */
void NormalizeVectorsCUDA(void);

/**
 * @brief This function calculates "small changes" of magnetic vectors (CUDA implementation)
 * @param k is 6 arrays of intermediate values for RKF45
 * @param dm5 is "small changes" of fifth order for RKF45
 */
void calculateSmallChangesMagnCUDA(double **k, double *dm5);

/**
 * @brief This function calculates "small changes" of coordinates and velocities (CUDA implementation)
 * @param k is 6 arrays of intermediate values for RKF45
 * @param dm5 is "small changes" of fifth order for RKF45
 */
void calculateSmallChangesOtherCUDA(double **k, double *dm5);

/**
 * @brief This function synchronizes "system array" for processes or threads (CUDA implementation)
 * @param m is "system array"
 */
void synchronizeSystemArrayCUDA(double *m);

/**
 * @brief This function adds "small changes" to vector of state of dynamic system (CUDA implementation)
 * @param m is vector of current state
 * @param dm5 is "small changes" of fifth order for RKF45
 */
void addSmallChangesCUDA(double *m, double *dm5);

/**
 * @brief This function adds "small changes" to new array of lattice (CUDA implementation)
 * @param latticeNew is new array of lattice
 * @param lattice is old array of lattice
 * @param dm is array of "small changes"
 */
void addSmallChangesToArrayCUDA(double *latticeNew, double *lattice, double *dm);

/**
 * @brief This function finds the maximum magnetic value that must be added
 * after the completion of the step for the Runge-Kutta-Feelberg method (on CPU)
 * @param dm array of values that must be added to the variables after the completion of the step and has fifth order of precision
 * @param h step of integration
 */
double CalculateMaxDmDtCUDA(double *dm, double h);

/**
 * @brief This function finds the maximum value of coordinates that must be added
 * after the completion of the step for the Runge-Kutta-Feelberg method (on CPU)
 * @param dm array of values that must be added to the variables after the completion of the step and has fifth order of precision
 * @param h step of integration
 */
double CalculateMaxDrDtCUDA(double *dm, double h);

#endif

#ifndef _DIFFEQUATION_SHARED_H_
#define _DIFFEQUATION_SHARED_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../alltypes.h"

/**
 * @brief Function to initialize energies (set "zero" for all energies)
 */
void initializeEnergies(void);

/**
 * @brief Function to check boundary condition (for closed system)
 * @param m array of coordinates and magnetic moments (array "lattice")
 */
void checkBoundaryCondition(double *m);

#endif

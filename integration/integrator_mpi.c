#include "integutils.h"
#include "../interactions/externalField/fieldSetupInt.h"
#include "../interactions/externalForce/forceSetupInt.h"
#include "../utils/utils.h"
#include "../utils/parallel_utils.h"
#include "../alltypes.h"
#include "diffequation.h"

void findEnergy(double *RKF, size_t spinsNumber, double t0, double step) {

	/* find out initial energy and other parameters of magnetic system */
	if (userVars.latticeType == LATTICE_CUBIC) {
		DiffEquation(RKF, lattice, spinsNumber, t0, step);
	} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
		DiffEquationTriangle(RKF, lattice, spinsNumber, t0, step);
	}

}

int updateValuesAfterStepMPI(double t) {

	/* value to return */
	int result = 0;

	/* normalize vectors of magnetic moments */
	if (userVars.normalOnOff == NORMAL_ON)
		NormalizeVectors();
	/* setup external magnetic field */
	if ((userVars.extField == EXTFIELD_HARMTIME) || (userVars.extField == EXTFIELD_SINC) ||
	    (userVars.extField == EXTFIELD_CONST) || (userVars.extField == EXTFIELD_SCRIPT)) {
		/* dividing using "userVars.NormCoefficientTime" because of normalization of time */
		result = fieldSetupInt(t / userVars.NormCoefficientTime);
	}
	/* synchronize array of magnetic moments, coordinates, external field and velocities */
	synchronizeSystemArray(lattice);
	/* it is only for flexible system */
	if (userVars.flexOnOff == FLEX_ON) {
		/* update basic vectors for curve lines */
		if (userVars.dimensionOfSystem == 1) {
			/* update orthonormal basis */
			UpdateOrthonormalBasis();
		}
		/* update basic vectors for triangular flexible lattice */
		if ((userVars.latticeType == LATTICE_TRIANGULAR) && (userVars.dimensionOfSystem == 2)) {
			/* update normal vectors */
			UpdateNormalVectorTriangle();
			/* update normal vectors (sites) */
			UpdateNormalVectorSite();
			/* update main direction */
			UpdateMainDirectionTriangle();
		}
		/* setup external force */
		if (userVars.extForce == EXTFORCE_HARMTIME) {
			/* dividing using "userVars.NormCoefficientTime" because of normalization of time */
			result = forceSetupInt(t / userVars.NormCoefficientTime);
		}
	}

	/* return result */
	return result;

}

int IntegrateFunctionsRKF(double t0, double tfin, size_t amount, double prec,
                       void (*pointerFunc)(double *, double *, size_t, double, double)) {

	/* value to return */
	int result = 0;
	/* variables to store current value */
	double s, h, t, MaxAngle;

	/* define initial time */
	t = t0;
	/* define integration step */
	h = userVars.integrationStepNorm;
	integrationStepMin = h;
	integrationStepMax = h;

	/* temporary array for magnetic moment */
	double * mCh = malloc(NUMBER_QUANTITIES * amount * sizeof (double));

	while (t <= tfin) {
		/* store information about maximal and minimal step */
		if (h > integrationStepMax)
			integrationStepMax = h;
		if (h < integrationStepMin)
			integrationStepMin = h;
		/* calculate intermediate values */
		CalculateGrid0(m_k[0], lattice, t, amount, h, pointerFunc);
		CalculateGrid1(m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
		CalculateGrid2(m_k[2], m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
		CalculateGrid3(m_k[3], m_k[2], m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
		CalculateGrid4(m_k[4], m_k[3], m_k[2], m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
		CalculateGrid5(m_k[5], m_k[4], m_k[3], m_k[2], m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
		/* calculate change of magnetic moments with change of time */
		calculateSmallChangesMagn(m_k, m_dm4, m_dm5);
		/* only for flexible system */
		if (userVars.flexOnOff == FLEX_ON) {
			/* calculate change of coordinates and velocities with change of time */
			calculateSmallChangesOther(m_k, m_dm4, m_dm5);
		}

		/* calculate maximum change of angle of inclination for magnetic moments */
		MaxAngle = CalculateMaxAngle(m_dm5);
		/* if angle is too large, then we make smaller step */
		while ((MaxAngle > userVars.critAngle) && (userVars.fixedStep == FIXEDSTEP_OFF)) {
			/* decrease integration step */
			h *= (userVars.critAngle / MaxAngle);
			/* calculate intermediate values */
			CalculateGrid0(m_k[0], lattice, t, amount, h, pointerFunc);
			CalculateGrid1(m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
			CalculateGrid2(m_k[2], m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
			CalculateGrid3(m_k[3], m_k[2], m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
			CalculateGrid4(m_k[4], m_k[3], m_k[2], m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
			CalculateGrid5(m_k[5], m_k[4], m_k[3], m_k[2], m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
			/* calculate change of magnetic moments with change of time */
			calculateSmallChangesMagn(m_k, m_dm4, m_dm5);
			/* only for flexible system */
			if (userVars.flexOnOff == FLEX_ON) {
				/* calculate change of coordinates and velocities with change of time */
				calculateSmallChangesOther(m_k, m_dm4, m_dm5);
			}
			/* calculate maximum change of angle of inclination for magnetic moments */
			MaxAngle = CalculateMaxAngle(m_dm5);
			/* if integration step is too small or big, we will put default value and get out of cycle */
			if (h < H_MIN) {
				h = H_MIN;
				break;
			}
			if (h > H_MAX) {
				h = H_MAX;
				break;
			}
		}
		/* calculate error of integration */
		errorStep = CalculateError(m_dm4, m_dm5);
		if ((errorStep <= prec) || (userVars.fixedStep == FIXEDSTEP_ON)) {
			/* if error is small, then work further */
			t = t + h;
			addSmallChanges(lattice, m_dm5);
			if (userVars.fixedStep != FIXEDSTEP_ON) {
				/* select step for next iteration */
				s = 0.840896 * pow((prec * h) / errorStep, 0.25);
				h *= s;
				/* if integration step is too small or big, we will put default value */
				if (h < H_MIN)
					h = H_MIN;
				if (h > H_MAX)
					h = H_MAX;
			}
		} else {
			/* if error is large, then calculate change of magnetic moments again with smaller step */
			while (errorStep > prec) {
				/* select step for next iteration */
				s = 0.840896 * pow((prec * h) / errorStep, 0.25);
				h *= s;
				/* calculate intermediate values */
				CalculateGrid0(m_k[0], lattice, t, amount, h, pointerFunc);
				CalculateGrid1(m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
				CalculateGrid2(m_k[2], m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
				CalculateGrid3(m_k[3], m_k[2], m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
				CalculateGrid4(m_k[4], m_k[3], m_k[2], m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
				CalculateGrid5(m_k[5], m_k[4], m_k[3], m_k[2], m_k[1], m_k[0], lattice, t, amount, h, pointerFunc, mCh);
				/* calculate change of magnetic moments with change of time */
				calculateSmallChangesMagn(m_k, m_dm4, m_dm5);
				/* only for flexible system */
				if (userVars.flexOnOff == FLEX_ON) {
					/* calculate change of coordinates and velocities with change of time */
					calculateSmallChangesOther(m_k, m_dm4, m_dm5);
				}
				/* calculate error of integration */
				errorStep = CalculateError(m_dm4, m_dm5);
				/* if integration step is too small or big, we will put default value and get out of cycle */
				if (h < H_MIN) {
					h = H_MIN;
					break;
				}
				if (h > H_MAX) {
					h = H_MAX;
					break;
				}
			}
			/* if error is small, then work further */
			t = t + h;
			addSmallChanges(lattice, m_dm5);
		}
		/* update values after step */
		result = updateValuesAfterStepMPI(t);
		if (result) {
			break;
		}
		/* save current step */
		intStep = h;
		/* to debug code */
		//fprintf(stderr, "%f\n", t);
	}

	/* calculate maximum of dm/dt and dr/dt */
	MaxDmDt = CalculateMaxDmDt(m_dm5, h);
	MaxDrDt = CalculateMaxDrDt(m_dm5, h);
	/* check value of magnetic change over time */
	if (MaxDmDt < userVars.stopDmDt) {
		fprintf(stderr, "INTEGRATION.C: too small change of magnetic moment!\n");
		fprintf(flog, "INTEGRATION.C: too small change of magnetic moment!\n");
		result = 1;
	}

	/* add all energies of system */
	addAllEnergies();
	/* copy system array */
	copySystemArray();

	/* free memory from temporary arrays */
	free(mCh);

	/* return value */
	return result;

}

int IntegrateFunctionsMiddlePoint(double t0, double tfin, size_t amount, double prec,
                                  void (*pointerFunc)(double *, double *, size_t, double, double)) {

	/* current step and time */
	double h, t;
	/* value to return */
	int result = 0;

	/* define initial time */
	t = t0;
	/* define integration step */
	h = userVars.integrationStepNorm;

	while (t <= tfin) {
		/* calculate the grid for half of step */
		CalculateGrid0(m_dmHalf, lattice, t, amount, h / 2.0, pointerFunc);
		/* receive new intermediate values */
		addSmallChangesToArray(latticeHalf, lattice, m_dmHalf);
		/* calculate the grid for full of step */
		CalculateGrid0(m_dmFinal, latticeHalf, t + h / 2.0, amount, h, pointerFunc);
		/* add time and small changes */
		t = t + h;
		addSmallChanges(lattice, m_dmFinal);
		/* update values after step */
		result = updateValuesAfterStepMPI(t);
		if (result) {
			break;
		}
		/* save current step */
		intStep = h;
	}

	/* calculate maximum of dm/dt and dr/dt */
	MaxDmDt = CalculateMaxDmDt(m_dmFinal, h);
	MaxDrDt = CalculateMaxDrDt(m_dmFinal, h);
	/* check value of magnetic change over time */
	if (MaxDmDt < userVars.stopDmDt) {
		fprintf(stderr, "INTEGRATION.C: too small change of magnetic moment!\n");
		fprintf(flog, "INTEGRATION.C: too small change of magnetic moment!\n");
		result = 1;
	}

	/* add all energies of system */
	addAllEnergies();
	/* copy system array */
	copySystemArray();

	/* return value */
	return result;

}

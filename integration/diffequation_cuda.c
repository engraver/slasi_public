#include "../interactions/interactions.h"
#include "../interactions/externalField/externalField.h"
#include "../utils/synchronize.h"
#include "diffequation.h"
#include "diffequation_shared.h"
#include "diffutils_cuda.h"

void DiffEquation(double *k, double *m, size_t amount, double t, double h) {

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;
	/* fill zero array of effective field */
	fillZeroEffective();
	fillZeroEnergies();

	/* launch functions to find effective field and energy in parallel */
	/* exchange interaction */
	if (userVars.exchOnOff == EXCH_ON) {
		if (userVars.exchType == EXCH_UNIFORM) {
			uniformExchangeFieldCUDA(d_H, d_EnergyExchangeArray, userVars.J, userVars.N2, userVars.N3, numberSites);
		} else if (userVars.exchType == EXCH_INHOMOGENEOUS) {
			inhomogeneousExchangeFieldCUDA(d_H, d_latticeParam, d_EnergyExchangeArray, userVars.N2, userVars.N3, numberSites);
		}
	}
	synchronizeThreads();
	/* anisotropy */
	uniaxialAnisotropyFieldCUDA(d_H, d_EnergyAnisotropyArrayAxisK1, d_EnergyAnisotropyArrayAxisK2, d_EnergyAnisotropyArrayAxisK3,
	                            d_latticeParam, numberSites);
	synchronizeThreads();
	/* external field */
	if (userVars.extField == EXTFIELD_CONST) {
		externalUniformFieldCUDA(d_H, userVars.Bx, userVars.By, userVars.Bz, d_EnergyExternalArray, userVars.NormExternal, numberSites);
	} else {
		externalGeneralFieldCUDA(d_H, d_EnergyExternalArray, d_latticeParam, userVars.NormExternal, numberSites);
	}
	synchronizeThreads();
	/* dipole interaction */
	if (userVars.dipOnOff != DIP_OFF) {
		if (userVars.flexOnOff == FLEX_OFF && userVars.dipOnOff == DIP_ON) {
			dipoleFieldCachedCUDA(d_H, d_EnergyDipoleArray, numberSites);
		} else {
			dipoleFieldCUDA(d_H, d_EnergyDipoleArray, numberSites);
		}
		synchronizeThreads();
	}
	/* DMI interaction */
	if (userVars.dmiOnOff != DMI_OFF) {
		dmiFieldCUDA(d_H, d_EnergyDMIAxis1Array, d_EnergyDMIAxis2Array, d_EnergyDMIAxis3Array, numberSites);
	}

	/* launch function to find "small changes" */
	calculateSmallChangesCUDA(d_H, m, k, h, userVars.gilbertDamping, numberSites);
	/* synchronize CUDA-threads */
	synchronizeThreads();

}

void DiffEquationTriangle(double *k, double *m, size_t amount, double t, double h) {

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;
	/* fill zero array of effective field */
	fillZeroEffective();
	fillZeroEnergies();

	/* launch functions to find effective field and energy in parallel */
	/* exchange interaction */
	if (userVars.exchOnOff == EXCH_ON) {
		if (userVars.exchType == EXCH_UNIFORM) {
			uniformExchangeFieldTriangleCUDA(d_H, d_EnergyExchangeArray, userVars.J, userVars.N2, userVars.N3,
			                                 numberSites);
		} else if (userVars.exchType == EXCH_INHOMOGENEOUS) {
			inhomogeneousExchangeFieldTriangleCUDA(d_H, d_latticeParam, d_EnergyExchangeArray, userVars.N2, userVars.N3,
			                                 numberSites);
		}
	}
	synchronizeThreads();
	/* anisotropy */
	uniaxialAnisotropyFieldTriangleCUDA(d_H, d_EnergyAnisotropyArrayAxisK1, d_EnergyAnisotropyArrayAxisK3,
	                                    d_latticeParam, numberSites);
	synchronizeThreads();
	/* external field */
	if (userVars.extField == EXTFIELD_CONST) {
		externalUniformFieldCUDA(d_H, userVars.Bx, userVars.By, userVars.Bz, d_EnergyExternalArray, userVars.NormExternal, numberSites);
	} else {
		externalGeneralFieldCUDA(d_H, d_EnergyExternalArray, d_latticeParam, userVars.NormExternal, numberSites);
	}
	synchronizeThreads();
	/* dipole interaction */
	if (userVars.dipOnOff != DIP_OFF) {
		if (userVars.flexOnOff == FLEX_OFF && userVars.dipOnOff == DIP_ON) {
			dipoleFieldCachedCUDA(d_H, d_EnergyDipoleArray, numberSites);
		} else {
			dipoleFieldCUDA(d_H, d_EnergyDipoleArray, numberSites);
		}
		synchronizeThreads();
	}
	/* interactions for flexible system (Newton's equation) */
	//if (userVars.flexOnOff == FLEX_ON) {
		/* force and energy of stretching */
	//	stretchingForceAndEnergyTriangleCUDA(d_F, d_EnergyStretchingArray, numberSites);
	//}

	/* launch function to find "small changes" */
	calculateSmallChangesCUDA(d_H, m, k, h, userVars.gilbertDamping, numberSites);
	/* synchronize CUDA-threads */
	synchronizeThreads();

}


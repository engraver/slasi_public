#include "../alltypes.h"
extern "C" {
	#include "integutils_cuda_memory.h"
}

extern "C"
void createDoubleArraysCUDA(double **array, size_t size) {

	/* create arrays for simulation */
	cudaMalloc(array, size * sizeof(double));
	cudaMemset(*array, 0, size * sizeof(double));

}

extern "C"
void deleteDoubleArraysCUDA(double *array) {
	cudaFree(array);
}

extern "C"
void copySystemArrayCUDA(void) {

	/* copy array "d_lattice" */
	cudaMemcpy(lattice, d_lattice, NUMBER_COMP * userVars.N1 * userVars.N2 * userVars.N3 * sizeof(double), cudaMemcpyDeviceToHost);

}

extern "C"
void copyExternalFieldToDevice() {

    /* copy array "d_Bamp" to device memory */
    cudaMemcpy(d_Bamp, Bamp, NUMBER_COMP_FIELD * sizeof(double) * userVars.N1 * userVars.N2 * userVars.N3, cudaMemcpyHostToDevice);

}


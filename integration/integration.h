#ifndef _INTERGRATION_H_
#define _INTERGRATION_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif
#include "../alltypes.h"
#include "../writing/writing.h"
#include "../interactions/externalField/fieldSetupInt.h"
#include "../interactions/externalForce/forceSetupInt.h"
#include "../utils/indices.h"

/**
  * @brief This function calculates total amount of energy for magnetic system
  * with special supplement (HUGE_VALUE*(1-mx^2-my^2-mz^2)^2) to limit length of magnetic vector
  * and gradient for each component of the magnetic vector
  * @param n number of components of magnetic vector
  * @param m all components of the magnetic vector
  * @param grad all gradients of component of magnetic vector
  * @param my_func_data required variable for signature in NLopt
  * @return complete system energy with special supplement
  */
double FunctionEnergy(unsigned n, const double *m, double *grad, void *my_func_data);

/**
 * @brief This function calculates total magnetic moments per axis
 * @param number of elements in array lattice
 */
void CalculateTotalMagneticMoments(size_t amount);

/**
 * @brief This function launches the magnetic simulator,
 * which is based on the integration of LLG equations, and records results with given frequency.
 * @param t0 initial value of the time
 * @param tfin final value of the time
 * @param frame frame time to record results
 * @param prec determines the precision of the calculation
 * @param flog_ON variable, which means whether to record results into log file (it is necessary for tests)
 * @return 0 in the case of absence of errors
 */
int LaunchTimeDynamics(double t0, double tfin, double frame, double prec, bool flog_ON);

/**
 * @brief This function launches optimization
 * using NLopt to find state of magnetic system with minimal energy
 * @param flog_ON variable, which means whether to record results into log file (it is necessary for tests)
 * @return 0 in the case of absence of errors
 */
int LaunchOptimization(bool flog_ON);

/**
 * @brief This function launches hysteresis
 * using NLopt to find state of magnetic system
 * with minimal energy and different external field
 * @param flog_ON variable, which means whether to record results into log file (it is necessary for tests)
 * @return 0 in the case of absence of errors
 */
int LaunchHysteresis(bool flog_ON);

#endif

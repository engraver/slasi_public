#include "integutils_cuda_memory.h"
#include "integutils_cuda_diff.h"
#include "integutils.h"
#include "../interactions/externalField/fieldSetupInt.h"
#include "../interactions/externalForce/forceSetupInt.h"
#include "../utils/utils.h"
#include "../utils/parallel_utils.h"
#include "../alltypes.h"
#include "diffequation.h"

void findEnergy(double *RKF, size_t spinsNumber, double t0, double step) {

	/* find out initial energy and other parameters of magnetic system */
	if (userVars.latticeType == LATTICE_CUBIC) {
		DiffEquation(RKF, d_lattice, spinsNumber, t0, step);
	}
	else if (userVars.latticeType == LATTICE_TRIANGULAR) {
		DiffEquationTriangle(RKF, d_lattice, spinsNumber, t0, step);
	}

}

int updateValuesAfterStepCUDA(double t) {

	/* value to return */
	int result = 0;

	/* normalize vectors of magnetic moments */
	if (userVars.normalOnOff == NORMAL_ON)
		NormalizeVectorsCUDA();

	/* setup external magnetic field */
	if (userVars.extField == EXTFIELD_SCRIPT) {
		/* dividing using "userVars.NormCoefficientTime" because of normalization of time */
		result = fieldSetupInt(t / userVars.NormCoefficientTime);
		copyExternalFieldToDevice();
	}

	/* return result */
	return result;

}

int IntegrateFunctionsRKF(double t0, double tfin, size_t amount, double prec,
                       void (*pointerFunc)(double *, double *, size_t, double, double)) {

	/* value to return */
	double h, t;

	/* define initial time */
	t = t0;
	/* define integration step */
	h = userVars.integrationStepNorm;
	/* define variable to save return code */
	int result = 0;

	while (t <= tfin) {

		/* counter for debugging */
		//number++;

		/* calculate intermediate values */
		CalculateGrid0CUDA(d_kHost[0], d_lattice, t, amount, h, pointerFunc);
		CalculateGrid1CUDA(d_kHost[1], d_kHost[0], d_lattice, t, amount, h, pointerFunc);
		CalculateGrid2CUDA(d_kHost[2], d_kHost[1], d_kHost[0], d_lattice, t, amount, h, pointerFunc);
		CalculateGrid3CUDA(d_kHost[3], d_kHost[2], d_kHost[1], d_kHost[0], d_lattice, t, amount, h, pointerFunc);
		CalculateGrid4CUDA(d_kHost[4], d_kHost[3], d_kHost[2], d_kHost[1], d_kHost[0], d_lattice, t, amount, h, pointerFunc);
		CalculateGrid5CUDA(d_kHost[5], d_kHost[4], d_kHost[3], d_kHost[2], d_kHost[1], d_kHost[0], d_lattice, t, amount, h, pointerFunc);
		//showArrayCUDA(d_kHost[0]);
		/* calculate change of magnetic moments with change of time */
		calculateSmallChangesMagnCUDA(d_k, d_dm5);
		/* only for flexible system */
		if (userVars.flexOnOff == FLEX_ON) {
			/* calculate change of coordinates and velocities with change of time */
			calculateSmallChangesOtherCUDA(d_k, d_dm5);
		}

		/* to debug code */
		//if (number == 1) {
		//	showArrayCUDA(d_dm4);
		//}	

		/* to debug code */
		/* if (number == 1) {
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					long curIndex = IDXmg(1, 1, 1);
					printf("k0 %li %d %d %d %.10f %.10f %.10f %.10f\n", curIndex, 1, 1, 1, k[0][curIndex], k[0][curIndex + 1], k[0][curIndex + 2], h);
					printf("dm5 %li %d %d %d %.10f %.10f %.10f\n", curIndex, 1, 1, 1, dm5[curIndex], dm5[curIndex + 1], dm5[curIndex + 2]);
				}
			}
		}
		} */
		//printf("%li %f\n", number, h);
		//nanosleep(&tw, &tr);
		//if (number == 1)
		//	showMagnWrapper();

		/* if error is small, then work further */
		t = t + h;
		addSmallChangesCUDA(d_lattice, d_dm5);

		/* update values after step */
		result = updateValuesAfterStepCUDA(t);
		if (result) {
			break;
		}

		/* save current step */
		intStep = h;

		/* to debug code */
		//fprintf(stderr, "%f\n", t);
		//sleep(1);

	}

	/* calculate maximum of dm/dt and dr/dt */
	MaxDmDt = CalculateMaxDmDtCUDA(d_dm5, h);
	MaxDrDt = CalculateMaxDrDtCUDA(d_dm5, h);
	/* check value of magnetic change over time */
	if (MaxDmDt < userVars.stopDmDt) {
		fprintf(stderr, "INTEGRATION.C: too small change of magnetic moment!\n");
		fprintf(flog, "INTEGRATION.C: too small change of magnetic moment!\n");
		result = 1;
	}

	/* add all energies of system */
	addAllEnergiesCUDA();
	/* copy system array from device to main memory */
	copySystemArrayCUDA();

	/* return value */
	return result;

}

int IntegrateFunctionsMiddlePoint(double t0, double tfin, size_t amount, double prec,
                                  void (*pointerFunc)(double *, double *, size_t, double, double)) {

	/* current step and time */
	double h, t;
	/* value to return */
	int result = 0;

	/* define initial time */
	t = t0;
	/* define integration step */
	h = userVars.integrationStepNorm;

	while (t <= tfin) {
		/* calculate the grid for half of step */
		CalculateGrid0CUDA(d_dmHalf, d_lattice, t, amount, h / 2.0, pointerFunc);
		/* receive new intermediate values */
		addSmallChangesToArrayCUDA(d_latticeHalf, d_lattice, d_dmHalf);
		/* calculate the grid for full of step */
		CalculateGrid0CUDA(d_dmFinal, d_latticeHalf, t + h / 2.0, amount, h, pointerFunc);
		/* add time and small changes */
		t = t + h;
		addSmallChangesCUDA(d_lattice, d_dmFinal);
		/* update values after step */
		result = updateValuesAfterStepCUDA(t);
		if (result) {
			break;
		}
		/* save current step */
		intStep = h;
	}

	/* calculate maximum of dm/dt and dr/dt */
	MaxDmDt = CalculateMaxDmDtCUDA(d_dmFinal, h);
	MaxDrDt = CalculateMaxDrDtCUDA(d_dmFinal, h);
	/* check value of magnetic change over time */
	if (MaxDmDt < userVars.stopDmDt) {
		fprintf(stderr, "INTEGRATION.C: too small change of magnetic moment!\n");
		fprintf(flog, "INTEGRATION.C: too small change of magnetic moment!\n");
		result = 1;
	}

	/* add all energies of system */
	addAllEnergiesCUDA();
	/* copy system array */
	copySystemArrayCUDA();

	/* return value */
	return result;

}
#include <stdio.h>
#include "../alltypes.h"
extern "C" {
	#include "diffutils_cuda.h"
}

/* function to calculate "small changes" (GPU) */
__global__
void calculateSmallChanges(double *d_H, double *d_m, double *d_dm, double step, double gilbertDamping, long numberSites, long number) {

	/* find indices of site */
	long indexLattice = blockIdx.x * blockDim.x + threadIdx.x;
	long curIndex = 3 * indexLattice;

	/* check boundary condition for threads */
	if ((indexLattice < numberSites) &&
	    (d_m[curIndex] * d_m[curIndex] + d_m[curIndex + 1] * d_m[curIndex + 1] + d_m[curIndex + 2] * d_m[curIndex + 2] > LENG_ZERO))  {

		/* initialization */
		/* magnetic moments */
		double mx = d_m[curIndex];
		double my = d_m[curIndex + 1];
		double mz = d_m[curIndex + 2];
		/* effective field */
		double Hx = d_H[curIndex];
		double Hy = d_H[curIndex + 1];
		double Hz = d_H[curIndex + 2];

		/* intermediate calculation for LLG */
		double ax = my * Hz - mz * Hy;
		double ay = mz * Hx - mx * Hz;
		double az = mx * Hy - my * Hx;
		
		/* calculate "small changes" */
		d_dm[curIndex] = step * ((mz * Hy - my * Hz) - gilbertDamping * (my * az - mz * ay));
		d_dm[curIndex + 1] = step * ((mx * Hz - mz * Hx) + gilbertDamping * (mx * az - mz * ax));
		d_dm[curIndex + 2] = step * ((my * Hx - mx * Hy) - gilbertDamping * (mx * ay - my * ax));
	
	}

	//printf("%f\n", d_m[curIndex]);
	//printf("%f\n", d_m[curIndex + 1]);
	//printf("%f\n", d_m[curIndex + 2]);

	//if (number == 1) {
	//	printf("1111\n");
	//}

}

extern "C"
void calculateSmallChangesCUDA(double *d_H, double *d_m, double *d_dm, double step, double gilbertDamping, long numberSites) {

	/* setup the execution configuration */
	dim3 cores(numberSites / THREADS_CORE + 1, 1);
	dim3 threads(THREADS_CORE, 1);

	/* to debug code */
	//if (number == 1) {
	//	printf("%li", numberSites / THREADS_CORE + 1);
	//	printf("%li", THREADS_CORE);
	//}
	/* calculate "small changes" using GPU */
	calculateSmallChanges<<<cores,threads>>>(d_H, d_m, d_dm, step, gilbertDamping, numberSites, number);

}

/* function to synchronize execution of CUDA-threads */
extern "C"
void synchronizeThreads(void) {

	cudaDeviceSynchronize();

}

extern "C"
void fillZeroEffective(void) {

	/* number of components */
	long numberComp = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;
	/* fill array of efective field using zero */
	cudaMemset(d_H, 0, numberComp * sizeof(double));

}

extern "C"
void fillZeroEnergies(void) {

	/* number of sites */
	long numberSites = userVars.N1 * userVars.N2 * userVars.N3;

	/* fill arrays of energies using zero */
	cudaMemset(d_EnergyExchangeArray, 0, numberSites * sizeof(double));
	cudaMemset(d_EnergyAnisotropyArrayAxisK1, 0, numberSites * sizeof(double));
	cudaMemset(d_EnergyAnisotropyArrayAxisK2, 0, numberSites * sizeof(double));
	cudaMemset(d_EnergyAnisotropyArrayAxisK3, 0, numberSites * sizeof(double));
	cudaMemset(d_EnergyDipoleArray, 0, numberSites * sizeof(double));
	cudaMemset(d_EnergyExternalArray, 0, numberSites * sizeof(double));
	if (userVars.latticeType == LATTICE_CUBIC) {
		cudaMemset(d_EnergyDMIAxis1Array, 0, numberSites * sizeof(double));
		cudaMemset(d_EnergyDMIAxis2Array, 0, numberSites * sizeof(double));
		cudaMemset(d_EnergyDMIAxis3Array, 0, numberSites * sizeof(double));
	} else {
		cudaMemset(d_EnergyDMITriangleBulkArray, 0, numberSites * sizeof(double));
		cudaMemset(d_EnergyDMITriangleInterfaceArray, 0, numberSites * sizeof(double));
		cudaMemset(d_EnergyStretchingArray, 0, numberSites * sizeof(double));
		cudaMemset(d_EnergyBendingArray, 0, numberSites * sizeof(double));
	}

}
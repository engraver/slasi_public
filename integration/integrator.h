#ifndef INTEGRATOR_H
#define INTEGRATOR_H

#include <glob.h>

/**
 * @brief This function solves the Cauchy problem by the Runge-Kutti-Feelberg method with a given precision
 * for equations that is in pointerFunc.
 * @param t0 initial value of the argument
 * @param tfin final value of the argument
 * @param amount number of variables for the Cauchy problem
 * @param prec determines the precision of the calculation
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 * @return 0 in the case of absence of errors
 */
int IntegrateFunctionsRKF(double t0, double tfin, size_t amount, double prec, void (*pointerFunc) (double *, double *, size_t, double, double));

/**
 * @brief This function solves the Cauchy problem by the middle point method
 * for equations that is in pointerFunc.
 * @param t0 initial value of the argument
 * @param tfin final value of the argument
 * @param amount number of variables for the Cauchy problem
 * @param prec determines the precision of the calculation
 * @param pointerFunc pointer to a function that has equations for the Cauchy problem
 * @return 0 in the case of absence of errors
 */
int IntegrateFunctionsMiddlePoint(double t0, double tfin, size_t amount, double prec, void (*pointerFunc) (double *, double *, size_t, double, double));


/**
 * @brief This function calls integrator to find out initial energy
 * @param RKF intermatiate array of RKF45
 * @param spinsNumber number of components for spins
 * @param t0 initial time
 * @param step of time
 */
void findEnergy(double *RKF, size_t spinsNumber, double t0, double step);

#endif


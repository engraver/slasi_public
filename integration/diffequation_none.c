#include "../interactions/interactions.h"
#include "../interactions/externalField/externalField.h"
#include "diffequation_shared.h"
#include "unistd.h"

void DiffEquation(double *k, double *m, size_t amount, double t, double h) {

	/* components of magnetic moment */
	double mx, my, mz;
	/* components of effective field */
	double Hx, Hy, Hz;
	/* components of force */
	double Fx, Fy, Fz;
	/* components of velocity */
	//double vx, vy, vz;
	/* intermediate values for LLG */
	double ax, ay, az;
	/* variables to go through all nodes */
	size_t indexLattice, curIndex;
	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* initialize different kinds of energy */
	initializeEnergies();
	/* check boundary condition */
	checkBoundaryCondition(m);

	/* to debug code */
	/*printf("############################################################\n");
	{
		int i1 = 3, i2 = 1, i3 = 1;
		size_t curIndex = INDEXmx(i1,i2,i3);
		size_t curIndex1 = INDEXmx(i1+1,i2,i3);
		// ((i1*userVars.N2 + i2)*3*userVars.N3 + i3)

		 size_t  p1 = curIndex + 3*userVars.N3*userVars.N2;
		 size_t  p2 = curIndex + 3*userVars.N3;
		 size_t  p3 = curIndex + 3;

		printf(" @@@1 %d %d | %d | %d\n", curIndex, curIndex1, p1, 3*userVars.N3*userVars.N2);
		curIndex1 = INDEXmx(i1,i2+1,i3);
		printf(" @@@1 %d %d | %d\n", curIndex, curIndex1, p2);
		curIndex1 = INDEXmx(i1,i2,(i3+1));
		printf(" @@@1 %d %d | %d\n", curIndex, curIndex1, p3);

	}

	for (int i1 = 0; i1 < userVars.N1; i1 ++) {
		for (int i2 = 0; i2 < userVars.N2; i2 ++) {
			for (int i3 = 0; i3 < userVars.N3; i3 ++) {
				curIndex = INDEXmx(i1,i2,i3);
i				printf("### %d %d %d | %lg %lg %lg\n", i1, i2, i3, m[curIndex], m[curIndex+1], m[curIndex+2]);
			}
		}
	}*/

	/* pass through each magnetic moment */
	int i1, i2, i3;
	for (i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {

				/* number of spin in array "latticeParam" */
				indexLattice = IDX(i1, i2, i3);
				/* check if node is magnetic */
				if (latticeParam[indexLattice].magnetic) {

					/* number of spin in array "lattice" */
					curIndex = IDXmg(i1, i2, i3);

					/* initialization */

					/* magnetic moments */
					mx = m[curIndex];
					my = m[curIndex + 1];
					mz = m[curIndex + 2];
					/* effective field */
					Hx = 0.0;
					Hy = 0.0;
					Hz = 0.0;
					/* force */
					Fx = latticeParam[indexLattice].Fx;
					Fy = latticeParam[indexLattice].Fy;
					Fz = latticeParam[indexLattice].Fz;
					/* velocity */
					//vx = m[curIndex + 2 * spinsNumber];
					//vy = m[curIndex + 2 * spinsNumber + 1];
					//vz = m[curIndex + 2 * spinsNumber + 2];

					//	printf("## i = %d\n\t", i1);
					/* calculate effective magnetic field and normalized energy of diffetent interaction */
					/* exchange interaction */
					if (userVars.exchOnOff == EXCH_ON) {
						if (userVars.exchType == EXCH_UNIFORM) {
							uniformExchangeField(curIndex, indexLattice, &Hx, &Hy, &Hz, &EnergyExchangeNorm);
						} else if (userVars.exchType == EXCH_INHOMOGENEOUS) {
							inhomogeneousExchangeField(curIndex, indexLattice, &Hx, &Hy, &Hz, &EnergyExchangeNorm);
						}
					}
					/* anisotropy */
					uniaxialAnisotropyField(curIndex, indexLattice, &Hx, &Hy, &Hz,
					                        &EnergyAnisotropyNormAxisK1, &EnergyAnisotropyNormAxisK2,
					                        &EnergyAnisotropyNormAxisK3);
					/* external field */
					if (userVars.extField == EXTFIELD_CONST) {
						externalUniformField(curIndex, indexLattice, &Hx, &Hy, &Hz, &EnergyExternalNorm);
					} else {
						externalGeneralField(curIndex, indexLattice, &Hx, &Hy, &Hz, &EnergyExternalNorm);
					}
					/* dipole interaction */
					if (userVars.dipOnOff != DIP_OFF) {
						if (userVars.flexOnOff == FLEX_OFF && userVars.dipOnOff == DIP_ON) {
							dipoleFieldCached(curIndex, indexLattice, &Hx, &Hy, &Hz, &EnergyDipoleNorm);
						} else {
							dipoleField(curIndex, indexLattice, &Hx, &Hy, &Hz, &EnergyDipoleNorm);
						}
					}
					/* Dzyaloshinsky-Moria interaction */
					if (userVars.dmiOnOff != DMI_OFF) {
						dmiField(curIndex, indexLattice, &Hx, &Hy, &Hz, &EnergyDMIAxis1Norm,
						         &EnergyDMIAxis2Norm, &EnergyDMIAxis3Norm);
					}
					//dmiBulkFieldCubic(i1, i2, i3, curIndex, indexLattice, &Hx, &Hy, &Hz, &EnergyDMITriangleBulkNorm);
					/* interactions for flexible system */
					if (userVars.flexOnOff == FLEX_ON) {
						/* force and energy of stretching */
						stretchingForceAndEnergy(i1, curIndex, indexLattice, &Fx, &Fy, &Fz, &EnergyStretchingNorm);
						/* force and energy of bending */
						bendingForceAndEnergy(i1, curIndex, indexLattice, &Fx, &Fy, &Fz, &EnergyBendingNorm);
						/* force of anisotropy */
						uniaxialAnisotropyForce(i1, curIndex, indexLattice, &Fx, &Fy, &Fz);
					}

					/* intermediate calculation for LLG */
					ax = my * Hz - mz * Hy;
					ay = mz * Hx - mx * Hz;
					az = mx * Hy - my * Hx;

					/* calculate intermediate values for Runge-Kutta-Fehlberg */

					/* changes of magnetic moments for LLG equation */
					k[curIndex] = h * ((mz * Hy - my * Hz) - userVars.gilbertDamping * (my * az - mz * ay));
					k[curIndex + 1] = h * ((mx * Hz - mz * Hx) + userVars.gilbertDamping * (mx * az - mz * ax));
					k[curIndex + 2] = h * ((my * Hx - mx * Hy) - userVars.gilbertDamping * (mx * ay - my * ax));

					if (userVars.flexOnOff == FLEX_ON) {
						/* changes of coordinates for damping equation */
						k[curIndex + spinsNumber] = (h * Fx) / (latticeParam[indexLattice].mechDamping);
						k[curIndex + spinsNumber + 1] = (h * Fy) / (latticeParam[indexLattice].mechDamping);
						k[curIndex + spinsNumber + 2] = (h * Fz) / (latticeParam[indexLattice].mechDamping);
						//fprintf(stderr,"%f",latticeParam[indexLattice].mechDamping);
						/* changes of velocities for damping equation */
						k[curIndex + 2 * spinsNumber] = 0.0;
						k[curIndex + 2 * spinsNumber + 1] = 0.0;
						k[curIndex + 2 * spinsNumber + 2] = 0.0;
					}

				}

			}
		}
	}


}

void DiffEquationTriangle(double *k, double *m, size_t amount, double t, double h) {

	/* components of magnetic moment */
	double mx, my, mz;
	/* components of effective field */
	double Hx, Hy, Hz;
	/* components of force */
	double Fx, Fy, Fz;
	/* components of velocity */
	/* it will be extension of Newton equation */
	// double vx, vy, vz;
	/* intermediate values for LLG */
	double ax, ay, az;
	/* variables to go through all nodes */
	size_t indexLattice, curIndex;
	/* number of components of spins in system */
	size_t spinsNumber = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* initialize different kinds of energy */
	initializeEnergies();
	/* check boundary condition */
	checkBoundaryCondition(m);

	/* pass through each magnetic moment (for triangular lattice) */
	int i1, i2;
	for (i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {

			/* number of spin in array "latticeParam" */
			indexLattice = IDXtr(i1, i2);
			/* check if node is magnetic */
			if (latticeParam[indexLattice].magnetic) {

				/* number of spin in array "lattice" */
				curIndex = IDXtrmg(i1, i2);

				/* initialization */

				/* magnetic moments */
				mx = m[curIndex];
				my = m[curIndex + 1];
				mz = m[curIndex + 2];
				/* effective field */
				Hx = 0.0;
				Hy = 0.0;
				Hz = 0.0;
				/* force */
				Fx = latticeParam[indexLattice].Fx;
				Fy = latticeParam[indexLattice].Fy;
				Fz = latticeParam[indexLattice].Fz;
				/* velocity */
				/* it will be extension of Newton equation */
				//vx = m[curIndex + 2 * spinsNumber];
				//vy = m[curIndex + 2 * spinsNumber + 1];
				//vz = m[curIndex + 2 * spinsNumber + 2];

				//	printf("## i = %d\n\t", i1);
				/* calculate effective magnetic field and normalized energy of diffetent interaction */
				/* exchange interaction */
				if (userVars.exchOnOff == EXCH_ON) {
					if (userVars.exchType == EXCH_UNIFORM) {
						uniformExchangeFieldTriangle(i1, i2, curIndex, indexLattice, &Hx, &Hy, &Hz,
						                             &EnergyExchangeNorm);
					} else if (userVars.exchType == EXCH_INHOMOGENEOUS) {
						inhomogeneousExchangeFieldTriangle(i1, i2, curIndex, indexLattice, &Hx, &Hy, &Hz,
						                                   &EnergyExchangeNorm);
					}
				}
				/* anisotropy */
				uniaxialAnisotropyFieldTriangle(i1, curIndex, indexLattice, &Hx, &Hy, &Hz,
				                                &EnergyAnisotropyNormAxisK1, &EnergyAnisotropyNormAxisK3);
				/* external field */
				if (userVars.extField == EXTFIELD_CONST) {
					externalUniformField(curIndex, indexLattice, &Hx, &Hy, &Hz, &EnergyExternalNorm);
				} else if ((userVars.extField == EXTFIELD_FILE) || (userVars.extField == EXTFIELD_SCRIPT) ||
				           (userVars.extField == EXTFIELD_HARMTIME)) {
					externalGeneralField(curIndex, indexLattice, &Hx, &Hy, &Hz, &EnergyExternalNorm);
				}
				/* dipole interaction */
				if (userVars.dipOnOff != DIP_OFF) {
					if (userVars.flexOnOff == FLEX_OFF && userVars.dipOnOff == DIP_ON) {
						dipoleFieldCached(curIndex, indexLattice, &Hx, &Hy, &Hz, &EnergyDipoleNorm);
					} else {
						dipoleField(curIndex, indexLattice, &Hx, &Hy, &Hz, &EnergyDipoleNorm);
					}
				}
				/* Dzyaloshinsky-Moria interaction */
				if (userVars.dmiOnOff != DMI_OFF) {
					dmiBulkFieldTriangle(i1, i2, curIndex, indexLattice, &Hx, &Hy, &Hz, &EnergyDMITriangleBulkNorm);
					dmiInterfaceFieldTriangle(i1, i2, curIndex, indexLattice, &Hx, &Hy, &Hz,
					                          &EnergyDMITriangleInterfaceNorm);
				}
				/* interactions for flexible system */
				if (userVars.flexOnOff == FLEX_ON) {
					/* force and energy of stretching */
					stretchingForceAndEnergyTriangle(i1, i2, curIndex, indexLattice, &Fx, &Fy, &Fz, &EnergyStretchingNorm);
					/* force and energy of bending */
					bendingForceAndEnergyTriangle(i1, i2, curIndex, indexLattice, &Fx, &Fy, &Fz, &EnergyBendingNorm);
					/* force of tangential anisotropy */
					uniaxialAnisotropyTangForceTriangle(i1, i2, curIndex, indexLattice, &Fx, &Fy, &Fz);
					/* force of normal anisotropy */
					uniaxialAnisotropyNormForceTriangle(i1, i2, &Fx, &Fy, &Fz);
					/* force of bulk DMI interaction */
					dmiBulkForceTriangle(i1, i2, curIndex, indexLattice, &Fx, &Fy, &Fz);
					/* force of interface DMI interaction */
					dmiInterfaceForceTriangle(i1, i2, &Fx, &Fy, &Fz);
				}

				/* intermediate calculation for LLG */
				ax = my * Hz - mz * Hy;
				ay = mz * Hx - mx * Hz;
				az = mx * Hy - my * Hx;

				/* calculate intermediate values for Runge-Kutta-Fehlberg */

				/* changes of magnetic moments for LLG equation */
				k[curIndex] = h * ((mz * Hy - my * Hz) - userVars.gilbertDamping * (my * az - mz * ay));
				k[curIndex + 1] = h * ((mx * Hz - mz * Hx) + userVars.gilbertDamping * (mx * az - mz * ax));
				k[curIndex + 2] = h * ((my * Hx - mx * Hy) - userVars.gilbertDamping * (mx * ay - my * ax));

				if (userVars.flexOnOff == FLEX_ON) {
					/* changes of coordinates for damping equation */
					k[curIndex + spinsNumber] = (h * Fx) / (latticeParam[indexLattice].mechDamping);
					k[curIndex + spinsNumber + 1] = (h * Fy) / (latticeParam[indexLattice].mechDamping);
					k[curIndex + spinsNumber + 2] = (h * Fz) / (latticeParam[indexLattice].mechDamping);
					/* to debug code */
					//fprintf(stderr,"%f",latticeParam[indexLattice].mechDamping);
					/* changes of velocities for damping equation */
					k[curIndex + 2 * spinsNumber] = 0.0;
					k[curIndex + 2 * spinsNumber + 1] = 0.0;
					k[curIndex + 2 * spinsNumber + 2] = 0.0;
				}

			}

		}
	}

	// to debug code
	//fprintf(stderr, "%f\n", EnergyDMITriangleNorm);

}

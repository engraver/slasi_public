#ifndef INTEGUTILS_CUDA_MEMORY_H
#define INTEGUTILS_CUDA_MEMORY_H

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include <sys/types.h>

/**
 * @brief This function creates array of given size in GPU (CUDA)
 * @param array is pointer to pointer of array
 * @param size is size of array
 */
void createDoubleArraysCUDA(double **array, size_t size);

/**
 * @brief This function copies system array if it is necessary (CUDA)
 */
void copySystemArrayCUDA(void);

/**
 * @brief This function deletes array
 * @param array is pointer of array
 */
void deleteDoubleArraysCUDA(double *array);

/**
 * @brief This function copies array of external field amplitude "Bamp" to device
 */
void copyExternalFieldToDevice(void);

#endif

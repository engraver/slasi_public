#include "integutils.h"
#include "integutils_cuda_memory.h"
#include "integutils_cuda_diff.h"

/* wrappers for CUDA-function */
void createDoubleArrays(double **array, size_t size) {
	createDoubleArraysCUDA(array, size);
}

void deleteDoubleArrays(double *array) {
	deleteDoubleArraysCUDA(array);
}

void copySystemArray(void) {
	copySystemArrayCUDA();
}

void addAllEnergies(void) {
	addAllEnergiesCUDA();
}
#include <string.h>
#include "diffequation_shared.h"

/* function to initialize different kind of energy */
void initializeEnergies(void) {

	/* assign zero for all kinds of energy */
	EnergyExchangeNorm = 0.0;
	EnergyAnisotropyNormAxisK1 = 0.0;
	EnergyAnisotropyNormAxisK2 = 0.0;
	EnergyAnisotropyNormAxisK3 = 0.0;
	EnergyDipoleNorm = 0.0;
	EnergyExternalNorm = 0.0;
	EnergyDMIAxis1Norm = 0.0;
	EnergyDMIAxis2Norm = 0.0;
	EnergyDMIAxis3Norm = 0.0;
	EnergyDMITriangleInterfaceNorm = 0.0;
	EnergyDMITriangleBulkNorm = 0.0;
	EnergyStretchingNorm = 0.0;
	EnergyBendingNorm = 0.0;

}

/* function to check boundary condition of system */
void checkBoundaryCondition(double *m) {

	/* indexes for copying */
	size_t curIndex, copyIndex, spinsNum = 3 * userVars.N1 * userVars.N2 * userVars.N3;

	/* checking periodic boundary conditions */

	/* the first axis */
	if (userVars.periodicBC1 == PBC_ON) {
		if (userVars.latticeType == LATTICE_CUBIC) {
			int i1a = 0, i1b = userVars.N1 - 1;
			/* magnetic moments for the first axis */
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					curIndex = IDXmg(i1a, i2, i3);
					copyIndex = IDXmg(i1b - 1, i2, i3);
					memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
					curIndex = IDXmg(i1b, i2, i3);
					copyIndex = IDXmg(i1a + 1, i2, i3);
					memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
				}
			}
			/* coordinates */
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					curIndex = IDXcoor(i1a, i2, i3);
					copyIndex = IDXcoor(i1b - 1, i2, i3);
					memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
					curIndex = IDXcoor(i1b, i2, i3);
					copyIndex = IDXcoor(i1a + 1, i2, i3);
					memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
				}
			}
		} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
			int i1a = 0, i1b = userVars.N1 - 1;
			/* magnetic moments for the first axis */
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					curIndex = IDXtrmg(i1a, i2);
					copyIndex = IDXtrmg(i1b - 1, i2);
					memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
					curIndex = IDXtrmg(i1b, i2);
					copyIndex = IDXtrmg(i1a + 1, i2);
					memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
				}
			}
			/* coordinates */
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
					curIndex = IDXtrmg(i1a, i2) + spinsNum;
					copyIndex = IDXtrmg(i1b - 1, i2) + spinsNum;
					memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
					curIndex = IDXtrmg(i1b, i2) + spinsNum;
					copyIndex = IDXtrmg(i1a + 1, i2) + spinsNum;
					memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
				}
			}
		}
	}

	/* the second axis */
	if (userVars.periodicBC2 == PBC_ON) {
		int i2a = 0, i2b = userVars.N2 - 1;
		/* magnetic nodes for the second axis */
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				curIndex = IDXmg(i1, i2a, i3);
				copyIndex = IDXmg(i1, i2b - 1, i3);
				memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
				curIndex = IDXmg(i1, i2b, i3);
				copyIndex = IDXmg(i1, i2a + 1, i3);
				memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
			}
		}
		/* coordinates */
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				curIndex = IDXcoor(i1, i2a, i3);
				copyIndex = IDXcoor(i1, i2b - 1, i3);
				memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
				curIndex = IDXcoor(i1, i2b, i3);
				copyIndex = IDXcoor(i1, i2a + 1, i3);
				memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
			}
		}
	}

	/* the third axis */
	if (userVars.periodicBC3 == PBC_ON) {
		int i3a = 0, i3b = userVars.N3 - 1;
		/* magnetic nodes for the third axis */
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				curIndex = IDXmg(i1, i2, i3a);
				copyIndex = IDXmg(i1, i2, (i3b - 1));
				memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
				curIndex = IDXmg(i1, i2, i3b);
				copyIndex = IDXmg(i1, i2, (i3a + 1));
				memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
			}
		}
		/* coordinates */
		for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
			for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
				curIndex = IDXcoor(i1, i2, i3a);
				copyIndex = IDXcoor(i1, i2, (i3b - 1));
				memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
				curIndex = IDXcoor(i1, i2, i3b);
				copyIndex = IDXcoor(i1, i2, (i3a + 1));
				memcpy(m + curIndex, m + copyIndex, sizeof(double) * 3);
			}
		}
	}

}

#ifndef _DIFFEQUATION_H_
#define _DIFFEQUATION_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../alltypes.h"
#include "../utils/indices.h"

/**
 * @brief This function contains differential Landau-Lifshitz-Hilbert
 * equations that must be solved by the Runge-Kutti-Feelberg method
 * @param k array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of spins for the Cauchy problem
 * @param amount number of spins for the Cauchy problem
 * @param t current time
 * @param h current integration step
 */
void DiffEquation(double *k, double *m, size_t amount, double t, double h);

/**
 * @brief This function contains differential Landau-Lifshitz-Hilbert
 * equations for triangular lattice that must be solved by the Runge-Kutti-Feelberg method
 * @param k array of intermediate values that is used for Runge-Kutti-Feelberg
 * @param m current values of spins for the Cauchy problem
 * @param amount number of spins for the Cauchy problem
 * @param t current time
 * @param h current integration step
 */
void DiffEquationTriangle(double *k, double *m, size_t amount, double t, double h);

#endif
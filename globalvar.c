#include <Python.h>
#include "alltypes.h"

/**
 * @brief global variables for all project
 * */

/* energy of system of magnetic spins and subspecies of this energy */
double EnergyOfSystem;
double EnergyExchange;
double EnergyAnisotropyAxisK1;
double EnergyAnisotropyAxisK2;
double EnergyAnisotropyAxisK3;
double EnergyDipole;
double EnergyExternal;
double EnergyDMIAxis1;
double EnergyDMIAxis2;
double EnergyDMIAxis3;
double EnergyDMITriangleBulk;
double EnergyDMITriangleInterface;
double EnergyStretching;
double EnergyBending;

/* variables to test */
bool testDMI;
bool testAnisotropy;

/* number to debug */
long number;

/* normalized energy of uniform state for correction */
double EnergyCorrectionNorm;
/* normalized energy for integration and subspecies of this energy */
double EnergyOfSystemNorm;
double EnergyExchangeNorm;
double EnergyAnisotropyNormAxisK1;
double EnergyAnisotropyNormAxisK2;
double EnergyAnisotropyNormAxisK3;
double EnergyDipoleNorm;
double EnergyExternalNorm;
double EnergyDMIAxis1Norm;
double EnergyDMIAxis2Norm;
double EnergyDMIAxis3Norm;
double EnergyDMITriangleBulkNorm;
double EnergyDMITriangleInterfaceNorm;
double EnergyStretchingNorm;
double EnergyBendingNorm;

/* indicator if user entered variable for triangular lattice */
bool indVarTri;
/* error of step RKF45 */
double errorStep;
/* integration step */
double intStep;
/* maximum value of magnetic derivative during integration */
double MaxDmDt;
/* maximum value of velocity during integration */
double MaxDrDt;
/* general magnetic moments on each axis */
double mxTot, myTot, mzTot;
/* number of frames for optimization */
int numberFrames;

/* time of execution for current snapshot */
double timeSnap;

/* minimum and maximum integration step during magnetic simulation */
double integrationStepMin;
double integrationStepMax;
/* current frame number of file (.slsb) */
unsigned long long frameNumber;
/* number in suffix of ".log" files for simulations with the same names */
unsigned long long numberOfFile;
/* rank of process */
int rank;
/* number of processes */
int numberProc;

size_t spinsNum; /**< number of spins in the lattice */

/** Array with components of magnetic moments, coordinates, velocities. For lattice of
 * N sites it contains 6N elements: 3N for (mx, my, mz)_i, 3N for (x, y, z)_i,
 * 3N for (vx, vy, vz)_i, i = 1,...,N. Structure is the following:
 * mx0, my0, mz0, mx1, my1, mz1, mx2, my2, mz2, ..., x0, y0, z0, x1, y1, z1, ...,
 * vx0, vy0, vz0, vx1, vy1, vz1, ...
 */
LatticeSiteSync *lattice;

/** Array of structures with material parameters
 */
LatticeSiteDescr *latticeParam;

UserVars userVars; /**< Instance of UserVars */
FILE * flog; /**< Log of commented rows for input parameters */
FILE * flogEnergy; /**< Log of energy of magnetic system */
FILE * flogMag; /**< Log of magnetic moments for magnetic system */
FILE * flogOther; /**< Log for other parameters of magnetic system */
bool silentMode; /**< Silent mode, do not print anything to terminal except error messages */

/** array for lattice connectivity */
int ** connectivity;
/** actual number of lattice cells */
int connElements;
/** maximal number of lattice cells for the given space */
int maxConnElements;
/** Number of neighbouring sites in the given lattice type */
int connNeighbours;
/** indices of lattice sites */
int * nodeIndices;
/** number of magnetic sites */
int magnSitesNum;

/** general buffer to save temporary rows */
char generalstr[DEFAULT_STR_LEN];

/** Python objects to read variables using script */
PyObject *pModulePython, *pNamePython;
/** Python objects to call function for external field */
/** Function variables */
PyObject *pFieldArgs, *pFieldFunc;
/** Usual variables */
PyObject *pValueTime, *pValueCoor1, *pValueCoor2, *pValueCoor3, *pValueInd1, *pValueInd2, *pValueInd3, *pValueRes;

/** "system" arrays to make simulation for MPI and usual versions (main memory) */
/** 6 arrays for intermediate values of RKF45 */
double *m_k[RKFARRAYS_NUM];
/** arrays for small changes of fourth and fifth order */
double *m_dm5, *m_dm4;

/** "system" arrays to make simulation (middle point method) for MPI and usual versions (main memory) */
/* values of intermediate and final small changes */
double *m_dmHalf, *m_dmFinal;
/* values of magnetic moments in middle point */
double *latticeHalf;

/** array to save amplitude (usual and saved for 'difficult' case) of external magnetic field (Bx, By, Bz!) for every site */
double * Bamp, * BampSaved;

/** "system" arrays to make simulation for CUDA version (GPU) */
/** 6 arrays for intermediate values of RKF45 (pointers in GPU) */
double **d_k;
/** 6 arrays for intermediate values of RKF45 (pointers in main memory) */
double *d_kHost[RKFARRAYS_NUM];
/** arrays for small changes of the fifth order */
double *d_dm5;
/** array "lattice" which was copied to device memory */
double *d_lattice;
/** array "latticeParam" which was copied to device memory */
LatticeSiteDescr *d_latticeParam;
/** array for effective field of system */
double *d_H;
/** array for force of system */
double *d_F;

/** "system" arrays to make simulation (middle point method) for MPI and usual versions (main memory) */
/* values of intermediate and final small changes */
double *d_dmHalf, *d_dmFinal;
/* values of magnetic moments in middle point */
double *d_latticeHalf;

/** array to save amplitude of external magnetic field (Bx, By, Bz!) for every site */
double * d_Bamp;

/** array of normalized energy (all kinds) for every site (GPU) */
double * d_EnergyExchangeArray;
double * d_EnergyAnisotropyArrayAxisK1;
double * d_EnergyAnisotropyArrayAxisK2;
double * d_EnergyAnisotropyArrayAxisK3;
double * d_EnergyDipoleArray;
double * d_EnergyExternalArray;
double * d_EnergyDMIAxis1Array;
double * d_EnergyDMIAxis2Array;
double * d_EnergyDMIAxis3Array;
double * d_EnergyDMITriangleBulkArray;
double * d_EnergyDMITriangleInterfaceArray;
double * d_EnergyStretchingArray;
double * d_EnergyBendingArray;

/** array of normalized energy (all kinds) for every site (main memory) */
double * EnergyExchangeArray;
double * EnergyAnisotropyArrayAxisK1;
double * EnergyAnisotropyArrayAxisK2;
double * EnergyAnisotropyArrayAxisK3;
double * EnergyDipoleArray;
double * EnergyExternalArray;
double * EnergyDMIAxis1Array;
double * EnergyDMIAxis2Array;
double * EnergyDMIAxis3Array;
double * EnergyDMITriangleBulkArray;
double * EnergyDMITriangleInterfaceArray;
double * EnergyStretchingArray;
double * EnergyBendingArray;

/** array of change of magnetic moments with change of time (for every site) */
double * DmDt;
/** array of maximal change of coordinates with change of time (for every site) */
double * DrDt;
/** array for intermediate magnetic moments, coordinates and velocities for RKF45 */
double * d_mCh;

/** variables and arrays to cache data for dipole interaction */

/** size of cache */
size_t dipole_cacheSize;
/** r^3 coefficients for dipole interaction */
double ** dipole_R3coef;
/** rx*r^5 coefficients for dipole interaction */
double ** dipole_RxR5coef;
/** ry*r^5 coefficients for dipole interaction */
double ** dipole_RyR5coef;
/** rz*r^5 coefficients for dipole interaction */
double ** dipole_RzR5coef;

/** r^3 coefficients for dipole interaction in GPU (pointers in main memory) */
double ** d_dipole_R3coefHost;
/** r^3 coefficients for dipole interaction in GPU (pointers in GPU) */
double ** d_dipole_R3coef;
/** rx*r^5 coefficients for dipole interaction in GPU (pointers in main memory) */
double ** d_dipole_RxR5coefHost;
/** rx*r^5 coefficients for dipole interaction in GPU (pointers in GPU) */
double ** d_dipole_RxR5coef;
/** ry*r^5 coefficients for dipole interaction in GPU (pointers in main memory) */
double ** d_dipole_RyR5coefHost;
/** ry*r^5 coefficients for dipole interaction in GPU (pointers in GPU) */
double ** d_dipole_RyR5coef;
/** rz*r^5 coefficients for dipole interaction in GPU (pointers in main memory) */
double ** d_dipole_RzR5coefHost;
/** rz*r^5 coefficients for dipole interaction in GPU (pointers in GPU) */
double ** d_dipole_RzR5coef;

/** additional vectors to work with VTK XML base64 format */
double * addDoubleVectorXML;
int * addIntVectorXML;
int * addIntVectorConnXML;
#ifndef _GEOMFROMPY_H_
#define _GEOMFROMPY_H_

/* function to set coordinates and magnetic activity of sites via python script */
int getCoordsAndGeomPy(void);

/* functions to set magnetic activity of sites via python script */
int getGeomPy(void);

#endif

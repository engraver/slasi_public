/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

/**
 * @file readdata.h
 * @author Oleksandr Pylypovskyi
 * @date 12 Dec 2017
 * @brief Module for reading input data
 * 
 * This module contains all stuff for reading input file with magnetic
 * data, geometry and external fields/currents
 */
 
#ifndef _READ_DATA_H_
#define _READ_DATA_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../alltypes.h"

/**
 * @brief Should be called to read input for starting simulations outside
 *
 * This function should be called outside the module to read all data 
 * provided inside the @p filename. 
 * 
 * @param filename Path to input file
 * @return 0 in the case of absence of errors
 */
int readConfig(const char filename[]);

/**
 * @brief Reads file with parametes of lattice for each site
 * 
 * @param filename Path to input file
 * @param latticeSites Two-dimensional dynamic array for storing data from paramFile
 * @return 0 in the case of absence of errors
 */
int readLatticeParams(const char filename[], double ** latticeSites);

/**
 * @brief Should be called to read scalar variables of binary file for launching of previous simulation
 *
 * @param filename Path to input binary file
 * @return 0 in the case of absence of errors
 */
int readScalarVariablesBin(const char filename[]);

/**
 * @brief Should be called to read all binary file for launching of previous simulation
 *
 * @param filename Path to input binary file
 * @return 0 in the case of absence of errors
 */
int readAllBin(const char filename[]);

#endif

#include <Python.h>
#include <stdio.h>
#include "../alltypes.h"
#include "geomfrompy.h"

int getCoordsAndGeomPy(void) {

	/* declarations of objects to work with Python.h */
	PyObject *pArgs, *pFunc;
	PyObject *pValue;

	double magn, x, y, z;
	int res;

	/* check if module was found */
	if (pModulePython != NULL) {

		if (!PyObject_HasAttrString(pModulePython, "getCoordsAndGeom")) {
			/* "getGeom function is absent, nothing to do */
			return 2; /* should be replaced by named constant in future */
		}

		/* search of function wint name "getCoordsAndGeom" in module */
		pFunc = PyObject_GetAttrString(pModulePython, "getCoordsAndGeom");
		/* check if function was found */
		if (pFunc) {
			if (PyCallable_Check(pFunc)) {
				/* go by each lattice cite */
				int i1, i2, i3;
				for (i1 = 1; i1 < userVars.N1 - 1; i1++) {
					for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
						for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
							/* number of spin in lattice */
							int indexLattice = IDX(i1, i2, i3);
							/* creating parameters to call function (coordinates and indices) */
							pArgs = PyTuple_New(3);

							/* the first index */
							pValue = PyLong_FromLong(i1);
							/* check value */
							if (!pValue) {
								/* output error */
								Py_DECREF(pArgs);
								fprintf(stderr, "Cannot convert arguments for function getGeom!\n");
								return 1;
							}
							/* put new parameter */
							PyTuple_SetItem(pArgs, 0, pValue);

							/* the second index */
							pValue = PyLong_FromLong(i2);
							/* check value */
							if (!pValue) {
								/* output error */
								Py_DECREF(pArgs);
								fprintf(stderr, "Cannot convert arguments for function getGeom!\n");
								return 1;
							}
							/* put new parameter */
							PyTuple_SetItem(pArgs, 1, pValue);

							/* the third index */
							pValue = PyLong_FromLong(i3);
							/* check value */
							if (!pValue) {
								/* output error */
								Py_DECREF(pArgs);
								fprintf(stderr, "Cannot convert arguments for function getGeom!\n");
								return 1;
							}
							/* put new parameter */
							PyTuple_SetItem(pArgs, 2, pValue);

							/* call function */
							pValue = PyObject_CallObject(pFunc, pArgs);
							/* check results of calling */
							if (pValue != NULL) {

								/* get results of calling */
								res = PyArg_ParseTuple(pValue, "dddd", &magn, &x, &y, &z);
								/* check if it was able to get */
								if (res) {
									/* save results */
									if (magn > 0.5) {
										latticeParam[indexLattice].magnetic = true;
									}
									else {
										latticeParam[indexLattice].magnetic = false;
									}
									*(latticeParam[indexLattice].x) = x / userVars.a;
									*(latticeParam[indexLattice].y) = y / userVars.a;
									*(latticeParam[indexLattice].z) = z / userVars.a;
								} else {
									/* output error */
									fprintf(stderr, "Call of python function getCoordsAndGeom failed!\n");
									return 1;
								}
								Py_DECREF(pValue);

							} else {
								/* output error */
								Py_DECREF(pFunc);
								//PyErr_Print();
								fprintf(stderr, "Call of python function getCoordsAndGeom failed\n");
								return 1;
							}
						}
					}
				}
			}
		}
		Py_XDECREF(pFunc);

	} else {
		/* "getGeom function is absent */
		return 1; /* should be replaced by named constant in future */
	}

	return 0;
}

int getGeomPy(void) {
	/* declarations of objects to work with Python.h */
	PyObject *pArgs, *pFunc;
	PyObject *pValue;

	double magn;

	/* check if module was found */
	if (pModulePython != NULL) {


		if (!PyObject_HasAttrString(pModulePython, "getGeom")) {
			/* "getGeom function is absent, nothing to do */
			return 2; /* should be replaced by named constant in future */
		}

		/* search of function wint name "getGeom" in module */
		pFunc = PyObject_GetAttrString(pModulePython, "getGeom");
		/* check if function was found */
		if (pFunc) {
			if (PyCallable_Check(pFunc)) {

				/* go by each lattice cite */
				int i1, i2, i3;
				for (i1 = 1; i1 < userVars.N1 - 1; i1++) {
					for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
						for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
							/* number of spin in lattice */
							int indexLattice = IDX(i1, i2, i3);
							/* creating parameters to call function (coordinates and indices) */
							pArgs = PyTuple_New(6);

							/* the first coordinate */
							pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].x) * userVars.a);
							/* check value */
							if (!pValue) {
								/* output error */
								Py_DECREF(pArgs);
								fprintf(stderr, "Cannot convert arguments for function getGeom!\n");
								return 1;
							}
							/* put new parameter */
							PyTuple_SetItem(pArgs, 0, pValue);

							/* the second coordinate */
							pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].y) * userVars.a);
							/* check value */
							if (!pValue) {
								/* output error */
								Py_DECREF(pArgs);
								fprintf(stderr, "Cannot convert arguments for function getGeom!\n");
								return 1;
							}
							/* put new parameter */
							PyTuple_SetItem(pArgs, 1, pValue);

							/* the third coordinate */
							pValue = PyFloat_FromDouble(*(latticeParam[indexLattice].z) * userVars.a);
							/* check value */
							if (!pValue) {
								/* output error */
								Py_DECREF(pArgs);
								fprintf(stderr, "Cannot convert arguments for function getGeom!\n");
								return 1;
							}
							/* put new parameter */
							PyTuple_SetItem(pArgs, 2, pValue);

							/* the first index */
							pValue = PyLong_FromLong(i1);
							/* check value */
							if (!pValue) {
								/* output error */
								Py_DECREF(pArgs);
								fprintf(stderr, "Cannot convert arguments for function getGeom!\n");
								return 1;
							}
							/* put new parameter */
							PyTuple_SetItem(pArgs, 3, pValue);

							/* the second index */
							pValue = PyLong_FromLong(i2);
							/* check value */
							if (!pValue) {
								/* output error */
								Py_DECREF(pArgs);
								fprintf(stderr, "Cannot convert arguments for function getGeom!\n");
								return 1;
							}
							/* put new parameter */
							PyTuple_SetItem(pArgs, 4, pValue);

							/* the third index */
							pValue = PyLong_FromLong(i3);
							/* check value */
							if (!pValue) {
								/* output error */
								Py_DECREF(pArgs);
								fprintf(stderr, "Cannot convert arguments for function getGeom!\n");
								return 1;
							}
							/* put new parameter */
							PyTuple_SetItem(pArgs, 5, pValue);

							/* call function */
							pValue = PyObject_CallObject(pFunc, pArgs);
							/* check results of calling */
							if (pValue != NULL) {

								/* get results of calling */
								magn = PyFloat_AsDouble(pValue);
								if (magn > 0.5) {
									latticeParam[indexLattice].magnetic = true;
								}
								else {
									latticeParam[indexLattice].magnetic = false;
									/**(latticeParam[indexLattice].x) = 0;
                                    *(latticeParam[indexLattice].y) = 0;
                                    *(latticeParam[indexLattice].z) = 0;*/
									*(latticeParam[indexLattice].mx) = 0;
									*(latticeParam[indexLattice].my) = 0;
									*(latticeParam[indexLattice].mz) = 0;
								}
								Py_DECREF(pValue);

							} else {
								/* output error */
								Py_DECREF(pFunc);
								//PyErr_Print();
								fprintf(stderr, "Call of python function getGeom failed\n");
								return 1;
							}
						}
					}
				}
			}

		} else {
			/* "getGeom function is absent */
			return 1; /* should be replaced by named constant in future */
		}
		Py_XDECREF(pFunc);
	}

	return 0;

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

/**
 * @file readdata_int.h
 * @author Oleksandr Pylypovskyi
 * @date 12 Dec 2017
 * @brief Module for reading input data (internal functions)
 *
 * Should not be included outside this module, only for testing purpuses
 */

#ifndef _READ_DATA_INT_H_
#define _READ_DATA_INT_H_

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include "../alltypes.h"

/**
 * @brief Parses strings of 'key = value' format
 *
 * Function gets a string @p str and allocates memory for @p key and
 * @p value if it can be parsed as 'key=value'. All spaces are ignored.
 * Symbol # means start of a comment, it ignored. If @str contains only
 * space symbols or/and comment, 0 is returned with @p key and @p value
 * of NULL value. If @p str cannot be parsed, nonzero value is returned
 * @p key and @p value equal NULL and message to log is printed.
 *
 * @param str String for parsing, not null
 * @param key Contains pointer to string with key if no errors
 * @param value Contains pointer to string with value if no errors
 * @param lineNum number of current line
 * @return 0 in the case of absence of errors; otherwise prints message
 * to log and returns nonzero value
 */
int parseKeyValue(const char * str, char ** key, char ** value, int lineNum);

/**
 * @brief Setting concrete value in userVars structure according to key
 *
 * @param key String with key
 * @param value String with value of the given key
 * @param lineNum Number of current line
 * @return 0 in the case of absense of errors; otherwise prints message
 * to log and returns nonzero value
 */
int assingValueToKey(const char * key, char * value, int lineNum);

/*
 * @brief Converts string to integer
 *
 * @param cvalue String value
 * @param str pointer to parsed string for finding of file's way, filled if parsing is possible
 * @param lineNum number of current line
 * @param valsetup indicator whether the variable has been updated (1 - if updated, 0 - if not)
 * @return 0 in the case of absence of errors; otherwise prints message
 * to log and returns nonzero value
 */
//int parseStringValue (const char * cvalue, char * str, bool * valsetup, int lineNum);

/**
 * @brief Converts string to integer
 *
 * @param cvalue String value
 * @param ivalue pointer to parsed int, filled if parsing is possible
 * @param lineNum number of current line
 * @param valsetup indicator whether the variable has been updated (1 - if updated, 0 - if not)
 * @return 0 in the case of absence of errors; otherwise prints message
 * to log and returns nonzero value
 */
int parseIntValue(const char * cvalue, int * ivalue, bool * valsetup,  int lineNum);

/**
 * @brief Converts string to integer
 *
 * @param cvalue String value
 * @param dvalue pointer to parsed double, filled if parsing is possible
 * @param lineNum number of current line
 * @param valsetup indicator whether the variable has been updated (1 - if updated, 0 - if not)
 * @return 0 in the case of absence of errors; otherwise prints message
 * to log and returns nonzero value
 */
int parseDoubleValue(const char * cvalue, double * dvalue, bool * valsetup, int lineNum);

/**
 * @brief Removes leading and final spaces in string
 *
 * @param str c-string for processing
 */
void trim(char * str);

/**
* @brief Setup indexes in UserVars using headers in paramFile
*
* @param key cvalue String value for updating
* @param colNum number of current column
* @param lineNum number of current line
* @return 0 in the case of absence of errors; otherwise prints message
* to log and returns nonzero value
*/
int setupIndexHeaders(const char *key, int colNum, int lineNum);

/**
 * @brief Get list of headers
 *
 * @param str line with headers
 * @param headers array of c-strings. We assume file can contain till 100 columns
 * @param headNum pointer to number of headers in file (updated after function work)
 * @return 0 in the case of absence of errors; otherwise prints message
 * to log and returns nonzero value
 */
int getHeaders(char * str, char ** headers, size_t * headNum, int lineNum);


/**
 * @brief Get list of headers
 *
 * @param str line with numbers
 * @param headers array of c-strings. We assume file can contain till 100 columns
 * @param headNum number of columns in the file
 * @param rowsNum number of row processing in this call
 * @return 0 in the case of absence of errors; otherwise prints message
 * to log and returns nonzero value
 */
int parseColumns(char * str, double ** numbers, size_t headNum, size_t currentRow, int lineNum);

#endif

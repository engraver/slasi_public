/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                    */
/*                This file is part of the SLaSi project              */
/*                        Spin-Lattice Simulator                      */
/*                                                                    */
/*    Copyright (C) 2009-now                                          */
/*                  RiTM Team                                         */
/*                    http://ritm.knu.ua/                             */
/*                                                                    */
/*    License: See LICENSE.txt in the root directory                  */
/*                                                                    */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#ifndef  __USE_ISOC99
#define  __USE_ISOC99
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <ctype.h>
#include "readdata.h"
#include "readdata_int.h"
#include "../utils/parallel_utils.h"

int readConfig(const char filename[]) {

	FILE *fid;
	char *str = NULL;
	size_t lineLength = 0;
	char *key = NULL, *value = NULL;
	ssize_t ind;
	int result = 0;
	int lineNum = 1;
	/* open file using filename */
	fid = fopen(filename, "r");

	if (fid != NULL) {
		/* read one row */
		ind = getline(&str, &lineLength, fid);
		/* if not the end of the file to continue to work */
		while ((ind != -1) && (result == 0)) {
			/* find key and value in the row */
			if (parseKeyValue(str, &key, &value, lineNum)) {
				messageToStream("READDATA.C: parseKeyValue() failed.", stderr);
				result = 1;
			}
			if (!result && key && value) {
				/* clear spaces in key */
				trim(key);
				/* setup value in UserVars */
				if (assingValueToKey(key, value, lineNum)) {
					messageToStream("READDATA.C: setupValue() failed.", stderr);
					result = 1;
				}
				free(key);
				free(value);
			}
			/* read new row */
			if (!result) {
				ind = getline(&str, &lineLength, fid);
				lineNum++;
			}
		}
	} else {
		/* if the file is not found, we will write error */
		result = 1;
		fprintf(stderr, "(file %s | line %d) Cannot find input configuration file: %s\n", __FILE__, __LINE__, filename);
	}

	fclose(fid);
	free(str);
	str = NULL;
	return result;

}

void trim(char *str) {

	size_t str_len = strlen(str);

	/* leading spaces */
	size_t k = 0;
	while (isspace(str[k]) && (k < str_len)) {
		k++;
	}

	memmove(str, str + k, str_len - k);
	k = str_len - k;
	str[k] = 0;

	/* final spaces */
	k--;
	while ((k > 0) && isspace(str[k])) {
		k--;
	}
	if (k == 0 && isspace(str[0])) {
		str[0] = 0;
	} else {
		str[k + 1] = 0;
	}

}

int parseKeyValue(const char *str, char **key, char **value, int lineNum) {

	*key = NULL;
	*value = NULL;
	size_t str_len = strlen(str);

	char *strt = malloc(sizeof(char) * (str_len + 1));
	strcpy(strt, str);

	/* find comment */
	char *comment = strchr(strt, '#');
	if (comment) {
		/* comment is found and we terminate string here */
		str_len = (size_t)(comment - strt);
		strt[str_len] = 0;
	}

	/* find '=' sign */
	char *assignment = strchr(strt, '=');
	if (assignment) {
		/* possible 'key=value' exists and we parse it */
		char *keyt = strtok(strt, "=");
		char *valuet = strtok(NULL, "=");
		*key = malloc(sizeof(char) * (strlen(keyt) + 1));
		*value = malloc(sizeof(char) * (strlen(valuet) + 1));
		strcpy(*key, keyt);
		strcpy(*value, valuet);
		free(strt);
		return 0;
	}

	/* 'key=value' pair is not found, check for empty string */
	for (size_t i = 0; i < str_len; i++) {
		if (!isspace(strt[i])) {
			fprintf(stderr, "(file %s | line %d) Mistake in line %i of input file! Does it satisfy 'key = value' format?\n", __FILE__, __LINE__, lineNum);
			fprintf(flog, "(file %s | line %d) Mistake in line %i of input file! Does it satisfy 'key = value' format?\n", __FILE__, __LINE__, lineNum);
			free(strt);
			return 1;
		}
	}

	/* empty line */
	free(strt);

	return 0;

}

int assingValueToKey(const char *key, char *value, int lineNum) {

	/* verify the key with the name of the UserVars structure field */

	/* field N1 */
	if (strcmp(key, "N1") == 0) {
		return parseIntValue(value, &(userVars.N1), &(userVars.bN1), lineNum);
	} /* field N2 */
	else if (strcmp(key, "N2") == 0) {
		return parseIntValue(value, &(userVars.N2), &(userVars.bN2), lineNum);
	} /* field N3 */
	else if (strcmp(key, "N3") == 0) {
		return parseIntValue(value, &(userVars.N3), &(userVars.bN3), lineNum);
	} /* field scriptFile */
	else if (strcmp(key, "scriptFile") == 0) {
		/* saving line in userVars */
		memset(userVars.scriptFile, 0, sizeof(userVars.scriptFile));
		trim(value);
		strcpy(userVars.scriptFile, value);
		return 0;
	} /* field magnFile */
	else if (strcmp(key, "magnFile") == 0) {
		/* saving line in userVars */
		memset(userVars.magnFile, 0, sizeof(userVars.magnFile));
		trim(value);
		strcpy(userVars.magnFile, value);
		return 0;
	} /* field a */
	else if (strcmp(key, "a") == 0) {
		return parseDoubleValue(value, &(userVars.a), &(userVars.ba), lineNum);
	} /* field mass */
	else if (strcmp(key, "mass") == 0) {
		return parseDoubleValue(value, &(userVars.mass), &(userVars.bmass), lineNum);
	} /* field mechDamping */
	else if (strcmp(key, "mechDamping") == 0) {
		return parseDoubleValue(value, &(userVars.mechDamping), &(userVars.bmechDamping), lineNum);
	} /* field fixedValue */
	else if (strcmp(key, "lagrangeDistance") == 0) {
		return parseDoubleValue(value, &(userVars.lagrangeDistance), &(userVars.blagrangeDistance), lineNum);
	}  /* field J */
	else if (strcmp(key, "J") == 0) {
		return parseDoubleValue(value, &(userVars.J), &(userVars.bJ), lineNum);
	} /* field g */
	else if (strcmp(key, "g") == 0) {
		return parseDoubleValue(value, &(userVars.g), &(userVars.bg), lineNum);
	} /* field gilbertDamping */
	else if (strcmp(key, "gilbertDamping") == 0) {
		int tester = 0;
		tester = parseDoubleValue(value, &(userVars.gilbertDamping), &(userVars.bgilbertDamping), lineNum);
		/* if this value is too small for further calculations */
		if (fabs(userVars.gilbertDamping) < 0.0001) {
			fprintf(stderr, "(file %s | line %d) gilbertDamping is recommended to be not less than 0.0001 in line %i!\n",
			        __FILE__, __LINE__, lineNum);
			fprintf(flog, "(file %s | line %d) gilbertDamping is recommended to be not less than 0.0001 in line %i!\n",
			        __FILE__, __LINE__, lineNum);
		}
		return tester;
	} /* field muB */
	else if (strcmp(key, "muB") == 0) {
		return parseDoubleValue(value, &(userVars.muB), &(userVars.bmuB), lineNum);
	} /* field S */
	else if (strcmp(key, "S") == 0) {
		return parseDoubleValue(value, &(userVars.S), &(userVars.bS), lineNum);
	} /* mu0 */
	else if (strcmp(key, "mu0") == 0) {
		return parseDoubleValue(value, &(userVars.mu0), &(userVars.bmu0), lineNum);
	} /* mu0 */
	else if (strcmp(key, "hbar") == 0) {
		return parseDoubleValue(value, &(userVars.hbar), &(userVars.bhbar), lineNum);
	} /* field aniK1 */
	else if (strcmp(key, "aniK1") == 0) {
		return parseDoubleValue(value, &(userVars.aniK1), &(userVars.baniK1), lineNum);
	} /* field aniK2 */
	else if (strcmp(key, "aniK2") == 0) {
		return parseDoubleValue(value, &(userVars.aniK2), &(userVars.baniK2), lineNum);
	} /* field aniK3 */
	else if (strcmp(key, "aniK3") == 0) {
		return parseDoubleValue(value, &(userVars.aniK3), &(userVars.baniK3), lineNum);
	} /* field aniInterK1 */
	else if (strcmp(key, "aniInterK1") == 0) {
		return parseDoubleValue(value, &(userVars.aniInterK1), &(userVars.baniInterK1), lineNum);
	} /* field aniInterK2 */
	else if (strcmp(key, "aniInterK2") == 0) {
		return parseDoubleValue(value, &(userVars.aniInterK2), &(userVars.baniInterK2), lineNum);
	} /* field aniInterK3 */
	else if (strcmp(key, "aniInterK3") == 0) {
		return parseDoubleValue(value, &(userVars.aniInterK3), &(userVars.baniInterK3), lineNum);
	} /* field mx0 */
	else if (strcmp(key, "mx0") == 0) {
		return parseDoubleValue(value, &(userVars.mx0), &(userVars.bmx0), lineNum);
	} /* field my0 */
	else if (strcmp(key, "my0") == 0) {
		return parseDoubleValue(value, &(userVars.my0), &(userVars.bmy0), lineNum);
	} /* field mz0 */
	else if (strcmp(key, "mz0") == 0) {
		return parseDoubleValue(value, &(userVars.mz0), &(userVars.bmz0), lineNum);
	} /* field integrationStep */
	else if (strcmp(key, "integrationStep") == 0) {
		return parseDoubleValue(value, &(userVars.integrationStep), &(userVars.bintegrationStep), lineNum);
	} /* field precision */
	else if (strcmp(key, "precision") == 0) {
		return parseDoubleValue(value, &(userVars.precision), &(userVars.bprecision), lineNum);
	} /* field stopDmDt */
	else if (strcmp(key, "stopDmDt") == 0) {
		return parseDoubleValue(value, &(userVars.stopDmDt), &(userVars.bstopDmDt), lineNum);
	} /* field t0 */
	else if (strcmp(key, "t0") == 0) {
		return parseDoubleValue(value, &(userVars.t0), &(userVars.bt0), lineNum);
	} /* snapshotPeriodMultiplier */
	else if (strcmp(key, "snapshotPeriodMultiplier") == 0) {
		return parseIntValue(value, &(userVars.snapshotPeriodMultiplier), &(userVars.bsnapshotPeriodMultiplier), lineNum);
	} /* field tfin */
	else if (strcmp(key, "tfin") == 0) {
		return parseDoubleValue(value, &(userVars.tfin), &(userVars.btfin), lineNum);
	} /* field frame */
	else if (strcmp(key, "frame") == 0) {
		return parseDoubleValue(value, &(userVars.frame), &(userVars.bframe), lineNum);
	} /* field critAngle */
	else if (strcmp(key, "critAngle") == 0) {
		return parseDoubleValue(value, &(userVars.critAngle), &(userVars.bcritAngle), lineNum);
	}  /* field Bxamp */
	else if (strcmp(key, "Bxamp") == 0) {
		return parseDoubleValue(value, &(userVars.Bxamp), &(userVars.bBxamp), lineNum);
	} /* field Byamp */
	else if (strcmp(key, "Byamp") == 0) {
		return parseDoubleValue(value, &(userVars.Byamp), &(userVars.bByamp), lineNum);
	} /* field Bzamp */
	else if (strcmp(key, "Bzamp") == 0) {
		return parseDoubleValue(value, &(userVars.Bzamp), &(userVars.bBzamp), lineNum);
	} /* field Bfreq */
	else if (strcmp(key, "Bfreq") == 0) {
		return parseDoubleValue(value, &(userVars.Bfreq), &(userVars.bBfreq), lineNum);
	} /* field Bphase */
	else if (strcmp(key, "Bphase") == 0) {
		return parseDoubleValue(value, &(userVars.Bphase), &(userVars.bBphase), lineNum);
	} /* field Bphase */
	else if (strcmp(key, "Bsinc_twidth") == 0) {
		return parseDoubleValue(value, &(userVars.fieldSincTWidth), &(userVars.bfieldSincTWidth), lineNum);
	} /* field Bphase */
	else if (strcmp(key, "Bsinc_tshift") == 0) {
		return parseDoubleValue(value, &(userVars.fieldSincTShift), &(userVars.bfieldSincTShift), lineNum);
	} /* field vx */
	else if (strcmp(key, "vx") == 0) {
		return parseDoubleValue(value, &(userVars.vx), &(userVars.bvx), lineNum);
	} /* field vy */
	else if (strcmp(key, "vy") == 0) {
		return parseDoubleValue(value, &(userVars.vy), &(userVars.bvy), lineNum);
	} /* field vz */
	else if (strcmp(key, "vz") == 0) {
		return parseDoubleValue(value, &(userVars.vz), &(userVars.bvz), lineNum);
	} /* field Fx */
	else if (strcmp(key, "Fx") == 0) {
		return parseDoubleValue(value, &(userVars.Fx), &(userVars.bFx), lineNum);
	} /* field Fy */
	else if (strcmp(key, "Fy") == 0) {
		return parseDoubleValue(value, &(userVars.Fy), &(userVars.bFy), lineNum);
	} /* field Fz */
	else if (strcmp(key, "Fz") == 0) {
		return parseDoubleValue(value, &(userVars.Fz), &(userVars.bFz), lineNum);
	} /* field Ffreq */
	else if (strcmp(key, "Ffreq") == 0) {
		return parseDoubleValue(value, &(userVars.Ffreq), &(userVars.bFfreq), lineNum);
	} /* field Fphase */
	else if (strcmp(key, "Fphase") == 0) {
		return parseDoubleValue(value, &(userVars.Fphase), &(userVars.bFphase), lineNum);
	} /* field nrmX */
	else if (strcmp(key, "nrmX") == 0) {
		return parseDoubleValue(value, &(userVars.nrmX), &(userVars.bnrmX), lineNum);
	} /* field nrmY */
	else if (strcmp(key, "nrmY") == 0) {
		return parseDoubleValue(value, &(userVars.nrmY), &(userVars.bnrmY), lineNum);
	} /* field nrmZ */
	else if (strcmp(key, "nrmZ") == 0) {
		return parseDoubleValue(value, &(userVars.nrmZ), &(userVars.bnrmZ), lineNum);
	} /* field stopValue */
	else if (strcmp(key, "stopValue") == 0) {
		return parseDoubleValue(value, &(userVars.stopValue), &(userVars.bstopValue), lineNum);
	} /* field updatePeriod */
	else if (strcmp(key, "updatePeriod") == 0) {
		return parseIntValue(value, &(userVars.updatePeriod), &(userVars.bupdatePeriod), lineNum);
	} /* field refreshRate */
	else if (strcmp(key, "refreshRate") == 0) {
		return parseIntValue(value, &(userVars.refreshRate), &(userVars.brefreshRate), lineNum);
	} /* field backupPeriod */
	else if (strcmp(key, "backupPeriod") == 0) {
		return parseIntValue(value, &(userVars.backupPeriod), &(userVars.bbackupPeriod), lineNum);
    } /* field lengthResidue */
	else if (strcmp(key, "lengthResidue") == 0) {
		return parseDoubleValue(value, &(userVars.lengthResidue), &(userVars.blengthResidue), lineNum);
	} /* field D11 */
	else if (strcmp(key, "D11") == 0) {
		return parseDoubleValue(value, &(userVars.D11), &(userVars.bD11), lineNum);
	} /* field D12 */
	else if (strcmp(key, "D12") == 0) {
		return parseDoubleValue(value, &(userVars.D12), &(userVars.bD12), lineNum);
	} /* field D13 */
	else if (strcmp(key, "D13") == 0) {
		return parseDoubleValue(value, &(userVars.D13), &(userVars.bD13), lineNum);
	} /* field D21 */
	else if (strcmp(key, "D21") == 0) {
		return parseDoubleValue(value, &(userVars.D21), &(userVars.bD21), lineNum);
	} /* field D22 */
	else if (strcmp(key, "D22") == 0) {
		return parseDoubleValue(value, &(userVars.D22), &(userVars.bD22), lineNum);
	} /* field D23 */
	else if (strcmp(key, "D23") == 0) {
		return parseDoubleValue(value, &(userVars.D23), &(userVars.bD23), lineNum);
	} /* field D31 */
	else if (strcmp(key, "D31") == 0) {
		return parseDoubleValue(value, &(userVars.D31), &(userVars.bD31), lineNum);
	} /* field D32 */
	else if (strcmp(key, "D32") == 0) {
		return parseDoubleValue(value, &(userVars.D32), &(userVars.bD32), lineNum);
	} /* field D33 */
	else if (strcmp(key, "D33") == 0) {
		return parseDoubleValue(value, &(userVars.D33), &(userVars.bD33), lineNum);
	}  /* field e11 */
	else if (strcmp(key, "e11") == 0) {
		return parseDoubleValue(value, &(userVars.e11), &(userVars.be11), lineNum);
	} /* field e12 */
	else if (strcmp(key, "e12") == 0) {
		return parseDoubleValue(value, &(userVars.e12), &(userVars.be12), lineNum);
	} /* field e13 */
	else if (strcmp(key, "e13") == 0) {
		return parseDoubleValue(value, &(userVars.e13), &(userVars.be13), lineNum);
	} /* field e21 */
	else if (strcmp(key, "e21") == 0) {
		return parseDoubleValue(value, &(userVars.e21), &(userVars.be21), lineNum);
	} /* field e22 */
	else if (strcmp(key, "e22") == 0) {
		return parseDoubleValue(value, &(userVars.e22), &(userVars.be22), lineNum);
	} /* field e23 */
	else if (strcmp(key, "e23") == 0) {
		return parseDoubleValue(value, &(userVars.e23), &(userVars.be23), lineNum);
	} /* field e31 */
	else if (strcmp(key, "e31") == 0) {
		return parseDoubleValue(value, &(userVars.e31), &(userVars.be31), lineNum);
	} /* field e32 */
	else if (strcmp(key, "e32") == 0) {
		return parseDoubleValue(value, &(userVars.e32), &(userVars.be32), lineNum);
	} /* field e33 */
	else if (strcmp(key, "e33") == 0) {
		return parseDoubleValue(value, &(userVars.e33), &(userVars.be33), lineNum);
	} /* field beta */
	else if (strcmp(key, "beta") == 0) {
		return parseDoubleValue(value, &(userVars.beta), &(userVars.bbeta), lineNum);
	} /* field lambda */
	else if (strcmp(key, "lambda") == 0) {
		return parseDoubleValue(value, &(userVars.lambda), &(userVars.blambda), lineNum);
	} /* field dmiBulk */
	else if (strcmp(key, "dmiBulk") == 0) {
		return parseDoubleValue(value, &(userVars.dmiBulk), &(userVars.bdmiBulk), lineNum);
	} /* field dmiBtri */
	else if (strcmp(key, "dmiBtri") == 0) {
		/* variable for triangular lattice was entered */
		indVarTri = true;
		return parseDoubleValue(value, &(userVars.dmiBtri), &(userVars.bdmiBtri), lineNum);
	} /* field dmiNtri */
	else if (strcmp(key, "dmiNtri") == 0) {
		/* variable for triangular lattice was entered */
		indVarTri = true;
		return parseDoubleValue(value, &(userVars.dmiNtri), &(userVars.bdmiNtri), lineNum);
	} /* field betaTri */
	else if (strcmp(key, "betaTri") == 0) {
		/* variable for triangular lattice was entered */
		indVarTri = true;
		return parseDoubleValue(value, &(userVars.betaTri), &(userVars.bbetaTri), lineNum);
	} /* field lambdaTri */
	else if (strcmp(key, "lambdaTri") == 0) {
		/* variable for triangular lattice was entered */
		indVarTri = true;
		return parseDoubleValue(value, &(userVars.lambdaTri), &(userVars.blambdaTri), lineNum);
	} /* field typeFlexParam */
	else if (strcmp(key, "typeFlexParam") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "const") == 0) {
			userVars.typeFlexParam = FLEXPARAM_CONST;
		} else if (strcmp(str, "file") == 0) {
			userVars.typeFlexParam = FLEXPARAM_FILE;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n",  __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n",  __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field typeMechParam */
	else if (strcmp(key, "typeMechParam") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "const") == 0) {
			userVars.typeMechParam = MECHPARAM_CONST;
		} else if (strcmp(str, "file") == 0) {
			userVars.typeMechParam = MECHPARAM_FILE;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field saveSLS */
	else if (strcmp(key, "saveSLS") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.saveSLS = SLSSAVE_ON;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.saveSLS = SLSSAVE_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field typeMagn */
	else if (strcmp(key, "typeMagn") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "all") == 0) {
			userVars.typeMagn = MAGN_ALL;
		} else if (strcmp(str, "file") == 0) {
			userVars.typeMagn = MAGN_FILE;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field fixedStep */
	else if (strcmp(key, "fixedStep") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.fixedStep = FIXEDSTEP_ON;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.fixedStep = FIXEDSTEP_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field integrationMethod */
	else if (strcmp(key, "integrationMethod") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "RKF") == 0) {
			userVars.integrationMethod = INTEGRATION_METHOD_RKF;
		} else if (strcmp(str, "MP") == 0) {
			userVars.integrationMethod = INTEGRATION_METHOD_MIDDLE_POINT;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field magnInit */
	else if (strcmp(key, "magnInit") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "random") == 0) {
			userVars.magnInit = MI_RANDOM;
		} else if (strcmp(str, "uniform") == 0) {
			userVars.magnInit = MI_UNIFORM;
		} else if (strcmp(str, "file") == 0) {
			userVars.magnInit = MI_FILE;
		} else if (strcmp(str, "script") == 0) {
			userVars.magnInit = MI_SCRIPT;
		} else if (strcmp(str, "slsb") == 0) {
			userVars.magnInit = MI_SLSB;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field exchInit */
	else if (strcmp(key, "exchInit") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "uniform") == 0) {
			userVars.exchInit = EI_UNIFORM;
		} else if (strcmp(str, "script") == 0) {
			userVars.exchInit = EI_SCRIPT;
		} else if (strcmp(str, "file") == 0) {
			userVars.exchInit = EI_FILE;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field dipOnOff */
	else if (strcmp(key, "dipOnOff") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.dipOnOff = DIP_ON;
		} else if (strcmp(str, "ON_no_cache") == 0) {
			userVars.dipOnOff = DIP_ON_NOCACHE;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.dipOnOff = DIP_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field exchOnOff */
	else if (strcmp(key, "exchOnOff") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.exchOnOff = EXCH_ON;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.exchOnOff = EXCH_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field dmiOnOff */
	else if (strcmp(key, "dmiOnOff") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.dmiOnOff = DMI_ON;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.dmiOnOff = DMI_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field latticeType */
	else if (strcmp(key, "latticeType") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str,0,sizeof(str));
		strcpy(str,value);
		trim (str);
		/* select the correct option in enumaration */
		if ( strcmp(str,"cubic") == 0 ) {
			userVars.latticeType = LATTICE_CUBIC;
		} else if ( strcmp(str,"triangular") == 0 ) {
			userVars.latticeType = LATTICE_TRIANGULAR;
		} else {
			fprintf(flog,"(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__,  str, lineNum);
			fprintf(stderr,"(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field flexOnOff */
	else if (strcmp(key, "flexOnOff") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.flexOnOff = FLEX_ON;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.flexOnOff = FLEX_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field writingEnergy */
	else if (strcmp(key, "writingEnergy") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.writingEnergy = WRITINGENERGY_ON;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.writingEnergy = WRITINGENERGY_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field writingBasis */
	else if (strcmp(key, "writingBasis") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.writingBasis = WRITINGBASIS_ON;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.writingBasis = WRITINGBASIS_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n",  __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field writingExtField */
	else if (strcmp(key, "writingExtField") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.writingExtField = WRITINGEXTFIELD_ON;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.writingExtField = WRITINGEXTFIELD_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* periodic boundary conditions for 1 axis */
	else if (strcmp(key, "periodicBC1") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.periodicBC1 = PBC_ON;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.periodicBC1 = PBC_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* periodic boundary conditions for 2 axis */
	else if (strcmp(key, "periodicBC2") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.periodicBC2 = PBC_ON;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.periodicBC2 = PBC_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* periodic boundary conditions for 3 axis */
	else if (strcmp(key, "periodicBC3") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.periodicBC3 = PBC_ON;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.periodicBC3 = PBC_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field normalOnOff */
	else if (strcmp(key, "normalOnOff") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.normalOnOff = NORMAL_ON;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.normalOnOff = NORMAL_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field extField */
	else if (strcmp(key, "extField") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "const") == 0) {
			userVars.extField = EXTFIELD_CONST;
		} else if (strcmp(str, "harmTime") == 0) {
			userVars.extField = EXTFIELD_HARMTIME;
		} else if (strcmp(str, "script") == 0) {
			userVars.extField = EXTFIELD_SCRIPT;
		} else if (strcmp(str, "file") == 0) {
			userVars.extField = EXTFIELD_FILE;
		} else if (strcmp(str, "sinc") == 0) {
			userVars.extField = EXTFIELD_SINC;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field extForce */
	else if (strcmp(key, "extForce") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "const") == 0) {
			userVars.extForce = EXTFORCE_CONST;
		} else if (strcmp(str, "harmTime") == 0) {
			userVars.extForce = EXTFORCE_HARMTIME;
		} else if (strcmp(str, "script") == 0) {
			userVars.extForce = EXTFORCE_SCRIPT;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n",  __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n",  __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field exchType */
	else if (strcmp(key, "exchType") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "uniform") == 0) {
			userVars.exchType = EXCH_UNIFORM;
		} else if (strcmp(str, "inhomogeneous") == 0) {
			userVars.exchType = EXCH_INHOMOGENEOUS;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field DMItype */
	else if (strcmp(key, "DMItype") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "const") == 0) {
			userVars.DMItype = DMI_CONST;
		} else if (strcmp(str, "script") == 0) {
			userVars.DMItype = DMI_SCRIPT;
		} else if (strcmp(str, "file") == 0) {
			userVars.DMItype = DMI_FILE;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field anisotropyType */
	else if (strcmp(key, "anisotropyType") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "const") == 0) {
			userVars.anisotropyType = ANIS_CONST;
		} else if (strcmp(str, "script") == 0) {
			userVars.anisotropyType = ANIS_SCRIPT;
		} else if (strcmp(str, "file") == 0) {
			userVars.anisotropyType = ANIS_FILE;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field typeLaunch */
	else if (strcmp(key, "launch") == 0) {
		int tester = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "minimization") == 0) {
			userVars.launch = LAUNCH_MINIMIZATION;
		} else if (strcmp(str, "simulation") == 0) {
			userVars.launch = LAUNCH_SIMULATION;
		} else if (strcmp(str, "hysteresis") == 0) {
			userVars.launch = LAUNCH_HYSTERESIS;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			tester = 1;
		}
		return tester;
	} /* field optimizationMethod */
	else if (strcmp(key, "minMethod") == 0) {
		int result = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "NewtonRestart") == 0) {
			userVars.minMethod = TNEWTON_RESTART;
		} else if (strcmp(str, "NewtonPreRestart") == 0) {
			userVars.minMethod = TNEWTON_PRECOND_RESTART;
		} else if (strcmp(str, "Newton") == 0) {
			userVars.minMethod = TNEWTON;
		} else if (strcmp(str, "LBFGS") == 0) {
			userVars.minMethod = LBFGS;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			result = 1;
		}
		return result;
	} /* field initPython */
	else if (strcmp(key, "initPython") == 0) {
		int result = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "ON") == 0) {
			userVars.initPython = INIT_PYTHON_ON;
		} else if (strcmp(str, "OFF") == 0) {
			userVars.initPython = INIT_PYTHON_OFF;
		} else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			result = 1;
		}
		return result;
	} /* field backupFileType */
	else if (strcmp(key, "outputFileType") == 0) {
		int result = 0;
		char str[MAX_STR_LEN];
		memset(str, 0, sizeof(str));
		strcpy(str, value);
		trim(str);
		/* select the correct option in enumaration */
		if (strcmp(str, "vtkBinary") == 0) {
			userVars.outputFileType = VTK_BINARY;
		} else if (strcmp(str, "ucdASCII") == 0) {
			userVars.outputFileType = UCD_ASCII;
		} else if (strcmp(str, "vtkXMLBase64") == 0) {
			userVars.outputFileType = VTK_XMLBASE64;
		}  else {
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, str, lineNum);
			result = 1;
		}
		return result;
	}

	/* given key is not found, error */
	fprintf(stderr, "(file %s | line %d) Mistake in line %i of input file! Key %s is unrecognized!\n", __FILE__, __LINE__, lineNum, key);
	fprintf(flog, "(file %s | line %d) Mistake in line %i of input file! Key %s is unrecognized!\n", __FILE__, __LINE__, lineNum, key);
	return 1;

}

/*int parseStringValue (const char * cvalue, char * str, bool *valsetup, int lineNum) {

  *valsetup = 0;
  regex_t regex_str;
  int ret;

    / prepare pattern for regular expression to parse string value /
  ret = regcomp(&regex_str, "^[[:space:]]*\".+\"[[:space:]]*$", REG_EXTENDED);
  if (ret) {
    char msg[] = "READDATA.C: cannot compile regular expression for string value!\n";
    fprintf(stderr, "%s", msg);
		fprintf(flog, "%s", msg);
		regfree(&regex_str);
		return 1;
  }

  / try to find required substring/
  ret = regexec(&regex_str, cvalue, 0, NULL, 0);
  if (!ret) {
		//char str[1000];
		memset(str,0,sizeof(str));
		strcpy(str,cvalue);
        *valsetup = 1;
		regfree(&regex_str);
		return 0;
	}
	else if (ret == REG_NOMATCH) {
		/ cvalue does not contain required substring/
		char msg[] = "READDATA.C: wrong value '%s' in line %i of input file!\n";
		fprintf(stderr, msg, cvalue, lineNum);
		fprintf(flog, msg, cvalue, lineNum);
		regfree(&regex_str);
		return 1;
	}

    / regex match failed at all... /
	char msgbuf[1000];
	regerror(ret, &regex_str, msgbuf, sizeof(msgbuf));
	char msg[] = "READDATA.C: regex failed in line %i of input file! MSG: %s\n";
	fprintf(stderr, msg, lineNum, msgbuf);
	fprintf(flog, msg, lineNum, msgbuf);
	regfree(&regex_str);
	return 1;

} */

int parseIntValue(const char *cvalue, int *ivalue, bool *valsetup, int lineNum) {

	*valsetup = 0;
	regex_t regex_int;
	int ret;

	/* prepare pattern for regular expression to parse integer value */
	ret = regcomp(&regex_int, "^[[:space:]]*[+-]?([1-9][[:digit:]]*|0)[[:space:]]*$", REG_EXTENDED);
	if (ret) {
		fprintf(stderr, "(file %s | line %d) cannot compile regular expression for int!\n", __FILE__, __LINE__);
		fprintf(flog, "(file %s | line %d) cannot compile regular expression for int!\n",  __FILE__, __LINE__);
		regfree(&regex_int);
		return 1;
	}

	/* try to parse cvalue */
	ret = regexec(&regex_int, cvalue, 0, NULL, 0);
	if (!ret) {
		const char *endptr = cvalue + strlen(cvalue) - 1;
		*ivalue = (int) strtol(cvalue, NULL, 10);

		if (endptr == cvalue) {
			/* strtol cannot convert cvalue to integer */
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, cvalue, lineNum);
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n",  __FILE__, __LINE__, cvalue, lineNum);
			regfree(&regex_int);
			return 1;
		} else {
			*valsetup = 1;
			regfree(&regex_int);
			return 0;
		}
	} else if (ret == REG_NOMATCH) {
		/* cvalue does not contain ingeger */
		fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, cvalue, lineNum);
		fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n", __FILE__, __LINE__, cvalue, lineNum);
		regfree(&regex_int);
		return 1;
	}

	/* regex match failed at all... */
	char msgbuf[1000];
	regerror(ret, &regex_int, msgbuf, sizeof(msgbuf));
	fprintf(stderr, "(file %s | line %d) Regex failed in line %i of input file! MSG: %s\n", __FILE__, __LINE__, lineNum, msgbuf);
	fprintf(flog, "(file %s | line %d) Regex failed in line %i of input file! MSG: %s\n", __FILE__, __LINE__, lineNum, msgbuf);
	regfree(&regex_int);
	return 1;

}

int parseDoubleValue(const char *cvalue, double *dvalue, bool *valsetup, int lineNum) {

	*valsetup = 0;
	regex_t regex_double;
	int ret;

	/* prepare pattern for regular expression to parse integer value */
	ret = regcomp(&regex_double,
	              "^[[:space:]]*[+-]?(([0-9]+\\.?[0-9]+[eE][+-][0-9]+)|([0-9]+\\.?[eE][+-]?[0-9]+)|([0-9]+\\.[0-9]*)|([0-9]+))[[:space:]]*$",
	              REG_EXTENDED);
	if (ret) {
		fprintf(stderr, "(file %s | line %d) Cannot compile regular expression for double!\n", __FILE__, __LINE__);
		fprintf(flog, "(file %s | line %d) Cannot compile regular expression for double!\n", __FILE__, __LINE__);
		regfree(&regex_double);
		return 1;
	}

	/* try to parse cvalue */
	ret = regexec(&regex_double, cvalue, 0, NULL, 0);
	if (!ret) {
		const char *endptr = cvalue + strlen(cvalue) - 1;
		*dvalue = strtod(cvalue, (char **) &endptr);

		if (endptr == cvalue) {
			/* strtol cannot convert cvalue to double */
			fprintf(stderr, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n",  __FILE__, __LINE__, cvalue, lineNum);
			fprintf(flog, "(file %s | line %d) Wrong value '%s' in line %i of input file!\n",  __FILE__, __LINE__, cvalue, lineNum);
			regfree(&regex_double);
			return 1;
		} else {
			*valsetup = 1;
			regfree(&regex_double);
			return 0;
		}
	} else if (ret == REG_NOMATCH) {
		/* cvalue does not contain double */
		char msg[] = "(file %s | line %d) wrong value '%s' in line %i of input file!\n";
		fprintf(stderr, msg, cvalue, lineNum);
		fprintf(flog, msg, cvalue, lineNum);
		regfree(&regex_double);
		return 1;
	}

	/* regex match failed at all... */
	char msgbuf[1000];
	regerror(ret, &regex_double, msgbuf, sizeof(msgbuf));
	fprintf(stderr, "(file %s | line %d) Regex failed in line %d of input file! MSG: %s\n", __FILE__, __LINE__, lineNum, msgbuf);
	regfree(&regex_double);
	return 1;

}

int readLatticeParams(const char filename[], double **latticeSites) {

	/* initialization */
	int ind;
	FILE *fid;
	char *str = NULL;
	int result = 0, lineNum = 1;
	char *headers[MAX_HEADERS_NUM];
	size_t maxFileTableRowNum = ((size_t)userVars.N1 - 2) * ((size_t)userVars.N2 - 2) * ((size_t)userVars.N3 - 2);
	double ** numbers = malloc(sizeof(double*)*MAX_HEADERS_NUM);
	for (ind = 0; ind < MAX_HEADERS_NUM; ind ++) {
		numbers[ind] = malloc(sizeof(double)*maxFileTableRowNum);
	}
	size_t fileTableColNum = 0, fileTableRowNum = 0;
	int headInd = 0;
	size_t lineLength = 0;
	char *comInd = NULL;

	/* open file using filename */
	fid = fopen(filename, "r");
	if (fid) {
		ind = (int)getline(&str, &lineLength, fid);
		/* if not the end of the file to continue to work */
		while ((ind != -1) && (!result) && (fileTableRowNum < maxFileTableRowNum)) {
			/* check if this is a comment */
			comInd = strstr(str, "@#");
			if (comInd == NULL) {
				/* first line without comments for headers */
				if (headInd == 0) {
					result = getHeaders(str, headers, &fileTableColNum, lineNum);
					headInd = 1;
				} else {
					/* the following lines without comments for numbers */
					result = parseColumns(str, numbers, fileTableColNum, fileTableRowNum, lineNum); //[latticeHeight]
					fileTableRowNum++;
				}
			}
			/* read new row */
			ind = (int)getline(&str, &lineLength, fid);
			lineNum++;
		}
		if ((fileTableRowNum != maxFileTableRowNum) && (result == 0)) {
			/* if the paramFile has small number of lines with input data, we will write error */
			result = 1;
			fprintf(stderr, "(file %s | line %d) Small number of lines with input data in paramFile!\n", __FILE__, __LINE__);
		} else if (result == 0) {
			/* if the file meets all the requirements, then we will write results in the array 'latticeSites' */
			size_t i = 0, j;
			for (i = 0; i < maxFileTableRowNum; i ++) {
				for (j = 0; j < MAX_HEADERS_NUM; j ++) {
					latticeSites[i][j] = numbers[j][i];
				}
			}
		}
	} else {
		/* if the file (paramFile) is not found, we will write error */
		result = 1;
		fprintf(stderr, "(file %s | line %d) Cannot find required file (paramFile)!\n", __FILE__, __LINE__);
		fprintf(stderr, "(file %s | line %d) You should write path to paramFile.\n", __FILE__, __LINE__);
	}

	/* close file and clear memory */
	fclose(fid);
	free(str);
	str = NULL;

	for (ind = 0; ind < MAX_HEADERS_NUM; ind ++) {
		free(numbers[ind]);
		numbers[ind] = NULL;
	}
	free(numbers);
	numbers = NULL;

	return result;

}

int setupIndexHeaders(const char *key, int colNum, int lineNum) {

	/* verify the key with the name of the UserVars structure field */
	/* you can see these fields in alltypes.h */
	/* these filds are indexes for updating latticeParam structure */
	int result = 0;
	/* field ix */
	if (strcmp(key, "x") == 0) {
		userVars.ix = colNum;
	} /* field iy */
	else if (strcmp(key, "y") == 0) {
		userVars.iy = colNum;
	} /* field iz */
	else if (strcmp(key, "z") == 0) {
		userVars.iz = colNum;
	} /* field imx */
	else if (strcmp(key, "mx") == 0) {
		userVars.imx = colNum;
	} /* field imy */
	else if (strcmp(key, "my") == 0) {
		userVars.imy = colNum;
	} /* field imz */
	else if (strcmp(key, "mz") == 0) {
		userVars.imz = colNum;
	} /* field igilbertDamping */
	else if (strcmp(key, "gilbertDamping") == 0) {
		userVars.igilbertDamping = colNum;
	} /* field iJm1 */
	else if (strcmp(key, "Jm1") == 0) {
		userVars.iJm1 = colNum;
	} /* field iJp1 */
	else if (strcmp(key, "Jp1") == 0) {
		userVars.iJp1 = colNum;
	} /* field iJm2 */
	else if (strcmp(key, "Jm2") == 0) {
		userVars.iJm2 = colNum;
	} /* field iJp2 */
	else if (strcmp(key, "Jp2") == 0) {
		userVars.iJp2 = colNum;
	} /* field iJm3 */
	else if (strcmp(key, "Jm3") == 0) {
		userVars.iJm3 = colNum;
	} /* field iJp3 */
	else if (strcmp(key, "Jp3") == 0) {
		userVars.iJp3 = colNum;
	} /* field iJ1 */
	else if (strcmp(key, "J1") == 0) {
		userVars.iJ1 = colNum;
	} /* field iJ2 */
	else if (strcmp(key, "J2") == 0) {
		userVars.iJ2 = colNum;
	} /* field iJ3 */
	else if (strcmp(key, "J3") == 0) {
		userVars.iJ3 = colNum;
	} /* field iJ4 */
	else if (strcmp(key, "J4") == 0) {
		userVars.iJ4 = colNum;
	} /* field iJ5 */
	else if (strcmp(key, "J5") == 0) {
		userVars.iJ5 = colNum;
	} /* field iJ6 */
	else if (strcmp(key, "J6") == 0) {
		userVars.iJ6 = colNum;
	} /* field ie11 */
	else if (strcmp(key, "e11") == 0) {
		userVars.ie11 = colNum;
	} /* field ie12 */
	else if (strcmp(key, "e12") == 0) {
		userVars.ie12 = colNum;
	} /* field ie13 */
	else if (strcmp(key, "e13") == 0) {
		userVars.ie13 = colNum;
	} /* field ie21 */
	else if (strcmp(key, "e21") == 0) {
		userVars.ie21 = colNum;
	} /* field ie22 */
	else if (strcmp(key, "e22") == 0) {
		userVars.ie22 = colNum;
	} /* field ie23 */
	else if (strcmp(key, "e23") == 0) {
		userVars.ie23 = colNum;
	} /* field ie31 */
	else if (strcmp(key, "e31") == 0) {
		userVars.ie31 = colNum;
	} /* field ie32 */
	else if (strcmp(key, "e32") == 0) {
		userVars.ie32 = colNum;
	} /* field ie33 */
	else if (strcmp(key, "e33") == 0) {
		userVars.ie33 = colNum;
	} /* field iD11 */
	else if (strcmp(key, "D11") == 0) {
		userVars.iD11 = colNum;
	} /* field iD12 */
	else if (strcmp(key, "D12") == 0) {
		userVars.iD12 = colNum;
	} /* field iD13 */
	else if (strcmp(key, "D13") == 0) {
		userVars.iD13 = colNum;
	} /* field iD21 */
	else if (strcmp(key, "D21") == 0) {
		userVars.iD21 = colNum;
	} /* field iD22 */
	else if (strcmp(key, "D22") == 0) {
		userVars.iD22 = colNum;
	} /* field iD23 */
	else if (strcmp(key, "D23") == 0) {
		userVars.iD23 = colNum;
	} /* field iD31 */
	else if (strcmp(key, "D31") == 0) {
		userVars.iD31 = colNum;
	} /* field iD32 */
	else if (strcmp(key, "D32") == 0) {
		userVars.iD32 = colNum;
	} /* field iD33 */
	else if (strcmp(key, "D33") == 0) {
		userVars.iD33 = colNum;
	} /* field idmiB1 */
	else if (strcmp(key, "dmiB1") == 0) {
		userVars.idmiB1 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field idmiB2 */
	else if (strcmp(key, "dmiB2") == 0) {
		userVars.idmiB2 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field idmiB3 */
	else if (strcmp(key, "dmiB3") == 0) {
		userVars.idmiB3 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field idmiB4 */
	else if (strcmp(key, "dmiB4") == 0) {
		userVars.idmiB4 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field idmiB5 */
	else if (strcmp(key, "dmiB5") == 0) {
		userVars.idmiB5 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field idmiB6 */
	else if (strcmp(key, "dmiB6") == 0) {
		userVars.idmiB6 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field idmiN1 */
	else if (strcmp(key, "dmiN1") == 0) {
		userVars.idmiN1 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field idmiN2 */
	else if (strcmp(key, "dmiN2") == 0) {
		userVars.idmiN2 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field idmiN3 */
	else if (strcmp(key, "dmiN3") == 0) {
		userVars.idmiN3 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field idmiN4 */
	else if (strcmp(key, "dmiN4") == 0) {
		userVars.idmiN4 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field idmiN5 */
	else if (strcmp(key, "dmiN5") == 0) {
		userVars.idmiN5 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field idmiN6 */
	else if (strcmp(key, "dmiN6") == 0) {
		userVars.idmiN6 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field ianiInterK1m1 */
	else if (strcmp(key, "aniInterK1m1") == 0) {
		userVars.ianiInterK1m1 = colNum;
	} /* field ianiInterK1p1 */
	else if (strcmp(key, "aniInterK1p1") == 0) {
		userVars.ianiInterK1p1 = colNum;
	} /* field ianiInterK1m2 */
	else if (strcmp(key, "aniInterK1m2") == 0) {
		userVars.ianiInterK1m2 = colNum;
	} /* field ianiInterK1p2 */
	else if (strcmp(key, "aniInterK1p2") == 0) {
		userVars.ianiInterK1p2 = colNum;
	} /* field ianiInterK1m3 */
	else if (strcmp(key, "aniInterK1m3") == 0) {
		userVars.ianiInterK1m3 = colNum;
	} /* field ianiInterK1p3 */
	else if (strcmp(key, "aniInterK1p3") == 0) {
		userVars.ianiInterK1p3 = colNum;
	} /* field ianiInterK2m1 */
	else if (strcmp(key, "aniInterK2m1") == 0) {
		userVars.ianiInterK2m1 = colNum;
	} /* field ianiInterK2p1 */
	else if (strcmp(key, "aniInterK2p1") == 0) {
		userVars.ianiInterK2p1 = colNum;
	} /* field ianiInterK2m2 */
	else if (strcmp(key, "aniInterK2m2") == 0) {
		userVars.ianiInterK2m2 = colNum;
	} /* field ianiInterK2p2 */
	else if (strcmp(key, "aniInterK2p2") == 0) {
		userVars.ianiInterK2p2 = colNum;
	} /* field ianiInterK2m3 */
	else if (strcmp(key, "aniInterK2m3") == 0) {
		userVars.ianiInterK2m3 = colNum;
	} /* field ianiInterK2p3 */
	else if (strcmp(key, "aniInterK2p3") == 0) {
		userVars.ianiInterK2p3 = colNum;
	} /* field ianiInterK3m1 */
	else if (strcmp(key, "aniInterK3m1") == 0) {
		userVars.ianiInterK3m1 = colNum;
	} /* field ianiInterK3p1 */
	else if (strcmp(key, "aniInterK3p1") == 0) {
		userVars.ianiInterK3p1 = colNum;
	} /* field ianiInterK3m2 */
	else if (strcmp(key, "aniInterK3m2") == 0) {
		userVars.ianiInterK3m2 = colNum;
	} /* field ianiInterK3p2 */
	else if (strcmp(key, "aniInterK3p2") == 0) {
		userVars.ianiInterK3p2 = colNum;
	} /* field ianiInterK3m3 */
	else if (strcmp(key, "aniInterK3m3") == 0) {
		userVars.ianiInterK3m3 = colNum;
	} /* field ianiInterK3p3 */
	else if (strcmp(key, "aniInterK3p3") == 0) {
		userVars.ianiInterK3p3 = colNum;
	} /* field imass */
	else if (strcmp(key, "mass") == 0) {
		userVars.imass = colNum;
	} /* field imechDamping */
	else if (strcmp(key, "mechDamping") == 0) {
		userVars.imechDamping = colNum;
	} /* field iBxamp */
	else if (strcmp(key, "Bxamp") == 0) {
		userVars.iBxamp = colNum;
	} /* field iByamp */
	else if (strcmp(key, "Byamp") == 0) {
		userVars.iByamp = colNum;
	} /* field iBzamp */
	else if (strcmp(key, "Bzamp") == 0) {
		userVars.iBzamp = colNum;
	} /* field ivx */
	else if (strcmp(key, "vx") == 0) {
		userVars.ivx = colNum;
	} /* field ivy */
	else if (strcmp(key, "vy") == 0) {
		userVars.ivy = colNum;
	} /* field ivz */
	else if (strcmp(key, "vz") == 0) {
		userVars.ivz = colNum;
	} /* field inrmX */
	else if (strcmp(key, "nrmX") == 0) {
		userVars.inrmX = colNum;
	} /* field inrmY */
	else if (strcmp(key, "nrmY") == 0) {
		userVars.inrmY = colNum;
	} /* field inrmZ */
	else if (strcmp(key, "nrmZ") == 0) {
		userVars.inrmZ = colNum;
	} /* field iFx */
	else if (strcmp(key, "Fx") == 0) {
		userVars.iFx = colNum;
	} /* field iFy */
	else if (strcmp(key, "Fy") == 0) {
		userVars.iFy = colNum;
	} /* field iFz */
	else if (strcmp(key, "Fz") == 0) {
		userVars.iFz = colNum;
	} /* field ilambda1 */
	else if (strcmp(key, "lambda1") == 0) {
		userVars.ilambda1 = colNum;
	} /* field ilambda2 */
	else if (strcmp(key, "lambda2") == 0) {
		userVars.ilambda2 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field ilambda3 */
	else if (strcmp(key, "lambda3") == 0) {
		userVars.ilambda3 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field ilambda4 */
	else if (strcmp(key, "lambda4") == 0) {
		userVars.ilambda4 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field ilambda5 */
	else if (strcmp(key, "lambda5") == 0) {
		userVars.ilambda5 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	} /* field ilambda6 */
	else if (strcmp(key, "lambda6") == 0) {
		userVars.ilambda6 = colNum;
		/* variable for triangular lattice was entered */
		indVarTri = true;
	}  /* field iFphase */
	else if (strcmp(key, "Fphase") == 0) {
		userVars.iFphase = colNum;
	} /* field iFfreq */
	else if (strcmp(key, "Ffreq") == 0) {
		userVars.iFfreq = colNum;
	}/* field iMagn */
	else if (strcmp(key, "magn") == 0) {
		userVars.iMagn = colNum;
	} /* field ibeta */
	else if (strcmp(key, "beta") == 0) {
		userVars.ibeta = colNum;
	} /* field ilambda */
	else if (strcmp(key, "lambda") == 0) {
		userVars.ilambda = colNum;
	} /* field ifieldSincTWidth */
	else if (strcmp(key, "Bsinc_twidth") == 0) {
		userVars.ifieldSincTWidth = colNum;
	} /* field ifieldSincTShift */
	else if (strcmp(key, "Bsinc_tshift") == 0) {
		userVars.ifieldSincTShift = colNum;
	} else {
		/* given key is not found, error */
		fprintf(stderr, "(file %s | line %d) Mistake in line %i of paramfile! Identifier '%s' in column %i is not correct\n", __FILE__, __LINE__, lineNum, key, colNum);
		result = 1;
	}

	/* return code of error */
	return result;

}

int getHeaders(char *str, char **headers, size_t *headNum, int lineNum) {

	int result = 0;
	//char *key = NULL;
	char *spaceind = NULL;
	/* find a comment and delete it */
	int str_len = (int)strlen(str);
	str[str_len - 1] = 0;
	int i = 0;
	while (i <= str_len - 1) {
		if ((str[i] == '#') || (str[i] == '\r') || (str[i] == '\n')) {
			str[i] = 0;
			break;
		}
		i++;
	}

	/* divide a string into substrings by the symbol ' ' */
	spaceind = strtok(str, " \t");
	size_t headersNum = 0; //, pok = 0;
	while ((spaceind != NULL) && (!result)) {
		headers[headersNum] = spaceind;
		/* write in userVars colomns of structure's field LatticeSite */
		result = setupIndexHeaders(spaceind, headersNum, lineNum);
		spaceind = strtok(NULL, " \t");
		headersNum++;
	}
	*headNum = headersNum;

	/* if nothing is in the line */
	if (headersNum == 0) {
		fprintf(stderr, "(file %s | line %d) Nothing was entered in line %d of paramFile!\n", __FILE__, __LINE__, lineNum);
		result = 1;
	}

	return result;

}

int parseColumns(char *str, double ** numbers, size_t headNum, size_t currentRow, int lineNum) {

	int result = 0;
	char *spaceind = NULL;
	/* find a comment and delete it */
	size_t str_len = strlen(str);
	size_t i = 0;
	while (i <= str_len - 1) {
		if ((str[i] == '#') || (str[i] == '\r') || (str[i] == '\n')) {
			str[i] = 0;
			break;
		}
		i++;
	}

	/* divide a string into substrings by the symbol ' ' */
	spaceind = strtok(str, " \t");
	size_t colNum = 0;
	bool valsetup = 0;
	while ((spaceind != NULL) && (result == 0)) {
		result = parseDoubleValue(spaceind, &(numbers[colNum][currentRow]), &valsetup, lineNum);
		spaceind = strtok(NULL, " \t");
		colNum++;
	}

	/* if the amount of numbers does not coincide with the number of headers */
	if ((colNum != headNum) && (result == 0)) {
		i = 0;
		while (i <= colNum - 1) {
			numbers[i] = 0;
			i++;
		}
		fprintf(stderr, "(file %s | line %d) The amount of numbers does not coincide with the amount of headers in line %d of paramFile!\n", __FILE__, __LINE__, lineNum);
		result = 1;
	}

	return result;

}

int readScalarVariablesBin(const char filename[]) {

	/* declaration of file object */
	FILE *file;
	/* open file using filename */
	file = fopen(filename, "r");

	/* if the file is not found, we will write error */
	if (file == NULL) {
		char msg[] = "Cannot find input binary file:";
		fprintf(stderr, "(file %s | line %d) %s %s\n", __FILE__, __LINE__, msg, filename);
		return 1;
	}

	/* intermediate values for reading */
	int numberScalarsInter;
	int numberFieldsInter;
	int N1Inter, N2Inter, N3Inter;
	int latticeTypeInter;
	int launchTypeInter;

	/* read variables about type and size of lattice */
	fread(&numberScalarsInter, sizeof(int), 1, file); /* scalar field 1 */
	fread(&numberFieldsInter, sizeof(int), 1, file); /* scalar field 2 */
	fread(&N1Inter, sizeof(int), 1, file); /* scalar field 3 */
	fread(&N2Inter, sizeof(int), 1, file); /* scalar field 4 */
	fread(&N3Inter, sizeof(int), 1, file); /* scalar field 5 */
	fread(&latticeTypeInter, sizeof(int), 1, file); /* scalar field 6 */
	fread(&launchTypeInter, sizeof(int), 1, file); /* scalar field 7 */

	/* read different energies */
	fread(&EnergyOfSystem, sizeof(double), 1, file); /* scalar field 8 */
	fread(&EnergyExchange, sizeof(double), 1, file); /* scalar field 9 */
	fread(&EnergyAnisotropyAxisK1, sizeof(double), 1, file); /* scalar field 10 */
	fread(&EnergyAnisotropyAxisK2, sizeof(double), 1, file); /* scalar field 11 */
	fread(&EnergyAnisotropyAxisK3, sizeof(double), 1, file); /* scalar field 12 */
	fread(&EnergyDipole, sizeof(double), 1, file); /* scalar field 13 */
	fread(&EnergyExternal, sizeof(double), 1, file); /* scalar field 14 */
	fread(&EnergyDMIAxis1, sizeof(double), 1, file); /* scalar field 15 */
	fread(&EnergyDMIAxis2, sizeof(double), 1, file); /* scalar field 16 */
	fread(&EnergyDMIAxis3, sizeof(double), 1, file); /* scalar field 17 */
	fread(&EnergyDMITriangleBulk, sizeof(double), 1, file); /* scalar field 18 */
	fread(&EnergyDMITriangleInterface, sizeof(double), 1, file); /* scalar field 19 */
	fread(&EnergyStretching, sizeof(double), 1, file); /* scalar field 20 */
	fread(&EnergyBending, sizeof(double), 1, file); /* scalar field 21 */

	/* read total magnetic moments for each axis */
	fread(&mxTot, sizeof(double), 1, file); /* scalar field 22 */
	fread(&myTot, sizeof(double), 1, file); /* scalar field 23 */
	fread(&mzTot, sizeof(double), 1, file); /* scalar field 24 */

	/* read uniform external field */
	fread(&userVars.Bx, sizeof(double), 1, file); /* scalar field 25 */
	fread(&userVars.By, sizeof(double), 1, file); /* scalar field 26 */
	fread(&userVars.Bz, sizeof(double), 1, file); /* scalar field 27 */

	if (launchTypeInter == LAUNCH_SIMULATION) {
		/* read time variables */
		fread(&MaxDmDt, sizeof(double), 1, file); /* scalar field 28 */
		fread(&userVars.t0, sizeof(double), 1, file); /* scalar field 29 */
		fread(&integrationStepMin, sizeof(double), 1, file); /* scalar field 30 */
		fread(&integrationStepMax, sizeof(double), 1, file); /* scalar field 31 */
		fread(&frameNumber, sizeof(unsigned long long), 1, file); /* scalar field 32 */
		fread(&numberOfFile, sizeof(unsigned long long), 1, file); /* scalar field 33 */
	} else {
		fprintf(stderr, "(file %s | line %d) Launch type is not \"simulation\", not implemented.\n", __FILE__, __LINE__);
	}


	/* close file and finish work */
	fclose(file);
	return 0;

}

/* function to read scalar variables from binary file using descriptor */
int readScalarVariablesDescr(FILE *file) {

	/* intermediate values for reading */
	int numberScalarsInter;
	int numberFieldsInter;
	int N1Inter, N2Inter, N3Inter;
	int latticeTypeInter;
	int launchTypeInter;

	/* read variables about type and size of lattice */
	fread(&numberScalarsInter, sizeof(int), 1, file); /* scalar field 1 */
	fread(&numberFieldsInter, sizeof(int), 1, file); /* scalar field 2 */
	fread(&N1Inter, sizeof(int), 1, file); /* scalar field 3 */
	fread(&N2Inter, sizeof(int), 1, file); /* scalar field 4 */
	fread(&N3Inter, sizeof(int), 1, file); /* scalar field 5 */
	fread(&latticeTypeInter, sizeof(int), 1, file); /* scalar field 6 */

	/* read variable about type of launch */
	fread(&launchTypeInter, sizeof(int), 1, file); /* scalar field 7 */

	/* read different energies */
	fread(&EnergyOfSystem, sizeof(double), 1, file); /* scalar field 8 */
	fread(&EnergyExchange, sizeof(double), 1, file); /* scalar field 9 */
	fread(&EnergyAnisotropyAxisK1, sizeof(double), 1, file); /* scalar field 10 */
	fread(&EnergyAnisotropyAxisK2, sizeof(double), 1, file); /* scalar field 11 */
	fread(&EnergyAnisotropyAxisK3, sizeof(double), 1, file); /* scalar field 12 */
	fread(&EnergyDipole, sizeof(double), 1, file); /* scalar field 13 */
	fread(&EnergyExternal, sizeof(double), 1, file); /* scalar field 14 */
	fread(&EnergyDMIAxis1, sizeof(double), 1, file); /* scalar field 15 */
	fread(&EnergyDMIAxis2, sizeof(double), 1, file); /* scalar field 16 */
	fread(&EnergyDMIAxis3, sizeof(double), 1, file); /* scalar field 17 */
	fread(&EnergyDMITriangleBulk, sizeof(double), 1, file); /* scalar field 18 */
	fread(&EnergyDMITriangleInterface, sizeof(double), 1, file); /* scalar field 19 */
	fread(&EnergyStretching, sizeof(double), 1, file); /* scalar field 20 */
	fread(&EnergyBending, sizeof(double), 1, file); /* scalar field 21 */

	/* read total magnetic moments for each axis */
	fread(&mxTot, sizeof(double), 1, file); /* scalar field 22 */
	fread(&myTot, sizeof(double), 1, file); /* scalar field 23 */
	fread(&mzTot, sizeof(double), 1, file); /* scalar field 24 */

	/* read uniform external field */
	fread(&userVars.Bx, sizeof(double), 1, file); /* scalar field 25 */
	fread(&userVars.By, sizeof(double), 1, file); /* scalar field 26 */
	fread(&userVars.Bz, sizeof(double), 1, file); /* scalar field 27 */

	if (launchTypeInter == LAUNCH_SIMULATION) {
		/* read time variables */
		fread(&MaxDmDt, sizeof(double), 1, file); /* scalar field 28 */
		fread(&userVars.t0, sizeof(double), 1, file); /* scalar field 29 */
		fread(&integrationStepMin, sizeof(double), 1, file); /* scalar field 30 */
		fread(&integrationStepMax, sizeof(double), 1, file); /* scalar field 31 */
		fread(&frameNumber, sizeof(unsigned long long), 1, file); /* scalar field 32 */
		fread(&numberOfFile, sizeof(unsigned long long), 1, file); /* scalar field 33 */
	} else {
		/* return error */
		fprintf(stderr, "(file %s | line %d) Launch type is not \"simulation\", not implemented.\n", __FILE__, __LINE__);
		return 1;
	}

	return 0;

}

/* function to read "lattice" array from binary file using descriptor */
int readLatticeArray(FILE *file, double *array, long shift) {

	/* read indicator and size of vector field */
	int ind, size;
	fread(&ind, sizeof(int), 1, file);
	fread(&size, sizeof(int), 1, file);

	/* declaration of variables */
	double varX, varY, varZ;
	size_t curIndex = 0;
	size_t indexLattice = 0;

	/* go through all sites */
	int i1, i2, i3;
	for (i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
					indexLattice = (size_t)IDX(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
					indexLattice = (size_t)IDXtr(i1, i2);
				}
				/* read variables for array */
				fread(&varX, sizeof(double), 1, file);
				fread(&varY, sizeof(double), 1, file);
				fread(&varZ, sizeof(double), 1, file);
				/* handle "nan" value */
				if ((isnan(varX) || isnan(varY) || isnan(varZ)) && latticeParam[indexLattice].magnetic) {
					/* return error */
					fprintf(stderr, "(file %s | line %d) Lattice site with indices (%d, %d, %d) has NaN magnetization, but marked as magnetic one!\n", __FILE__, __LINE__, i1, i2, i3);
					return 1;
				} else if ((isnan(varX) || isnan(varY) || isnan(varZ)) && (!latticeParam[indexLattice].magnetic)) {
					/* non-magnetic site */
					varX = 0.0;
					varY = 0.0;
					varZ = 0.0;
				}
				/* save result in array */
				array[curIndex + shift] = varX;
				array[curIndex + shift + 1] = varY;
				array[curIndex + shift + 2] = varZ;
			}
		}
	}

	/* return default value */
	return 0;

	// to debug code
	//curIndex = IDXmg(20, 10, 1);
	//printf("%f %f %f\n", array[curIndex + shift], array[curIndex + shift + 1], array[curIndex + shift + 2]);

}

void normalizeCoord(void) {

	/* calculate shift for coordinates */
	long shift = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* normalize coordinates on lattice constant */
	size_t curIndex = 0;
	for (int i1 = 1; i1 < userVars.N1 - 1; i1++) {
		for (int i2 = 1; i2 < userVars.N2 - 1; i2++) {
			for (int i3 = 1; i3 < userVars.N3 - 1; i3++) {
				if (userVars.latticeType == LATTICE_CUBIC) {
					curIndex = (size_t)IDXmg(i1, i2, i3);
				} else if (userVars.latticeType == LATTICE_TRIANGULAR) {
					curIndex = (size_t)IDXtrmg(i1, i2);
				}
				lattice[curIndex + shift]     = lattice[curIndex + shift] / userVars.a;
				lattice[curIndex + shift + 1] = lattice[curIndex + shift + 1] / userVars.a;
				lattice[curIndex + shift + 2] = lattice[curIndex + shift + 2] / userVars.a;
			}
		}
	}

}

int readAllBin(const char filename[]) {

	/* declaration of file object */
	FILE *file;
	/* open file using filename */
	file = fopen(filename, "r");

	/* if the file is not found, we will write error */
	if (file == NULL) {
		char msg[] = "Cannot find input binary file:";
		fprintf(stderr, "(file %s | line %d) %s %s\n", __FILE__, __LINE__, msg, filename);
		return 1;
	}

	/* read scalar variables */
	if (readScalarVariablesDescr(file)) {
		return 1;
	}

	/* calculate shift for coordinates */
	long shift = NUMBER_COMP_MAGN * userVars.N1 * userVars.N2 * userVars.N3;

	/* read vector fields (magnetic moments and coordinates) */
	if (readLatticeArray(file, lattice, ZERO_SHIFT)) {
		return 1;
	}
	if (readLatticeArray(file, lattice, shift)) {
		return 1;
	}
	/* normalize coordinates */
	normalizeCoord();

	/* close file and finish work */
	fclose(file);
	return 0;

}
